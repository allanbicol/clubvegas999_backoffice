<html>
<head>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
	

	<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/gamesetting.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		//active menu color
		document.getElementById('tablelimitHeader').className="start active";
		document.getElementById('mnu_table_limit').className="active";
		$('.breadcrumb').html("<li><i class='icon-home'></i>Game Setting <i class='icon-angle-right'></i></li><li><a href='#'>Table Limit</a></li>");
		
	</script>
	<script type="text/javascript">
		var cRowNo=0;
		function tableBacarrat(currency_id) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableBacarratLimit&currency_id='+currency_id, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['No.', 'All','Banker Player','Tie','Pair','Big','Small','Setting'],
				    colModel: [
				      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
				      {name: 'all', index: 'all', width: 150, sortable: false},
				      {name: 'banker_player', index: 'banker_player', width: 150, sortable: false},
				      {name: 'tie', index: 'tie', width: 150, sortable: false},
				      {name: 'pair', index: 'pair', width: 150,sortable: false},
				      {name: 'big', index: 'big', width: 150,sortable: false},
				      {name: 'small', index: 'small', width: 150,sortable: false},
				      {name: 'setting', index: 'setting', width: 60, align: 'right', sortable: false}
				    ],
				    beforeSelectRow: function(rowid,e){
				    	var bac_limit_row = grid.jqGrid('getRowData', rowid);
				    	cRowNo = rowid;
				    	document.getElementById('txtUAllMax').tag=bac_limit_row.no;
						document.getElementById('txtUAllMax').value=bac_limit_row.all.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAllMin').value=bac_limit_row.all.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUBankerPlayerMax').value=bac_limit_row.banker_player.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUBankerPlayerMin').value=bac_limit_row.banker_player.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUTieMax').value=bac_limit_row.tie.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUTieMin').value=bac_limit_row.tie.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUPairMax').value=bac_limit_row.pair.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUPairMin').value=bac_limit_row.pair.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUBigMax').value=bac_limit_row.big.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUBigMin').value=bac_limit_row.big.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUSmallMax').value=bac_limit_row.small.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUSmallMin').value=bac_limit_row.small.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						},
					loadComplete: function(){
						$("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
					},
					loadtext:'',
					rowNum: 99999,
					width:1300,
                    rownumbers: true,
                    sortable: false,
				    sortname: 'no',
				    sortorder: 'ASC',
				    caption: 'Baccarat Limit (Min - Max) <button class="btn red" id="btnBac" value="Add" onclick="javascript: $addBacarratLimitDialog.dialog(\'open\');bacarratLimitFieldClear();">Add <i class="icon-plus"></i></button>',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list1').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false});
			});
		} 
		function tableDragonTiger(currency_id) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list2");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableDragonTigerLimit&currency_id='+currency_id, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['No.', 'All','Dragon Tiger','Tie','Setting'],
				    colModel: [
				      {name: 'no', index: 'no', width: 40, search:true, hidden: true},
				      {name: 'all', index: 'all', width: 120, sortable: false},
				      {name: 'dragon_tiger', index: 'dragon_tiger', width: 120, sortable: false},
				      {name: 'tie', index: 'tie', width: 120, sortable: false},
				      {name: 'setting', index: 'setting', width: 25, align: 'right', sortable: false}
				    ],
				    beforeSelectRow: function(rowid,e){
				    	var bac_limit_row = grid.jqGrid('getRowData', rowid);
				    	cRowNo = rowid;
				    	document.getElementById('txtUDragonTigerAllMax').tag=bac_limit_row.no;
						document.getElementById('txtUDragonTigerAllMax').value=bac_limit_row.all.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUDragonTigerAllMin').value=bac_limit_row.all.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUDragonTigerMax').value=bac_limit_row.dragon_tiger.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUDragonTigerMin').value=bac_limit_row.dragon_tiger.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUDragonTigerTieMax').value=bac_limit_row.tie.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUDragonTigerTieMin').value=bac_limit_row.tie.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
					},
					loadComplete: function(){
						$("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
					},
					loadtext:'',
					rowNum: 99999,
					width: 1300,
				    rownumbers: true,
				    sortname: 'no',
				    sortorder: 'ASC',
				    caption: 'Dragon Tiger Limit (Min - Max) <button class="btn red" id="btnDra" value="Add" onclick="javascript: $addDragonTigerLimitDialog.dialog(\'open\');dragonLimitFieldClear();">Add <i class="icon-plus"></i></button>',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });
			});
		} 
		function tableRoulette(currency_id) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list3'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list3");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableRouletteLimit&currency_id='+currency_id,
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['No.', 'All','35 to 1','17 to 1','11 to 1','8 to 1','5 to 1','2 to 1','1 to 1','Setting'],
				    colModel: [
				      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
				      {name: 'all', index: 'all', width: 50, sortable: false},
				      {name: 'r_35_1', index: 'r_35_1', width: 50, sortable: false},
				      {name: 'r_18_1', index: 'r_18_1', width: 50, sortable: false},
				      {name: 'r_12_1', index: 'r_12_1', width: 50, sortable: false},
				      {name: 'r_9_1', index: 'r_9_1', width: 50, sortable: false},
				      {name: 'r_6_1', index: 'r_6_1', width: 50, sortable: false},
				      {name: 'r_2_1', index: 'r_2_1', width: 50, sortable: false},
				      {name: 'r_1_1', index: 'r_1_1', width: 50, sortable: false},
				      {name: 'setting', index: 'setting', width: 28,  align: 'right', sortable: false}
				    ],
				    beforeSelectRow: function(rowid,e){
				    	var bac_limit_row = grid.jqGrid('getRowData', rowid);
				    	cRowNo = rowid;
				    	document.getElementById('txtURouletteAllMax').tag=bac_limit_row.no;
						document.getElementById('txtURouletteAllMax').value=bac_limit_row.all.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtURouletteAllMin').value=bac_limit_row.all.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU35_1max').value=bac_limit_row.r_35_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU35_1min').value=bac_limit_row.r_35_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU18_1max').value=bac_limit_row.r_18_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU18_1min').value=bac_limit_row.r_18_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU12_1max').value=bac_limit_row.r_12_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU12_1min').value=bac_limit_row.r_12_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU9_1max').value=bac_limit_row.r_9_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU9_1min').value=bac_limit_row.r_9_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU6_1max').value=bac_limit_row.r_6_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU6_1min').value=bac_limit_row.r_6_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU2_1max').value=bac_limit_row.r_2_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU2_1min').value=bac_limit_row.r_2_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU1_1max').value=bac_limit_row.r_1_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtU1_1min').value=bac_limit_row.r_1_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
					},
					loadComplete: function(){
						$("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
					},
					loadtext:'',
					rowNum: 99999,
					width: 1300,
				    rownumbers: true,
				    sortname: 'id',
				    sortorder: 'ASC',
				    caption: 'European Roulette Limit (Min - Max) <button class="btn red" id="btnEur" value="Add" onclick="javascript: $addRouletteLimitDialog.dialog(\'open\'); rouletteLimitFieldClear();">Add <i class="icon-plus"></i></button>',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list3').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });
			});
		} 
		function tableSlot(currency_id) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list4'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list4");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CurrencyList/CurrencyList&currency_id='+currency_id, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['No.', 'Currency','Exchange rate','Last Update','Setting'],
				    colModel: [
				      {name: 'id', index: 'id', width: 50, search:true},
				      {name: 'currency_name', index: 'currency_name', width: 100},
				      {name: 'exchange_rate', index: 'exchange_rate', width: 100},
				      {name: 'last_update', index: 'last_update', width: 130},
				      {name: 'setting', index: 'setting', width: 20}
				    ],
				    loadComplete: function(){
				    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
					},
					loadtext:'',
					rowNum: 99999,
				    width: 1300,
				    rownumbers: true,
				    sortname: 'id',
				    sortorder: 'ASC',
				    caption: 'Slot Limit (Min - Max) <input type="button" value="Add"/>',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list4').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });
			});
		}

		function tableBlackjack(currency_id) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list5'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list5");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableBlackjackLimit&currency_id='+currency_id,
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['No.', 'Main','Pair','Rummy','Insurance','Setting'],
				    colModel: [
				      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
				      {name: 'all', index: 'all', width: 50, sortable: false},
				      {name: 'pair', index: 'pair', width: 50, sortable: false},
				      {name: 'rummy', index: 'rummy', width: 50, sortable: false},
				      {name: 'insurance', index: 'insurance', width: 50, sortable: false},
				      {name: 'setting', index: 'setting', width: 14, align: 'right', sortable: false},
				    ],
				    beforeSelectRow: function(rowid,e){
				    	var bac_limit_row = grid.jqGrid('getRowData', rowid);
				    	cRowNo = rowid;
				    	document.getElementById('txtUBlackjackAllMax').tag=bac_limit_row.no;
						document.getElementById('txtUBlackjackAllMax').value=bac_limit_row.all.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUBlackjackAllMin').value=bac_limit_row.all.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUBlkPairMax').value=bac_limit_row.pair.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUBlkPairMin').value=bac_limit_row.pair.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtURummyMax').value=bac_limit_row.rummy.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtURummyMin').value=bac_limit_row.rummy.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUInsuranceMax').value=bac_limit_row.insurance.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUInsuranceMin').value=bac_limit_row.insurance.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
					},
					loadComplete: function(){
						$("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
					},
					loadtext:'',
					rowNum: 99999,
					width: 1300,
				    rownumbers: true,
				    sortname: 'id',
				    sortorder: 'ASC',
				    caption: 'Blackjack Limit (Min - Max) <button class="btn red" id="btnBla" value="Add" onclick="javascript: $addBlackjackLimitDialog.dialog(\'open\'); blackjackLimitFieldClear();">Add <i class="icon-plus"></i></button>',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list5').jqGrid('navGrid', '#pager5', {edit:false, add:false, del:false, search:true });
			});
		} 

		function tableAmericanRoulette(currency_id) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list6'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list6");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableAmericanRouletteLimit&currency_id='+currency_id,
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['No.', 'All','35 to 1','17 to 1','11 to 1','8 to 1','5 to 1','2 to 1','1 to 1','Setting'],
				    colModel: [
				      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
				      {name: 'all', index: 'all', width: 50, sortable: false,hidden:true},
				      {name: 'r_35_1', index: 'r_35_1', width: 50, sortable: false},
				      {name: 'r_18_1', index: 'r_18_1', width: 50, sortable: false},
				      {name: 'r_12_1', index: 'r_12_1', width: 50, sortable: false},
				      {name: 'r_9_1', index: 'r_9_1', width: 50, sortable: false},
				      {name: 'r_6_1', index: 'r_6_1', width: 50, sortable: false},
				      {name: 'r_2_1', index: 'r_2_1', width: 50, sortable: false},
				      {name: 'r_1_1', index: 'r_1_1', width: 50, sortable: false},
				      {name: 'setting', index: 'setting', width: 25,  align: 'right', sortable: false}
				    ],
				    loadComplete: function(){
				    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
					},
				    beforeSelectRow: function(rowid,e){
				    	var bac_limit_row = grid.jqGrid('getRowData', rowid);
				    	cRowNo = rowid;
				    	document.getElementById('txtUAR35_1max').tag=bac_limit_row.no;
						//document.getElementById('txtUAmericanRouletteAllMax').value=bac_limit_row.all.split(' - ')[1];
						//document.getElementById('txtUAmericanRouletteAllMin').value=bac_limit_row.all.split(' - ')[0];
						document.getElementById('txtUAR35_1max').value=bac_limit_row.r_35_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR35_1min').value=bac_limit_row.r_35_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR18_1max').value=bac_limit_row.r_18_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR18_1min').value=bac_limit_row.r_18_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR12_1max').value=bac_limit_row.r_12_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR12_1min').value=bac_limit_row.r_12_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR9_1max').value=bac_limit_row.r_9_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR9_1min').value=bac_limit_row.r_9_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR6_1max').value=bac_limit_row.r_6_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR6_1min').value=bac_limit_row.r_6_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR2_1max').value=bac_limit_row.r_2_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR2_1min').value=bac_limit_row.r_2_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR1_1max').value=bac_limit_row.r_1_1.split(' - ')[1].replace(",","").replace(",","").replace(",","").replace(",","");
						document.getElementById('txtUAR1_1min').value=bac_limit_row.r_1_1.split(' - ')[0].replace(",","").replace(",","").replace(",","").replace(",","");
					},
					loadtext:'',
					rowNum: 99999,
					width: 1300,
				    rownumbers: true,
				    sortname: 'id',
				    sortorder: 'ASC',
				    caption: 'American Roulette Limit (Min - Max) <button class="btn red" id="btnAme" value="Add" onclick="javascript: $addAmericanRouletteLimitDialog.dialog(\'open\'); americanRouletteLimitFieldClear();">Add <i class="icon-plus"></i></button>',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search:true });
			});
		} 
		//Dialoags
		var $addBacarratLimitDialog='';
		$(document).ready(function() {
		$addBacarratLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">All Max</td><td class="right"><input  type="text"/ id="txtAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Banker Player Max</td><td class="right"><input type="text"/ id="txtBankerPlayerMax"><font color="red"> *</font></td><td class="left">Banker Player Min</td><td class="right"><input type="text"/ id="txtBankerPlayerMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Tie Max</td><td class="right"><input type="text"/ id="txtTieMax"><font color="red"> *</font></td><td class="left">Tie Min</td><td class="right"><input type="text"/ id="txtTieMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Pair Max</td><td class="right"><input type="text"/ id="txtPairMax"><font color="red"> *</font></td><td class="left">Pair Min</td><td class="right"><input type="text"/ id="txtPairMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Big Max</td><td class="right"><input type="text"/ id="txtBigMax"><font color="red"> *</font></td><td class="left">Big Min</td><td class="right"><input type="text"/ id="txtBigMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Small Max</td><td class="right"><input type="text"/ id="txtSmallMax"><font color="red"> *</font></td><td class="left">Small Min</td><td class="right"><input type="text"/ id="txtSmallMin"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-bacarrat-limit" >Confirm</button><br/><input type="text" id="bacarrat_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Add Baccarat Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-bacarrat-limit').click(function(event) {
				if(trim(document.getElementById('txtAllMax').value)==''){document.getElementById('bacarrat_err_message').value='All Max is required!';document.getElementById('txtAllMax').focus(); return false;}else if(validate(document.getElementById('txtAllMax').value)==false){document.getElementById('bacarrat_err_message').value='Invalid all max!';document.getElementById('txtAllMax').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtAllMin').value)==''){document.getElementById('bacarrat_err_message').value='All Min is required!';document.getElementById('txtAllMin').focus(); return false;}else if(validate(document.getElementById('txtAllMin').value)==false){document.getElementById('bacarrat_err_message').value='Invalid all min!';document.getElementById('txtAllMin').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtBankerPlayerMax').value)==''){document.getElementById('bacarrat_err_message').value='Banker Player Max is required!';document.getElementById('txtBankerPlayerMax').focus(); return false;}else if(validate(document.getElementById('txtBankerPlayerMax').value)==false){document.getElementById('bacarrat_err_message').value='Invalid banker player max!';document.getElementById('txtBankerPlayerMax').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtBankerPlayerMin').value)==''){document.getElementById('bacarrat_err_message').value='Banker Player Min is required!';document.getElementById('txtBankerPlayerMin').focus(); return false;}else if(validate(document.getElementById('txtBankerPlayerMin').value)==false){document.getElementById('bacarrat_err_message').value='Invalid banker player min!';document.getElementById('txtBankerPlayerMin').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtTieMax').value)==''){document.getElementById('bacarrat_err_message').value='Tie Max is required!';document.getElementById('txtTieMax').focus(); return false;}else if(validate(document.getElementById('txtTieMax').value)==false){document.getElementById('bacarrat_err_message').value='Invalid tie max!';document.getElementById('txtTieMax').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtTieMin').value)==''){document.getElementById('bacarrat_err_message').value='Tie Min is required!';document.getElementById('txtTieMin').focus(); return false;}else if(validate(document.getElementById('txtTieMin').value)==false){document.getElementById('bacarrat_err_message').value='Invalid tie min!';document.getElementById('txtTieMin').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtPairMax').value)==''){document.getElementById('bacarrat_err_message').value='Pair Max is required!';document.getElementById('txtPairMax').focus(); return false;}else if(validate(document.getElementById('txtPairMax').value)==false){document.getElementById('bacarrat_err_message').value='Invalid pair max!';document.getElementById('txtPairMax').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtPairMin').value)==''){document.getElementById('bacarrat_err_message').value='Pair Min is required!';document.getElementById('txtPairMin').focus(); return false;}else if(validate(document.getElementById('txtPairMin').value)==false){document.getElementById('bacarrat_err_message').value='Invalid pair min!';document.getElementById('txtPairMin').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtBigMax').value)==''){document.getElementById('bacarrat_err_message').value='Big Max is required!';document.getElementById('txtBigMax').focus(); return false;}else if(validate(document.getElementById('txtBigMax').value)==false){document.getElementById('bacarrat_err_message').value='Invalid big max!';document.getElementById('txtBigMax').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtBigMin').value)==''){document.getElementById('bacarrat_err_message').value='Big Min is required!';document.getElementById('txtBigMin').focus(); return false;}else if(validate(document.getElementById('txtBigMin').value)==false){document.getElementById('bacarrat_err_message').value='Invalid big min!';document.getElementById('txtBigMin').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtSmallMax').value)==''){document.getElementById('bacarrat_err_message').value='Small Max is required!';document.getElementById('txtSmallMax').focus(); return false;}else if(validate(document.getElementById('txtSmallMax').value)==false){document.getElementById('bacarrat_err_message').value='Invalid small max!';document.getElementById('txtSmallMax').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtSmallMin').value)==''){document.getElementById('bacarrat_err_message').value='Small Min is required!';document.getElementById('txtSmallMin').focus(); return false;}else if(validate(document.getElementById('txtSmallMin').value)==false){document.getElementById('bacarrat_err_message').value='Invalid small min!';document.getElementById('txtSmallMin').focus();return false;}else{document.getElementById('bacarrat_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/CreateTableLimit/CreateBacarratLimit',
		    		method: 'POST',
		    		data: {'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': trim(document.getElementById('txtAllMax').value), 
			    			'all_min': trim(document.getElementById('txtAllMin').value), 
			    			'banker_player_max': trim(document.getElementById('txtBankerPlayerMax').value),
			    			'banker_player_min': trim(document.getElementById('txtBankerPlayerMin').value),
			    			'tie_max': trim(document.getElementById('txtTieMax').value),
			    			'tie_min': trim(document.getElementById('txtTieMin').value),
			    			'pair_max': trim(document.getElementById('txtPairMax').value),
			    			'pair_min': trim(document.getElementById('txtPairMin').value),
			    			'big_max': trim(document.getElementById('txtBigMax').value),
			    			'big_min': trim(document.getElementById('txtBigMin').value),
			    			'small_max': trim(document.getElementById('txtSmallMax').value),
			    			'small_min': trim(document.getElementById('txtSmallMin').value)
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			var addNewRow = {
				    		no: data, 
				    		all: trim(document.getElementById('txtAllMin').value) + ' - ' + trim(document.getElementById('txtAllMax').value), 
				    		banker_player: trim(document.getElementById('txtBankerPlayerMin').value) + ' - ' + trim(document.getElementById('txtBankerPlayerMax').value),
				    		tie: trim(document.getElementById('txtTieMin').value) + ' - ' + trim(document.getElementById('txtTieMax').value), 
				    		pair: trim(document.getElementById('txtPairMin').value) + ' - ' + trim(document.getElementById('txtPairMax').value),
				    		big: trim(document.getElementById('txtBigMin').value) + ' - ' + trim(document.getElementById('txtBigMax').value),
				    		small: trim(document.getElementById('txtSmallMin').value) + ' - ' + trim(document.getElementById('txtSmallMax').value),
				    		setting:"<a href='#' id='" + data + "' onclick='javascript: $updateBacarratLimitDialog.dialog(\"open\");document.getElementById(\"update_bacarrat_err_message\").value=\"\";'>Setting</a>"};
		    			jQuery("#list1").addRowData(data, addNewRow);
		    			$addBacarratLimitDialog.dialog('close');
		    			jQuery("#list1").trigger("reloadGrid");
			    	}
		    	});
			});
			
		});
		function bacarratLimitFieldClear(){
			document.getElementById('txtAllMax').value='';
			document.getElementById('txtAllMin').value='';
			document.getElementById('txtBankerPlayerMax').value='';
			document.getElementById('txtBankerPlayerMin').value='';
			document.getElementById('txtTieMax').value='';
			document.getElementById('txtTieMin').value='';
			document.getElementById('txtPairMax').value='';
			document.getElementById('txtPairMin').value='';
			document.getElementById('txtBigMax').value='';
			document.getElementById('txtBigMin').value='';
			document.getElementById('txtSmallMax').value='';
			document.getElementById('txtSmallMin').value='';
			document.getElementById('bacarrat_err_message').value='';
			document.getElementById('txtAllMax').focus();
		}
		var $updateBacarratLimitDialog='';
		$(document).ready(function() {
		$updateBacarratLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">All Max</td><td class="right"><input type="text"/ id="txtUAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtUAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Banker Player Max</td><td class="right"><input type="text"/ id="txtUBankerPlayerMax"><font color="red"> *</font></td><td class="left">Banker Player Min</td><td class="right"><input type="text"/ id="txtUBankerPlayerMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Tie Max</td><td class="right"><input type="text"/ id="txtUTieMax"><font color="red"> *</font></td><td class="left">Tie Min</td><td class="right"><input type="text"/ id="txtUTieMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Pair Max</td><td class="right"><input type="text"/ id="txtUPairMax"><font color="red"> *</font></td><td class="left">Pair Min</td><td class="right"><input type="text"/ id="txtUPairMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Big Max</td><td class="right"><input type="text"/ id="txtUBigMax"><font color="red"> *</font></td><td class="left">Big Min</td><td class="right"><input type="text"/ id="txtUBigMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Small Max</td><td class="right"><input type="text"/ id="txtUSmallMax"><font color="red"> *</font></td><td class="left">Small Min</td><td class="right"><input type="text"/ id="txtUSmallMin"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-bacarrat-limit1">Update</button>&nbsp;&nbsp;<button class="btn red" onClick="$deleteBacarratLimitDialog.dialog(\'open\')" >Delete</button><br/><input type="text" id="update_bacarrat_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Setting Baccarat Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-bacarrat-limit1').click(function(event) {
				if(trim(document.getElementById('txtUAllMax').value)==''){document.getElementById('update_bacarrat_err_message').value='All Max is required!';document.getElementById('txtUAllMax').focus(); return false;}else if(validate(document.getElementById('txtUAllMax').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid all max!';document.getElementById('txtUAllMax').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUAllMin').value)==''){document.getElementById('update_bacarrat_err_message').value='All Min is required!';document.getElementById('txtUAllMin').focus(); return false;}else if(validate(document.getElementById('txtUAllMin').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid all min!';document.getElementById('txtUAllMin').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUBankerPlayerMax').value)==''){document.getElementById('update_bacarrat_err_message').value='Banker Player Max is required!';document.getElementById('txtUBankerPlayerMax').focus(); return false;}else if(validate(document.getElementById('txtUBankerPlayerMax').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid banker player max!';document.getElementById('txtUBankerPlayerMax').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUBankerPlayerMin').value)==''){document.getElementById('update_bacarrat_err_message').value='Banker Player Min is required!';document.getElementById('txtUBankerPlayerMin').focus(); return false;}else if(validate(document.getElementById('txtUBankerPlayerMin').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid banker player min!';document.getElementById('txtUBankerPlayerMin').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUTieMax').value)==''){document.getElementById('update_bacarrat_err_message').value='Tie Max is required!';document.getElementById('txtUTieMax').focus(); return false;}else if(validate(document.getElementById('txtUTieMax').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid tie max!';document.getElementById('txtUTieMax').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUTieMin').value)==''){document.getElementById('update_bacarrat_err_message').value='Tie Min is required!';document.getElementById('txtUTieMin').focus(); return false;}else if(validate(document.getElementById('txtUTieMin').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid tie min!';document.getElementById('txtUTieMin').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUPairMax').value)==''){document.getElementById('update_bacarrat_err_message').value='Pair Max is required!';document.getElementById('txtUPairMax').focus(); return false;}else if(validate(document.getElementById('txtUPairMax').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid pair max!';document.getElementById('txtUPairMax').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUPairMin').value)==''){document.getElementById('update_bacarrat_err_message').value='Pair Min is required!';document.getElementById('txtUPairMin').focus(); return false;}else if(validate(document.getElementById('txtUPairMin').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid pair min!';document.getElementById('txtUPairMin').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUBigMax').value)==''){document.getElementById('update_bacarrat_err_message').value='Big Max is required!';document.getElementById('txtUBigMax').focus(); return false;}else if(validate(document.getElementById('txtUBigMax').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid big max!';document.getElementById('txtUBigMax').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUBigMin').value)==''){document.getElementById('update_bacarrat_err_message').value='Big Min is required!';document.getElementById('txtUBigMin').focus(); return false;}else if(validate(document.getElementById('txtUBigMin').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid big min!';document.getElementById('txtUBigMin').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUSmallMax').value)==''){document.getElementById('update_bacarrat_err_message').value='Small Max is required!';document.getElementById('txtUSmallMax').focus(); return false;}else if(validate(document.getElementById('txtUSmallMax').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid small max!';document.getElementById('txtUSmallMax').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				if(trim(document.getElementById('txtUSmallMin').value)==''){document.getElementById('update_bacarrat_err_message').value='Small Min is required!';document.getElementById('txtUSmallMin').focus(); return false;}else if(validate(document.getElementById('txtUSmallMin').value)==false){document.getElementById('update_bacarrat_err_message').value='Invalid small min!';document.getElementById('txtUSmallMin').focus();return false;}else{document.getElementById('update_bacarrat_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/UpdateBacarratLimit',
		    		method: 'POST',
		    		data: {'baccarat_limit_id': document.getElementById('txtUAllMax').tag,
			    			'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': trim(document.getElementById('txtUAllMax').value), 
			    			'all_min': trim(document.getElementById('txtUAllMin').value), 
			    			'banker_player_max': trim(document.getElementById('txtUBankerPlayerMax').value),
			    			'banker_player_min': trim(document.getElementById('txtUBankerPlayerMin').value),
			    			'tie_max': trim(document.getElementById('txtUTieMax').value),
			    			'tie_min': trim(document.getElementById('txtUTieMin').value),
			    			'pair_max': trim(document.getElementById('txtUPairMax').value),
			    			'pair_min': trim(document.getElementById('txtUPairMin').value),
			    			'big_max': trim(document.getElementById('txtUBigMax').value),
			    			'big_min': trim(document.getElementById('txtUBigMin').value),
			    			'small_max': trim(document.getElementById('txtUSmallMax').value),
			    			'small_min': trim(document.getElementById('txtUSmallMin').value)
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			jQuery("#list1").jqGrid('setCell', cRowNo, 2, trim(document.getElementById('txtUAllMin').value) + ' - ' + trim(document.getElementById('txtUAllMax').value));
		    			jQuery("#list1").jqGrid('setCell', cRowNo, 3, trim(document.getElementById('txtUBankerPlayerMin').value) + ' - ' + trim(document.getElementById('txtUBankerPlayerMax').value));
		    			jQuery("#list1").jqGrid('setCell', cRowNo, 4, trim(document.getElementById('txtUTieMin').value) + ' - ' + trim(document.getElementById('txtUTieMax').value));
		    			jQuery("#list1").jqGrid('setCell', cRowNo, 5, trim(document.getElementById('txtUPairMin').value) + ' - ' + trim(document.getElementById('txtUPairMax').value));
		    			jQuery("#list1").jqGrid('setCell', cRowNo, 6, trim(document.getElementById('txtUBigMin').value) + ' - ' + trim(document.getElementById('txtUBigMax').value));
		    			jQuery("#list1").jqGrid('setCell', cRowNo, 7, trim(document.getElementById('txtUSmallMin').value) + ' - ' + trim(document.getElementById('txtUSmallMax').value));
		    			$updateBacarratLimitDialog.dialog('close');
		    			jQuery("#list1").trigger("reloadGrid");
			    	}
		    	});
			});

		});
		var $deleteBacarratLimitDialog='';
		$(document).ready(function() {
		$deleteBacarratLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<br/>'
					+'<center>Are you sure you want to delete this setting?</center><br/><br/>'
					+'<div id="table_limit_footer"><button class="btn red" id="delete-bacarrat-limit">Yes</button>&nbsp;&nbsp;<button class="btn red"  onClick="$deleteBacarratLimitDialog.dialog(\'close\')">No</button><br/><input type="text" id="update_american_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 300,
				title: 'Delete',
				resizable: false,
				modal: true
			});
			$('#delete-bacarrat-limit').click(function(event) {
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/DeleteBacarratLimit',
		    		method: 'POST',
		    		data: {'baccarat_limit_id': document.getElementById('txtUAllMax').tag},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			$updateBacarratLimitDialog.dialog('close');
		    			$deleteBacarratLimitDialog.dialog('close');
		    			jQuery("#list1").trigger("reloadGrid");
			    	}
		    	});
			});
			
		});
		//--------------------------------------
		var $addDragonTigerLimitDialog='';
		$(document).ready(function() {
		$addDragonTigerLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">All Max</td><td class="right"><input type="text"/ id="txtDragonTigerAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtDragonTigerAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Dragon Tiger Max</td><td class="right"><input type="text"/ id="txtDragonTigerMax"><font color="red"> *</font></td><td class="left">Dragon Tiger Min</td><td class="right"><input type="text"/ id="txtDragonTigerMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Tie Max</td><td class="right"><input type="text"/ id="txtDragonTigerTieMax"><font color="red"> *</font></td><td class="left">Tie Min</td><td class="right"><input type="text"/ id="txtDragonTigerTieMin"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-dragon-limit">Confirm</button><br/><input type="text" id="dragon_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Add Dragon Tiger Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-dragon-limit').click(function(event) {
				if(trim(document.getElementById('txtDragonTigerAllMax').value)==''){document.getElementById('dragon_err_message').value='All Max is required!';document.getElementById('txtDragonTigerAllMax').focus(); return false;}else if(validate(document.getElementById('txtDragonTigerAllMax').value)==false){document.getElementById('dragon_err_message').value='Invalid all max!';document.getElementById('txtDragonTigerAllMax').focus();return false;}else{document.getElementById('dragon_err_message').value='';}
				if(trim(document.getElementById('txtDragonTigerAllMin').value)==''){document.getElementById('dragon_err_message').value='All Min is required!';document.getElementById('txtDragonTigerAllMin').focus(); return false;}else if(validate(document.getElementById('txtDragonTigerAllMin').value)==false){document.getElementById('dragon_err_message').value='Invalid all min!';document.getElementById('txtDragonTigerAllMin').focus();return false;}else{document.getElementById('dragon_err_message').value='';}
				if(trim(document.getElementById('txtDragonTigerMax').value)==''){document.getElementById('dragon_err_message').value='Dragon Tiger Max is required!';document.getElementById('txtDragonTigerMax').focus(); return false;}else if(validate(document.getElementById('txtDragonTigerMax').value)==false){document.getElementById('dragon_err_message').value='Invalid dragon tiger max!';document.getElementById('txtDragonTigerMax').focus();return false;}else{document.getElementById('dragon_err_message').value='';}
				if(trim(document.getElementById('txtDragonTigerMin').value)==''){document.getElementById('dragon_err_message').value='Dragon Tiger Min is required!';document.getElementById('txtDragonTigerMin').focus(); return false;}else if(validate(document.getElementById('txtDragonTigerMin').value)==false){document.getElementById('dragon_err_message').value='Invalid dragon tiger min!';document.getElementById('txtDragonTigerMin').focus();return false;}else{document.getElementById('dragon_err_message').value='';}
				if(trim(document.getElementById('txtDragonTigerTieMax').value)==''){document.getElementById('dragon_err_message').value='Tie Max is required!';document.getElementById('txtDragonTigerTieMax').focus(); return false;}else if(validate(document.getElementById('txtDragonTigerTieMax').value)==false){document.getElementById('dragon_err_message').value='Invalid tie max!';document.getElementById('txtDragonTigerTieMax').focus();return false;}else{document.getElementById('dragon_err_message').value='';}
				if(trim(document.getElementById('txtDragonTigerTieMin').value)==''){document.getElementById('dragon_err_message').value='Tie Min is required!';document.getElementById('txtDragonTigerTieMin').focus(); return false;}else if(validate(document.getElementById('txtDragonTigerTieMin').value)==false){document.getElementById('dragon_err_message').value='Invalid tie min!';document.getElementById('txtDragonTigerTieMin').focus();return false;}else{document.getElementById('dragon_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/CreateTableLimit/CreateDragonTigerLimit',
		    		method: 'POST',
		    		data: {'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': trim(document.getElementById('txtDragonTigerAllMax').value), 
			    			'all_min': trim(document.getElementById('txtDragonTigerAllMin').value), 
			    			'dragon_tiger_max': trim(document.getElementById('txtDragonTigerMax').value),
			    			'dragon_tiger_min': trim(document.getElementById('txtDragonTigerMin').value),
			    			'tie_max': trim(document.getElementById('txtDragonTigerTieMax').value),
			    			'tie_min': trim(document.getElementById('txtDragonTigerTieMin').value)
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			var addNewRow = {
					    		no: data, 
					    		all: trim(document.getElementById('txtDragonTigerAllMin').value) + ' - ' + trim(document.getElementById('txtDragonTigerAllMax').value), 
					    		dragon_tiger: trim(document.getElementById('txtDragonTigerMin').value) + ' - ' + trim(document.getElementById('txtDragonTigerMax').value),
					    		tie: trim(document.getElementById('txtDragonTigerTieMin').value) + ' - ' + trim(document.getElementById('txtDragonTigerTieMax').value), 
					    		setting:"<a href='#' id='" + data + "' onclick='javascript: $updateDragonTigerLimitDialog.dialog(\"open\");document.getElementById(\"update_dragon_err_message\").value=\"\";'>Setting</a>"};
			    			jQuery("#list2").addRowData(data, addNewRow);
		    			$addDragonTigerLimitDialog.dialog('close');
		    			jQuery("#list2").trigger("reloadGrid");
			    	}
		    	});
			});
			
		});
		function dragonLimitFieldClear(){
			document.getElementById('txtDragonTigerAllMax').value='';
			document.getElementById('txtDragonTigerAllMin').value='';
			document.getElementById('txtDragonTigerMax').value='';
			document.getElementById('txtDragonTigerMin').value='';
			document.getElementById('txtDragonTigerTieMax').value='';
			document.getElementById('txtDragonTigerTieMin').value='';
			document.getElementById('dragon_err_message').value='';
			document.getElementById('txtDragonTigerAllMax').focus();
		}
		var $updateDragonTigerLimitDialog='';
		$(document).ready(function() {
		$updateDragonTigerLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">All Max</td><td class="right"><input type="text"/ id="txtUDragonTigerAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtUDragonTigerAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Dragon Tiger Max</td><td class="right"><input type="text"/ id="txtUDragonTigerMax"><font color="red"> *</font></td><td class="left">Dragon Tiger Min</td><td class="right"><input type="text"/ id="txtUDragonTigerMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Tie Max</td><td class="right"><input type="text"/ id="txtUDragonTigerTieMax"><font color="red"> *</font></td><td class="left">Tie Min</td><td class="right"><input type="text"/ id="txtUDragonTigerTieMin"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-dragon-limit1">Update</button>&nbsp;&nbsp;<button class="btn red" onClick="$deleteDragonTigerLimitDialog.dialog(\'open\')">Delete</button><br/><input type="text" id="update_dragon_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Setting Dragon Tiger Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-dragon-limit1').click(function(event) {
				if(trim(document.getElementById('txtUDragonTigerAllMax').value)==''){document.getElementById('update_dragon_err_message').value='All Max is required!';document.getElementById('txtUDragonTigerAllMax').focus(); return false;}else if(validate(document.getElementById('txtUDragonTigerAllMax').value)==false){document.getElementById('update_dragon_err_message').value='Invalid all max!';document.getElementById('txtUDragonTigerAllMax').focus();return false;}else{document.getElementById('update_dragon_err_message').value='';}
				if(trim(document.getElementById('txtUDragonTigerAllMin').value)==''){document.getElementById('update_dragon_err_message').value='All Min is required!';document.getElementById('txtUDragonTigerAllMin').focus(); return false;}else if(validate(document.getElementById('txtUDragonTigerAllMin').value)==false){document.getElementById('update_dragon_err_message').value='Invalid all min!';document.getElementById('txtUDragonTigerAllMin').focus();return false;}else{document.getElementById('update_dragon_err_message').value='';}
				if(trim(document.getElementById('txtUDragonTigerMax').value)==''){document.getElementById('update_dragon_err_message').value='Dragon Tiger Max is required!';document.getElementById('txtUDragonTigerMax').focus(); return false;}else if(validate(document.getElementById('txtUDragonTigerMax').value)==false){document.getElementById('update_dragon_err_message').value='Invalid dragon tiger max!';document.getElementById('txtUDragonTigerMax').focus();return false;}else{document.getElementById('update_dragon_err_message').value='';}
				if(trim(document.getElementById('txtUDragonTigerMin').value)==''){document.getElementById('update_dragon_err_message').value='Dragon Tiger Min is required!';document.getElementById('txtUDragonTigerMin').focus(); return false;}else if(validate(document.getElementById('txtUDragonTigerMin').value)==false){document.getElementById('update_dragon_err_message').value='Invalid dragon tiger min!';document.getElementById('txtUDragonTigerMin').focus();return false;}else{document.getElementById('update_dragon_err_message').value='';}
				if(trim(document.getElementById('txtUDragonTigerTieMax').value)==''){document.getElementById('update_dragon_err_message').value='Tie Max is required!';document.getElementById('txtUDragonTigerTieMax').focus(); return false;}else if(validate(document.getElementById('txtUDragonTigerTieMax').value)==false){document.getElementById('update_dragon_err_message').value='Invalid tie max!';document.getElementById('txtUDragonTigerTieMax').focus();return false;}else{document.getElementById('update_dragon_err_message').value='';}
				if(trim(document.getElementById('txtUDragonTigerTieMin').value)==''){document.getElementById('update_dragon_err_message').value='Tie Min is required!';document.getElementById('txtUDragonTigerTieMin').focus(); return false;}else if(validate(document.getElementById('txtUDragonTigerTieMin').value)==false){document.getElementById('update_dragon_err_message').value='Invalid tie min!';document.getElementById('txtUDragonTigerTieMin').focus();return false;}else{document.getElementById('update_dragon_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/UpdateDragonTigerLimit',
		    		method: 'POST',
		    		data: {'dragon_limit_id': document.getElementById('txtUDragonTigerAllMax').tag,
			    			'all_max': trim(document.getElementById('txtUDragonTigerAllMax').value), 
			    			'all_min': trim(document.getElementById('txtUDragonTigerAllMin').value), 
			    			'dragon_tiger_max': trim(document.getElementById('txtUDragonTigerMax').value),
			    			'dragon_tiger_min': trim(document.getElementById('txtUDragonTigerMin').value),
			    			'tie_max': trim(document.getElementById('txtUDragonTigerTieMax').value),
			    			'tie_min': trim(document.getElementById('txtUDragonTigerTieMin').value)
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			jQuery("#list2").jqGrid('setCell', cRowNo, 2, trim(document.getElementById('txtUDragonTigerAllMin').value) + ' - ' + trim(document.getElementById('txtUDragonTigerAllMax').value));
		    			jQuery("#list2").jqGrid('setCell', cRowNo, 3, trim(document.getElementById('txtUDragonTigerMin').value) + ' - ' + trim(document.getElementById('txtUDragonTigerMax').value));
		    			jQuery("#list2").jqGrid('setCell', cRowNo, 4, trim(document.getElementById('txtUDragonTigerTieMin').value) + ' - ' + trim(document.getElementById('txtUDragonTigerTieMax').value));
		    			$updateDragonTigerLimitDialog.dialog('close');
		    			jQuery("#list2").trigger("reloadGrid");
			    	}
		    	});
			});	
			
		});
		var $deleteDragonTigerLimitDialog='';
		$(document).ready(function() {
		$deleteDragonTigerLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<br/>'
					+'<center>Are you sure you want to delete this setting?</center><br/><br/>'
					+'<div id="table_limit_footer"><button class="btn red" id="delete-dragon-limit">Yes</button>&nbsp;&nbsp;<button class="btn red"  onClick="$deleteDragonTigerLimitDialog.dialog(\'close\')">No</button><br/><input type="text" id="update_american_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 300,
				title: 'Delete',
				resizable: false,
				modal: true
			});
			$('#delete-dragon-limit').click(function(event) {
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/DeleteDragonTigerLimit',
		    		method: 'POST',
		    		data: {'dragon_limit_id': document.getElementById('txtUDragonTigerAllMax').tag},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			$updateDragonTigerLimitDialog.dialog('close');
		    			$deleteDragonTigerLimitDialog.dialog('close');
		    			jQuery("#list2").trigger("reloadGrid");
		    			
			    	}
		    	});
			});
			
		});
		//--------------------------------------
		var $addRouletteLimitDialog='';
		$(document).ready(function() {
		$addRouletteLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">All Max</td><td class="right"><input type="text"/ id="txtRouletteAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtRouletteAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">35 to 1 Max</td><td class="right"><input type="text"/ id="txt35_1max"><font color="red"> *</font></td><td class="left">35 to 1 Min</td><td class="right"><input type="text"/ id="txt35_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">18 to 1 Max</td><td class="right"><input type="text"/ id="txt18_1max"><font color="red"> *</font></td><td class="left">18 to 1 Min</td><td class="right"><input type="text"/ id="txt18_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">12 to 1 Max</td><td class="right"><input type="text"/ id="txt12_1max"><font color="red"> *</font></td><td class="left">12 to 1 Min</td><td class="right"><input type="text"/ id="txt12_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">9 to 1 Max</td><td class="right"><input type="text"/ id="txt9_1max"><font color="red"> *</font></td><td class="left">9 to 1 Min</td><td class="right"><input type="text"/ id="txt9_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">6 to 1 Max</td><td class="right"><input type="text"/ id="txt6_1max"><font color="red"> *</font></td><td class="left">6 to 1 Min</td><td class="right"><input type="text"/ id="txt6_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">2 to 1 Max</td><td class="right"><input type="text"/ id="txt2_1max"><font color="red"> *</font></td><td class="left">2 to 1 Min</td><td class="right"><input type="text"/ id="txt2_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">1 to 1 Max</td><td class="right"><input type="text"/ id="txt1_1max"><font color="red"> *</font></td><td class="left">1 to 1 Min</td><td class="right"><input type="text"/ id="txt1_1min"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-roulette-limit">Confirm</button><br/><input type="text" id="roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Add European Roulette Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-roulette-limit').click(function(event) {
				if(trim(document.getElementById('txtRouletteAllMax').value)==''){document.getElementById('roulette_err_message').value='All Max is required!';document.getElementById('txtRouletteAllMax').focus(); return false;}else if(validate(document.getElementById('txtRouletteAllMax').value)==false){document.getElementById('roulette_err_message').value='Invalid all max!';document.getElementById('txtRouletteAllMax').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txtRouletteAllMin').value)==''){document.getElementById('roulette_err_message').value='All Min is required!';document.getElementById('txtRouletteAllMin').focus(); return false;}else if(validate(document.getElementById('txtRouletteAllMin').value)==false){document.getElementById('roulette_err_message').value='Invalid all min!';document.getElementById('txtRouletteAllMin').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt35_1max').value)==''){document.getElementById('roulette_err_message').value='35 to 1 Max is required!';document.getElementById('txt35_1max').focus(); return false;}else if(validate(document.getElementById('txt35_1max').value)==false){document.getElementById('roulette_err_message').value='Invalid 35 to 1 max!';document.getElementById('txt35_1max').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt35_1min').value)==''){document.getElementById('roulette_err_message').value='35 to 1 Min is required!';document.getElementById('txt35_1min').focus(); return false;}else if(validate(document.getElementById('txt35_1min').value)==false){document.getElementById('roulette_err_message').value='Invalid 35 to 1 min!';document.getElementById('txt35_1min').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt18_1max').value)==''){document.getElementById('roulette_err_message').value='18 to 1 Max is required!';document.getElementById('txt18_1max').focus(); return false;}else if(validate(document.getElementById('txt18_1max').value)==false){document.getElementById('roulette_err_message').value='Invalid 18 to 1 max!';document.getElementById('txt18_1max').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt18_1min').value)==''){document.getElementById('roulette_err_message').value='18 to 1 Min is required!';document.getElementById('txt18_1min').focus(); return false;}else if(validate(document.getElementById('txt18_1min').value)==false){document.getElementById('roulette_err_message').value='Invalid 18 to 1 min!';document.getElementById('txt18_1min').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt12_1max').value)==''){document.getElementById('roulette_err_message').value='12 to 1 Max is required!';document.getElementById('txt12_1max').focus(); return false;}else if(validate(document.getElementById('txt12_1max').value)==false){document.getElementById('roulette_err_message').value='Invalid 12 to 1 max!';document.getElementById('txt12_1max').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt12_1min').value)==''){document.getElementById('roulette_err_message').value='12 to 1 Min is required!';document.getElementById('txt12_1min').focus(); return false;}else if(validate(document.getElementById('txt12_1min').value)==false){document.getElementById('roulette_err_message').value='Invalid 12 to 1 min!';document.getElementById('txt12_1min').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt9_1max').value)==''){document.getElementById('roulette_err_message').value='9 to 1 Max is required!';document.getElementById('txt9_1max').focus(); return false;}else if(validate(document.getElementById('txt9_1max').value)==false){document.getElementById('roulette_err_message').value='Invalid 9 to 1 max!';document.getElementById('txt9_1max').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt9_1min').value)==''){document.getElementById('roulette_err_message').value='9 to 1 Min is required!';document.getElementById('txt9_1min').focus(); return false;}else if(validate(document.getElementById('txt9_1min').value)==false){document.getElementById('roulette_err_message').value='Invalid 9 to 1 min!';document.getElementById('txt9_1min').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt6_1max').value)==''){document.getElementById('roulette_err_message').value='6 to 1 Max is required!';document.getElementById('txt6_1max').focus(); return false;}else if(validate(document.getElementById('txt6_1max').value)==false){document.getElementById('roulette_err_message').value='Invalid 6 to 1 max!';document.getElementById('txt6_1max').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt6_1min').value)==''){document.getElementById('roulette_err_message').value='6 to 1 Min is required!';document.getElementById('txt6_1min').focus(); return false;}else if(validate(document.getElementById('txt6_1min').value)==false){document.getElementById('roulette_err_message').value='Invalid 6 to 1 min!';document.getElementById('txt6_1min').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt2_1max').value)==''){document.getElementById('roulette_err_message').value='2 to 1 Max is required!';document.getElementById('txt2_1max').focus(); return false;}else if(validate(document.getElementById('txt2_1max').value)==false){document.getElementById('roulette_err_message').value='Invalid 2 to 1 max!';document.getElementById('txt2_1max').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt2_1min').value)==''){document.getElementById('roulette_err_message').value='2 to 1 Min is required!';document.getElementById('txt2_1min').focus(); return false;}else if(validate(document.getElementById('txt2_1min').value)==false){document.getElementById('roulette_err_message').value='Invalid 2 to 1 min!';document.getElementById('txt2_1min').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt1_1max').value)==''){document.getElementById('roulette_err_message').value='1 to 1 Max is required!';document.getElementById('txt1_1max').focus(); return false;}else if(validate(document.getElementById('txt1_1max').value)==false){document.getElementById('roulette_err_message').value='Invalid 1 to 1 max!';document.getElementById('txt1_1max').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				if(trim(document.getElementById('txt1_1min').value)==''){document.getElementById('roulette_err_message').value='1 to 1 Min is required!';document.getElementById('txt1_1min').focus(); return false;}else if(validate(document.getElementById('txt1_1min').value)==false){document.getElementById('roulette_err_message').value='Invalid 1 to 1 min!';document.getElementById('txt1_1min').focus();return false;}else{document.getElementById('roulette_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/CreateTableLimit/CreateRouletteLimit',
		    		method: 'POST',
		    		data: {'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': trim(document.getElementById('txtRouletteAllMax').value), 
			    			'all_min': trim(document.getElementById('txtRouletteAllMin').value), 
			    			'35_to_1_max': trim(document.getElementById('txt35_1max').value),
			    			'35_to_1_min': trim(document.getElementById('txt35_1min').value),
			    			'18_to_1_max': trim(document.getElementById('txt18_1max').value),
			    			'18_to_1_min': trim(document.getElementById('txt18_1min').value),
			    			'12_to_1_max': trim(document.getElementById('txt12_1max').value),
			    			'12_to_1_min': trim(document.getElementById('txt12_1min').value),
			    			'9_to_1_max': trim(document.getElementById('txt9_1max').value),
			    			'9_to_1_min': trim(document.getElementById('txt9_1min').value),
			    			'6_to_1_max': trim(document.getElementById('txt6_1max').value),
			    			'6_to_1_min': trim(document.getElementById('txt6_1min').value),
			    			'2_to_1_max': trim(document.getElementById('txt2_1max').value),
			    			'2_to_1_min': trim(document.getElementById('txt2_1min').value),
			    			'1_to_1_max': trim(document.getElementById('txt1_1max').value),
			    			'1_to_1_min': trim(document.getElementById('txt1_1min').value),
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			var addNewRow = {
					    		no: data, 
					    		all: trim(document.getElementById('txtRouletteAllMin').value) + ' - ' + trim(document.getElementById('txtRouletteAllMax').value), 
					    		r_35_1: trim(document.getElementById('txt35_1min').value) + ' - ' + trim(document.getElementById('txt35_1max').value),
					    		r_18_1: trim(document.getElementById('txt18_1min').value) + ' - ' + trim(document.getElementById('txt18_1max').value), 
					    		r_12_1: trim(document.getElementById('txt12_1min').value) + ' - ' + trim(document.getElementById('txt12_1max').value),
					    		r_9_1: trim(document.getElementById('txt9_1min').value) + ' - ' + trim(document.getElementById('txt9_1max').value),
					    		r_6_1: trim(document.getElementById('txt6_1min').value) + ' - ' + trim(document.getElementById('txt6_1max').value), 
					    		r_2_1: trim(document.getElementById('txt2_1min').value) + ' - ' + trim(document.getElementById('txt2_1max').value),
					    		r_1_1: trim(document.getElementById('txt1_1min').value) + ' - ' + trim(document.getElementById('txt1_1max').value),
					    		setting:"<a href='#' id='" + data + "' onclick='javascript: $updateRouletteLimitDialog.dialog(\"open\");document.getElementById(\"update_roulette_err_message\").value=\"\";'>Setting</a>"};
			    		jQuery("#list3").addRowData(data, addNewRow);
		    			$addRouletteLimitDialog.dialog('close');
		    			jQuery("#list3").trigger("reloadGrid");
		    		  
			    	}
		    	});
			});
			
		});
		function rouletteLimitFieldClear(){
			document.getElementById('txtRouletteAllMax').value='';
			document.getElementById('txtRouletteAllMin').value='';
			document.getElementById('txt35_1max').value='';
			document.getElementById('txt35_1min').value='';
			document.getElementById('txt18_1max').value='';
			document.getElementById('txt18_1min').value='';
			document.getElementById('txt12_1max').value='';
			document.getElementById('txt12_1min').value='';
			document.getElementById('txt9_1max').value='';
			document.getElementById('txt9_1min').value='';
			document.getElementById('txt6_1max').value='';
			document.getElementById('txt6_1min').value='';
			document.getElementById('txt2_1max').value='';
			document.getElementById('txt2_1min').value='';
			document.getElementById('txt1_1max').value='';
			document.getElementById('txt1_1min').value='';
			document.getElementById('roulette_err_message').value='';
			document.getElementById('txtRouletteAllMax').focus();
		}
		var $updateRouletteLimitDialog='';
		$(document).ready(function() {
		$updateRouletteLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">All Max</td><td class="right"><input type="text"/ id="txtURouletteAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtURouletteAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">35 to 1 Max</td><td class="right"><input type="text"/ id="txtU35_1max"><font color="red"> *</font></td><td class="left">35 to 1 Min</td><td class="right"><input type="text"/ id="txtU35_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">18 to 1 Max</td><td class="right"><input type="text"/ id="txtU18_1max"><font color="red"> *</font></td><td class="left">18 to 1 Min</td><td class="right"><input type="text"/ id="txtU18_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">12 to 1 Max</td><td class="right"><input type="text"/ id="txtU12_1max"><font color="red"> *</font></td><td class="left">12 to 1 Min</td><td class="right"><input type="text"/ id="txtU12_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">9 to 1 Max</td><td class="right"><input type="text"/ id="txtU9_1max"><font color="red"> *</font></td><td class="left">9 to 1 Min</td><td class="right"><input type="text"/ id="txtU9_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">6 to 1 Max</td><td class="right"><input type="text"/ id="txtU6_1max"><font color="red"> *</font></td><td class="left">6 to 1 Min</td><td class="right"><input type="text"/ id="txtU6_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">2 to 1 Max</td><td class="right"><input type="text"/ id="txtU2_1max"><font color="red"> *</font></td><td class="left">2 to 1 Min</td><td class="right"><input type="text"/ id="txtU2_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">1 to 1 Max</td><td class="right"><input type="text"/ id="txtU1_1max"><font color="red"> *</font></td><td class="left">1 to 1 Min</td><td class="right"><input type="text"/ id="txtU1_1min"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-roulette-limit1" >Update</button>&nbsp;&nbsp;<button class="btn red" onClick="$deleteRouletteLimitDialog.dialog(\'open\')" >Delete</button><br/><input type="text" id="update_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Setting European Roulette Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-roulette-limit1').click(function(event) {
				if(trim(document.getElementById('txtURouletteAllMax').value)==''){document.getElementById('update_roulette_err_message').value='All Max is required!';document.getElementById('txtURouletteAllMax').focus(); return false;}else if(validate(document.getElementById('txtURouletteAllMax').value)==false){document.getElementById('update_roulette_err_message').value='Invalid all max!';document.getElementById('txtURouletteAllMax').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtURouletteAllMin').value)==''){document.getElementById('update_roulette_err_message').value='All Min is required!';document.getElementById('txtURouletteAllMin').focus(); return false;}else if(validate(document.getElementById('txtURouletteAllMin').value)==false){document.getElementById('update_roulette_err_message').value='Invalid all min!';document.getElementById('txtURouletteAllMin').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU35_1max').value)==''){document.getElementById('update_roulette_err_message').value='35 to 1 Max is required!';document.getElementById('txtU35_1max').focus(); return false;}else if(validate(document.getElementById('txtU35_1max').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 35 to 1 max!';document.getElementById('txtU35_1max').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU35_1min').value)==''){document.getElementById('update_roulette_err_message').value='35 to 1 Min is required!';document.getElementById('txtU35_1min').focus(); return false;}else if(validate(document.getElementById('txtU35_1min').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 35 to 1 min!';document.getElementById('txtU35_1min').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU18_1max').value)==''){document.getElementById('update_roulette_err_message').value='18 to 1 Max is required!';document.getElementById('txtU18_1max').focus(); return false;}else if(validate(document.getElementById('txtU18_1max').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 18 to 1 max!';document.getElementById('txtU18_1max').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU18_1min').value)==''){document.getElementById('update_roulette_err_message').value='18 to 1 Min is required!';document.getElementById('txtU18_1min').focus(); return false;}else if(validate(document.getElementById('txtU18_1min').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 18 to 1 min!';document.getElementById('txtU18_1min').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU12_1max').value)==''){document.getElementById('update_roulette_err_message').value='12 to 1 Max is required!';document.getElementById('txtU12_1max').focus(); return false;}else if(validate(document.getElementById('txtU12_1max').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 12 to 1 max!';document.getElementById('txtU12_1max').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU12_1min').value)==''){document.getElementById('update_roulette_err_message').value='12 to 1 Min is required!';document.getElementById('txtU12_1min').focus(); return false;}else if(validate(document.getElementById('txtU12_1min').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 12 to 1 min!';document.getElementById('txtU12_1min').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU9_1max').value)==''){document.getElementById('update_roulette_err_message').value='9 to 1 Max is required!';document.getElementById('txtU9_1max').focus(); return false;}else if(validate(document.getElementById('txtU9_1max').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 9 to 1 max!';document.getElementById('txtU9_1max').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU9_1min').value)==''){document.getElementById('update_roulette_err_message').value='9 to 1 Min is required!';document.getElementById('txtU9_1min').focus(); return false;}else if(validate(document.getElementById('txtU9_1min').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 9 to 1 min!';document.getElementById('txtU9_1min').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU6_1max').value)==''){document.getElementById('update_roulette_err_message').value='6 to 1 Max is required!';document.getElementById('txtU6_1max').focus(); return false;}else if(validate(document.getElementById('txtU6_1max').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 6 to 1 max!';document.getElementById('txtU6_1max').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU6_1min').value)==''){document.getElementById('update_roulette_err_message').value='6 to 1 Min is required!';document.getElementById('txtU6_1min').focus(); return false;}else if(validate(document.getElementById('txtU6_1min').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 6 to 1 min!';document.getElementById('txtU6_1min').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU2_1max').value)==''){document.getElementById('update_roulette_err_message').value='2 to 1 Max is required!';document.getElementById('txtU2_1max').focus(); return false;}else if(validate(document.getElementById('txtU2_1max').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 2 to 1 max!';document.getElementById('txtU2_1max').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU2_1min').value)==''){document.getElementById('update_roulette_err_message').value='2 to 1 Min is required!';document.getElementById('txtU2_1min').focus(); return false;}else if(validate(document.getElementById('txtU2_1min').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 2 to 1 min!';document.getElementById('txtU2_1min').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU1_1max').value)==''){document.getElementById('update_roulette_err_message').value='1 to 1 Max is required!';document.getElementById('txtU1_1max').focus(); return false;}else if(validate(document.getElementById('txtU1_1max').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 1 to 1 max!';document.getElementById('txtU1_1max').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				if(trim(document.getElementById('txtU1_1min').value)==''){document.getElementById('update_roulette_err_message').value='1 to 1 Min is required!';document.getElementById('txtU1_1min').focus(); return false;}else if(validate(document.getElementById('txtU1_1min').value)==false){document.getElementById('update_roulette_err_message').value='Invalid 1 to 1 min!';document.getElementById('txtU1_1min').focus();return false;}else{document.getElementById('update_roulette_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/UpdateAmericanRouletteLimit',
		    		method: 'POST',
		    		data: {	'roulette_limit_id': document.getElementById('txtURouletteAllMax').tag,
			    			'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': trim(document.getElementById('txtURouletteAllMax').value), 
			    			'all_min': trim(document.getElementById('txtURouletteAllMin').value), 
			    			'35_to_1_max': trim(document.getElementById('txtU35_1max').value),
			    			'35_to_1_min': trim(document.getElementById('txtU35_1min').value),
			    			'18_to_1_max': trim(document.getElementById('txtU18_1max').value),
			    			'18_to_1_min': trim(document.getElementById('txtU18_1min').value),
			    			'12_to_1_max': trim(document.getElementById('txtU12_1max').value),
			    			'12_to_1_min': trim(document.getElementById('txtU12_1min').value),
			    			'9_to_1_max': trim(document.getElementById('txtU9_1max').value),
			    			'9_to_1_min': trim(document.getElementById('txtU9_1min').value),
			    			'6_to_1_max': trim(document.getElementById('txtU6_1max').value),
			    			'6_to_1_min': trim(document.getElementById('txtU6_1min').value),
			    			'2_to_1_max': trim(document.getElementById('txtU2_1max').value),
			    			'2_to_1_min': trim(document.getElementById('txtU2_1min').value),
			    			'1_to_1_max': trim(document.getElementById('txtU1_1max').value),
			    			'1_to_1_min': trim(document.getElementById('txtU1_1min').value),
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 2, trim(document.getElementById('txtURouletteAllMin').value) + ' - ' + trim(document.getElementById('txtURouletteAllMax').value));
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 3, trim(document.getElementById('txtU35_1min').value) + ' - ' + trim(document.getElementById('txtU35_1max').value));
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 4, trim(document.getElementById('txtU18_1min').value) + ' - ' + trim(document.getElementById('txtU18_1max').value));
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 5, trim(document.getElementById('txtU12_1min').value) + ' - ' + trim(document.getElementById('txtU12_1max').value));
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 6, trim(document.getElementById('txtU9_1min').value) + ' - ' + trim(document.getElementById('txtU9_1max').value));
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 7, trim(document.getElementById('txtU6_1min').value) + ' - ' + trim(document.getElementById('txtU6_1max').value));
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 8, trim(document.getElementById('txtU2_1min').value) + ' - ' + trim(document.getElementById('txtU2_1max').value));
		    			jQuery("#list3").jqGrid('setCell', cRowNo, 9, trim(document.getElementById('txtU1_1min').value) + ' - ' + trim(document.getElementById('txtU1_1max').value));
		    			$updateRouletteLimitDialog.dialog('close');
		    			jQuery("#list3").trigger("reloadGrid");
			    	}
		    	});
			});

		});
		var $deleteRouletteLimitDialog='';
		$(document).ready(function() {
		$deleteRouletteLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<br/>'
					+'<center>Are you sure you want to delete this setting?</center><br/><br/>'
					+'<div id="table_limit_footer"><button class="btn red" id="delete-roulette-limit">Yes</button>&nbsp;&nbsp;<button class="btn red"  onClick="$deleteRouletteLimitDialog.dialog(\'close\')">No</button><br/><input type="text" id="update_american_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 300,
				title: 'Delete',
				resizable: false,
				modal: true
			});
			$('#delete-roulette-limit').click(function(event) {
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/DeleteRouletteLimit',
		    		method: 'POST',
		    		data: {'roulette_limit_id': document.getElementById('txtURouletteAllMax').tag},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			$updateRouletteLimitDialog.dialog('close');
		    			$deleteRouletteLimitDialog.dialog('close');
		    			jQuery("#list3").trigger("reloadGrid");
			    	}
		    	});
			});
			
		});
		
		//--------------------------------------
		var $addBlackjackLimitDialog='';
		$(document).ready(function() {
		$addBlackjackLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">Main Max</td><td class="right"><input type="text"/ id="txtBlackjackAllMax"><font color="red"> *</font></td><td class="left">Main Min</td><td class="right"><input type="text"/ id="txtBlackjackAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Pair Max</td><td class="right"><input type="text"/ id="txtBlkPairMax"><font color="red"> *</font></td><td class="left">Pair Min</td><td class="right"><input type="text"/ id="txtBlkPairMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Rummy Max</td><td class="right"><input type="text"/ id="txtRummyMax"><font color="red"> *</font></td><td class="left">Rummy Min</td><td class="right"><input type="text"/ id="txtRummyMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Insurance Max</td><td class="right"><input type="text"/ id="txtInsuranceMax"><font color="red"> *</font></td><td class="left">Insurance Min</td><td class="right"><input type="text"/ id="txtInsuranceMin"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-blackjack-limit" >Confirm</button><br/><input type="text" id="blackjack_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Add Blackjack Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-blackjack-limit').click(function(event) {
				if(trim(document.getElementById('txtBlackjackAllMax').value)==''){document.getElementById('blackjack_err_message').value='All Max is required!';document.getElementById('txtBlackjackAllMax').focus(); return false;}else if(validate(document.getElementById('txtBlackjackAllMax').value)==false){document.getElementById('blackjack_err_message').value='Invalid all max!';document.getElementById('txtBlackjackAllMax').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				if(trim(document.getElementById('txtBlackjackAllMin').value)==''){document.getElementById('blackjack_err_message').value='All Min is required!';document.getElementById('txtBlackjackAllMin').focus(); return false;}else if(validate(document.getElementById('txtBlackjackAllMin').value)==false){document.getElementById('blackjack_err_message').value='Invalid all min!';document.getElementById('txtBlackjackAllMin').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				if(trim(document.getElementById('txtBlkPairMax').value)==''){document.getElementById('blackjack_err_message').value='Pair Max is required!';document.getElementById('txtBlkPairMax').focus(); return false;}else if(validate(document.getElementById('txtBlkPairMax').value)==false){document.getElementById('blackjack_err_message').value='Invalid Pair Max!';document.getElementById('txtBlkPairMax').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				if(trim(document.getElementById('txtBlkPairMin').value)==''){document.getElementById('blackjack_err_message').value='Pair Min is required!';document.getElementById('txtBlkPairMin').focus(); return false;}else if(validate(document.getElementById('txtBlkPairMin').value)==false){document.getElementById('blackjack_err_message').value='Invalid Pair Min!';document.getElementById('txtBlkPairMin').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				if(trim(document.getElementById('txtRummyMax').value)==''){document.getElementById('blackjack_err_message').value='Rummy Max is required!';document.getElementById('txtRummyMax').focus(); return false;}else if(validate(document.getElementById('txtRummyMax').value)==false){document.getElementById('blackjack_err_message').value='Invalid Rummy max!';document.getElementById('txtRummyMax').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				if(trim(document.getElementById('txtRummyMin').value)==''){document.getElementById('blackjack_err_message').value='Rummy Min is required!';document.getElementById('txtRummyMin').focus(); return false;}else if(validate(document.getElementById('txtRummyMin').value)==false){document.getElementById('blackjack_err_message').value='Invalid Rummy min!';document.getElementById('txtRummyMin').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				if(trim(document.getElementById('txtInsuranceMax').value)==''){document.getElementById('blackjack_err_message').value='Insurance Max is required!';document.getElementById('txtInsuranceMax').focus(); return false;}else if(validate(document.getElementById('txtInsuranceMax').value)==false){document.getElementById('blackjack_err_message').value='Invalid Insurance Max!';document.getElementById('txtInsuranceMax').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				if(trim(document.getElementById('txtInsuranceMin').value)==''){document.getElementById('blackjack_err_message').value='Insurance Min is required!';document.getElementById('txtInsuranceMin').focus(); return false;}else if(validate(document.getElementById('txtInsuranceMin').value)==false){document.getElementById('blackjack_err_message').value='Invalid Insurance Min!';document.getElementById('txtInsuranceMin').focus();return false;}else{document.getElementById('blackjack_err_message').value='';}
				
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/CreateTableLimit/CreateBlackjackLimit',
		    		method: 'POST',
		    		data: {'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': trim(document.getElementById('txtBlackjackAllMax').value), 
			    			'all_min': trim(document.getElementById('txtBlackjackAllMin').value), 
			    			'pair_max': trim(document.getElementById('txtBlkPairMax').value),
			    			'pair_min': trim(document.getElementById('txtBlkPairMin').value),
			    			'rummy_max': trim(document.getElementById('txtRummyMax').value),
			    			'rummy_min': trim(document.getElementById('txtRummyMin').value),
			    			'insurance_max': trim(document.getElementById('txtInsuranceMax').value),
			    			'insurance_min': trim(document.getElementById('txtInsuranceMin').value),
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			var addNewRow = {
					    		no: data, 
					    		all: trim(document.getElementById('txtBlackjackAllMin').value) + ' - ' + trim(document.getElementById('txtBlackjackAllMax').value), 
					    		pair: trim(document.getElementById('txtBlkPairMin').value) + ' - ' + trim(document.getElementById('txtBlkPairMax').value),
					    		rummy: trim(document.getElementById('txtRummyMin').value) + ' - ' + trim(document.getElementById('txtRummyMax').value), 
					    		insurance: trim(document.getElementById('txtInsuranceMin').value) + ' - ' + trim(document.getElementById('txtInsuranceMax').value),
					    		setting:"<a href='#' id='" + data + "' onclick='javascript: $updateBlackjackLimitDialog.dialog(\"open\");document.getElementById(\"update_blackjack_err_message\").value=\"\";'>Setting</a>"};
			    		jQuery("#list5").addRowData(data, addNewRow);
		    			$addBlackjackLimitDialog.dialog('close');
		    			jQuery("#list5").trigger("reloadGrid");
		    		  
			    	}
		    	});
			});
			
		});
		function blackjackLimitFieldClear(){
			document.getElementById('txtBlackjackAllMax').value='';
			document.getElementById('txtBlackjackAllMin').value='';
			document.getElementById('txtBlkPairMax').value='';
			document.getElementById('txtBlkPairMin').value='';
			document.getElementById('txtRummyMax').value='';
			document.getElementById('txtRummyMin').value='';
			document.getElementById('txtInsuranceMax').value='';
			document.getElementById('txtInsuranceMin').value='';
			document.getElementById('blackjack_err_message').value='';
			document.getElementById('txtBlackjackAllMax').focus();
		}
		var $updateBlackjackLimitDialog='';
		$(document).ready(function() {
		$updateBlackjackLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					+'<tr><td class="left">Main Max</td><td class="right"><input type="text"/ id="txtUBlackjackAllMax"><font color="red"> *</font></td><td class="left">Main Min</td><td class="right"><input type="text"/ id="txtUBlackjackAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Pair Max</td><td class="right"><input type="text"/ id="txtUBlkPairMax"><font color="red"> *</font></td><td class="left">Pair Min</td><td class="right"><input type="text"/ id="txtUBlkPairMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Rummy Max</td><td class="right"><input type="text"/ id="txtURummyMax"><font color="red"> *</font></td><td class="left">Rummy Min</td><td class="right"><input type="text"/ id="txtURummyMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">Insurance Max</td><td class="right"><input type="text"/ id="txtUInsuranceMax"><font color="red"> *</font></td><td class="left">Insurance Min</td><td class="right"><input type="text"/ id="txtUInsuranceMin"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-update-blackjack-limit1" >Update</button>&nbsp;&nbsp;<button class="btn red" onClick="$deleteBlackjackLimitDialog.dialog(\'open\')" >Delete</button><br/><input type="text" id="update_blackjack_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Setting Blackjack Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-update-blackjack-limit1').click(function(event) {
				if(trim(document.getElementById('txtUBlackjackAllMax').value)==''){document.getElementById('update_blackjack_err_message').value='All Max is required!';document.getElementById('txtUBlackjackAllMax').focus(); return false;}else if(validate(document.getElementById('txtUBlackjackAllMax').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid all max!';document.getElementById('txtUBlackjackAllMax').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				if(trim(document.getElementById('txtUBlackjackAllMin').value)==''){document.getElementById('update_blackjack_err_message').value='All Min is required!';document.getElementById('txtUBlackjackAllMin').focus(); return false;}else if(validate(document.getElementById('txtUBlackjackAllMin').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid all min!';document.getElementById('txtUBlackjackAllMin').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				if(trim(document.getElementById('txtUBlkPairMax').value)==''){document.getElementById('update_blackjack_err_message').value='Pair Max is required!';document.getElementById('txtUBlkPairMax').focus(); return false;}else if(validate(document.getElementById('txtUBlkPairMax').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid Pair Max!';document.getElementById('txtUBlkPairMax').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				if(trim(document.getElementById('txtUBlkPairMin').value)==''){document.getElementById('update_blackjack_err_message').value='Pair Min is required!';document.getElementById('txtUBlkPairMin').focus(); return false;}else if(validate(document.getElementById('txtUBlkPairMin').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid Pair Min!';document.getElementById('txtUBlkPairMin').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				if(trim(document.getElementById('txtURummyMax').value)==''){document.getElementById('update_blackjack_err_message').value='Rummy Max is required!';document.getElementById('txtURummyMax').focus(); return false;}else if(validate(document.getElementById('txtURummyMax').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid Rummy max!';document.getElementById('txtURummyMax').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				if(trim(document.getElementById('txtURummyMin').value)==''){document.getElementById('update_blackjack_err_message').value='Rummy Min is required!';document.getElementById('txtURummyMin').focus(); return false;}else if(validate(document.getElementById('txtURummyMin').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid Rummy min!';document.getElementById('txtURummyMin').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				if(trim(document.getElementById('txtUInsuranceMax').value)==''){document.getElementById('update_blackjack_err_message').value='Insurance Max is required!';document.getElementById('txtUInsuranceMax').focus(); return false;}else if(validate(document.getElementById('txtUInsuranceMax').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid Insurance Max!';document.getElementById('txtUInsuranceMax').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				if(trim(document.getElementById('txtUInsuranceMin').value)==''){document.getElementById('update_blackjack_err_message').value='Insurance Min is required!';document.getElementById('txtUInsuranceMin').focus(); return false;}else if(validate(document.getElementById('txtUInsuranceMin').value)==false){document.getElementById('update_blackjack_err_message').value='Invalid Insurance Min!';document.getElementById('txtUInsuranceMin').focus();return false;}else{document.getElementById('update_blackjack_err_message').value='';}
				
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/UpdateBlackjackLimit',
		    		method: 'POST',
		    		data: {	'blackjack_limit_id': document.getElementById('txtUBlackjackAllMax').tag,
			    			'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': trim(document.getElementById('txtUBlackjackAllMax').value), 
			    			'all_min': trim(document.getElementById('txtUBlackjackAllMin').value), 
			    			'pair_max': trim(document.getElementById('txtUBlkPairMax').value),
			    			'pair_min': trim(document.getElementById('txtUBlkPairMin').value),
			    			'rummy_max': trim(document.getElementById('txtURummyMax').value),
			    			'rummy_min': trim(document.getElementById('txtURummyMin').value),
			    			'insurance_max': trim(document.getElementById('txtUInsuranceMax').value),
			    			'insurance_min': trim(document.getElementById('txtUInsuranceMin').value),
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			jQuery("#list5").jqGrid('setCell', cRowNo, 2, trim(document.getElementById('txtUBlackjackAllMin').value) + ' - ' + trim(document.getElementById('txtUBlackjackAllMax').value));
		    			jQuery("#list5").jqGrid('setCell', cRowNo, 3, trim(document.getElementById('txtUBlkPairMin').value) + ' - ' + trim(document.getElementById('txtUBlkPairMax').value));
		    			jQuery("#list5").jqGrid('setCell', cRowNo, 4, trim(document.getElementById('txtURummyMin').value) + ' - ' + trim(document.getElementById('txtURummyMax').value));
		    			jQuery("#list5").jqGrid('setCell', cRowNo, 5, trim(document.getElementById('txtUInsuranceMin').value) + ' - ' + trim(document.getElementById('txtUInsuranceMax').value));		    			
		    			$updateBlackjackLimitDialog.dialog('close');
		    			jQuery("#list5").trigger("reloadGrid");
			    	}
		    	});
			});

			
		});

		var $deleteBlackjackLimitDialog='';
		$(document).ready(function() {
		$deleteBlackjackLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<br/>'
					+'<center>Are you sure you want to delete this setting?</center><br/><br/>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-delete-blackjack-limit" >Yes</button>&nbsp;&nbsp;<button class="btn red"  onClick="$deleteBlackjackLimitDialog.dialog(\'close\')">No</button><br/><input type="text" id="update_american_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 300,
				title: 'Delete',
				resizable: false,
				modal: true
			});
			$('#confirm-delete-blackjack-limit').click(function(event) {
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/DeleteBlackjackLimit',
		    		method: 'POST',
		    		data: {'blackjack_limit_id': document.getElementById('txtUBlackjackAllMax').tag},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			$updateBlackjackLimitDialog.dialog('close');
		    			$deleteBlackjackLimitDialog.dialog('close');
		    			jQuery("#list5").trigger("reloadGrid");
			    	}
		    	});
			});
			
		});
		
		//--------------------------------------
		var $addAmericanRouletteLimitDialog='';
		$(document).ready(function() {
		$addAmericanRouletteLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					//+'<tr><td class="left">All Max</td><td class="right"><input type="text"/ id="txtAmericanRouletteAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtAmericanRouletteAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">35 to 1 Max</td><td class="right"><input type="text"/ id="txtAR35_1max"><font color="red"> *</font></td><td class="left">35 to 1 Min</td><td class="right"><input type="text"/ id="txtAR35_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">17 to 1 Max</td><td class="right"><input type="text"/ id="txtAR18_1max"><font color="red"> *</font></td><td class="left">17 to 1 Min</td><td class="right"><input type="text"/ id="txtAR18_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">11 to 1 Max</td><td class="right"><input type="text"/ id="txtAR12_1max"><font color="red"> *</font></td><td class="left">11 to 1 Min</td><td class="right"><input type="text"/ id="txtAR12_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">8 to 1 Max</td><td class="right"><input type="text"/ id="txtAR9_1max"><font color="red"> *</font></td><td class="left">8 to 1 Min</td><td class="right"><input type="text"/ id="txtAR9_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">5 to 1 Max</td><td class="right"><input type="text"/ id="txtAR6_1max"><font color="red"> *</font></td><td class="left">5 to 1 Min</td><td class="right"><input type="text"/ id="txtAR6_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">2 to 1 Max</td><td class="right"><input type="text"/ id="txtAR2_1max"><font color="red"> *</font></td><td class="left">2 to 1 Min</td><td class="right"><input type="text"/ id="txtAR2_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">1 to 1 Max</td><td class="right"><input type="text"/ id="txtAR1_1max"><font color="red"> *</font></td><td class="left">1 to 1 Min</td><td class="right"><input type="text"/ id="txtAR1_1min"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-american-roulette-limit" >Confirm</button><br/><input type="text" id="american_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Add American Roulette Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-american-roulette-limit').click(function(event) {
				//if(trim(document.getElementById('txtAmericanRouletteAllMax').value)==''){document.getElementById('american_roulette_err_message').value='All Max is required!';document.getElementById('txtAmericanRouletteAllMax').focus(); return false;}else if(validate(document.getElementById('txtAmericanRouletteAllMax').value)==false){document.getElementById('american_roulette_err_message').value='Invalid all max!';document.getElementById('txtAmericanRouletteAllMax').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				//if(trim(document.getElementById('txtAmericanRouletteAllMin').value)==''){document.getElementById('american_roulette_err_message').value='All Min is required!';document.getElementById('txtAmericanRouletteAllMin').focus(); return false;}else if(validate(document.getElementById('txtAmericanRouletteAllMin').value)==false){document.getElementById('american_roulette_err_message').value='Invalid all min!';document.getElementById('txtAmericanRouletteAllMin').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR35_1max').value)==''){document.getElementById('american_roulette_err_message').value='35 to 1 Max is required!';document.getElementById('txtAR35_1max').focus(); return false;}else if(validate(document.getElementById('txtAR35_1max').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 35 to 1 max!';document.getElementById('txtAR35_1max').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR35_1min').value)==''){document.getElementById('american_roulette_err_message').value='35 to 1 Min is required!';document.getElementById('txtAR35_1min').focus(); return false;}else if(validate(document.getElementById('txtAR35_1min').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 35 to 1 min!';document.getElementById('txtAR35_1min').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR18_1max').value)==''){document.getElementById('american_roulette_err_message').value='17 to 1 Max is required!';document.getElementById('txtAR18_1max').focus(); return false;}else if(validate(document.getElementById('txtAR18_1max').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 17 to 1 max!';document.getElementById('txtAR18_1max').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR18_1min').value)==''){document.getElementById('american_roulette_err_message').value='17 to 1 Min is required!';document.getElementById('txtAR18_1min').focus(); return false;}else if(validate(document.getElementById('txtAR18_1min').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 17 to 1 min!';document.getElementById('txtAR18_1min').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR12_1max').value)==''){document.getElementById('american_roulette_err_message').value='11 to 1 Max is required!';document.getElementById('txtAR12_1max').focus(); return false;}else if(validate(document.getElementById('txtAR12_1max').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 11 to 1 max!';document.getElementById('txtAR12_1max').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR12_1min').value)==''){document.getElementById('american_roulette_err_message').value='11 to 1 Min is required!';document.getElementById('txtAR12_1min').focus(); return false;}else if(validate(document.getElementById('txtAR12_1min').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 11 to 1 min!';document.getElementById('txtAR12_1min').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR9_1max').value)==''){document.getElementById('american_roulette_err_message').value='8 to 1 Max is required!';document.getElementById('txtAR9_1max').focus(); return false;}else if(validate(document.getElementById('txtAR9_1max').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 8 to 1 max!';document.getElementById('txtAR9_1max').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR9_1min').value)==''){document.getElementById('american_roulette_err_message').value='8 to 1 Min is required!';document.getElementById('txtAR9_1min').focus(); return false;}else if(validate(document.getElementById('txtAR9_1min').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 8 to 1 min!';document.getElementById('txtAR9_1min').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR6_1max').value)==''){document.getElementById('american_roulette_err_message').value='5 to 1 Max is required!';document.getElementById('txtAR6_1max').focus(); return false;}else if(validate(document.getElementById('txtAR6_1max').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 5 to 1 max!';document.getElementById('txtAR6_1max').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR6_1min').value)==''){document.getElementById('american_roulette_err_message').value='5 to 1 Min is required!';document.getElementById('txtAR6_1min').focus(); return false;}else if(validate(document.getElementById('txtAR6_1min').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 5 to 1 min!';document.getElementById('txtAR6_1min').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR2_1max').value)==''){document.getElementById('american_roulette_err_message').value='2 to 1 Max is required!';document.getElementById('txtAR2_1max').focus(); return false;}else if(validate(document.getElementById('txtAR2_1max').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 2 to 1 max!';document.getElementById('txtAR2_1max').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR2_1min').value)==''){document.getElementById('american_roulette_err_message').value='2 to 1 Min is required!';document.getElementById('txtAR2_1min').focus(); return false;}else if(validate(document.getElementById('txtAR2_1min').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 2 to 1 min!';document.getElementById('txtAR2_1min').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR1_1max').value)==''){document.getElementById('american_roulette_err_message').value='1 to 1 Max is required!';document.getElementById('txtAR1_1max').focus(); return false;}else if(validate(document.getElementById('txtAR1_1max').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 1 to 1 max!';document.getElementById('txtAR1_1max').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtAR1_1min').value)==''){document.getElementById('american_roulette_err_message').value='1 to 1 Min is required!';document.getElementById('txtAR1_1min').focus(); return false;}else if(validate(document.getElementById('txtAR1_1min').value)==false){document.getElementById('american_roulette_err_message').value='Invalid 1 to 1 min!';document.getElementById('txtAR1_1min').focus();return false;}else{document.getElementById('american_roulette_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/CreateTableLimit/CreateAmericanRouletteLimit',
		    		method: 'POST',
		    		data: {'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': 0,//trim(document.getElementById('txtAmericanRouletteAllMax').value), 
			    			'all_min': 0,//trim(document.getElementById('txtAmericanRouletteAllMin').value), 
			    			'35_to_1_max': trim(document.getElementById('txtAR35_1max').value),
			    			'35_to_1_min': trim(document.getElementById('txtAR35_1min').value),
			    			'18_to_1_max': trim(document.getElementById('txtAR18_1max').value),
			    			'18_to_1_min': trim(document.getElementById('txtAR18_1min').value),
			    			'12_to_1_max': trim(document.getElementById('txtAR12_1max').value),
			    			'12_to_1_min': trim(document.getElementById('txtAR12_1min').value),
			    			'9_to_1_max': trim(document.getElementById('txtAR9_1max').value),
			    			'9_to_1_min': trim(document.getElementById('txtAR9_1min').value),
			    			'6_to_1_max': trim(document.getElementById('txtAR6_1max').value),
			    			'6_to_1_min': trim(document.getElementById('txtAR6_1min').value),
			    			'2_to_1_max': trim(document.getElementById('txtAR2_1max').value),
			    			'2_to_1_min': trim(document.getElementById('txtAR2_1min').value),
			    			'1_to_1_max': trim(document.getElementById('txtAR1_1max').value),
			    			'1_to_1_min': trim(document.getElementById('txtAR1_1min').value),
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			var addNewRow = {
					    		no: data, 
					    		//all: trim(document.getElementById('txtAmericanRouletteAllMin').value) + ' - ' + trim(document.getElementById('txtAmericanRouletteAllMax').value), 
					    		r_35_1: trim(document.getElementById('txtAR35_1min').value) + ' - ' + trim(document.getElementById('txtAR35_1max').value),
					    		r_18_1: trim(document.getElementById('txtAR18_1min').value) + ' - ' + trim(document.getElementById('txtAR18_1max').value), 
					    		r_12_1: trim(document.getElementById('txtAR12_1min').value) + ' - ' + trim(document.getElementById('txtAR12_1max').value),
					    		r_9_1: trim(document.getElementById('txtAR9_1min').value) + ' - ' + trim(document.getElementById('txtAR9_1max').value),
					    		r_6_1: trim(document.getElementById('txtAR6_1min').value) + ' - ' + trim(document.getElementById('txtAR6_1max').value), 
					    		r_2_1: trim(document.getElementById('txtAR2_1min').value) + ' - ' + trim(document.getElementById('txtAR2_1max').value),
					    		r_1_1: trim(document.getElementById('txtAR1_1min').value) + ' - ' + trim(document.getElementById('txtAR1_1max').value),
					    		setting:"<a href='#' id='" + data + "' onclick='javascript: $updateAmericanRouletteLimitDialog.dialog(\"open\");document.getElementById(\"update_american_roulette_err_message\").value=\"\";'>Setting</a>"};
			    		jQuery("#list6").addRowData(data, addNewRow);
		    			$addAmericanRouletteLimitDialog.dialog('close');
		    			jQuery("#list6").trigger("reloadGrid");
		    		  
			    	}
		    	});
			});
			
		});
		function americanRouletteLimitFieldClear(){
			//document.getElementById('txtAmericanRouletteAllMax').value='';
			//document.getElementById('txtAmericanRouletteAllMin').value='';
			document.getElementById('txtAR35_1max').value='';
			document.getElementById('txtAR35_1min').value='';
			document.getElementById('txtAR18_1max').value='';
			document.getElementById('txtAR18_1min').value='';
			document.getElementById('txtAR12_1max').value='';
			document.getElementById('txtAR12_1min').value='';
			document.getElementById('txtAR9_1max').value='';
			document.getElementById('txtAR9_1min').value='';
			document.getElementById('txtAR6_1max').value='';
			document.getElementById('txtAR6_1min').value='';
			document.getElementById('txtAR2_1max').value='';
			document.getElementById('txtAR2_1min').value='';
			document.getElementById('txtAR1_1max').value='';
			document.getElementById('txtAR1_1min').value='';
			document.getElementById('american_roulette_err_message').value='';
			document.getElementById('txtAR35_1max').focus();
		}
		var $updateAmericanRouletteLimitDialog='';
		$(document).ready(function() {
		$updateAmericanRouletteLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<table border="0" cellpadding="0" cellspacing="0">'
					//+'<tr><td class="left">All Max</td><td class="right"><input type="text"/ id="txtUAmericanRouletteAllMax"><font color="red"> *</font></td><td class="left">All Min</td><td class="right"><input type="text"/ id="txtUAmericanRouletteAllMin"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">35 to 1 Max</td><td class="right"><input type="text"/ id="txtUAR35_1max"><font color="red"> *</font></td><td class="left">35 to 1 Min</td><td class="right"><input type="text"/ id="txtUAR35_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">17 to 1 Max</td><td class="right"><input type="text"/ id="txtUAR18_1max"><font color="red"> *</font></td><td class="left">17 to 1 Min</td><td class="right"><input type="text"/ id="txtUAR18_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">11 to 1 Max</td><td class="right"><input type="text"/ id="txtUAR12_1max"><font color="red"> *</font></td><td class="left">11 to 1 Min</td><td class="right"><input type="text"/ id="txtUAR12_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">8 to 1 Max</td><td class="right"><input type="text"/ id="txtUAR9_1max"><font color="red"> *</font></td><td class="left">8 to 1 Min</td><td class="right"><input type="text"/ id="txtUAR9_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">5 to 1 Max</td><td class="right"><input type="text"/ id="txtUAR6_1max"><font color="red"> *</font></td><td class="left">5 to 1 Min</td><td class="right"><input type="text"/ id="txtUAR6_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">2 to 1 Max</td><td class="right"><input type="text"/ id="txtUAR2_1max"><font color="red"> *</font></td><td class="left">2 to 1 Min</td><td class="right"><input type="text"/ id="txtUAR2_1min"><font color="red"> *</font></td></tr>'
					+'<tr><td class="left">1 to 1 Max</td><td class="right"><input type="text"/ id="txtUAR1_1max"><font color="red"> *</font></td><td class="left">1 to 1 Min</td><td class="right"><input type="text"/ id="txtUAR1_1min"><font color="red"> *</font></td></tr>'
					+'</table>'
					+'<div id="table_limit_footer"><button class="btn red" id="confirm-american-roulette-limit1" >Update</button>&nbsp;&nbsp;<button class="btn red" onClick="$deleteAmericanRouletteLimitDialog.dialog(\'open\')" >Delete</button><br/><input type="text" id="update_american_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 660,
				title: 'Setting American Roulette Limit',
				resizable: false,
				modal: true
			});
			$('#confirm-american-roulette-limit1').click(function(event) {
				//if(trim(document.getElementById('txtUAmericanRouletteAllMax').value)==''){document.getElementById('update_american_roulette_err_message').value='All Max is required!';document.getElementById('txtUAmericanRouletteAllMax').focus(); return false;}else if(validate(document.getElementById('txtUAmericanRouletteAllMax').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid all max!';document.getElementById('txtUAmericanRouletteAllMax').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				//if(trim(document.getElementById('txtUAmericanRouletteAllMin').value)==''){document.getElementById('update_american_roulette_err_message').value='All Min is required!';document.getElementById('txtUAmericanRouletteAllMin').focus(); return false;}else if(validate(document.getElementById('txtUAmericanRouletteAllMin').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid all min!';document.getElementById('txtUAmericanRouletteAllMin').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR35_1max').value)==''){document.getElementById('update_american_roulette_err_message').value='35 to 1 Max is required!';document.getElementById('txtUAR35_1max').focus(); return false;}else if(validate(document.getElementById('txtUAR35_1max').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 35 to 1 max!';document.getElementById('txtUAR35_1max').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR35_1min').value)==''){document.getElementById('update_american_roulette_err_message').value='35 to 1 Min is required!';document.getElementById('txtUAR35_1min').focus(); return false;}else if(validate(document.getElementById('txtUAR35_1min').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 35 to 1 min!';document.getElementById('txtUAR35_1min').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR18_1max').value)==''){document.getElementById('update_american_roulette_err_message').value='17 to 1 Max is required!';document.getElementById('txtUAR18_1max').focus(); return false;}else if(validate(document.getElementById('txtUAR18_1max').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 17 to 1 max!';document.getElementById('txtUAR18_1max').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR18_1min').value)==''){document.getElementById('update_american_roulette_err_message').value='17 to 1 Min is required!';document.getElementById('txtUAR18_1min').focus(); return false;}else if(validate(document.getElementById('txtUAR18_1min').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 17 to 1 min!';document.getElementById('txtUAR18_1min').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR12_1max').value)==''){document.getElementById('update_american_roulette_err_message').value='11 to 1 Max is required!';document.getElementById('txtUAR12_1max').focus(); return false;}else if(validate(document.getElementById('txtUAR12_1max').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 11 to 1 max!';document.getElementById('txtUAR12_1max').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR12_1min').value)==''){document.getElementById('update_american_roulette_err_message').value='1 to 1 Min is required!';document.getElementById('txtUAR12_1min').focus(); return false;}else if(validate(document.getElementById('txtUAR12_1min').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 11 to 1 min!';document.getElementById('txtUAR12_1min').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR9_1max').value)==''){document.getElementById('update_american_roulette_err_message').value='8 to 1 Max is required!';document.getElementById('txtUAR9_1max').focus(); return false;}else if(validate(document.getElementById('txtUAR9_1max').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 8 to 1 max!';document.getElementById('txtUAR9_1max').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR9_1min').value)==''){document.getElementById('update_american_roulette_err_message').value='8 to 1 Min is required!';document.getElementById('txtUAR9_1min').focus(); return false;}else if(validate(document.getElementById('txtUAR9_1min').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 8 to 1 min!';document.getElementById('txtUAR9_1min').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR6_1max').value)==''){document.getElementById('update_american_roulette_err_message').value='5 to 1 Max is required!';document.getElementById('txtUAR6_1max').focus(); return false;}else if(validate(document.getElementById('txtUAR6_1max').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 5 to 1 max!';document.getElementById('txtUAR6_1max').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR6_1min').value)==''){document.getElementById('update_american_roulette_err_message').value='5 to 1 Min is required!';document.getElementById('txtUAR6_1min').focus(); return false;}else if(validate(document.getElementById('txtUAR6_1min').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 5 to 1 min!';document.getElementById('txtUAR6_1min').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR2_1max').value)==''){document.getElementById('update_american_roulette_err_message').value='2 to 1 Max is required!';document.getElementById('txtUAR2_1max').focus(); return false;}else if(validate(document.getElementById('txtUAR2_1max').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 2 to 1 max!';document.getElementById('txtUAR2_1max').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR2_1min').value)==''){document.getElementById('update_american_roulette_err_message').value='2 to 1 Min is required!';document.getElementById('txtUAR2_1min').focus(); return false;}else if(validate(document.getElementById('txtUAR2_1min').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 2 to 1 min!';document.getElementById('txtUAR2_1min').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR1_1max').value)==''){document.getElementById('update_american_roulette_err_message').value='1 to 1 Max is required!';document.getElementById('txtUAR1_1max').focus(); return false;}else if(validate(document.getElementById('txtUAR1_1max').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 1 to 1 max!';document.getElementById('txtUAR1_1max').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				if(trim(document.getElementById('txtUAR1_1min').value)==''){document.getElementById('update_american_roulette_err_message').value='1 to 1 Min is required!';document.getElementById('txtUAR1_1min').focus(); return false;}else if(validate(document.getElementById('txtUAR1_1min').value)==false){document.getElementById('update_american_roulette_err_message').value='Invalid 1 to 1 min!';document.getElementById('txtUAR1_1min').focus();return false;}else{document.getElementById('update_american_roulette_err_message').value='';}
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/UpdateAmericanRouletteLimit',
		    		method: 'POST',
		    		data: {	'roulette_limit_id': document.getElementById('txtUAR35_1max').tag,
			    			'currency_id': document.getElementById('cmbCurrencyType').value, 
			    			'all_max': 0,//trim(document.getElementById('txtUAmericanRouletteAllMax').value), 
			    			'all_min': 0,//trim(document.getElementById('txtUAmericanRouletteAllMin').value), 
			    			'35_to_1_max': trim(document.getElementById('txtUAR35_1max').value),
			    			'35_to_1_min': trim(document.getElementById('txtUAR35_1min').value),
			    			'18_to_1_max': trim(document.getElementById('txtUAR18_1max').value),
			    			'18_to_1_min': trim(document.getElementById('txtUAR18_1min').value),
			    			'12_to_1_max': trim(document.getElementById('txtUAR12_1max').value),
			    			'12_to_1_min': trim(document.getElementById('txtUAR12_1min').value),
			    			'9_to_1_max': trim(document.getElementById('txtUAR9_1max').value),
			    			'9_to_1_min': trim(document.getElementById('txtUAR9_1min').value),
			    			'6_to_1_max': trim(document.getElementById('txtUAR6_1max').value),
			    			'6_to_1_min': trim(document.getElementById('txtUAR6_1min').value),
			    			'2_to_1_max': trim(document.getElementById('txtUAR2_1max').value),
			    			'2_to_1_min': trim(document.getElementById('txtUAR2_1min').value),
			    			'1_to_1_max': trim(document.getElementById('txtUAR1_1max').value),
			    			'1_to_1_min': trim(document.getElementById('txtUAR1_1min').value),
			    	},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			//jQuery("#list6").jqGrid('setCell', cRowNo, 2, trim(document.getElementById('txtUAmericanRouletteAllMin').value) + ' - ' + trim(document.getElementById('txtUAmericanRouletteAllMax').value));
		    			jQuery("#list6").jqGrid('setCell', cRowNo, 3, trim(document.getElementById('txtUAR35_1min').value) + ' - ' + trim(document.getElementById('txtUAR35_1max').value));
		    			jQuery("#list6").jqGrid('setCell', cRowNo, 4, trim(document.getElementById('txtUAR18_1min').value) + ' - ' + trim(document.getElementById('txtUAR18_1max').value));
		    			jQuery("#list6").jqGrid('setCell', cRowNo, 5, trim(document.getElementById('txtUAR12_1min').value) + ' - ' + trim(document.getElementById('txtUAR12_1max').value));
		    			jQuery("#list6").jqGrid('setCell', cRowNo, 6, trim(document.getElementById('txtUAR9_1min').value) + ' - ' + trim(document.getElementById('txtUAR9_1max').value));
		    			jQuery("#list6").jqGrid('setCell', cRowNo, 7, trim(document.getElementById('txtUAR6_1min').value) + ' - ' + trim(document.getElementById('txtUAR6_1max').value));
		    			jQuery("#list6").jqGrid('setCell', cRowNo, 8, trim(document.getElementById('txtUAR2_1min').value) + ' - ' + trim(document.getElementById('txtUAR2_1max').value));
		    			jQuery("#list6").jqGrid('setCell', cRowNo, 9, trim(document.getElementById('txtUAR1_1min').value) + ' - ' + trim(document.getElementById('txtUAR1_1max').value));
		    			$updateAmericanRouletteLimitDialog.dialog('close');
		    			jQuery("#list6").trigger("reloadGrid");
			    	}
		    	});
			});
			
		});
		var $deleteAmericanRouletteLimitDialog='';
		$(document).ready(function() {
		$deleteAmericanRouletteLimitDialog = $('<div></div>')
			.html('<div id="table_limit">'
					+'<br/>'
					+'<center>Are you sure you want to delete this setting?</center><br/><br/>'
					+'<div id="table_limit_footer"><button class="btn red" id="delete-american-roulette-limit">Yes</button>&nbsp;&nbsp;<button class="btn red"  onClick="$deleteAmericanRouletteLimitDialog.dialog(\'close\')">No</button><br/><input type="text" id="update_american_roulette_err_message" readonly="true"/></div>'
					+'</div>')
			.dialog({
				autoOpen: false,
				width: 300,
				title: 'Delete',
				resizable: false,
				modal: true
			});
			$('#delete-american-roulette-limit').click(function(event) {
				jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/UpdateTableLimit/DeleteAmericanRouletteLimit',
		    		method: 'POST',
		    		data: {'roulette_limit_id': document.getElementById('txtUAR35_1max').tag},
		    		context: jQuery(this).val(),
		    		success: function(data) {
		    			$updateAmericanRouletteLimitDialog.dialog('close');
		    			$deleteAmericanRouletteLimitDialog.dialog('close');
		    			jQuery("#list6").trigger("reloadGrid");
			    	}
		    	});
			});
		});
		//Other functions
		function selectGameType(selectGame,selectCurrency){
		    var gametype = selectGame.options[selectGame.selectedIndex].value;
		    var currency_id = selectCurrency.options[selectCurrency.selectedIndex].value;
		    loadGameTable(gametype,currency_id);
		}
		function loadGameTable(selected_gametype,selected_currency){
			if (selected_gametype=='all'){
		    	document.getElementById('qry_result').innerHTML='';
		    	tableBacarrat(selected_currency);
		    	tableDragonTiger(selected_currency);
		    	tableRoulette(selected_currency);
		    	tableBlackjack(selected_currency);
		    	tableAmericanRoulette(selected_currency);
			}
		    else if (selected_gametype==1){
		    	document.getElementById('qry_result').innerHTML='';
		    	tableBacarrat(selected_currency);
		    }
		    else if(selected_gametype==2){
		    	document.getElementById('qry_result').innerHTML='';
		    	tableRoulette(selected_currency);
		    }
		    else if(selected_gametype==3){
		    	document.getElementById('qry_result').innerHTML='';
		    	tableBlackjack(selected_currency);
			}
		    else if(selected_gametype==4){
		    	document.getElementById('qry_result').innerHTML='';
		    	tableDragonTiger(selected_currency);
			}
		    else if(selected_gametype==5){
		    	document.getElementById('qry_result').innerHTML='';
		    	tableAmericanRoulette(selected_currency);
			}
		    else
		    {
		    	document.getElementById('qry_result').innerHTML='';
			}

			<?php //if (Yii::app()->session['level']==7) {
			if(!Yii::app()->user->checkAccess('gameSetting.writeTableLimit')){
			?>
				$('#btnBac').hide();
				$('#btnDra').hide();
				$('#btnBla').hide();
				$('#btnEur').hide();
				$('#btnAme').hide();
			<?php }?>
		}
		function trim(stringToTrim) {
			return stringToTrim.replace(/^\s+|\s+$/g,"");
		}
		function validate(text)
		{
			var pattern = /^-?[0-9]+(.[0-9]{1,2})?$/; 
			//var text = document.getElementById('txtBunosAmount').value;
		    if (text.match(pattern)==null) 
		    {
				return false;
		    }
			else
			{
				return true;
			}
		}
		function get_cookie(cookie_name)
		{
		  var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');
		
		  if (results)
		    return (unescape(results[2]));
		  else
		    return null;
		}
		
	</script>
</head>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
<body onload="javascript: loadGameTable(document.getElementById('cmbGameType').value,document.getElementById('cmbCurrencyType').value);">
	<div id="tl_header">Table Limit</div>
	<div id="qry_selector">
		Game Type: 
		<select name="cmbGameType" id="cmbGameType" class="nput" onchange="javascript: selectGameType(this,document.getElementById('cmbCurrencyType'))">
			<option value="all">ALL</option>
			<?php 
				$dataReader = TableGame::model()->findAll();
				foreach ($dataReader as $row){
					echo '<option value="' . $row['id'] . '">'. strtoupper($row['game_name']) . '</option>';
				}
			?>
		</select>
		Currency Type:
		<select name="cmbCurrencyType" id="cmbCurrencyType" class="nput" onchange="javascript: selectGameType(document.getElementById('cmbGameType'),this)">
			<?php 
				$dataReader = TableCurrency::model()->findAll();
				foreach ($dataReader as $row){
					echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
				}
			?>
		</select><hr>
		<a class="btn mini red" href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitLog">View Table Limit Logs <i class="icon-reorder"></i></a>
	</div>
	<div id="qry_result">
	
	</div>
	<br/>
</body>
</html>