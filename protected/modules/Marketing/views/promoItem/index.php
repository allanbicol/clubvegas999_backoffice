<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/systemsetting.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/promoItem.js"></script>
<script type="text/javascript">
	document.getElementById('marketingHeader').className="start active";
	document.getElementById('mnu_promo_item').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Marketing <i class='icon-angle-right'></i></li><li><a href='#'>Manage Promo Items</a></li>");

	var createPromoItemsURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PromoItem/SavePromoItem';
	var updateAvailableURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PromoItem/UpdateAvailablePromoItem';
	var updatePromoItemURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PromoItem/UpdatePromoItem';
	var promoItemListURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PromoItem/PromoItemList';
	var getDetailsUrl="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PromoItem/PromoItemDetails";
</script>
</head>
<body>
	<div><b>PROMO ITEM MANAGEMENT</b></div>
	<br/>
	<table id="list2" ><tr><td/></tr></table>
	<div id="pager2"></div>
</body>
</html>
