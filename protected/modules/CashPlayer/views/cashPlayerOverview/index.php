<html>
<head>
	<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayeroverview.css"/>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.7.2/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	
	<script type="text/javascript">
	//active menu color

	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_cash_player_overview').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><a href='#'>Cash Player Overview</a></li>");
	</script>
	<script type="text/javascript">
	function getTotalBalance(){
		
		testChecked=0;
		if (document.getElementById("chkExceptTestCurrency").checked==true){
			 testChecked=1;
		}
		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerOverview/PlayersBalance',
    		type: 'POST',
    		data: {'currency': document.getElementById('cmbCurrency').value,'test':testChecked},
    		context:'',
    		 beforeSend: function(){
    			 document.getElementById('lblLoading').style.display='block';
    			},
    		success: function(data) {
        		var result=data.split("#");
        		yesterdayWinloss=parseFloat(result[2]);
        		yesterdaySlotWinloss=parseFloat(result[3]);
        		if (parseFloat(result[2])==0){
					yesterdayWinloss=parseFloat(result[2]);
            	}else{
            		yesterdayWinloss=parseFloat(result[2])*-1;
                }

        		if (parseFloat(result[3])==0){
					yesterdaySlotWinloss=parseFloat(result[3]);
            	}else{
            		yesterdaySlotWinloss=parseFloat(result[3])*-1;
                }
        		document.getElementById('lblLoading').style.display='none';
        		document.getElementById('lblPlayerBalance').innerHTML=result[0];
    			document.getElementById('lblThisCurrency').innerHTML=result[1];
    			document.getElementById('lblYesterdayWinLossCurrency').innerHTML=result[1];
    			document.getElementById('lblYesterdayWinLoss').innerHTML=yesterdayWinloss.toFixed(2);
    			document.getElementById('lblYesterdaySlotWinLossCurrency').innerHTML=result[1];
    			document.getElementById('lblYesterdaySlotWinLoss').innerHTML=yesterdaySlotWinloss.toFixed(2);
	    	}
    	});
	}

	function tableCashPlayerOverviewDetails(test_currency) { 
		document.getElementById('qry_result').innerHTML='';
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerOverview/CashPlayerOverviewDetails&test_currency=' + test_currency, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ["Currency","Number of Players","Total Balance"],
			    colModel: [
			      {name: 'currency', index: 'currency', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { if(tv=='Active'){return 'style="color:#2184c2"'}else if(tv=='Closed'){return 'style="color:#912626"'}else{ return 'style="color:black"'};}},
			      {name: 'no_players', index: 'no_players', width: 140,sortable: false, align:"right"},
			      {name: 'balance', index: 'balance', width: 160,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			    ],
			    loadComplete: function() {
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    				    
			    loadtext:"",
                rownumbers: true,
                sortable: false,
			    sortname: 'currency',
			    sortorder: 'ASC',
			    caption: 'Clubvegas999 Details <button class="btn red"  id="btnReturn" onclick="javascript: document.getElementById(\'qry_result\').innerHTML=\'\'">Hide <i class="icon-circle-arrow-left"></i></button>',
			    hidegrid: false,
			    viewrecords: true,
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
		});
	} 

	function btnDetails()
	{
		var test_currency='';
		if(document.getElementById('chkExceptTestCurrency').checked==true){
			test_currency=1;
		}else{
			test_currency=0;
		}
		
		tableCashPlayerOverviewDetails(test_currency);
		
	}
	function load(){
		document.getElementById("chkExceptTestCurrency").checked=true;
	}
	</script>
	<style>
		.ui-jqgrid .loading
		{
		    left: 45%;
		    top: 45%;
		    border:0;
		    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/3d-loader.png");
		    background-position-x: 50%;
		    background-position-y: 50%;
		    background-repeat: no-repeat;
		    height: 90px;
		    width: 90px;
		}
	</style>
</head>
<body onLoad="javascript: load();getTotalBalance(); ">
<b>Cash Player Overview</b>
<br/><br/>
<div id="lblLoading" style="display: none;">Calculating balance and winloss...</div>
<div id="body_wrapper">
	<div id='title_header'>Clubvegas999<b> <button class='btn red' id='btnDetails' onclick='javascript: btnDetails();'>View Balance Details <i class="icon-search"></i></button>
	Convert to<Select id="cmbCurrency" name="cmbCurrency" onchange="javascript:getTotalBalance();">
	<?php 
	$cur=TableCurrency::model()->findAll();
	foreach($cur as $row){
		echo '<option value="'.$row['id'].'">'.$row['currency_name'].'</option>';
	}
	?>
	</Select>
	</b>
	<input type="checkbox" id="chkExceptTestCurrency" name="chkExceptTestCurrency" value="0" onclick="javascript:getTotalBalance();"/><label for="chkExceptTestCurrency">Except Test Currency</label>
</div>
	<table id="tblAgentOverview" border="0" cellpadding="0" cellspacing="0" width="100%">
	
	<!-- header -->
	<tr>
		<td class="td_header_left"></td><td class="td_header_right" colspan="100%"><center>Win/Loss</center></td>
	</tr>
	<!-- header -->
	
	<tr>
		<td class="left">Live Casino Yesterday Total Win/loss</td>
		<td class="right" colspan="100%">
		<center><label id="lblYesterdayWinLossCurrency"></label> : <label id="lblYesterdayWinLoss"></label></center>
		</td>
	</tr>
	<tr>
		<td class="left">Live Casino Yesterday Total Slot Win/loss</td>
		<td class="right" colspan="100%">
		<center><label id="lblYesterdaySlotWinLossCurrency"></label> : <label id="lblYesterdaySlotWinLoss"></label></center>
		</td>
	</tr>
	<!-- header -->
	<tr>
		<td class="td_header_left"></td><td class="td_header_right" colspan="100%"><center>Total</center></td>
	</tr>
	<!-- header -->
	
	<tr>
		<td class="left"> Players Total Balance</td>
		<td class="right" colspan="100%">
		<center><label id="lblThisCurrency"></label> : <label id="lblPlayerBalance"></label></center>
		</td>
	</tr>
	<tr>
		<td class="left"> Players Total <font style="color: #4681a3;">Active</font> / <font style="color: #912626;">Closed</font></td>
		<td class="right" colspan="100%">
		<?php
		echo '<center>';
			$mem_active=TableCashPlayer::model()->count(array('select'=>'account_id','condition'=>'status_id=:status_id','params'=>array(':status_id'=>1),));
			$mem_closed=TableCashPlayer::model()->count(array('select'=>'account_id','condition'=>'status_id=:status_id','params'=>array(':status_id'=>3),));
		echo '<font style="color: #4681a3;">' . $mem_active . '</font> / ' . '<font style="color: #912626;">' . $mem_closed . '</font>';
		echo '</center>';
		?>
		</td>
	</tr>
	</table>
</div>
<div id="qry_result"></div>
<br/>
</body>
</html>