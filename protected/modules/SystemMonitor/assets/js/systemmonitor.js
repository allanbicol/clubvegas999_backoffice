var check;
/*
 * @todo casino column formatter
 * @author leokarl
 * @date 2012-12-08
 */
function casinoFormatter(cellvalue, options, rowObject) {
    if (cellvalue==0){
    	return 'ClubVegas999';
    }else if(cellvalue==1){
    	return 'HatienVegas999';
    }else if(cellvalue==2){
    	return 'SavanVegas999';
    }else if(cellvalue==3){
    	return 'CostaVegas999';
    }else if(cellvalue==4){
    	return 'VirtuaVegas999';
    }else if(cellvalue==5){
    	return 'SportBook';
    }else if(cellvalue==6){
    	return 'SlotVegas999';
    }
}
/*
 * @todo level column formatter
 * @author leokarl
 * @date 2012-12-08
 */
function levelFormatter(cellvalue, options, rowObject){
	if(cellvalue=='a'){
		return 'Agent Player';
	}else if(cellvalue=='c'){
		return 'Cash Player';
	}
}
/*
 * @todo operation column formatter
 * @author leokarl
 * @date 2012-12-08
 */
function operationFormatter(cellvalue, options, rowObject) {
	// initialize
	var account_id =  (rowObject[0] === undefined) ? rowObject.account_id : rowObject[0];
	var level = (rowObject[1] === undefined) ? rowObject.level : rowObject[1];
    if (account_id!=""){
    	if(writeOnlineUsers == 1 || userType == 'agent'){
    		return '<center><button class="btn red"  style="margin-buttom:5px" onClick="javacript: forcePlayerLogOut(\'' + account_id + '\',\'' + level + '\');">Logout <i class="icon-off"></i></button></center>';
    	}else{
    		return '<center><button class="btn red disabled"  disabled="disabled" style="margin-buttom:5px">Logout <i class="icon-off"></i></button></center>';
    	}
    }else{
        return '';
    }
}

/*
 * @todo force logout function
 * @author leokarl
 * @date 2012-12-08
 */
function forcePlayerLogOut(player_id,player_level){
	$createForceLogoutConfirmation.dialog('open');
	document.getElementById('player_id').value=player_id;
	document.getElementById('player_level').value=player_level;
	document.getElementById('player_name').innerHTML=player_id;
	//get casino_name
	jQuery.ajax({
		url: urlGetCasinoNameFromRedisByPlayerID,
		type: 'POST',
		data: {'player_id': player_id},
		context: '',
		success: function(casino_name) {
			document.getElementById('casino_name').innerHTML=casino_name;
    	}
	});
}




/*
 * @todo open when forcePlayerLogOut function clicked
 * @author leokarl
 * @date 2012-12-08
 */
var $createForceLogoutConfirmation='';
$(document).ready(function() {
	$createForceLogoutConfirmation = $('<div></div>')
		.html('<div id="dvDeposit">'
				+'<div><input id="player_id" type="hidden"><input id="player_level" type="hidden"></input></div>'
				+'<center><div>Are you sure you want to logout  <label style="color:red" id="player_name"></label> from the <label id="casino_name"></label>?</div></center><br/><br/>'
    			+'<div id="deposit_confrm_footer"><center><input class="btn red" type="submit" value="    OK    " onclick="javascript: logout(document.getElementById(\'player_id\').value,document.getElementById(\'player_level\').value);"/>&nbsp;<input class="btn red" id="cancel" type="submit" value="CANCEL"/></center></div>'
    			//+'<div id="waitgif"><img id="imgLogout1" style="cursor:pointer; width:470px;height:20px" src="' + urlJsAssets + '/images/logout-loader.gif" ></div>'
    			+'</div>')
		.dialog({
			autoOpen: false,
			width: 400,
			title: 'Force Logout',
			resizable: true,
			modal: true
		});
		$('#cancel').click(function(event) {
			$createForceLogoutConfirmation.dialog('close');
		});
		$(".ui-dialog-titlebar-close").hide(); 
});


/*
 * @todo refresh list every 10 seconds
 * @author leokarl
 * @date 2012-12-09
 */
$(document).ready(function() {
	setInterval(function(){
		//$('#list1').trigger("reloadGrid");
		tableOnlineUsers();//refresh list
	},10000);
});

/*
 * @todo wait dialog
 * @author leokarl
 * @date 2012-12-08
 */
/*
var $createWaitWithdrawProcessDialog='';
$(document).ready(function() {
	$createWaitWithdrawProcessDialog = $('<div></div>')
		.html( '<div id="dialogBonusError">'
		 +'</br><font color="black"><b>Note:</b> Processing balance stuck withdraw from the casino lobby.</font>'
		 +'</br></br>'
		 +'</table><img id="imgLogout1" style="cursor:pointer; width:470px;height:20px" src="' + urlJsAssets + '/images/logout-loader.gif" ></div>'
			)
		.dialog({
			autoOpen: false,
			closeOnEscape: false,
			width: 510,
			title: 'Please wait...',
			resizable: false,
			modal: true,
			cache: false,
		});
});

*/
/*
 * @todo call RedisLobbyManager/withdrawFromCasinoLobby() function
 * @author leokarl
 * @date 2012-12-08
 */
/*
function withdrawFromCasinoLobby(player_id){
	jQuery.ajax({
		url: urlWithdrawFromCasinoLobby, 
		type: 'POST',
		data: {'player_id': player_id},
		context: '',
		success: function(msg) {
			if(msg == 'error_connecting_htv'){
				alert('There is a connection problem with HatienVegas999.');
				// close wait dialog
				$createForceLogoutConfirmation.dialog('close'); // close ForceLogout dialog
				$('.logout').attr('disabled',false);// enable forcelogout button
	    		tableOnlineUsers();//refresh list
			}else if(msg == 'error_connecting_savan'){
				alert('There is a connection problem with SavanVegas999.');
				// close wait dialog
				$createForceLogoutConfirmation.dialog('close'); // close ForceLogout dialog
				$('.logout').attr('disabled',false);// enable forcelogout button
	    		tableOnlineUsers();//refresh list
			}else{
				$createForceLogoutConfirmation.dialog('close'); // close ForceLogout dialog
				$createWaitWithdrawProcessDialog.dialog('open');
				$(".ui-dialog-titlebar-close").hide();
				check = setInterval(function(){balanceTransferStatus(player_id);},2000);
			}
    	}
	});
}
*/
