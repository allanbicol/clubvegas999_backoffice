//active menu color
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_advisory').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i><li>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Advisory</a></li>");

var advisoryKeyUpdate = {'saveOld': 0, 'saveNew': 1}; // update
var cRowNo=0; // jQGrid
jQuery(document).ready(function() {
	// validate element if has or not, plugin
	jQuery.fn.isExisted = function(){
        return jQuery(this).length > 0;
	};

	var grid = jQuery("#advisoryList");
	grid.jqGrid({ 
		url: advisoryListURL, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['No.', 'Site', 'Published', 'Services Affected', 'Schedule', 'Start Time', 'End Time', 'Operation'],
	    colModel: [
	      {name: 'id', index: 'id', width: 45, align:'center'},
	      {name: 'site', index: 'site', width: 200},
	      {name: 'published', index: 'published', width: 100},
	      {name: 'servicesAffected', index: 'servicesAffected', width: 250},
	      {name: 'schedule', index: 'schedule', width: 100},
	      {name: 'startTime', index: 'startTime', width: 150},
	      {name: 'endTime', index: 'endTime', width: 150},
	      {name: 'modify', index: 'modify', width: 130, align:'center'}
	    ],

	    beforeSelectRow: function(rowid,e){

		},

		loadComplete:function(){
			jQuery("tr.jqgrow:odd").css("background", "#DDDDDC");
			jQuery("tr.jqgrow:even").css("background", "#ffffff");
		},

	    rowNum: 20,
	    rownumbers: true,
	    rowList: [5,10, 20, 30],
	    pager: '#advisoryPaper',
	    sortname: 'id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Advisory List',
	    viewrecords: true
	});

	jQuery('#advisoryList').jqGrid('navGrid', '#advisoryPaper', {edit: false, add: false, del:false,search: false, refresh: false}).navButtonAdd('#advisoryPaper', {
		   caption: "LOGS", 
		   buttonicon: "ui-icon-calculator", 
		   onClickButton: function(){ 
		     window.location='';
		   },
		   position:"last"
	});

	// lang
	jQuery(function() {
		// hide box
		// jQuery('#advisoryModifiedBox').hide();
		// tab
		jQuery('#tabLangs').tabs();
		// radio
		jQuery('#sch_radio2').click(function(event) {
			jQuery('#scheGroupEveryday').removeClass('disable-color');	
			jQuery('#scheGroupSomeday').addClass('disable-color');
			// hide startDateShow box and set
			startDateShow(true);
		});
		jQuery('#sch_radio3').click(function(event) {
			jQuery('#scheGroupEveryday').addClass('disable-color');
			jQuery('#scheGroupSomeday').removeClass('disable-color');
			// show box
			startDateShow(false);
		});
		// loop with languages
		//jQuery.each(advisoryLanguages, function(key) {
			// time-schedule1
			jQuery.each(['startDateTime2', 'endDateTime2'], function() {
				jQuery('#' + this).timepicker({
					hourGrid: 4,
					minuteGrid: 10,
					timeFormat: 'hh:mm tt'
				});
			});
			// datetime-schedule2
			jQuery.each(['startDateTime3', 'endDateTime3'], function() {
				jQuery('#' + this).datetimepicker({
					controlType: 'select',
					dateFormat: 'yy-mm-dd',
					timeFormat: 'hh:mm:ss tt'
				});
			});

			jQuery('#startDateShow').datepicker({
				controlType: 'select',
				dateFormat: 'yy-mm-dd'
			});

			jQuery('#startTimeShow').timepicker({
				hourGrid: 4,
				minuteGrid: 10,
				timeFormat: 'hh:mm tt'
			});
		//});

		// new
		if (jQuery('#advisoryAddbtn').isExisted()) {
//			jQuery('#advisoryAddbtn').click(function() {
//				// clean
//				cleanObjects();
//				// add test
//				jQuery('#advisoryActionTitleBox').text('Add New');
//				// show detail box
//				if (jQuery('#advisoryModifiedBox').isExisted()) jQuery('#advisoryModifiedBox').show();
//
//				// assign value
//				if (jQuery('#advisoryKeyID').isExisted()) jQuery('#advisoryKeyID').val(advisoryKeyUpdate.saveNew);
//				if (jQuery('#advisoryKeyUpdateValue').isExisted()) jQuery('#advisoryKeyUpdateValue').val(advisoryKeyUpdate.saveNew);
//			});
		}

		// cancel modify
		if (jQuery('#advisoryCancelbtn').isExisted()) {
			jQuery('#advisoryCancelbtn').click(function() {
				jQuery('#advisoryModifiedBox').hide();	
			});
		}

		// 
		jQuery('[id^=days_]').click(function() {
			validateScheduleText();
		});
	});
});

function startDateShow(status) {
	if (status == false) {
		jQuery('#startDateShow').show();
		jQuery('#startDateShowLabel').show();		
	} else {
		jQuery('#startDateShow').hide();
		jQuery('#startDateShowLabel').hide();
	}
}

// append everydays if true
function validateScheduleText() {
	if (jQuery('[id^=days_]:checked').length == jQuery('[id^=days_]').length) {
		jQuery('#label_days_sunday').text('Sunday (Everyday)');
	} else {
		jQuery('#label_days_sunday').text('Sunday');
	}
}

// clean and clear object
function cleanObjects() {
	// jQuery('');
//	jQuery.each(['startDateTime2', 'endDateTime2', 'startDateTime3', 'endDateTime3', 'startDateShow', 'startTimeShow'], function() {
//		jQuery('#' + this).val('');
//	});
	jQuery('.advisory-checkbox').attr('checked', false);
	// check box
	jQuery('.advisory-access-checkbox').attr('checked', false);
	// dropdown
	//jQuery('.advisory-dropdown').get(0).selectedIndex = 0; // status
	//jQuery('.advisory-dropdown').get(1).selectedIndex = 0; // type
	//jQuery('.advisory-dropdown').get(2).selectedIndex = 0; // icon
	// textarea and editor
	jQuery.each(advisoryLanguages, function(i, item) {
		// if (jQuery('#' + item.language + '_services_text').isExisted()) jQuery('#' + item.language + '_services_text').val('');
		if (CKEDITOR.instances[item.language + '_editor1']) CKEDITOR.instances[item.language + '_editor1'].setData('');
		if (CKEDITOR.instances[item.language + '_editor2']) CKEDITOR.instances[item.language + '_editor2'].setData('');
	});
	// schedule
	if (jQuery('#sch_radio' + 2).isExisted()) jQuery('#sch_radio' + 2).prop('checked', true);
	// hide startDate schedule
	startDateShow(true);
	if (jQuery('#startTimeShow').isExisted()) jQuery('#startTimeShow').val(''); 
	if (jQuery('#startDateShow').isExisted()) jQuery('#startDateShow').val('');
	if (jQuery('#scheGroupSomeday').isExisted()) jQuery('#scheGroupSomeday').addClass('disable-color');
}

// fill data
function fillDataToBoxes(json) {
	// accesses/previlege
	for (var i = 0; i < json.accesses.length; i++) {
		if (jQuery('#access_' + json.accesses[i]).isExisted()) jQuery('#access_' + json.accesses[i]).prop('checked', true);
	}
	// status
	if (jQuery('#status').isExisted()) jQuery('#status').val(json.status).attr('selected', true);
	// date time show
	if (jQuery('#startDateShow').isExisted()) jQuery('#startDateShow').val(json.scheduleStartedDateShow);
	if (jQuery('#startTimeShow').isExisted()) jQuery('#startTimeShow').val(json.scheduleStartedTimeShow);

	// schedule radio
	if (json.scheduleID == 1 || json.scheduleID == 2) {
		if (jQuery('#sch_radio' + 2).isExisted()) jQuery('#sch_radio' + 2).prop('checked', true);
		if (jQuery('#startDateTime' + 2).isExisted()) jQuery('#startDateTime' + 2).val(json.scheduleStartedDate); 
		if (jQuery('#endDateTime' + 2).isExisted()) jQuery('#endDateTime' + 2).val(json.scheduleEndedDate);

		// hide startDate schedule
		startDateShow(true);
	} else {
		if (jQuery('#sch_radio' + json.scheduleID).isExisted()) jQuery('#sch_radio' + json.scheduleID).prop('checked', true);
		if (jQuery('#startDateTime' + json.scheduleID).isExisted()) jQuery('#startDateTime' + json.scheduleID).val(json.scheduleStartedDate); 
		if (jQuery('#endDateTime' + json.scheduleID).isExisted()) jQuery('#endDateTime' + json.scheduleID).val(json.scheduleEndedDate);

		// show startDate schedule
		startDateShow(false);
	}

	// if (jQuery('#iconID').isExisted()) jQuery('#iconID').val(json.imagePath).attr('selected', true);
	if (jQuery('#iconID').isExisted()) jQuery('#iconID').val(json.imageID).attr('selected', true);

	for (var i = 0; i < json.sites.length; i++) {
		if (jQuery('#site_' + json.sites[i]).isExisted()) jQuery('#site_' + json.sites[i]).prop('checked', true);
	}

	if (jQuery('#serviceAffected').isExisted()) jQuery('#serviceAffected').val(json.servicesTypeID).attr('selected', true);

	jQuery.each(advisoryLanguages, function(i, item) {
		// if (jQuery('#' + item.language + '_services_text').isExisted()) jQuery('#' + item.language + '_services_text').val(json[item.language].servicetext);
		if (CKEDITOR.instances[item.language + '_editor1']) CKEDITOR.instances[item.language + '_editor1'].setData(json[item.language].servicetext);
		if (CKEDITOR.instances[item.language + '_editor2']) CKEDITOR.instances[item.language + '_editor2'].setData(json[item.language].description);
	});

	if (jQuery('#days_monday').isExisted()) jQuery('#days_monday').prop('checked', (json.monday == 1) ? true: false);
	if (jQuery('#days_tuesday').isExisted()) jQuery('#days_tuesday').prop('checked', (json.tuesday == 1) ? true: false);
	if (jQuery('#days_wednesday').isExisted()) jQuery('#days_wednesday').prop('checked', (json.wednesday == 1) ? true: false);
	if (jQuery('#days_thursday').isExisted()) jQuery('#days_thursday').prop('checked', (json.thursday == 1) ? true: false);
	if (jQuery('#days_friday').isExisted()) jQuery('#days_friday').prop('checked', (json.friday == 1) ? true: false);
	if (jQuery('#days_saturday').isExisted()) jQuery('#days_saturday').prop('checked', (json.saturday == 1) ? true: false);
	if (jQuery('#days_sunday').isExisted()) jQuery('#days_sunday').prop('checked', (json.sunday == 1) ? true: false);
}

//The instanceReady event is fired, when an instance of CKEditor has finished
//its initialization.
CKEDITOR.on('instanceReady', function(ev) {});

// edit
function advisoryEdit(id) {
	// load
	jQuery(function() {
	//	jQuery('.advisoryEdit').text('edit');
		// clean objects first
	//	cleanObjects();
		// change title
	//	jQuery('#advisoryActionTitleBox').text('Modifies ID: ' + id);
		// edit old
	//	if (jQuery('#advisoryKeyUpdateValue').isExisted()) jQuery('#advisoryKeyUpdateValue').val(advisoryKeyUpdate.saveOld);
		// show box
	//	if (jQuery('#advisoryModifiedBox').isExisted()) jQuery('#advisoryModifiedBox').show();
		// assign value
//		if (jQuery('#advisoryKeyID').isExisted()) jQuery('#advisoryKeyID').val(id);
//		// request
//		jQuery.ajax({
//		    type: 'POST',
//		    url: editAdvisoryURL,
//		    //contentType: 'application/json; charset=utf-8',
//		    //data: JSON.stringify({'data': jQuery(this).attr('rel')}),
//		    data: {'id': id},
//		    dataType: 'json',
//		    success: function(response) {
//		    	fillDataToBoxes(response);
//				// validate
//				validateScheduleText()
//		    },
//		    failure: function(errMsg) {
//		        alert(errMsg);
//		    }
//		});
        alert(id);
	});
}

// edit
function advisoryDelete(id) {
	// load
	jQuery(function() {
		jQuery('.advisoryDelete').text('delete');
		// clean objects first
		cleanObjects();
		// change title
		jQuery('#advisoryActionTitleBox').text('Delete');
		// delete old
		if (jQuery('#advisoryKeyUpdateValue').isExisted()) jQuery('#advisoryKeyUpdateValue').val(advisoryKeyUpdate.saveOld);
		// show box
		if (jQuery('#advisoryModifiedBox').isExisted()) jQuery('#advisoryModifiedBox').hide();
		// assign value
		if (jQuery('#advisoryKeyID').isExisted()) jQuery('#advisoryKeyID').val(id);
		// dialogbox
		if(confirm('Are you sure, you want to delete this No.=' + id + ' ?')) {
			// request
			jQuery.ajax({
			    type: 'POST',
			    url: deleteAdvisoryURL,
			    // contentType: 'application/json; charset=utf-8',
			    //data: JSON.stringify({'data': jQuery(this).attr('rel')}),
			    data:{'id': id},
			    // dataType: 'json',
			    success: function(response) {
			    	// window.location.href = window.location.href;
			    	// window.location.reload();
                    window.location.href = 'index.php?r=SystemSetting/Advisory';
			    },
			    failure: function(errMsg) {
			        alert(errMsg);
			    }
			});
		}
	});
}
