<?php
/**
 * @todo UpdateTableLimitController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-30
 */
class UpdateTableLimitController extends MyController
{
	public function actionUpdateBacarratLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->updateBacarratLimit();
		}
	}
	public function actionUpdateDragonTigerLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->updateDragonTigerLimit();
		}
	}
	public function actionUpdateRouletteLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->updateRouletteLimit();
		}
	}
	public function actionUpdateBlackjackLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->updateBlackjackLimit();
		}
	}
	public function actionUpdateAmericanRouletteLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->updateAmericanRouletteLimit();
		}
	}
	
	public function actionDeleteBacarratLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->deleteBacarratLimit();
		}
	}
	public function actionDeleteDragonTigerLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->deleteDragonTigerLimit();
		}
	}
	public function actionDeleteRouletteLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->deleteRouletteLimit();
		}
	}
	public function actionDeleteBlackjackLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->deleteBlackjackLimit();
		}
	}
	public function actionDeleteAmericanRouletteLimit()
	{
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->deleteAmericanRouletteLimit();
		}
	}	
	
	
// 	public function actionIndex()
// 	{
// 		$this->render("index");
// 	}
}
