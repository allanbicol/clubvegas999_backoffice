<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/winlossTotalPages.css" />-->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_consolidated_winloss').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Consolidated Win/Loss</a><i class='icon-angle-right'></i></li><li><a href='#'>Consolidated Winloss Details</a></li>");
</script>
<script type="text/javascript">
	function gotoBack()
	{
		var dateFrom = (document.getElementById('datefrom').value).split("-");
		
		var monthF = dateFrom[1];
		var dayF = dateFrom[2];
		var yearF = dateFrom[0];
		var dateF=(yearF + "/" + monthF + "/" + dayF);
		var dateTo =(document.getElementById('dateto').value).split("-");
		var monthT = dateTo[1];
		var dayT = dateTo[2];
		var yearT = dateTo[0];
		if (dayT.length==1){
			dayT="0"+dayT;
			}
		if (dayF.length==1){
			dayF="0"+dayF;
			}
		if (monthT.length==1){
			monthT="0"+monthT;
			}
		if (monthF.length==1){
			monthF="0"+monthF;
			}
		var dateT= (yearT + "/" + monthT + "/" +dayT);
		var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
		var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
		var accountId='<?php  if (isset($_GET['id'])){ echo $_GET['id'];}?>';
		window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss&dateFrom="+enc(dateSubmitFrom)+"&dateTo="+enc(dateSubmitTo)+"&id="+accountId;

		}
	
	var cRowNo=0;
	function tableConsolidatedWinLossSummary(method_id,dateFrom,dateTo,accountID) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_summary").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    //divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_summary").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLossDetail/CashPlayerConsolidatedWinLossSummary&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Casino', 'Account ID','Currency','Bet Count','Total Stake','Valid Stake','Average Bet','Win/Loss','Commission','Bonus','Tips','Total','P/L'],
			    colModel: [
					{name: 'casino', index: 'casino', width: 125, search:true,title:false},
					{name: 'account_id', index: 'account_id', width: 90,title:false,hidden:true},
					{name: 'currency_name', index: 'currency_name', width: 55, align:"center",title:false},
					{name: 'bet_count', index: 'bet_count', width: 60, align:"right",title:false},
					{name: 'total_stake', index: 'total_stake', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_stake">{0}</label>'},
					{name: 'amount_wager', index: 'amount_wager', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'avg_bet', index: 'avg_bet', width: 90, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'win_los', index: 'win_los', width: 90, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'commission', index: 'commission', width: 90, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'bonus', index: 'bonus', width: 90, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'amount_tips', index: 'amount_tips', width: 90, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'p_l', index: 'p_l', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				    ],
			
			    afterInsertRow : function(rowid, rowdata)
			    {
			        if (rowdata.win_loss < 0)
			        {
			           $(this).jqGrid('setCell', rowid,8 ,'', {color:'#990000'});
			           $(this).jqGrid('setCell', rowid,11 ,'', {color:'#990000'});
			          // $(this).jqGrid('setRowData', rowid, false, {color:'red'});
			        }
			        //getTotal();
			    },
			    loadComplete: function(){
			    	var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"casino","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bet_count","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"account_id","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"currency_name","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bet_caount","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_stake","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_wager","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"avg_bet","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_tips","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"p_l","",{background:'#D84A38',color:'white'});

				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'yellow'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'yellow'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'yellow'});	
					    }
				    	if(myrow.amount_tips < 0){
				    		grid.jqGrid('setCell',i,"amount_tips","",{color:'yellow'});	
					    }
					}   
			    	getTotal();
				},
			    loadtext:"",
			    //rownumbers:true,
			    hidegrid: false,
			    rowNum: 20,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'bet_date',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> Win/Loss Summary &nbsp;&nbsp;<button class="btn red" value="" onclick="javascript:gotoBack();">Back <i class="icon-circle-arrow-left"></i></button>',
			    viewrecords: true,
			    footerrow:true,
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});

			function getTotal(){
				var grid = $("#list1"),
		        sumBetCount = grid.jqGrid('getCol', 'bet_count', false, 'sum');
				sumTotalStake = grid.jqGrid('getCol', 'total_stake', false, 'sum');
				sumValidStake = grid.jqGrid('getCol', 'amount_wager', false, 'sum');
				sumAverageBet = grid.jqGrid('getCol', 'avg_bet', false, 'sum');
		        sumWinLoss=grid.jqGrid('getCol', 'win_los', false, 'sum');
		        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
		        sumBonus = grid.jqGrid('getCol', 'bonus', false, 'sum');
		        sumAmountTips = grid.jqGrid('getCol', 'amount_tips', false, 'sum');
		        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');
				sumPL=grid.jqGrid('getCol', 'p_l', false, 'sum');

		        grid.jqGrid('footerData','set', {casino:'Total',bet_count:sumBetCount,total_stake:sumTotalStake,amount_wager:sumValidStake,avg_bet:sumAverageBet,win_los:sumWinLoss,commission:sumCommission,bonus:sumBonus,amount_tips:sumAmountTips,total_win_los:sumTotal,p_l:sumPL});  
			}
		});
	} 
	
	function tableConsolidatedWinLossDetail(method_id,dateFrom,dateTo,accountID) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list2'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager2'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list2");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLossDetail/CashPlayerConsolidatedWinLossDetail&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Bet DateTime', 'Account ID','Currency','Transaction Type','Transaction Amount','Game Type','Bet Amount','Valid Bet','Win/Loss','Commission','Balance','Casino'],
			    colModel: [
					{name: 'bet_date', index: 'bet_date', width: 125, search:true,title:false},
					{name: 'account_id', index: 'account_id', width: 90,title:false,hidden:true},
					{name: 'currency_name', index: 'currency_name', width: 50, align:"center",title:false},
					{name: 'trans_type', index: 'trans_type', width: 100, align:"right",title:false},
					{name: 'trans_amount', index: 'trans_amount', width: 120, align:"right",title:false},
					{name: 'game_type', index: 'game_type', width: 110, align:"left",title:false,formatter:gameTypeFormatter},
					{name: 'bet_amount', index: 'bet_amount', width: 80, align:"right",title:false},
					{name: 'valid_bet', index: 'valid_bet', width: 90, align:"right",title:false},
					{name: 'win_los', index: 'win_los', width: 130, align:"right",title:false},
					{name: 'commission', index: 'commission', width: 100, align:"right",title:false},
					{name: 'balance', index: 'balance', width: 100, align:"right",title:false},
					{name: 'casino_name', index: 'casino_name', width: 100,title:false,formatter:casinoFormatter},
			    ],
			    afterInsertRow : function(rowid, rowdata)
			    {
			        if (rowdata.win_loss < 0)
			        {
			           $(this).jqGrid('setCell', rowid,8 ,'', {color:'#990000'});
			           $(this).jqGrid('setCell', rowid,11 ,'', {color:'#990000'});
			          // $(this).jqGrid('setRowData', rowid, false, {color:'red'});
			        }
			        //getTotal();
			    },
			    loadComplete: function(){
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
				},
			    loadtext:"",
			    rownumbers:true,
			    hidegrid: false,
			    rowNum: 20,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager2',
			    sortname: 'bet_date',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> Betting History - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    footerrow: false,
			});
			$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search: false});

			function casinoFormatter(cellvalue, options, rowObject) {
				var celval=cellvalue.split(":");
		        $("cellvalue").val(celval[1]);
		        if (celval[0]=="cv999_1"){
		        	return '<label >HTV999</label>';
		        }else if (celval[0]=="cv999_2"){
		        	return '<label >SavanVegas999</label>';
		        }else if (celval[0]=="cv999_3"){
		        	return '<label >CostaVegas999</label>';
		        }else if (celval[0]=="cv999_4"){
		        	return '<label >VirtuaVegas999</label>';
			 	}else if (celval[0]=="cv999_5"){
		        	return '<label >SportsBook999</label>';
	    		}else if (celval[0]=="cv999_6"){
        			return '<label >SlotsVegas999</label>';
				}else{
			    	return '<label>'+ celval[1]+'</label>';
				}
			};
			function gameTypeFormatter(cellvalue, options, rowObject) {
				var celval=cellvalue.split(":");
		        $("cellvalue").val(celval[1]);
		        if (celval[0]=="1"){
		        	return '<label >Jungle</label>';
		        }else if (celval[0]=="2"){
		        	return '<label >Pyramid</label>';
		        }else if (celval[0]=="3"){
		        	return '<label >Sango</label>';
		        }else if (celval[0]=="4"){
		        	return '<label >Crazy Monkey</label>';
				}else{
			    	return '<label>'+ celval[0]+'</label>';
				}
			};
			jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
	            caption:"Export current page", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportConsolidatedWinLossDetailToExcel();
	           }, 
	            position:"last"
	        });
			jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
	            caption:"Export all pages", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllConsolidatedWinLossDetailToExcel(method_id,dateFrom,dateTo,accountID);
	           }, 
	            position:"last"
	        });
		});
	} 

	
</script>
<script type="text/javascript">
function exportConsolidatedWinLossDetailToExcel(){
	var table1= document.getElementById('qry_result_summary');
	var table2= document.getElementById('qry_result');
	var html1 = table1.outerHTML;
	var html2 = table2.outerHTML;
	document.forms[2].csvBuffer.value="";
	document.forms[2].csvBuffer.value=html1+''+html2 ;
    document.forms[2].method='POST';
    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLossDetail/ConsolidatedDetailExcel';  // send it to server which will open this contents in excel file
    document.forms[2].target='_top';
    document.forms[2].submit();
}

function exportAllConsolidatedWinLossDetailToExcel(method_id,dateFrom,dateTo,accountID){
    $.ajax({	
		 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLossDetail/ExportAllCashPlayerConsolidatedWinLossDetail&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID, 
		 async:true,
		 success: function(result) {
				var data =result.split("<BREAK>");
				
				document.forms[2].csvBuffer.value="";
				document.forms[2].csvBuffer.value=data[2]+''+data[0] ;
			    document.forms[2].method='POST';
			    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLossDetail/ConsolidatedDetailExcel';  // send it to server which will open this contents in excel file
			    document.forms[2].target='_top';
			    document.forms[2].submit();
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
}

function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
function addCommas(nStr){
    nStr += '';
    c = nStr.split(','); // Split the result on commas
    nStr = c.join('');  // Make it back to a string without the commas
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

         
function decryptparam()
{
	var strF = document.getElementById('f').value;
	var strT= document.getElementById('t').value;
	var encodeF = enc(strF);
	var encodeT = enc(strT);

	var omonthF="";
	var omonthT="";
	var odayF="";
	var odayT="";
	
	var dateF= encodeF.split("_");	
	var dateFrom=dateF[0];
	var dateT= encodeT.split("_");	
	var dateTo=dateT[0];
	var dateFSplit=dateFrom.split("/");
	var dateTSplit=dateTo.split("/");
	
	if (dateFSplit[1].length==1){
		omonthF="0"+ dateFSplit[1];
	}else{omonthF=dateFSplit[1]; }
	if (dateTSplit[1].length==1){
		omonthT="0"+ dateTSplit[1];
	}else{omonthT=dateTSplit[1];}
	if (dateFSplit[2].length==1){
		odayF="0"+ dateFSplit[2];
	}else{odayF=dateFSplit[2];}
	if (dateTSplit[2].length==1){
		odayT="0"+ dateTSplit[2];
	}else{odayT=dateTSplit[2];}
	document.getElementById("datefrom").value=dateFSplit[0]+"-"+omonthF+"-"+odayF;
	document.getElementById("dateto").value=dateTSplit[0]+"-"+omonthT+"-"+odayT;

}
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

	function loadTransTable(selected_method,dateFrom,dateTo,accountID){
		if (selected_method=='ALL'){
			//for fund group by currency
			document.getElementById('qry_result_summary').innerHTML='';
	    	tableConsolidatedWinLossSummary(selected_method,dateFrom,dateTo,accountID);
	    	document.getElementById('qry_result').innerHTML='';
	    	tableConsolidatedWinLossDetail(selected_method,dateFrom,dateTo,accountID);
	    	
		}
	    else
	    {
	    	//for fund group by currency
	    	document.getElementById('qry_result_summary').innerHTML='';
	    	tableConsolidatedWinLossSummary(selected_method,dateFrom,dateTo,accountID);
	    	document.getElementById('qry_result').innerHTML='';
	    	tableConsolidatedWinLossDetail(selected_method,dateFrom,dateTo,accountID);


		}
	}

	function btnclick(btnname)
	{
		//document.getElementById('currentBetcount').innerHTML="0";
        //document.getElementById('currentBetAmount').innerHTML="0.00";
        //document.getElementById('currentWinloss').innerHTML="0.00";
        //document.getElementById('currentTips').innerHTML="0.00";
        //document.getElementById('currentTotal').innerHTML="0.00";
        
		//document.getElementById('allBetcount').innerHTML="0";
        //document.getElementById('allBetAmount').innerHTML="0.00";
       // document.getElementById('allWinloss').innerHTML="0.00";
       	//document.getElementById('allTips').innerHTML="0.00";
       	//document.getElementById('allTotal').innerHTML="0.00";
       //	document.getElementById('currentBalance').innerHTML="0.00";

		if (btnname=="Load")
		{
			var datefrom=((document.getElementById('datefrom').value).replace("-","/").replace("-","/")+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateto=((document.getElementById('dateto').value).replace("-","/").replace("-","/")+"_"+ document.getElementById('cbHourto').value +":59:59");
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable("ALL",datefrom,dateto,accountID);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			var dayL=theday.toString;
			var monthL=themonth.toString;
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theday="0"+theday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('gameType').value;
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(methodtype,datefrom,dateto,accountID);

			//jQuery.ajax({
	    	//	url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetail/GetRecordAllPages',
	    	//	type: 'POST',
	    	//	data: {'accountId':accountID,
	        //			'methodId':methodtype,
	        //			'dateFrom':datefrom,
	        //			'dateTo':dateto},
	    	//	context:'',
	    	//	success: function(data) {
	        //		var resultAllPages=data.split("#");
	        //		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	        //        document.getElementById('allBetAmount').innerHTML=resultAllPages[1];
	        //        document.getElementById('allWinloss').innerHTML=resultAllPages[2];
	         //      	document.getElementById('allTips').innerHTML=resultAllPages[3];
	         //      	document.getElementById('allTotal').innerHTML=resultAllPages[4];
	         //      	document.getElementById('currentBalance').innerHTML=resultAllPages[5];
		    //	}
	    	//});
			
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			var dayL=theyday.toString;
			var monthL=themonth.toString;
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theyday="0"+theyday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theyday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theyday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('gameType').value;
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(methodtype,datefrom,dateto,accountID);

			//jQuery.ajax({
	    	//	url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetail/GetRecordAllPages',
	    	//	type: 'POST',
	    	//	data: {'accountId':accountID,
	        //			'methodId':methodtype,
	        //			'dateFrom':datefrom,
	        //			'dateTo':dateto},
	    	//	context:'',
	    	//	success: function(data) {
	        //		var resultAllPages=data.split("#");
	        //		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	         //       document.getElementById('allBetAmount').innerHTML=resultAllPages[1];
	         //       document.getElementById('allWinloss').innerHTML=resultAllPages[2];
	         //      	document.getElementById('allTips').innerHTML=resultAllPages[3];
	        //       	document.getElementById('allTotal').innerHTML=resultAllPages[4];
	         //      	document.getElementById('currentBalance').innerHTML=resultAllPages[5];
		    //	}
	    	//});
			
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var methodtype=document.getElementById('gameType').value;
			var accountID=document.getElementById('txtAccountID').value;
			
			loadTransTable(methodtype,dateSubmitFrom,dateSubmitTo,accountID);
			
			//jQuery.ajax({
	    	//	url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetail/GetRecordAllPages',
	    	//	type: 'POST',
	    	//	data: {'accountId':accountID,
	        //			'methodId':methodtype,
	        //			'dateFrom':dateSubmitFrom,
	        //			'dateTo':dateSubmitTo},
	    	//	context:'',
	    	//	success: function(data) {
	        //		var resultAllPages=data.split("#");
	        //		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	        //        document.getElementById('allBetAmount').innerHTML=resultAllPages[1];
	        //        document.getElementById('allWinloss').innerHTML=resultAllPages[2];
	        //       	document.getElementById('allTips').innerHTML=resultAllPages[3];
	        //       	document.getElementById('allTotal').innerHTML=resultAllPages[4];
	        //       	document.getElementById('currentBalance').innerHTML=resultAllPages[5];
		    //	}
	    	//});
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
div.dateSummaryFooter { text-align: Left; }
tr.footrow-ltr td { background-color: #AD8533; color: black; }
</style>
</head>
<body onload="javascript:decryptparam();btnclick('Submit');">
<div style="position: relative;left: 5px; top:3px">
	<form action="" >
		<table style="background-color:transparent; border-style:ridge;  width: 500px; display: none;" hidden>
		<tr><td style=" background-color: black; color:white; height: 25px "><label style="color:red"><?php echo $_GET['account_id'];?></label> Betting History</td></tr>
		<tr>
		<input type="text" id="txtAccountID" value="<?php echo $_GET['account_id'];?>" style="display: none;" ></tr>
		<tr><td>METHOD:
			<select id="gameType">
				<option value="ALL">ALL</option>
  				<?php 
				$dataReader = TableGame::model()->findAll();
				foreach ($dataReader as $row){
					echo '<option value="' . $row['game_name'] . '">'. strtoupper($row['game_name']) . '</option>';
				}
				?>
			</select>
		</td></tr>
		<tr><td>FROM&nbsp;:<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px; background-color: transparent" disabled></td></tr><tr><td>TO &nbsp;&nbsp;&nbsp;&nbsp;:<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; background-color: transparent" disabled></td></tr>
			<tr><td><div align="center">
		<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btnclickMe">
		<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btnclickMe">
		<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btnclickMe">
		<input onclick="javascript: gotoBack();" type="button" value="Back"></div>
		</td></tr>
		</table>
		</form>	
		
		<br/>
	<div id="qry_result_summary">	
	</div></br>
	<div id="qry_result">
	</div>
	<div id="pager2"></div>
	<br/>
	<!--  <table id="tableTotalPages">
		<tr>
			<th class="header" colspan=8 align="left"><b>Total</b></th>
		</tr>
		<tr class="tr" >
			<th class="th" width="100px" align="left">Type</th>
			<th class="th" width="120px">Number of Bet</th>
			<th class="th" width="150px">Valid Bet</th>
			<th class="th" width="150px">Win/Loss</th>
			<th class="th" width="90px">Tips</th>
			<th class="th" width="150px">Total</th>
			<th class="th" width="100px">Balance</th>
		</tr>
		<tr class="tr" >
			<td class="td" ><b>Current Page</b></td>
			<td class="td" id="currentBetcount" align="right"></td>
			<td class="td" id="currentBetAmount" align="right"></td>
			<td class="td" id="currentWinloss" align="right"></td>
			<td class="td" id="currentTips" align="right"></td>
			<td class="td" id="currentTotal" align="right"></td>
			<td class="td" id="currentBalance" align="center" rowspan="2"></td>
		</tr>
		<tr class="tr" >
			<td class="td" ><b>All Pages</b></td>
			<td class="td" id="allBetcount" align="right"></td>
			<td class="td" id="allBetAmount" align="right"></td>
			<td class="td" id="allWinloss" align="right"></td>
			<td class="td" id="allTips" align="right"></td>
			<td class="td" id="allTotal" align="right"></td>
		</tr>
	</table>-->
	<br/>
	<!--
	<div id="qry_resultTotal"></div>
	<div id="qry_resultTotal1"></div>
	<br/>-->
	<div><input id="f" value="<?php echo $_GET['f'];?>" hidden=true><input id="t" value="<?php echo $_GET['t'];?>" hidden=true></div>
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
	</form>
</div>

</body>
</html>