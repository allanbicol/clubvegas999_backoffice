<?php

class OnlineManagementController extends MyController
{
	private $onlinePlayer;
	public function actionOnlineUserList()
	{
		$this->onlinePlayer = new RedisManager();//CV999OnlinePlayersYii();
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : '';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : '';
		
		$records=count($this->onlinePlayer->getAllPlayersOnSite());
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records=$this->onlinePlayer->getAllPlayersJSONToJQGrid();
		echo JsonUtil::jsonRedisData($player_records, $total_pages, $page, $records);
	}
	public function actionLockUserList()
	{
		$cp = new OnlineManagement;
		$page = $_POST['page'];
		$limit = $_POST['rows'];
		$orderField = $_POST['sidx'];
		$sortType = $_POST['sord'];
	
		$result=$cp->getCountLockList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cp->getLockList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("account_id","onlive","session_date","onlive","account_id");
		$urlAS= Yii::app()->request->baseUrl;
	
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmValue,7);
	}
	public function actionLogoutUser()
	{
		if (Yii::app()->user->checkAccess('systemMonitor.writeOnlineUsers'))
		{
			$logout=new RedisManager();//OnlineUserLogout();
			$logout->forcePlayerLogout();
		}
	}
	
	/*
	 * @todo check if player is on the lobby. return 1 if online, 0 if not 
	 * @author leokarl
	 * @since 2012-12-07
	 */
	public function actionIsPlayerOnTheLoblly(){
		$redis=new RedisManager();
		echo ($redis->isExistingOnLobbyByPlayer($_POST['account_id']));
	}
	
	
	/*
	 * @todo withdraw balance from lobby
	 * @author leokarl
	 * @since 2012-12-08
	 */
	public function actionWithdrawFromCasinoLobby(){
		$redis = new RedisLobbyManager();
		$result = $redis->withdrawFromCasinoLobby($_POST['player_id']);
		 
		if ($result['errorCode'] == 1 && $result['casino'] == 'HTV') // error
		{
			//to display message that there is an error during lobby withdraw ih HTV
			echo 'error_connecting_htv';
		}
		else if ($result['errorCode'] == 1 && $result['casino'] == 'SAVAN') // error
		{
			//to display message that there is an error during lobby withdraw ih SAVAN
			echo 'error_connecting_savan';
		}
		else
		{
			// withdraw successful
			echo 'withdraw_succeeded';
		}
	}
	
	
	
	/**
	 * @todo GetCasinoNameFromRedisByPlayerID
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-26
	 */
	public function actionGetCasinoNameFromRedisByPlayerID(){
		$model=new RedisManager();//OnlineUserLogout();
		echo $model->getCasinoNameFromRedisByPlayerID($_POST['player_id']);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemMonitor.readOnlineUsers'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	/*
	 * @todo check if balance if stucked on the lobby. return 1 or 0
	* @author leokarl
	* @since 2012-12-08
	*/
	/*
	 public function actionCheckBalanceStuckedOnTheLobby(){
	$redis=new RedisLobbyManager();
	$return = ($redis->isStuckedBalanceOnLobby($_POST['player_id'])) ? 1 : 0;
	echo $return;
	}
	*/
	/*
	 * @todo check balance transfer status
	* @author leokarl
	* @date 2012-12-08
	*/
	/*
	public function actionBalanceTransferStatus(){
		$model = new OnlineManagement();
		echo $model->balanceTransferStatus($_POST['player_id']);
	}
	*/
}