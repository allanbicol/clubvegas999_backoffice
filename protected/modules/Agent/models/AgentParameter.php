<?php

/**
 * @todo AgentParameter Model
 * @copyright CE
 * @author Leo karl
 * @since 05/12/2012
 */
class AgentParameter
{
	public function saveAgentParameter()
	{
		$post=TableAgentParameter::model()->find(array('select'=>'id','condition'=>'id=:id','params'=>array(':id'=>1),));
		$post->baccarat_max_sharing=$_GET['baccarat_max_sharing'];
		$post->baccarat_max_commission=$_GET['baccarat_max_commission'];
		$post->roulette_max_sharing=$_GET['roulette_max_sharing'];
		$post->roulette_max_commission=$_GET['roulette_max_commission'];
		$post->american_roulette_max_sharing=$_GET['american_roulette_max_sharing'];
		$post->american_roulette_max_commission=$_GET['american_roulette_max_commission'];
		$post->dragon_tiger_max_sharing=$_GET['dragon_tiger_max_sharing'];
		$post->dragon_tiger_max_commission=$_GET['dragon_tiger_max_commission'];
		$post->blackjack_max_sharing=$_GET['blackjack_max_sharing'];
		$post->blackjack_max_commission=$_GET['blackjack_max_commission'];
		$post->slot_max_sharing=$_GET['slot_max_sharing'];
		$post->slot_max_commission=$_GET['slot_max_commission'];
		$post->max_win_multiple=$_GET['max_win_multiple'];
		$post->save();
	}
}
