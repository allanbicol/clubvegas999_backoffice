<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/validationerror.css" />
</head>

<body>
	<div id="body_content">
		<img id="process_warning" src="<?php echo Yii::app()->request->baseUrl;?>/images/process_warning.png"><br/>
		<label id="lblError"><?php echo $label;?>:</label>
		<label id="lblMessage"><?php echo $message; ?></label>
	</div>
</body>
</html>