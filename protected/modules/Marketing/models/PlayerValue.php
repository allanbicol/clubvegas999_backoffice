<?php
class PlayerValue
{
	
	public function getPlayerValueList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT tppv.account_id,SUM(IFNULL(tppv.player_value,0)) AS player_value,IFNULL(tpr.total_value,0) AS value_redeem,(IFNULL(SUM(tppv.player_value),0) - IFNULL(tpr.total_value,0)) AS available_value,
												SUM(IFNULL(tppv.points_earned,0)) AS player_points,IFNULL(tpr.total_points,0) AS points_redeem, (IFNULL(SUM(tppv.points_earned),0)-IFNULL(tpr.total_points,0)) AS available_points,tpr.redeem_count,'Redeem' AS operation
												FROM tbl_promo_player_value AS tppv LEFT JOIN (SELECT account_id,SUM(total_value) AS total_value,SUM(total_points) AS total_points,COUNT(account_id) AS redeem_count FROM tbl_promo_redeem GROUP BY account_id )AS tpr ON tppv.account_id = tpr.account_id WHERE tppv.date_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'   GROUP BY tppv.account_id
				 								ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountPlayerValueList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM (SELECT COUNT(0) FROM tbl_promo_player_value AS tppv LEFT JOIN tbl_promo_redeem AS tpr ON tppv.account_id = tpr.account_id WHERE tppv.date_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY tppv.account_id ) as Counter");
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayerValueListDetails($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT tpr.redeem_date,tpi.item_name,tpr.quantity,tpr.total_value,tpr.total_points FROM tbl_promo_redeem AS tpr LEFT JOIN tbl_promo_items AS tpi ON tpr.item_id = tpi.id WHERE account_id='".$_GET['account_id']."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountPlayerValueListDetails()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_promo_redeem AS tpr LEFT JOIN tbl_promo_items AS tpi ON tpr.item_id = tpi.id WHERE account_id='".$_GET['account_id']."'");
		$rows = $command->query();
		return $rows;
	}
	
	public function getItemName()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT id,item_name FROM tbl_promo_items where available=1 and stock <>0 order by item_name");
		$rows = $command->query();
		return $rows;
	}
}