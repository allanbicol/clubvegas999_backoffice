<?php
class UserListLogController extends MyController
{
	
	
	public function actionUserListLog()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readUserAccount'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	
}
