<?php
class CasinoInformationController extends MyController
{
	public function actionCasinoInformation()
	{
		//update new revenue sharing 
		$cps = new CasinoInformation();
		$cps->updateRevenueSharing();
		//-----------------------------------
		
		
		//echo $_GET['AccountID'];
		$cpHfund = new CasinoInformation;

		$player_records = $cpHfund->getCasinoInformation();
		//$test = $records->readAll();
		
		$filedNames = array("casino_name","currency_name","winloss","commission","total","agent_winloss","revenue_sharing","amount_to_pay","net_total");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),1,1, 1,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCasinoInformationTotal()
	{
		//update new revenue sharing 
		$cps = new CasinoInformation();
		$cps->updateRevenueSharing();
		//-----------------------------------
		
		
		//echo $_GET['AccountID'];
		$cpHfund = new CasinoInformation;
		
		$player_records = $cpHfund->getCasinoInformationTotal();
		//$test = $records->readAll();
		
		$filedNames = array("casino_name","currency_name","winloss","commission","total","agent_winloss","revenue_sharing","amount_to_pay","net_total","exchange_rate","winloss","commission","total","amount_to_pay","net_total");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),1,1, 1,$filedNames,$htmvalue,6);
	}
	
	public function actionIndex()
	{
		$this->render("index");
	}
	
	
}