$(function() {
	$( "#sortable1, #sortable2, #sortable3" ).sortable({
		connectWith: ".connectedSortable"
	}).disableSelection();
});

jQuery(function() {
	var id="";
	// Make all ul.sortable lists into sortable lists
	jQuery('ul.sortable').sortable({
	tolerance: 'pointer',
	cursor: 'pointer',
	dropOnEmpty: true,
	connectWith: 'ul.sortable',
	update: function(event, ui) {
			if(this.id == 'sortable-delete') {
				// Remove the element dropped on #sortable-delete
				id=$('#'+ui.item.attr('id')).attr('value');
				
				jQuery.ajax({
					url: deleteFilterURL,
					type: 'POST',
					data: {'id':id},
					context:'',
					success: function(data) {
						
			    	}
				});
				
				jQuery('#'+ui.item.attr('id')).remove();
				
			} else {
				// Update code for the actual sortable lists
			}
			}
		});
	});

	$(document).ready(function(){
	$('#txtDateFrom').datetimepicker({
		controlType: 'select',
		dateFormat: "yy-mm-dd",
		timeFormat: 'HH:mm:ss'
	});

	$('#txtDateTo').datetimepicker({
		controlType: 'select',
		dateFormat: "yy-mm-dd",
		timeFormat: 'HH:mm:ss'
	});
	$('#txtLastDatePlayed').datetimepicker({
		controlType: 'select',
		dateFormat: "yy-mm-dd",
		timeFormat: 'HH:mm:ss',
	});
	
	$('#loader').hide();
	
	if (document.getElementById('chkDeposit').checked==true){
		document.getElementById('grid_report_title').innerHTML='DEPOSITING PLAYER REPORT';
	}else{
		document.getElementById('grid_report_title').innerHTML='SIGNUP PLAYER REPORT';
	}
	var selectedField="";
	var unselectedField="";
	if (sessionSelectedItem!=null && sessionSelectedItem!=""){
		//Split Selected Field fron the database
		var splitSelectedData = sessionSelectedItem;
		var splitFilteredData=splitSelectedData.split('#');

		var selectedID = splitFilteredData[0].split(',');
		var selectedName = splitFilteredData[1].split(',');
		for (var i = 0; i < selectedID.length; i++) {
			selectedField=selectedField + '<li class="ui-state-default" value="'+selectedID[i]+'" id="'+selectedName[i]+'">'+selectedName[i]+'</li>';
		}
		
		//Split Unselected Field fron the database
		var splitUn_selectedData = sessionUnselectedItem;
		var splitUnselectedData=splitUn_selectedData.split('#');

		var unselectedID = splitUnselectedData[0].split(',');
		var unselectedName = splitUnselectedData[1].split(',');
		if (unselectedID[0]!=""){
			for (var i = 0; i < unselectedID.length; i++) {
				unselectedField=unselectedField + '<li class="ui-state-default" value="'+unselectedID[i]+'" id="'+unselectedName[i]+'">'+unselectedName[i]+'</li>';
			}
		}
		
		document.getElementById('pageSize').value=pager;
		document.getElementById('txtDateFrom').value=dateFrom;
		document.getElementById('txtDateTo').value=dateTo;
		document.getElementById('txtAccountID').value=accountID;
		document.getElementById('txtPlayerType').value=playerType;
		document.getElementById('txtCurrency').value=currency;
		document.getElementById('txtOperator').value=operator;
		document.getElementById('txtWinLoss').value=winloss;
		document.getElementById('txtCountry').value=country;
		document.getElementById('txtEmail').value=email;
		document.getElementById('txtPhone').value=phone;
		if (depositOpt==1){
			document.getElementById('chkDeposit').checked=true;
		}else{document.getElementById('chkDeposit').checked=false;}
		
		document.getElementById('sortable1').innerHTML = '';
		document.getElementById('sortable2').innerHTML = '';
		document.getElementById('sortable1').innerHTML = unselectedField;
		document.getElementById('sortable2').innerHTML = selectedField;
	}
});

function btnApplyFunction(){
		$('#loader').show(); 
		//selected fields
		var selected_columns = $('ul#sortable2 li').map(function(i,n) {
		    return $(n).attr('value');
		}).get().join(',');

		var selected_column_names = $('ul#sortable2 li').map(function(i,n) {
		    return $(n).attr('id');
		}).get().join(',');
		//unselected fields
		var unselected_columns = $('ul#sortable1 li').map(function(i,n) {
		    return $(n).attr('value');
		}).get().join(',');

		var unselected_column_names = $('ul#sortable1 li').map(function(i,n) {
		    return $(n).attr('id');
		}).get().join(',');
		
		var deposit=0;
		if (document.getElementById('chkDeposit').checked==true){
			deposit=1;
		}
		
		if (document.getElementById('chkDeposit').checked==true){
			document.getElementById('grid_report_title').innerHTML='DEPOSITING PLAYER REPORT';
		}else{
			document.getElementById('grid_report_title').innerHTML='SIGNUP PLAYER REPORT';
		}
		
		if (selected_column_names==""){
			alert("No item selected.");
			$('#loader').hide(); 
			return false;
		}else{
			$('#columns').val(selected_columns);
			$('#column_names').val(selected_column_names);
			//document.getElementById('params').submit();
			//return true;
			
			jQuery.ajax({
				url: customReportUrl,
				type: 'POST',
				beforeSend: function() { $('#loader').show(); },
				data: {'columns':selected_columns,'column_names':selected_column_names,
					   'unselectedColumns':unselected_columns,'unselectedColumn_names':unselected_column_names,
					   'pageSize':document.getElementById('pageSize').value,'deposit':deposit,
					   'dateFrom':document.getElementById('txtDateFrom').value,'dateTo':document.getElementById('txtDateTo').value,
					   'accountID':document.getElementById('txtAccountID').value,'playertype':document.getElementById('txtPlayerType').value,'currency':document.getElementById('txtCurrency').value,
					   'operator':document.getElementById('txtOperator').value,'winloss':document.getElementById('txtWinLoss').value,
					   'country':document.getElementById('txtCountry').value,'email':document.getElementById('txtEmail').value,'phone':document.getElementById('txtPhone').value},
				context:'',
				success: function(data) {
					var report= data.split('<!--grid report-->');
					var report1=report[1].split('<!--end grid report-->');
					document.getElementById('grid_report_wrapper').innerHTML = report1[0];
					$('#loader').hide();
		    	}
			});
		}
}

function saveFilter()
{
	var selected_columns = $('ul#sortable2 li').map(function(i,n) {
	    return $(n).attr('value');
	}).get().join('#');

	var selected_column_names = $('ul#sortable2 li').map(function(i,n) {
	    return $(n).attr('id');
	}).get().join('#');
	
	
	var unselected_columns = $('ul#sortable1 li').map(function(i,n) {
	    return $(n).attr('value');
	}).get().join('#');

	var unselected_column_names = $('ul#sortable1 li').map(function(i,n) {
	    return $(n).attr('id');
	}).get().join('#');
	
	var deposit=0;
	if (document.getElementById('chkDeposit').checked==true){
		deposit=1;
	}
	
	if (selected_columns==''){
		alert('No item selected.')
	}else{
		var filter=prompt("Filter name:","Filter1");
	
		if (filter!="" && filter!=null)
		  {
				jQuery.ajax({
					url: addFilterURL,
					type: 'POST',
					beforeSend: function() { $('#loader').show(); },
					data: {'filterName':filter,'sel_columnId':selected_columns,'sel_columnNames':selected_column_names,
						   'unsel_columnId':unselected_columns,'unsel_columnNames':unselected_column_names,
						   'account_id':document.getElementById('txtAccountID').value,'playertype':document.getElementById('txtPlayerType').value,'currency':document.getElementById('txtCurrency').value,'operator':document.getElementById('txtOperator').value,
						   'win_loss':document.getElementById('txtWinLoss').value,'country':document.getElementById('txtCountry').value,'email':document.getElementById('txtEmail').value,
						   'phone':document.getElementById('txtPhone').value,'deposit':deposit,'dateFrom':document.getElementById('txtDateFrom').value,'dateTo':document.getElementById('txtDateTo').value},
					context:'',
					success: function(data) {
						alert('New Filter has been saved.');
						$('#loader').hide();
						window.location.replace(window.location.href);
			    	}
				});
		  }
	}
}

function loadSavedFilters(val)
{
	jQuery.ajax({
		url: loadFilterURL,
		type: 'POST',
		data: {'filterName':val},
		beforeSend: function() { $('#loader').show(); },
		context:'',
		success: function(data) {
			var selectedField="";
			var unselectedField="";
			var splitData=data.split('|');
			//Split Selected Field fron the database
			var splitFilteredData=splitData[0].split('-');

			var selectedID = splitFilteredData[0].split('#');
			var selectedName = splitFilteredData[1].split('#');
			for (var i = 0; i < selectedID.length; i++) {
				selectedField=selectedField + '<li class="ui-state-default" value="'+selectedID[i]+'" id="'+selectedName[i]+'">'+selectedName[i]+'</li>';
			}
			//Split Unselected Field fron the database
			var splitUnselectedData=splitData[1].split('-');

			var unselectedID = splitUnselectedData[0].split('#');
			var unselectedName = splitUnselectedData[1].split('#');
			if (unselectedID[0]!=""){
				for (var i = 0; i < unselectedID.length; i++) {
					unselectedField=unselectedField + '<li class="ui-state-default" value="'+unselectedID[i]+'" id="'+unselectedName[i]+'">'+unselectedName[i]+'</li>';
				}
			}
			//Split params Field fron the database
			var splitParamsData=splitData[2].split('#');
			document.getElementById('txtAccountID').value=splitParamsData[0];
			document.getElementById('txtCurrency').value=splitParamsData[1];
			document.getElementById('txtOperator').value=splitParamsData[2];
			document.getElementById('txtWinLoss').value=splitParamsData[3];
			document.getElementById('txtCountry').value=splitParamsData[4];
			document.getElementById('txtEmail').value=splitParamsData[5];
			document.getElementById('txtPhone').value=splitParamsData[6];
			if (splitParamsData[7]==1){
				document.getElementById('chkDeposit').checked=true;
			}else{
				document.getElementById('chkDeposit').checked=false;
			}
			document.getElementById('txtDateFrom').value=splitParamsData[8];
			document.getElementById('txtDateTo').value=splitParamsData[9];
			
			
			document.getElementById('sortable1').innerHTML = '';
			document.getElementById('sortable2').innerHTML = '';
			document.getElementById('sortable1').innerHTML = unselectedField;
			document.getElementById('sortable2').innerHTML = selectedField;
			$('#loader').hide();
    	}
	});
	 
}


function clearFilters(){
	document.getElementById('txtAccountID').value='';
	document.getElementById('txtPlayerType').value='All';
	document.getElementById('txtCurrency').value='All';
	document.getElementById('txtWinLoss').value='';
	document.getElementById('txtCountry').value='All';
	document.getElementById('txtEmail').value='';
	document.getElementById('txtPhone').value='';
	document.getElementById('txtOperator').value=0;
	document.getElementById('chkDeposit').checked=false;
	document.getElementById('sortable1').innerHTML = '';
	document.getElementById('sortable2').innerHTML = '';
	document.getElementById('sortable1').innerHTML = '<li class="ui-state-default" value="opening_acc_date" id="Opening Account Date">Opening Account Date</li>'
													  +'<li class="ui-state-default" value="last_date_played" id="Last Date Played">Last Date Played</li>'
													  +'<li class="ui-state-default" value="account_id" id="Account ID">Account ID</li>'
													  +'<li class="ui-state-default" value="account_name" id="Account Name">Account Name</li>'
													  +'<li class="ui-state-default" value="win_loss" id="Win/Loss">Win/Loss</li>'
													  +'<li class="ui-state-default" value="country" id="Country">Country</li>'
													  +'<li class="ui-state-default" value="email" id="Email">Email</li>'
													  +'<li class="ui-state-default" value="phone_number" id="Phone">Phone</li>'
													  +'<li class="ui-state-default" value="currency_name" id="Currency">Currency</li>'
	 												  +'<li class="ui-state-default" value="first_deposit_date" id="First Deposit Date">First Deposit Date</li>';
}

function exportCurrentToExcel(){
	var dateFrom = $('#txtDateFrom')[0].value;
	var dateTo = $('#txtDateTo')[0].value;
	var accountId= $('#txtAccountID')[0].value;
	var playerType=$('#txtPlayerType')[0].value;
	var currency = $('#txtCurrency')[0].value;
	var winloss = $('#txtOperator')[0].value + ' '+ $('#txtWinLoss')[0].value;
	var country = $('#txtCountry')[0].value;
	var email = $('#txtEmail')[0].value;
	var phone = $('#txtPhone')[0].value;
	var deposit=0;
	if (document.getElementById('chkDeposit').checked==true){
		deposit=1;
	}
	var rowperpage = $('#pageSize')[0].value;
	
	var table1= document.getElementById('grid_report_wrapper');
	var html1 = table1.outerHTML;
	document.forms[2].csvBuffer.value="";
	document.forms[2].txtParams.value=$('.summary')[0].innerHTML+'#'+dateFrom+'#'+dateTo+'#'+accountId+'#'+playerType+'#'+currency+'#'+winloss+'#'+country+'#'+email+'#'+phone+'#'+deposit+'#'+rowperpage;
	document.forms[2].csvBuffer.value=html1 ;
    document.forms[2].method='POST';
    document.forms[2].action=exportCustomReportURL;  // send it to server which will open this contents in excel file
    document.forms[2].target='_top';
    document.forms[2].submit(); 
}

function exportAllToExcel(){
	$('#loader').show(); 
	//selected fields
	var selected_columns = $('ul#sortable2 li').map(function(i,n) {
	    return $(n).attr('value');
	}).get().join(',');

	var selected_column_names = $('ul#sortable2 li').map(function(i,n) {
	    return $(n).attr('id');
	}).get().join(',');
	//unselected fields
	var unselected_columns = $('ul#sortable1 li').map(function(i,n) {
	    return $(n).attr('value');
	}).get().join(',');

	var unselected_column_names = $('ul#sortable1 li').map(function(i,n) {
	    return $(n).attr('id');
	}).get().join(',');
	
	var deposit=0;
	if (document.getElementById('chkDeposit').checked==true){
		deposit=1;
	}
	
	if (selected_column_names==""){
		alert("No item selected.");
		$('#loader').hide(); 
		return false;
	}else{
		$('#columns').val(selected_columns);
		$('#column_names').val(selected_column_names);
		//document.getElementById('params').submit();
		//return true;
		
		jQuery.ajax({
			url: customReportUrl,
			type: 'POST',
			beforeSend: function() { $('#loader').show(); document.getElementById('btnExportAllPages').value='Please Wait';document.getElementById('btnExportAllPages').disabled=true;},
			data: {'columns':selected_columns,'column_names':selected_column_names,
				   'unselectedColumns':unselected_columns,'unselectedColumn_names':unselected_column_names,
				   'pageSize':document.getElementById('pageSize').value,'deposit':deposit,
				   'dateFrom':document.getElementById('txtDateFrom').value,'dateTo':document.getElementById('txtDateTo').value,
				   'accountID':document.getElementById('txtAccountID').value,'playertype':document.getElementById('txtPlayerType').value,'currency':document.getElementById('txtCurrency').value,
				   'operator':document.getElementById('txtOperator').value,'winloss':document.getElementById('txtWinLoss').value,
				   'country':document.getElementById('txtCountry').value,'email':document.getElementById('txtEmail').value,
				   'phone':document.getElementById('txtPhone').value,'allPages':999999},
			context:'',
			success: function(data) {
				var report= data.split('<!--grid report-->');
				var report1=report[1].split('<!--end grid report-->');
				document.getElementById('grid_report_wrapper').innerHTML = report1[0];
				
				var dateFrom = $('#txtDateFrom')[0].value;
				var dateTo = $('#txtDateTo')[0].value;
				var accountId= $('#txtAccountID')[0].value;
				var playertype=$('#txtPlayerType')[0].value;
				var currency = $('#txtCurrency')[0].value;
				var winloss = $('#txtOperator')[0].value + ' '+ $('#txtWinLoss')[0].value;
				var country = $('#txtCountry')[0].value;
				var email = $('#txtEmail')[0].value;
				var phone = $('#txtPhone')[0].value;
				var deposit=0;
				if (document.getElementById('chkDeposit').checked==true){
					deposit=1;
				}
				var rowperpage = $('#pageSize')[0].value;
				
				document.forms[2].csvBuffer.value="";
				document.forms[2].txtParams.value=$('.summary')[0].innerHTML+'#'+dateFrom+'#'+dateTo+'#'+accountId+'#'+playertype+'#'+currency+'#'+winloss+'#'+country+'#'+email+'#'+phone+'#'+deposit+'#'+rowperpage;
				document.forms[2].csvBuffer.value=report1[0] ;
			    document.forms[2].method='POST';
			    document.forms[2].action=exportCustomReportURL;  // send it to server which will open this contents in excel file
			    document.forms[2].target='_top';
			    document.forms[2].submit(); 
			    $('#loader').hide();
			    document.getElementById('btnExportAllPages').disabled=false;
			    document.getElementById('btnExportAllPages').value='Export All Pages';
	    	}
		});
	}
}