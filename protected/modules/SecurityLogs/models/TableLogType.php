<?php
/**
 * @todo bind tbl_log_type to TableGame CActiveRecord
 * @copyright CE
 * @author Allan
 * @since 2013-04-12
 */
class TableLogType extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_log_type';
	}
}

