//active menu color
document.getElementById('systemmonitorHeader').className="start active";
document.getElementById('mnu_instant_pools').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>System Monitor <i class='icon-angle-right'></i></li><li><a href='#'>Instant Pools</a></li>");

function betHighlight(){
	setInterval(function(){
		$(".instant1 td").each(function() {
			var cell = $(this).closest('td');
			var cellIndex = cell[0].cellIndex
			
			var val = parseInt(this.innerHTML,10);
		    if (cellIndex < 8){
		    	if (val > 0) {
		    		this.style.color = "#003366";
		    		this.style.fontWeight="bold";
		    	}
		   	}
		    if (cellIndex == 9){
		    	if (val < 0) {
		    		this.style.color = "red";
		    		this.style.fontWeight="bold";
		    	}
		   	}
		});
	},100);

}
function btnMB03(){
//	document.getElementById('mb01').style.backgroundColor='#D2D2D2';
//	document.getElementById('mb02').style.backgroundColor='#D2D2D2';
//	document.getElementById('mb03').style.backgroundColor='#808080';
//	document.getElementById('mb03').style.color='white';
//	document.getElementById('rl01').style.backgroundColor='#D2D2D2';
//	document.getElementById('dt01').style.backgroundColor='#D2D2D2';
//	document.getElementById('all').style.backgroundColor='#D2D2D2';
	$.ajax({
		 url:urlInstantPoolsMB03,
		 async:true,
		 success: function(result) {
			 document.getElementById('qry_result_mb_03').innerHTML=result;
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
	getPoolsResult();
	betHighlight();
	return false;
}

var requestSent, timeout;
var processing=false;
function getPoolsResult(){
	timeout = setInterval(function(){
        if (!processing)
       {
        processing=true;
		requestSent=$.ajax({
			 url:urlInstantPoolsMB03,
			 async:true,
			 success: function(result) {
				 processing=false;
				 document.getElementById('qry_result_mb_03').innerHTML=result;
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown) { 
				 processing=false;
			      
		 }});
       }
    }, 8000
);
}


function showMB02() 
{
	window.location=urlMB02;
}
function showMB01() 
{
	window.location=urlMB01;
}
function showRL01() 
{
	window.location=urlRL01;
}
function showDT01() 
{
	window.location=urlDT01;
}
function showALL() 
{
	window.location=urlALL;
}