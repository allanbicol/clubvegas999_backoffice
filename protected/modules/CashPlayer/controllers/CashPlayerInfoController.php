<?php
class CashPlayerInfoController extends MyController
{
	public function actionPlayerInfo()
	{
		
		$cp = new CashPlayer;
		
		$page = $_POST['page'];
		$limit = $_POST['rows'];
		$orderField = $_POST['sidx'];
		$sortType = $_POST['sord'];
		
		$result=$cp->getCountPlayerInfo();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cp->getPlayerInfo($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("id","account_id","account_name","account_type","afftoken","currency_name","country","email","phone_number","security_num","value_score","sex","dob","address","zip_code","city","opening_acc_date","survey_name","status_name","bank_account_number","bank_account_name","bank_used","bank_branch","skrill_email","neteller_account");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records, $total_pages, $page, $records,$filedNames,$htmvalue,7);
			
	}
	public function actionExportAllPlayerInfo()
	{
	
		$cp = new CashPlayer;
		$result=$cp->getCountPlayerInfo();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
		$player_records = $cp->getExporAllPlayerInfo();
		//$test = $records->readAll();
		$fieldNames = array("id","account_id","account_name","account_type","currency_name","country","email","phone_number","security_num","value_score","sex","dob","address","zip_code","city","opening_acc_date","survey_name","status_name","bank_account_number","bank_account_name","bank_used","bank_branch","skrill_email","neteller_account");
		$headerNames =array("id","Account Id","Account Name","Type","Currency","Country","Email","Phone Number","Security Number","Value Score","Sex","Date of Birth","Address","Zip Code","City","Opening Acc Date","Player Survey","Status","Bank Account Number","Bank Account Name","Bank Used","Bank Branch","Skrill Email","Neteller Account");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
			
	}
	public function actionPlayerInformation()
	{
		$playerInformation=new CashPlayer();
		$playerInformation->getPlayerInformation();
	}
	public function actionPlayerInformationById()
	{
		$playerInformationById=new CashPlayer();
		$playerInformationById->getPlayerInformationById();
	}
	
	public function actionAssignValueScore(){
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerInfo'))
		{
			$connection = Yii::app()->db_cv999_fd_master;
			$command1 = $connection->createCommand("SELECT email from tbl_cash_player where email='".$_POST['email']."' and account_id <>'".$_POST['accountId']."'");
			$rd1=$command1->queryRow();
			if($rd1['email']==$_POST['email']){
				exit("dup");
			}
			
			if (is_numeric($_POST['valueScore'])){
				if ($_POST['valueScore']<0 || $_POST['valueScore']>100 ){
					echo('0');
				}else{
					$command = $connection->createCommand("SELECT email,sex,address,city,phone_number,country,zip_code,value_score from tbl_cash_player where account_id='".$_POST['accountId']."'");
					$rd=$command->queryRow();
					$email=$rd['email'];
					$sex=$rd['sex'];
					$address=$rd['address'];
					$city=$rd['city'];
					$phone=$rd['phone_number'];
					$country=$rd['country'];
					$zip=$rd['zip_code'];
					$valueScore=$rd['value_score'];
					$dateTime=date('Y-m-d H:i:s');
					
					TableCashPlayer::model()->updateAll(array(
										'email'=>$_POST['email'],
										'sex'=>$_POST['sex'],
										'address'=>$_POST['address'],
										'city'=>$_POST['city'],
										'phone_number'=>$_POST['phoneNumber'],
										'country'=>$_POST['country'],
										'zip_code'=>$_POST['zipCode'],
										'value_score'=>$_POST['valueScore']),
										'account_id="'.$_POST['accountId'].'"');
					if ($email!=$_POST['email']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$email,$_POST['email'],'email');
					}
					if ($sex!=$_POST['sex']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$sex,$_POST['sex'],'sex');
					}
					if ($address!=$_POST['address']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$address,$_POST['address'],'address');
					}
					if ($city!=$_POST['city']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$city,$_POST['city'],'city');
					}
					if ($phone!=$_POST['phoneNumber']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$phone,$_POST['phoneNumber'],'phone number');
					}
					if ($country!=$_POST['country']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$country,$_POST['country'],'country');
					}
					if ($zip!=$_POST['zipCode']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$zip,$_POST['zipCode'],'zip code');
					}
					if ($valueScore!=$_POST['valueScore']){
						self::actionSaveLog(Yii::app()->session['account_id'],Yii::app()->session['level_name'],$_POST['accountId'],'Cash Player',$valueScore,$_POST['valueScore'],'value score');
					}
					
					
					echo('Information saved successfully.');
				}
			}else{
				echo('Invalid input.');
			}
		}
		else
		{
			echo "You don't have permission to update the setting!";
		}
	}
	
	public function actionSaveLog($operator_id,$operator_level,$account_id,$account_id_level,$change_before,$change_after,$change_name)
	{
		$dateTime=date('Y-m-d H:i:s');
		$postLog = new TableLog;
		$postLog->operated_by = $operator_id;
		$postLog->operated_by_level = $operator_level;
		$postLog->operated = $account_id;
		$postLog->operated_level =$account_id_level;
		$postLog->operation_time = $dateTime;
		$postLog->log_type_id = 24;
		$postLog->log_details = '<b>'.$operator_level.' <label style=\"color:#7A5C00\">'.$operator_id.'</label> change '.$change_name.' from:<label style=\"color:red\">'.$change_before.'</label> to: <label style=\"color:green\">'.$change_after.'</label></b>';
		$postLog->save();
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerInfo'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	} 
	
	
	
}