<?php
class WinnerListController extends MyController
{
	public function actionWinnerList()
	{
		$model_name = new WinnerList();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$model_name->countWinnerList();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $model_name->getWinnerList($orderField, $sortType, $startIndex, $limit);
		
		$filedNames = array("id","account_id","game_type","casino_name","winning_amount","enable_winning","operation");
		$urlAS= Yii::app()->request->baseUrl;
		
		$htmvalue='<a class=\'btn mini red\' href=\'index.php?r=SystemSetting/WinnerList/WinnerList&Account_ID=\' onclick=\'javascript:openAlterDialog(this); return false;\'>Edit <i class=\'icon-edit\'></i></a> <a class=\'btn mini red\' href=\'index.php?r=SystemSetting/WinnerList/WinnerList&Account_ID=\' onclick=\'javascript:openDeleteDialog(this); return false;\'>Delete <i class=\'icon-trash\'><//i></a>';
		
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	}
	
	public function actionUpdateWinnerList() {
		//START: $_POST['is_checked'] validation
		if(Yii::app()->user->checkAccess('systemSetting.writeWinnerList'))
		{
			if($_POST['is_checked']!='true' && $_POST['is_checked']!='false'){
				exit('Invalid white_list value!');
			}
			
			if(trim($_POST['account_id'])==''){
				exit('Invalid account_id value!');
			}
			
			
			$log_details='';
			
			if($_POST['is_checked']=='true'){
				$is_checked=1;
				$msg='SC '. $_POST['account_id'] . ' were successfully enabled in the winner list.';
				$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> enabled <label style=\"color:green\">Cash Player</label> <label style=\"color:red\">'.$_POST['account_id'].'</label> to Winner List.</b>';
			}else{
				$is_checked=0;
				$msg='SC '. $_POST['account_id'] . ' were successfully disabled in the winner list.';
				$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> disabled <label style=\"color:green\">Cash Player</label> <label style=\"color:red\">'.$_POST['account_id'].'</label> from Winner List.</b>';
			}
			TableLatestWinner::model()->updateAll(array('enable_winning'=>$is_checked),'account_id = "'.$_POST['account_id'].'"');
			
			// save logs
			$logs = new SystemSetting();
			$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'], $_POST['account_id'], 'WL_Cash Player', 22, $log_details);
			
		}else{
			$msg='You dont have permission to do this operation.';
		}
		echo $msg;
	}
	
	public function actionUpdateWinner() {
		if(Yii::app()->user->checkAccess('systemSetting.writeWinnerList'))
		{
			if($_POST['isEnable']!=1 && $_POST['isEnable']!=0){
				exit('Invalid white_list value!');
			}
		
			if(trim($_POST['accountID'])==''){
				exit('Invalid account_id value!');
			}
			
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("Select id,account_id,game_type,casino_name,winning_amount,enable_winning from tbl_latest_winner_list where id='".$_POST['id']."'");
			$rows = $command->queryRow();
	
			$account_id= $rows['account_id'];
			$game_type= $rows['game_type'];
			$casino_name= $rows['casino_name'];
			$winning_amount= $rows['winning_amount'];
			$isEnable= $rows['enable_winning'];
			
			TableLatestWinner::model()->updateAll(array('account_id'=>$_POST['accountID'],'game_type'=>$_POST['gameName'],'casino_name'=>$_POST['casinoName'],'winning_amount'=>$_POST['winningAmount'],'enable_winning'=>$_POST['isEnable']),'id = "'.$_POST['id'].'"');
		
			$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Change winning player details <br/>from:<br/><label style=\"color:green\">ID='.$account_id.', Game='.$game_type.', Casino:'.$casino_name.', Amount:'.$winning_amount.', Enable:'.$isEnable.'</label><br/>To:<br/><label style=\"color:red\"> ID:'.$_POST['accountID'].', Game:'.$_POST['gameName'].', Casino:'.$_POST['casinoName'].', Amount'.$_POST['winningAmount'].', Enable:'.$_POST['isEnable'].'</label></b>';
			
			// save logs
			$logs = new SystemSetting();
			$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'], $_POST['accountID'], 'WL_Cash Player', 22, $log_details);
			
			$msg='suc';
		}else{
			$msg='err';
		}
		echo $msg;
	}
	
	public function actionDeleteWinner() {
		if(Yii::app()->user->checkAccess('systemSetting.writeWinnerList'))
		{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("Select account_id from tbl_latest_winner_list where id='".$_POST['id']."'");
			$rows = $command->queryRow();
		
			$account_id= $rows['account_id'];
			
			TableLatestWinner::model()->deleteAll(array('condition'=>'id=:id',
					'params'=>array(':id'=>$_POST['id'])));
			
			$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Delete winning player <label style=\"color:green\">'.$account_id.'</label></b>';
		
			// save logs
			$logs = new SystemSetting();
			$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'],$account_id, 'WL_Cash Player', 22, $log_details);
		
			$msg='Winning player '.$account_id.' successfully deleted.';
		}else{
			$msg='You dont have permission to do this operation.';
		}
		echo $msg;
	}
	public function actionGetWinnerDetails()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select id,account_id,game_type,casino_name,winning_amount,enable_winning from tbl_latest_winner_list where id='".$_POST['id']."'");
		$rows = $command->queryRow();
		
		$id=$rows['id'];
		$account_id= $rows['account_id'];
		$game_type= $rows['game_type'];
		$casino_name= $rows['casino_name'];
		$winning_amount= $rows['winning_amount'];
		$isEnable= $rows['enable_winning'];
	
		echo $id.'#'.$account_id.'#'.$game_type.'#'.$casino_name.'#'.$winning_amount.'#'.$isEnable;
	}
	
	public function actionGetGameName()
	{
		$htmlCode='';
		$dataReader = TableGame::model()->findAll(array('condition'=>'id<>:id',
				'params'=>array(':id'=>0),'order'=>'id'));
		foreach ($dataReader as $row){
			$htmlCode.= $row['game_name'] . ':' . $row['game_name'] . ',';
		}
		echo $htmlCode;
	}
	public function actionGetCasinoName()
	{
		$htmlCode='';
		$dataReader = TableCasino::model()->findAll(array('condition'=>'id<>:id',
				'params'=>array(':id'=>0 ),'order'=>'casino_name'));
		foreach ($dataReader as $row){
			$htmlCode.= $row['casino_name'] . ':' . $row['casino_name'] . ',';
		}
		echo $htmlCode;
	}
	
	public function actionSaveWinner()
	{
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
	
		$postLog=new TableLog;
		$postLatestWinner=new TableLatestWinner;
		$dateTime = date('Y-m-d H:i:s');
		if(Yii::app()->user->checkAccess('systemSetting.writeWinnerList'))
		{
				$postLatestWinner->account_id=$_POST['accountID'];
				$postLatestWinner->game_type=$_POST['gameName'];
				$postLatestWinner->casino_name=$_POST['casinoName'];
				$postLatestWinner->winning_amount=$_POST['winningAmount'];
				$postLatestWinner->enable_winning=$_POST['isEnable'];
				$postLatestWinner->save();
				
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_POST['accountID'];
				$postLog->operated_level='Winner';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=22;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> added new winner <label style=\"color:red\">'.$_POST['accountID'].'</label> to winner list with the amount of <label style=\"color:red\">'.$_POST['winningAmount'].'</label></b>';
				$postLog->save();
	
				echo 'suc';

		}else{
			echo 'err';
		}
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readWinnerList')){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
}