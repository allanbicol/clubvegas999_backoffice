<?php
/**
 * @todo bind tbl_locked_user_auth to Level CActiveRecord
 * @copyright CE
 * @author Allan
 * @since 2013-04-09
 */
class TableLockedUser extends CActiveRecord //CV99ActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_locked_user_auth';
	}
}