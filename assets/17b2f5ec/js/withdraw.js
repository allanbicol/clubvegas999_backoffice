var isProcessingWithdrawal = false;

function checkPlayerOnlineStatWithdraw(aElement){
	
	if(isProcessingWithdrawal){
       return;
    }
	var addUrl=aElement.toString();
	var acountID = addUrl.split("=");
	
	isProcessingWithdrawal = true;
	jQuery.ajax({
	url: baseUrl+'/index.php?r=CashPlayer/CashPlayerWithdraw/Withdraw',
	type: 'POST',
	data: {'task': 'checkPlayerOnlineStat', 'accountID' :acountID[2]},
	context: '',
	async: false,
	success: function(data){
		isProcessingWithdrawal = false;
		if (data!='die'){
			if (data==0){
				var urlLoadFormWithdraw=baseUrl+'/index.php?r=CashPlayer/CashPlayerList/LoadFormWithdraw';
				$('#viewWithdraw').load(urlLoadFormWithdraw + '&account_id='+ acountID[2],
    			function(response, status, xhr) {		    	
	  				if (status == "success") {			    		  									
		  				$("#withdraw-dialog").dialog("open"); return false;
		  			}else{
	  					$('#viewWithdraw').load('<p style="color:red;">Something wrong during request!!!</p>');
	  					$("#withdraw-dialog").dialog("open"); return false;
		  			}
	  			});
				return false;
			}
			else{

				if(data=='w'){
					$createWaitWithdrawProcessDialog.dialog('open');
					$(".ui-dialog-titlebar-close").hide();
					var check =setInterval(function(){
					jQuery.ajax({
						url: baseUrl+'/index.php?r=CashPlayer/CashPlayerWithdraw/ProcessWithdrawalRequest',
						type: 'POST',
						data: {'accountID' : acountID[2]},
						async: false,
						context: '',
						success: function(data){
							isProcessingWithdrawal = false;
							if (data==0){ // withdrawal successfully
								$('#list2').trigger("reloadGrid");// reload grid
								$(".ui-dialog-titlebar-close").show();
								$createWaitWithdrawProcessDialog.dialog('close');
								var urlLoadFormWithdraw=baseUrl+'/index.php?r=CashPlayer/CashPlayerList/LoadFormWithdraw';
								$('#viewWithdraw').load(urlLoadFormWithdraw + '&account_id='+ acountID[2],
				    			function(response, status, xhr) {		    	
					  				if (status == "success") {			    		  									
						  				$("#withdraw-dialog").dialog("open"); return false;
						  			}else{
					  					$('#viewWithdraw').load('<p style="color:red;">Something wrong during request!!!</p>');
					  					$("#withdraw-dialog").dialog("open"); return false;
						  			}
					  			});
								clearInterval(check);
								return false;
							}
							else if(data=='h'){
								$createWaitWithdrawProcessDialog.dialog('close');
								alert('Cannot connect to HTV withdrawal API Server.Please try again!.');
								clearInterval(check);
								return false;
							}else if(data=='s'){
								$createWaitWithdrawProcessDialog.dialog('close');
								alert('Cannot connect to Savan withdrawal API Server.Please try again!.');
								clearInterval(check);
								return false;
							}
							
							
							
							else if(data=='vc'){
								$createWaitWithdrawProcessDialog.dialog('close');
								alert('Cannot connect to VirtualVegas withdrawal API Server.Please try again!.');
								clearInterval(check);
								return false;
							}
							else if(data=='svs'){
								$createWaitWithdrawProcessDialog.dialog('close');
								alert('Cannot connect to SlotVegas withdrawal API Server.Please try again!.');
								clearInterval(check);
								return false;
							}

				    	}
					});
				},2000);							
				
				}
				else{
					// force logout player on the lobby
					forceLogOutDialog(acountID[2]);
				}
			}
		}else{
			window.location=baseUrl+"/index.php?r=Login";
		}
	}
	});
}

function checkAmountToWithdraw(item){
	//var balance = document.getElementById("balanceAmountWithdraw").value;
	var amount = document.getElementById("txtWithdrawAmount").value;

		var pattern = /^[0-9]+(.[0-9]{1,2})?$/; 
		var text = document.getElementById('txtWithdrawAmount').value;
		//level='<?php echo Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerWithdraw'); ?>';
	    if (text.match(pattern)==null) 
	    {
			alert('Invalid value entered.');
			document.getElementById("btnSubmitWithdraw").disabled = false;
			return false;
	    }
		else
		{
			if ($('#txtTransItem').val()==""){
				alert('Select Withdrawal type.');
				document.getElementById("btnSubmitWithdraw").disabled = false;
				return false;
			}
			
			if (parseFloat(document.getElementById('txtWithdrawAmount').value)==0)
			{
				alert('Cannot withdraw zero amount.');
				document.getElementById("btnSubmitWithdraw").disabled = false;
				return false;
			}else{

					confirmWithdraw(item);
			}
		}
	//}
}

function confirmWithdraw(withdrawType)
{
	jQuery.ajax({
		url: baseUrl+'/index.php?r=CashPlayer/CashPlayerWithdraw/Withdraw',
		type: 'POST',
		async:false,
		data: {'task':'confirmWithdraw',
			   'accountIDWithdraw': document.getElementById('txtAccountId').value, 
    		   'AmountWithdraw' :document.getElementById('txtWithdrawAmount').value,
    		   'txtTransItem' :withdrawType
    		   },
		context: '',
		async:false,
		success: function(data) {
			$("#withdraw-dialog").dialog("close");
			alert(data);
			$('#list2').trigger("reloadGrid");
    	},
   	    	
    	error: function (request, status, error) {
    		        alert(request.responseText);
    	}
    			   	    	
	});
}
function validateWithdraw()
	{
		var pattern = /^[0-9]+(.[0-9]{1,2})?$/; 
		var text = document.getElementById('txtWithdrawAmount').value;
		if (parseFloat(document.getElementById('txtWithdrawAmount').value)==0)
		{
			alert('Invalid value entered.');
			document.getElementById("btnSubmitWithdraw").disabled = false;
			return false;
		}

	    if (text.match(pattern)==null) 
	    {
			alert('Invalid value entered.');
			document.getElementById("btnSubmitWithdraw").disabled = false;
			return false;
	    }
		else
		{
			return true;
		}
		
	    
}