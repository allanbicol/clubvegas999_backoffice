<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentwinloss.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/settlementwinloss.js"></script>
	<script type="text/javascript">
		var baseURL = '<?php echo Yii::app()->request->baseUrl;?>';
		var urlHTV999WinLossDetailsSummaryFooter = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTV999WinLossDetailsSummaryFooter';
		var urlSAVANWinLossBettingDetailsSummaryFooter = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANWinLossBettingDetailsSummaryFooter';
		var urlWinLossArrow = '<?php echo $this->module->assetsUrl; ?>';
		var urlWinLossResultDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasWinLossResultDetails';
		var urlHTVWinLossResultDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HatienVegasWinLossResultDetails';
		var urlSAVANWinLossResultDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SavanVegasWinLossResultDetails';
		var urlCostaVegasTotal = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasTotal';
		var lblMainCompany = '<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		var lblSubCompany = '<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
		var lblSeniorMasterAgent = '<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
		var lblMasterAgent = '<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
		var lblAgent = '<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
		var lblSubCompanyTotal = '<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
		var lblSeniorMasterAgentTotal = '<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
		var lblMasterAgentTotal = '<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
		var lblAgentTotal = '<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
		var lblNumber = '<?php echo Yii::t('agent','agent.agentwinloss.number');?>';
		var lblCurrency = '<?php echo Yii::t('agent','agent.agentwinloss.currency');?>';
		var lblValidStake = '<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>';
		var lblWinLoss = '<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>';
		var lblCommission = '<?php echo Yii::t('agent','agent.agentwinloss.commission');?>';
		var lblTips = '<?php echo Yii::t('agent','agent.agentwinloss.tips');?>';
		var lblTotal = '<?php echo Yii::t('agent','agent.agentwinloss.total');?>';
		var lblCompany = '<?php echo Yii::t('agent','agent.agentwinloss.company');?>';
		var captionCostaVegasWinLossTotal = "<font color='#ab0e10'><label id='lblCostaTotalWinLossID'></label></font> COSTA VEGAS TOTAL <select id='currencyTypeConvertCOSTA' onchange='javascript: costaTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select> <label style='<?php if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=9){echo 'display: inline;';}else{echo 'display: none;';}?>'>VIG Sharing:</label> <input type='text' id='txtVIGsharing1' onkeypress='javascript: return checkIt(event,\"txtVIGsharing\"); ' onKeyup='javascript:copyVIGValue(this.value);' style='<?php if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=9){echo 'display: inline;';}else{echo 'display: none;';}?>'><label style='<?php if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=9){echo 'display: inline;';}else{echo 'display: none;';}?>'>%</label>";
		var urlCostaVegasSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasSummaryWinLoss';
		var urlExportAllCostaVegasSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllCostaVegasSummaryWinLoss';
		var lblAccountId = '<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>';
		var lblAccountName = '<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>';
		var lblSharingCommission = '<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>';
		var lblAverageBet = '<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>';
		var urlAgentSharingAndCommission = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SharingAndCommission';
		var captionCostaVegasWinLoss = "<label id='lblAgentID'></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_title');?> <label id='lblDateFrom_costa'></label> - <label id='lblDateTo_costa'></label> <input type='button' id='btnBack_costa' value='<<' onclick='backToParent(document.getElementById(\"lblAgentID\").innerHTML);'> <select id='currencyTypeConvertCOSTAWinLoss' onchange='javascript: costaWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var urlCostaVegasSummaryMemberWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasSummaryMemberWinLoss';
		var captionCostaVegasMemberWinLoss = "<label id='lblAgentID'></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_title');?> <label id='lblDateFrom_costa'></label> - <label id='lblDateTo_costa'></label> <input type='button' value='<<' id='btnBack_costa' onclick='backToParent(document.getElementById(\"lblAgentID\").innerHTML);'> <select id='currencyTypeConvertCOSTAWinLoss' onchange='javascript: costaMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var lblPlayer = '<?php echo Yii::t('agent','agent.agentwinloss.player');?>';
		var lblBalance = '<?php echo Yii::t('agent','agent.agentwinloss.balance');?>';
		var urlCostaVegasWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasWinLossDetails';
		var captionCostaVegasWinLossDetails = "<label id='lblDetailAgentID'></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_winloss_details');?> <label id='lblDetailDateFrom_costa_details'></label> - <label id='lblDetailDateTo_costa_details'></label> <input type='button' id='btnDetailBack' value='<<' onclick='backToParentDetail(document.getElementById(\"lblDetailAgentID\").innerHTML);'>";
		var urlCostaVegasMemberWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasMemberWinLossDetails';
		var urlExportAllCostaVegasMemberWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllCostaVegasMemberWinLossDetails';
		var lblBetTime = '<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>';
		var lblGameType = '<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>';
		var lblCasino = '<?php echo Yii::t('agent','agent.agentwinloss.casino');?>';
		var cationMemberWinLossCostaVegasDetails = '<b><label id="lblBettingDetailAgentID"></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_details_title');?></b> <label id="lblDDateFrom"></label> - <label id="lblDDateTo"></label> <input type="button" value="<<" onclick="hideBettingDetails();">';
		var cationMemberWinLossCostaVegasDetails1 = '<b><label id="lblBettingDetailAgentID"></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_details_title');?></b> <label id="lblDDateFrom_details"></label> - <label id="lblDDateTo_details"></label> <input type="button" value="<<" onclick="hideBettingDetails1();">';
		var urlHTV999Total = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTV999Total';
		var lblTotalStake = '<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>';
		var cationHTVWinLossTotal = "<font color='#ab0e10'><label id='lblHTVTotalWinLossID'></label></font> HTV TOTAL <select id='currencyTypeConvertHTV' onchange='javascript: htvTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var urlHTVSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTVSummaryWinLoss';
		var lblBetCount = '<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>';
		var captionHTVWinLoss = "<label id='lblAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='lblDateFrom_htv'></label> - <label id='lblDateTo_htv'></label> <input type='button' id='btnBack_htv' value='<<' onclick='backToParent_htv(document.getElementById(\"lblAgentID_htv\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: htvWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var urlHTVSummaryMemberWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTVSummaryMemberWinLoss';
		var captionHTVMemberWinLoss = "<label id='lblAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='lblDateFrom_htv'></label> - <label id='lblDateTo_htv'></label> <input type='button' value='<<' id='btnBack_htv' onclick='backToParent_htv(document.getElementById(\"lblAgentID_htv\").innerHTML);'> <input type='button' id='btnDetails_htv' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_htv(document.getElementById(\"lblAgentID_htv\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: htvMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var urlHTVWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTVWinLossDetails';
		var captionHTVWinLossDetails = "<label id='lblDetailAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_winloss_details');?> <label id='lblDetailDateFrom_htv'></label> - <label id='lblDetailDateTo_htv'></label> <input type='button' id='btnDetailBack_htv' value='<<' onclick='backToParentDetail_htv(document.getElementById(\"lblDetailAgentID_htv\").innerHTML);'>";
		var urlHTV999WinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTV999WinLossDetails';
		var lblTableShoeGame = "<?php echo Yii::t('agent','agent.agentwinloss.table_shoe_game');?>";
		var lblBetAmount = "<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>";
		var lblValidAmount = '<?php echo Yii::t('agent','agent.agentwinloss.valid_amount');?>';
		var lblResult = "<?php echo Yii::t('agent','agent.agentwinloss.result');?>";
		var lblBalanceBefore = "<?php echo Yii::t('agent','agent.agentwinloss.balance_before');?>";
		var lblBalanceAfter = "<?php echo Yii::t('agent','agent.agentwinloss.balance_after');?>";
		var captionMemberWinLossHTV999details = '<b><label id="lblBettingDetailAgentID_htv"></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_betting_details_title');?></b> <label id="lblDDateFrom_htv"></label> - <label id="lblDDateTo_htv"></label> <input type="button" value="<<" onclick="hideBettingDetails_htv();">';
		var urlSAVANTotal = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANTotal';
		var captionSAVANWinLossTotal = "<font color='#ab0e10'><label id='lblSAVANTotalWinLossID'></label></font> SAVANVEGAS TOTAL <select id='currencyTypeConvertSAVAN' onchange='javascript: savanTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var urlSAVANSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANSummaryWinLoss';
		var urlExportAllSAVANSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllSAVANSummaryWinLoss';
		var captionSAVANWinLoss = "<label id='lblAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_savan'></label> - <label id='lblDateTo_savan'></label> <input class='btn red' type='button' id='btnBack_savan' value='<<' onclick='backToParent_savan(document.getElementById(\"lblAgentID_savan\").innerHTML);'> <select id='currencyTypeConvertSAVANWinLoss' onchange='javascript: savanWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var urlSAVANSummaryMemberWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANSummaryMemberWinLoss';
		var captionSAVANMemberWinLoss = "<label id='lblAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_savan'></label> - <label id='lblDateTo_savan'></label> <input class='btn red' type='button' value='<<' id='btnBack_savan' onclick='backToParent_savan(document.getElementById(\"lblAgentID_savan\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: savanMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var urlSAVANWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANWinLossDetails';
		var captionSAVANWinLossDetails = "<label id='lblDetailAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_winloss_details');?> <label id='lblDetailDateFrom_savan'></label> - <label id='lblDetailDateTo_savan'></label> <input type='button' id='btnDetailBack_savan' value='<<' onclick='backToParentDetail_savan_details(document.getElementById(\"lblDetailAgentID_savan\").innerHTML);'>";
		var urlSAVANWinLossBettingDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANWinLossBettingDetails';
		var urlExportAllSAVANWinLossBettingDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllSAVANMemberWinLossDetails';
		var lblValidBet = "<?php echo Yii::t('agent','agent.agentwinloss.valid_bet');?>";
		var captionMemberWinLossSavanVegasDetails = '<b><label id="lblBettingDetailAgentID_savan"></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_betting_details_title');?></b> <label id="lblDDateFrom_savan"></label> - <label id="lblDDateTo_savan"></label> <input class="btn red" type="button" value="<<" onclick="hideBettingDetails_savan();">';
		var captionMemberWinLossSavanVegasDetails1 = '<b><label id="lblBettingDetailAgentID_savan"></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_betting_details_title');?></b> <label id="lblDDateFrom_savan_details"></label> - <label id="lblDDateTo_savan_details"></label> <input class="btn red"  type="button" value="<<" onclick="hideBettingDetails_savan1();">';
		var sessionLevel = '<?php echo Yii::app()->session['level'];?>';
		var sessionAccountId = '<?php echo Yii::app()->session['account_id'];?>';
		var account_type='<?php echo User::getUserType();?>';
		var dtToday='<?php echo date("Y-m-d");?>';
		var dtYesterday='<?php echo date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d"))));?>';
		var readCostavegasWinLoss = '<?php echo Yii::app()->user->checkAccess('agent.readCostaWinLoss');?>';
		var readHatienvegasWinLoss = '<?php echo Yii::app()->user->checkAccess('agent.readHTVWinLoss');?>';
		var readSavanvegasWinLoss = '<?php echo Yii::app()->user->checkAccess('agent.readSavanWinLoss');?>';
		var urlCostaVegasMemberWinLossDetailsSummaryFooter = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasMemberWinLossDetailsSummaryFooter';
		var lblIpAddress = '<?php echo Yii::t('agent','agent.agentwinloss.ip_address');?>';;
	</script>
	
</head>
<body>
	<div id="parameter_area">
		<div class="header"><font color="#f0e62b"><?php echo Yii::app()->session['account_id'];?></font> <?php echo Yii::t('agent','agent.agentwinloss.agent_winloss');?></div>
		<div class="body">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.game');?></td>
			<td class="right">
				<select name="cmbGame" id="cmbGame" class="cmb">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. strtoupper($row['game_name']) . '</option>';
					}
					?>
				</select>
			</td></tr>
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.currency');?></td>
			<td class="right">
				<select name="cmbCurrency" id="cmbCurrency" class="cmb" onchange="if(this.value!=''){document.getElementById('chkTestCurrency').style.display='none';document.getElementById('lblTestCurrency').style.display='none';}else{document.getElementById('chkTestCurrency').style.display='inline';document.getElementById('lblTestCurrency').style.display='inline';}">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableCurrency::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['currency_name'] . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>
				<input type="checkbox" id="chkTestCurrency" checked/><label id="lblTestCurrency" for="chkTestCurrency"><?php echo Yii::t('agent','agent.agentwinloss.except_test_currency');?></label>
			</td></tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.from');?></td>
				<td class="right">
					<input type="text" id="dtFrom" value="<?php echo date("Y-m-d");?>">
					<select id="cmbTime1">
					<?php 
						for($i=0;$i<=23;$i++){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					?>
					</select> :00:00
				</td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.to');?></td>
				<td class="right">
					<input type="text" id="dtTo" value="<?php echo date("Y-m-d");?>">
					<select id="cmbTime2">
					<?php 
						for($i=0;$i<=23;$i++){
							if($i<23){
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}else{
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}
						}
					?> 
					</select> :59:59
				</td>
			</tr>
			<tr>
				<td class="left">Account ID</td>
				<td class="right"><input type="text" id="txtAccountID"></td>
			</tr>
			<tr>
				<td class="left">Casino</td>
				<td class="right">
				<?php 
					// Costavegas
					if (Yii::app()->user->checkAccess('agent.readCostaWinLoss') ||	User::getUserType() === 'agent'){
						echo '<input type="checkbox" id="chkVIG" checked><label id="lblVIG" for="chkVIG">Costavegas999</label>';
					}else{ // for provider role
						echo '<input type="checkbox" id="chkVIG" style="display:none;"><label id="lblVIG" for="chkVIG" style="display:none;">Costavegas999</label>';
						echo '<input type="hidden" id="lblAgentID"><input type="hidden" id="lblDateFrom_costa"><input type="hidden" id="lblDateTo_costa"><input type="hidden" id="btnBack_costa">';
					}
					
					// Hatienvegas enable or disable HTV999 
// 					if (Yii::app()->user->checkAccess('agent.readHTVWinLoss') ||	User::getUserType() === 'agent'){
// 						echo '<input type="checkbox" id="chkHTV" checked><label for="chkHTV">Hatienvegas999</label> ';
// 					}else{
						//echo '<input type="checkbox" id="chkHTV" ><label for="chkHTV">Hatienvegas999</label> ';
						echo '<input type="checkbox" id="chkHTV" disabled><label for="chkHTV">Hatienvegas999</label> ';
						echo '<input type="hidden" id="lblAgentID_htv"><input type="hidden" id="lblDateFrom_htv"><input type="hidden" id="lblDateTo_htv"><input type="hidden" id="btnBack_htv">';
//					}
					
					// Savanvegas
					if (Yii::app()->user->checkAccess('agent.readSavanWinLoss') ||	User::getUserType() === 'agent'){
						echo '<input type="checkbox" id="chkSAVAN" checked><label for="chkSAVAN">Savanvegas999</label></td>';
					}else{
						echo '<input type="checkbox" id="chkSAVAN" style="display:none;"><label for="chkSAVAN" style="display:none;">Savanvegas999</label> ';
						echo '<input type="hidden" id="lblAgentID_savan"><input type="hidden" id="lblDateFrom_savan"><input type="hidden" id="lblDateTo_savan"><input type="hidden" id="btnBack_savan">';
					}
				?>
				 
				
				
			</tr>
			</table>
			<div class="footer"><input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.submit');?>" onclick="javascript: submitComplete(''); clearContainer();"> <input type="button" class="btn red " value="<?php echo Yii::t('agent','agent.agentwinloss.yesterday');?>" onclick="javascript: btnYesterday(); clearContainer();"> <input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.today');?>" onclick="javascript: btnToDay(); clearContainer();"></div>
		</div>
	</div>
	<div id="agent_winloss_body">
		<div id="vig_sharing"><input type='text' id='txtVIGsharing' value='10'/></div>
	<div id="dv_costavegas">
		<div id="qry_costavegas_total"></div>
    	<div id="qry_costavegas_result"></div>
    	<div id="qry_costavegas_result_details"></div>
    	<div id="qry_costavegas_betting_details"></div>
    	<div id="qry_costavegas_betting_details_summary_footer" style="display: none;">
    		<div id="betting_footer_header"><label id="lblCostaBettingSummaryPlayerID"></label> COSTAVEGAS BETTING HISTORY SUMMARY</div>
    		<table id="betting_footer" cellpadding="5" cellspacing="0">
    		<tr>
    			<td class="theader">Type</td>
    			<td class="theader">No. of Bet</td>
    			<td class="theader">Valid Stake</td>
    			<td class="theader">Win/Loss</td>
    			<td class="theader">Commission</td>
    			<td class="theader">Tips</td>
    			<td class="theader">Total</td>
    			<td class="theader">Balance</td>
    		</tr>
    		<tr>
    			<td>Current Page</td>
    			<td><label id="lblCPNoOfBet">0</label></td>
    			<td><label id="lblCPValidStake">0</label></td>
    			<td><label id="lblCPWinLoss">0</label></td>
    			<td><label id="lblCPCommission">0</label></td>
    			<td><label id="lblCPTips">0</label></td>
    			<td><label id="lblCPTotal">0</label></td>
    			<td><label id="lblCPBalance">N/A</label></td>
    		</tr>
    		<tr>
    			<td>All Page</td>
    			<td><label id="lblAPNoOfBet">0</label></td>
    			<td><label id="lblAPValidStake">0</label></td>
    			<td><label id="lblAPWinLoss">0</label></td>
    			<td><label id="lblAPCommission">0</label></td>
    			<td><label id="lblAPTips">0</label></td>
    			<td><label id="lblAPTotal">0</label></td>
    			<td><label id="lblAPBalance">0</label></td>
    		</tr>
    		</table>
    	</div>
    </div>
    <div id="dv_hatienvegas">
    	<div id="qry_htv999_total"></div>
    	<div id="qry_htv999_result"></div>
    	<!--  <div id="qry_htv999_result_details"></div>-->
    	<div id="qry_htv999_betting_details"></div>
    	<div id="qry_htv999_betting_details_summary_footer" style="display: none;">
    		<div id="betting_footer_header"><label id="lblHTVBettingSummaryPlayerID"></label> HTV BETTING HISTORY SUMMARY</div>
    		<table id="betting_footer" cellpadding="5" cellspacing="0">
    		<tr>
    			<td class="theader">Type</td>
    			<td class="theader">No. of Bet</td>
    			<td class="theader">Bet Amount</td>
    			<td class="theader">Valid Bet</td>
    			<td class="theader">Win/Loss</td>
    			<td class="theader">Commission</td>
    			<td class="theader">Total</td>
    			<td class="theader">Balance</td>
    		</tr>
    		<tr>
    			<td>Current Page</td>
    			<td><label id="lblCPNoOfBet_htv">0</label></td>
    			<td><label id="lblCPBetAmount_htv">0</label></td>
    			<td><label id="lblCPValidBet_htv">0</label></td>
    			<td><label id="lblCPWinLoss_htv">0</label></td>
    			<td><label id="lblCPCommission_htv">0</label></td>
    			<td><label id="lblCPTotal_htv">0</label></td>
    			<td><label id="lblCPBalance_htv">0</label></td>
    		</tr>
    		<tr>
    			<td>All Page</td>
    			<td><label id="lblAPNoOfBet_htv">0</label></td>
    			<td><label id="lblAPBetAmount_htv">0</label></td>
    			<td><label id="lblAPValidBet_htv">0</label></td>
    			<td><label id="lblAPWinLoss_htv">0</label></td>
    			<td><label id="lblAPCommission_htv">0</label></td>
    			<td><label id="lblAPTotal_htv">0</label></td>
    			<td><label id="lblAPBalance_htv">0</label></td>
    		</tr>
    		</table>
    	</div>
    </div>
    <div id="dv_savanvegas">
    	<div id="qry_savan_vegas_total"></div>
    	<div id="qry_savan_vegas_result"></div>
    	<div id="qry_savan_vegas_result_details"></div>
    	<div id="qry_savan_vegas_betting_details"></div>
	    <div id="qry_savan_vegas_betting_details_summary_footer" style="display: none;">
    		<div id="betting_footer_header"><label id="lblSAVANBettingSummaryPlayerID"></label> SAVANVEGAS BETTING HISTORY SUMMARY</div>
    		<table id="betting_footer" cellpadding="5" cellspacing="0">
    		<tr>
    			<td class="theader">Type</td>
    			<td class="theader">No. of Bet</td>
    			<td class="theader">Bet Amount</td>
    			<td class="theader">Valid Bet</td>
    			<td class="theader">Win/Loss</td>
    			<td class="theader">Commission</td>
    			<td class="theader">Total</td>
    			<td class="theader">Balance</td>
    		</tr>
    		<tr>
    			<td>Current Page</td>
    			<td><label id="lblCPNoOfBet_savan">0</label></td>
    			<td><label id="lblCPBetAmount_savan">0</label></td>
    			<td><label id="lblCPValidBet_savan">0</label></td>
    			<td><label id="lblCPWinLoss_savan">0</label></td>
    			<td><label id="lblCPCommission_savan">0</label></td>
    			<td><label id="lblCPTotal_savan">0</label></td>
    			<td><label id="lblCPBalance_savan">0</label></td>
    		</tr>
    		<tr>
    			<td>All Page</td>
    			<td><label id="lblAPNoOfBet_savan">0</label></td>
    			<td><label id="lblAPBetAmount_savan">0</label></td>
    			<td><label id="lblAPValidBet_savan">0</label></td>
    			<td><label id="lblAPWinLoss_savan">0</label></td>
    			<td><label id="lblAPCommission_savan">0</label></td>
    			<td><label id="lblAPTotal_savan">0</label></td>
    			<td><label id="lblAPBalance_savan">0</label></td>
    		</tr>
    		</table>
    	</div>
    </div>
	</div>
	<script type="text/javascript">
		<?php if(Yii::app()->user->checkAccess('agent.readCostavegasWinLoss')) { ?>
		$('#chkVIG').hide();
		$('#lblVIG').hide();
		<?php }?>
	</script>
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
	</form>
	<?php 
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		    'id'=>'winloss_result_img',
		    // additional javascript options for the dialog plugin
		    'options'=>array(
		        'title'=>'Screen Shot',
		        'autoOpen'=>false,
	    		'width'=>'auto',
	    		'height'=>'auto',
		    	'modal'=> true,
		    	'resizable'=> false,
		    ),
		));
	
	// Initialize dialog content
    echo '<div id="viewWinLossResultImg"></div>';
	
    // End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'winloss_bet_result_detail',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Win/Loss Betting Detail',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> true,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewResultDetail"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
</body>
</html>