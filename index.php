<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/yii/framework/yii.php';

$iniFile = parse_ini_file(dirname(__FILE__).'/server-environment.ini', true);
$yiiEnv = trim($iniFile['server_env']);


if ($yiiEnv === "production") 
{
	$config=dirname(__FILE__).'/protected/config/production.php';
	defined('YII_DEBUG') or define('YII_DEBUG',false);
}
else if ($yiiEnv === "staging")
{
	$config=dirname(__FILE__).'/protected/config/staging.php';
	defined('YII_DEBUG') or define('YII_DEBUG',false);
}
else if ($yiiEnv === "new_staging")
{
	$config=dirname(__FILE__).'/protected/config/new_staging.php';
	defined('YII_DEBUG') or define('YII_DEBUG',false);
}
else if ($yiiEnv === "development")
{
	$config=dirname(__FILE__).'/protected/config/development.php';
	defined('YII_DEBUG') or define('YII_DEBUG',true);
}
else if ($yiiEnv === "localhost")
{
	$config=dirname(__FILE__).'/protected/config/localhost.php';
	defined('YII_DEBUG') or define('YII_DEBUG',true);
}
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
//Set 1 for Only Errors
//Set 2 for Only Errors Error+Warnings
//All Errors

require_once($yii);
Yii::createWebApplication($config)->run();