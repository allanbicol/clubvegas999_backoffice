<?php
/**
 * @todo AgentCreditAndBalanceValidationController
 * @copyright CE
 * @author Leo karl
 * @since 2012-09-13
 */
class AgentCreditAndBalanceValidationController extends MyController
{
	public function actionCreditAndBalance()
	{
		$cb=new AgentCreditAndBalanceValidation();
		
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cb->getAgentCreditAndBalanceCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records=$cb->getAgentCreditAndBalance($orderField, $sortType, $startIndex, $limit);
		
		$filedNames = array("account_id","agent_type","currency_name","available_credit","balance","assigned_credit","total_downline_credit","balance_validation","amount","withdraw","deposit","return_value");
		
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionAgentPlayerCreditValidation()
	{
		$cb=new AgentCreditAndBalanceValidation();
	
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cb->getAgentPlayerCreditCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$cb->getAgentPlayerCredit($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("agent_id","account_id","available_credit","balance","amount","withdraw","deposit","return_value");
	
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionDuplicateAgentTransHistory()
	{
		$cb=new AgentCreditAndBalanceValidation();
	
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cb->getDuplicateAgentTransHistoryCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$cb->getDuplicateAgentTransHistory($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("agent_account_id","transaction_date","transaction_number","trans_type_id","currency_id","amount","credit_before","credit_after","balance_before","balance_after","upper_agent_credit_before","upper_agent_credit_after","operator_id");
	
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionDuplicateAgentPlayerTransHistory()
	{
		$cb=new AgentCreditAndBalanceValidation();
	
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cb->getDuplicateAgentPlayerTransHistoryCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$cb->getDuplicateAgentPlayerTransHistory($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("agent_account_id","transaction_date","transaction_number","trans_type_id","currency_id","amount","credit_before","credit_after","balance_before","balance_after","upper_agent_credit_before","upper_agent_credit_after","operator_id");
	
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionIndex()
	{
		$this->render("index");
	}	
}