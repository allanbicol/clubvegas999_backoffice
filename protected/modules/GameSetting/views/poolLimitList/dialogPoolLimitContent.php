<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/gamesetting.css" />
<script type="text/javascript">
function validate(text)
{
	
	var pattern = /^-?[0-9]+(.[0-9]{1,2})?$/; 
	//var text = document.getElementById('txtBunosAmount').value;
    if (text.match(pattern)==null) 
    {
		return false;
    }
	else
	{
		return true;
	}
}
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function get_cookie(cookie_name)
{
  var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

  if (results)
    return (unescape(results[2]));
  else
    return null;
}

function confirmPoolLimit(){
	if (document.getElementById('cmbGame').value==0){
		document.getElementById('txtPoolLimitDialogErrorMsg').value='Please select game.';
		document.getElementById('cmbGame').focus();
		return false;
	}
	if (trim(document.getElementById('txtPoolLimit').value)==''){
		document.getElementById('txtPoolLimitDialogErrorMsg').value='Pool Limit is required!';
		document.getElementById('txtPoolLimit').focus();
		return false;
	}
	if (validate(document.getElementById('txtPoolLimit').value)==false){
		document.getElementById('txtPoolLimitDialogErrorMsg').value='Invalid Pool Limit!';
		return false;
	}
	jQuery.ajax({
		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/PoolLimitList/CreatePoolLimit',
		method: 'POST',
		data: {'currency_id': document.getElementById('txtPoolLimit').title,'game_id': document.getElementById('cmbGame').value,'amount':document.getElementById('txtPoolLimit').value},
		context: '',
		success: function(msg) {
			var data= msg.split(';');
			var addNewRow = {
					id: data[0], 
					game_name: data[3], 
					amount: trim(document.getElementById('txtPoolLimit').value),
					last_update: data[1],
				    setting:"<a href='#' onclick='javascript: $(\"#viewContent1\").load(\"<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/PoolLimitList/LoadPoolLimitSettingContent&pool_id=" + data[0] + "&pool_game_id="+ data[2] +"\");$(\"#dialogSettingPoolLimit\").dialog(\"open\");'>Setting</a>"};
		    jQuery("#list1").addRowData(data[0], addNewRow);
			$('#dialogAddPoolLimit').dialog('close');
    	}
	});
}
</script>
<div id="dialogPoolLimitContent">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr><td class="left">Game</td><td class="right">
			<Select id="cmbGame">
				<option value="0">Select Game</option>
				<?php 
					$connection = Yii::app()->db_cv999_fd_master;
					$command = $connection->createCommand("CALL spGetPoolLimitNewGame(". $_GET['currency_id'] .")");
					$game_records = $command->query();
					foreach ($game_records as $row){
						if ($row['count']==0){
						echo '<option value="' . $row['id'] . '">'. $row['game_name'] . '</option>';}
					}
				?>
			</Select>
		</td></tr>
		
		<tr><td class="left">Pool Limit</td><td class="right"><input title="<?php echo $_GET['currency_id'];?>" type="text" id="txtPoolLimit"/></td></tr>
	</table>
	<div id="dialogPoolLimitContentFooter">
		<input type="button" value="Confirm" onclick="javascript: confirmPoolLimit();"/>
	</div>
</div>
<input type="text" name="txtPoolLimitDialogErrorMsg" id="txtPoolLimitDialogErrorMsg" readonly="true"/>