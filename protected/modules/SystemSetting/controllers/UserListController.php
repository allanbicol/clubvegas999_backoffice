<?php
/**
 * @todo UserListController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class UserListController extends MyController
{
	/**
	 * @todo 	Display the user change password form
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-05-13
	 */
	public function actionUserPassword()
	{
		$this->render("userPassword");
		
	}
	/**
	 * @todo 	Change the user password
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-05-13
	 */
	public function actionChangePassword()
	{
		$oldPassword = CV999Utility::generateOneWayPassword($_POST['oldPassword']);
		$newPassword =  CV999Utility::generateOneWayPassword($_POST['newPassword']);
		$confirmPassword = CV999Utility::generateOneWayPassword($_POST['confirmPassword']);
		
		CreateUser::changePassword(Yii::app()->session['account_id'], $oldPassword, $newPassword, $confirmPassword);
		
	}
	public function actionUserList()
	{
		$cp = new UserAccountList();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result = $cp->getCountUserRecord();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
		
		$startIndex = $limit*$page - $limit;
		$user_records=$cp->getUserList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("account_id","account_name","account_status","level_name","opening_date_time","max_deposit","max_withdraw","account_id","account_id","account_id","account_id","account_id","account_id");
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='<a class=\'btn mini red\' href=\'index.php?r=SystemSetting/UserListLog/UserListLog&Account_ID=\'>Log <i class=\'icon-list\'></i></a><a class=\'btn mini red\' href=\'index.php?r=SystemSetting/CreateUser&Account_ID=\'>Setting <i class=\'icon-cogs\'></i></a>';
	
		echo JsonUtil::jsonEncode($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,7);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readUserAccount'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}
