<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/role.css" />
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/rolelist.js"></script>
<script type="text/javascript">
	var baseUrl="<?php echo Yii::app()->request->baseUrl;?>";
	$(document).ready(function() {
		userList('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Role/RoleList');
	});
	var permissionWrite = ('<?php echo (Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission')) ? 1 : 0;?>' == 1) ? true : false;
	var permissionView = ('<?php echo (Yii::app()->user->checkAccess('systemSetting.readRoleAndPermission')) ? 1 : 0;?>' == 1) ? true : false; 
</script>
</head>
<body>
	<b>User Role Information</b><br><br>
	<table id="listlog" ><tr><td/></tr></table>
	<div id="pager2"></div>
</body>
</html>