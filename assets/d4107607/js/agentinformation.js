//active menu color
document.getElementById('agentHeader').className="start active";
document.getElementById('mnu_agent_info').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><li><a href='#'>Agent Overview</a></li>");

function btnDetails(account_type)
{
	var test_currency='';
	if(document.getElementById('chkExceptTestCurrency').checked==true){
		test_currency=0;
	}else{
		test_currency=1;
	}
	if(strIsUser=='user'){
		tableAgentOverviewDetails(test_currency);
	}else if(account_type=='SC'){
		tableAgentOverviewDetailsSC(test_currency);
	}else if(account_type=='SMA'){
		tableAgentOverviewDetailsSMA(test_currency);
	}else if(account_type=='MA'){
		tableAgentOverviewDetailsMA(test_currency);
	}else if(account_type=='AGT'){
		tableAgentOverviewDetailsAGT(test_currency);
	}
}
function cmbCurrency(fltValue,intlevel,actionURL)
{
	
	var flt_yesterday=document.getElementById('lblYesterdayWinLoss').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
	var flt_yesterday_slot=document.getElementById('lblYesterdaySlotWinLoss').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
	var flt_available=document.getElementById('lblAvailableCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
	var flt_SC=0;
	var flt_SMA=0;
	var flt_MA=0;
	var flt_AGT=0;
	var flt_MEM=0;
	var flt_SC_percentage=0;
	var flt_SMA_percentage=0;
	var flt_MA_percentage=0;
	var flt_AGT_percentage=0;
	var flt_MEM_percentage=0;
	var flt_SC_assigned=0;
	var flt_SMA_assigned=0;
	var flt_MA_assigned=0;
	var flt_AGT_assigned=0;
	var flt_MEM_assigned=0;
	
	if(strIsUser=='user'){
		flt_SC=document.getElementById('lblSCCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_SMA=document.getElementById('lblSMACredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_MA=document.getElementById('lblMACredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_AGT=document.getElementById('lblAGTCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_MEM=document.getElementById('lblMEMCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');

		//credit percentage
		flt_SC_percentage=document.getElementById('lblSCCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_SMA_percentage=document.getElementById('lblSMACreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_MA_percentage=document.getElementById('lblMACreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_AGT_percentage=document.getElementById('lblAGTCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_MEM_percentage=document.getElementById('lblMEMCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');

		//credit assigned
		flt_SC_assigned=document.getElementById('lblSCAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_SMA_assigned=document.getElementById('lblSMAAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_MA_assigned=document.getElementById('lblMAAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_AGT_assigned=document.getElementById('lblAGTAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		flt_MEM_assigned=document.getElementById('lblMEMAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
	}else{
		if(intlevel=='SC'){
			
			flt_SMA=document.getElementById('lblSMACredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MA=document.getElementById('lblMACredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_AGT=document.getElementById('lblAGTCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM=document.getElementById('lblMEMCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			//credit percentage
			flt_SMA_percentage=document.getElementById('lblSMACreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MA_percentage=document.getElementById('lblMACreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_AGT_percentage=document.getElementById('lblAGTCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM_percentage=document.getElementById('lblMEMCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			//credit assigned
			flt_SMA_assigned=document.getElementById('lblSMAAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MA_assigned=document.getElementById('lblMAAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_AGT_assigned=document.getElementById('lblAGTAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM_assigned=document.getElementById('lblMEMAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		}else if(intlevel=='SMA'){
			flt_MA=document.getElementById('lblMACredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_AGT=document.getElementById('lblAGTCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM=document.getElementById('lblMEMCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');

			//credit percentage
			flt_MA_percentage=document.getElementById('lblMACreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_AGT_percentage=document.getElementById('lblAGTCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM_percentage=document.getElementById('lblMEMCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');

			//credit assigned
			flt_MA_assigned=document.getElementById('lblMAAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_AGT_assigned=document.getElementById('lblAGTAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM_assigned=document.getElementById('lblMEMAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		}else if(intlevel=='MA'){
			flt_AGT=document.getElementById('lblAGTCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM=document.getElementById('lblMEMCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			//credit percentage
			flt_AGT_percentage=document.getElementById('lblAGTCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM_percentage=document.getElementById('lblMEMCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			//credit assigned
			flt_AGT_assigned=document.getElementById('lblAGTAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			flt_MEM_assigned=document.getElementById('lblMEMAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		}else if(intlevel=='AGT'){
			flt_MEM=document.getElementById('lblMEMCredit').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			//credit percentage
			flt_MEM_percentage=document.getElementById('lblMEMCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
			//credit assigned
			flt_MEM_assigned=document.getElementById('lblMEMAssignedCreditPercentage').innerHTML.replace(',','').replace(',','').replace(',','').replace(',','');
		}
	}
	
	jQuery.ajax({
		url: actionURL,
		method: 'POST',
		data: {
			'current_currency_name': document.getElementById('lblYesterdayCurrency').innerHTML,
			'currency_name': fltValue,
			'flt_yesterday': flt_yesterday,
			'flt_yesterday_slot': flt_yesterday_slot,
			'flt_available': flt_available,
			'flt_SC': flt_SC,
			'flt_SMA': flt_SMA,
			'flt_MA': flt_MA,
			'flt_AGT': flt_AGT,
			'flt_MEM': flt_MEM,
			'flt_SC_percentage':flt_SC_percentage,
			'flt_SMA_percentage':flt_SMA_percentage,
			'flt_MA_percentage':flt_MA_percentage,
			'flt_AGT_percentage':flt_AGT_percentage,
			'flt_MEM_percentage':flt_MEM_percentage,
			'flt_SC_assigned':flt_SC_assigned,
			'flt_SMA_assigned':flt_SMA_assigned,
			'flt_MA_assigned':flt_MA_assigned,
			'flt_AGT_assigned':flt_AGT_assigned,
			'flt_MEM_assigned':flt_MEM_assigned,
		},
		context: '',
		success: function(data){
			
			var strdata=data.split(':');
			//amount
			document.getElementById('lblYesterdayWinLoss').innerHTML=strdata[0];
			document.getElementById('lblYesterdaySlotWinLoss').innerHTML=strdata[17];
			document.getElementById('lblAvailableCredit').innerHTML=strdata[1];
			//currency
			document.getElementById('lblYesterdayCurrency').innerHTML=fltValue;
			document.getElementById('lblYesterdaySlotCurrency').innerHTML=fltValue;
			document.getElementById('lblAvailableCurrency').innerHTML=fltValue;
			//if (intlevel>=1 && intlevel<=8){
			if(strIsUser=='user'){
				//amount
				document.getElementById('lblSCCredit').innerHTML=strdata[2];
				document.getElementById('lblSMACredit').innerHTML=strdata[3];
				document.getElementById('lblMACredit').innerHTML=strdata[4];
				document.getElementById('lblAGTCredit').innerHTML=strdata[5];
				document.getElementById('lblMEMCredit').innerHTML=strdata[6];

				//percentage
				document.getElementById('lblSCCreditPercentage').innerHTML=strdata[7];
				document.getElementById('lblSMACreditPercentage').innerHTML=strdata[8];
				document.getElementById('lblMACreditPercentage').innerHTML=strdata[9];
				document.getElementById('lblAGTCreditPercentage').innerHTML=strdata[10];
				document.getElementById('lblMEMCreditPercentage').innerHTML=strdata[11];

				//assigned credit
				document.getElementById('lblSCAssignedCreditPercentage').innerHTML=strdata[12];
				document.getElementById('lblSMAAssignedCreditPercentage').innerHTML=strdata[13];
				document.getElementById('lblMAAssignedCreditPercentage').innerHTML=strdata[14];
				document.getElementById('lblAGTAssignedCreditPercentage').innerHTML=strdata[15];
				document.getElementById('lblMEMAssignedCreditPercentage').innerHTML=strdata[16];
				
				//currency
				document.getElementById('lblSCCurrency').innerHTML=fltValue;
				document.getElementById('lblSMACurrency').innerHTML=fltValue;
				document.getElementById('lblMACurrency').innerHTML=fltValue;
				document.getElementById('lblAGTCurrency').innerHTML=fltValue;
				document.getElementById('lblMEMCurrency').innerHTML=fltValue;
				//percentage currency
				document.getElementById('lblSCCreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblSMACreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblMACreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblAGTCreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblMEMCreditPercentageCurrency').innerHTML=fltValue;
				//assigned credit currency
				document.getElementById('lblSCAssignedCreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblSMAAssignedCreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblMAAssignedCreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblAGTAssignedCreditPercentageCurrency').innerHTML=fltValue;
				document.getElementById('lblMEMAssignedCreditPercentageCurrency').innerHTML=fltValue;
			}else{
				if(intlevel=='SC'){
					//amount
					document.getElementById('lblSMACredit').innerHTML=strdata[3];
    				document.getElementById('lblMACredit').innerHTML=strdata[4];
    				document.getElementById('lblAGTCredit').innerHTML=strdata[5];
    				document.getElementById('lblMEMCredit').innerHTML=strdata[6];
    				//percentage
					document.getElementById('lblSMACreditPercentage').innerHTML=strdata[8];
    				document.getElementById('lblMACreditPercentage').innerHTML=strdata[9];
    				document.getElementById('lblAGTCreditPercentage').innerHTML=strdata[10];
    				document.getElementById('lblMEMCreditPercentage').innerHTML=strdata[11];
    				//assigned credit
					document.getElementById('lblSMAAssignedCreditPercentage').innerHTML=strdata[13];
    				document.getElementById('lblMAAssignedCreditPercentage').innerHTML=strdata[14];
    				document.getElementById('lblAGTAssignedCreditPercentage').innerHTML=strdata[15];
    				document.getElementById('lblMEMAssignedCreditPercentage').innerHTML=strdata[16];
    				//currency
    				document.getElementById('lblSMACurrency').innerHTML=fltValue;
    				document.getElementById('lblMACurrency').innerHTML=fltValue;
    				document.getElementById('lblAGTCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMCurrency').innerHTML=fltValue;
    				//percentage currency
    				document.getElementById('lblSMACreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMACreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblAGTCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMCreditPercentageCurrency').innerHTML=fltValue;
    				//assigned credit currency
    				document.getElementById('lblSMAAssignedCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMAAssignedCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblAGTAssignedCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMAssignedCreditPercentageCurrency').innerHTML=fltValue;
				}else if(intlevel=='SMA'){
					//amount
					document.getElementById('lblMACredit').innerHTML=strdata[4];
    				document.getElementById('lblAGTCredit').innerHTML=strdata[5];
    				document.getElementById('lblMEMCredit').innerHTML=strdata[6];
    				//percentage
    				document.getElementById('lblMACreditPercentage').innerHTML=strdata[9];
    				document.getElementById('lblAGTCreditPercentage').innerHTML=strdata[10];
    				document.getElementById('lblMEMCreditPercentage').innerHTML=strdata[11];
    				//assigned credit
    				document.getElementById('lblMAAssignedCreditPercentage').innerHTML=strdata[14];
    				document.getElementById('lblAGTAssignedCreditPercentage').innerHTML=strdata[15];
    				document.getElementById('lblMEMAssignedCreditPercentage').innerHTML=strdata[16];
    				//currency
    				document.getElementById('lblMACurrency').innerHTML=fltValue;
    				document.getElementById('lblAGTCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMCurrency').innerHTML=fltValue;
    				//percentage currency
    				document.getElementById('lblMACreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblAGTCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMCreditPercentageCurrency').innerHTML=fltValue;
    				//assigned credit currency
    				document.getElementById('lblMAAssignedCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblAGTAssignedCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMAssignedCreditPercentageCurrency').innerHTML=fltValue;
				}else if(intlevel=='MA'){
					//amount
					document.getElementById('lblAGTCredit').innerHTML=strdata[5];
    				document.getElementById('lblMEMCredit').innerHTML=strdata[6];
    				//percentage
    				document.getElementById('lblAGTCreditPercentage').innerHTML=strdata[10];
    				document.getElementById('lblMEMCreditPercentage').innerHTML=strdata[11];
    				//assigned credit
    				document.getElementById('lblAGTAssignedCreditPercentage').innerHTML=strdata[15];
    				document.getElementById('lblMEMAssignedCreditPercentage').innerHTML=strdata[16];
    				//currency
    				document.getElementById('lblAGTCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMCurrency').innerHTML=fltValue;
    				//percentage currency
    				document.getElementById('lblAGTCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMCreditPercentageCurrency').innerHTML=fltValue;
    				//assigned credit currency
    				document.getElementById('lblAGTAssignedCreditPercentageCurrency').innerHTML=fltValue;
    				document.getElementById('lblMEMAssignedCreditPercentageCurrency').innerHTML=fltValue;
				}else if(intlevel=='AGT'){
					//amount
					document.getElementById('lblMEMCredit').innerHTML=strdata[6];
					//percentage
    				document.getElementById('lblMEMCreditPercentage').innerHTML=strdata[11];
    				//assigned credit
    				document.getElementById('lblMEMAssignedCreditPercentage').innerHTML=strdata[16];
					//currency
					document.getElementById('lblMEMCurrency').innerHTML=fltValue;
					//percentage currency 
    				document.getElementById('lblMEMCreditPercentageCurrency').innerHTML=fltValue;
    				//assigned credit currency
    				document.getElementById('lblMEMAssignedCreditPercentageCurrency').innerHTML=fltValue;
				}
			}
    	}
	});
}

function tableAgentOverviewDetails(test_currency) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: urlTableAgentOverviewDetails + test_currency, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: [lblOverview,lblTotalCredit,lblTotalAssignedCredit,lblTotalPercentageCredit,lblTotalCredit,lblTotalAssignedCredit,lblTotalPercentageCredit,lblTotalCredit,lblTotalAssignedCredit,lblTotalPercentageCredit,lblTotalCredit,lblTotalAssignedCredit,lblTotalPercentageCredit,"<font color='black'>" + lblPlayers + "</font> " + lblTotalCredit],
		    colModel: [
		      {name: 'currency', index: 'currency', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { if(tv=='Active'){return 'style="color:#2184c2"'}else if(tv=='Closed'){return 'style="color:#912626"'}else{ return 'style="color:black"'};}},
		      {name: 'sc', index: 'sc', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'sc_credit_assigned', index: 'sc_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'sc_percentage', index: 'sc_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'sma', index: 'sma', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'sma_credit_assigned', index: 'sma_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'sma_percentage', index: 'sma_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma', index: 'ma', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma_credit_assigned', index: 'ma_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma_percentage', index: 'ma_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt', index: 'agt', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_credit_assigned', index: 'agt_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_percentage', index: 'agt_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'mem', index: 'mem', width: 150,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		    ],
		    loadComplete: function() {
		        $("tr.jqgrow:odd").css("background", "#DDDDDC");
		        $("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    loadtext:"",
            rownumbers: true,
            sortable: false,
		    sortname: 'currency',
		    sortorder: 'ASC',
		    caption: lblDetailTitle + ' <input class="btn red" type="button" value="<<" id="btnReturn" onclick="javascript: document.getElementById(\'qry_result\').innerHTML=\'\'"/>',
		    hidegrid: false,
		    viewrecords: true,
		});
		$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
		$("#list1").jqGrid('setGroupHeaders', {
			useColSpanStyle: true, 
			groupHeaders:[
				{startColumnName: 'sc', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblSubCompanies +'</label>'},
				{startColumnName: 'sma', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblSeniorMasterAgents +'</label>'},
				{startColumnName: 'ma', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblMasterAgents +'</label>'},
				{startColumnName: 'agt', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblAgents +'</label>'},
			]
		});
	});
} 
function tableAgentOverviewDetailsSC(test_currency) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: urlTableAgentOverviewDetails + test_currency, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: [lblOverview,lblTotalCredit,lblTotalAssignedCredit,lblTotal + " %",lblTotalCredit,lblTotalAssignedCredit,lblTotal + " %",lblTotalCredit,lblTotalAssignedCredit,lblTotal +" %",lblPlayers + " " + lblTotalCredit],
		    colModel: [
		      {name: 'currency', index: 'currency', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { if(tv=='Active'){return 'style="color:#2184c2"'}else if(tv=='Closed'){return 'style="color:#912626"'}else{ return 'style="color:black"'};}},
		      {name: 'sma', index: 'sma', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'sma_credit_assigned', index: 'sma_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'sma_percentage', index: 'sma_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma', index: 'ma', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma_credit_assigned', index: 'ma_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma_percentage', index: 'ma_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt', index: 'agt', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_credit_assigned', index: 'agt_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_percentage', index: 'agt_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'mem', index: 'mem', width: 150,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		    ],
		    loadComplete: function() {
		    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    				    
		    loadtext:"",
            rownumbers: true,
            sortable: false,
		    sortname: 'currency',
		    sortorder: 'ASC',
		    caption: lblDetailTitle + ' <input type="button" value="<<" id="btnReturn" onclick="javascript: document.getElementById(\'qry_result\').innerHTML=\'\'"/>',
		    hidegrid: false,
		    viewrecords: true,
		});
		$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
		$("#list1").jqGrid('setGroupHeaders', {
			useColSpanStyle: true, 
			groupHeaders:[
				{startColumnName: 'sma', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblSeniorMasterAgents +'</label>'},
				{startColumnName: 'ma', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblMasterAgents +'</label>'},
				{startColumnName: 'agt', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblAgents +'</label>'},
			]
		});
	});
} 

function tableAgentOverviewDetailsSMA(test_currency) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: urlTableAgentOverviewDetails + test_currency, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: [lblOverview,lblTotalCredit,lblTotalAssignedCredit,lblTotal + " %",lblTotalCredit,lblTotalAssignedCredit,lblTotal + " %",lblPlayers + " " + lblTotalCredit],
		    colModel: [
		      {name: 'currency', index: 'currency', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { if(tv=='Active'){return 'style="color:#2184c2"'}else if(tv=='Closed'){return 'style="color:#912626"'}else{ return 'style="color:black"'};}},
		      {name: 'ma', index: 'ma', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma_credit_assigned', index: 'ma_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'ma_percentage', index: 'ma_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt', index: 'agt', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_credit_assigned', index: 'agt_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_percentage', index: 'agt_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'mem', index: 'mem', width: 150,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		    ],
		    loadComplete: function() {
		    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    				    
		    loadtext:"",
            rownumbers: true,
            sortable: false,
		    sortname: 'currency',
		    sortorder: 'ASC',
		    caption: lblDetailTitle + ' <input type="button" value="<<" id="btnReturn" onclick="javascript: document.getElementById(\'qry_result\').innerHTML=\'\'"/>',
		    hidegrid: false,
		    viewrecords: true,
		});
		$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
		$("#list1").jqGrid('setGroupHeaders', {
			useColSpanStyle: true, 
			groupHeaders:[
				{startColumnName: 'ma', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblMasterAgents +'</label>'},
				{startColumnName: 'agt', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblAgents +'</label>'},
			]
		});
	});
}

function tableAgentOverviewDetailsMA(test_currency) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: urlTableAgentOverviewDetails + test_currency, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: [lblOverview,lblTotalCredit,lblTotalAssignedCredit,lblTotal + " %",lblPlayers + " " + lblTotalCredit],
		    colModel: [
		      {name: 'currency', index: 'currency', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { if(tv=='Active'){return 'style="color:#2184c2"'}else if(tv=='Closed'){return 'style="color:#912626"'}else{ return 'style="color:black"'};}},
		      {name: 'agt', index: 'agt', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_credit_assigned', index: 'agt_credit_assigned', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'agt_percentage', index: 'agt_percentage', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'mem', index: 'mem', width: 150,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		    ],
		    loadComplete: function() {
		    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    				    
		    loadtext:"",
            rownumbers: true,
            sortable: false,
		    sortname: 'currency',
		    sortorder: 'ASC',
		    caption: lblDetailTitle + ' <input type="button" value="<<" id="btnReturn" onclick="javascript: document.getElementById(\'qry_result\').innerHTML=\'\'"/>',
		    hidegrid: false,
		    viewrecords: true,
		});
		$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
		$("#list1").jqGrid('setGroupHeaders', {
			useColSpanStyle: true, 
			groupHeaders:[
				{startColumnName: 'agt', numberOfColumns: 3, titleText: '<label style="Color:black">'+ lblAgents +'</label>'},
			]
		});
	});
} 
function tableAgentOverviewDetailsAGT(test_currency) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: urlTableAgentOverviewDetails + test_currency, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: [lblOverview,lblPlayers + " " + lblTotalCredit],
		    colModel: [
		      {name: 'currency', index: 'currency', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { if(tv=='Active'){return 'style="color:#2184c2"'}else if(tv=='Closed'){return 'style="color:#912626"'}else{ return 'style="color:black"'};}},
		      {name: 'mem', index: 'mem', width: 150,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
		    ],
		    loadComplete: function() {
		    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    				    
		    loadtext:"",
            rownumbers: true,
            sortable: false,
		    sortname: 'currency',
		    sortorder: 'ASC',
		    caption: lblDetailTitle + ' <input type="button" value="<<" id="btnReturn" onclick="javascript: document.getElementById(\'qry_result\').innerHTML=\'\'"/>',
		    hidegrid: false,
		    viewrecords: true,
		});
		$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
	});
}