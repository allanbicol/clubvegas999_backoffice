<?php
/**
 * @TODO store all session variables CV999
 * @copyright Creative Entertainment Ltd.
 * @author sitthykun
 * @since 2012-04-05
 */

defined('_CV999_') or die;

class CV999OnlinePlayers extends CV999RedisCore {
	private $lobby;
	private $lobbyAgentPlayer;
	private $lobbyCashPlayer;
	private $login;
	private $mainKey = '';
	
	/**
	 * 
	 */
	public function CV999OnlinePlayers() {
		// 
		parent::CV999RedisCore();
		// initialize
		$this->lobby = array(
				'0' =>  parent::LOBBY . ':*',
				'1' =>  parent::LOBBY . ':*:a:*',
				'2' =>  parent::LOBBY . ':*:c:*',
// 				'3' =>  parent::LOBBY . ':1:*',
// 				'4' =>  parent::LOBBY . ':1:a:*',
// 				'5' =>  parent::LOBBY . ':1:c:*',
// 				'6' =>  parent::LOBBY . ':2:*',
// 				'7' =>  parent::LOBBY . ':2:a:*',
// 				'8' =>  parent::LOBBY . ':2:c:*',
// 				'9' =>  parent::LOBBY . ':3:*',
// 				'10' => parent::LOBBY . ':3:a:*',
// 				'11' => parent::LOBBY . ':3:c:*',
// 				'12' => parent::LOBBY . ':4:*',
// 				'13' => parent::LOBBY . ':4:a:*',
// 				'14' => parent::LOBBY . ':4:c:*'
		);
		// 
		$this->lobbyAgentPlayer = parent::LOBBY . ':*:a:';
		$this->lobbyCashPlayer = parent::LOBBY . ':*:c:';
		// 
		$this->login = array(
				'0' => parent::LOGIN . ':*',
				'1' => parent::LOGIN . ':a:*',
				'2' => parent::LOGIN . ':c:*'
		);
	}

	/**
	 *
	 * @param string/integer $casinoID
	 * @param string/integer $type
	 */
	public function countPlayersOnLobby($casinoID = '*', $type = '*') {
		$query = parent::LOBBY . ':';
		if ($casinoID != '*') {
			if ($type != '*') {
				$query = $query . $casinoID . ':' . $type . ':*';
			} else {
				$query = $query . $casinoID . ':*';
			}
		} else {
			if ($type != '*') {
				$query = $query . '*:' . $type . ':*';
			} else {
				$query = $query . '*';
			}
		}
		// 
		return count($this->redis->keys($query));
	}

	/**
	 *
	 */
	public function getAllPlayersOnSite() {
		return $this->redis->keys($this->login['0']);
	}

	/**
	 * 
	 */
	public function getAllAgentPlayersOnSite() {
		//$this->redisLogin->get
		return $this->redis->keys($this->login['1']);
	}

	/**
	 *
	 */
	public function getAllCashPlayersOnSite() {
		return $this->redis->keys($this->login['2']);
	}

	/**
	 *
	 */
	public function getAllPlayersOnLobby() {
		return $this->redis->keys($this->lobby['0']);
	}
	
	/**
	 *
	 */
	public function getAllAgentPlayersOnLobby() {
		return $this->redis->keys($this->lobby['1']);
	}

	/**
	 *
	 */
	public function getAllCashPlayersOnLobby() {
		return $this->redis->keys($this->lobby['2']);
	}
	
	/**
	 * 
	 * @param string $playerName
	 * @return boolean
	 */
	public function isExistingOnSiteByPlayer($playerName) {
		return $this->redis->exists($this->login['0'] . $playerName) > 0 ? TRUE : FALSE;	
	}

	/**
	 * 
	 * @param string $playerName
	 * @return boolean
	 */
	public function isExistingOnLobbyByPlayer($playerName) {
		return $this->redis->exists($this->lobby['0'] . $playerName) > 0 ? TRUE : FALSE;
	}

}

?>
