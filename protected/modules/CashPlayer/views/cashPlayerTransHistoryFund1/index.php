<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_trans1').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Fund Transaction History Summary</a></li>");
</script>
<script type="text/javascript">
function showBonuspage() 
{
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryBonus1";
}

</script>

<script type="text/javascript">

	var cRowNo=0;
	function tableTransHistoryToday(currency_id,dateFrom,dateTo,testChecked,accountId,optSelect,transItem) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1/CashPlayerTransHistoryFund1&currencyId='+currency_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&accountId='+accountId+'&optSel='+optSelect+'&transItem='+transItem, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['No','Account ID', 'Account Name','Currency','No of Transaction','Deposit Count','Deposit','Withdrawal Count','Withdrawal','<b>Total</b>'],
			    colModel: [
			        {name: 'no', index: 'no', width: 25,title:false,sortable:false},
					{name: 'player_id', index: 'player_id', width: 120, search:true,title:false,summaryType:'count', summaryTpl : '<label style="color:red">Total<label>',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
					{name: 'account_name', index: 'account_name', width: 130,title:false},
					{name: 'currency_name', index: 'currency_name', width: 70,title:false},
					{name: 'trans_number', index: 'trans_number', width: 130, align:"right",title:false,sorttype:'number', summaryType:'sum'},
					{name: 'deposit_count', index: 'deposit_count', width: 70, align:"right",title:false,sorttype:'number', summaryType:'sum'},
					{name: 'deposit_amount', index: 'deposit_amount', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'withdrawal_count', index: 'withdrawal_count', width: 70, align:"right",title:false,sorttype:'number', summaryType:'sum'},
					{name: 'withdrawal_amount', index: 'withdrawal_amount', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'Total', index: 'Total', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
			    ],
			    loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list1");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFundDetail&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo)+"&opt="+optSelect+"&item="+transItem+'&s_id='+document.getElementById("txtAccountId").value;
			                }
			                e.preventDefault();
			            });
				    
			        } 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"player_id","",{'text-decoration':'underline'});
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
					}     
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
	
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">FUND</label> TRANSACTION HISTORY - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});

			jQuery("#list1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'deposit_count', numberOfColumns: 6, titleText: '<b>FUND</b>'},
				
				  ]
			});
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export Current to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportFundToExcel(optSelect);
	           }, 
	            position:"last"
	        }); 
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export All to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllFundToExcel(currency_id,dateFrom,dateTo,testChecked,accountId,optSelect,transItem);
	           }, 
	            position:"last"
	        });   
			  
		});
	} 


	function tableTransHistoryTodayTotal(currency_id,dateFrom,dateTo,testChecked,accountId,optSelect,transItem) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list2'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultTotal").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager2'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultTotal").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#list2");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1/CashPlayerTotalTransHistoryFund1&currencyId='+currency_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&accountId='+accountId+'&optSel='+optSelect+'&transItem='+transItem, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['<label><b>Currency</b></label>','<label><b>No of Transaction</b></label>','<b>Deposit</b>','<b>Withdrawal</b>','<b>Total</b>','Ex-Rate','UC Deposit','UC Withdraw','UC Total'],
			    colModel: [
					{name: 'currency_name', index: 'currency_name', width: 95,title:false,summaryType:'count',summaryTpl : 'Total'},
					{name: 'trans_number', index: 'trans_number', width: 130, align:"right",title:false,sorttype:'number', summaryType:'sum'},
					{name: 'deposit_amount', index: 'deposit_amount', width: 120, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'withdrawal_amount', index: 'withdrawal_amount', width: 120, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'Total', index: 'Total', width: 120, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'rate', index: 'rate', width: 95,title:false,hidden:true},
					{name: 'deposit_amount1', index: 'deposit_amount1', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'withdrawal_amount1', index: 'withdrawal_amount1', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'Total1', index: 'Total1', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
				    ],
			   	loadtext:"",
			   	loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"currency_name","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"trans_number","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"deposit_amount","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"withdrawal_amount","",{background:'#D84A38',color:'yellow'});
				    	grid.jqGrid('setCell',i,"Total","",{background:'#D84A38',color:'white'});
  	    
					}   
			    },
			    rowNum: 99999999,	
			    //rowList: [4, 8, 12],
			    hidegrid: false,
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">FUND</label> TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convert this to </label><select id="currencyTypeConvert" style="width:80px" onchange="javascript: convert();"><option value="0-0">--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>',
			    viewrecords: true,
			    footerrow:true,
			});
			//$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search: false});
			
		});
	   
	} 
	function convert(){
    	var comboValue=document.getElementById("currencyTypeConvert").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	
		
    	if (comboRange==0){
    		 $('#list2').trigger("reloadGrid");
    		 $('#list2').jqGrid('footerData','set', {trans_number:'0',deposit_amount:'',withdrawal_amount:'',Total: ''}); 
    		 return false;
        }
		var noRow=$("#list2").getGridParam("reccount");
		var rate;
		var deposit;
		var withdraw;
		var total;
		var depositConversion;
		var withdrawConversion;
		var totalConversion;

	    var x=1;
	    while (x<=noRow)
	      {
	    	rate=$("#list2").getCell(x, 'rate');
	    	deposit=$("#list2").getCell(x, 'deposit_amount1');
	    	withdraw=$("#list2").getCell(x, 'withdrawal_amount1');
	    	deduction=$("#list2").getCell(x, 'deduction1');
	    	total=$("#list2").getCell(x, 'Total1');

	    		depositConversion=((parseFloat(deposit)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	withdrawConversion=((parseFloat(withdraw)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list2").jqGrid('setCell', x, 0, comboName);
	    		jQuery("#list2").jqGrid('setCell', x, 2, depositConversion);
	    		jQuery("#list2").jqGrid('setCell', x, 3, withdrawConversion);
	    	    jQuery("#list2").jqGrid('setCell', x, 4, totalConversion);
	
	     	x++;
	      }
	    var grid = $("#list2"),
        sumCount= grid.jqGrid('getCol', 'trans_number', false, 'sum');
        sumDeposit=grid.jqGrid('getCol', 'deposit_amount', false, 'sum');
        sumWithdraw=grid.jqGrid('getCol', 'withdrawal_amount', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'Total', false, 'sum');

        grid.jqGrid('footerData','set', {trans_number:sumCount,deposit_amount:sumDeposit,withdrawal_amount:sumWithdraw,Total: sumTotal});  
	}
</script>
<script type="text/javascript">

function exportFundToExcel(optSelect){
	var accountId = $('#txtAccountId')[0].value;
	var transItem ="";
	
	if (optSelect=='others'){
		transItem = $('#depositType')[0].value + '-' + $('#withdrawType')[0].value;
	}else if (optSelect=='bank'){
		transItem = $('#selectBank')[0].value + '-' + $('#selectBank1')[0].value;
	}else{ 
		transItem='ALL-ALL';
	}
	
	var currency = $('#currencyType')[0].value;
	testChecked=0;
	if (document.getElementById("chkTest").checked==true){
		 testChecked=1;
	}
	var testC	=	testChecked;
	var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
	var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
	
	var table1= document.getElementById('qry_result');
	var table2= document.getElementById('qry_resultTotal');
	var html1 = table1.outerHTML;
	var html2 = table2.outerHTML;
	document.forms[2].csvBuffer.value="";
	document.forms[2].txtParams.value="";
	document.forms[2].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+optSelect+'#'+accountId+'#'+transItem+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
	document.forms[2].csvBuffer.value=html1+''+html2 ;
    document.forms[2].method='POST';
    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1/FundExcel';  // send it to server which will open this contents in excel file
    document.forms[2].target='_top';
    document.forms[2].submit();
}

function exportAllFundToExcel(currency_id,dateFrom,dateTo,testChecked,accountId,optSelect,transItem){

    $.ajax({
		 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1/ExportAllFundTransactionHistory&currencyId='+currency_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&accountId='+accountId+'&optSel='+optSelect+'&transItem='+transItem,
		 async:true,
		 success: function(result) {
				var data =result.split("<BREAK>");
				
				var accountId = $('#txtAccountId')[0].value;
				var transItem ="";
				
				if (optSelect=='others'){
					transItem = $('#depositType')[0].value + '-' + $('#withdrawType')[0].value;
				}else if (optSelect=='bank'){
					transItem = $('#selectBank')[0].value + '-' + $('#selectBank1')[0].value;
				}else{ 
					transItem='ALL-ALL';
				}
				
				var currency = $('#currencyType')[0].value;
				testChecked=0;
				if (document.getElementById("chkTest").checked==true){
					 testChecked=1;
				}
				var testC	=	testChecked;
				var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
				var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';

				document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+optSelect+'#'+accountId+'#'+transItem+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
				document.forms[2].csvBuffer.value=data[0]+""+data[2] ;
				document.forms[2].method='POST';
			   	document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1/FundExcel';  // send it to server which will open this contents in excel file
			    document.forms[2].target='_top';
			   	document.forms[2].submit();
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
}

function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
var sChar=false;
function alphanumeric(inputtxt)  
{  
	var letters = /^[0-9a-zA-Z]+$/;  
	if(inputtxt.match(letters) || inputtxt=='')  
	{  
		sChar=false;  
	}else{  
		sChar=true;  
	}  
}  

function changeCurrency(){
	var lblExcept = document.getElementById('lblExcept');
	var chkTest = document.getElementById('chkTest');
	if (document.getElementById('currencyType').value != "ALL"){
		chkTest.style.visibility = 'hidden';
		lblExcept.style.visibility = 'hidden';
		chkTest.checked=false;
	 }else{
		chkTest.style.visibility = 'visible';
		lblExcept.style.visibility = 'visible';
     }
}
</script>

<script type="text/javascript">

	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

	function loadTransTable(selected_currency,dateFrom,dateTo,testChecked,accountId,optSelect,transItem){
		if (selected_currency=='ALL'){
			//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_currency,dateFrom,dateTo,testChecked,accountId,optSelect,transItem);
	    	//for fund total
	    	document.getElementById('qry_resultTotal').innerHTML='';
	    	tableTransHistoryTodayTotal(selected_currency,dateFrom,dateTo,testChecked,accountId,optSelect,transItem);
		}
	    else
	    {
	    	//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_currency,dateFrom,dateTo,testChecked,accountId,optSelect,transItem);
	    	//for fund total
	    	document.getElementById('qry_resultTotal').innerHTML='';
	    	tableTransHistoryTodayTotal(selected_currency,dateFrom,dateTo,testChecked,accountId,optSelect,transItem);
		}
	}

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{

			document.getElementById("chkTest").checked=true;
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			var option='';
			var itemSelected='';
			
			<?php if (isset($_GET['dateFrom'])==''){?>
				var mydate= new Date();
				mydate.setDate(mydate.getDate());
				var theyear=mydate.getFullYear();
				var themonth=mydate.getMonth()+1;
				var theday=mydate.getDate();
				
				var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
				var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
				var accountId=document.getElementById('txtAccountId').value;
				option='all';
				itemSelected='ALL-ALL';
				optSelectType('all');
				alphanumeric(accountId);
				
				if (sChar==false){
					loadTransTable("ALL",datefrom,dateto,testChecked,accountId,option,itemSelected);
				}
				
			<?php }else{?>
				var datefrom=enc('<?php echo $_GET['dateFrom'];?>');
				var dateto=enc('<?php echo $_GET['dateTo'];?>');
				var id=enc('<?php echo $_GET['id'];?>');
				var opt=enc('<?php echo $_GET['opt'];?>');
				var item=enc('<?php echo $_GET['item'];?>');
				var itemSplit=item.split("-");
				
				var from=datefrom.split("_");
				var to=dateto.split("_");
				document.getElementById('datefrom').value=from[0].replace("/","-").replace("/","-");
				document.getElementById('dateto').value=to[0].replace("/","-").replace("/","-");
				document.getElementById('txtAccountId').value=id;
				var accountId=id;
				if (opt=='bank'){
					document.getElementById('radAll').checked = false;
					document.getElementById('radBank').checked = true;
					document.getElementById('radOther').checked = false;
					option='bank';
					optSelectType('bank');
					document.getElementById('selectBank').value=itemSplit[0];
					document.getElementById('selectBank1').value=itemSplit[1];
					itemSelected=item;
					loadTransTable("ALL",datefrom,dateto,testChecked,accountId,option,itemSelected);
				}else if (opt=='others'){
					document.getElementById('radAll').checked = false;
					document.getElementById('radBank').checked = false;
					document.getElementById('radOther').checked = true;
					option='others';
					optSelectType('other');
					document.getElementById('depositType').value=itemSplit[0];
					document.getElementById('withdrawType').value=itemSplit[1];
					itemSelected=item;
					loadTransTable("ALL",datefrom,dateto,testChecked,accountId,option,itemSelected);
				}else{
					document.getElementById('radBank').checked = false;
					document.getElementById('radOther').checked = false;
					document.getElementById('radAll').checked = true;
					option='all';
					optSelectType('all');
					itemSelected=item;
					loadTransTable("ALL",datefrom,dateto,testChecked,accountId,option,itemSelected);
				}
				//itemSelected='ALL-ALL';
				
			<?php }?>
			
		}
		else if (btnname=="Today")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var currencytype=document.getElementById('currencyType').value;
			var accountId=document.getElementById('txtAccountId').value;
			var optSelect=$('input[name=radOption]:checked').val();
			if (optSelect=='bank'){
				transItem=$('#selectBank').val()+'-'+$('#selectBank1').val();
			}else if (optSelect=='others'){
				transItem=$('#depositType').val()+'-'+$('#withdrawType').val();
			}else{
				transItem="ALL-ALL";
			}
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(currencytype,datefrom,dateto,testChecked,accountId,optSelect,transItem);
			}
		}
		else if (btnname=="Yesterday")
		{
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var currencytype=document.getElementById('currencyType').value;
			var accountId=document.getElementById('txtAccountId').value;
			var optSelect=$('input[name=radOption]:checked').val();
			if (optSelect=='bank'){
				transItem=$('#selectBank').val()+'-'+$('#selectBank1').val();
			}else if (optSelect=='others'){
				transItem=$('#depositType').val()+'-'+$('#withdrawType').val();
			}else{
				transItem="ALL-ALL";
			}
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(currencytype,datefrom,dateto,testChecked,accountId,optSelect,transItem);
			}
		}
		else if (btnname=="Submit")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var currencytype=document.getElementById('currencyType').value;
			var accountId=document.getElementById('txtAccountId').value;
			var optSelect=$('input[name=radOption]:checked').val();
			if (optSelect=='bank'){
				transItem=$('#selectBank').val()+'-'+$('#selectBank1').val();
			}else if (optSelect=='others'){
				transItem=$('#depositType').val()+'-'+$('#withdrawType').val();
			}else{
				transItem='ALL-ALL';
			}
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(currencytype,dateSubmitFrom,dateSubmitTo,testChecked,accountId,optSelect,transItem);
			}
			
		}
	}

	function optSelectType(val){
		if (val=="bank"){
			//document.getElementById('selectBank').value='ALL';
			//document.getElementById('selectBank1').value='15';
			document.getElementById('rwBank').style.display='table-row';
			document.getElementById('rwBank1').style.display='table-row';
			document.getElementById('rwDepositType').style.display='none';
			document.getElementById('rwWithdrawType').style.display='none';
		}else if (val=="others"){
			//document.getElementById('depositType').value='ALL';
			//document.getElementById('withdrawType').value='ALL';
			document.getElementById('rwBank').style.display='none';
			document.getElementById('rwBank1').style.display='none';
			document.getElementById('rwDepositType').style.display='table-row';
			document.getElementById('rwWithdrawType').style.display='table-row';
		}else{
			document.getElementById('rwBank').style.display='none';
			document.getElementById('rwBank1').style.display='none';
			document.getElementById('rwDepositType').style.display='none';
			document.getElementById('rwWithdrawType').style.display='none';
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
input[type="radio"], input[type="checkbox"]{
margin: 0px 0 0!important;
}
</style>
</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area" style="width: 500px">
	<div class="header" >Cash Player Transaction History</div>
	<form action="">
		<table style="background-color:transparent;  width: 500px;">
			<tr><td style=" height: 35px; border-bottom: groove; ">
					<input class="btn red disabled" type="button" value="FUND">
					<input class="btn red" type="button" value="BONUS" onclick="showBonuspage()">
				</td>
			</tr>
		</table>
		<table>
			<tr>
			<td style="padding-left: 5px;">TRANSACTION TYPE:</td>
			<td><input class="radio" type="radio" name="radOption" id="radAll" Checked onclick="javascript:optSelectType('all');btnclick('Submit');" value="all">ALL
            <input class="radio"  type="radio" name="radOption" id="radBank" onclick="javascript:optSelectType('bank');btnclick('Submit');" value="bank">BANKS
			<input class="radio"  type="radio" name="radOption" id="radOther" onclick="javascript:optSelectType('others');btnclick('Submit');" value="others">OTHERS</td>
			</tr>
			<tr id="rwBank" style="display: none;">
			<td  style="padding-left: 5px;"><label id="lblSelectBank">Deposit Bank</label></td>
			<td ><select id="selectBank" >
						<option value="ALL">ALL</option>
		  				<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='id,transaction_item';  
		  				$criteria->condition='item_type=:type and transaction_bank=:transBank';
		  				$criteria->params=array(':type'=>2,':transBank'=>1);
						$dataReader = TableTransactionItem::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['transaction_item']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr id="rwBank1" style="display: none;">
			<td  style="padding-left: 5px;"><label id="lblSelectBank">Withdrawal Bank</label></td>
			<td><select id="selectBank1">
						<option value="ALL">ALL</option>
		  				<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='id,transaction_item';  
		  				$criteria->condition='item_type=:type and transaction_bank=:transBank';
		  				$criteria->params=array(':type'=>4,':transBank'=>1);
						$dataReader = TableTransactionItem::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['transaction_item']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr id="rwDepositType" style="display: none;">
			<td style="padding-left: 5px;"><label id="lblDepositType">Deposit Type</label></td>
			<td><select id="depositType" >
						<option value="ALL">ALL</option>
		  				<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='id,transaction_item';
		  				$criteria->condition='item_type=:type and transaction_bank=:transBank';
		  				$criteria->params=array(':type'=>2,':transBank'=>0);
						$dataReader = TableTransactionItem::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['transaction_item']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr id="rwWithdrawType" style="display: none;">
			<td  style="padding-left: 5px;"><label id="lblWithdrawType">Withdraw Type</label></td>
			<td><select id="withdrawType" onChange="javascript:changeCurrency();">
						<option value="ALL">ALL</option>
		  				<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='id,transaction_item';
		  				$criteria->condition='item_type=:type and transaction_bank=:transBank';
		  				$criteria->params=array(':type'=>4,':transBank'=>0);
						$dataReader = TableTransactionItem::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['transaction_item']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr><td style="padding-left: 5px;">ACCOUNT ID:</td>
				<td>
					<input type="text" id="txtAccountId" style="width: 130px">
				</td>
			</tr>
			<tr><td style="padding-left: 5px;">CURRENCY:</td>
				<td><select id="currencyType" onChange="javascript:changeCurrency();">
						<option value="ALL">ALL</option>
		  				<?php 
						$dataReader = CashPlayerCurrencyType::model()->findAll();
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
						}
						?>
					</select>&nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="chkTest" checked ><label id="lblExcept">Except TEST currency</label>

				</td>
			</tr>
			<tr><td style="padding-left: 5px;">FROM :</td>
				<td><input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />
						<select id="cbHourfrom">
							<?php 
								$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
								echo '<option>'.$value.'</option>';
								}
							?>
						</select>
					<input value=":00:00" style="border: 0px; background-color: transparent ; width: 50px" disabled>
				</td></tr>
			<tr><td style="padding-left: 5px;">TO :</td>
				<td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
						<select id="cbHourto">
							<?php 
								$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
								echo '<option selected="true">'.$value.'</option>';
								}
							?>
						</select>
					<input value=":59:59" style="border: 0px; background-color: transparent; width: 50px" disabled >
				</td>
			</tr>
			</table>
			<div align="center" style="height: 35px; v-align: middle; padding-top: 5px;">
				<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
				<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
				<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
				<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red">
			</div>
	
	</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		month=month.toString();
		day=day.toString();
		
		if (month.length==1){
			month='0'+month;
		}
		if (day.length==1){
			day='0'+day;
		}
		document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
		document.getElementById("dateto").value=(year + "-" + month + "-" + day);
		</script>

</div>		
	<div id="qry_result"></div>
	<div id="pager1"></div>
	<br/>
	<div id="qry_resultTotal"></div>

	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
	</form>
</body>
</html>