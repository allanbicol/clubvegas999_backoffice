<html>
<head>
	<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/gamesettinglog.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		//active menu color
		document.getElementById('tablelimitHeader').className="start active";
		document.getElementById('mnu_table_limit').className="active";
		$('.breadcrumb').html("<li><i class='icon-home'></i>Game Setting <i class='icon-angle-right'></i></li><li><a href='#'>Table Limit Log</a></li>");
	</script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		    jQuery("#dtFrom").datepicker({
		        changeMonth: true,
		        changeYear: true,
		        dateFormat: "yy-mm-dd"
		        //dateFormat: "dd-mm-yy"
		    });
		
		    jQuery("#dtTo").datepicker({
		        changeMonth: true,
		        changeYear: true,
		        dateFormat: "yy-mm-dd"
		    });
		});
		function tableLog(logOperating,dtfrom,dtto) { 
			document.getElementById('qry_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list2");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitLog/TableLimitLog&logOpt='+ logOperating +'&dtfrom='+ dtfrom + '&dtto=' + dtto, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['Operated by','Level','Operated','Level','Operation Time','Detail'],
				    colModel: [
				      {name: 'operated_by', index: 'operated_by', width: 100, sortable: false,title:false},
				      {name: 'operated_by_level', index: 'operated_by_level', width: 100, sortable: false,title:false},
				      {name: 'operated', index: 'operated', width: 100, sortable: false,title:false},
				      {name: 'operated_level', index: 'operated_level', width: 100, sortable: true,title:false},
				      {name: 'operation_time', index: 'operation_time', width: 125, sortable: true,title:false},
				      {name: 'log_details', index: 'log_details', width: 800, sortable: false,title:false},
				      
				    ],
				    loadtext:"",
				    loadComplete: function() {
				        var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	grid.jqGrid('setCell',i,"log_details","",{background:'#f7f7c8'});
					    	grid.jqGrid('setCell',i,"operated_by","",{'font-weight':'bold'});
							grid.jqGrid('setCell',i,"operated_by","",{color:'#754719'});	
						}   
					    $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    },
				    rowNum: 25,	
				    rowList: [25, 50, 75],	
                    rownumbers: true,
                    sortable: true,
				    sortname: 'operation_time',
				    sortorder: 'DESC',
				    pager:'pager1',
				    caption: '<b>Logs</b> <input type="text" name="" id="txtDate" readonly="true">',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list2').jqGrid('navGrid', '#pager1', {search:false,edit: false, add: false, del:false});
			});
		} 
		
		function submitComplete()
		{
			//alert(obj.name);
			var dtfrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
			var dtto=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
			tableLog(document.getElementById('cmbOperating').value,dtfrom,dtto);
			document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
			document.getElementById('btnSubmit').name=document.getElementById('cmbOperating').value;
			
		}
		
		function return_to_main()
		{
			var str_date=document.getElementById('txtDDate').value.split(" >> ");
			tableLog(document.getElementById('btnSubmit').name,str_date[0],str_date[1]);
			document.getElementById('txtDate').value=str_date[0] + " >> " + str_date[1];
			
		}
	</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/3d-loader.gif");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
.ui-jqgrid tr.jqgrow td {
        word-wrap: break-word; /* IE 5.5+ and CSS3 */
        white-space: pre-wrap; /* CSS3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        overflow: hidden;
        height: auto;
        vertical-align: middle;
        padding-top: 3px;
        padding-bottom: 3px
    }
</style>
</head>
<body onload="javascript: submitComplete();">
<div id="body_wraper">
	<div id="transaction_parameter_box">
		<div class="header">LOGS</div>
		<div class="body">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left">Operating</td>
			<td class="right">
				<Select name="cmbOperating" id="cmbOperating">
					<option value="0">All</option>
					<option value="1"><?php echo Yii::app()->session['account_id'];?></option>
				</Select>
			</td></tr>
			<tr><td class="left">FROM</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtFrom" name="dtFrom" class="dtText">
				<select id="cmbTime1">
				<?php 
					for($i=0;$i<=23;$i++){
						echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
					}
				?>
				</select> :00:00
			</td></tr>
			<tr><td class="left">
				TO
			</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtTo" class="dtText">
				<select id="cmbTime2">
				<?php 
					for($i=0;$i<=23;$i++){
						if($i<23){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}else{
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					}
				?> 
				</select> :59:59
			</td>
			</tr>
		</table>
		
		
		</div>
		<div class="row">
		</div>
		<center><input class="btn red" id="btnSubmit" name="" type="button" value="Submit" onclick="javascript: submitComplete();"> <input class="btn red" type="button" value="Back" onclick="javascript: history.go(-1);"></center>
		<br/>
	</div>
	<div id="qry_result">
	<div id="pager1"></div>
	</div>
</div>
</body>
</html>