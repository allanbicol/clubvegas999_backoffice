<?php
/**
 * @todo bind tbl_game_bacarrat_limit to TableGameBacarratLimit CActiveRecord
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-28
 */
class TableGameBacarratLimit extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_game_baccarat_limit';
	}
}