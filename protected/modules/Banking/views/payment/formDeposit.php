	<table class="bank-withdraw">
		<thead>
			<tr>
				<td style="width: 200px;">
					
				</td>
				<td style="width: 250px;">
					
				</td>
			</tr>
		</thead>
		<tbody>
			<tr style="background-color: #FFFACD">
				<td>
					<label>Trans: <?php echo $deposit[0]['transaction_number'];?></label>
				</td>
				<td>
					<label>Date: <?php echo $deposit[0]['transaction_date'];?></label>
				</td>
			</tr>			
			<tr>
				<td>
					<label>Account ID:</label>			
				</td>
				<td>
					<label><?php echo $deposit[0]['account_id'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Full Name:</label>			
				</td>
				<td>
					<label><?php echo $deposit[0]['fullname'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Email:</label>			
				</td>
				<td>
					<label><?php echo $deposit[0]['email'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Phone Number:</label>			
				</td>
				<td>
					<label><?php echo $deposit[0]['phone_number'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Deposit Amount:</label>		
				</td>
				<td>
					<label><?php echo $deposit[0]['amount'];?></label>
				</td>
			</tr>			
			<tr>
				<td>
					<label>Currency:</label>
				</td>
				<td>
					<label><?php echo $deposit[0]['currency_name'];?></label>
				</td>
			</tr>	
			<tr>
				<td align="center" colspan="2" style="padding-top: 15px;">					
						<input class="btn red" type="submit" value="OK" name="yt0" id="depositConfirm">					
				</td>
			</tr>
		</tbody>
	</table>
		
	<script type="text/javascript">
		$('#depositConfirm').click(function(event) {
			$("#deposit-dialog").dialog("close");
			return false;
		});
	</script>