<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/statpopup.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/userlist.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		userList('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/UserList/UserList',
			'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/UserStatus/UpdateUserStatus',
			'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CreateUser');
	});
</script>
</head>
<body onload="javascript: createDiv();hideDiv('dvStatus')">
	<b>User Account List</b><br><br>
	<table id="listlog" ><tr><td/></tr></table>
	<div id="pager2"></div>
</body>
</html>