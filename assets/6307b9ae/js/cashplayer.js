
/*
 * @todo forceLogOut dialog box
 * @author leokarl
 * @date 2012-12-07
 */
function forceLogOutDialog(player_id){
	$('#accountIDLogout').val(player_id);
	$createForceLogoutDialog.dialog('open');
	$('#imgLogout').hide();
	$(".ui-dialog-titlebar-close").hide();
	//document.getElementById("submitWithdraw").disabled = false;
	$("#submitWithdraw").attr("disabled", false);
	return false;
} 
