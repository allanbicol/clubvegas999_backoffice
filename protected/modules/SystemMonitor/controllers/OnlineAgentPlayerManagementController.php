<?php

class OnlineAgentPlayerManagementController extends MyController
{
	private $onlinePlayer;
	public function actionGetOnlineAgentPlayer()
	{
		$this->onlinePlayer = new RedisManager();//CV999OnlinePlayersYii();
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : '';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : '';
		
		$records=$this->onlinePlayer->getCountUnderAgent(Yii::app()->session['account_id']);
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records=$this->onlinePlayer->getAllAgentPlayersJSONToJQGrid(Yii::app()->session['account_id']);
		echo JsonUtil::jsonRedisData($player_records, $total_pages, $page, $records);
	}
	/**
	 * @todo actionForceLogOutPlayer
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-26
	 */
	public function actionForcePlayerLogOut()
	{
		if (User::getUserType() === Constants::ROLE_AGENT)
		{
			$model=new RedisManager();//OnlineUserLogout();
			$model->forcePlayerLogout();
		}
	}
	/**
	 * @todo GetCasinoNameFromRedisByPlayerID
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-26
	 */
	public function actionGetCasinoNameFromRedisByPlayerID(){
		$model=new RedisManager();//OnlineUserLogout();
		echo $model->getCasinoNameFromRedisByPlayerID($_POST['player_id']);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemMonitor.readOnlineAgentPlayerManagement') || User::getUserType() === Constants::ROLE_AGENT)
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}