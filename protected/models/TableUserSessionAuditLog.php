<?php
/**
 * @todo bind tbl_user_session_audit_log to Level CActiveRecord
 * @copyright CE
 * @author Allan
 * @since 2013-04-09
 */
class TableUserSessionAuditLog extends CActiveRecord //CV99ActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_user_session_audit_log';
	}
}