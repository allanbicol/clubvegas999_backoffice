<?php

/**
 * @todo CreateUser Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-07-20
 */

class CreateUser
{
 
    /**
     * @todo makeNewUser
     * @copyright CE
     * @author Leo karl
     * @since 2012-07-20
     */
	public function makeNewUser()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		$command2 = $connection->createCommand("SELECT level_desc from tbl_level  WHERE id='" . $_GET['Level'] . "' ");
		$rd2=$command2->queryRow();
		$levelnameA=$rd2['level_desc'];

		if ($_GET['IsEnable']==1){
			$statusA='Active';
		}else{$statusA='Closed';}
			
			
		$countAccountID=0;
		$countAccountName=0;
		if($_GET['s_edit']==0){
			$countAccountID=User::model()->count(array('select'=>'account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['AccountID']),));
			$countAccountName=User::model()->count(array('select'=>'account_name','condition'=>'account_name=:account_name','params'=>array(':account_name'=>$_GET['AccountName']),));
			//if ($countAccountID==0 && $countAccountName==0)
			if ($countAccountID==0)
			{
				$post=new User();
				$post->account_id=$_GET['AccountID'];
				$post->account_name=$_GET['AccountName'];
				$post->password= CV999Utility::generateOneWayPassword($_GET['Password']);
				$post->is_enable=$_GET['IsEnable'];
				$post->level_id=$_GET['Level'];
				$post->opening_date_time=new CDbExpression('NOW()');
				$post->max_deposit=$_GET['MaxDeposit'];
				$post->max_withdraw=$_GET['MaxWithdraw'];

				$post->black_list = $_GET['black_list'];
				$post->white_list_ip_address = $_GET['white_list_ip_address'];
				$post->white_list_ip_range_address = $_GET['white_list_ip_range_address'];
				$post->save();
				
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level=$levelnameA;
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=1;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ADDED new user account:<label style=\"color:red\">'.$_GET['AccountID'].'</label> as <label style=\"color:red\">'.$levelnameA.'</label>.</b>';
				$postLog->save();
			}
		}else{
			
			//Get previous user account detail- Added by Allan |date:2012-06-30
			$command = $connection->createCommand("SELECT * from tbl_user  WHERE account_id='" . $_GET['AccountID'] . "' ");
			$rd=$command->queryRow();
			$name=$rd['account_name'];
			$password=$rd['password'];
			$status=$rd['is_enable'];
			$levelid=$rd['level_id'];
			$maxDeposit=$rd['max_deposit'];
			$maxWithdraw=$rd['max_withdraw'];
			
			if ($status==1){
				$statusB='Active';
			}else{$statusB='Closed';}
			
			$command1 = $connection->createCommand("SELECT level_desc from tbl_level  WHERE id='" . $levelid . "' ");
			$rd1=$command1->queryRow();
			$levelnameB=$rd1['level_desc'];
			
			//-------------------------------------------------------------------
				
			$post=User::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['AccountID']),));
			//$post=TableAgent::model()->find(array('select'=>'account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
			$post->account_id=$_GET['AccountID'];
			$post->account_name=$_GET['AccountName'];
			if(trim($_GET['Password'])!=''){
				$post->password= CV999Utility::generateOneWayPassword($_GET['Password']);
			}
			$post->is_enable=$_GET['IsEnable'];
			$post->level_id=$_GET['Level'];
			//$post->opening_date_time=new CDbExpression('NOW()');
			$post->max_deposit=$_GET['MaxDeposit'];
			$post->max_withdraw=$_GET['MaxWithdraw'];
			$post->save();
			
			if ($name!=$_GET['AccountName']){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level=$levelnameA;
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=1;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE user account name from:<label style=\"color:red\">'.$name.'</label> to:<label style=\"color:red\">'.$_GET['AccountName'].'</label>.</b>';
				$postLog->save();
			}
			if ($password!=CV999Utility::generateOneWayPassword($_GET['Password']) && trim($_GET['Password'])!=''){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level=$levelnameA;
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=10;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE user password.</b>';
				$postLog->save();
			}
			if ($status!=$_GET['IsEnable']){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level=$levelnameA;
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=8;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE user status from:<label style=\"color:red\">'.$statusB.'</label> to:<label style=\"color:red\">'.$statusA.'</label>.</b>';
				$postLog->save();
			}
			if ($level!=$_GET['Level']){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level=$levelnameA;
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=15;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE user level from:<label style=\"color:red\">'.$levelnameB.'</label> to:<label style=\"color:red\">'.$levelnameA.'</label>.</b>';
				$postLog->save();
			}
			if($maxDeposit!=$_GET['MaxDeposit']){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level=$levelnameA;
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=3;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE user max deposit from:<label style=\"color:red\">'.$maxDeposit.'</label> to:<label style=\"color:red\">'.$_GET['MaxDeposit'].'</label>.</b>';
				$postLog->save();
			}
			if ($maxWithdraw!=$_GET['MaxWithdraw']){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level=$levelnameA;
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=2;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE user max withdraw from:<label style=\"color:red\">'.$maxWithdraw.'</label> to:<label style=\"color:red\">'.$_GET['MaxWithdraw'].'</label>.</b>';
				$postLog->save();
			}
			
			echo $_GET['AccountID'];
		}
		echo $countAccountID . ";" . $countAccountName;
	}
	
	/**
	 * @todo	Change user password
	 * @copyright	CE
	 * @author	Kimny MOUK
	 * @since	2012-05-14
	 * @param string $accountID
	 * @param string $oldPassword
	 * @param string $newPassword
	 * @param string $confirmPassword
	 */
	public static function changePassword($accountID, $oldPassword, $newPassword, $confirmPassword)
	{
		$connection=Yii::app()->db_cv999_fd_master;

		$account_type = Yii::app()->session['account_type'];
		if ($account_type == "user")
		{	
			$command=$connection->createCommand("select account_id from tbl_user where account_id=:account_id and password=:password");
			$command->bindParam(":account_id",$accountID,PDO::PARAM_STR);
			$command->bindParam(":password",$oldPassword,PDO::PARAM_STR);
			$user = $command->queryRow();
			
			if ($user['account_id'] != null)
			{
				if ($newPassword == $confirmPassword)
				{
					$command=$connection->createCommand("update tbl_user set password=:password where account_id=:account_id");
					$command->bindParam(":account_id",$accountID,PDO::PARAM_STR);
					$command->bindParam(":password",$newPassword,PDO::PARAM_STR);
					$command->execute();
					echo "Your password has been changed!";
				}
				else 
					echo "Password is not match!";
			}
			else 
				echo "Old password is not correct!";
		}
		else if ($account_type == "agent")
		{
			$command=$connection->createCommand("select account_id from tbl_agent where account_id=:account_id and agent_password=:agent_password");
			$command->bindParam(":account_id",$accountID,PDO::PARAM_STR);
			$command->bindParam(":agent_password",$oldPassword,PDO::PARAM_STR);
			$user = $command->queryRow();
				
			if ($user['account_id'] != null)
			{
				if ($newPassword == $confirmPassword)
				{
					$command=$connection->createCommand("update tbl_agent set agent_password=:password where account_id=:account_id");
					$command->bindParam(":account_id",$accountID,PDO::PARAM_STR);
					$command->bindParam(":password",$newPassword,PDO::PARAM_STR);
					$command->execute();
					echo "Your password has been changed!";
				}
				else
					echo "Password is not match!";
			}
			else
				echo "Old password is not correct!";
		}
		
	}
}