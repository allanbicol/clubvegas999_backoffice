<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>CE | Admin</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS --> 
  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.2.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS 

	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<!--<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>  
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>-->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.sparkline.min.js" type="text/javascript"></script>  
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/app.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/index.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/tasks.js" type="text/javascript"></script>  
	<!--<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/jquery.jscrollpane.min.js" type="text/javascript"></script>   
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/jquery.mousewheel.js" type="text/javascript"></script>  
	<!-- END PAGE LEVEL SCRIPTS -->  
	<script>
		jQuery(document).ready(function() {    
		   App.init(); // initlayout and core plugins
		   Index.init();
		   //Index.initJQVMAP(); // init index page's custom scripts
		   //Index.initCalendar(); // init index page's custom scripts
		   //Index.initCharts(); // init index page's custom scripts
		   //Index.initChat();
		   //Index.initMiniCharts();
		   //Index.initDashboardDaterange();
		   //Index.initIntro();
		   Tasks.initDashboardWidget();
		   //$('.page-sidebar').jScrollPane();
		});
	</script>
	<!-- END JAVASCRIPTS -->
	
	<!-- BEGIN GLOBAL MANDATORY STYLES -->        
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!--<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/jquery.jscrollpane.css" rel="stylesheet" type="text/css"/>
	 END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	<!--  <link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>-->
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/pages/tasks.css" rel="stylesheet" type="text/css" media="screen"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" />
<style type="text/css">
 #content  
 {
	height:500px;
 	padding-top: 10px; 
 	padding-left: 3px; 
	overflow:auto; 
	min-height: 100%;
 } 
  #content.overview {
            /*width: 1836px;*/
            /* or */
            white-space: nowrap;
        }
 body {
    overflow:hidden;
}
.container-fluid {
    padding-right: 1px!important;
}
 </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->   
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="#">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/resource/img/logo1.png" alt="logo" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/resource/img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->            
				<!-- BEGIN TOP NAVIGATION MENU --> 
				<form id="loginform" name="loginform" method="post" action="">
			    <input type="hidden" value="en" name="chooselan" id="chooselan" />             
				<ul class="nav pull-right">
					<!-- BEGIN LANGUAGE DROPDOWN -->
					<li class="dropdown language">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img id="langImg" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/<?php echo Yii::t('main','main.language.img');?>" />
						<span class="username"><?php echo Yii::t('main','main.language');?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a onclick="document.getElementById('chooselan').value='en';document.getElementById('loginform').submit();"  href="#"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/en.png" /> English</a></li>
							<li><a onclick="document.getElementById('chooselan').value='th';document.getElementById('loginform').submit();" href="#"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/th.png" /> Thai</a></li>
							<li><a onclick="document.getElementById('chooselan').value='vi';document.getElementById('loginform').submit();" href="#"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/vn.png" /> Vietnamese</a></li>
							<li><a onclick="document.getElementById('chooselan').value='zh_cn';document.getElementById('loginform').submit();" href="#"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/cn.png" /> Chinese</a></li>
							<li><a onclick="document.getElementById('chooselan').value='zh_hk';document.getElementById('loginform').submit();" href="#"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/hk.png" /> Hongkong</a></li>
							<li><a onclick="document.getElementById('chooselan').value='kr';document.getElementById('loginform').submit();" href="#"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/kr.png" /> Korean</a></li>
						</ul>
					</li>
					</form>
					<!-- END LANGUAGE DROPDOWN -->             
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/resource/img/avatar4_small.jpg" />
						<span class="username"><?php echo (Yii::app()->session['account_type']!='agent_alias') ? Yii::app()->session['account_id'] : Yii::app()->session['account_id'] . ' ('. Yii::app()->session['agent_alias_name'] .')';?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a><i class="icon-user"></i> <?php echo Yii::app()->session['level_name'];?></a></li>

							<li class="divider"></li>
							<li><a href="javascript:;" id="trigger_fullscreen"><i class="icon-move"></i> Full Screen</a></li>
							<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=SystemSetting/UserList/UserPassword"><i class="icon-lock"></i> <?php echo Yii::t('main','main.header.change_password');?></a></li>
							<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=Login/Logout"><i class="icon-key"></i> <?php echo Yii::t('main','main.header.logout');?></a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU --> 
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->

		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<ul class="page-sidebar-menu">
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<?php if( Yii::app()->user->checkAccess('agent.addNewSubCompany') ||
						Yii::app()->user->checkAccess('agent.readAgentOverview') ||
						Yii::app()->user->checkAccess('agent.readAgentParameters') ||
						Yii::app()->user->checkAccess('agent.readSlotWinLoss') ||
						Yii::app()->user->checkAccess('agent.readSubCompanyList') ||
						Yii::app()->user->checkAccess('agent.readTransactionHistory') ||
						Yii::app()->user->checkAccess('agent.readWinLoss') ||
						User::getUserType() === Constants::ROLE_AGENT
			    	){?>
			    		<li id="agentHeader" class=""><a href=""><i class="icon-group"></i> <span class="title"><?php echo Yii::t('main','main.agentsystem.title');?></span><span class="arrow "></span></a>
		        		<ul class="sub-menu">
		        			<?php if( Yii::app()->user->checkAccess('agent.readAgentOverview') ||	User::getUserType() === 'agent'){?>
							<li id="mnu_agent_info"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentInformation"><i class="icon-file-text"></i> <?php echo Yii::t('main','main.agentsystem.agent_overview');?></a></li>
							<?php }?>
							
							<?php
				        	$linkTitle='';
				        	$linkListTitle='';
							if(Yii::app()->user->checkAccess('agent.addNewSubCompany') || Yii::app()->user->checkAccess('agent.readSubCompanyList')){
								$linkTitle=Yii::t('main','main.agentsystem.new_sub_company');
								$linkListTitle=Yii::t('main','main.agentsystem.sub_company');
							}else if(Yii::app()->session['level']=='SC'){
								$linkTitle=Yii::t('main','main.agentsystem.new_senior_master_agent');
								$linkListTitle=Yii::t('main','main.agentsystem.senior_master_agent');
							}else if(Yii::app()->session['level']=='SMA'){
								$linkTitle=Yii::t('main','main.agentsystem.new_master_agent');
								$linkListTitle=Yii::t('main','main.agentsystem.master_agent');
							}else if(Yii::app()->session['level']=='MA'){
								$linkTitle=Yii::t('main','main.agentsystem.new_agent');
								$linkListTitle=Yii::t('main','main.agentsystem.agent');
							}else if(Yii::app()->session['level']=='AGT'){
								$linkTitle=Yii::t('main','main.agentsystem.new_player');
								$linkListTitle=Yii::t('main','main.agentsystem.player');
							}
				        	?>
				        	
				        	<?php if(Yii::app()->user->checkAccess('agent.addNewSubCompany') || User::getUserType() === 'agent'){?>
				            <li  id="mnu_new_edit_agent"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany"><i class="icon-plus"></i> <font><?php echo $linkTitle;?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readSubCompanyList') || User::getUserType() === 'agent'){?>
				            <li id="mnu_agent_list_info"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSubCompany"><i class="icon-reorder"></i> <font><?php echo $linkListTitle;?> <?php echo Yii::t('main','main.agentsystem.list');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readWinLoss') || User::getUserType() === 'agent'){?>
				            <li id="mnu_agent_winloss"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.agentsystem.winloss');?></font></a></li>
				            <?php }?>
				             <?php if(Yii::app()->user->checkAccess('agent.readWinLossV1')){?>
				            <li id="mnu_agent_winloss_v1"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLossV1"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.agentsystem.winloss');?> - Version 1</font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readSlotWinLoss') || User::getUserType() === 'agent'){?>
				            <li id="mnu_agent_slot_winloss"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.agentsystem.slot_winloss');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readSlotWinLossV1')){?>
				            <li id="mnu_agent_slot_winloss_v1"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLossV1"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.agentsystem.slot_winloss');?> - Version 1</font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readTransactionHistory') || User::getUserType() === 'agent'){?>
				            <li id="mnu_transaction_hist"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentTransactionHistory"><i class="icon-money"></i> <font><?php echo Yii::t('main','main.agentsystem.transaction_history');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readFundTransferHistory') || User::getUserType() === 'agent'){?>
				            <li id="mnu_afundTrans"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentFundTransferHistory"><i class="icon-money"></i> <font><?php echo Yii::t('main','main.agentsystem.fund_transfer_history');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readAgentParameters')){?>
				            <li id="mnu_agent_parameter"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentParameter"><i class="icon-tasks"></i> <font><?php echo Yii::t('main','main.agentsystem.agent_parameter');?></font></a></li>
				            <?php }?>
				        	<?php if(Yii::app()->user->checkAccess('agent.writeRevenueSharingAdjustment')){?>
				        	<li id="mnu_agent_revenue_sharing_adjustment"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentRevenueSharingAdjustment"><i class="icon-tasks"></i> <font><?php echo Yii::t('main','main.agentsystem.revenue_sharing_adjustment');?></font></a></li>
				        	<?php }?>
				        	<?php if(Yii::app()->user->checkAccess('agent.readTransactionHistory') || User::getUserType() === 'agent'){?>
				            <li id="mnu_agent_logs"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSubCompanyLogAll"><i class="icon-table"></i> <font><?php echo Yii::t('main','main.agentsystem.agent_logs');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('agent.readSettlementReport')){?>
				            <li id="mnu_agent_settlement_report"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/Settlement"><i class="icon-list-alt"></i> <font>Settlement Report</font></a></li>
				            <?php }?>
        	
						</ul>
	        			</li>
	        		<?php }?>
					
				<?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerOverview') ||
        			Yii::app()->user->checkAccess('cashPlayer.readCashPlayerList') ||
        			Yii::app()->user->checkAccess('cashPlayer.readCashPlayerInfo') ||
        			Yii::app()->user->checkAccess('cashPlayer.readWinLoss') ||
        			Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss') ||
        			Yii::app()->user->checkAccess('cashPlayer.readConsolidatedWinLoss') ||
        			Yii::app()->user->checkAccess('cashPlayer.readTransHistorySummary') ||
        			Yii::app()->user->checkAccess('cashPlayer.readTransHistoryDetails') ||
        			Yii::app()->user->checkAccess('cashPlayer.readCashPlayerParameters') 
        			) { ?>
        			
	        		<li id="cashplayerHeader" class=""><a href="javascript:;"><i class="icon-user"></i> <span class="title"><?php echo Yii::t('main','main.cashplayer.title') ;?></span><span class="arrow "></span></a>
	        		<ul class="sub-menu">
	        			<?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerOverview')) {?>
			        	<li id="mnu_cash_player_overview"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerOverview"><i class="icon-file-text"></i> <font><?php echo Yii::t('main','main.cashplayer.cashplayer_overview');?></font></a></li>
			        	<?php }?>
			        	<?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerList')) {?>
			            <li id="mnu_cash_player"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList"><i class="icon-reorder"></i> <font ><?php echo Yii::t('main','main.cashplayer.cashplayer_list');?></font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerInfo')) {?>
				        <li  id="mnu_cash_player_info"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.cashplayer.cashplayer_information');?></font></a></li>
			          	<?php }?>
			          	<?php if(Yii::app()->user->checkAccess('cashPlayer.readWinLoss')) {?>
			            <li id="mnu_winloss"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLoss"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.cashplayer.winloss');?></font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readWinLoss')) {?>
			            <li id="mnu_winloss_v1" ><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.cashplayer.winloss');?> - Version 1</font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss')) {?>
			            <li id="mnu_slot_winloss"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLoss"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.cashplayer.slot_winloss');?></font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss')) {?>
			            <li id="mnu_slot_winloss_v1"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.cashplayer.slot_winloss');?> - Version 1</font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readConsolidatedWinLoss')) {?>
			            <li id="mnu_consolidated_winloss"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss"><i class="icon-list-alt"></i> <font><?php echo Yii::t('main','main.cashplayer.consolidated_winloss');?></font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readTransHistorySummary')) {?>
			            <li id="mnu_trans1"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1" ><i class="icon-money"></i> <font><?php echo Yii::t('main','main.cashplayer.transaction_history_summary');?></font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readTransHistoryDetails')) {?>
			            <li  id="mnu_trans2"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory" ><i class="icon-money"></i> <font><?php echo Yii::t('main','main.cashplayer.transaction_history_detailed');?></font></a></li>
			            <?php }?>
			             <?php if(Yii::app()->user->checkAccess('cashPlayer.readFundTransferHistory')) {?>
            			<li  id="mnu_fundTrans"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerFundTransferHistory" ><i class="icon-money"></i> <font><?php echo Yii::t('main','main.agentsystem.fund_transfer_history');?></font></a></li>
            			<?php }?>
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerParameters')) {?>
			            <li id="mnu_cashplayer_param"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerMaxCommission"><i class="icon-tasks"></i> <font><?php echo Yii::t('main','main.cashplayer.cashplayer_parameter');?></font></a></li>
			            <?php }?>
			            
			            <?php if(Yii::app()->user->checkAccess('cashPlayer.readTransactionItem')) {?>
			            <li id="mnu_transaction_items"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/TransactionItems"><i class="icon-tasks"></i> <font><?php echo Yii::t('main','main.cashplayer.transaction_items');?></font></a></li>
			       		<?php }?>
	        		</ul>
	        		</li>
	        	 <?php  }?>	
	        	 
        		<?php if(Yii::app()->user->checkAccess('marketing.readCustomReport')) {?>
		       		<li id="marketingHeader" class=""><a href="javascript:;"><i class="icon-sitemap"></i>  <span class="title"><?php echo Yii::t('main','main.marketing.title');?></span><span class="arrow "></span></a>
		       		<ul class="sub-menu">
			       		<?php if(Yii::app()->user->checkAccess('marketing.readCustomReport')) {?>
			            <li  id="mnu_custom_report1"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1"><i class="icon-tasks"></i> <font><?php echo Yii::t('main','main.marketing.customreport');?></font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('marketing.readPromoItem')) {?>
			            <li  id="mnu_promo_item"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PromoItem"><i class="icon-tasks"></i> <font><?php echo yii::t('main','main.marketing.promo_items');?></font></a></li>
			            <?php }?>
			            <?php if(Yii::app()->user->checkAccess('marketing.readPlayerValueReport')) {?>
			            <li  id="mnu_player_value"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PlayerValueReport"><i class="icon-tasks"></i> <font><?php echo yii::t('main','main.marketing.player_value');?></font></a></li>
			         	<?php }?>
		         	</ul>
		         	</li>
	         	<?php }?>
	         	
        		<?php if(Yii::app()->user->checkAccess('banking.readPayment')) {?>
        			<li id="bankingHeader" class=""><a href="javascript:;"><i class="icon-briefcase"></i> <span class="title"><?php echo Yii::t('main','main.banking.title') ;?></span><span class="arrow "></span></a>
			        	<ul class="sub-menu">
				        	<?php if(Yii::app()->user->checkAccess('banking.readPayment')) {?>
				        	<li id="mnu_payment"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment"><i class="icon-money"></i> <font ><?php echo Yii::t('main','main.banking.payment');?></font></a></li>
				        	<?php }?>
						</ul>
		        	</li>
				<?php }?>
				
	        	<?php if(Yii::app()->user->checkAccess('gameSetting.readTableLimit','gameSetting.readTableLimitLog','gameSetting.writeTableLimit')) {?>
	        	<li id="tablelimitHeader" class=""><a href="javascript:;"><i class="icon-cog"></i> <span class="title"><?php echo Yii::t('main','main.gamesetting.title');?></span><span class="arrow "></span></a>
			        	<ul class="sub-menu">
				        	<?php if(Yii::app()->user->checkAccess('gameSetting.readTableLimit')) {?>
				            <li id="mnu_table_limit"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList"><i class="icon-table"></i> <font ><?php echo Yii::t('main','main.gamesetting.table_limit');?></font></a></li>
				        	<?php }?>
			        	</ul>
		        	</li>
	        	<?php }?>
	        	
	        	<?php if(Yii::app()->user->checkAccess('systemMonitor.readOnlineUsers') || User::getUserType() === 'agent') {?>
	            	<li id="systemmonitorHeader" class=""><a href="javascript:;"><i class="icon-play"></i> <span class="title"><?php echo Yii::t('main','main.systemmonitor.title');?></span><span class="arrow "></span></a>
		            	<ul class="sub-menu">
					    <?php if(Yii::app()->user->checkAccess('systemMonitor.readOnlineUsers')) {?>
			            <li  id="mnu_online_management"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement"><i class="icon-user"></i> <font><?php echo Yii::t('main','main.systemmonitor.online_users');?></font></a></li>
			        	<?php }?>
			        	<?php if(Yii::app()->user->checkAccess('systemMonitor.readInstantPools') || User::getUserType() === Constants::ROLE_AGENT) {?>
			            <li id="mnu_instant_pools"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPoolsMB01"><i class="icon-time"></i> <font><?php echo Yii::t('main','main.systemmonitor.instant_pools');?></font></a></li>
			        	<?php }?>
			        	<?php if (User::getUserType() === Constants::ROLE_AGENT) {?>
						<li id="mnu_online_management"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement"><i class="icon-user"></i> <font><?php echo Yii::t('main','main.systemmonitor.online_agent_players');?></font></a></li>
	        			<?php }?>
	        	     </ul>
	        	</li>
        		<?php }?>
        	
	        	<?php if(Yii::app()->user->checkAccess('systemSetting.readCurrencySetting') ||
	    				Yii::app()->user->checkAccess('systemSetting.readTranslation') ||
	    				Yii::app()->user->checkAccess('systemSetting.readUserAccount') ||
	    				Yii::app()->user->checkAccess('systemSetting.readWhiteList')   ||
	    				Yii::app()->user->checkAccess('systemSetting.readAnnouncement')||
	    				Yii::app()->user->checkAccess('systemSetting.readWinnerList')
						) 
	    			  {?>
	    			  <li id="systemsettingHeader" class=""><a href="javascript:;"><i class="icon-cogs"></i> <span class="title"><?php echo Yii::t('main','main.systemsetting.title');?></span><span class="arrow "></span></a>
			        	<ul class="sub-menu">
				        	<?php if(Yii::app()->user->checkAccess('systemSetting.readCurrencySetting')) {?>
				            <li  id="mnu_currency_setting"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CurrencyList"><i class="icon-money"></i> <font><?php echo Yii::t('main','main.systemsetting.currency_setting');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('systemSetting.readEmailSetting')) {?>
				            <li  id="mnu_email_setting"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting"><i class="icon-envelope-alt"></i> <font><?php echo Yii::t('main','main.systemsetting.email_setting');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('systemSetting.readUserAccount')) {?>
				            <li  id="mnu_user_account"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/User">	<i class="icon-user"></i><font><?php echo Yii::t('main','main.systemsetting.user_account');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('systemSetting.readRoleAndPermission')) {?>
				            <li  id="mnu_user_role_information"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Role"><i class="icon-bookmark"></i> <font><?php echo Yii::t('main','main.systemsetting.role_and_permission');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('systemSetting.readTranslation')) {?>
				            <li  id="mnu_translation"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/translate/edit/admin"><i class="icon-bullhorn"></i> <font><?php echo Yii::t('main','main.systemsetting.translation');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('systemSetting.readWhiteList')) {?>
				            <li id="mnu_white_list"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WhiteList"><i class="icon-list"></i> <font><?php echo Yii::t('main','main.systemsetting.white_list');?></font></a></li>
					        <?php }?>
							<?php if(Yii::app()->user->checkAccess('systemSetting.readWinnerList')) {?>
				            <li  id="mnu_winner_list"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList"><i class="icon-trophy"></i> <font><?php echo Yii::t('main','main.systemsetting.winner_list');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('systemSetting.readAdvisory')) {?>
				            <li  id="mnu_advisory"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory"><i class="icon-warning-sign"></i> <font><?php echo Yii::t('main','main.systemsetting.advisory');?></font></a></li>
				            <?php }?>
				            <?php if(Yii::app()->user->checkAccess('systemSetting.readAnnouncement')) {?>
				            <li id="mnu_system_setting_announcement"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Announcement"><i class="icon-warning-sign"></i> <font><?php echo Yii::t('main','main.systemsetting.announcement');?></font></a></li>
				            <?php }?>
			            </ul>
		            </li>
	            <?php }?>
	            
	            <?php if (Yii::app()->user->checkAccess('securityLog.readLoginAudit') || Yii::app()->user->checkAccess('securityLog.readApplicationLogs') ) { ?>
	        		<li id="securitylogHeader" class=""><a href="javascript:;"><i class="icon-edit"></i> <span class="title"><?php echo Yii::t('main','main.security_logs.title');?></span><span class="arrow "></span></a>
		        		<ul class="sub-menu">
				        	<?php if (Yii::app()->user->checkAccess('securityLog.readLoginAudit')) {?>
				            	<li  id="mnu_login_audit"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/LoginAudit"><i class="icon-unlock"></i> <font><?php echo Yii::t('main','main.security_logs.login_audit');?></font></a></li>
					      	<?php } ?>
					      	<?php if (Yii::app()->user->checkAccess('securityLog.readPlayerLoginAudit')) {?>
				            	<li  id="mnu_player_login_audit"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/PlayerLoginAudit"><i class="icon-unlock"></i> <font><?php echo Yii::t('main','main.security_logs.player_login_audit');?></font></a></li>
					      	<?php } ?>
							<?php if (Yii::app()->user->checkAccess('securityLog.readApplicationLogs')) { ?>
				            	<li  id="mnu_application_logs"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/ApplicationLogs"><i class="icon-asterisk"></i> <font><?php echo Yii::t('main','main.security_logs.application_logs');?></font></a></li>
				            <?php } ?>
			            </ul>
		            </li>
	            <?php }?>
	            
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>

		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					Widget settings form goes here
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">   
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						
						<ul class="breadcrumb">
							<li id="breadcrumd1">
							</li>
							<li id="breadcrumd2"></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<?php 
						echo $content;
        			?>
        
				</div>
				
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<!-- 	<div class="footer"> -->
<!-- 		<div class="footer-inner"> -->

<!-- 		</div> -->
<!-- 		<div class="footer-tools"> -->
<!-- 			<span class="go-top"> -->
<!-- 			<i class="icon-angle-up"></i> -->
<!-- 			</span> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<!-- END FOOTER -->
<script type="text/javascript">
$( document ).ready(function() {
	setInterval(function(){getMonitorHeightWithNav();},50);
});     			


function getMonitorHeightWithNav(){
	var myWidth;
	var myHeight;
	
	if( typeof( window.innerWidth ) == 'number' ) { 
	
	//Non-IE 
	myWidth = window.innerWidth;
	myHeight = window.innerHeight; 

	} else if( document.documentElement && 

	( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) { 

	//IE 6+ in 'standards compliant mode' 
	myWidth = document.documentElement.clientWidth; 
	myHeight = document.documentElement.clientHeight; 

	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) { 

	//IE 4 compatible 
	myWidth = document.body.clientWidth; 
	myHeight = document.body.offsetHeight; 
	
	} 


		document.getElementById('content').style.width=(((myWidth/myWidth)*100)+"%"); //"84.7%"//
		

		document.getElementById('content').style.height=(myHeight-102)+"px";


}

</script>
</body>

<!-- END BODY -->
</html>