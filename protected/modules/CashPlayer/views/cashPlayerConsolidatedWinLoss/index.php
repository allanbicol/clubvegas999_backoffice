<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_consolidated_winloss').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Consolidated Win/Loss</a></li>");
</script>
<script type="text/javascript">

	var cRowNo=0;
	function tableConsolidatedWinLoss(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_consolidated").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_consolidated").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss/CashPlayerConsolidatedWinLoss&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked,
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','Account ID','Currency','Total Stake','Valid Stake','Average Bet','Win/Loss','Commission','Assigned','Available','Used','Tips','<b>Total</b>','<b>P/L</b>','Deposit','Bonus','Withdraw'],
			    colModel: [
			        {name: 'no', index: 'no', width: 25,title:false,sortable:false},
					{name: 'account_id', index: 'account_id', width: 120, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
					{name: 'currency_name', index: 'currency_name', width: 55,title:false},
					{name: 'total_stake', index: 'total_stake', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_stake">{0}</label>'},
					{name: 'amount_wager', index: 'amount_wager', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_wager">{0}</label>'},
					{name: 'avg_bet', index: 'avg_bet', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_bet">{0}</label>'},
					{name: 'win_los', index: 'win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="commission">{0}</label>'},
					{name: 'bonus_total', index: 'bonus_total', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="assigned_bonus">{0}</label>'},
					{name: 'bonus', index: 'bonus', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bonus">{0}</label>'},
					{name: 'bonus_used', index: 'bonus_used', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="used_bonus">{0}</label>'},
					{name: 'amount_tips', index: 'amount_tips', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_tips">{0}</label>'},
					{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'deposit', index: 'deposit', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="deposit">{0}</label>'},
					{name: 'bonus_total', index: 'bonus_total', width: 90, align:"right",hidden:true,title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bonus_total">{0}</label>'},
					{name: 'withdraw', index: 'withdraw', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="withdraw">{0}</label>'},
			    ],

				loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list1");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLossDetail&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo)+"&id="+enc(document.getElementById('txtAccountId').value);
				                }

			                e.preventDefault();
			            });
			        } 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"amount_tips","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
				    	grid.jqGrid('setCell',i,"deposit","",{background:'#CAB3B3'});
				    	grid.jqGrid('setCell',i,"bonus_used","",{background:'#CAB3B3'});
				    	grid.jqGrid('setCell',i,"withdraw","",{background:'#CAB3B3'});
				    	
				    	//valid_bet
				    	grid.jqGrid('setCell',i,"account_id","",{'text-decoration':'underline'});
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
					    }
				    	if(myrow.amount_tips == '-0.00'){
				    		grid.jqGrid('setCell',i,"amount_tips","0.00");	
					    }
				    	if(myrow.amount_tips < 0){
				    		grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
					    }
				    	
				    	 $(".dateSummaryFooter").each(function(i) {this.style.color='#000000';});
				    	 $(".amount_wager").each(function(i) {this.style.color='#000000';});
					     $(".avg_bet").each(function(i) {this.style.color='#000000';});
				    	 $(".win_los").each(function(i) {this.style.color='#000000';});
					     $(".commission").each(function(i) {this.style.color='#000000';});
					     $(".amount_tips").each(function(i) {this.style.color='#000000';});
					     $(".total_win_los").each(function(i) {this.style.color='#000000';});
					     $(".bonus").each(function(i) {this.style.color='#000000';});
					     $(".total_stake").each(function(i) {this.style.color='#000000';});
					     $(".current_balance").each(function(i) {this.style.color='#000000';});
					     $(".p_l").each(function(i) {this.style.color='#000000';});//pl
					     $(".deposit").each(function(i) {this.style.color='#000000';});
					     $(".assigned_bonus").each(function(i) {this.style.color='#000000';});
					     $(".bonus").each(function(i) {this.style.color='#000000';});
					     $(".used_bonus").each(function(i) {this.style.color='#000000';});
					     $(".withdraw").each(function(i) {this.style.color='#000000';});

					     $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//total_winloss
					     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					     $(".p_l").each(function(i) {if ($(this).val().replace(",","") < 0){this.style.color='#730000';}});//pl
					    
					}
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");   
			    },
			   
			    hidegrid: false,  
			    rowNum: 200,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">Consolidated</label> </label> Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
				groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	afterInsertRow : function(rowid, rowdata)
			    {
			        if (rowdata.p_l < 0)
			        {
			           $(this).jqGrid('setCell', rowid,5 ,'', {color:'#990000'});
			          // alert("sefsdfsD");
			        }

			    },
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
			jQuery("#list1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'deposit', numberOfColumns: 3, titleText: '<label ><b>CASH FLOW</b></label>'},
					{startColumnName: 'bonus_total', numberOfColumns: 3, titleText: '<label ><b>BONUS</b></label>'},
				  ]
			});
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportConsolidatedWinLossToExcel();
	           }, 
	            position:"last"
	        });
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllConsolidatedWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked);
	           }, 
	            position:"last"
	        });
		});
	} 

	// HTV ALL PAGES SUMMARY
	function tableConsolidatedWinLossTotal(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list4'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_consolidated_summary").appendChild(divTag);
	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager4'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_consolidated_summary").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list4");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss/CashPlayerConsolidatedWinLossTotal&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Currency','Player Count','Total Stake','Valid Stake','Win/Loss','Commission','Bonus Used','Tips','<b>Total</b>','<b>P/L</b>','<b>Avg Player Value</b>','Ex-Rate','UC Total Stake','UC Valid Stake','UC WinLoss','UC Commission','UC Bonus Used','UC Tips','UC Total','UC P/L','UC Avg Player Value'],
			    colModel: [
					{name: 'currency_name', index: 'currency_name', width: 60,title:false},
					{name: 'player_count', index: 'player_count', width: 50,title:false,align:"right", summaryType:'sum'},
					{name: 'total_stake', index: 'total_stake', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'amount_wager', index: 'amount_wager', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'bonus', index: 'bonus', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'amount_tips', index: 'amount_tips', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_win_los', index: 'total_win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'avg_player_value', index: 'avg_player_value', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_player_value">{0}</label>'},
					{name: 'rate', index: 'rate', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','},hidden:true},
					{name: 'total_stake1', index: 'total_stake1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'amount_wager1', index: 'amount_wager1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'win_los1', index: 'win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>',hidden:true},
					{name: 'commission1', index: 'commission1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'bonus1', index: 'bonus1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'amount_tips1', index: 'amount_tips1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'total_win_los1', index: 'total_win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>',hidden:true},
					{name: 'p_l1', index: 'p_l1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
					{name: 'avg_player_value1', index: 'avg_player_value1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_player_value">{0}</label>',hidden:true},
				    ],

				loadtext:"",
				loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"currency_name","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"player_count","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_wager","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_stake","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_tips","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"p_l","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"avg_player_value","",{background:'#D84A38',color:'white'});
				    	
						
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'yellow'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'yellow'});	
					    }
				    	if(myrow.amount_tips < 0){
				    		grid.jqGrid('setCell',i,"amount_tips","",{color:'yellow'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'yellow'});	
					    }
				    	if(myrow.avg_player_value < 0){
				    		grid.jqGrid('setCell',i,"avg_player_value","",{color:'yellow'});	
					    }			    
					}   
			    },
			    hidegrid: false,  
			    rowNum: 25,	
			    rowList: [25, 50, 75],
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">Consolidated Win/Loss Summary</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convert this to </label><select id="currencyTypeConvertConsolidatedWinLoss" style="width:80px" onchange="javascript: convertConsolidatedWinLoss();"><option value="0-0">--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>',
			    viewrecords: true,
			    footerrow:true,
			    
			});
			$('#list4').jqGrid('navGrid', '#pager4', {edit: false, add: false, del:false, search: false});

			
		});
	} 

	function convertConsolidatedWinLoss(){
    	var comboValue=document.getElementById("currencyTypeConvertConsolidatedWinLoss").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	

		if (comboRange==0){
	   		 $('#list4').trigger("reloadGrid");
	   		$('#list4').jqGrid('footerData','set', {total_stake:'',amount_wager:'',win_los:'',commission:'',bonus:'',amount_tips:'',total_win_los:'',p_l:'',avg_player_value:''});   
	   		 return false;
	     }
    
		var noRow=$("#list4").getGridParam("reccount");
		var rate;
		var totalStake;
		var validStake;
		var winloss;
		var commission;
		var total;
		var bonus;
		var tips;
		var pl;
		var avgPlayerValue;
		var totalStakeConversion;
		var validStakeConversion;
		var totalConversion;
		var winlossConversion;
		var commissionConversion;
		var bonusConversion;
		var tipsConversion;
		var plConversion;
		var avgPlayerValueConversion;

	    var x=1;
	    while (x<=noRow)
	      {
	    	rate=$("#list4").getCell(x, 'rate');
	    	totalStake=$("#list4").getCell(x, 'total_stake1');
	    	validStake=$("#list4").getCell(x, 'amount_wager1');
	    	winloss=$("#list4").getCell(x, 'win_los1');
	    	commission=$("#list4").getCell(x, 'commission1');
			total=$("#list4").getCell(x, 'total_win_los1');
			bonus=$("#list4").getCell(x, 'bonus1');
			tips=$("#list4").getCell(x, 'amount_tips1');
			pl=$("#list4").getCell(x, 'p_l1');
			avgPlayerValue=$("#list4").getCell(x, 'avg_player_value1');
				totalStakeConversion=((parseFloat(totalStake)/parseFloat(rate)) * parseFloat(comboRange));
				validStakeConversion=((parseFloat(validStake)/parseFloat(rate)) * parseFloat(comboRange));
	    		winlossConversion=((parseFloat(winloss)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	commissionConversion=((parseFloat(commission)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		bonusConversion=((parseFloat(bonus)/parseFloat(rate)) * parseFloat(comboRange));
	    		tipsConversion=((parseFloat(tips)/parseFloat(rate)) * parseFloat(comboRange));
	    		plConversion=((parseFloat(pl)/parseFloat(rate)) * parseFloat(comboRange));
	    		avgPlayerValueConversion=((parseFloat(avgPlayerValue)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list4").jqGrid('setCell', x, 0, comboName);
	    		jQuery("#list4").jqGrid('setCell', x, 2, totalStakeConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 3, validStakeConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 4, winlossConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 5, commissionConversion);
	    	    jQuery("#list4").jqGrid('setCell', x, 6, bonusConversion);
	    	    jQuery("#list4").jqGrid('setCell', x, 7, tipsConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 8, totalConversion);	
	    		jQuery("#list4").jqGrid('setCell', x, 9, plConversion);	
	    		jQuery("#list4").jqGrid('setCell', x, 10, avgPlayerValueConversion);	
	     	x++;
	      }
	    var grid = $("#list4"),
	    sumTotalStake=grid.jqGrid('getCol', 'total_stake', false, 'sum');
	    sumValidStake=grid.jqGrid('getCol', 'amount_wager', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_los', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');
        sumBonus=grid.jqGrid('getCol', 'bonus', false, 'sum');
        sumTips=grid.jqGrid('getCol', 'amount_tips', false, 'sum');
        sumPl=grid.jqGrid('getCol', 'p_l', false, 'sum');
        sumAvgPlayerValue=grid.jqGrid('getCol', 'avg_player_value', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake:sumTotalStake,amount_wager:sumValidStake,win_los:sumWinLoss,commission:sumCommission,total_win_los:sumTotal, bonus: sumBonus, amount_tips: sumTips,p_l:sumPl,avg_player_value:sumAvgPlayerValue});  
	}

</script>

<script type="text/javascript">
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
var sChar=false;
function alphanumeric(inputtxt)  
{  
	var letters = /^[0-9a-zA-Z]+$/;  
	if(inputtxt.match(letters) || inputtxt=='')  
	{  
		sChar=false;  
	}else{  
		sChar=true;  
	}  
} 
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});


	function btnclick(btnname)
	{
		
		
		if (btnname=="Load")
		{
			document.getElementById("chkTest").checked=true;
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			<?php if(isset($_GET['dateFrom'])=='') {?>
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");

			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			
			var year = currentTime.getFullYear();
		
			var dayL=day.toString();
			var monthL=month.toString();

			if (monthL.length==1){
				month="0"+month;
			}
			if (dayL.length==1){
				day="0"+day;
			}

			document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
			document.getElementById("dateto").value=(year + "-" + month + "-" + day);
			
			document.getElementById('qry_result_consolidated').innerHTML='';
			tableConsolidatedWinLoss("All","All","All",datefrom,dateto,testChecked);

			document.getElementById('qry_result_consolidated_summary').innerHTML='';
			tableConsolidatedWinLossTotal("All","All","All",datefrom,dateto,testChecked);

			<?php }else{ ?>
				//var datefrom=enc(document.getElementById('defaultTimeFrom').value);
				//var dateto=enc(document.getElementById('defaultTimeTo').value);
				var datefrom=enc('<?php echo $_GET['dateFrom'];?>');
				var dateto=enc('<?php echo $_GET['dateTo'];?>');
				id='<?php echo $_GET['id'];?>';
				var splitDateFrom=datefrom.split("_");
				var splitDateTo=dateto.split("_");
				var splitDateFrom1=splitDateFrom[0].split("/");
				var splitDateTo1=splitDateTo[0].split("/");
				document.getElementById('txtAccountId').value=id;
				accountId=id;
				if (accountId==''){
					accountId='All';
				}else{
					accountId=id;
				}
				document.getElementById("datefrom").value=splitDateFrom1[0]+"-"+splitDateFrom1[1]+"-"+splitDateFrom1[2];
				document.getElementById("dateto").value=splitDateTo1[0]+"-"+splitDateTo1[1]+"-"+splitDateTo1[2];
				

				document.getElementById('qry_result_consolidated').innerHTML='';
				tableConsolidatedWinLoss(accountId,"All","All",datefrom,dateto,testChecked);

				document.getElementById('qry_result_consolidated_summary').innerHTML='';
				tableConsolidatedWinLossTotal(accountId,"All","All",datefrom,dateto,testChecked);

			<?php } ?>			
		}
		else if (btnname=="Today")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			var dayL=theday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theday="0"+theday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			alphanumeric(accountId);
			if (sChar==false){
				document.getElementById('qry_result_consolidated').innerHTML='';
				tableConsolidatedWinLoss(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_consolidated_summary').innerHTML='';
				tableConsolidatedWinLossTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked);
			}
		}
		else if (btnname=="Yesterday")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			var dayL=theyday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theyday="0"+theyday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theyday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theyday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			alphanumeric(accountId);
			if (sChar==false){
				document.getElementById('qry_result_consolidated').innerHTML='';
				tableConsolidatedWinLoss(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_consolidated_summary').innerHTML='';
				tableConsolidatedWinLossTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked);
			}
		}
		else if (btnname=="Submit")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			alphanumeric(accountId);
			if (sChar==false){
				document.getElementById('qry_result_consolidated').innerHTML='';
				tableConsolidatedWinLoss(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
				document.getElementById('qry_result_consolidated_summary').innerHTML='';
				tableConsolidatedWinLossTotal(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
			}
			
		}
	}

	function exportConsolidatedWinLossToExcel(){
		var table1= document.getElementById('qry_result_consolidated');
		var table2= document.getElementById('qry_result_consolidated_summary');
		var html1 = table1.outerHTML;
		var html2 = table2.outerHTML;
		document.forms[2].csvBuffer.value="";
		document.forms[2].csvBuffer.value=html1+''+html2 ;
	    document.forms[2].method='POST';
	    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss/ConsolidatedExcel';  // send it to server which will open this contents in excel file
	    document.forms[2].target='_top';
	    document.forms[2].submit();
	}
	function exportAllConsolidatedWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked){
	    $.ajax({	
			 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss/ExportAllCashPlayerConsolidatedWinLoss&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked,
			 async:true,
			 success: function(result) {
					var data =result.split("<BREAK>");
					
					document.forms[2].csvBuffer.value="";
					document.forms[2].csvBuffer.value=data[0]+""+data[2];
				    document.forms[2].method='POST';
				    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss/ConsolidatedExcel';  // send it to server which will open this contents in excel file
				    document.forms[2].target='_top';
				    document.forms[2].submit();
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown) { 
		 }});
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
div.dateSummaryFooter { text-align: Left; }
tr.jqfoot td { background-color: #AD8533; color: #FFF; }
</style>

</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area" style="width: 500px;height: 250px">
	<div class="header" >Consolidated Win/Loss</div>
	<form action="">
		<table style="background-color:transparent; width: 500px; margin-top: 5px;">
		<tr>
			<td width="25%" style="padding-left: 5px;">ACCOUNT ID:</td>
			<td width="75%"><input type="text" id="txtAccountId" style="width: 150px;" onkeydown="if (event.keyCode == 13){btnclick('Submit');}"></td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">GAME TYPE:</td>
			<td>
				<select id="gameType">
					<option value="All">All</option>
	  				<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. $row['game_name'] . '</option>';
					}
					echo '<option value=6>Slots</option>';
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">CURRENCY TYPE:</td>
			<td>
				<select id="currencyType" style="width: 99px" onChange="javascript:changeCurrency();">
					<option value="All">ALL</option>
  					<?php 
					$dataReader = CashPlayerCurrencyType::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . strtoupper($row['currency_name']) . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>&nbsp;&nbsp;&nbsp;
				<input type="checkbox" id="chkTest" checked><label id="lblExcept">Except TEST currency</label>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">FROM:</td>
			<td>
			<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px;width:50px; background-color: transparent" disabled></td>
		</tr>
		<tr><td style="padding-left: 5px;">TO:</td>
			<td>
			<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; width:50px; background-color: transparent" disabled="disabled" ondblclick="this.disabled=false;">
			</td>
		</tr>
		</table>
		<div align="center" style="padding-top: 5px;">
			<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
			<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
			<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
			<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red">
		</div>
		</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
		document.getElementById("dateto").value=(day + "-" + month + "-" + year);

		function changeCurrency(){
			var lblExcept = document.getElementById('lblExcept');
			var chkTest = document.getElementById('chkTest');
			if (document.getElementById('currencyType').value != "All"){
				chkTest.style.visibility = 'hidden';
				lblExcept.style.visibility = 'hidden';
				chkTest.checked=false;
			 }else{
				chkTest.style.visibility = 'visible';
				lblExcept.style.visibility = 'visible';
		     }
		}
		</script>
</div>
		
	<div id="qry_result_consolidated"></div>
	<br/>
	<div id="qry_result_consolidated_summary"></div>
	<br/><br/>
	
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
	</form>
</body>
</html>