<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',
        // Localization
        'sourceLanguage' => 'en_us',
        'language' => 'en', 
	// preloading 'log' component
	'preload'=>array('log','translate'),//'bootstrap'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
        'application.models.backend.*',
		'application.components.*',
		'application.modules.CashPlayer.*',
		'application.modules.Agent.*',
		'application.modules.translate.*',
		//'application.widgets.bootstrap.*',
		//'application.extensions.bootstrap.widgets.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
// 		'gii'=>array(
// 			'class'=>'system.gii.GiiModule',
// 			'password'=>'123456',
// 		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
// 			'ipFilters'=>array('127.0.0.1','::1'),
// 		),
		'translate',
		'MainCompany',
        'Agent',
		'CashPlayer',
		'Banking',
		'SystemSetting',
		'SystemMonitor',
		'GameSetting',
		'SystemTools'
	),

	// application components
	'components'=>array(
		//define the class and its missingTranslation event
		'bootstrap'=>array(
				'class'=>'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
		),
			
		'messages'=>array(
				'class'=>'CDbMessageSource',
				'onMissingTranslation' => array('TranslateModule', 'missingTranslation'),
				
		),
		'translate'=>array(//if you name your component something else change TranslateModule
				'class'=>'translate.components.MPTranslate',
				//any avaliable options here
				'acceptedLanguages'=>array(
						'en'=>'English',
						'th'=>'ไทย',
						'vi'=>'Tiếng Việt',
						'zh_cn'=>'中文简体',
						'zh_hk'=>'中文繁体'
				),
		),
		// Enable memcache
// 		'cache'=>array(
// 				'class'=>'CMemCache',
// 				'servers'=>array(
// 						array(
// 								'host'=>'192.168.4.44',
// 								'port'=>11211,
// 								'weight'=>1,
// 						),
// 				),
// 		),
		// Enable GeoIP
// 		'geoip' => array(
// 				'class' => 'application.extensions.geoip.CGeoIP',
// 				// specify filename location for the corresponding database
// 				'filename' => '/var/www/be-clubvegas999/protected/extensions/geoip/GeoLiteCity.dat',
// 				// Choose MEMORY_CACHE or STANDARD mode
// 				'mode' => 'STANDARD',
// 		),
		
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// Mapping to the default controller.
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				''=>'Login/Index',
				
			),
		),

		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
		),
		// DELOPMENT DB connection string============================================
		'db'=>array(
			'connectionString' => 'mysql:host=192.168.4.43;dbname=cv999_fd_master',
                        'username'         => 'cv999_be',
                        'password'         => 'jw42l8k23R',
						'charset'=>'utf8',
		),
       
        'db_cv999_fd_master'=>array(
			'connectionString' => 'mysql:host=192.168.4.43;dbname=cv999_fd_master',
                        'username'         => 'cv999_be',
                        'password'         => 'jw42l8k23R',
                        'class'            => 'CDbConnection',
        				'charset'=>'utf8',
		),
		// DELOPMENT DB connection string============================================

		// PRODUCTION DB connection string============================================
// 		'db'=>array(
// 				'connectionString' => 'mysql:host=172.30.30.114;dbname=cv999_fd_master',
// 				'username'         => 'cv999_be',
// 				'password'         => 'gLj2JEw21gBcBub',
// 				'class'            => 'CDbConnection',
// 				'charset'=>'utf8',
// 		),
		 
// 		'db_cv999_fd_master'=>array(
// 				'connectionString' => 'mysql:host=172.30.30.114;dbname=cv999_fd_master',
// 				'username'         => 'cv999_be',
// 				'password'         => 'gLj2JEw21gBcBub',
// 				'class'            => 'CDbConnection',
// 				'charset'=>'utf8',
// 		),
// 		// PRODUCTION DB connection string============================================

		// uncomment the following to use a MySQL database
		/*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testdrive',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		*/
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				// Log to application.log under runtime directory.
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
//				Add Message Route to emails by Kimny
// 				array(
// 						'class'=>'CEmailLogRoute',
// 						'levels'=>'error, warning',
// 						'emails'=>'kim.mou@creativeentertainmentltd.com',
// 				),
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);