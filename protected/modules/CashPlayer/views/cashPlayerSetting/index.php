<html>
<head>
<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.7.2/jquery.min.js" type="text/javascript"></script>
	<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayersetting.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_cash_player').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Cash Player Setting</a></li>");
	</script>
	
	
	<script language="javascript">
	var hide= true;
	function hideRows(clicker,tableId,trNum){
		var t = document.getElementById(tableId);
		if (clicker.checked==false){
			t.rows[trNum].style.visibility = 'hidden';
			t.rows[trNum].style.display = 'none';
		}
		else{
			t.rows[trNum].style.visibility = 'visible';
			t.rows[trNum].style.display = 'table-row';
		}
	}

	
	$(function() {
		//var autoComList;
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetting/Commision',
			method: 'POST',
			data: {'task': 'getCommission'},
			context: '',
			success: function(msg){
				var data=msg.split(';');
				var autoBaccaratComList=data[0].split(',');
				$("#txtBacarratCommision").autocomplete({
					source: autoBaccaratComList
				});
				var autoRouletteComList=data[1].split(',');
				$("#txtRouletteCommision").autocomplete({
					source: autoRouletteComList
				});
				var autoDragonTigerComList=data[2].split(',');
				$("#txtDragonTigerCommision").autocomplete({
					source: autoDragonTigerComList
				});
				var autoBlackjackComList=data[3].split(',');
				$("#txtBlackjackCommision").autocomplete({
					source: autoBlackjackComList
				});
				var autoAmericanRouletteComList=data[4].split(',');
				$("#txtAmericanRouletteCommision").autocomplete({
					source: autoAmericanRouletteComList
				});
				var autoSlotComList=data[4].split(',');
				$("#txtSlotCommision").autocomplete({
					source: autoSlotComList
				});
	    	}
		});
		
	});
	function tableBacarrat(currency_id) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    if (document.getElementById('chkBacarratGame').checked==true){
	    	document.getElementById("qry_baccarat").appendChild(divTag);
	    }else{
	    	document.getElementById("qry_baccarat").innerHTML='';
		}
	    
	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableBacarratLimit&currency_id='+currency_id, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['No.', 'All','Banker Player','Tie','Pair','Big','Small'],
			    colModel: [
			      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
			      {name: 'all', index: 'all', width: 70, sortable: false},
			      {name: 'banker_player', index: 'banker_player', width: 90, sortable: false},
			      {name: 'tie', index: 'tie', width: 70, sortable: false},
			      {name: 'pair', index: 'pair', width: 70,sortable: false},
			      {name: 'big', index: 'big', width: 65,sortable: false},
			      {name: 'small', index: 'small', width: 60,sortable: false}
			    ],
			    loadComplete: function(){
			    	jQuery.ajax({
			    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetting/CashPlayerGameTableLimit',
			    		method: 'GET',
			    		data: {'Account_ID': document.getElementById('txtAccountID').value},
			    		success: function(msg) {
			    			var data= msg.split(';');
			    			var bacarratLimitId=data[0].split(',');
			    			var i=0;
					    	var countRows=grid.jqGrid('getGridParam', 'records');
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=bacarratLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == bacarratLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	});
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rowNum: 99999,
				width: 950,
                multiselect: true,
                sortable: false,
			    sortname: 'no',
			    sortorder: 'ASC',
			    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>Baccarat Limit<font color="red">*</font></b> (Choose Credit Limit in continuous form, member level can only choose four types of credit Limit!)</div>',
			    hidegrid: false,
			    viewrecords: true
			});
			$('#list1').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false});
		});
	} 
	function tableRoulette(currency_id) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list3'; 
	    divTag.style.margin = "0px auto"; 
	    if (document.getElementById('chkRouletteGame').checked==true){
	    	document.getElementById("qry_roullete").appendChild(divTag);
	    }else{
	    	document.getElementById("qry_roullete").innerHTML='';
		}
	    
	    $(document).ready(function() {
			var grid=jQuery("#list3");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableRouletteLimit&currency_id='+currency_id,
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['No.', 'All','35 to 1','17 to 1','11 to 1','8 to 1','5 to 1','2 to 1','1 to 1'],
			    colModel: [
			      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
			      {name: 'all', index: 'all', width: 50, sortable: false},
			      {name: 'r_35_1', index: 'r_35_1', width: 50, sortable: false},
			      {name: 'r_18_1', index: 'r_18_1', width: 50, sortable: false},
			      {name: 'r_12_1', index: 'r_12_1', width: 50, sortable: false},
			      {name: 'r_9_1', index: 'r_9_1', width: 50, sortable: false},
			      {name: 'r_6_1', index: 'r_6_1', width: 50, sortable: false},
			      {name: 'r_2_1', index: 'r_2_1', width: 50, sortable: false},
			      {name: 'r_1_1', index: 'r_1_1', width: 50, sortable: false}
			    ],
			    loadComplete: function(){
			    	jQuery.ajax({
			    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetting/CashPlayerGameTableLimit',
			    		method: 'GET',
			    		data: {'Account_ID': document.getElementById('txtAccountID').value},
			    		success: function(msg) {
			    			var data= msg.split(';');
			    			var rouletteLimitId=data[1].split(',');
			    			var i=0;
					    	var countRows=grid.jqGrid('getGridParam', 'records');
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=rouletteLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == rouletteLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	});
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rowNum: 99999,
				width: 950,
				multiselect: true,
			    sortname: 'id',
			    sortorder: 'ASC',
			    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>European Roulette Limit<font color="red">*</font></b> (Choose Credit Limit in continuous form, member level can only choose four types of credit Limit!)</div>',
			    hidegrid: false,
			    viewrecords: true
			});
			$('#list3').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });
		});
	} 
	function tableDragonTiger(currency_id) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list2'; 
	    divTag.style.margin = "0px auto"; 
	    if (document.getElementById('chkDragonTigerGame').checked==true){
	    	document.getElementById("qry_dragon_tiger").appendChild(divTag);
	    }else{
	    	document.getElementById("qry_dragon_tiger").innerHTML='';
		}
	    $(document).ready(function() {
			var grid=jQuery("#list2");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableDragonTigerLimit&currency_id='+currency_id, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['No.', 'All','Dragon Tiger','Tie'],
			    colModel: [
			      {name: 'no', index: 'no', width: 40, search:true, hidden: true},
			      {name: 'all', index: 'all', width: 120, sortable: false},
			      {name: 'dragon_tiger', index: 'dragon_tiger', width: 120, sortable: false},
			      {name: 'tie', index: 'tie', width: 120, sortable: false}
			    ],
			    loadComplete: function(){
			    	jQuery.ajax({
			    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetting/CashPlayerGameTableLimit',
			    		method: 'GET',
			    		data: {'Account_ID': document.getElementById('txtAccountID').value},
			    		success: function(msg) {
			    			var data= msg.split(';');
			    			var dragonTigerLimitId=data[2].split(',');
			    			var i=0;
					    	var countRows=grid.jqGrid('getGridParam', 'records');
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=dragonTigerLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == dragonTigerLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	});
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rowNum: 99999,
				width: 950,
				multiselect: true,
			    sortname: 'no',
			    sortorder: 'ASC',
			    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>DragonTiger Limit<font color="red">*</font></b> (Choose Credit Limit in continuous form, member level can only choose four types of credit Limit!)</div>',
			    hidegrid: false,
			    viewrecords: true
			});
			$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });
		});
	} 

	function tableBlackjack(currency_id) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list4'; 
	    divTag.style.margin = "0px auto"; 
	    if (document.getElementById('chkBlackjackGame').checked==true){
	    	document.getElementById("qry_blackjack").appendChild(divTag);
	    }else{
	    	document.getElementById("qry_blackjack").innerHTML='';
		}
	    $(document).ready(function() {
			var grid=jQuery("#list4");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableBlackjackLimit&currency_id='+currency_id, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['No.', 'Main','Pair','Rummy','Insurance'],
			    colModel: [
					{name: 'no', index: 'no', width: 30, search:true, hidden: true},
					{name: 'all', index: 'all', width: 50, sortable: false},
					{name: 'pair', index: 'pair', width: 50, sortable: false},
					{name: 'rummy', index: 'rummy', width: 50, sortable: false},
					{name: 'insurance', index: 'insurance', width: 50, sortable: false},
			    ],
			    loadComplete: function(){
			    	jQuery.ajax({
			    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetting/CashPlayerGameTableLimit',
			    		method: 'GET',
			    		data: {'Account_ID': document.getElementById('txtAccountID').value},
			    		success: function(msg) {
			    			var data= msg.split(';');
			    			var dragonTigerLimitId=data[3].split(',');
			    			var i=0;
					    	var countRows=grid.jqGrid('getGridParam', 'records');
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=dragonTigerLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == dragonTigerLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	});
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rowNum: 99999,
				width: 950,
				multiselect: true,
			    sortname: 'no',
			    sortorder: 'ASC',
			    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>Blackjack Limit<font color="red">*</font></b> (Choose Credit Limit in continuous form, member level can only choose four types of credit Limit!)</div>',
			    hidegrid: false,
			    viewrecords: true
			});
			$('#list4').jqGrid('navGrid', '#pager4', {edit: false, add: false, del:false, search:true });
		});
	} 
	
	function tableAmericanRoulette(currency_id) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list5'; 
	    divTag.style.margin = "0px auto"; 
	    if (document.getElementById('chkAmericanRouletteGame').checked==true){
	    	document.getElementById("qry_american_roulette").appendChild(divTag);
	    }else{
	    	document.getElementById("qry_american_roulette").innerHTML='';
		}
	    $(document).ready(function() {
			var grid=jQuery("#list5");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableAmericanRouletteLimit&currency_id='+currency_id, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['No.', 'All','35 to 1','17 to 1','11 to 1','8 to 1','5 to 1','2 to 1','1 to 1'],
			    colModel: [
			      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
			      {name: 'all', index: 'all', width: 50, sortable: false,hidden:true},
			      {name: 'r_35_1', index: 'r_35_1', width: 50, sortable: false},
			      {name: 'r_18_1', index: 'r_18_1', width: 50, sortable: false},
			      {name: 'r_12_1', index: 'r_12_1', width: 50, sortable: false},
			      {name: 'r_9_1', index: 'r_9_1', width: 50, sortable: false},
			      {name: 'r_6_1', index: 'r_6_1', width: 50, sortable: false},
			      {name: 'r_2_1', index: 'r_2_1', width: 50, sortable: false},
			      {name: 'r_1_1', index: 'r_1_1', width: 50, sortable: false}
			    ],
			    loadComplete: function(){
			    	jQuery.ajax({
			    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetting/CashPlayerGameTableLimit',
			    		method: 'GET',
			    		data: {'Account_ID': document.getElementById('txtAccountID').value},
			    		success: function(msg) {
			    			var data= msg.split(';');
			    			var dragonTigerLimitId=data[4].split(',');
			    			var i=0;
					    	var countRows=grid.jqGrid('getGridParam', 'records');
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=dragonTigerLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == dragonTigerLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	});
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rowNum: 99999,
				width: 950,
				multiselect: true,
			    sortname: 'no',
			    sortorder: 'ASC',
			    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>American Roulette Limit<font color="red">*</font></b> (Choose Credit Limit in continuous form, member level can only choose four types of credit Limit!)</div>',
			    hidegrid: false,
			    viewrecords: true
			});
			$('#list5').jqGrid('navGrid', '#pager5', {edit: false, add: false, del:false, search:true });
		});
	} 
	function onTheLoad(){
		hideRows(document.getElementById('chkBacarratGame'),'setting_table1',1); 
		hideRows(document.getElementById('chkRouletteGame'),'setting_table1',2); 
		hideRows(document.getElementById('chkDragonTigerGame'),'setting_table1',3);
		hideRows(document.getElementById('chkBlackjackGame'),'setting_table1',4);  
		hideRows(document.getElementById('chkAmericanRouletteGame'),'setting_table1',5);
		hideRows(document.getElementById('chkSlotGame'),'setting_table1',6);
		tableBacarrat(document.getElementById('txtCurrencyId').value);
		tableRoulette(document.getElementById('txtCurrencyId').value);
		tableDragonTiger(document.getElementById('txtCurrencyId').value);
		tableBlackjack(document.getElementById('txtCurrencyId').value);
		tableAmericanRoulette(document.getElementById('txtCurrencyId').value);
	}
	function isSelectGameNull(){
		var s_select=0;
		if (document.getElementById('chkBacarratGame').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkRouletteGame').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkDragonTigerGame').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkBlackjackGame').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkAmericanRouletteGame').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkSlotGame').checked==true){
			s_select+=1;
		}
		
		if (s_select==0){
			return false;
		}
		else{
			return true;
		}
	}
	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
	
	function reSet(){
		document.getElementById('txtSettingMessage').value='';
	}
	
	function isSelectCasinoNull(){
		var s_select=0;
		if (document.getElementById('chkHtv999').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkSavanVegas999').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkCostaVegas999').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkVirtuaVegas999').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkSportBook999').checked==true){
			s_select+=1;
		}
		if (document.getElementById('chkSavanSlot999').checked==true){
			s_select+=1;
		}
		if (s_select==0){
			return false;
		}
		else{
			return true;
		}
	}
	
	function cashierConfirm(){
		if(isSelectCasinoNull()==false){
			document.getElementById('txtSettingMessage').value='Please select atleast one Casino under the Betting Information!';
			return false;
		}
		if(isSelectGameNull()==false){
			document.getElementById('txtSettingMessage').value='Please select atleast one game under the Betting Information!';
			return false;
		}
		var baccarat='';
		var roullete='';
		var dragontiger='';
		var blackjack='';
		var american_roulette='';
		var slot='';
		
		if (document.getElementById('chkBacarratGame').checked==true){baccarat='1';}else{baccarat='0';}
		if (document.getElementById('chkRouletteGame').checked==true){roullete='2';}else{roullete='0';}
		if (document.getElementById('chkDragonTigerGame').checked==true){dragontiger='4';}else{dragontiger='0';}
		if (document.getElementById('chkBlackjackGame').checked==true){blackjack='3';}else{blackjack='0';}
		if (document.getElementById('chkAmericanRouletteGame').checked==true){american_roulette='5';}else{american_roulette='0';}
		if (document.getElementById('chkSlotGame').checked==true){slot='6';}else{slot='0';}
		
		var selected_baccarat_limit='';
		var selected_roullete_limit='';
		var selected_dragon_tiger_limit='';
		var selected_blackjack_limit='';
		var selected_american_roulette_limit='';
		if(jQuery('#list1').jqGrid('getGridParam','selarrrow')!=null){
			var selBaccaratCount = jQuery('#list1').jqGrid('getGridParam', 'selarrrow');
		    if (selBaccaratCount.length > 4){
		   	 	alert('You can only select 4 table limit for Baccarat game.');
		   		 return false;
		    }
			var i=0;
			for (i=0;i<=jQuery('#list1').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_baccarat_limit+=jQuery('#list1').jqGrid('getRowData', jQuery('#list1').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_baccarat_limit=0;
		}
		if(jQuery('#list3').jqGrid('getGridParam','selarrrow')!=null){
			var selEURRouletteCount = jQuery('#list3').jqGrid('getGridParam', 'selarrrow');
		    if (selEURRouletteCount.length > 4){
		   	 	alert('You can only select 4 table limit for European Roulette game.');
		   		 return false;
		    }
			var i=0;
			for (i=0;i<=jQuery('#list3').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_roullete_limit+=jQuery('#list3').jqGrid('getRowData', jQuery('#list3').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_roullete_limit=0;
		}
		if(jQuery('#list2').jqGrid('getGridParam','selarrrow')!=null){
			 var selDragontigerCount = jQuery('#list2').jqGrid('getGridParam', 'selarrrow');
			    if (selDragontigerCount.length > 4){
			   	 	alert('You can only select 4 table limit for Dragontiger game.');
			   		 return false;
			    }
			var i=0;
			for (i=0;i<=jQuery('#list2').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_dragon_tiger_limit+=jQuery('#list2').jqGrid('getRowData', jQuery('#list2').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_dragon_tiger_limit=0;
		}

		if(jQuery('#list4').jqGrid('getGridParam','selarrrow')!=null){
			var selBlackjackCount = jQuery('#list4').jqGrid('getGridParam', 'selarrrow');
			    if (selBlackjackCount.length > 4){
			   	 	alert('You can only select 4 table limit for Blackjack game.');
			   		 return false;
			    }
			var i=0;
			for (i=0;i<=jQuery('#list4').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_blackjack_limit+=jQuery('#list4').jqGrid('getRowData', jQuery('#list4').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_blackjack_limit=0;
		}

		if(jQuery('#list5').jqGrid('getGridParam','selarrrow')!=null){
			var selAMERouletteCount = jQuery('#list5').jqGrid('getGridParam', 'selarrrow');
		    if (selAMERouletteCount.length > 4){
		   	 	alert('You can only select 4 table limit for American Roulette game.');
		   		 return false;
		    }
			var i=0;
			for (i=0;i<=jQuery('#list5').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_american_roulette_limit+=jQuery('#list5').jqGrid('getRowData', jQuery('#list5').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_american_roulette_limit=0;
		}


		if(trim(document.getElementById('txtBacarratCommision').value)=='')
		{
			document.getElementById('txtBacarratCommision').value='0';
		}
		if(trim(document.getElementById('txtRouletteCommision').value)=='')
		{
			document.getElementById('txtRouletteCommision').value='0';
		}
		if(trim(document.getElementById('txtDragonTigerCommision').value)=='')
		{
			document.getElementById('txtDragonTigerCommision').value='0';
		}
		if(trim(document.getElementById('txtBlackjackCommision').value)=='')
		{
			document.getElementById('txtBlackjackCommision').value='0';
		}
		if(trim(document.getElementById('txtAmericanRouletteCommision').value)=='')
		{
			document.getElementById('txtAmericanRouletteCommision').value='0';
		}
		if(trim(document.getElementById('txtSlotCommision').value)=='')
		{
			document.getElementById('txtSlotCommision').value='0';
		}

		if(parseFloat(trim(document.getElementById('txtBacarratCommision').value))>parseFloat(trim(document.getElementById('txtBacarratCommision').title)))
		{
			document.getElementById('txtSettingMessage').value='Baccarat commission must not exceed to its limit!';
			document.getElementById('txtBacarratCommision').focus();
			return false;
		}
		if(parseFloat(trim(document.getElementById('txtRouletteCommision').value))>parseFloat(trim(document.getElementById('txtRouletteCommision').title)))
		{
			document.getElementById('txtSettingMessage').value='European Roulette commission must not exceed to its limit!';
			document.getElementById('txtRouletteCommision').focus();
			return false;
		}
		if(parseFloat(trim(document.getElementById('txtDragonTigerCommision').value))>parseFloat(trim(document.getElementById('txtDragonTigerCommision').title)))
		{
			document.getElementById('txtSettingMessage').value='Dragon tiger commission must not exceed to its limit!';
			document.getElementById('txtDragonTigerCommision').focus();
			return false;
		}
		if(parseFloat(trim(document.getElementById('txtBlackjackCommision').value))>parseFloat(trim(document.getElementById('txtBlackjackCommision').title)))
		{
			document.getElementById('txtSettingMessage').value='Blackjack commission must not exceed to its limit!';
			document.getElementById('txtBlackjackCommision').focus();
			return false;
		}
		if(parseFloat(trim(document.getElementById('txtAmericanRouletteCommision').value))>parseFloat(trim(document.getElementById('txtAmericanRouletteCommision').title)))
		{
			document.getElementById('txtSettingMessage').value='American Roulette commission must not exceed to its limit!';
			document.getElementById('txtAmericanRouletteCommision').focus();
			return false;
		}
		if(parseFloat(trim(document.getElementById('txtSlotCommision').value))>parseFloat(trim(document.getElementById('txtSlotCommision').title)))
		{
			document.getElementById('txtSettingMessage').value='Slots commission must not exceed to its limit!';
			document.getElementById('txtSlotCommision').focus();
			return false;
		}
		if (document.getElementById('txtPassword').value!=document.getElementById('txtConfirmPassword').value){
			document.getElementById('txtSettingMessage').value="Password does'nt matched!";
			document.getElementById('txtSlotCommision').focus();
			return false;
		}
		if(parseFloat(document.getElementById('txtWinmax').value)<=parseFloat(document.getElementById('txtBalance').value) && parseFloat(document.getElementById('txtWinmax').value)!=-1 && parseFloat(document.getElementById('txtWinmax').value)!=0)
		{
			document.getElementById('txtSettingMessage').value='Player balance is '+document.getElementById('txtBalance').value+'. Win Max must be greater than the balance!';
			return false;
		}
		var tip='';
		if (document.getElementById('chkEnableTip').checked==true){
			tip='0';
		}else{
			tip='1';
		}	
		//casino
		var htv999='';
		var savan999='';
		var costa999='';
		var virtua999='';
		var sport999='';
		if (document.getElementById('chkHtv999').checked==true){
			htv999='1';
		}else{
			htv999='0';
		}
		if (document.getElementById('chkSavanVegas999').checked==true){
			savan999='1';
		}else{
			savan999='0';
		}
		if (document.getElementById('chkCostaVegas999').checked==true){
			costa999='1';
		}else{
			costa999='0';
		}
		if (document.getElementById('chkVirtuaVegas999').checked==true){
			virtua999='1';
		}else{
			virtua999='0';
		}
		if (document.getElementById('chkSportBook999').checked==true){
			sport999='1';
		}else{
			sport999='0';
		}	
		if (document.getElementById('chkSavanSlot999').checked==true){
			slot999='1';
		}else{
			slot999='0';
		}	
		
		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetting/SaveCashPlayerSetting',
    		method: 'GET',
    		data: {'account_id': document.getElementById('txtAccountID').value, 
        		'baccarat' : baccarat,
        		'baccarat_commission': document.getElementById('txtBacarratCommision').value, 
        		'selected_baccarat_limit': selected_baccarat_limit,
        		'roullete':roullete, 
        		'roullete_commission': document.getElementById('txtRouletteCommision').value,
        		'selected_roullete_limit': selected_roullete_limit, 
        		'dragontiger':dragontiger,
        		'dragontiger_commission': document.getElementById('txtDragonTigerCommision').value,
        		'selected_dragon_tiger_limit': selected_dragon_tiger_limit,
        		'blackjack':blackjack,
        		'blackjack_commission': document.getElementById('txtBlackjackCommision').value,
        		'selected_blackjack_limit': selected_blackjack_limit,
        		'american_roulette':american_roulette,
        		'american_roulette_commission': document.getElementById('txtAmericanRouletteCommision').value,
        		'selected_american_roulette_limit': selected_american_roulette_limit,
        		'slot':slot,
        		'slot_commission': document.getElementById('txtSlotCommision').value,
        		'password':document.getElementById('txtConfirmPassword').value,
        		'max_win_multiple':document.getElementById('txtWinmax').value,
        		'daily_max_win':document.getElementById('txtDailyMaxWin').value,
        		'htv999': htv999,
        		'savan999': savan999,
        		'costa999': costa999,
        		'virtua999': virtua999,
        		'sport999': sport999,
        		'slot999': slot999,
        		'enableTip':tip,
        		},
    		success: function(msg) {
        		if(msg == 'invalid_daily_max_win'){
            		alert('<?php echo Yii::t('cashplayer','cashplayer.cashplayersetting.invalid_daily_max_win');?>');
            		document.getElementById('txtDailyMaxWin').focus();
        		}else if(msg == 'done'){
            		alert('<?php echo Yii::t('cashplayer','cashplayer.cashplayersetting.update_complete');?>');
            		document.location.reload(true);
        		}else if(msg == 'error_submit_win_loss_limit'){
        			alert('<?php echo Yii::t('cashplayer','cashplayer.cashplayersetting.system_cant_set_daily_max_win');?>');
            		document.location.reload(true);
        		}else if(msg == 'error_occured'){
        			alert('<?php echo Yii::t('cashplayer','cashplayer.cashplayersetting.system_cant_update_cashplayer');?>');
            	}else{
                	alert(msg);

                	
    				//document.getElementById('txtSettingMessage').value=msg;
            	}
	    	}
    	});
	}
	</script>
	<script>
	function winMax()
	{
		if(parseFloat(document.getElementById('txtWinmax').value)!=-1){
			
			if(parseFloat(document.getElementById('txtWinmax').value)<=parseFloat(document.getElementById('txtBalance').value) || parseFloat(document.getElementById('txtWinmax').value) == 0.00)
			{
				if(parseFloat(document.getElementById('txtWinmax').value)==0.00){
					document.getElementById('lblWinMax').innerHTML='<font color="black">* You can login but cannot play any casino.</font>';
				}else{
					document.getElementById('lblWinMax').innerHTML='<font color="red">* Player balance is '+ parseFloat(document.getElementById('txtBalance').value).toFixed(2) +'. Win Max must be greater than the balance!</font>';
				}
			}else{
				if(document.getElementById('txtWinmax').value!=''){
					document.getElementById('lblWinMax').innerHTML='* Player can win ' + (parseFloat(document.getElementById('txtWinmax').value)-parseFloat(document.getElementById('txtBalance').value)).toFixed(2) + '.';
				}else{
					document.getElementById('lblWinMax').innerHTML='';
				}
			}
		}else{
			document.getElementById('lblWinMax').innerHTML='* Player can win unlimited.';
		}
	}
	function dailyWinMax(){
		if(parseFloat(document.getElementById('txtDailyMaxWin').value)!=0){
			if(document.getElementById('txtDailyMaxWin').value!=''){
				document.getElementById('lblDailyMaxWin').innerHTML='Player can win ' + parseFloat(document.getElementById('txtDailyMaxWin').value).toFixed(2) + ' daily.';
			}else{
				document.getElementById('lblDailyMaxWin').innerHTML='';
			}
		}else{
			document.getElementById('lblDailyMaxWin').innerHTML='Player can win unlimited daily.';
		}
	}
	</script>
	<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript: onTheLoad();winMax(); dailyWinMax();">
	<?php 
		$rd = TableCashPlayer::model()->find("account_id = '" . $_GET['Account_ID'] . "'");
		//$rd1 = TableCashPlayerSelectGame::model()->findAll("cash_player_account_id = '" . $_GET['Account_ID'] . "'");
		//new table structure. get data from tbl_cash_player
		$rd1 = TableCashPlayer::model()->findAll("account_id = '" . $_GET['Account_ID'] . "'");
	?>
	<input type="text" id="txtCurrencyId" value="<?php echo $rd['currency_id']?>"/>
	<div id="setting_header">Basic Information <input class="btn red" type="button" value="Back" onclick="javascript: window.location='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList'"/></div>
	<table id="setting_table" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="left">Account ID</td><td class="right"><input type="text" class="txt" id="txtAccountID" value="<?php echo $_GET['Account_ID'];?>" readonly="true"/></td>
			<td class="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone number</td class="right"><td><input type="text" class="txt" id="txtPhoneNo" value="<?php echo $rd['phone_number'];?>" readonly="true"/></td>
		</tr>
		
		<tr>
		 	<?php //if(Yii::app()->session['level']==1 || Yii::app()->session['level']==3){
				if(Yii::app()->user->checkAccess('cashplayer.writeCashPlayerPassword')){
			?>
				<td class="left">Password</td><td class="right"><input type="Password" class="txt" id="txtPassword" value=""/> (6 - 14 Char) leave it blank for no change</td>
			<?php }else{?>
				<td class="left">Password</td><td class="right"><input type="Password" class="txt" id="txtPassword" value="" readonly="true"/> (6 - 14 Char) leave it blank for no change</td>
			<?php }?>
			<td class="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please Input Email</td><td class="right"><input type="text" class="txt" id="txtEmail" value="<?php echo $rd['email'];?>" readonly="true"/></td>
		</tr>
		<tr>
			<?php //if(Yii::app()->session['level']==1 || Yii::app()->session['level']==3){
			if(Yii::app()->user->checkAccess('cashplayer.writeCashPlayerPassword')){
			?>
				<td class="left">Confirm Password</td><td class="right"><input type="Password" class="txt" id="txtConfirmPassword" /> (6 - 14 Char)</td>
			<?php }else{?>
				<td class="left">Confirm Password</td><td class="right"><input type="Password" class="txt" id="txtConfirmPassword" readonly="true"/> (6 - 14 Char)</td>
			<?php }?>
			<td class="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last Name</td><td class="right"><input type="text" class="txt" id="txtLname" value="<?php echo $rd['last_name'];?>" readonly="true"/></td>
		</tr>
		<tr>
			<td class="left">First Name</td><td class="right"><input type="text" class="txt" id="txtFname" value="<?php echo $rd['first_name'];?>" readonly="true"/></td>
			<td class="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth</td><td class="right"><input type="text" class="txt" id="txtDateOfBirth" value="<?php echo $rd['dob'];?>" readonly="true"/></td>
		</tr>
		<tr>
			<td class="left">Sex</td><td class="right"><input type="radio" name="radSex" id="radMaleSex" value="Male" disabled="true" <?php if(strtolower($rd['sex'])=='m'){echo 'checked';}?>/><label for="radMaleSex">Male</label> <input type="radio" name="radSex" id="radFemaleSex" value="Female" disabled="true" <?php if(strtolower($rd['sex'])=='f'){echo 'checked';}?>/><label for="radFemaleSex">Female</label></td>
			<td class="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Postal</td><td class="right"><input type="text" class="txt" id="txtPostal" value="<?php echo $rd['zip_code'];?>" readonly="true"/></td>
		</tr>
		<tr>
			<td class="left">Address</td><td class="right"><input type="text" class="txt" id="txtAddress" value="<?php echo $rd['address'];?>" readonly="true"/></td>
			<td class="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Country</td><td class="right"><input type="text" class="txt" id="txtCountry" value="<?php echo $rd['country'];?>" readonly="true"/></td>
		</tr>
		<tr>
			<td class="left">City</td><td class="right"><input type="text" class="txt" id="txtCity" value="<?php echo $rd['city'];?>" readonly="true"/></td>
			<td class="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Name</td><td class="right"><input type="text" class="txt" id="txtAccountName" value="<?php echo $rd['account_name'];?>" readonly="true"/></td>
		</tr>
		<tr>
			<td class="left">Security Number</td><td class="right"><input type="text" class="txt" id="txtSecurityNo" value="<?php echo $rd['digit_security_num'];?>" readonly="true"/></td>
			<td class="left"></td>
	</tr>
	</table>
	<div id="setting_header"><div style="padding: 5px;">Bank Information</div></div>
	<table id="setting_table" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="left">Account Number</td><td class="right"><input  class="txt" width="300px" type="text" id="txtAccountNumber" value="<?php echo $rd['bank_account_number'];?>" readonly="true"></td>
		</tr>
		<tr>
			<td class="left">Account Name</td><td class="right"><input class="txt" width="300px"  type="text" id="txtAccountName" value="<?php echo $rd['bank_account_name'];?>"  readonly="true"></td>
		</tr>
		<tr>
			<td class="left">Bank Used</td><td class="right"><input class="txt" width="300px" type="text" id="txtBankUsed" value="<?php echo $rd['bank_used'];?>" readonly="true"></td>
		</tr>
		<tr>
			<td class="left">Bank Branch</td><td class="right"><input class="txt" width="300px" type="text" id="txtBankBranch" value="<?php echo $rd['bank_branch'];?>" readonly="true"></td>
		</tr>
	</table>
	<div id="setting_header1"><div style="padding: 5px;">Betting Information</div></div>
			<?php 
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT currency_name from tbl_currency where id='".$rd['currency_id']."'");
			$currency_name=$command->queryRow();
		?>
	<table id="setting_table" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="left">Balance</td><td class="right"><input class="txt" type="text" id="txtBalance" value="<?php echo $rd['balance'];?>" readonly="true"></td>
		</tr>
		<tr>
			<td class="left">Max Balance (<font color="#961111">Hatien & Savan</font>)</td><td class="right"><input class="txt" type="text" id="txtWinmax" value="<?php echo $rd['max_win_multiple'];?>" onkeyup="javascript: winMax();"><label id="lblWinMax"></label></td>
		</tr>
		<tr>
			<td class="left">Daily Max Win (<font color="#961111">Costa</font>)</td><td class="right"><input class="txt" type="text" id="txtDailyMaxWin" value="<?php echo $rd['max_win'];?>" onkeyup="javascript: dailyWinMax();">*<label id="lblDailyMaxWin"></label></td>
		</tr>
		<tr>
			<td class="left">Enable Tip (<font color="#961111">Savan</font>)</td><td class="right"><input class="txt" type="checkbox" id="chkEnableTip" <?php if ($rd['enable_tip']==0) { echo 'checked';}?> value="<?php echo $rd['enable_tip'];?>"></td>
		</tr>
		<tr>
			<td class="left">Currency</td><td class="right"><input class="txt" type="text" id="txtCurrency" value="<?php echo $currency_name['currency_name'];?>" readonly="true"></td>
		</tr>
	
		<tr>
				<td class="left">Select Casino</td>
				<td class="right">
				<?php 
					$s_htv999_select='';
					$s_savan999_select='';
					$s_costa999_select='';
					$s_virtua999_select='';
					$s_sport999_select='';
					$s_slot999_select='';
					
					$s_htv999_checked='';
					$s_savan999_checked='';
					$s_costa999_checked='';
					$s_virtua999_checked='';
					$s_sport999_checked='';
					$s_slot999_checked='';
					//new structure
					$cl=TableCashPlayer::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['Account_ID'] ),));
					$cls=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id','params'=>array(':level_id'=>$cl['casino_access_level']),));
					$s_htv999_select=$cls['htv'];
					$s_savan999_select=$cls['sv'];
				    $s_costa999_select=$cls['vig'];
					$s_virtua999_select=$cls['vv'];
					$s_sport999_select=$cls['sb'];
					$s_slot999_select=$cls['svs'];
				
				?>
					<ul class="chklist">
						<li><input type="checkbox" id="chkHtv999" <?php if($s_htv999_checked=='' || $s_htv999_checked==1){if($s_htv999_select!='' && $s_htv999_select==0){}else{echo 'checked';}}?> <?php if($s_htv999_select!='' && $s_htv999_select==0){echo 'unchecked';}?>><label for="chkHtv999">HTV999</label></li>
						<li><input type="checkbox" id="chkSavanVegas999" <?php if($s_savan999_checked=='' || $s_savan999_checked==1){if($s_savan999_select!='' && $s_savan999_select==0){}else{echo 'checked';}}?> <?php if($s_savan999_select!='' && $s_savan999_select==0){echo 'uncheked';}?>><label for="chkSavanVegas999">Savanvegas9999</label></li>
						<li><input type="checkbox" id="chkCostaVegas999" <?php if($s_costa999_checked=='' || $s_costa999_checked==1){if($s_costa999_select!='' && $s_costa999_select==0){}else{echo 'checked';}}?> <?php if($s_costa999_select!='' && $s_costa999_select==0){echo 'unchecked';}?>><label for="chkCostaVegas999">Costavegas999</label></li>
						<li><input type="checkbox" id="chkVirtuaVegas999" <?php if($s_virtua999_checked=='' || $s_virtua999_checked==1){if($s_virtua999_select!='' && $s_virtua999_select==0){}else{echo 'checked';}}?> <?php if($s_virtua999_select!='' && $s_virtua999_select==0){echo 'unchecked';}?>><label  for="chkVirtuaVegas999">Virtuavegas999</label></li>
						<li><input type="checkbox" id="chkSportBook999" <?php if($s_sport999_checked=='' || $s_sport999_checked==1){if($s_sport999_select!='' && $s_sport999_select==0){}else{echo 'checked';}}?> <?php if($s_sport999_select!='' && $s_sport999_select==0){echo 'unchecked';}?>><label  for="chkSportBook999">Sport Book</label></li>
						<li><input type="checkbox" id="chkSavanSlot999" <?php if($s_slot999_checked=='' || $s_slot999_checked==1){if($s_slot999_select!='' && $s_slot999_select==0){}else{echo 'checked';}}?> <?php if($s_slot999_select!='' && $s_slot999_select==0){echo 'unchecked';}?>><label  for="chkSavanSlot999">SlotVegas</label></li>
					</ul>
					
					
					
					
				</td>
		</tr>
			
	</table>
	<table id="setting_table1" border="0" cellpadding="0" cellspacing="0">
		

		<tr>
			<td class="left">Select Game</td>
			<td class="right">
				<ul class="chklist">
					<li><input type="checkbox" name="chkGame" id="chkBacarratGame" value="Bacarrat" onclick="javascript: hideRows(this,'setting_table1',1); tableBacarrat(document.getElementById('txtCurrencyId').value);" <?php foreach($rd1 as $row){if($row['limit_baccarat']!='' && $row['limit_baccarat']!=0){echo 'checked';}} ?>/> <label for="chkBacarratGame">Bacarrat</label></li>
					<li><input type="checkbox" name="chkGame" id="chkRouletteGame" value="Roulette" onclick="javascript: hideRows(this,'setting_table1',2); tableRoulette(document.getElementById('txtCurrencyId').value);" <?php foreach($rd1 as $row){if($row['limit_roulette']!='' && $row['limit_roulette']!=0){echo 'checked';}} ?>/> <label for="chkRouletteGame">European Roulette</label></li>
					<li><input type="checkbox" name="chkGame" id="chkDragonTigerGame" value="DragonTiger" onclick="javascript: hideRows(this,'setting_table1',3); tableDragonTiger(document.getElementById('txtCurrencyId').value);" <?php foreach($rd1 as $row){if($row['limit_dragon_tiger']!='' && $row['limit_dragon_tiger']!=0){echo 'checked';}} ?>/> <label for="chkDragonTigerGame">Dragon Tiger</label></li>
					<li><input type="checkbox" name="chkGame" id="chkBlackjackGame" value="Blackjack" onclick="javascript: hideRows(this,'setting_table1',4); tableBlackjack(document.getElementById('txtCurrencyId').value);" <?php foreach($rd1 as $row){if($row['limit_blackjack']!='' && $row['limit_blackjack']!=0){echo 'checked';}} ?>/> <label for="chkBlackjackGame" >Blackjack</label></li>
					<li><input type="checkbox" name="chkGame" id="chkAmericanRouletteGame" value="AmericanRoulette" onclick="javascript: hideRows(this,'setting_table1',5); tableAmericanRoulette(document.getElementById('txtCurrencyId').value);" <?php foreach($rd1 as $row){if($row['limit_american_roulette']!='' && $row['limit_american_roulette']!=0){echo 'checked';}} ?>/> <label for="chkAmericanRouletteGame" >American Roulette</label></li>
					<li><input type="checkbox" name="chkGame" id="chkSlotGame" value="Slot" onclick="javascript: hideRows(this,'setting_table1',6);" <?php foreach($rd1 as $row){if($row['limit_slots']!='' && $row['limit_slots']!=0){echo 'checked';}} ?>/> <label for="chkSlotGame" >Slots</label></li>
				</ul>
			</td>
		</tr>
		<?php 
		$rd = TableCashPlayerCommission::model()->find("id = 1");
		?>
		<tr>
			<td class="left"><b>Bacarrat</b> Commission</td><td class="right"><input type="text" class="txt" title="<?php echo $rd['baccarat_max_commission'];?>" id="txtBacarratCommision" value="<?php foreach($rd1 as $row){if($row['limit_baccarat']!='' && $row['limit_baccarat']!=0){echo $row['commission_baccarat'];}} ?>"> % * 0%, Limit(0% - <?php echo $rd['baccarat_max_commission'];?>%)</td>
		</tr>
		<tr>
			<td class="left"><b>European Roulette</b> Commission</td><td class="right"><input type="text" class="txt" title="<?php echo $rd['roulette_max_commission'];?>" id="txtRouletteCommision" value="<?php foreach($rd1 as $row){if($row['limit_roulette']!='' && $row['limit_roulette']!=0){echo $row['commission_roulette'];}} ?>"> % * 0%, Limit(0% - <?php echo $rd['roulette_max_commission'];?>%)</td>
		</tr>
		<tr>
			<td class="left"><b>Dragon Tiger</b> Commission</td><td class="right"><input type="text" class="txt" title="<?php echo $rd['dragon_tiger_max_commission'];?>" id="txtDragonTigerCommision" value="<?php foreach($rd1 as $row){if($row['limit_dragon_tiger']!='' && $row['limit_dragon_tiger']!=0){echo $row['commission_dragon_tiger'];}} ?>"> % * 0%, Limit(0% - <?php echo $rd['dragon_tiger_max_commission'];?>%)</td>
		</tr>
		<tr>
			<td class="left"><b>Blackjack</b> Commission</td><td class="right"><input type="text" class="txt" title="<?php echo $rd['blackjack_max_commission'];?>" id="txtBlackjackCommision" value="<?php foreach($rd1 as $row){if($row['limit_blackjack']!='' && $row['limit_blackjack']!=0){echo $row['commission_blackjack'];}} ?>"> % * 0%, Limit(0% - <?php echo $rd['blackjack_max_commission'];?>%)</td>
		</tr>
		<tr>
			<td class="left"><b>American Roulette</b> Commission</td><td class="right"><input type="text" class="txt" title="<?php echo $rd['american_roulette_max_commission'];?>" id="txtAmericanRouletteCommision" value="<?php foreach($rd1 as $row){if($row['limit_american_roulette']!='' && $row['limit_american_roulette']!=0){echo $row['commission_american_roulette'];}} ?>"> % * 0%, Limit(0% - <?php echo $rd['american_roulette_max_commission'];?>%)</td>
		</tr>
		<tr>
			<td class="left"><b>Slots</b> Commission</td><td class="right"><input type="text" class="txt" title="<?php echo $rd['slot_max_commission'];?>" id="txtSlotCommision" value="<?php foreach($rd1 as $row){if($row['limit_slots']!='' && $row['limit_slots']!=0){echo $row['commission_slots'];}} ?>"> % * 0%, Limit(0% - <?php echo $rd['slot_max_commission'];?>%)</td>
		</tr>
	</table>
	<div id="qry_baccarat"></div>
	<div id="qry_roullete"></div>
	<div id="qry_dragon_tiger"></div>
	<div id="qry_blackjack"></div>
	<div id="qry_american_roulette"></div>
	
	<div id="cash_player_game_table_limit_footer"><input class="btn red" type="button" value="Confirm" onclick="javascript: cashierConfirm();"/> <input class="btn red" type="button" value="Reset" onclick="javascript: reSet();"/> <input class="btn red" type="button" value="Back" onclick="javascript: window.location='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList'"/></div>
	<input type="text" id="txtSettingMessage" readonly="true"/>
</body>
</html>