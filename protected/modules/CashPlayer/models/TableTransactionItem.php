<?php
/**
 * @todo bind tbl_transaction_item to TableTransactionItem CActiveRecord
 * @copyright CE
 * @author leokarl
 * @since 2012-12-20
 */
class TableTransactionItem extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_transaction_item';
	}
}

