<?php
/**
 * @todo Advisory Model
 * @copyright CE
 * @author Sitthykun
 * @since 2013-07-02
 */
class AdvisoryModel {

	/**
	 * 
	 * @param integer $id
	 */
	public function deleteByID($id) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeDeleteAdvisoryByID(" . $id . ")");
		// execute
		$command->execute();
	}

	/**
	 * 
	 * @param integer $id
	 */
	public function getContentListByAdvisoryID($id) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetContentListById(" . $id . ")");

		// return array
		return $command->queryAll();
	}
	
	/**
	 * 
	 */
	public function getCountRows() {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeCountAdvisoryList()");
		
		return $command->queryScalar();
	}

	/**
	 *
	 * @param string $languageCode
	 */
	public function getCountRowsByLanguageCode($languageCode = '') {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeCountAdvisoryListByLanguageCode()");

		return $command->queryScalar();
	}

    /**
     * @param $id
     */
    public function getDetailByID($id) {
        $connection = Yii::app()->db_cv999_fd_master;
        $command = $connection->createCommand("CALL spBeGetAdvisoryDetailByID(" . $id . ")");
        // many languages
        return $command->queryAll();
    }

	/**
	 * 
	 * @param string
	 */
	public function getList() {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetAdvisoryList()"); // default value is string blank
		// clear
		$connection = null;
		// return array
		return $command->queryAll();
	}

	/**
	 * 
	 * @param string $languageCode
	 */
	public function getListByLanguageCode($languageCode) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetAdvisoryList()");
		// clear
		$connection = null;
		// array
		return $command->queryAll();
	}

	/**
	 * 
	 * @param integer $id
	 */
	public function getListByID($id) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetAdvisoryByID(" . $id . ")");
		// many languages
		return $command->queryRow();
	}

	/**
	 * @return array
	 */
	public function getListToJGrid() {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetAdvisoryList()");
		// array of advisory list
		$list = $command->queryAll();
		// temp storage
		$temp = array();
		$index = 1;

		// loop
		foreach ($list as $a) {
			//colNames: ['No.', 'Site', 'Published','Schedule', 'Start Time', 'End Time'],
			// a.id AS `id`, a.site_code AS `site`, l.language_code, l.language, l.id AS `language_id`, a.published_category_id, 
			// apc.content AS `published_category_content`, ac.content, a.schedule_category_id, sc.content AS `schedule_category_content`, 
			// a.started_date, a.ended_date, a.monday, a.tuesday, a.wednesday, a.thursday, a.friday, a.saturday, a.sunday
			$temp[$index]['id'] = $a['id'];
			$temp[$index]['site'] = $a['sites'];
			$temp[$index]['published'] = $a['published_category'];
			$temp[$index]['servicesAffected'] = str_replace('"', '\"', $a['services_affected']);
			$temp[$index]['schedule'] = $a['schedule_type'];
			$temp[$index]['startTime'] = $a['started_date'];
			$temp[$index]['endTime'] = $a['ended_date'];
			$temp[$index]['modify'] = $a['id'];

			// increase
			$index++;
		}
		// clear
		$connection = null;
		// return array
		return $temp;
	}

	/**
	 * 
	 * @param array $accesses
	 * @param array $status
	 * @param string $startTime
	 * @param string $endTime
	 * @param integer $servicesAffect
	 * @param array $servicesAffectTexts
	 * @param array $contents
	 * @param array $days
	 */
	public function saveNewDaily($accesses, $status, $siteList, $iconID, $startTime, $endTime, $startedDateShow, $startHoursTimeShow, $scheduleSelectedID, $servicesAffectedID, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday, $servicesAffectTexts, $contents) {
		// connect to db
		$connection = Yii::app()->db_cv999_fd_master;
		// get language list
		$languages = Languages::getLanguages();
		// insert
		$command = $connection->createCommand("CALL spBeAddAdvisoryDaily('" . $accesses . "', " . $status . ",'" . $siteList . "', " . $scheduleSelectedID . "," . $iconID . ", '" . 
				$startTime . "', '" . $endTime . "', '" . $startedDateShow  . "', '" . $startHoursTimeShow  . "', " . $monday . "," . $tuesday . "," . $wednesday . "," .
				 $thursday . "," . $friday . "," . $saturday . "," .$sunday .")");
		// execute
		$command->execute();

		// loop to insert content
		foreach ($languages as $l) {
			// insert to table
			// $command = $connection->createCommand("CALL spBeAddAdvisoryContentTempory(" . $contentID . ",'" . $l['language_code'] . "', '" . $servicesAffectTexts[$l['language_code']]. "', '" .$contents[$l['language_code']]. "')");
			$command = $connection->createCommand("CALL spBeAddAdvisoryContent('" . $l['language_code'] . "'," . $servicesAffectedID . ", '" . addslashes($servicesAffectTexts[$l['language_code']]) . "', '" . addslashes($contents[$l['language_code']]) . "')");
			// execute
			$command->execute();
		}

		// save
		$connection = null;
	}

	/**
	 * 
	 * @param unknown_type $accesses
	 * @param unknown_type $status
	 * @param unknown_type $siteList
	 * @param unknown_type $startTime
	 * @param unknown_type $endTime
	 * @param unknown_type $servicesAffectedID
	 * @param unknown_type $servicesAffectTexts
	 * @param unknown_type $contents
	 */
	public function saveNewSomedays($accesses, $status, $siteList, $iconID, $startTime, $endTime, $startedDateShow, $startHoursTimeShow, $scheduleSelectedID, $servicesAffectedID, $servicesAffectTexts, $contents) {
		$connection = Yii::app()->db_cv999_fd_master;

		// get language list
		$languages = Languages::getLanguages();

		// insert
		$command = $connection->createCommand("CALL spBeAddAdvisorySomedays('" . $accesses . "', " . $status . ",'" . $siteList . "', " . $scheduleSelectedID . "," . $iconID . ", '" .
				$startTime . "', '" . $endTime . "', '" . $startedDateShow . "', '" . $startHoursTimeShow . "')");

		// execute
		$command->execute();

		// loop to insert content
		foreach ($languages as $l) {
			// insert to table
			// $command = $connection->createCommand("CALL spBeAddAdvisoryContentTempory(" . $contentID . ",'" . $l['language_code'] . "', '" . $servicesAffectTexts[$l['language_code']]. "', '" .$contents[$l['language_code']]. "')");
			$command = $connection->createCommand("CALL spBeAddAdvisoryContent('" . $l['language_code'] . "'," . $servicesAffectedID . ", '" . addslashes($servicesAffectTexts[$l['language_code']]) . "', '" . addslashes($contents[$l['language_code']]) . "')");
			// execute
			$command->execute();
		}

		// save
		$connection = null;
	}

	/**
	 * 
	 * @param unknown_type $advisoryID
	 * @param unknown_type $accesses
	 * @param unknown_type $status
	 * @param unknown_type $siteList
	 * @param unknown_type $startTime
	 * @param unknown_type $endTime
	 * @param unknown_type $servicesAffectedID
	 * @param unknown_type $monday
	 * @param unknown_type $tuesday
	 * @param unknown_type $wednesday
	 * @param unknown_type $thursday
	 * @param unknown_type $friday
	 * @param unknown_type $saturday
	 * @param unknown_type $sunday
	 * @param unknown_type $servicesAffectTexts
	 * @param unknown_type $contents
	 */
	public function updateDaily($advisoryID, $accesses, $status, $siteList, $iconID, $startTime, $endTime, $startDateShow, $startHoursTimeShow, $scheduleSelectedID, $servicesAffectedID, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday, $servicesAffectTexts, $contents) {
		// connect to db
		$connection = Yii::app()->db_cv999_fd_master;
		// get language list
		$languages = Languages::getLanguages();
		// insert
		$command = $connection->createCommand("CALL spBeUpdateAdvisoryDaily(" . $advisoryID . ", '" . $accesses . "', " . $status . ",'" . $siteList . "', " . $scheduleSelectedID . "," . $iconID  . ", '" .
				$startTime . "', '" . $endTime . "', '".  $startDateShow . "', '" . $startHoursTimeShow . "', " . $monday . "," . $tuesday . "," . $wednesday . "," .
				$thursday . "," . $friday . "," . $saturday . "," .$sunday .")");
		// execute
		$command->execute();
		
		// loop to insert content
		foreach ($languages as $l) {
			// insert to table
			// $command = $connection->createCommand("CALL spBeAddAdvisoryContentTempory(" . $contentID . ",'" . $l['language_code'] . "', '" . $servicesAffectTexts[$l['language_code']]. "', '" .$contents[$l['language_code']]. "')");
			$command = $connection->createCommand("CALL spBeUpdateAdvisoryContent(" . $advisoryID . ", '" . $l['language_code'] . "'," . $servicesAffectedID . ", '" . addslashes($servicesAffectTexts[$l['language_code']]) . "', '" . addslashes($contents[$l['language_code']]) . "')");
			// execute
			$command->execute();
		}
		
		// save
		$connection = null;
	}

	/**
	 * 
	 * @param unknown_type $advisoryID
	 * @param unknown_type $accesses
	 * @param unknown_type $status
	 * @param unknown_type $siteList
	 * @param unknown_type $startDate
	 * @param unknown_type $endDate
	 * @param unknown_type $servicesAffectedID
	 * @param unknown_type $servicesAffectTexts
	 * @param unknown_type $contents
	 */
	public function updateSomedays($advisoryID, $accesses, $status, $siteList, $iconID, $startDate, $endDate, $startDateShow, $startHoursTimeShow, $scheduleSelectedID, $servicesAffectedID, $servicesAffectTexts, $contents) {
		$connection = Yii::app()->db_cv999_fd_master;
		// get language list
		$languages = Languages::getLanguages();
		// insert
		$command = $connection->createCommand("CALL spBeUpdateAdvisorySomedays(" . $advisoryID . ",'" . $accesses . "', " . $status . ",'" . $siteList . "', " . $scheduleSelectedID . "," . $iconID . ", '" .
				$startDate . "', '" . $endDate ."', '".  $startDateShow . "', '" . $startHoursTimeShow . "')");
		// execute
		$command->execute();
		
		// loop to insert content
		foreach ($languages as $l) {
			// insert to table
			// $command = $connection->createCommand("CALL spBeAddAdvisoryContentTempory(" . $contentID . ",'" . $l['language_code'] . "', '" . $servicesAffectTexts[$l['language_code']]. "', '" .$contents[$l['language_code']]. "')");
			$command = $connection->createCommand("CALL spBeUpdateAdvisoryContent(" . $advisoryID . ", '" . $l['language_code'] . "'," . $servicesAffectedID . ", '" . addslashes($servicesAffectTexts[$l['language_code']]) . "', '" . addslashes($contents[$l['language_code']]) . "')");
			// execute
			$command->execute();
		}
		
		// save
		$connection = null;
	}

}

?>