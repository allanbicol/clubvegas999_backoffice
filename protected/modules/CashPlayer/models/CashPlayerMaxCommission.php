<?php

/**
 @todo: This is the model class for table "tbl_cash_player_commission".
 @Author: Allan Bicol
 @Since: 02/02/2012
 */



class CashPlayerMaxCommission extends CActiveRecord
{
	

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}
	
	public function tableName()
	{
		return 'tbl_cash_player_commission';
	}

	

}

