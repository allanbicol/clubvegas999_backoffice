<html>
<head>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/advisory.css" />
  	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/ui/minified/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-timepicker-addon.js"></script>
	<!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/ui/i18n/jquery.ui.timepicker.js"></script> -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/emailsetting.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ckeditor/ckeditor.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/emailsetting.css" />

</head>
<script type="text/javascript">
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_email_setting').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Email Setting</a></li>");
</script>

<body>

<?php
 $signupTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.signup',null,TRUE);
 $profilepasswordTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.profilepassword',null,TRUE);
 $forgetpasswordTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.forgetpassword',null,TRUE);
 $paymentTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.payment',null,TRUE);
 
echo "<h3>Email Settings</h3>";


	$this->widget('zii.widgets.jui.CJuiTabs', array(

		'tabs' => array(
				'Signup'=>array('content'=>$signupTab, 'id'=>'tabSignup'),
				'Profile Change Password'=>array('content'=>$profilepasswordTab, 'id'=>'tabProfile'),
				'Forget Password'=>array('content'=>$forgetpasswordTab	, 'id'=>'tabForgetPass'),
				'Payment'=>array('content'=>$paymentTab, 'id'=>'tabPayment'),
		),
		// additional javascript options for the tabs plugin
		'options' => array(
				'collapsible' => false,
		),
		// set id for this widgets
		'id'=>'EmailSettingTab',
	));
?>
<!--  
<br/>
<br/>
<div id="body_header">Email Setting  <input id="log" type="button" value="View Logs" onClick="javascript: viewLogs('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSettingLog');"></div>
<div id="body_wrapper">
<form name="frmEmailSetting" method="post" action="<?php echo Yii::app()->baseUrl?>/index.php?r=SystemSetting/EmailSetting">
	<table cellspacing="0">
    	<tbody>
	    		<tr>
	    			<td class="left">Choose Language</td>
	            	<td>
		                <select name="emailChoseLanguage" id="emailLang" onchange="emailChangeLanguage('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/GetEmailSetting')">
		                <?php 
							$dataReader = TableLanguage::model()->findAll();
							foreach ($dataReader as $row){
								echo '<option value="' . $row['id'] . '">'. $row['language'] . '</option>';
							}
						?>
	                 	</select>
	                </td>
	           	</tr>
                <tr>
                    <td class="left">Email Type</td>
                    <td>
                    	<input type="radio" checked=checked name=email_type id="chkRegister" value="1" onclick="javascript: emailChangeLanguage('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/GetEmailSetting');"/><label for="chkRegister">Register</label>
                    	<input type="radio" name=email_type id="chkReset" value="2" onclick="javascript: emailChangeLanguage('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/GetEmailSetting');"/><label for="chkReset">Reset Password</label>
                   	</td>
                </tr>
                <tr>
                    <td class="left">Email Subject</td>
                    <td><textarea name="content1" rows="2" cols="50" id="txtEmailSubject"></textarea> </td>
                </tr>
                <tr>
                    <td class="left">Content 1</td>
                    <td><input type="text" id="txtContent1"> Customer First Name</td>
                </tr>
                <tr>
                    <td class="left">Content 2</td>
                    <td><textarea rows="2" cols="50" id="txtContent2"></textarea>
                    </td>
                    
                </tr>
                <tr>
                    <td class="left">Content 3</td>
                    <td><textarea rows="12" cols="80" id="txtContent3"></textarea></td>
                </tr>
                <tr>
                	<td class="left"></td>
                	<td colspan="2">
                		<input type="button" name="confirm" value="Confirm" onclick="javascript: emailConfirm('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/SaveEmailSetting');" />
                		<input type="button" name="reset" value="Reset" onclick="javascript: document.getElementById('emailLang').value=1; document.getElementById('chkRegister').checked=true; emailChangeLanguage('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/GetEmailSetting');"/>
                	</td>
                </tr>
                <tr>
                	<td></td>
                	 <td><input type="text" id="txtErrMessage" readonly="true"/></td>
                </tr>
            </tbody>
       </table>
   </form>
  
</div> 
-->
</body>      
</html>

