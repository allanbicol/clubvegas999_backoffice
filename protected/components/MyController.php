<?php
/**
 * @todo	To manage every controller in one place
 * @copyright	CE
 * @author	Kimny MOUK
 * @since	2012-11-05
 */
class MyController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	/**
	 * @todo	Set the choosen language
	 * @copyright	CE
	 * @author	Kimny MOUK
	 * @since	2012-05-15
	 */
public function init()
	{

		if (isset($_POST['chooselan']))
		{
			Yii::app()->language=$_POST['chooselan'];
			Yii::app()->session['chooselanaguage'] = $_POST['chooselan'];
		}
		else
		{
			Yii::app()->language= Yii::app()->session['chooselanaguage'];
		}
		
		if (!isset(Yii::app()->session['account_id']) || Yii::app()->session['account_id'] == '')
		{

    		if (isset(Yii::app()->request->cookies['login_account'])){
    			$account_id = htmlentities(strtoupper(CV999Utility::decryption(Yii::app()->request->cookies['login_account']->value)));
    		}else{
    			$account_id='';
    		}

			//Logout User Session==========================
    		$browser=CV999Utility::getBrowser();
			$postUserSessionLog=new TableUserSessionAuditLog;
			$postUserSessionLog->account_id=$account_id;
			$postUserSessionLog->session_type=0;
			$postUserSessionLog->session_date=date('Y-m-d H:i:s');
			$postUserSessionLog->session_id='';
			$postUserSessionLog->login_ip_address=$_SERVER['REMOTE_ADDR'];
			$postUserSessionLog->browser=$browser['name'].' '.$browser['version'];
			$postUserSessionLog->event=0;
			$postUserSessionLog->fail_reason='';
			$postUserSessionLog->save();
			
			//==========================================
			unset(Yii::app()->request->cookies['login_account']);
			
			$this->redirect(Yii::app()->request->baseUrl.'/index.php?r=Login');
		}
		
	}


}