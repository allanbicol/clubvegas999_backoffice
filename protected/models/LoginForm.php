<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;
	public $verifyCode;
	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password, verifyCode', 'required'),
			// password needs to be authenticated
			array('password', 'authenticate'),
			array('rememberMe', 'boolean'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
			
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
			'verifyCode'=>'Code'
		);
		
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			//$a= $this->_identity->authenticate();
// 			echo "a =" . $a;
// 		//exit();
// 			//if(!$this->_identity->authenticate())
			if(!$this->_identity->authenticate())
			{
// 				echo $this->_identity->errorCode;
// 				echo "</br>" . UserIdentity::ERROR_DISABLE_ACCOUNT;
			//	echo $this->_identity->errorCode; exit();
				switch($this->_identity->errorCode)
				{
// 					case UserIdentity::ERROR_USERNAME_INVALID:
// 						$this->addError('username', 'Incorrect Username.');
// 						break;
// 					case UserIdentity::ERROR_PASSWORD_INVALID:
// 						$this->addError('username', '');
// 						$this->addError('password', 'Incorrect Password.');
// 						break;
					case UserIdentity::ERROR_DISABLE_ACCOUNT:
						$this->addError('username', '');
						$this->addError('password', 'Your account was deactived!');
						$this->checkIfError();
						break;
					case UserIdentity::ERROR_ACCOUNT_ALREADY_LOGIN:
						$this->addError('username', '');
						$this->addError('password', 'Your account was already logined!');
						$this->checkIfError();
						break;
					case UserIdentity::ERROR_WHITE_LIST:
						$this->addError('username', '');
						$this->addError('password', 'You are forbiden to access the AG backend!');
						$this->checkIfError();
						break;
					default:
						
						$this->addError('username', '');
						$this->addError('password', '<br/>Incorrect Username or Password.');
						$this->checkIfError();
						break;
				}
				
			}
				
		}
	}

	public function checkIfError()
	{

		if (isset(yii::app()->session['no_attempt'])){
			yii::app()->session['no_attempt']=yii::app()->session['no_attempt']+1;
			
		}else{
			yii::app()->session['no_attempt']=1;
		}
		yii::app()->session['user_that_attempt']=$this->username;
	}
	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		//exit();
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}
