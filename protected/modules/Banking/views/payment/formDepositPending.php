	<table class="bank-withdraw">
		<thead>
			<tr>
				<td style="width: 200px;">
					
				</td>
				<td style="width: 250px;">
					
				</td>
			</tr>
		</thead>
		<tbody>
			<tr style="background-color: #FFFACD">
				<td>
					<label>Trans: <?php echo $deposit[0]['transaction_number'];?></label>
				</td>
				<td>
					<label>Date: <?php echo $deposit[0]['transaction_date'];?></label>
				</td>
			</tr>			
			<tr>
				<td>
					<label>Account ID:</label>			
				</td>
				<td>
					<label id="account_id"><?php echo $deposit[0]['account_id'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Full Name:</label>			
				</td>
				<td>
					<label><?php echo $deposit[0]['fullname'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Email:</label>			
				</td>
				<td>
					<label><?php echo $deposit[0]['email'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Phone Number:</label>			
				</td>
				<td>
					<label><?php echo $deposit[0]['phone_number'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Deposit Amount:</label>		
				</td>
				<td>
					<label><?php echo $deposit[0]['amount'];?></label>
				</td>
			</tr>			
			<tr>
				<td>
					<label>Currency:</label>
				</td>
				<td>
					<label><?php echo $deposit[0]['currency_name'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Change status to:</label>
				</td>
				<td>
					<select id="txtStatusType">
						<option value=5>Pending</option>
						<option value=6>Complete</option>
					</select>
				</td>
			</tr>			
			<tr>
				<td align="center" colspan="2" style="padding-top: 15px;">					
						<input class="btn red" onclick="javascript: checkPlayerOnlineStatWithdraw();" type="button" value="OK" name="yt0" id="depositConfirm">					
				</td>
			</tr>
		</tbody>
	</table>
		
	<script type="text/javascript">
		var txtID='<?php echo $deposit[0]['account_id'];?>';
		var txtAmount='<?php echo $deposit[0]['amount'];?>';
		var txtTransactionNo='<?php echo $deposit[0]['transaction_number'];?>';
		var txtSubmitDate='<?php echo $deposit[0]['transaction_date'];?>';
		
		function checkPlayerOnlineStatWithdraw(){
		if (document.getElementById('txtStatusType').value == 6){
			jQuery.ajax({
				url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Deposit/DepositProcess',
				type: 'POST',
				data: { 'accountID' : txtID,'task': 'checkPlayerOnlineStat' },
				context: '',
				success: function(data){
	
					if (data==1){
						alert("Player "+ txtID +" should be logout to casino lobby to complete transaction.");
					}
					else{
						jQuery.ajax({
							url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Deposit/DepositProcess',
							type: 'POST',
							data: { 'accountID' : txtID,'task': 'updateBalance','amount':txtAmount,'transNo':txtTransactionNo,'submitDate': txtSubmitDate},
							context: '',
							success: function(data){
								jQuery("#list2").trigger("reloadGrid");
							}
						});
					}
		    	}
			});
		}
			$("#deposit-dialog").dialog("close");
			return false;
	}
	</script>