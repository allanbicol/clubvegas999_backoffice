
function validate(text)
{
	var intval=Math.round(text*100)/100;
	var regex = /^\d{1,3}(,?\d{3})*?(.\d{2})?$/g;
	//return regex.test(intval);
	return !isNaN(parseFloat(intval)) && isFinite(intval);
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}
function checkIt(evt,strValue) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    //var noOfPeriod = strValue.replace(/[^.]/g, '').length;
	//alert("There are " + (theString.replace(/[^.]/g, '').length) + "letter o's in the string");
	//if(charCode==222){alert('oo');}
    if(((charCode >= 48 && charCode <= 57 )||(charCode == 46 )||(charCode == 37 )||(charCode == 39 )||(charCode == 8 ) || (charCode == 13)))
	{
    	return true;
	}
    else
    {
    	return false;
	}
}
function get_cookie(cookie_name)
{
  var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

  if (results)
    return (unescape(results[2]));
  else
    return null;
}
var $addPromoItemDialog='';
$(document).ready(function() {
	$addPromoItemDialog = $('<div></div>')
		.html('<div id="cs_addcurrency"><table border="0" cellpadding="0" cellspacing="0">'
				+'<tr><td class="left">Item Name</td><td class="right"><input type="text" name="txtItemName" id="txtItemName"> <font color="red">*</font></td></tr>'
				+'<tr><td class="left">Equevalent Value</td><td class="right"><input type="text" name="txtValue" id="txtValue"> <font color="red">*</font></td></tr>'
				+'<tr><td class="left">Equevalent Points</td><td class="right"><input type="text" name="txtPoints" id="txtPoints"> <font color="red">*</font></td></tr>'
				+'<tr><td class="left">Stock</td><td class="right"><input type="text" name="txtStock" id="txtStock"> <font color="red">*</font></td></tr>'
				+'</table><div id="cs_footer"><input id="confirm-addItem" type="submit" class="btn red" value="  Confirm  "><br/>'
				+'<input type="text" id="cs_errMessage" readonly="true"/></div></div>')
		.dialog({
			autoOpen: false,
			width: 390,
			title: 'Add Item',
			resizable: false,
			modal: true
		});
		$('#confirm-addItem').click(function(event) {
			if (trim(document.getElementById('txtItemName').value)==''){document.getElementById('cs_errMessage').value='Item name is required!';document.getElementById('txtItemName').focus(); return false;}
   		 	if (trim(document.getElementById('txtValue').value)==''){document.getElementById('cs_errMessage').value='Equevalent value is required!';document.getElementById('txtValue').focus(); return false;}
   		 	if (trim(document.getElementById('txtPoints').value)==''){document.getElementById('cs_errMessage').value='Equevalent points is required!';document.getElementById('txtPoints').focus(); return false;}
   		 	if (trim(document.getElementById('txtStock').value)==''){document.getElementById('cs_errMessage').value='Stock is required!';document.getElementById('txtStock').focus(); return false;}
		 	
   		 	if (validate(document.getElementById('txtValue').value)==false){document.getElementById('cs_errMessage').value='Invalid equevalent value!';document.getElementById('txtValue').focus(); return false;}
   		 	if (validate(document.getElementById('txtPoints').value)==false){document.getElementById('cs_errMessage').value='Invalid equevalent points!';document.getElementById('txtPoints').focus(); return false;}
   		 	if (validate(document.getElementById('txtStock').value)==false){document.getElementById('cs_errMessage').value='Invalid stock!';document.getElementById('txtStock').focus(); return false;}
   		 
			jQuery.ajax({
	    		url: createPromoItemsURL,
	    		type: 'POST',
	    		data: {'itemName': document.getElementById('txtItemName').value, 'equevalentValue' : document.getElementById('txtValue').value,
	    			'equevalentPoints': document.getElementById('txtPoints').value,'stock': document.getElementById('txtStock').value},
	    		context: '',
	    		success: function(msg) {
	    			 if (msg=='suc'){
	    				 alert('New promo item successfully added.');
	    				 $('#list2').trigger( 'reloadGrid' );
	    				 $addPromoItemDialog.dialog('close');
	    			 }else{
	    				 alert('You don\'t have permision for adding new promo item.');
	    			 }
		    	}
	    	});
		});
	});

var $updatePromoItemDialog='';
$(document).ready(function() {
	$updatePromoItemDialog = $('<div></div>')
	.html('<div id="cs_updatecurrency"><table border="0" cellpadding="0" cellspacing="0">'
			+'<tr><td class="left">Item Name</td><td class="right"><input type="text" name="e_txtItemName" id="e_txtItemName"> <font color="red">*</font></td></tr>'
			+'<tr><td class="left">Equevalent Value</td><td class="right"><input type="text" name="e_txtValue" id="e_txtValue"> <font color="red">*</font></td></tr>'
			+'<tr><td class="left">Equevalent Points</td><td class="right"><input type="text" name="e_txtPoints" id="e_txtPoints"> <font color="red">*</font></td></tr>'
			+'<tr><td class="left">Stock</td><td class="right"><input type="text" name="e_txtStock" id="e_txtStock"> <font color="red">*</font></td></tr>'
			+'</table><div id="cs_footer"><input type="submit" id="confirm_update_promo_item" class="btn red" value="  Confirm  "><br/>'
			+'<input type="text" id="e_cs_errMessage" readonly="true"/></div></div>')
	.dialog({
		autoOpen: false,
		width: 390,
		title: 'Update Item',
		resizable: false,
		modal: true
	});
	$('#confirm_update_promo_item').click(function(event) {
		if (trim(document.getElementById('e_txtItemName').value)==''){document.getElementById('e_cs_errMessage').value='Item name is required!';document.getElementById('e_txtItemName').focus(); return false;}
		 	if (trim(document.getElementById('e_txtValue').value)==''){document.getElementById('e_cs_errMessage').value='Equevalent value is required!';document.getElementById('e_txtValue').focus(); return false;}
		 	if (trim(document.getElementById('e_txtPoints').value)==''){document.getElementById('e_cs_errMessage').value='Equevalent points is required!';document.getElementById('e_txtPoints').focus(); return false;}
		 	if (trim(document.getElementById('e_txtStock').value)==''){document.getElementById('e_cs_errMessage').value='Stock is required!';document.getElementById('e_txtStock').focus(); return false;}
	 	
		 	if (validate(document.getElementById('e_txtValue').value)==false){document.getElementById('e_cs_errMessage').value='Invalid equevalent value!';document.getElementById('e_txtValue').focus(); return false;}
		 	if (validate(document.getElementById('e_txtPoints').value)==false){document.getElementById('e_cs_errMessage').value='Invalid equevalent points!';document.getElementById('e_txtPoints').focus(); return false;}
		 	if (validate(document.getElementById('e_txtStock').value)==false){document.getElementById('e_cs_errMessage').value='Invalid stock!';document.getElementById('e_txtStock').focus(); return false;}
		 
		jQuery.ajax({
    		url: updatePromoItemURL,
    		type: 'POST',
    		data: {'id': document.getElementById('e_txtItemName').title,'itemName': document.getElementById('e_txtItemName').value, 'equevalentValue' : document.getElementById('e_txtValue').value,
    			'equevalentPoints': document.getElementById('e_txtPoints').value,'stock': document.getElementById('e_txtStock').value},
    		context:'',
    		success: function(msg) {
    			if (msg=='suc'){
   				 alert('Item successfully updated.');
   				 $('#list2').trigger( 'reloadGrid' );
   				$updatePromoItemDialog.dialog('close');
   			 }else{
   				 alert('You don\'t have permision for adding new promo item.');
   			 }
    			
	    	}
    	});
	});
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
	
function getCurrencyName(aElement) {
	var rowData=aElement.id;

	var Currency=rowData.split(':');
	jQuery.ajax({
		url: getDetailsUrl,
		type: 'POST',
		data: {'id':Currency[0]},
		context: '',
		success: function(data){
			var dataObj = JSON.parse(data);
			document.getElementById('e_txtItemName').title=dataObj.id;
			document.getElementById('e_txtItemName').value=dataObj.item_name;
			document.getElementById('e_txtValue').value=dataObj.equevalent_value;
			document.getElementById('e_txtPoints').value=dataObj.equevalent_points;
			document.getElementById('e_txtStock').value=dataObj.stock;
    	}
	});
}
var cRowNo=0;
$(document).ready(function() {
	var grid=jQuery("#list2");
	grid.jqGrid({ 
		url: promoItemListURL, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['id','Item Name', 'Equevalent Value','Equevalent Points','Stock','Available','Operation'],
	    colModel: [
	      {name: 'id', index: 'id', width: 200, search:true,hidden:true},
	      {name: 'item_name', index: 'item_name', width: 200, search:true},
	      {name: 'equevalent_value', index: 'equevalent_value', width: 150, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'equevalent_points', index: 'equevalent_points', width: 150, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'stock', index: 'stock', width: 80, align:"right"},
	      {name: 'available', index: 'available', width: 80, align:"center",title:false, editable: true, edittype: "checkbox",editoptions: { value:"1:0"},formatter: "checkbox",formatoptions: {disabled : false}},
	      {name: 'operation', index: 'operation', width: 100, align: 'center' , sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="text-decoration: underline"'; },}
	    ],
		loadComplete:function(){
			$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    	
	    	var getColumnIndexByName = function(grid, columnName) {
	            var cm = grid.jqGrid('getGridParam', 'colModel'), i, l;
	            for (i = 1, l = cm.length; i < l; i += 1) {
	                if (cm[i].name === columnName) {
	                    return i; // return the index
	                }
	            }
	            return -1;
	        };
	    	
	    	var iCol = getColumnIndexByName ($(this), 'available'), rows = this.rows, i,
	        c = rows.length;
		    for (i = 1; i < c; i += 1) {
		        $(rows[i].cells[iCol]).change(function (e) {
		            var id = $(e.target).closest('tr')[0].id, isChecked = $(e.target).is(':checked');
			    	var item_id = grid.jqGrid('getCell', id, 'id');
			    	var item_name = grid.jqGrid('getCell', id, 'item_name');
			    	
		            var confirmMsg='';
		            if(isChecked==true){
		            	confirmMsg="Do you want " + item_name + " to be available as promo item? ";
			        }else{
			        	confirmMsg="Do you want " + item_name + " to be unavailable in a promo item?";
				    }
			    	var conf = confirm(confirmMsg);
			        if(conf == true){
			             jQuery.ajax({
		            			url: updateAvailableURL,
		            			type: 'POST',
		            			data: {'id':item_id,'is_checked':isChecked,'item_name':item_name},
		            			context: '',
		            			success: function(data){
		            				if(data == 'no_permission'){
		            					alert('Warning: You don\'t have permission to change Promo Item list!');
		            				}else{
		            					alert(data);	
		            				}
		            				$('#list2').trigger( 'reloadGrid' );
		            	    	}
		            	});
			        }else{
			        	$('#list2').trigger( 'reloadGrid' );
				    }
		        });
		    }
		},
	    rowNum: 20,
	    rownumbers: true,
	    rowList: [20,50, 100, 500],
	    pager: '#pager2',
	    sortname: 'id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Promotion Item List <input class="btn red" type="submit" value="Add" onclick="$addPromoItemDialog.dialog(\'open\');"/> ',
	    viewrecords: true
	});

});