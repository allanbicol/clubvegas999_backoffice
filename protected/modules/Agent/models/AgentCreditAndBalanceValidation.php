<?php

/**
 * @todo AgentCreditAndBalanceValidation Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-07-27
 */
Class AgentCreditAndBalanceValidation
{
 
   /**
    * @todo getAgentCreditAndBalance
    * @copyright CE
    * @author Leo karl
    * @since 2012-07-27
    */
	public function getAgentCreditAndBalance($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT a.account_id,
			       a.agent_type,
			       a.currency_name,
			       CAST(a.available_credit as DECIMAL(10,2)) as available_credit,
			       CAST(a.balance as DECIMAL(10,2)) as balance,
			       CAST(a.assigned_credit as DECIMAL(10,2)) as assigned_credit,
			       SUM(b.available_credit) AS total_downline_credit,
			       SUM(b.balance-(b.balance-b.available_credit)) as balance_validation,
				   (Select sum(amount) from tbl_agent_trans_history where agent_account_id=a.account_id and (trans_type_id=7 or trans_type_id=8)) as amount,
			       (Select sum(amount) from tbl_agent_trans_history where agent_account_id=a.account_id and trans_type_id=4) as withdraw,
			       (Select sum(amount) from tbl_agent_trans_history where agent_account_id=a.account_id and trans_type_id=2) as deposit,
				   (CASE WHEN CAST(SUM(b.available_credit) as DECIMAL(10,2))!=CAST(a.assigned_credit as DECIMAL(10,2)) OR CAST(SUM(b.balance-(b.balance-b.available_credit)) as DECIMAL(10,2))!=CAST(a.assigned_credit as DECIMAL(10,2)) THEN 'Failed' ELSE
        			'Passed' END) as return_value
			FROM vwAgentAndPlayerVaCreditBalanceValidation a LEFT JOIN vwAgentAndPlayerVaCreditBalanceValidation b
			ON b.account_id LIKE CONCAT(a.account_id,'%') 
			WHERE a.account_id LIKE '". $_GET['account_id'] ."%' and a.agent_type!='MEM'
			GROUP BY a.account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getAgentCreditAndBalanceCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-07-27
	 */
	public function getAgentCreditAndBalanceCount()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwAgentAndPlayerVaCreditBalanceValidation
				WHERE account_id LIKE '". $_GET['account_id'] ."%' and agent_type!='MEM'");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getAgentPlayerCredit
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-07-27
	 */
	public function getAgentPlayerCredit($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT SUBSTRING(a.account_id,1,8) as agent_id, 
					a.account_id,
				    CAST(a.credit as DECIMAL(10,2)) as available_credit,
				    CAST(a.balance as DECIMAL(10,2)) as balance,
				    (CASE WHEN sum(b.amount) IS NULL THEN 0 ELSE sum(b.amount) END) as amount,
					(Select sum(amount) from tbl_agent_player_trans_history where agent_account_id=b.agent_account_id and trans_type_id=4) as withdraw,
    				(Select sum(amount) from tbl_agent_player_trans_history where agent_account_id=b.agent_account_id and trans_type_id=2) as deposit,
				    (CASE WHEN CAST(a.credit as DECIMAL(10,2))!=CAST((CASE WHEN sum(b.amount) IS NULL THEN 0 ELSE sum(b.amount) END) as DECIMAL(10,2)) THEN 'Failed' ELSE 'Passed' END) as return_value
				FROM tbl_agent_player a 
				LEFT JOIN tbl_agent_player_trans_history b ON b.agent_account_id LIKE concat(a.account_id,'%') AND (b.trans_type_id=7 OR b.trans_type_id=8)
				WHERE a.account_id LIKE '". $_GET['account_id'] ."%'
				GROUP BY a.account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getAgentPlayerCreditCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-07-27
	 */
	public function getAgentPlayerCreditCount()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_agent_player WHERE account_id LIKE '". $_GET['account_id'] ."%'");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getDuplicateAgentTransHistory
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-07-27
	 */
	public function getDuplicateAgentTransHistory($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
				SELECT * 
				FROM tbl_agent_trans_history pt 
				INNER JOIN (
				        SELECT * FROM tbl_agent_trans_history 
				        GROUP BY transaction_date, agent_account_id,trans_type_id HAVING COUNT(agent_account_id) >1 and trans_type_id<>0) dpt
				ON pt.transaction_date = dpt.transaction_date
		");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getDuplicateAgentTransHistoryCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-07-27
	 */
	public function getDuplicateAgentTransHistoryCount()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
				SELECT COUNT(0)
				FROM tbl_agent_trans_history pt
				INNER JOIN (
				SELECT * FROM tbl_agent_trans_history
				GROUP BY transaction_date, agent_account_id,trans_type_id HAVING COUNT(agent_account_id) >1 and trans_type_id<>0) dpt
				ON pt.transaction_date = dpt.transaction_date
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getDuplicateAgentPlayerTransHistory
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-07-27
	 */
	public function getDuplicateAgentPlayerTransHistory($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
				SELECT * 
				FROM tbl_agent_player_trans_history pt 
				INNER JOIN (
				        SELECT * FROM tbl_agent_player_trans_history 
				        GROUP BY transaction_date, agent_account_id,trans_type_id HAVING COUNT(agent_account_id) >1 and trans_type_id<>0) dpt
				ON pt.transaction_date = dpt.transaction_date
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getDuplicateAgentPlayerTransHistoryCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-07-27
	 */
	public function getDuplicateAgentPlayerTransHistoryCount()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
				SELECT COUNT(0) 
				FROM tbl_agent_player_trans_history pt 
				INNER JOIN (
				        SELECT * FROM tbl_agent_player_trans_history 
				        GROUP BY transaction_date, agent_account_id,trans_type_id HAVING COUNT(agent_account_id) >1 and trans_type_id<>0) dpt
				ON pt.transaction_date = dpt.transaction_date
				");
		$rows = $command->query();
		return $rows;
	}
	
}