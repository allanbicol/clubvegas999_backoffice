<?php
/**
 * @todo CashPlayerRunnerController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerRunnerController extends MyController
{
	public function actionCashPlayerRunnerProcess()
	{
		$cpd = new CashPlayerRunner();
		$cpd->makeNewRunner();
	}
}
