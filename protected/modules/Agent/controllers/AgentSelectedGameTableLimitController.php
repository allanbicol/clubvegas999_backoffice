<?php
/**
 * @todo NewSubCompanyController
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-08
 */
class AgentSelectedGameTableLimitController extends MyController
{
	public function actionGetSelectedGameTableLimit()
	{
		$ca = new SelectedGameTableLimit();
		$ca->getSelectedBaccaratTableLimitIds();
	}
	public function actionGetSelectedRouletteTableLimit()
	{
		$ca = new SelectedGameTableLimit();
		$ca->getSelectedRouletteTableLimitIds();
	}
	public function actionGetSelectedDragonTableLimit()
	{
		$ca = new SelectedGameTableLimit();
		$ca->getSelectedDragonTableLimitIds();
	}
	public function actionGetSelectedBlackjackTableLimit()
	{
		$ca = new SelectedGameTableLimit();
		$ca->getSelectedBlackjackTableLimitIds();
	}
	public function actionAgentGameTableLimit()
	{
		$tl = new AgentGameTableLimit();
		$tl->getGameTableLimit();
	}
	public function actionGetSelectedAmericanRouletteTableLimit()
	{
		$ca = new SelectedGameTableLimit();
		$ca->getSelectedAmericanRouletteTableLimitIds();
	}
	public function actionIndex()
	{
		$this->render("index");
	}
}