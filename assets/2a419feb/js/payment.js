//active menu color
document.getElementById('bankingHeader').className="start active";
document.getElementById('mnu_payment').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Banking <i class='icon-angle-right'></i></li><li><a href='#'>Payment</a></li>");

function tableTransactions(dtFrom,dtTo,accountID,email,currencyID,transactionType,except_test_currency,except_cancel) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
	$(document).ready(function() {
	
	jQuery("#list2").jqGrid({ 
		url: urlPaymentList + '&dtFrom='+ dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&accountID=' + accountID + '&email=' + email + '&currencyID=' + currencyID + '&transactionType=' + transactionType + '&except_test_currency=' + except_test_currency + '&except_cancel=' + except_cancel, 
		datatype: 'json',
	    mtype: 'GET',
	    colNames: [lblTransactionID,lblDateTime,lblTransactionType,lblTransactionVia,lblAccountID,lblFullname,lblCurrency,lblAmount,lblStatus],
	    colModel: [		   	    
	      {name: 'transaction_number', index: 'transaction_number', width: 120,title:false},
	      {name: 'transaction_date', index: 'transaction_date', width: 123,title:false},
	      {name: 'transaction_type', index: 'transaction_type', width: 120,title:false, formatter: withdrawal},
	      {name: 'transaction_via', index: 'transaction_via',  width: 140,title:false},//,formatter:DepositViaFormatter},
	      {name: 'account_id', index: 'account_id', width: 100,title:false},
	      {name: 'fullname', index: 'fullname', width: 100,title:false},
	      {name: 'currency_name', index: 'currency_name', width: 100,title:false},
	      {name: 'amount', index: 'amount', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'status_name', index: 'status_name', width: 100,title:false},	     
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
		},
	    rowNum: 25,
	    rownumbers:true,
	    rowList: [25,50,100,200,500,99999],
	    pager: '#pager2',
	    sortname: 'transaction_date',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: lblTransactions,
	    viewrecords: true,
	    loadonce: true, // to enable sorting on client side
        sortable: true, //to enable sorting
	});
	$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del: false});
	});
}
function DepositViaFormatter(cellvalue, options, rowObject) {
	var celval=cellvalue.split(":");
    $("cellvalue").val(celval[1]);
    if (celval[0]=="bank"){
    	return '<label style="color:black;font-size:12px;">'+ celval[1]+'</label>';
    }else if (celval[0]=="runner"){
    	return '<label style="color:#5E5E5E;font-size:12px">'+ celval[1]+'</label>';
    }else if (celval[0]=="moneybookers"){
    	return '<label style="color:#A3A3FF;font-size:12px">'+ celval[1]+'</label>';
    }else if (celval[0]=="neteller"){
    	return '<label style="color:green;font-size:12px">'+ celval[1]+'</label>';
	}else{
    	return '<label style="color:#5E5E5E;font-size:12px">'+ celval[1]+'</label>';
	}
}
/*
 * @todo withdrawal column formatter
 * @author leokarl
 * @date 2012-12-05 
 */
function withdrawal(cellvalue, options, rowObject){
	// initialize
	var account_id =  (rowObject[4] === undefined) ? rowObject.account_id : rowObject[4];
	var transaction_number =  (rowObject[0] === undefined) ? rowObject.transaction_number : rowObject[0];
	
	
	return '<a class="btn mini red" style="width:100px;" id="withdrawal" onclick="withdraw(\'' + account_id + '\',\'' + transaction_number + '\')">' + getWithdrawTypeByTransNumber(transaction_number) + '</a>';
}

/*
 * @todo get withdraw type by transaction number
 * @author leokarl
 * @since 2013-01-03
 */
function getWithdrawTypeByTransNumber(trans_number){
	return (trans_number.substring(0,2) == 'FW') ? lblFrontEndWithdraw : lblManualWithdraw;
}
/*
 * @todo withdrawal function
 * @author leokarl
 * @date 2012-12-05 
 */
var isProcessingWithdrawal = false;
function withdraw(account_id,transaction_number){
	if(isProcessingWithdrawal){
	       return;
	    }
		
		isProcessingWithdrawal = true;
		jQuery.ajax({
		url: baseURL+'/index.php?r=Banking/Payment/CheckPlayerOnlineStatus',
		type: 'POST',
		data: {'accountID' :account_id, 'trans_number': transaction_number},
		context: '',
		async: false,
		success: function(data){
			isProcessingWithdrawal = false;
			if (data!='die'){
				if (data==0){
					$('#viewWithdraw').load(urlLoadFormWithdraw + '&account_id='+ account_id +'&transaction_number=' + transaction_number,
		    			function(response, status, xhr) {		    	
			  				if (status == "success") {			    		  									
				  				$("#withdraw-dialog").dialog("open"); return false;
				  			}else{
			  					$('#viewWithdraw').load('<p style="color:red;">Something wrong during request!!!</p>');
			  					$("#withdraw-dialog").dialog("open"); return false;
	    		  			}
			  			}
					);
					return false;
				}
				else{

					if(data=='w'){
						$createWaitWithdrawProcessDialog.dialog('open');
						$(".ui-dialog-titlebar-close").hide();
						var check =setInterval(function(){
							jQuery.ajax({
								url: baseURL + '/index.php?r=Banking/Payment/ProcessWithdrawalRequest',
								type: 'POST',
								data: {'accountID' : account_id},
								async: false,
								context: '',
								success: function(data){
									isProcessingWithdrawal = false;
									if (data==0){ // withdrawal successfully
										$('#list2').trigger("reloadGrid");// reload grid
										$(".ui-dialog-titlebar-close").show();
										$createWaitWithdrawProcessDialog.dialog('close');
										
										$('#viewWithdraw').load(urlLoadFormWithdraw + '&account_id='+ account_id +'&transaction_number=' + transaction_number,
							    			function(response, status, xhr) {		    	
								  				if (status == "success") {			    		  									
									  				$("#withdraw-dialog").dialog("open"); return false;
									  			}else{
								  					$('#viewWithdraw').load('<p style="color:red;">Something wrong during request!!!</p>');
								  					$("#withdraw-dialog").dialog("open"); return false;
						    		  			}
								  			}
										);
										clearInterval(check);
										return false;
									}
									else if(data=='h'){
										$createWaitWithdrawProcessDialog.dialog('close');
										alert('Cannot connect to HTV withdrawal API Server.Please try again!.');
										clearInterval(check);
										return false;
									}else if(data=='s'){
										$createWaitWithdrawProcessDialog.dialog('close');
										alert('Cannot connect to Savan withdrawal API Server.Please try again!.');
										clearInterval(check);
										return false;
									}
	
						    	}
							});
						},2000);							
					
					}
					else{
						// force logout player on the lobby
						$createForceLogoutDialog.dialog('open');
						$('#imgLogout').hide();
						document.getElementById("accountIDLogout").value=account_id;
						document.getElementById("casinoId").value = data;
						$(".ui-dialog-titlebar-close").hide();
						//document.getElementById("submitBonus").disabled = false;
						return false;
					}
				}
			}else{
				window.location=baseURL+"/index.php?r=Login";
			}
		}
		});
}

jQuery(document).ready(function() {
    jQuery("#dtFrom").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    jQuery("#dtTo").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});

function qryTransactions()
{
	var transType='';
	var except_test_currency='';
	var except_cancel='';
	if(document.getElementById('chkExceptTestCurrency').checked==true){except_test_currency=1;}else{except_test_currency=0;}
	if(document.getElementById('chkExceptCancel').checked==true){except_cancel=1;}else{except_cancel=0;}
	if(document.getElementById('radAll').checked==true){transType='';}
	if(document.getElementById('radDeposit').checked==true){transType="AND transaction_type='Deposit'";}
	if(document.getElementById('radWithdraw').checked==true){transType="AND transaction_type='Withdraw'";}
	var dtFrom=document.getElementById('dtFrom').value + ' 00:00:00';
	var dtTo=document.getElementById('dtTo').value + ' 23:59:59';
	var accountID=document.getElementById('txtAccountID').value;
	var email=document.getElementById('txtEmail').value;
	var currencyID=document.getElementById('cmbCurrency').value;
	tableTransactions(dtFrom,dtTo,accountID,email,currencyID,transType,except_test_currency,except_cancel);
}
function qryTransactionsToday()
{
	document.getElementById('dtFrom').value=dtToday;
	document.getElementById('dtTo').value=dtToday;
	var transType='';
	var except_test_currency='';
	var except_cancel='';
	if(document.getElementById('chkExceptTestCurrency').checked==true){except_test_currency=1;}else{except_test_currency=0;}
	if(document.getElementById('chkExceptCancel').checked==true){except_cancel=1;}else{except_cancel=0;}
	if(document.getElementById('radAll').checked==true){transType='';}
	if(document.getElementById('radDeposit').checked==true){transType="AND transaction_type='Deposit'";}
	if(document.getElementById('radWithdraw').checked==true){transType="AND transaction_type='Withdraw'";}
	
	var dtFrom=document.getElementById('dtFrom').value + ' 00:00:00';
	var dtTo=document.getElementById('dtTo').value + ' 23:59:59';
	var accountID=document.getElementById('txtAccountID').value;
	var email=document.getElementById('txtEmail').value;
	var currencyID=document.getElementById('cmbCurrency').value;
	tableTransactions(dtFrom,dtTo,accountID,email,currencyID,transType,except_test_currency,except_cancel);
}
function qryTransactionsYesterday()
{
	document.getElementById('dtFrom').value=dtYesterday;
	document.getElementById('dtTo').value=dtYesterday;
	var transType='';
	var except_test_currency='';
	var except_cancel='';
	if(document.getElementById('chkExceptTestCurrency').checked==true){except_test_currency=1;}else{except_test_currency=0;}
	if(document.getElementById('chkExceptCancel').checked==true){except_cancel=1;}else{except_cancel=0;}
	if(document.getElementById('radAll').checked==true){transType='';}
	if(document.getElementById('radDeposit').checked==true){transType="AND transaction_type='Deposit'";}
	if(document.getElementById('radWithdraw').checked==true){transType="AND transaction_type='Withdraw'";}
	
	var dtFrom=document.getElementById('dtFrom').value + ' 00:00:00';
	var dtTo=document.getElementById('dtTo').value + ' 23:59:59';
	var accountID=document.getElementById('txtAccountID').value;
	var email=document.getElementById('txtEmail').value;
	var currencyID=document.getElementById('cmbCurrency').value;
	tableTransactions(dtFrom,dtTo,accountID,email,currencyID,transType,except_test_currency,except_cancel);
}
function showExceptTestCurrency(tool)
{
	if(tool.value==''){
		document.getElementById('chkExceptTestCurrency').style.display="inline";
		document.getElementById('lblExceptTestCurrency').style.display="inline";
		document.getElementById('chkExceptTestCurrency').checked=true;
	}else{
		document.getElementById('chkExceptTestCurrency').style.display="none";
		document.getElementById('lblExceptTestCurrency').style.display="none";
		document.getElementById('chkExceptTestCurrency').checked=false;
	}
}
function alertme(){
	alert('yes!');
}







var $createForceLogoutDialog='';
$(document).ready(function() {
	$createForceLogoutDialog = $('<div></div>')
		.html( '<div id="dialogBonusError">'
		 +'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
		 +'<tr><td class="row"><b>Warning</b></td><td class="row1"><font color="red">The player must logout from the casino lobby first before you can process this operation.</font></td></tr>'
		 +'<tr><td class="row"></td><td></td></tr>'
		 +'<tr><td class="row"><b>Account ID</b></td><td class="row1"><input id="accountIDLogout" name="accountIDLogout" class="depConfirmInput"  readonly="true"/><input id="casinoId" name="casinoId" style="display: none;" type="hidden"/></b></td></tr>'
		 +'<tr><td class="row"></td><td class="row1"><input style="width:100px;height:30px" id="btnForceLogout" type="button" value="Force Logout" onclick="javascript:logoutPlayer();" />&nbsp;&nbsp;<input style="width:100px;height:30px" id="btnCancelLogout" type="button" value="Cancel" onclick="javascript:$createForceLogoutDialog.dialog(\'close\'); $(\'.ui-dialog-titlebar-close\').show();" /></td></tr>'
		 +'</table><img id="imgLogout" style="cursor:pointer; width:470px;height:20px" src="'+ assetURL +'/images/logout-loader.gif" ></div>'
			)
		.dialog({
			autoOpen: false,
			closeOnEscape: false,
			width: 500,
			title: 'Force Logout',
			resizable: false,
			modal: true,
			cache: false,
		});
});
var $createWaitWithdrawProcessDialog='';
$(document).ready(function() {
	$createWaitWithdrawProcessDialog = $('<div></div>')
		.html( '<div id="dialogBonusError">'
		 +'<font color="red"><b>System is transfering balance from casino lobby to Clubvegas999 main balance.</b></font>'
		 +'</br>'
		 +'</table><img id="imgLogout1" style="cursor:pointer; width:470px;height:20px" src="'+ assetURL +'/images/logout-loader.gif" ></div>'
			)
		.dialog({
			autoOpen: false,
			closeOnEscape: false,
			width: 500,
			title: 'Please wait!',
			resizable: false,
			modal: true,
			cache: false,
		});
});
//Logout function

function logoutPlayer()
{
	jQuery.ajax({
		url: baseURL +'/index.php?r=Banking/Payment/ForceLogOutPlayer',
		type: 'POST',
		data: {'player_id' : $('#accountIDLogout').val()},
		context: '',
		success: function(data){
			document.getElementById("btnForceLogout").disabled = true;
			document.getElementById("btnCancelLogout").disabled = true;
			$('#imgLogout').show();
			startCounting();
    	}
	});
}
function startCounting(){
	var check=null;
	var check =setInterval(function(){
	jQuery.ajax({
		url: baseURL + '/index.php?r=Banking/Payment/CheckIfStillProcessing',
		type: 'POST',
		data: {'accountID' : document.getElementById('accountIDLogout').value,'casinoID':document.getElementById('casinoId').value},
		context: '',
		success: function(data){
			if (data==0){
				$(".ui-dialog-titlebar-close").show();
				$createForceLogoutDialog.dialog('close');
				location.reload(); 
			}
    	}
	});
	},2000);
}