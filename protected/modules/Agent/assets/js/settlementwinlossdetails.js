/**
 * @todo export to excel
 * @author leokarl
 * @since 2013-01-24
 */
function wd_exportWinLossToExcel(filename, html_table){
	var game = $('#wd_cmbGame')[0].value;
	var currency = $('#wd_cmbCurrency')[0].value;
	testChecked=0;
	if (document.getElementById("wd_chkTestCurrency").checked==true){
		 testChecked=1;
	}
	var dateFrom	=	$('#wd_dtFrom')[0].value + ' ' + $('#wd_cmbTime1')[0].value + ':00:00';
	var dateTo	=	$('#wd_dtTo')[0].value + ' ' + $('#wd_cmbTime2')[0].value + ':59:59';
	var accountId = $('#wd_txtAccountID')[0].value;
	var filenameSplit=filename.split("_");
	
	if (document.getElementById('wd_chkVIG').checked==true && document.getElementById('wd_chkHTV').checked!=true && document.getElementById('wd_chkSAVAN').checked!=true){
		document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
	}else if (document.getElementById('wd_chkVIG').checked!=true && document.getElementById('wd_chkHTV').checked==true && document.getElementById('wd_chkSAVAN').checked!=true){
		document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
	}else if (document.getElementById('wd_chkVIG').checked!=true && document.getElementById('wd_chkHTV').checked!=true && document.getElementById('wd_chkSAVAN').checked==true){
		document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
	}else if (document.getElementById('wd_chkVIG').checked==true && document.getElementById('wd_chkHTV').checked==true && document.getElementById('wd_chkSAVAN').checked!=true){
		if(filenameSplit[0]=='COSTAVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}else if (document.getElementById('wd_chkVIG').checked==true && document.getElementById('wd_chkHTV').checked!=true && document.getElementById('wd_chkSAVAN').checked==true){
		if(filenameSplit[0]=='COSTAVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}else if (document.getElementById('wd_chkVIG').checked!=true && document.getElementById('wd_chkHTV').checked==true && document.getElementById('wd_chkSAVAN').checked==true){
		if(filenameSplit[0]=='HATIENVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}else if (document.getElementById('wd_chkVIG').checked==true && document.getElementById('wd_chkHTV').checked==true && document.getElementById('wd_chkSAVAN').checked==true){
		if(filenameSplit[0]=='COSTAVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else if (filenameSplit[0]=='HATIENVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[2].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}
	document.forms[1].csvBuffer.value = ""; // clear buffer
	document.forms[1].csvBuffer.value = html_table;
    document.forms[1].method = 'POST';
    document.forms[1].action = wd_baseURL + '/index.php?r=Agent/AgentWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
    document.forms[1].target = '_top';
    document.forms[1].submit();
}
function wd_exportAllWinLossToExcel(filename,url){
    $.ajax({	
		 url:url, 
		 async:true,
		 success: function(result) {
				var data =result.split("<BREAK>");
		
				var game = $('#wd_cmbGame')[0].value;
				var currency = $('#wd_cmbCurrency')[0].value;
				testChecked=0;
				if (document.getElementById("wd_chkTestCurrency").checked==true){
					 testChecked=1;
				}
				var dateFrom	=	$('#wd_dtFrom')[0].value + ' ' + $('#wd_cmbTime1')[0].value + ':00:00';
				var dateTo	=	$('#wd_dtTo')[0].value + ' ' + $('#wd_cmbTime2')[0].value + ':59:59';
				var accountId = $('#wd_txtAccountID')[0].value;

				document.forms[1].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				
				document.forms[1].csvBuffer.value = ""; // clear buffer
				document.forms[1].csvBuffer.value = data[2]+''+data[0];
			    document.forms[1].method = 'POST';
			    document.forms[1].action = baseURL + '/index.php?r=Agent/AgentWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
			    document.forms[1].target = '_top';
			    document.forms[1].submit();
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
}
/**
 * @todo winloss column header
 * @author leokarl
 * @since 2012-12-15
 * @param string account_id 
 * @returns column header
 */
function wd_getWinLossColumnHeaderByAccountId(account_id){
	if(account_id.length == 0){
    	return wd_lblMainCompany;
    }else if(account_id.length == 2){
    	return wd_lblSubCompany;
	}else if(account_id.length == 4){
		return wd_lblSeniorMasterAgent;
	}else if(account_id.length == 6){
		return wd_lblMasterAgent;
	}else if(account_id.length == 8){
		return wd_lblAgent;
	}
}

/**
 * @todo winloss column header
 * @author leokarl
 * @since 2012-12-15
 * @param string account_id 
 * @returns agent column header
 */
function wd_getAgentColumnHeaderByAccountId(account_id){
	if(account_id.length==0){
    	return wd_lblSubCompanyTotal;
    }else if(account_id.length==2){
    	return wd_lblSeniorMasterAgentTotal;
	}else if(account_id.length==4){
		return wd_lblMasterAgentTotal;
	}else if(account_id.length==6){
		return wd_lblAgentTotal;
	}
}


function wd_checkIt(evt,id) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    var strText=document.getElementById(id).value;
    var countStr=(strText.split(".").length - 1);
    if (charCode==46){
	    //allow only one period
    	if(countStr > 0){
	    	return false;
	    }
	}
    if (charCode > 31 && ((charCode < 48 && charCode!=46) || charCode > 57)) {
        status = "This field accepts numbers only.";
        return false;
    }
    status = "";
    return true;
}

function wd_onMouseOutDiv(id){
	document.getElementById('id').addEventListener('mouseout',wd_onMouseOut,true);
}
function wd_onMouseOut(event) {
    //this is the original element the event handler was assigned to
        e = event.toElement || event.relatedTarget;
        if (e.parentNode == this || e == this) {
           return;
        }
        hideDiv("wd_dvSharingCommission");
    // handle mouse event here!
}
$(document).ready(function() { 
	var div1Tag = document.createElement("div"); 
    div1Tag.id = "wd_dvSharingCommission"; 
    div1Tag.setAttribute("align", "center"); 
    div1Tag.setAttribute("onmouseout","javascript: document.getElementById('wd_dvSharingCommission').addEventListener('mouseout',wd_onMouseOut,true);");
    div1Tag.style.margin = "0px auto"; 
    div1Tag.className = "dynamicDiv"; 
    document.body.appendChild(div1Tag);
    hideDiv('wd_dvSharingCommission');
}); 

function hideDiv(divID) { 
	if (document.getElementById) { 
	 	document.getElementById(divID).style.visibility = 'hidden'; 
	} 
}
	
function showDiv(divID,objXY,x,y) { 
	//get link position
	for (var lx=0, ly=0;
	objXY != null;
    lx += objXY.offsetLeft, ly += objXY.offsetTop, objXY = objXY.offsetParent);
    
    //set div new position
	document.getElementById(divID).style.left= (x-65) +"px";
	document.getElementById(divID).style.top= (y+30) +"px";
	document.getElementById(divID).style.visibility = 'visible'; 
}

jQuery(document).ready(function() {
    jQuery("#wd_dtFrom").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    jQuery("#wd_dtTo").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
    
    jQuery("#dtFrom_winloss_details").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    jQuery("#dtTo_winloss_details").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});

function wd_hideBettingDetails(){
	//start: show parent grids
    //document.getElementById('wd_qry_costavegas_result').style.display='block';
    document.getElementById('wd_qry_costavegas_result_details').style.display='block';
    //end: show parent grids
    //start: clear betting details
    document.getElementById('wd_qry_costavegas_betting_details').innerHTML='';
    document.getElementById('wd_qry_costavegas_betting_details_summary_footer').style.display='none';
    //end: clear betting details
}
function wd_hideBettingDetails_htv(){
	//start: show parent grids
    //document.getElementById('wd_qry_htv999_result').style.display='block';
    document.getElementById('wd_qry_htv999_result_details').style.display='block';
    //end: show parent grids
    //start: clear betting details
    document.getElementById('wd_qry_htv999_betting_details').innerHTML='';
    document.getElementById('wd_qry_htv999_betting_details_summary_footer').style.display='none';
    //end: clear betting details
}
function wd_hideBettingDetails_savan(){
	//start: show parent grids
    //document.getElementById('wd_qry_savan_vegas_result').style.display='block';
    document.getElementById('wd_qry_savan_vegas_result_details').style.display='block';
    //end: show parent grids
    //start: clear betting details
    document.getElementById('wd_qry_savan_vegas_betting_details').innerHTML='';
    document.getElementById('wd_qry_savan_vegas_betting_details_summary_footer').style.display='none';
    //end: clear betting details
}
function clearContainer(){
	//document.getElementById('wd_qry_costavegas_result_details').innerHTML='';
	//document.getElementById('wd_qry_htv999_result_details').innerHTML='';
	//document.getElementById('wd_qry_savan_vegas_result_details').innerHTML='';
}

function wd_costaTotalConversion(currency){
	var grid = jQuery("#wd_lstTotal");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_lstTotal").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var tips=grid.getCell(x, 'tips');
	    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
	    	var mc_comm=grid.getCell(x, 'mc_comm');
	    	var mc_total=grid.getCell(x, 'mc_total');
	    	var vig_win_loss=grid.getCell(x, 'vig_win_loss');
	    	var vig_tips=grid.getCell(x, 'vig_tips');
	    	var vig_total=grid.getCell(x, 'vig_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	    	
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var tips_convert=((parseFloat(tips)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var vig_win_loss_convert=((parseFloat(vig_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var vig_tips_convert=((parseFloat(vig_tips)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var vig_total_convert=((parseFloat(vig_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 1, fltRate[0]);
	    	grid.jqGrid('setCell', x, 2, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 3, mc_win_loss_convert);
	    	grid.jqGrid('setCell', x, 4, mc_comm_convert);
	    	grid.jqGrid('setCell', x, 5, tips_convert);
	    	grid.jqGrid('setCell', x, 6, mc_total_convert);
	    	grid.jqGrid('setCell', x, 7, vig_win_loss_convert);
	    	grid.jqGrid('setCell', x, 8, vig_tips_convert);
	    	grid.jqGrid('setCell', x, 9, vig_total_convert);
	    	grid.jqGrid('setCell', x, 10, company_convert);
	    	grid.jqGrid('setCell', x, 11, fltRate[1]);
	    	
	     	x++;
	    }

	    var valid_bet_sum=grid.jqGrid('getCol', 'valid_bet', false, 'sum');
	    var tips_sum= grid.jqGrid('getCol', 'tips', false, 'sum');
	    var mc_win_loss_sum=grid.jqGrid('getCol', 'mc_win_loss', false, 'sum');
	    var mc_comm_sum=grid.jqGrid('getCol', 'mc_comm', false, 'sum');
	    var mc_total_sum=grid.jqGrid('getCol', 'mc_total', false, 'sum');
	    var vig_win_loss_sum=grid.jqGrid('getCol', 'vig_win_loss', false, 'sum');
	    var vig_tips_sum=grid.jqGrid('getCol', 'vig_tips', false, 'sum');
	    var vig_total_sum=grid.jqGrid('getCol', 'vig_total', false, 'sum');
	    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
        grid.jqGrid('footerData','set', {valid_bet: valid_bet_sum,tips: tips_sum,mc_win_loss: mc_win_loss_sum,mc_comm: mc_comm_sum,mc_total: mc_total_sum,
	        vig_win_loss: vig_win_loss_sum, vig_tips: vig_tips_sum, vig_total: vig_total_sum, company: company_sum});
	}else{
		grid.trigger("reloadGrid");
		grid.jqGrid('footerData','set', {valid_bet: '',tips: '',mc_win_loss: '',mc_comm: '',mc_total: '',vig_win_loss: '', vig_tips: '', vig_total: '', company: ''});
	}
}
function wd_costaWinLossConversion(currency){
	var grid = jQuery("#wd_list1_costa");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_list1_costa").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var average_bet=grid.getCell(x, 'average_bet');
	    	var total=grid.getCell(x, 'total');
	    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
	    	var mc_comm=grid.getCell(x, 'mc_comm');
	    	var tips=grid.getCell(x, 'tips');
	    	var mc_total=grid.getCell(x, 'mc_total');
	    	var vig_win_loss=grid.getCell(x, 'vig_win_loss');
	    	var vig_tips=grid.getCell(x, 'vig_tips');
	    	var vig_total=grid.getCell(x, 'vig_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var tips_convert=((parseFloat(tips)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var vig_win_loss_convert=((parseFloat(vig_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var vig_tips_convert=((parseFloat(vig_tips)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var vig_total_convert=((parseFloat(vig_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	grid.jqGrid('setCell', x, 5, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, total_convert);
	    	grid.jqGrid('setCell', x, 8, mc_win_loss_convert);
	    	grid.jqGrid('setCell', x, 9, mc_comm_convert);
	    	grid.jqGrid('setCell', x, 10, tips_convert);
	    	grid.jqGrid('setCell', x, 11, mc_total_convert);
	    	grid.jqGrid('setCell', x, 12, vig_win_loss_convert);
	    	grid.jqGrid('setCell', x, 13, vig_tips_convert);
	    	grid.jqGrid('setCell', x, 14, vig_total_convert);
	    	grid.jqGrid('setCell', x, 15, company_convert);
	    	grid.jqGrid('setCell', x, 16, fltRate[1]);
	     	x++;
	    }//costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("costa_sum_data");
	    var i=0;
	    var xrate=11;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=12;
			}
			//alert(xrate + ':   ' + labels[i].innerHTML + '  /  ' + labels[xrate].innerHTML + '  *  ' + fltRate[1]);
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
}
function wd_costaMemberWinLossConversion(currency){
	var grid = jQuery("#wd_list1_costa");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_list1_costa").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var average_bet=grid.getCell(x, 'average_bet');
	    	var win_loss=grid.getCell(x, 'win_loss');
	    	var commission=grid.getCell(x, 'commission');
	    	var tips=grid.getCell(x, 'tips');
	    	var total=grid.getCell(x, 'total');
	    	var balance=grid.getCell(x, 'balance');
	    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
	    	var agt_comm=grid.getCell(x, 'agt_comm');
	    	var agt_tips=grid.getCell(x, 'agt_tips');
	    	var agt_total=grid.getCell(x, 'agt_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var tips_convert=((parseFloat(tips)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_tips_convert=((parseFloat(agt_tips)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	grid.jqGrid('setCell', x, 5, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, win_loss_convert);
	    	grid.jqGrid('setCell', x, 8, commission_convert);
	    	grid.jqGrid('setCell', x, 9, tips_convert);
	    	grid.jqGrid('setCell', x, 10, total_convert);
	    	grid.jqGrid('setCell', x, 11, balance_convert);
	    	grid.jqGrid('setCell', x, 12, agt_win_loss_convert);
	    	grid.jqGrid('setCell', x, 13, agt_comm_convert);
	    	grid.jqGrid('setCell', x, 14, agt_tips_convert);
	    	grid.jqGrid('setCell', x, 15, agt_total_convert);
	    	grid.jqGrid('setCell', x, 16, company_convert);
	    	grid.jqGrid('setCell', x, 17, fltRate[1]);
	     	x++;
	    }//costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("costa_sum_data");
	    var i=0;
	    var xrate=12;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=13;
			}
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
}
function wd_htvTotalConversion(currency){
	var grid = jQuery("#wd_lstTotal_htv");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_lstTotal_htv").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	var total_stake=grid.getCell(x, 'total_stake');
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var win_loss=grid.getCell(x, 'win_loss');
	    	var commission=grid.getCell(x, 'commission');
	    	var company=grid.getCell(x, 'company');
	    	var total=grid.getCell(x, 'total');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 1, fltRate[0]);
	    	grid.jqGrid('setCell', x, 2, total_stake_convert);
	    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 4, win_loss_convert);
	    	grid.jqGrid('setCell', x, 5, commission_convert);
	    	grid.jqGrid('setCell', x, 6, total_convert);
	    	grid.jqGrid('setCell', x, 7, company_convert);
	    	grid.jqGrid('setCell', x, 8, fltRate[1]);
	    	
	     	x++;
	    }

	    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
	    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
	    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
	    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
	    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
	    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum,company: company_sum});
	}else{
		grid.trigger("reloadGrid");
		grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',total: '',company: ''});
	}
}
function wd_htvWinLossConversion(currency){
	var grid = jQuery("#wd_lstHTV1");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_lstHTV1").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
		      
	    	//var bet_count=grid.getCell(x, 'bet_count');
	    	var average_bet=grid.getCell(x, 'average_bet');
	    	var total_stake=grid.getCell(x, 'total_stake');
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var total=grid.getCell(x, 'total');
	    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
	    	var mc_comm=grid.getCell(x, 'mc_comm');
	    	var mc_total=grid.getCell(x, 'mc_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, total_stake_convert);
	    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 9, total_convert);
	    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
	    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
	    	grid.jqGrid('setCell', x, 12, mc_total_convert);
	    	grid.jqGrid('setCell', x, 13, company_convert);
	    	grid.jqGrid('setCell', x, 14, fltRate[1]);
	     	x++;
	    }//costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("htv_sum_data");
	    var i=0;
	    var xrate=8;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=9;
			}
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
	
	
}
function wd_htvMemberWinLossConversion(currency){
	var grid = jQuery("#wd_lstHTV1");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_lstHTV1").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	
	    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var win_loss=grid.getCell(x, 'win_loss');
	    	var commission=grid.getCell(x, 'commission');
	    	var total=grid.getCell(x, 'total');
	    	var balance=grid.getCell(x, 'balance');
	    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
	    	var agt_comm=grid.getCell(x, 'agt_comm');
	    	var agt_total=grid.getCell(x, 'agt_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, total_stake_convert);
	    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 9, win_loss_convert);
	    	grid.jqGrid('setCell', x, 10, commission_convert);
	    	grid.jqGrid('setCell', x, 11, total_convert);
	    	grid.jqGrid('setCell', x, 12, balance_convert);
	    	grid.jqGrid('setCell', x, 13, agt_win_loss_convert);
	    	grid.jqGrid('setCell', x, 14, agt_comm_convert);
	    	grid.jqGrid('setCell', x, 15, agt_total_convert);
	    	grid.jqGrid('setCell', x, 16, company_convert);
	    	grid.jqGrid('setCell', x, 17, fltRate[1]);
	     	x++;
	    }//costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("htv_sum_data");
	    var i=0;
	    var xrate=11;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=12;
			}
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
}
function wd_savanTotalConversion(currency){
	var grid = jQuery("#wd_lstTotal_savan");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_lstTotal_savan").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	var total_stake=grid.getCell(x, 'total_stake');
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var win_loss=grid.getCell(x, 'win_loss');
	    	var commission=grid.getCell(x, 'commission');
	    	var company=grid.getCell(x, 'company');
	    	var total=grid.getCell(x, 'total');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 1, fltRate[0]);
	    	grid.jqGrid('setCell', x, 2, total_stake_convert);
	    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 4, win_loss_convert);
	    	grid.jqGrid('setCell', x, 5, commission_convert);
	    	grid.jqGrid('setCell', x, 6, total_convert);
	    	grid.jqGrid('setCell', x, 7, company_convert);
	    	grid.jqGrid('setCell', x, 8, fltRate[1]);
	    	
	     	x++;
	    }

	    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
	    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
	    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
	    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
	    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
	    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum, company: company_sum});
	}else{
		grid.trigger("reloadGrid");
		grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',total: '',company: ''});
	}
}
function wd_savanWinLossConversion(currency){
	var grid = jQuery("#wd_lstSAVAN1");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_lstSAVAN1").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
		      
	    	//var bet_count=grid.getCell(x, 'bet_count');
	    	var average_bet=grid.getCell(x, 'average_bet');
	    	var total_stake=grid.getCell(x, 'total_stake');
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var total=grid.getCell(x, 'total');
	    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
	    	var mc_comm=grid.getCell(x, 'mc_comm');
	    	var mc_total=grid.getCell(x, 'mc_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, total_stake_convert);
	    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 9, total_convert);
	    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
	    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
	    	grid.jqGrid('setCell', x, 12, mc_total_convert);
	    	grid.jqGrid('setCell', x, 13, company_convert);
	    	grid.jqGrid('setCell', x, 14, fltRate[1]);
	     	x++;
	    }//costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("savan_sum_data");
	    var i=0;
	    var xrate=8;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=9;
			}
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
	
	
}
function wd_savanMemberWinLossConversion(currency){
	var grid = jQuery("#wd_lstSAVAN1");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#wd_lstSAVAN1").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	
	    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var win_loss=grid.getCell(x, 'win_loss');
	    	var commission=grid.getCell(x, 'commission');
	    	var total=grid.getCell(x, 'total');
	    	var balance=grid.getCell(x, 'balance');
	    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
	    	var agt_comm=grid.getCell(x, 'agt_comm');
	    	var agt_total=grid.getCell(x, 'agt_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, total_stake_convert);
	    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 9, win_loss_convert);
	    	grid.jqGrid('setCell', x, 10, commission_convert);
	    	grid.jqGrid('setCell', x, 11, total_convert);
	    	grid.jqGrid('setCell', x, 12, balance_convert);
	    	grid.jqGrid('setCell', x, 13, agt_win_loss_convert);
	    	grid.jqGrid('setCell', x, 14, agt_comm_convert);
	    	grid.jqGrid('setCell', x, 15, agt_total_convert);
	    	grid.jqGrid('setCell', x, 16, company_convert);
	    	grid.jqGrid('setCell', x, 17, fltRate[1]);
	     	x++;
	    }
	    //costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("savan_sum_data");
	    var i=0;
	    var xrate=11;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=12;
			}
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
}
function wd_copyVIGValue(val){
	
	document.getElementById('wd_txtVIGsharing').value=val;
}

function wd_showHTVBettingFooter(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency){
	document.getElementById('wd_lblHTVBettingSummaryPlayerID').innerHTML=strAccountID;
	document.getElementById('wd_qry_htv999_betting_details_summary_footer').style.display='block';
	//Current Page
	var grid = jQuery("#wd_wd_list5");
	var noRow=$("#wd_wd_list5").getGridParam("reccount");
    var x=1;
    var bet_amount=0;
    var amount_wagers=0;
	var win_loss=0;
	var commission=0;
	var total=0;
	var balance=0;
    while (x<=noRow)
    {
    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
    	amount_wagers+=parseFloat(grid.getCell(x, 'amount_wagers'));
    	//win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
    	win_loss+=parseFloat(grid.getCell(x, 'win_loss').substring(grid.getCell(x, 'win_loss').indexOf('>')+1,grid.getCell(x, 'win_loss').indexOf('</')));
    	commission+=parseFloat(grid.getCell(x, 'commission'));
    	total+=parseFloat(grid.getCell(x, 'total'));
    	if(x==1){
    		balance+=parseFloat(grid.getCell(x, 'balance'));
    	}
     	x++;
    }
    document.getElementById('wd_lblCPNoOfBet_htv').innerHTML=x-1;
    document.getElementById('wd_lblCPBetAmount_htv').innerHTML=parseFloat(bet_amount).toFixed(2);
    document.getElementById('wd_lblCPValidBet_htv').innerHTML=parseFloat(amount_wagers).toFixed(2);
    document.getElementById('wd_lblCPWinLoss_htv').innerHTML=parseFloat(win_loss).toFixed(2);
    document.getElementById('wd_lblCPCommission_htv').innerHTML=parseFloat(commission).toFixed(2);
    document.getElementById('wd_lblCPTotal_htv').innerHTML=parseFloat(total).toFixed(2);
    document.getElementById('wd_lblCPBalance_htv').innerHTML=parseFloat(balance).toFixed(2);
    if(parseFloat(bet_amount)<0){document.getElementById('wd_lblCPBetAmount_htv').style.color='red';}else{document.getElementById('wd_lblCPBetAmount_htv').style.color='black';}
    if(parseFloat(amount_wagers)<0){document.getElementById('wd_lblCPValidBet_htv').style.color='red';}else{document.getElementById('wd_lblCPValidBet_htv').style.color='black';}
    if(parseFloat(win_loss)<0){document.getElementById('wd_lblCPWinLoss_htv').style.color='red';}else{document.getElementById('wd_lblCPWinLoss_htv').style.color='black';}
    if(parseFloat(commission)<0){document.getElementById('wd_lblCPCommission_htv').style.color='red';}else{document.getElementById('wd_lblCPCommission_htv').style.color='black';}
    if(parseFloat(total)<0){document.getElementById('wd_lblCPTotal_htv').style.color='red';}else{document.getElementById('wd_lblCPTotal_htv').style.color='black';}
    if(parseFloat(balance)<0){document.getElementById('wd_lblCPBalance_htv').style.color='red';}else{document.getElementById('wd_lblCPBalance_htv').style.color='black';}
	//All Pages
	
    jQuery.ajax({
		url: wd_urlHTV999WinLossDetailsSummaryFooter + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			var fltTotal = data.split(';');
			document.getElementById('wd_lblAPNoOfBet_htv').innerHTML=fltTotal[0];
		    document.getElementById('wd_lblAPBetAmount_htv').innerHTML=fltTotal[1];
		    document.getElementById('wd_lblAPValidBet_htv').innerHTML=fltTotal[2];
		    document.getElementById('wd_lblAPWinLoss_htv').innerHTML=fltTotal[3];
		    document.getElementById('wd_lblAPCommission_htv').innerHTML=fltTotal[4];
		    document.getElementById('wd_lblAPTotal_htv').innerHTML=fltTotal[5];
		    document.getElementById('wd_lblAPBalance_htv').innerHTML=fltTotal[6];
		    if(parseFloat(fltTotal[1])<0){document.getElementById('wd_lblAPBetAmount_htv').style.color='red';}else{document.getElementById('wd_lblAPBetAmount_htv').style.color='black';}
		    if(parseFloat(fltTotal[2])<0){document.getElementById('wd_lblAPValidBet_htv').style.color='red';}else{document.getElementById('wd_lblAPValidBet_htv').style.color='black';}
		    if(parseFloat(fltTotal[3])<0){document.getElementById('wd_lblAPWinLoss_htv').style.color='red';}else{document.getElementById('wd_lblAPWinLoss_htv').style.color='black';}
		    if(parseFloat(fltTotal[4])<0){document.getElementById('wd_lblAPCommission_htv').style.color='red';}else{document.getElementById('wd_lblAPCommission_htv').style.color='black';}
		    if(parseFloat(fltTotal[5])<0){document.getElementById('wd_lblAPTotal_htv').style.color='red';}else{document.getElementById('wd_lblAPTotal_htv').style.color='black';}
		    if(parseFloat(fltTotal[6])<0){document.getElementById('wd_lblAPBalance_htv').style.color='red';}else{document.getElementById('wd_lblAPBalance_htv').style.color='black';}
    	}
	});
}
function wd_showSAVANBettingFooter(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency){
	document.getElementById('wd_lblSAVANBettingSummaryPlayerID').innerHTML=strAccountID;
	document.getElementById('wd_qry_savan_vegas_betting_details_summary_footer').style.display='block';
	//Current Page
	var grid = jQuery("#wd_list6");
	var noRow=$("#wd_list6").getGridParam("reccount");
    var x=1;
    var bet_amount=0;
    var amount_wagers=0;
	var win_loss=0;
	var commission=0;
	var total=0;
	var balance=0;
    while (x<=noRow)
    {
    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
    	amount_wagers+=parseFloat(grid.getCell(x, 'amount_wagers'));
    	//win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
    	win_loss+=parseFloat(grid.getCell(x, 'win_loss').substring(grid.getCell(x, 'win_loss').indexOf('>')+1,grid.getCell(x, 'win_loss').indexOf('</')));
    	commission+=parseFloat(grid.getCell(x, 'commission'));
    	total+=parseFloat(grid.getCell(x, 'total'));
    	if(x==1){
    		balance+=parseFloat(grid.getCell(x, 'balance'));
    	}
     	x++;
    }
    document.getElementById('wd_lblCPNoOfBet_savan').innerHTML=x-1;
    document.getElementById('wd_lblCPBetAmount_savan').innerHTML=parseFloat(bet_amount).toFixed(2);
    document.getElementById('wd_lblCPValidBet_savan').innerHTML=parseFloat(amount_wagers).toFixed(2);
    document.getElementById('wd_lblCPWinLoss_savan').innerHTML=parseFloat(win_loss).toFixed(2);
    document.getElementById('wd_lblCPCommission_savan').innerHTML=parseFloat(commission).toFixed(2);
    document.getElementById('wd_lblCPTotal_savan').innerHTML=parseFloat(total).toFixed(2);
    document.getElementById('wd_lblCPBalance_savan').innerHTML=parseFloat(balance).toFixed(2);
    if(parseFloat(bet_amount)<0){document.getElementById('wd_lblCPBetAmount_savan').style.color='red';}else{document.getElementById('wd_lblCPBetAmount_savan').style.color='black';}
    if(parseFloat(amount_wagers)<0){document.getElementById('wd_lblCPValidBet_savan').style.color='red';}else{document.getElementById('wd_lblCPValidBet_savan').style.color='black';}
    if(parseFloat(win_loss)<0){document.getElementById('wd_lblCPWinLoss_savan').style.color='red';}else{document.getElementById('wd_lblCPWinLoss_savan').style.color='black';}
    if(parseFloat(commission)<0){document.getElementById('wd_lblCPCommission_savan').style.color='red';}else{document.getElementById('wd_lblCPCommission_savan').style.color='black';}
    if(parseFloat(total)<0){document.getElementById('wd_lblCPTotal_savan').style.color='red';}else{document.getElementById('wd_lblCPTotal_savan').style.color='black';}
    if(parseFloat(balance)<0){document.getElementById('wd_lblCPBalance_savan').style.color='red';}else{document.getElementById('wd_lblCPBalance_savan').style.color='black';}
	//All Pages
	
    jQuery.ajax({
		url: wd_urlSAVANWinLossBettingDetailsSummaryFooter +'&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			var fltTotal = data.split(';');
			document.getElementById('wd_lblAPNoOfBet_savan').innerHTML=fltTotal[0];
		    document.getElementById('wd_lblAPBetAmount_savan').innerHTML=fltTotal[1];
		    document.getElementById('wd_lblAPValidBet_savan').innerHTML=fltTotal[2];
		    document.getElementById('wd_lblAPWinLoss_savan').innerHTML=fltTotal[3];
		    document.getElementById('wd_lblAPCommission_savan').innerHTML=fltTotal[4];
		    document.getElementById('wd_lblAPTotal_savan').innerHTML=fltTotal[5];
		    document.getElementById('wd_lblAPBalance_savan').innerHTML=fltTotal[6];
		    if(parseFloat(fltTotal[1])<0){document.getElementById('wd_lblAPBetAmount_savan').style.color='red';}else{document.getElementById('wd_lblAPBetAmount_savan').style.color='black';}
		    if(parseFloat(fltTotal[2])<0){document.getElementById('wd_lblAPValidBet_savan').style.color='red';}else{document.getElementById('wd_lblAPValidBet_savan').style.color='black';}
		    if(parseFloat(fltTotal[3])<0){document.getElementById('wd_lblAPWinLoss_savan').style.color='red';}else{document.getElementById('wd_lblAPWinLoss_savan').style.color='black';}
		    if(parseFloat(fltTotal[4])<0){document.getElementById('wd_lblAPCommission_savan').style.color='red';}else{document.getElementById('wd_lblAPCommission_savan').style.color='black';}
		    if(parseFloat(fltTotal[5])<0){document.getElementById('wd_lblAPTotal_savan').style.color='red';}else{document.getElementById('wd_lblAPTotal_savan').style.color='black';}
		    if(parseFloat(fltTotal[6])<0){document.getElementById('wd_lblAPBalance_savan').style.color='red';}else{document.getElementById('wd_lblAPBalance_savan').style.color='black';}
    	}
	});
}

$(document).ready(function() { 
	var htmTag = document.createElement("div"); 
	htmTag.id = "wd_dvWinLossResultDetails"; 
	htmTag.setAttribute("align", "center"); 
	htmTag.setAttribute("onmouseout","javascript: document.getElementById('wd_dvWinLossResultDetails').addEventListener('mouseout',wd_onMouseOutWinLossResultDetails,true);");
	htmTag.style.margin = "0px auto"; 
	htmTag.className = "dynamicDiv"; 
    document.body.appendChild(htmTag);
    hideDiv('wd_dvWinLossResultDetails');
    
//    var htmTag1 = document.createElement("div"); 
//	htmTag1.id = "wd_dvResultDetails"; 
//	htmTag1.setAttribute("align", "center"); 
//	htmTag1.setAttribute("onmouseout","javascript: document.getElementById('wd_dvResultDetails').addEventListener('mouseout',wd_onMouseOutResultDetails,true);");
//	htmTag1.style.margin = "0px auto"; 
//	htmTag1.className = "dynamicDiv"; 
//    document.body.appendChild(htmTag1);
//    hideDiv('wd_dvResultDetails');
    
    var htmTag2 = document.createElement("div"); 
	htmTag2.id = "wd_dvWinLossResultCards"; 
	htmTag2.setAttribute("align", "center"); 
	htmTag2.style.margin = "0px auto"; 
	htmTag2.className = "dynamicDiv"; 
    document.body.appendChild(htmTag2);
    hideDiv('wd_dvWinLossResultCards');
}); 

function wd_onMouseOutWinLossResultDetails(event) {
    e = event.toElement || event.relatedTarget;
    if (e.parentNode == this || e == this) {
       return;
    }
    hideDiv("wd_dvWinLossResultDetails");
}

function wd_onMouseOutResultDetails(event) {
    e = event.toElement || event.relatedTarget;
    if (e.parentNode == this || e == this) {
       return;
    }
    hideDiv("wd_dvResultDetails");
}
function wd_winLossArrow(cellvalue, options, rowObject) {
	var imgsrc=" <img class='winlossarrow' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"wd_dvWinLossResultDetails\");' src='" + wd_urlWinLossArrow + "/images/winlossarrow.png'/>";
	if(parseFloat(cellvalue)<0){
		return "<font color='red'>" + cellvalue + "</font>" + imgsrc;
	}else{
		return "<font color='black'>" + cellvalue + "</font>"+ imgsrc;
	}
	
}
function wd_winLossArrow_htv(cellvalue, options, rowObject) {
	var imgsrc=" <img class='htvwinlossarrow' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"wd_dvWinLossResultDetails\");' src='" + wd_urlWinLossArrow + "/images/winlossarrow.png'/>";
	if(parseFloat(cellvalue)<0){
		return "<font color='red'>" + cellvalue + "</font>" + imgsrc;
	}else{
		return "<font color='black'>" + cellvalue + "</font>"+ imgsrc;
	}
	
}
function wd_winLossArrow_savan(cellvalue, options, rowObject) {
	var imgsrc="<img style='opacity:0.5;' class='savanwinlossarrow' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"dvWinLossResultDetails\");' src='" + urlWinLossArrow + "/images/details.png'/>";
	//var imgsrc=" <img class='savanwinlossarrow' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"dvWinLossResultDetails\");' src='" + urlWinLossArrow + "/images/winlossarrow.png'/>";
	if(parseFloat(cellvalue)<0){
		return "<font color='red'>" + cellvalue + "</font>" + imgsrc;
	}else{
		return "<font color='black'>" + cellvalue + "</font>"+ imgsrc;
	}
}
function wd_resultArrow_htv(cellvalue, options, rowObject){
	var imgsrc=" <img class='htvresultarrow' alt='"+ rowObject[1] +"' src='" + wd_urlWinLossArrow + "/images/winlossarrow.png'/>";
	/* Baccarat (3#1#1#1#^9#9#0#9#9#0#)
	 　　 1(Banker Win),2(Player Win),3(Tie Win)　 # 0(No Banker Pair),1(Banker pair) # 0(No Player Pair),1(Player Pair)　 # 1(Small),2(Big) #
	 　　 ^
	 	Player poker one # Player poker two #  Player poker three　# Banker poker one # Banker poker two #  Banker poker three
	*/
	if(rowObject[7]=='Baccarat'){
		var strData=(cellvalue.substring(0,cellvalue.indexOf('^')-1)).split('#');
		var strResult='';
		// 1(Banker Win),2(Player Win),3(Tie Win)
		if(strData[0]==1){
			strResult='<font color="red">Banker Win</font>';
		}else if(strData[0]==2){
			strResult='<font color="blue">Player Win</font>';
		}else if(strData[0]==3){
			strResult='<font color="green">Tie</font>';
		}
		// 0(No Banker Pair),1(Banker pair)
		if(strData[1]==1){
			strResult+=', <font color="red">B Pair</font>';
		}
		// 0(No Player Pair),1(Player Pair)
		if(strData[2]==1){
			strResult+=', <font color="blue">P Pair</font>';
		}
		// 1(Small),2(Big)
		if(strData[3]==1){
			strResult+=', <font color="blue">Small</font>';
		}else if(strData[3]==2){
			strResult+=', <font color="red">Big</font>';
		}
		return strResult; // + imgsrc;
	}
	/* Dragon Tiger (1#^25#36#)
		1(Dragon Win ),2(Tiger Win),3(Tie Win)　 #
		^　
		Dragon poker  # Tiger poker  #
	*/
	else if(rowObject[7]=='Dragontiger'){
		var strData=cellvalue.substring(0,cellvalue.indexOf('^')-1);
		var strResult='';
		if(strData[0]==1){
			strResult='<font color="red">Dragon Win</font>';
		}else if(strData[0]==2){
			strResult='<font color="blue">Tiger Win</font>';
		}else if(strData[0]==3){
			strResult='<font color="green">Tie</font>';
		}
		return strResult;// + imgsrc;
	}
	/* Roulette (2)
		2
	*/
	else if(rowObject[7]=='Roulette'){
		return cellvalue;
	}
}
function wd_resultArrow_savan(cellvalue, options, rowObject){
	var imgsrc="<img style='opacity:0.5;' class='savanresultarrowCards' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"wd_dvWinLossResultCards\");' src='" + urlWinLossArrow + "/images/details.png'/>";
	//var imgsrc=" <img class='savanresultarrow' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"dvResultDetails\");' src='" + urlWinLossArrow + "/images/winlossarrow.png'/>";
	/* Baccarat (3#1#1#1#^9#9#0#9#9#0#)
	 　　 1(Banker Win),2(Player Win),3(Tie Win)　 # 0(No Banker Pair),1(Banker pair) # 0(No Player Pair),1(Player Pair)　 # 1(Small),2(Big) #
	 　　 ^
	 	Player poker one # Player poker two #  Player poker three　# Banker poker one # Banker poker two #  Banker poker three
	*/
	if(rowObject[7]=='BACCARAT'){
		var strResult = cellvalue.substring(1,cellvalue.length-1);
		var strData = strResult.split(',');
		var strResult='';
		// 1(Banker Win),2(Player Win),3(Tie Win)
		if(strData[0]==1){
			strResult='<a class="savanresultarrow" id="'+ rowObject[1] +'"><font color="red">Banker Win</font>';
		}else if(strData[0]==2){
			strResult='<a class="savanresultarrow" id="'+ rowObject[1] +'"><font color="blue">Player Win</font>';
		}else if(strData[0]==3){
			strResult='<a class="savanresultarrow" id="'+ rowObject[1] +'"><font color="green">Tie</font>';
		}
		// 0(No Banker Pair),1(Banker pair)
		if(strData[1]==1){
			strResult+=', <font color="red">B Pair</font>';
		}
		// 0(No Player Pair),1(Player Pair)
		if(strData[1]==2){
			strResult+=', <font color="blue">P Pair</font>';
		}
		// 1(Small),2(Big)
		if(strData[2]==1){
			strResult+=', <font color="blue">Small</font>';
		}else if(strData[2]==2){
			strResult+=', <font color="red">Big</font>';
		}
		strResult+='<font color="black">('+strData[3]+')</font></a>';
		
		return strResult + imgsrc;
	}
	/* Dragon Tiger (1#^25#36#)
		1(Dragon Win ),2(Tiger Win),3(Tie Win)　 #
		^　
		Dragon poker  # Tiger poker  #
	*/
	else if(rowObject[7]=='DRAGON_TIGER'){
		//var strData=cellvalue.substring(0,cellvalue.indexOf('^')-1);
		var strResult = cellvalue.substring(1,cellvalue.length-1);
		var strData = strResult.split(',');
		var strResult='';
		if(strData[0]==1){
			strResult='<a class="savanresultarrow" id="'+ rowObject[1] +'"><font color="red">Dragon Win</font>';
		}else if(strData[0]==2){
			strResult='<a class="savanresultarrow" id="'+ rowObject[1] +'"><font color="blue">Tiger Win</font>';
		}else if(strData[0]==3){
			strResult='<a class="savanresultarrow" id="'+ rowObject[1] +'"><font color="green">Tie</font>';
		}
		strResult+='('+strData[1]+')</a>';
		return strResult + imgsrc;
	}
	/* Roulette (2)
		2
	*/
	else if(rowObject[7]=='ROULETTE'){
		var imgResult='';
		imgResult='<a class="savanresultarrow" id="'+ rowObject[1] +'"><img	style="width:40px;height:18px;padding:3px 3px 0px 5px;" src="'+imgURL+'roulette/r_'+cellvalue+'.png" /></a>';
		return imgResult + imgsrc;
	}
}
function wd_showWinLossResultDetails(winLossId,obj,x,y){
	jQuery.ajax({
		url: wd_urlWinLossResultDetails,
		method: 'POST',
		data: {'winlossid': winLossId},
		context: '',
		success: function(data){
			
		}
	});
	showDiv('wd_dvWinLossResultDetails',obj,x,y);
}
function wd_showWinLossResultDetails_htv(winLossId,obj,x,y){
	document.getElementById('wd_dvWinLossResultDetails').innerHTML="<b>Loading...</b>";
	showDiv('wd_dvWinLossResultDetails',obj,x,y);	
	jQuery.ajax({
		url: wd_urlHTVWinLossResultDetails + '&winlossid=' + winLossId,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			//document.getElementById('wd_dvWinLossResultDetails').innerHTML=data;
			$("#wd_winloss_bet_result_detail").html(data);
			$("#wd_winloss_bet_result_detail").dialog("open"); return false;
		}
	});
	
}
function wd_showWinLossResultDetails_savan(winLossId,obj,x,y){
	document.getElementById('wd_dvWinLossResultDetails').innerHTML="<b>Loading...</b>";
	showDiv('wd_dvWinLossResultDetails',obj,x+355,y+90);	
	jQuery.ajax({
		url: urlSAVANWinLossResultDetails + '&winlossid=' + winLossId,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			document.getElementById('wd_dvWinLossResultDetails').innerHTML=data;
			//$("#winloss_bet_result_detail").html(data);
			//$("#winloss_bet_result_detail").dialog("open"); return false;
		}
	});
	
}

function wd_showResultDetails_savan(winLossId,obj,x,y){
	//showDiv('wd_dvResultDetails',obj,x + 420,y + 240);	
	jQuery.ajax({
		url: baseURL + '/index.php?r=Agent/AgentWinLoss/SavanVegasResultDetails&winlossid=' + winLossId,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			//document.getElementById('dvResultDetails').innerHTML="<img class='resultImage' src='" + data + "'/>";
			//showDiv('dvResultDetails',obj,x,y);
			$("#wd_winloss_result_img").html("<iframe class='resultImage' style=\"width:454px; height:300px; border:none;\"   src='" + data + "'></iframe>");
			$("#wd_winloss_result_img").dialog("open"); return false;
		}
	});
}
function wd_showWinLossCards_savan(winLossId,obj,x,y){
	var urlSAVANWinLossCards =baseURL+'/index.php?r=Agent/AgentWinLoss/SavanVegasWinLossResultCards';
	document.getElementById('wd_dvWinLossResultCards').innerHTML="<b>Loading...</b>";
	showDiv('wd_dvWinLossResultCards',obj,x+410,y+90);	
	
	jQuery.ajax({
		url: urlSAVANWinLossCards + '&winlossid=' + winLossId,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			//document.getElementById('dvWinLossResultDetails').innerHTML=data;
			$("#wd_dvWinLossResultCards").html(data);
			//$("#winloss_bet_result_detail").dialog("open"); return false;
		}
	});
	
}
/**
 * @todo costavegas winloss total
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	// initialize
	document.getElementById('wd_qry_costavegas_total').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstTotal'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_total").appendChild(divTag);

	var grid=jQuery("#wd_lstTotal");
	var grid_url= wd_urlCostaVegasTotal + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
		'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&vigSharing=' + document.getElementById('wd_txtVIGsharing').value + 
		'&specificID=' + document.getElementById('wd_txtAccountID').value;
    var col_group_header = wd_getWinLossColumnHeaderByAccountId(accountID);
   
	
	grid.jqGrid({ 
		url: grid_url, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblCurrency, wd_lblValidStake, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblTotal, wd_lblWinLoss, wd_lblTips, wd_lblTotal, wd_lblCompany,'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
			  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'tips', index: 'tips', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
		      {name: 'vig_win_loss', hidden: true, index: 'vig_win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'vig_tips', hidden: true,index: 'vig_tips', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'vig_total', hidden: true,index: 'vig_total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: true,hidden: true},
		],
		loadComplete: function(){
			if(accountID==''){
    			grid.jqGrid('showCol', grid.getGridParam("colModel")[7].name);
    			grid.jqGrid('showCol', grid.getGridParam("colModel")[8].name);
    			grid.jqGrid('showCol', grid.getGridParam("colModel")[9].name);
			}
			document.getElementById('wd_txtVIGsharing1').value=document.getElementById('wd_txtVIGsharing').value;
    	},
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        rownumbers: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionCostaVegasWinLossTotal,
        footerrow:true,
	});
	$("#wd_lstTotal").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
			{startColumnName: 'vig_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">VIG</label>'},
		  ]
	});
}

/**
 * @todo costavegas winloss
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_costavegas_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_list1_costa'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pager1_costa'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_result").appendChild(divTag1);
    
	var grid=jQuery("#wd_list1_costa");
    var col_agent = wd_getAgentColumnHeaderByAccountId(accountID);
    var col_group_header = wd_getWinLossColumnHeaderByAccountId(accountID);
    
	function costa_xrate(val,name,record){
		return record['exchange_rate'];
	}
	grid.jqGrid({ 
		url: wd_urlCostaVegasSummaryWinLoss + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + 
			intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&vigSharing=' + document.getElementById('wd_txtVIGsharing').value + 
			'&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblValidStake, wd_lblAverageBet, col_agent, wd_lblWinLoss, 
		    		wd_lblCommission, wd_lblTips, wd_lblTotal, wd_lblWinLoss, wd_lblTips, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'tips', index: 'tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'vig_win_loss', hidden: true,index: 'vig_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'vig_tips',hidden: true, index: 'vig_tips', classes: 'vigtips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'vig_total',hidden: true, index: 'vig_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: costa_xrate,summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".costa_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		
		    //Lower level
		    $(".account_id").click(function() {
		    	var account_id_val=this.innerHTML;
			    if(account_id_val!='&nbsp;'){
			    	wd_costaSubAgent(account_id_val);
	    			document.getElementById('wd_lblAgentID').innerHTML = account_id_val;	    
	    		}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML = data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML = 'Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
			if(accountID==''){
    			grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
    			grid.jqGrid('showCol', grid.getGridParam("colModel")[13].name);
    			grid.jqGrid('showCol', grid.getGridParam("colModel")[14].name);
			}
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionCostaVegasWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pager1_costa',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_list1_costa").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + col_group_header + '</label>'},
			{startColumnName: 'vig_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">VIG</label>'},
		  ]
	});
	$('#wd_list1_costa').jqGrid('navGrid', '#wd_pager1_costa', {edit: false, add: false, del:false, search: false});
	$("#wd_list1_costa").jqGrid('navButtonAdd','#wd_pager1_costa',{
        caption:"Export to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_costavegas_total');
        	var table2= document.getElementById('wd_qry_costavegas_result');
			wd_exportWinLossToExcel('COSTAVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
}


/**
 * @todo costavegas member winloss
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_costavegas_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_list1_costa'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pager1_costa'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_result").appendChild(divTag1);
    
	var grid = jQuery("#wd_list1_costa");
	function costa_xrate(val,name,record){
		return record['exchange_rate'];
	}
	grid.jqGrid({ 
		url: wd_urlCostaVegasSummaryMemberWinLoss + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&vigSharing=' + document.getElementById('wd_txtVIGsharing').value + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblValidStake, wd_lblAverageBet, wd_lblWinLoss, wd_lblCommission, wd_lblTips,
		    		wd_lblTotal, wd_lblBalance, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'tips', index: 'tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'agt_tips', index: 'agt_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 100, hidden: true,summaryType: costa_xrate,summaryTpl:'<label class="costa_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".costa_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".account_id").click(function() {
		    	var account_id_val=this.innerHTML;
			    if(account_id_val!='&nbsp;'){
				    //start: hide parent grids
				    document.getElementById('wd_qry_costavegas_result').style.display='none';
				    document.getElementById('wd_qry_costavegas_result_details').style.display='none';
				    //end: hide parent grids
			    	var dtfrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":00:00";;
        			var dtto=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
        			wd_tableMemberWinLoss_costavegas_details(account_id_val,wd_dtFrom,wd_dtTo,document.getElementById('wd_cmbGame').value);
        			document.getElementById('lblBettingDetailAgentID').innerHTML=account_id_val;
        			document.getElementById('lblDDateFrom').innerHTML=dtfrom;
        			document.getElementById('lblDDateTo').innerHTML=dtto;
			    }
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});

			
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionCostaVegasMemberWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pager1_costa',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_list1_costa").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblAgent + '</label>'},
		  ]
	});
	$('#wd_list1_costa').jqGrid('navGrid', '#wd_pager1_costa', {edit: false, add: false, del:false, search: false});
	$("#wd_list1_costa").jqGrid('navButtonAdd','#wd_pager1_costa',{
        caption:"Export to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_costavegas_total');
        	var table2= document.getElementById('wd_qry_costavegas_result');
			wd_exportWinLossToExcel('COSTAVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
}

/**
 * @todo costavegas winloss details
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableCostaVegasWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_costavegas_result_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_list2_costa'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_result_details").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pager2_costa'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_result_details").appendChild(divTag1);
    
	var grid = jQuery("#wd_list2_costa");
	
	grid.jqGrid({ 
		url: wd_urlCostaVegasWinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&vigSharing=' + document.getElementById('wd_txtVIGsharing').value + 
			'&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblValidStake, wd_lblAverageBet, wd_lblWinLoss, wd_lblCommission,
				wd_lblTips, wd_lblTotal, wd_lblBalance, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblTotal,
				wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblTotal,
				wd_lblWinLoss, wd_lblTips, wd_lblTotal, wd_lblCompany],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'detail_account_id',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'tips', index: 'tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_tips', index: 'agt_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_tips', index: 'ma_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_tips', index: 'sma_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_tips', index: 'sc_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_tips', index: 'mc_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'vig_win_loss', index: 'vig_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'vig_tips', index: 'vig_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'vig_total', index: 'vig_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    
		    //Lower level
		    $(".detail_account_id").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
				    if(account_id_val.length!=10){
		    			wd_showDetails(account_id_val);
		    			document.getElementById('lblDetailAgentID').innerHTML=account_id_val;
				    }else{
					    //start: hide parent grids
					    document.getElementById('wd_qry_costavegas_result').style.display='none';
					    document.getElementById('wd_qry_costavegas_result_details').style.display='none';
					    //end: hide parent grids
				    	var dtfrom=document.getElementById('lblDetailDateFrom_costa_details').innerHTML;
	        			var dtto=document.getElementById('lblDetailDateTo_costa_details').innerHTML;
	        			wd_tableMemberWinLoss_costavegas_details(account_id_val,wd_dtFrom,wd_dtTo,document.getElementById('wd_cmbGame').value);
	        			document.getElementById('lblBettingDetailAgentID').innerHTML=account_id_val;
	        			document.getElementById('lblDDateFrom').innerHTML=dtfrom;
	        			document.getElementById('lblDDateTo').innerHTML=dtto;
					}
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
    			var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
			    
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionCostaVegasWinLossDetails,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pager2_costa',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_list2_costa").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblAgent + '</label>'},
			{startColumnName: 'ma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblMasterAgent + '</label>'},
			{startColumnName: 'sma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblSeniorMasterAgent + '</label>'},
			{startColumnName: 'sc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblSubCompany + '</label>'},
			{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblMainCompany + '</label>'},
			{startColumnName: 'vig_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">VIG</label>'},
		  ]
	});
	$('#wd_list2_costa').jqGrid('navGrid', '#wd_pager2_costa', {edit: false, add: false, del:false, search: false});
	$("#wd_list2_costa").jqGrid('navButtonAdd','#wd_pager2_costa',{
        caption:"Export Current Page to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_costavegas_result_details');
			wd_exportWinLossToExcel('COSTAVEGAS999_WIN_LOSS_DETAILS_', table1.outerHTML);
       }, 
        position:"last"
    }); 
	$("#wd_list2_costa").jqGrid('navButtonAdd','#wd_pager2_costa',{
        caption:"Export All Pages to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var url= wd_urlExportAllCostaVegasWinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&vigSharing=' + document.getElementById('wd_txtVIGsharing').value + 
			'&specificID=' + document.getElementById('wd_txtAccountID').value;
			wd_exportAllWinLossToExcel('COSTAVEGAS999_WIN_LOSS_DETAILS_',url);
       }, 
        position:"last"
    });
}


/**
 * @todo member winloss costavegas details
 * @author leokarl
 * @since 2012-12-15
 * @param strAccountID
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param intGame
 * @param testCurrency
 */
function wd_tableMemberWinLoss_costavegas_details(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency){
	document.getElementById('wd_qry_costavegas_betting_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_list3'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_betting_details").appendChild(divTag);

    var div2Tag = document.createElement("Div"); 
    div2Tag.id = 'wd_pager3'; 
    div2Tag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_costavegas_betting_details").appendChild(div2Tag);
    
    $(document).ready(function() {
		var grid=jQuery("#wd_list3");
		grid.jqGrid({ 
			url: wd_urlCostaVegasMemberWinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + 
				'&intGame=' + intGame + '&testCurrency=' + testCurrency, 
			datatype: 'json',
		    mtype: 'POST',
		    height: 'auto',
		    colNames: [wd_lblNumber, "id", wd_lblBetTime, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblGameType, wd_lblValidStake, wd_lblWinLoss, wd_lblCommission, 
					    wd_lblTips, wd_lblTotal, wd_lblBalance, wd_lblCasino],
		    colModel: [
			  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'id', index: 'id', width: 90, sortable: false,hidden: true},
			  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : wd_lblTotal},
		      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
		      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
		      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
		      {name: 'amount_wagers', index: 'amount_wagers', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
		      {name: 'amount_tips', index: 'amount_tips', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
		      //{name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'balance', index: 'balance', width: 100,sortable: false},
		      {name: 'casino_code', index: 'casino_code', width: 100,sortable: false,},
		    ],
		    loadComplete: function() {
			    var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
			    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
			    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
			    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
			    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
			    	//if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
				}
			  	//color for summary footer
		        $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
		        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winlos
		        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
		        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
		        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
		       // $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

		        $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    	wd_showCostaBettingFooter(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency);
		    },
		    loadtext:"",
            sortable: false,
		    sortname: 'bet_date',
		    sortorder: 'DESC',
		    caption: wd_cationMemberWinLossCostaVegasDetails,
		    hidegrid: false,
		    viewrecords: true,
		    pager: '#wd_pager3',
		    rowNum: 50,	
		    rowList: [20, 50, 100, 200, 500, 99999],
		    grouping: true,
		   	groupingView : {
		   		groupField : ['currency_name'],
		   		groupColumnShow : [true],
		   		groupText : ['<b>{0}</b>'],
		   		groupCollapse : false,
				groupOrder: ['asc'],
				groupSummary : [true],
				groupDataSorted : true
		   	},
		});
		$('#wd_list3').jqGrid('navGrid', '#wd_pager3', {edit: false, add: false, del:false, refresh: false, search: false});
		$("#wd_list3").jqGrid('setGroupHeaders', {
			  useColSpanStyle: true, 
			  groupHeaders:[
				{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			  ]
		});
	});
    $('#wd_list3').jqGrid('navGrid', '#wd_pager3', {edit: false, add: false, del:false, search: false});
	$("#wd_list3").jqGrid('navButtonAdd','#wd_pager3',{
        caption:"Export Current Page to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_costavegas_betting_details');
			wd_exportWinLossToExcel('COSTAVEGAS999_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
       }, 
        position:"last"
    });  
	$("#wd_list3").jqGrid('navButtonAdd','#wd_pager3',{
        caption:"Export All Pages to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var url= wd_urlExportAllCostaVegasMemberWinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + 
			'&intGame=' + intGame + '&testCurrency=' + testCurrency;
			wd_exportAllWinLossToExcel('COSTAVEGAS999_WIN_LOSS_BETTING_DETAILS_', url);
       }, 
        position:"last"
    });  
}

/**
 * @todo HTV winloss total
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_htv999_total').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstTotal_htv'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_total").appendChild(divTag);
	var grid=jQuery("#wd_lstTotal_htv");
	var col_group_header = wd_getWinLossColumnHeaderByAccountId(accountID);
    
	grid.jqGrid({ 
		url: wd_urlHTV999Total + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + 
			'&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblCurrency, wd_lblTotalStake, wd_lblValidStake, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
			  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
			  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
		      {name: 'company', index: 'company', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
		],
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        rownumbers: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_cationHTVWinLossTotal,
        footerrow:true,
	});
	$("#wd_lstTotal_htv").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + col_group_header + '</label>'},
		  ]
	});
}


/**
 * @todo HTV WinLoss
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_htv999_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstHTV1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pgrHTV1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_result").appendChild(divTag1);
    
	var grid=jQuery("#wd_lstHTV1");

    var col_agent = wd_getAgentColumnHeaderByAccountId(accountID);
    var col_group_header = wd_getWinLossColumnHeaderByAccountId(accountID);
   
    var fltBetCount=0;
    var fltValidBet=0;
   // var strCurrency1='';
    function myAvg(val, name, record)
    {
        if (strCurrency==record['currency_name']){
        	fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}else{
			fltBetCount=0;
	        fltValidBet=0;
	        fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}
    	strCurrency=record['currency_name'];
    	return parseFloat(fltValidBet/fltBetCount);
    }

    function htv_xrate(val,name,record){
		return record['exchange_rate'];
	}
	grid.jqGrid({ 
		url: wd_urlHTVSummaryWinLoss + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblBetCount, wd_lblAverageBet, wd_lblTotalStake, wd_lblValidStake,
		    		col_agent, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: htv_xrate,summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		
		    //Lower level
		    $(".account_id_htv").click(function() {
			    var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
			    	wd_htvSubAgent(account_id_val);
	    			document.getElementById('wd_lblAgentID_htv').innerHTML=account_id_val;
	    			//document.getElementById('wd_qry_costavegas_total').innerHTML='';
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
			    
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionHTVWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pgrHTV1',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_lstHTV1").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + col_group_header + '</label>'},
		  ]
	});
	$('#wd_lstHTV1').jqGrid('navGrid', '#wd_pgrHTV1', {edit: false, add: false, del:false, search: false});
	$("#wd_lstHTV1").jqGrid('navButtonAdd','#wd_pgrHTV1',{
        caption:"Export to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_htv999_total');
        	var table2= document.getElementById('wd_qry_htv999_result');
			wd_exportWinLossToExcel('HATIENVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
}

/**
 * @todo HTV member winloss
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_htv999_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstHTV1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pgrHTV1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_result").appendChild(divTag1);
    
	var grid=jQuery("#wd_lstHTV1");
	var fltBetCount=0;
    var fltValidBet=0;
    //var strCurrency='';
    function myAvg(val, name, record)
    {
        if (strCurrency==record['currency_name']){
        	fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}else{
			fltBetCount=0;
	        fltValidBet=0;
	        fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}
    	strCurrency=record['currency_name'];
    	return parseFloat(fltValidBet/fltBetCount);
    }
    function htv_xrate(val,name,record){
		return record['exchange_rate'];
	}
	
	grid.jqGrid({ 
		url: wd_urlHTVSummaryMemberWinLoss + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblBetCount, wd_lblAverageBet, wd_lblTotalStake, wd_lblValidStake,
		    		wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblBalance, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: htv_xrate,summaryTpl:'<label class="htv_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".account_id_htv").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
				    //start: hide parent grids
				    document.getElementById('wd_qry_htv999_result').style.display='none';
				    //document.getElementById('wd_qry_htv999_result_details').style.display='none';
				    //end: hide parent grids
			    	var dtfrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":00:00";;
        			var dtto=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
        			wd_tableMemberWinLoss_htv999_details(account_id_val,wd_dtFrom,wd_dtTo,document.getElementById('wd_cmbGame').value);
        			document.getElementById('lblBettingDetailAgentID_htv').innerHTML=account_id_val;
        			document.getElementById('lblDDateFrom_htv').innerHTML=dtfrom;
        			document.getElementById('lblDDateTo_htv').innerHTML=dtto;
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionHTVMemberWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pgrHTV1',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_lstHTV1").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblAgent + '</label>'},
		  ]
	});
	
	$('#wd_lstHTV1').jqGrid('navGrid', '#wd_pgrHTV1', {edit: false, add: false, del:false, search: false});
	$("#wd_lstHTV1").jqGrid('navButtonAdd','#wd_pgrHTV1',{
        caption:"Export to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_htv999_total');
        	var table2= document.getElementById('wd_qry_htv999_result');
			wd_exportWinLossToExcel('HATIENVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
}


/**
 * @todo HTV winloss details
 * @author leokarl
 * @since 2012-12-15
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableHTVWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_htv999_result_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstHTV2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_result_details").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pgrHTV2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_result_details").appendChild(divTag1);
    
	var grid=jQuery("#wd_lstHTV2");

	grid.jqGrid({ 
		url: wd_urlHTVWinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblBetCount, wd_lblAverageBet, wd_lblTotalStake,
	    		wd_lblValidStake, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblBalance, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblWinLoss, wd_lblCommission,
	    		wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTotal,
				wd_lblCompany],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'mem_bonus', index: 'mem_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'ma_bonus', index: 'ma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'sma_bonus', index: 'sma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'sc_bonus', index: 'sc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'mc_bonus', index: 'mc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    
		    //Lower level
		    $(".detail_account_id_htv").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
				    if(account_id_val.length!=10){
		    			wd_showDetails_htv(account_id_val);
		    			document.getElementById('lblDetailAgentID_htv').innerHTML=account_id_val;
				    }else{
					    //start: hide parent grids
					    document.getElementById('wd_qry_htv999_result').style.display='none';
					    //document.getElementById('wd_qry_htv999_result_details').style.display='none';
					    //end: hide parent grids
				    	var dtfrom=document.getElementById('lblDetailDateFrom_htv').innerHTML;
	        			var dtto=document.getElementById('lblDetailDateTo_htv').innerHTML;
	        			wd_tableMemberWinLoss_htv999_details(account_id_val,wd_dtFrom,wd_dtTo,document.getElementById('wd_cmbGame').value);
	        			document.getElementById('lblBettingDetailAgentID_htv').innerHTML=account_id_val;
	        			document.getElementById('lblDDateFrom_htv').innerHTML=dtfrom;
	        			document.getElementById('lblDDateTo_htv').innerHTML=dtto;
					}
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
    			var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
			    
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionHTVWinLossDetails,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pgrHTV2',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_lstHTV2").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mem_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblAgent + '</label>'},
			{startColumnName: 'ma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblMasterAgent + '</label>'},
			{startColumnName: 'sma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblSeniorMasterAgent + '</label>'},
			{startColumnName: 'sc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblSubCompany + '</label>'},
			{startColumnName: 'mc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblMainCompany + '</label>'},
		  ]
	});
	
	$('#wd_lstHTV2').jqGrid('navGrid', '#wd_pgrHTV2', {edit: false, add: false, del:false, search: false});
	$("#wd_lstHTV2").jqGrid('navButtonAdd','#wd_pgrHTV2',{
        caption:"Export to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_htv999_result_details');
			wd_exportWinLossToExcel('HATIENVEGAS999_WIN_LOSS_DETAILS_', table1.outerHTML);
       }, 
        position:"last"
    });   
}

/**
 * @todo HTV999 member winloss details
 * @author leokarl
 * @since 2012-12-15 
 * @param strAccountID
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param intGame
 * @param testCurrency
 */
function wd_tableMemberWinLoss_htv999_details(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency){
	document.getElementById('wd_qry_htv999_betting_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_wd_list5'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_betting_details").appendChild(divTag);

    var div2Tag = document.createElement("Div"); 
    div2Tag.id = 'wd_pager5'; 
    div2Tag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_htv999_betting_details").appendChild(div2Tag);
    
    $(document).ready(function() {
		var grid=jQuery("#wd_wd_list5");
		grid.jqGrid({ 
			url: wd_urlHTV999WinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + 
				'&testCurrency=' + testCurrency, 
			datatype: 'json',
		    mtype: 'POST',
		    height: 'auto',
		    colNames: [wd_lblNumber, "id", wd_lblTableShoeGame, wd_lblBetTime, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblGameType, wd_lblBetAmount,
					    wd_lblValidAmount, wd_lblResult, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblBalanceBefore, wd_lblBalanceAfter, wd_lblTotal, wd_lblCasino],
		    colModel: [
			  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'id', index: 'id', width: 90, sortable: false,hidden: true},
			  {name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
			  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : wd_lblTotal},
		      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
		      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
		      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
		      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
		      {name: 'amount_wagers', index: 'amount_wagers', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
		      {name: 'result', index: 'result', width: 150,sortable: false,formatter: wd_resultArrow_htv},
		      {name: 'win_loss', index: 'win_loss',align: 'right', width: 100,sortable: false,summaryType:'sum',formatter: wd_winLossArrow_htv,summaryTpl:'<label class="cwinlos">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
		      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
		      //{name: 'bonus', index: 'bonus', width: 90,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
		      {name: 'balance_before', index: 'balance_before', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
		      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
		    ],
		    loadComplete: function() {
			    var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
			    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
			    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
			    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
			    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
			    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
				}
			  	//color for summary footer
		        $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
		        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';} this.innerHTML=this.innerHTML.substring(0,this.innerHTML.indexOf('<i'));});//winlos
		        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
		        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
		        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
		        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

		        $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    	wd_showHTVBettingFooter(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency);

		    	$('.htvwinlossarrow').click(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		wd_showWinLossResultDetails_htv(this.alt,this,x-190,y-100);
		    	});
		    },
		    loadtext:"",
            sortable: false,
		    sortname: 'no',
		    sortorder: 'ASC',
		    pager: '#wd_pager5',
		    rowNum: 50,	
		    rowList: [20, 50, 100, 200, 500, 99999],
		    caption: wd_captionMemberWinLossHTV999details,
		    hidegrid: false,
		    viewrecords: true,
		    grouping: true,
		   	groupingView : {
		   		groupField : ['currency_name'],
		   		groupColumnShow : [true],
		   		groupText : ['<b>{0}</b>'],
		   		groupCollapse : false,
				groupOrder: ['asc'],
				groupSummary : [true],
				groupDataSorted : true
		   	},
		});
		$('#wd_wd_list5').jqGrid('navGrid', '#wd_pager5', {edit: false, add: false, del:false, search: false, refresh: false});
		$("#wd_wd_list5").jqGrid('setGroupHeaders', {
			  useColSpanStyle: true, 
			  groupHeaders:[
				{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			  ]
		});
		$('#wd_wd_list5').jqGrid('navGrid', '#wd_pager5', {edit: false, add: false, del:false, search: false});
		$("#wd_wd_list5").jqGrid('navButtonAdd','#wd_pager5',{
	        caption:"Export to Excel", 
	        buttonicon:"ui-icon-calculator", 
	        onClickButton: function(){
	        	var table1= document.getElementById('wd_qry_htv999_betting_details');
				wd_exportWinLossToExcel('HATIENVEGAS999_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
	       }, 
	        position:"last"
	    });   
		
	});
}

/**
 * @todo Savan winloss total
 * @author leokarl
 * @since 2012-12-17
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_savan_vegas_total').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstTotal_savan'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_total").appendChild(divTag);
	var grid=jQuery("#wd_lstTotal_savan");
	var col_group_header = wd_getWinLossColumnHeaderByAccountId(accountID);
        
	grid.jqGrid({ 
		url: wd_urlSAVANTotal + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + 
			'&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblCurrency, wd_lblTotalStake, wd_lblValidStake, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
			  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
			  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
		      {name: 'company', index: 'company', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
		],
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        rownumbers: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionSAVANWinLossTotal,
        footerrow:true,
	});
	$("#wd_lstTotal_savan").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + col_group_header + '</label>'},
		  ]
	});
}


/**
 * @todo savan winloss
 * @author leokarl
 * @since 2012-12-17
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_savan_vegas_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstSAVAN1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pgrSAVAN1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_result").appendChild(divTag1);
    
	var grid=jQuery("#wd_lstSAVAN1");

    var col_agent = wd_getAgentColumnHeaderByAccountId(accountID);
    var col_group_header = wd_getWinLossColumnHeaderByAccountId(accountID);
    var fltBetCount=0;
    var fltValidBet=0;
    //var strCurrency='';
    function myAvg(val, name, record)
    {
        if (strCurrency==record['currency_name']){
        	fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}else{
			fltBetCount=0;
	        fltValidBet=0;
	        fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}
    	strCurrency=record['currency_name'];
    	return parseFloat(fltValidBet/fltBetCount);
    }
    function savan_xrate(val,name,record){
		return record['exchange_rate'];
	}
	grid.jqGrid({ 
		url: wd_urlSAVANSummaryWinLoss + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblBetCount, wd_lblAverageBet, wd_lblTotalStake, wd_lblValidStake,
		    		col_agent, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      //{name: 'mc_bonus', index: 'mc_comm', width: 70,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".savan_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		
		    //Lower level
		    $(".account_id_savan").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
			    	wd_savanSubAgent(account_id_val);
	    			document.getElementById('wd_lblAgentID_savan').innerHTML=account_id_val;
	    			//document.getElementById('wd_qry_costavegas_total').innerHTML='';
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
			    
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionSAVANWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pgrSAVAN1',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_lstSAVAN1").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + col_group_header + '</label>'},
		  ]
	});
	$('#wd_lstSAVAN1').jqGrid('navGrid', '#wd_pgrSAVAN1', {edit: false, add: false, del:false, search: false});
	$("#wd_lstSAVAN1").jqGrid('navButtonAdd','#wd_pgrSAVAN1',{
        caption:"Export to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_savan_vegas_total');
        	var table2= document.getElementById('wd_qry_savan_vegas_result');
			wd_exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
}

/**
 * @todo savan member winloss
 * @author leokarl
 * @since 2012-12-17
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_savan_vegas_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstSAVAN1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pgrSAVAN1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_result").appendChild(divTag1);
    
	var grid=jQuery("#wd_lstSAVAN1");

	var fltBetCount=0;
    var fltValidBet=0;
    //var strCurrency='';
    function myAvg(val, name, record)
    {
        if (strCurrency==record['currency_name']){
        	fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}else{
			fltBetCount=0;
	        fltValidBet=0;
	        fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}
    	strCurrency=record['currency_name'];
    	return parseFloat(fltValidBet/fltBetCount);
    }
    function savan_xrate(val,name,record){
		return record['exchange_rate'];
	}
	grid.jqGrid({ 
		url: wd_urlSAVANSummaryMemberWinLoss + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblBetCount, wd_lblAverageBet, wd_lblTotalStake,
		    		wd_lblValidStake, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblBalance, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 100,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      //{name: 'bonus', index: 'bonus', width: 50,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 100, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
    		$(".savan_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".account_id_savan").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
				    //start: hide parent grids
				    document.getElementById('wd_qry_savan_vegas_result').style.display='none';
				    document.getElementById('wd_qry_savan_vegas_result_details').style.display='none';
				    //end: hide parent grids
			    	var dtfrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":00:00";;
        			var dtto=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
        			wd_tableMemberWinLoss_savan_vegas_details(account_id_val,wd_dtFrom,wd_dtTo,document.getElementById('wd_cmbGame').value);
        			document.getElementById('lblBettingDetailAgentID_savan').innerHTML=account_id_val;
        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionSAVANMemberWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pgrSAVAN1',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_lstSAVAN1").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblAgent + '</label>'},
		  ]
	});
	$('#wd_lstSAVAN1').jqGrid('navGrid', '#wd_pgrSAVAN1', {edit: false, add: false, del:false, search: false});
	$("#wd_lstSAVAN1").jqGrid('navButtonAdd','#wd_pgrSAVAN1',{
        caption:"Export to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_savan_vegas_total');
        	var table2= document.getElementById('wd_qry_savan_vegas_result');
			wd_exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
}

/**
 * @todo savan winloss details
 * @author leokarl
 * @since 2012-12-17
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function wd_tableSAVANWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('wd_qry_savan_vegas_result_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_lstSAVAN2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_result_details").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'wd_pgrSAVAN2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_result_details").appendChild(divTag1);
    
	var grid=jQuery("#wd_lstSAVAN2");

	grid.jqGrid({ 
		url: wd_urlSAVANWinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [wd_lblNumber, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblSharingCommission, wd_lblBetCount, wd_lblAverageBet, wd_lblTotalStake,
	    		wd_lblValidStake, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblBalance, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblWinLoss, 
	    		wd_lblCommission, wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblWinLoss, wd_lblCommission, wd_lblTotal, wd_lblWinLoss, wd_lblCommission,
	    		wd_lblTotal, wd_lblCompany],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 100,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'mem_bonus', index: 'mem_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'ma_bonus', index: 'ma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'sma_bonus', index: 'sma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'sc_bonus', index: 'sc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'mc_bonus', index: 'mc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    
		    //Lower level
		    $(".detail_account_id_savan").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
				    if(account_id_val.length!=10){
		    			wd_showDetails_savan(account_id_val);
		    			document.getElementById('lblDetailAgentID_savan').innerHTML=account_id_val;
				    }else{
					    //start: hide parent grids
					    document.getElementById('wd_qry_savan_vegas_result').style.display='none';
					    document.getElementById('wd_qry_savan_vegas_result_details').style.display='none';
					    //end: hide parent grids
				    	var dtfrom=document.getElementById('lblDetailDateFrom_savan').innerHTML;
	        			var dtto=document.getElementById('lblDetailDateTo_savan').innerHTML;
	        			wd_tableMemberWinLoss_savan_vegas_details(account_id_val,wd_dtFrom,wd_dtTo,document.getElementById('wd_cmbGame').value);
	        			document.getElementById('lblBettingDetailAgentID_savan').innerHTML=account_id_val;
	        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
	        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
					}
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
    			var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('wd_dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: wd_urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('wd_dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('wd_dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('wd_dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('wd_dvSharingCommission');
			});
			    
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: wd_captionSAVANWinLossDetails,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#wd_pgrSAVAN2',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#wd_lstSAVAN2").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mem_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblAgent + '</label>'},
			{startColumnName: 'ma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblMasterAgent + '</label>'},
			{startColumnName: 'sma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblSeniorMasterAgent + '</label>'},
			{startColumnName: 'sc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblSubCompany + '</label>'},
			{startColumnName: 'mc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + wd_lblMainCompany + '</label>'},
		  ]
	});
	$('#wd_lstSAVAN2').jqGrid('navGrid', '#wd_pgrSAVAN2', {edit: false, add: false, del:false, search: false});
	$("#wd_lstSAVAN2").jqGrid('navButtonAdd','#wd_pgrSAVAN2',{
        caption:"Export Current Page to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('wd_qry_savan_vegas_result_details');
			wd_exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_DETAILS_', table1.outerHTML);
       }, 
        position:"last"
    });   
	$("#wd_lstSAVAN2").jqGrid('navButtonAdd','#wd_pgrSAVAN2',{
        caption:"Export All Pages to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var url=  wd_urlExportAllSAVANWinLossDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('wd_txtAccountID').value;
			wd_exportAllWinLossToExcel('SAVANVEGAS999_WIN_LOSS_DETAILS_', url);
       }, 
        position:"last"
    });  
}

/**
 * @todo savan vegas member winloss
 * @author leokarl
 * @since 2012-12-17
 * @param strAccountID
 * @param wd_dtFrom
 * @param wd_dtTo
 * @param intGame
 * @param testCurrency
 */
function wd_tableMemberWinLoss_savan_vegas_details(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency){
	document.getElementById('wd_qry_savan_vegas_betting_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'wd_list6'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_betting_details").appendChild(divTag);

    var div2Tag = document.createElement("Div"); 
    div2Tag.id = 'wd_pager6'; 
    div2Tag.style.margin = "0px auto"; 
    document.getElementById("wd_qry_savan_vegas_betting_details").appendChild(div2Tag);
    
    $(document).ready(function() {
		var grid=jQuery("#wd_list6");
		grid.jqGrid({ 
			url: wd_urlSAVANWinLossBettingDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + 
				'&testCurrency=' + testCurrency, 
			datatype: 'json',
		    mtype: 'POST',
		    height: 'auto',
		    colNames: [wd_lblNumber, "id", wd_lblTableShoeGame, wd_lblBetTime, wd_lblAccountId, wd_lblAccountName, wd_lblCurrency, wd_lblGameType, wd_lblBetAmount, wd_lblValidBet, 
					    wd_lblResult, wd_lblWinLoss, wd_lblCommission, wd_lblTips, wd_lblBalanceBefore, wd_lblBalanceAfter, wd_lblTotal, wd_lblIpAddress],
		    colModel: [
			  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'id', index: 'id', width: 90, sortable: false,hidden: true},
			  {name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
			  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : wd_lblTotal},
		      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
		      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
		      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
		      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
		      {name: 'amount_wagers', index: 'amount_wagers', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
		      {name: 'result', index: 'result', width: 150,align:'right',title:false,sortable: false,formatter: wd_resultArrow_savan},
		      {name: 'win_loss', index: 'win_loss',title:false, width: 100,align:'right',sortable: false,summaryType:'sum',formatter: wd_winLossArrow_savan,summaryTpl:'<label class="cwinlos">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
		      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
		      //{name: 'bonus', index: 'bonus', width: 90,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
		      {name: 'balance_before', index: 'balance_before', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
		      {name: 'ip_address', index: 'ip_address', width: 230,sortable: false,},
		    ],
		    loadComplete: function() {
			    var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
			    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
			    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
			    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
			    	//if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
			    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
			    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
				}
			  	//color for summary footer
		        $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
		        //$(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
		        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';} this.innerHTML=this.innerHTML.substring(0,this.innerHTML.indexOf('<i'));});//winlos
		        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
		        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
		        //$(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
		        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
		        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

		        $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    	wd_showSAVANBettingFooter(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency);

		    	$('.savanwinlossarrow').hover(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		wd_showWinLossResultDetails_savan(this.alt,this,x-190,y-100);
		    	});
		    	$('.savanwinlossarrow').mouseout(function(e) {
		    		hideDiv("wd_dvWinLossResultDetails");
		    	});
		    	$('.savanresultarrowCards').hover(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		wd_showWinLossCards_savan(this.alt,this,x-190,y-100);
		    	});
		    	$('.savanresultarrowCards').mouseout(function(e) {
		    		hideDiv("wd_dvWinLossResultCards");
		    	});
		    	/*
		    	$('.savanresultarrow').mouseover(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		showResultDetails_savan(this.alt,this,x-420,y-240);
		    	});
		    	*/
		    	$('.savanresultarrow').click(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		wd_showResultDetails_savan(this.id,this,x-420,y-240);
		    	});
		    },
		    loadtext:"",
            sortable: false,
		    sortname: 'no',
		    sortorder: 'ASC',
		    pager: '#wd_pager6',
		    rowNum: 50,	
		    rowList: [20, 50, 100, 200, 500, 99999],
		    caption: wd_captionMemberWinLossSavanVegasDetails,
		    hidegrid: false,
		    viewrecords: true,
		    grouping: true,
		   	groupingView : {
		   		groupField : ['currency_name'],
		   		groupColumnShow : [true],
		   		groupText : ['<b>{0}</b>'],
		   		groupCollapse : false,
				groupOrder: ['asc'],
				groupSummary : [true],
				groupDataSorted : true
		   	},
		});
		$('#wd_list6').jqGrid('navGrid', '#wd_pager6', {edit: false, add: false, del:false, search: false, refresh: false});
		$("#wd_list6").jqGrid('setGroupHeaders', {
			  useColSpanStyle: true, 
			  groupHeaders:[
				{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black">' + wd_lblPlayer + '</label>'},
			  ]
		});
		$('#wd_list6').jqGrid('navGrid', '#wd_pager6', {edit: false, add: false, del:false, search: false});
		$("#wd_list6").jqGrid('navButtonAdd','#wd_pager6',{
	        caption:"Export Current Page to Excel", 
	        buttonicon:"ui-icon-calculator", 
	        onClickButton: function(){
	        	var table1= document.getElementById('wd_qry_savan_vegas_betting_details');
				wd_exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
	       }, 
	        position:"last"
	    });   
		$("#wd_list6").jqGrid('navButtonAdd','#wd_pager6',{
	        caption:"Export All Pages to Excel", 
	        buttonicon:"ui-icon-calculator", 
	        onClickButton: function(){
	        	var url= wd_urlExportAllSAVANWinLossBettingDetails + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + 
				'&testCurrency=' + testCurrency;
				wd_exportAllWinLossToExcel('SAVANVEGAS999_WIN_LOSS_BETTING_DETAILS_', url);
	       }, 
	        position:"last"
	    });
	});
}

function wd_costaSubAgent(accountID)
{
	// initialize
	var wd_dtFrom = document.getElementById('wd_dtFrom').value + " " + document.getElementById('wd_cmbTime1').value + ":00:00";
	var wd_dtTo = document.getElementById('wd_dtTo').value + " " + document.getElementById('wd_cmbTime2').value + ":59:59";
	var strCurrency = document.getElementById('wd_cmbCurrency').value;
	var intGame = document.getElementById('wd_cmbGame').value;
	var user_level = wd_sessionLevel;
	var account_id = wd_sessionAccountId;
	var testCurrency = (document.getElementById('wd_chkTestCurrency').checked == true) ? 1 : 0;
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){ // if ce user
		if(accountID==''){
			wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
		if(accountID.length!=8){
			wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}else{
			wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}else{
		if(accountID.length!=8){
			if(accountID==''){
				if(user_level!='AGT'){
					wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}
	if(accountID==''){
		document.getElementById('btnBack_costa').style.display='none';
	}else{
		document.getElementById('btnBack_costa').style.display='inline';
	}
	document.getElementById('wd_lblDateFrom_costa').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_costa').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(document.getElementById('wd_lblAgentID').innerHTML==''){
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId == account_id){
				document.getElementById('wd_lblAgentID').innerHTML=account_id;
				document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
			}
		}
	}
}
function wd_htvSubAgent(accountID)
{
	var wd_dtFrom=document.getElementById('wd_dtFrom').value + " " + document.getElementById('wd_cmbTime1').value + ":00:00";
	var wd_dtTo=document.getElementById('wd_dtTo').value + " " + document.getElementById('wd_cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var user_level=wd_sessionLevel;
	
	var account_id=wd_sessionAccountId;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		if(accountID==''){
			wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
		if(accountID.length!=8){
			wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}else{
			wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}else{
		if(accountID.length!=8){
			if(accountID==''){
				if(user_level!='AGT'){
					wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}
	if(accountID==''){
		document.getElementById('btnBack_htv').style.display='none';
	}else{
		document.getElementById('btnBack_htv').style.display='inline';
	}
	document.getElementById('wd_lblDateFrom_htv').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_htv').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(document.getElementById('wd_lblAgentID_htv').innerHTML==''){
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('wd_lblAgentID_htv').innerHTML=account_id;
				//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
			}
		}
	}
}
function wd_savanSubAgent(accountID)
{
	var wd_dtFrom=document.getElementById('wd_dtFrom').value + " " + document.getElementById('wd_cmbTime1').value + ":00:00";
	var wd_dtTo=document.getElementById('wd_dtTo').value + " " + document.getElementById('wd_cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var user_level=wd_sessionLevel;
	var account_id=wd_sessionAccountId;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		if(accountID==''){
			wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
		if(accountID.length!=8){
			wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}else{
			wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}else{
		if(accountID.length!=8){
			if(accountID==''){
				if(user_level!='AGT'){
					wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}
	if(accountID==''){
		document.getElementById('btnBack_savan').style.display='none';
	}else{
		document.getElementById('btnBack_savan').style.display='inline';
	}
	document.getElementById('wd_lblDateFrom_savan').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_savan').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(document.getElementById('wd_lblAgentID_savan').innerHTML==''){
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('wd_lblAgentID_savan').innerHTML=account_id;
				//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
			}
		}
	}
}
function wd_submitComplete(accountID)
{
	var wd_dtFrom=document.getElementById('wd_dtFrom').value + " " + document.getElementById('wd_cmbTime1').value + ":00:00";
	var wd_dtTo=document.getElementById('wd_dtTo').value + " " + document.getElementById('wd_cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var user_level=wd_sessionLevel;
	var account_id=wd_sessionAccountId;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		if(accountID==''){
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent')
				{
					wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
			if(document.getElementById('wd_chkHTV').checked==true){
				document.getElementById('dv_hatienvegas').style.display='block';
				if(wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent')
				{
					wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_hatienvegas').style.display='none';
			}
			if(document.getElementById('wd_chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if(wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent')
				{
					wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
		if(accountID.length!=8){
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
			if(document.getElementById('wd_chkHTV').checked==true){
				document.getElementById('dv_hatienvegas').style.display='block';
				if(wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent')
				{
					wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_hatienvegas').style.display='none';
			}
			if(document.getElementById('wd_chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if(wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent')
				{
					wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}else{
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
		}
	}else{
		if(accountID.length!=8){
			if(accountID==''){
				if(user_level!='AGT'){
					if(document.getElementById('wd_chkVIG').checked==true){
						document.getElementById('dv_costavegas').style.display='block';
						if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
							wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
							wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_costavegas').style.display='none';
					}
					if(document.getElementById('wd_chkHTV').checked==true){
						document.getElementById('dv_hatienvegas').style.display='block';
						if(wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
							wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
							wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_hatienvegas').style.display='none';
					}
					if(document.getElementById('wd_chkSAVAN').checked==true){
						document.getElementById('dv_savanvegas').style.display='block';
						if(wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
							wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
							wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_savanvegas').style.display='none';
					}
				}else{
					if(document.getElementById('wd_chkVIG').checked==true){
						document.getElementById('dv_costavegas').style.display='block';
						if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
							wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
							wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_costavegas').style.display='none';
					}
					if(document.getElementById('wd_chkHTV').checked==true){
						document.getElementById('dv_hatienvegas').style.display='block';
						if(wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
							wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
							wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_hatienvegas').style.display='none';
					}
					if(document.getElementById('wd_chkSAVAN').checked==true){
						document.getElementById('dv_savanvegas').style.display='block';
						if(wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
							wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
							wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_savanvegas').style.display='none';
					}
				}
			}else{
				if(document.getElementById('wd_chkVIG').checked==true){
					document.getElementById('dv_costavegas').style.display='block';
					if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
					}
				}else{
					document.getElementById('dv_costavegas').style.display='none';
				}
				if(document.getElementById('wd_chkHTV').checked==true){
					document.getElementById('dv_hatienvegas').style.display='block';
					if(wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
					}
				}else{
					document.getElementById('dv_hatienvegas').style.display='none';
				}
				if(document.getElementById('wd_chkSAVAN').checked==true){
					document.getElementById('dv_savanvegas').style.display='block';
					if(wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
					}
				}else{
					document.getElementById('dv_savanvegas').style.display='none';
				}
			}
		}else{
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
				wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
			if(document.getElementById('wd_chkHTV').checked==true){
				document.getElementById('dv_hatienvegas').style.display='block';
				if(wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
				wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_hatienvegas').style.display='none';
			}
			if(document.getElementById('wd_chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if(wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
				wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
	}
	if(accountID==''){
		document.getElementById('btnBack_costa').style.display='none';
		document.getElementById('btnBack_htv').style.display='none';
		document.getElementById('btnBack_savan').style.display='none';
	}else{
		document.getElementById('btnBack_costa').style.display='inline';
		document.getElementById('btnBack_htv').style.display='inline';
		document.getElementById('btnBack_savan').style.display='inline';
	}
	document.getElementById('wd_lblDateFrom_costa').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_costa').innerHTML=wd_dtTo;
	document.getElementById('wd_lblDateFrom_htv').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_htv').innerHTML=wd_dtTo;
	document.getElementById('wd_lblDateFrom_savan').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_savan').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(document.getElementById('wd_lblAgentID').innerHTML==''){
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('wd_lblAgentID').innerHTML=account_id;
				document.getElementById('wd_lblAgentID_htv').innerHTML=account_id;
				document.getElementById('wd_lblAgentID_savan').innerHTML=account_id;
				document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				document.getElementById('lblHTVTotalWinLossID').innerHTML=account_id;
				document.getElementById('lblSAVANTotalWinLossID').innerHTML=account_id;
			}
		}
	}
	wd_hideBettingDetails();
	wd_hideBettingDetails_htv();
	wd_hideBettingDetails_savan();
}

function wd_btnToDay()
{
	
	document.getElementById('wd_dtFrom').value=wd_dtToday;
	document.getElementById('wd_dtTo').value=wd_dtToday;
	document.getElementById('wd_cmbTime1').value='00';
	document.getElementById('wd_cmbTime2').value='23';
	
	var wd_dtFrom=document.getElementById('wd_dtFrom').value + " " + document.getElementById('wd_cmbTime1').value + ":00:00";
	var wd_dtTo=document.getElementById('wd_dtTo').value + " " + document.getElementById('wd_cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency = (document.getElementById('wd_chkTestCurrency').checked == true) ? 1 : 0;
	var user_level=wd_sessionLevel;
	var account_id=wd_sessionAccountId;
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		if(document.getElementById('wd_chkVIG').checked==true){
			document.getElementById('dv_costavegas').style.display='block';
			if (wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				//wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				wd_showDetails('');
			}
		}else{
			document.getElementById('dv_costavegas').style.display='none';
		}
		if(document.getElementById('wd_chkHTV').checked==true){
			document.getElementById('dv_hatienvegas').style.display='block';
			if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
				wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
			}
		}else{
			document.getElementById('dv_hatienvegas').style.display='none';
		}
		if(document.getElementById('wd_chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				//wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				wd_showDetails_savan('');
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
	}else{
		if(document.getElementById('wd_chkVIG').checked==true){
			document.getElementById('dv_costavegas').style.display='block';
			if (wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				wd_showDetails('');
			}
		}else{
			document.getElementById('dv_costavegas').style.display='none';
		}
		if(document.getElementById('wd_chkHTV').checked==true){
			document.getElementById('dv_hatienvegas').style.display='block';
			if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
				wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
			}
		}else{
			document.getElementById('dv_hatienvegas').style.display='none';
		}
		if(document.getElementById('wd_chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				wd_showDetails_savan('');
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
		if(user_level!='AGT'){
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if (wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails('');
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
			if(document.getElementById('wd_chkHTV').checked==true){
				document.getElementById('dv_hatienvegas').style.display='block';
				if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_hatienvegas').style.display='none';
			}
			if(document.getElementById('wd_chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails_savan('');
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}else{
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if (wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails('');
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
			if(document.getElementById('wd_chkHTV').checked==true){
				document.getElementById('dv_hatienvegas').style.display='block';
				if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_hatienvegas').style.display='none';
			}
			if(document.getElementById('wd_chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails_savan('');
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
	}
	document.getElementById('wd_lblDateFrom_costa').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_costa').innerHTML=wd_dtTo;
	document.getElementById('wd_lblDateFrom_htv').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_htv').innerHTML=wd_dtTo;
	document.getElementById('wd_lblDateFrom_savan').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_savan').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(document.getElementById('wd_lblAgentID').innerHTML==''){
		document.getElementById('btnBack_costa').style.display='none';
		document.getElementById('btnBack_htv').style.display='none';
		document.getElementById('btnBack_savan').style.display='none';
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('wd_lblAgentID').innerHTML=account_id;
				document.getElementById('wd_lblAgentID_htv').innerHTML=account_id;
				document.getElementById('wd_lblAgentID_savan').innerHTML=account_id;
				document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				document.getElementById('lblHTVTotalWinLossID').innerHTML=account_id;
				document.getElementById('lblSAVANTotalWinLossID').innerHTML=account_id;
			}
		}
	}
	wd_hideBettingDetails();
	wd_hideBettingDetails_htv();
	wd_hideBettingDetails_savan();
}

function wd_btnYesterday()
{
	
	document.getElementById('wd_dtFrom').value=wd_dtYesterday;
	document.getElementById('wd_dtTo').value=wd_dtYesterday;
	document.getElementById('wd_cmbTime1').value='00';
	document.getElementById('wd_cmbTime2').value='23';
	var wd_dtFrom=document.getElementById('wd_dtFrom').value + " " + document.getElementById('wd_cmbTime1').value + ":00:00";
	var wd_dtTo=document.getElementById('wd_dtTo').value + " " + document.getElementById('wd_cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var user_level=wd_sessionLevel;
	var account_id=wd_sessionAccountId;
	
	
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		if(document.getElementById('wd_chkVIG').checked==true){
			document.getElementById('dv_costavegas').style.display='block';
			if(wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				//wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				wd_showDetails('');
			}
		}else{
			document.getElementById('dv_costavegas').style.display='none';
		}
		if(document.getElementById('wd_chkHTV').checked==true){
			document.getElementById('dv_hatienvegas').style.display='block';
			if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
				wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
			}
		}else{
			document.getElementById('dv_hatienvegas').style.display='none';
		}
		if(document.getElementById('wd_chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				//wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,'',testCurrency);
				wd_showDetails_savan('');
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
	}else{
		if(document.getElementById('wd_chkVIG').checked==true){
			document.getElementById('dv_costavegas').style.display='block';
			if (wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableCostaVegasWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				wd_showDetails('');
			}
		}else{
			document.getElementById('dv_costavegas').style.display='none';
		}
		if(document.getElementById('wd_chkHTV').checked==true){
			document.getElementById('dv_hatienvegas').style.display='block';
			if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
				wd_tableHTVWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
			}
		}else{
			document.getElementById('dv_hatienvegas').style.display='none';
		}
		if(document.getElementById('wd_chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
				//wd_tableSAVANWinLossTotal(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				wd_showDetails_savan('');
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
		if(user_level!='AGT'){
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if (wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails('');
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
			if(document.getElementById('wd_chkHTV').checked==true){
				document.getElementById('dv_hatienvegas').style.display='block';
				if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_hatienvegas').style.display='none';
			}
			if(document.getElementById('wd_chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails_savan('');
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}else{
			if(document.getElementById('wd_chkVIG').checked==true){
				document.getElementById('dv_costavegas').style.display='block';
				if (wd_readCostavegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableCostaVegasMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails('');
				}
			}else{
				document.getElementById('dv_costavegas').style.display='none';
			}
			if(document.getElementById('wd_chkHTV').checked==true){
				document.getElementById('dv_hatienvegas').style.display='block';
				if (wd_readHatienvegasWinLoss == 1 || wd_account_type == 'agent'){
					wd_tableHTVMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_hatienvegas').style.display='none';
			}
			if(document.getElementById('wd_chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (wd_readSavanvegasWinLoss == 1 || wd_account_type == 'agent'){
					//wd_tableSAVANMemberWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
					wd_showDetails_savan('');
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
	}
	
	document.getElementById('wd_lblDateFrom_costa').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_costa').innerHTML=wd_dtTo;
	document.getElementById('wd_lblDateFrom_htv').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_htv').innerHTML=wd_dtTo;
	document.getElementById('wd_lblDateFrom_savan').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_savan').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(document.getElementById('wd_lblAgentID').innerHTML==''){
		document.getElementById('btnBack_costa').style.display='none';
		document.getElementById('btnBack_htv').style.display='none';
		document.getElementById('btnBack_savan').style.display='none';
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('wd_lblAgentID').innerHTML=account_id;
				document.getElementById('wd_lblAgentID_htv').innerHTML=account_id;
				document.getElementById('wd_lblAgentID_savan').innerHTML=account_id;
				document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				document.getElementById('lblHTVTotalWinLossID').innerHTML=account_id;
				document.getElementById('lblSAVANTotalWinLossID').innerHTML=account_id;
			}
		}
	}
	wd_hideBettingDetails();
	wd_hideBettingDetails_htv();
	wd_hideBettingDetails_savan();
}

function wd_backToParent(accountID){
	var wd_dtFrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":00:00";;
	var wd_dtTo=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
	var strCurrency= document.getElementById('wd_cmbCurrency').name;
	var intGame=document.getElementById('wd_cmbGame').name;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=wd_sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	wd_tableCostaVegasWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
	
	document.getElementById('wd_lblDateFrom_costa').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_costa').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(accountID.length!=0){
		document.getElementById('wd_lblAgentID').innerHTML=account_id;
	}
	if(document.getElementById('wd_lblAgentID').innerHTML==''){
		document.getElementById('btnBack_costa').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('btnBack_costa').style.display='none';
			}
		}
	}
}
function wd_backToParent_htv(accountID){
	var wd_dtFrom=document.getElementById('wd_lblDateFrom_htv').innerHTML;
	var wd_dtTo=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
	var strCurrency= document.getElementById('wd_cmbCurrency').name;
	var intGame=document.getElementById('wd_cmbGame').name;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=wd_sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	wd_tableHTVWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
	
	document.getElementById('wd_lblDateFrom_htv').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_htv').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(accountID.length!=0){
		document.getElementById('wd_lblAgentID_htv').innerHTML=account_id;
	}
	if(document.getElementById('wd_lblAgentID_htv').innerHTML==''){
		document.getElementById('btnBack_htv').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('btnBack_htv').style.display='none';
			}
		}
	}
}
function wd_backToParent_savan(accountID){
	var wd_dtFrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":00:00";;
	var wd_dtTo=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=wd_sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	wd_tableSAVANWinLoss(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
	
	document.getElementById('wd_lblDateFrom_savan').innerHTML=wd_dtFrom;
	document.getElementById('wd_lblDateTo_savan').innerHTML=wd_dtTo;
	document.getElementById('wd_cmbGame').name=intGame;
	document.getElementById('wd_cmbCurrency').name=strCurrency;
	if(accountID.length!=0){
		document.getElementById('wd_lblAgentID_savan').innerHTML=account_id;
	}
	if(document.getElementById('wd_lblAgentID_savan').innerHTML==''){
		document.getElementById('btnBack_savan').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('btnBack_savan').style.display='none';
			}
		}
	}
}
function wd_showDetails(account_id){
	var user_level=wd_sessionLevel;
	var wd_dtFrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":00:00";
	var wd_dtTo=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	if(account_id!=''){
		accountID=account_id;
	}else{
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
			accountID='';
		}else{
			accountID=wd_sessionAccountId;
		}
	}

	jQuery("#wd_list1_costa").trigger("reloadGrid");//reload winloss
	wd_tableCostaVegasWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
	jQuery("#wd_lstTotal").trigger("reloadGrid");//reload costa vegas total
	
	if(accountID.length!=0){
		var myGrid = $('#wd_list2_costa');
		document.getElementById('lblDetailAgentID').innerHTML=accountID;
		if(accountID.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}else if(accountID.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}else if(accountID.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}else if(accountID.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}
	}
	document.getElementById('lblDetailDateFrom_costa_details').innerHTML=wd_dtFrom;
	document.getElementById('lblDetailDateTo_costa_details').innerHTML=wd_dtTo;
	//hide back button
	if(document.getElementById('lblDetailAgentID').innerHTML==''){
		document.getElementById('btnDetailBack').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==accountID){
				document.getElementById('btnDetailBack').style.display='none';
			}
		}
	}
}
function wd_showDetails_htv(account_id){
	var user_level=wd_sessionLevel;
	var wd_dtFrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":59:59";;
	var wd_dtTo=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	if(account_id!=''){
		accountID=account_id;
	}else{
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
			accountID='';
		}else{
			accountID=wd_sessionAccountId;
		}
	}

	jQuery("#wd_lstHTV1").trigger("reloadGrid");//reload hatien winloss
	wd_tableHTVWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
	jQuery("#wd_lstTotal_htv").trigger("reloadGrid");//reload hatien total
	
	
	if(accountID.length!=0){
		var myGrid = $('#wd_lstHTV2');
		document.getElementById('lblDetailAgentID_htv').innerHTML=accountID;
		if(accountID.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}
	}
	document.getElementById('lblDetailDateFrom_htv').innerHTML=wd_dtFrom;
	document.getElementById('lblDetailDateTo_htv').innerHTML=wd_dtTo;
	//hide back button
	if(document.getElementById('lblDetailAgentID_htv').innerHTML==''){
		document.getElementById('btnDetailBack_htv').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==accountID){
				document.getElementById('btnDetailBack_htv').style.display='none';
			}
		}
	}
}
function wd_showDetails_savan(account_id){
	var user_level=wd_sessionLevel;
	var wd_dtFrom=document.getElementById('wd_dtFrom').value +" "+document.getElementById('wd_cmbTime1').value + ":00:00";;
	var wd_dtTo=document.getElementById('wd_dtTo').value +" "+document.getElementById('wd_cmbTime2').value + ":59:59";;
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	if(account_id!=''){
		accountID=account_id;
	}else{
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
			accountID='';
		}else{
			accountID=wd_sessionAccountId;
		}
	}

	jQuery("#wd_lstSAVAN1").trigger("reloadGrid");//reload savan winloss
	wd_tableSAVANWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,accountID,testCurrency);
	jQuery("#wd_lstTotal_savan").trigger("reloadGrid");//reload savan total
	
	if(accountID.length!=0){
		var myGrid = $('#wd_lstSAVAN2');
		document.getElementById('lblDetailAgentID_savan').innerHTML=accountID;
		if(accountID.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}
	}
	document.getElementById('lblDetailDateFrom_savan').innerHTML=wd_dtFrom;
	document.getElementById('lblDetailDateTo_savan').innerHTML=wd_dtTo;
	//hide back button
	if(document.getElementById('lblDetailAgentID_savan').innerHTML==''){
		document.getElementById('btnDetailBack_savan').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==accountID){
				document.getElementById('btnDetailBack_savan').style.display='none';
			}
		}
	}
}


function wd_backToParentDetail(accountID){
	var wd_dtFrom=document.getElementById('lblDetailDateFrom_costa_details').innerHTML;
	var wd_dtTo=document.getElementById('lblDetailDateTo_costa_details').innerHTML;
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=wd_sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	wd_tableCostaVegasWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
	if(account_id.length!=0){
		var myGrid = $('#wd_list2_costa');
		document.getElementById('lblDetailAgentID').innerHTML=accountID;
		if(account_id.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}else if(account_id.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}else if(account_id.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}else if(account_id.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[34].name);
		}
	}
	document.getElementById('lblDetailDateFrom_costa_details').innerHTML=wd_dtFrom;
	document.getElementById('lblDetailDateTo_costa_details').innerHTML=wd_dtTo;
	if(accountID.length!=0){
		document.getElementById('lblDetailAgentID').innerHTML=account_id;
	}
	if(document.getElementById('lblDetailAgentID').innerHTML==''){
		document.getElementById('btnDetailBack').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('btnDetailBack').style.display='none';
			}
		}
	}
}
function wd_backToParentDetail_htv(accountID){
	var wd_dtFrom=document.getElementById('lblDetailDateFrom_htv').innerHTML;
	var wd_dtTo=document.getElementById('lblDetailDateTo_htv').innerHTML;
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=wd_sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	wd_tableHTVWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
	if(account_id.length!=0){
		var myGrid = $('#wd_lstHTV2');
		document.getElementById('lblDetailAgentID_htv').innerHTML=accountID;
		if(account_id.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[15].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}
	}
	document.getElementById('lblDetailDateFrom_htv').innerHTML=wd_dtFrom;
	document.getElementById('lblDetailDateTo_htv').innerHTML=wd_dtTo;
	if(accountID.length!=0){
		document.getElementById('lblDetailAgentID_htv').innerHTML=account_id;
	}
	if(document.getElementById('lblDetailAgentID_htv').innerHTML==''){
		document.getElementById('btnDetailBack_htv').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('btnDetailBack_htv').style.display='none';
			}
		}
	}
}
function wd_backToParentDetail_savan(accountID){
	var wd_dtFrom=document.getElementById('lblDetailDateFrom_savan').innerHTML;
	var wd_dtTo=document.getElementById('lblDetailDateTo_savan').innerHTML;
	var strCurrency= document.getElementById('wd_cmbCurrency').value;
	var intGame=document.getElementById('wd_cmbGame').value;
	var testCurrency=0;
	if(document.getElementById('wd_chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=wd_sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	wd_tableSAVANWinLossDetails(wd_dtFrom,wd_dtTo,strCurrency,intGame,account_id,testCurrency);
	if(account_id.length!=0){
		var myGrid = $('#wd_lstSAVAN2');
		document.getElementById('lblDetailAgentID_savan').innerHTML=accountID;
		if(account_id.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[15].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}
	}
	document.getElementById('lblDetailDateFrom_savan').innerHTML=wd_dtFrom;
	document.getElementById('lblDetailDateTo_savan').innerHTML=wd_dtTo;
	if(accountID.length!=0){
		document.getElementById('lblDetailAgentID_savan').innerHTML=account_id;
	}
	if(document.getElementById('lblDetailAgentID_savan').innerHTML==''){
		document.getElementById('btnDetailBack_savan').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(wd_sessionAccountId==account_id){
				document.getElementById('btnDetailBack_savan').style.display='none';
			}
		}
	}
}

function wd_showCostaBettingFooter(strAccountID,wd_dtFrom,wd_dtTo,intGame,testCurrency){
	document.getElementById('wd_lblCostaBettingSummaryPlayerID').innerHTML=strAccountID;
	document.getElementById('wd_qry_costavegas_betting_details_summary_footer').style.display='block';
	//Current Page
	var grid = jQuery("#wd_list3");
	var noRow=$("#wd_list3").getGridParam("reccount");
    var x=1;
    var amount_wagers=0;
	var win_loss=0;
	var commission=0;
	var amount_tips=0;
	var total=0;
	var balance=0;
    while (x<=noRow)
    {
    	amount_wagers+=parseFloat(grid.getCell(x, 'amount_wagers'));
    	//win_loss+=parseFloat(grid.getCell(x, 'win_loss').substring(grid.getCell(x, 'win_loss').indexOf('>')+1,grid.getCell(x, 'win_loss').indexOf('</')));
    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
    	commission+=parseFloat(grid.getCell(x, 'commission'));
    	amount_tips+=parseFloat(grid.getCell(x, 'amount_tips'));
    	total+=parseFloat(grid.getCell(x, 'total'));
    	if(x==1){
    		balance+=parseFloat(grid.getCell(x, 'balance'));
    	}
     	x++;
    }
    document.getElementById('wd_lblCPNoOfBet').innerHTML=x-1;
    document.getElementById('wd_lblCPValidStake').innerHTML=parseFloat(amount_wagers).toFixed(2);
    document.getElementById('wd_lblCPWinLoss').innerHTML=parseFloat(win_loss).toFixed(2);
    document.getElementById('wd_lblCPCommission').innerHTML=parseFloat(commission).toFixed(2);
    document.getElementById('wd_lblCPTips').innerHTML=parseFloat(amount_tips).toFixed(2);
    document.getElementById('wd_lblCPTotal').innerHTML=parseFloat(total).toFixed(2);
    //document.getElementById('wd_lblCPBalance').innerHTML=parseFloat(balance).toFixed(2);
    if(parseFloat(amount_wagers)<0){document.getElementById('wd_lblCPValidStake').style.color='red';}else{document.getElementById('wd_lblCPValidStake').style.color='black';}
    if(parseFloat(win_loss)<0){document.getElementById('wd_lblCPWinLoss').style.color='red';}else{document.getElementById('wd_lblCPWinLoss').style.color='black';}
    if(parseFloat(commission)<0){document.getElementById('wd_lblCPCommission').style.color='red';}else{document.getElementById('wd_lblCPCommission').style.color='black';}
    if(parseFloat(amount_tips)<0){document.getElementById('wd_lblCPTips').style.color='red';}else{document.getElementById('wd_lblCPTips').style.color='black';}
    if(parseFloat(total)<0){document.getElementById('wd_lblCPTotal').style.color='red';}else{document.getElementById('wd_lblCPTotal').style.color='black';}
    if(parseFloat(balance)<0){document.getElementById('wd_lblCPBalance').style.color='red';}else{document.getElementById('wd_lblCPBalance').style.color='black';}
	//All Pages
    jQuery.ajax({
		url: wd_urlCostaVegasMemberWinLossDetailsSummaryFooter + '&dtFrom=' + wd_dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + wd_dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			var fltTotal = data.split(';');
			document.getElementById('wd_lblAPNoOfBet').innerHTML=fltTotal[0];
		    document.getElementById('wd_lblAPValidStake').innerHTML=fltTotal[1];
		    document.getElementById('wd_lblAPWinLoss').innerHTML=fltTotal[2];
		    document.getElementById('wd_lblAPCommission').innerHTML=fltTotal[3];
		    document.getElementById('wd_lblAPTips').innerHTML=fltTotal[4];
		    document.getElementById('wd_lblAPTotal').innerHTML=fltTotal[5];
		    document.getElementById('wd_lblAPBalance').innerHTML=fltTotal[6];
		    if(parseFloat(fltTotal[1])<0){document.getElementById('wd_lblAPValidStake').style.color='red';}else{document.getElementById('wd_lblAPValidStake').style.color='black';}
		    if(parseFloat(fltTotal[2])<0){document.getElementById('wd_lblAPWinLoss').style.color='red';}else{document.getElementById('wd_lblAPWinLoss').style.color='black';}
		    if(parseFloat(fltTotal[3])<0){document.getElementById('wd_lblAPCommission').style.color='red';}else{document.getElementById('wd_lblAPCommission').style.color='black';}
		    if(parseFloat(fltTotal[4])<0){document.getElementById('wd_lblAPTips').style.color='red';}else{document.getElementById('wd_lblAPTips').style.color='black';}
		    if(parseFloat(fltTotal[5])<0){document.getElementById('wd_lblAPTotal').style.color='red';}else{document.getElementById('wd_lblAPTotal').style.color='black';}
		    if(parseFloat(fltTotal[6])<0){document.getElementById('wd_lblAPBalance').style.color='red';}else{document.getElementById('wd_lblAPBalance').style.color='black';}
    	}
	});
}
