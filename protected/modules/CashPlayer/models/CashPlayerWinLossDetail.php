<?php

class CashPlayerWinLossDetail
{

	public function getPlayerWinLossDetail($orderField, $sortType, $startIndex, $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + IF(ISNULL(commission),0,commission))-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + IF(ISNULL(commission),0,commission))-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter  from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountPlayerWinLossDetail()
	{
		$connection = Yii::app()->db_cv999_fd_master;
	if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select COUNT(0) from tbl_costavegas_casino_history where account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' "  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(0) from tbl_costavegas_casino_history where account_type='c' and account_id='".$_GET['accountId']."' and  game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getPlayerWinLossDetailAllPages()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_POST['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select COUNT(id) as bet_count,
					format(IFNULL(sum(amount_wagers),0),2) as amount_wagers,
					format(IFNULL(sum(amount_settlements-amount_wagers),0),2) as win_loss,
					format(IFNULL(sum(amount_tips),0),2) as amount_tips,
					format(IFNULL((sum(amount_settlements-amount_wagers) + IF(ISNULL(SUM(commission)),0,SUM(commission)))-sum(amount_tips),0),2) as total_win_loss,
					format(IFNULL((SELECT casino_balance FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id=MAX(CH.id)),0),2) AS current_balance 
					from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_POST['accountId']."' and  bet_date between '".$_POST['dateFrom']."' and '".$_POST['dateTo']."'  "  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(id) as bet_count,
					format(IFNULL(sum(amount_wagers),0),2) as amount_wagers,
					format(IFNULL(sum(amount_settlements-amount_wagers),0),2) as win_loss,
					format(IFNULL(sum(amount_tips),0),2) as amount_tips,
					format(IFNULL((sum(amount_settlements-amount_wagers) + IF(ISNULL(SUM(commission)),0,SUM(commission)))-sum(amount_tips),0),2) as total_win_loss,
					format(IFNULL((SELECT casino_balance FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id=MAX(CH.id)),0),2) AS current_balance 
					from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_POST['accountId']."' and game_type='".$_POST['methodId']."' and bet_date between '".$_POST['dateFrom']."' and '".$_POST['dateTo']."' "  );
		}
		
		$rows = $command->queryRow();
		$betCount= $rows['bet_count'];
		$validBet= $rows['amount_wagers'];
		$winloss= $rows['win_loss'];
		$tips= $rows['amount_tips'];
		$total= $rows['total_win_loss'];
		$currentBalance=$rows['current_balance'];
		
		echo $betCount.'#'.$validBet.'#'.$winloss.'#'.$tips.'#'.$total.'#'.$currentBalance;
	
	}
	
	public function getExportPlayerWinLossDetail()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + IF(ISNULL(commission),0,commission))-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		else{
			$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + IF(ISNULL(commission),0,commission))-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter  from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	
}



