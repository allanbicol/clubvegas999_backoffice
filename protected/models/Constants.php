<?php
/**
 * @todo	Manage all constant variables & application configurations
 * @author Kimny MOUK
 * @since	2012-11-19
 */
class Constants
{

/***************************************
 * Manage all constant varables
 **************************************/
	
//role type for agent user
const ROLE_AGENT = 'agent';
//const EFFICIA_API_ADDRESS = 'http://192.168.10.113:8080/';
//const VIRTUALCASINO_API_ADDRESS = 'http://192.168.20.213:8082/';
const VIRTUALCASINO_API_ADDRESS = 'http://192.168.10.113:8080/';
// const SPORTBOOK_API_ADDRESS = 'http://192.168.10.213:8080/';
const SPORTBOOK_API_ADDRESS = 'http://192.168.10.113:8080/';
// const SLOTVEGAS_API_ADDRESS = 'http://192.168.20.213:8082/';
const SLOTVEGAS_API_ADDRESS = 'http://192.168.10.113:8080/';
 
// const Url_MB_all='http://192.168.10.117:8080/efficia-get-instant-pools-sv/GetInstantPools8748726384?tableID=0&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
// const Url_MB_01='http://192.168.10.117:8080/efficia-get-instant-pools-sv/GetInstantPools8748726384?tableID=1&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
// const Url_MB_02='http://192.168.10.117:8080/efficia-get-instant-pools-sv/GetInstantPools8748726384?tableID=2&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
// const Url_MB_03='http://192.168.10.117:8080/efficia-get-instant-pools-sv/GetInstantPools8748726384?tableID=3&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
// const Url_RL_01='http://192.168.10.117:8080/efficia-get-instant-pools-sv/GetInstantPools8748726384?tableID=21&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
// const Url_DT_01='http://192.168.10.117:8080/efficia-get-instant-pools-sv/GetInstantPools8748726384?tableID=41&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
//Version2 instant pool
const Url_MB_all='http://192.168.10.217:8081/efficia-get-instant-pools-sv2/GetInstantPools8748726384?line=0&tableID=0&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';

const ANNOUNCEMENT_API_ADR = 'http://192.168.10.117:8080/efficia-announcement-api-sv2/Announcement86836749575934?webuser=RoCher21@clubvegas&webpassword=@clubvegas!';

const CE_ENCRYPTION_KEY ='c702bf1c625298656816a47e4fa9d43d1c4fafd2';

//key for encryption
const KEY_LOGIN='c3D3Vt3am123';
/***************************************
 * Auto load application configuration
 **************************************/
public $VIGSetPlayerLimitAPIAddress;
public $VIGUser;
public $VIGPassword;

public $redisServerIPAddress;
public $redisPort;
public $redisPassword;

public $effeciaAPIAddress;
public $savanInstantPool;

function __construct() {
		
	$iniFile = parse_ini_file('/etc/server-environment.ini', true);
	$yiiEnvironment = trim($iniFile['server_env']);
	
	if ($yiiEnvironment === 'production')
	{
		/* SetPlayerLimit API */
		$this->VIGSetPlayerLimitAPIAddress = 'https://costavegas999.clubvegas999.com/ws/gf.php?wsdl';
		$this->VIGUser = 'livedealer';
		$this->VIGPassword = '(IJNuhb@vigws';
		
		/* set effecia api */
		$this->effeciaAPIAddress = 'http://192.168.10.113:8080/';
		$this->savanInstantPool = 'http://192.168.10.117:8080/efficia-get-instant-pools-sv2/GetInstantPools8748726384?line=0&tableID=0&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
		
		/* Redis Server Configuration */
		$this->redisServerIPAddress = '172.30.30.114';
		$this->redisPort = '6278';
		$this->redisPassword = 'r3d15@C3';
		 
	}
	else if ($yiiEnvironment === 'staging')
	{
		/* SetPlayerLimit API */
		$this->VIGSetPlayerLimitAPIAddress = 'https://costavegas999.clubvegas999.com/ws/gf.php?wsdl';
		$this->VIGUser = 'livedealer';
		$this->VIGPassword = '(IJNuhb@vigws';
		
		/* set effecia api */
		$this->effeciaAPIAddress = 'http://192.168.10.213:8081/';
		$this->savanInstantPool='http://192.168.10.217:8081/efficia-get-instant-pools-sv2/GetInstantPools8748726384?line=0&tableID=0&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
		
		/* Redis Server Configuration */
		$this->redisServerIPAddress = '192.168.10.213';
		$this->redisPort = '6278';
		$this->redisPassword = 'r3d15@C3';
	}
	else if ($yiiEnvironment === 'new_staging')
	{
		/* SetPlayerLimit API */
		$this->VIGSetPlayerLimitAPIAddress = 'https://costavegas999.clubvegas999.com/ws/gf.php?wsdl';
		$this->VIGUser = 'livedealer';
		$this->VIGPassword = '(IJNuhb@vigws';
		
		/* set effecia api */
		$this->effeciaAPIAddress = 'http://192.168.10.213:8081/';
		$this->savanInstantPool='http://192.168.10.217:8081/efficia-get-instant-pools-sv2/GetInstantPools8748726384?line=0&tableID=0&webuser=RoCher21@clubvegas&webpassword=@clubvegas!';
	
		/* Redis Server Configuration */
        $this->redisServerIPAddress = '192.168.10.213';
		$this->redisPort = '6278';
		$this->redisPassword = 'r3d15@C3';
	}
	else if ($yiiEnvironment === 'development')
	{
		/* SetPlayerLimit API */
		$this->VIGSetPlayerLimitAPIAddress = 'http://creative.livecasinodemo.com/ws/gf.php?wsdl';
		$this->VIGUser = 'livedealer';
		$this->VIGPassword = 'vigAdm1n@11';
		
		/* Redis Server Configuration */
		$this->redisServerIPAddress = '192.168.4.43';
		$this->redisPort = '6278';
		$this->redisPassword = 'r3d15@C3';
	}
	else if ($yiiEnvironment === 'localhost')
	{
		/* SetPlayerLimit API */
		$this->VIGSetPlayerLimitAPIAddress = 'http://creative.livecasinodemo.com/ws/gf.php?wsdl';
		$this->VIGUser = 'livedealer';
		$this->VIGPassword = 'vigAdm1n@11';
	
		/* Redis Server Configuration */
		$this->redisServerIPAddress = '192.168.4.43';
		$this->redisPort = '6278';
		$this->redisPassword = 'r3d15@C3';
	}

	
	
}




}
