<?php
/**
 * @todo AgentPTransferController
 * @copyright CE
 * @author Allan
 * @since 2012-06-14
 */
class AgentTransferController extends MyController
{
	public function actionAgentTransferProcess()
	{
		if(Yii::app()->user->checkAccess('agent.writeTransfer') || User::getUserType() === Constants::ROLE_AGENT)
		{
			$cpd = new AgentTransfer();
			$cpd->makeAgentTransfer();
		}
	}
}
