<?php


class CashPlayerMaxCommissionController extends MyController
{
	public function actionUpdateCommission()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerParameters'))
		{
			$postPlayerMaxCommission=new CashPlayerMaxCommission;
	
			$postPlayerMaxCommission=CashPlayerMaxCommission::model()->findByPk(1);
			$postPlayerMaxCommission->baccarat_max_commission=$_POST['txtBaccarat'];
			$postPlayerMaxCommission->roulette_max_commission=$_POST['txtRoulette'];
			$postPlayerMaxCommission->american_roulette_max_commission=$_POST['txtARoulette'];
			$postPlayerMaxCommission->dragon_tiger_max_commission=$_POST['txtDragonTiger'];
			$postPlayerMaxCommission->blackjack_max_commission=$_POST['txtBlackJack'];
			$postPlayerMaxCommission->slot_max_commission=$_POST['txtSlots'];
			$postPlayerMaxCommission->save();
			echo "Games maximum commission has been updated.";
			//header('location: ' . Yii::app()->request->baseUrl.'/index.php?r=CashPlayer/CashPlayerMaxCommission');
		}
		else
			echo "You don't have permission to update the setting!";
		
	}
	
	public function actionIndex()
	{
		
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerParameters'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		
	} 
	
}

