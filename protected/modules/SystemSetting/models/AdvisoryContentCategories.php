<?php
class AdvisoryContentCategories
{

	/**
	 * @return string
	 * @param string $agent
	 */
	public function getCategories() {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetContentCategories()");

		//$command = $connection->createCommand("SELECT " . $outputParam . ";");
		return $command->queryAll();
	}

}