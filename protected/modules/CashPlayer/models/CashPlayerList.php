<?php
/**
 * @todo Manage Cash Player List
 * @author	Kimny MOUK
 * @since 2012-12-22
 */

class CashPlayerList
{
	public function getTransactionItemBankList()
	{
		$criteria=new CDbCriteria;
		$criteria->condition='item_type=:item_type AND transaction_bank=:transaction_bank';
		$criteria->params=array(':item_type'=>2,':transaction_bank'=>1);
		$model = TableTransactionItem::model()->findAll($criteria);
		
		// generate array data
		$list = CHtml::listData($model, 'id', 'transaction_item');
		return $list;
	}	
	
	/**
	 * @todo get others list from transaction item
	 * @author leokarl
	 * @since 2012-12-24
	 */
	public function getTransactionItemOtherList()
	{
		$criteria=new CDbCriteria;
		$criteria->condition='item_type=:item_type AND transaction_bank=:transaction_bank';
		$criteria->params=array(':item_type'=>2,':transaction_bank'=>0);
		$model = TableTransactionItem::model()->findAll($criteria);
	
		// generate array data
		$list = CHtml::listData($model, 'id', 'transaction_item');
		return $list;
	}
}