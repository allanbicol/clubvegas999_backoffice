

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/statpopup.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/cashplayer.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/setbonus.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/withdraw.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/deposit.js"></script>


<script type="text/javascript">
	//active menu color
	document.getElementById('agentHeader').className="";
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_cash_player').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Cash Player List</a></li>");
	
</script>  
<script type="text/javascript">
function deposit(aElement) {
	
	var AcountID = aElement.id.split("=");
	var urlLoadFormDeposit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/LoadFormDeposit';
	$('#viewDeposit').load(urlLoadFormDeposit + '&account_id='+ AcountID[1],
			function(response, status, xhr) {		    	
  				if (status == "success") {		    		  									
	  				$("#deposit-dialog").dialog("open"); return false;
	  			}else{
  					$('#viewDeposit').load('<p style="color:red;">Something wrong during request!!!</p>');
  					$( "#deposit-dialog" ).dialog({ position: { my: "center", at: "center", of: window } });
  					$("#deposit-dialog").dialog("open"); return false;
	  			}
  			}
		);
 }

var baseUrl = '<?php echo Yii::app()->request->baseUrl;?>';
var s_session_expired = '<?php if(isset(Yii::app()->session['account_id'])){echo 0;}else{echo 1;}?>';

function onlyNumbers(evt)
{
	var e = event || evt; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;

}
function userAccountSettings(){
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CashPlayerAccountSettings/Index";
}

function echeck(str) {
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var lstr=str.length;
	var ldot=str.indexOf(dot);
	if (str.indexOf(at)==-1){
	   return false;
	}
	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
	   return false;
	}
	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
	    return false;
	}
	if (str.indexOf(at,(lat+1))!=-1){
	    return false;
	}
	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
	    return false;
	}
	if (str.indexOf(dot,(lat+2))==-1){
	    return false;
	}
	if (str.indexOf(" ")!=-1){
	    return false;
	}
		 return true;	
}

function ValidateForm(elementID){
	var emailID=document.getElementById(elementID);
	if (echeck(emailID.value)==false){
		return false;
	}
	return true;
}
var $createWarningPlayerOnlineStat='';
$(document).ready(function() {
	$createWarningPlayerOnlineStat = $('<div></div>')
		.html('<div id="dvDeposit">'
				+'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
    			+'<tr><td class="left">Warning</td><td><font color="red">The Player must logout from the game lobby first before deposit.</font></td></tr>'
    			+'<tr><td class="left">Account ID</td><td><input id="txtOnlineStatAcntID" class="depConfirmInput" name="txtOnlineStatAcntID" readonly="true"/></td></tr>'
    			
    			+'</table>'
    			+'<div id="deposit_confrm_footer"><input class="online-stat-ok" type="submit" value="  OK  "/></div>'
    			+'</div>')
		.dialog({
			autoOpen: false,
			width: 530,
			title: 'DEPOSIT',
			resizable: true,
			modal: true,
			cache: false,
			close: function () {
                	 $('#list2').trigger("reloadGrid");
            }
		});
		$('.online-stat-ok').click(function(event) {
			$createWarningPlayerOnlineStat.dialog('close');
		});
		
});

</script>


<script type="text/javascript">
var isIE = document.all?true:false;
var cRowNo=0;
if (!isIE) document.captureEvents(Event.CLICK);
document.onmousemove = getMousePosition;
function getMousePosition(e) {
  var _x;
  var _y;
  if (!isIE) {
    _x = e.pageX;
    _y = e.pageY;
  }
  if (isIE) {
    _x = event.clientX + document.body.scrollLeft;
    _y = event.clientY + document.body.scrollTop;
  }

posX=_x;
posY=_y;

  return true;
}

var divTag = document.createElement("div"); 
function createDiv() { 
    
    divTag.id = "dvStatus"; 
    divTag.setAttribute("align", "center"); 
    divTag.setAttribute("onmouseout","javascript: document.getElementById('dvStatus').addEventListener('mouseout',onMouseOut,true);")
    divTag.style.margin = "0px auto"; 
    divTag.className = "dynamicDiv"; 
    //divTag.innerHTML = ''; 
    document.body.appendChild(divTag);
} 


function hideDiv(divID) { 
	 if (document.getElementById) { 
	 document.getElementById(divID).style.visibility = 'hidden'; 
	 } 
	} 
function showDiv(divID,x,y) { 
	 var X = 0; 
	 var Y = 0; 
	 
	 if (document.getElementById) { 
	 
	   var relativeX = x;
	   var relativeY = y;
	 document.getElementById(divID).style.left= relativeX+"px";
	 document.getElementById(divID).style.top= relativeY+"px";
	 document.getElementById(divID).style.visibility = 'visible'; 
	 } 
	} 
function getPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    } 
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX + 
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY + 
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    alert(cursor);
    return cursor;
}
function onMouseOutDiv(id){
	document.getElementById('id').addEventListener('mouseout',onMouseOut,true);
}
function onMouseOut(event) {
    //this is the original element the event handler was assigned to
        e = event.toElement || event.relatedTarget;
        if (e.parentNode == this || e == this) {
           return;
        }
        hideDiv("dvStatus");
    // handle mouse event here!
}


function expandContract(id,e)
{
   if (!e) var e = window.event;

   var divObj = document.getElementById(id)

   var eventSource = (window.event) ? e.srcElement : e.target;
   if (e.type == "mouseout" && eventSource.nodeName != "DIV")
       return;

   // prevent event bubbling
   var relTarg = e.relatedTarget || e.toElement || e.fromElement;

   try
   {
       while (relTarg && relTarg != divObj)
           relTarg = relTarg.parentNode;

       if (relTarg == divObj)
           return;
//hideDiv();
       divObj.style.display = (divObj.style.display == "block") ? "none" : "block";
   }
   catch(e)
   {
   }
}

function checkIt(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        status = "This field accepts numbers only.";
        return false;
    }
    status = "";
    return true;
}

//function getAccoundID(aElement) {
//	var AcountID = aElement.id.split("=");
//   document.getElementById('txtAcountID').value=AcountID[1];
//  document.getElementById('lblCurrencyName').innerHTML=aElement.name;
// }
function getRowNo(aElement) {
	cRowNo = aElement.title;
}
 function get_cookie(cookie_name)
 {
   var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

   if (results)
     return (unescape(results[2]));
   else
     return null;
 }

</script>

<script type="text/javascript">

	var accountId="";
	var rValue='';
	
	function cashPlayerList(type,status,currency,strvalue){
		document.getElementById('qry_result').innerHTML='';
		var tableTag = document.createElement("Table"); 
	    tableTag.id = 'list2'; 
	    tableTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(tableTag);

	    var div2Tag = document.createElement("Div"); 
	    div2Tag.id = 'pager2'; 
	    div2Tag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(div2Tag);
	    
		$(document).ready(function() {
			var grid=jQuery("#list2");
			var currenctStatusValue = '';
			var colNo=1;
			grid.jqGrid({ 
		 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/PlayerList&type='+ type + '&status='+ status +'&currency='+ currency +'&strvalue='+strvalue, 
				datatype: 'json',
			    mtype: 'POST',
			    
			    colNames: ['<i class="icon-user"></i> Account ID', '<i class="icon-user"></i> Account Name','<i class="icon-info-sign"></i> Account Type','<i class="icon-money"></i> Currency','<i class="icon-tag"></i> Status','<i class="icon-dashboard"></i> Opening Date','Last Access','<i class="icon-star"></i> Bonus','<i class="icon-money"></i> Balance','<i class="icon-cogs"></i> Operations'],//,'ClubVegas Status','Casino Status'],
			    colModel: [
			      {name: 'account_id', index: 'account_id', width: 110,title:false},
		          {name: 'account_name', index: 'account_name', width: 100,title:false},
		          {name: 'account_type', index: 'account_type', width: 100,title:false},
		          {name: 'currency_name', index: 'currency_name', width: 100,title:false},
		          {name: 'status_name', index: 'status_name', width: 70,title:false}, 
		          {name: 'opening_acc_date', index: 'opening_acc_date', width: 127,title:false},
		          {name: 'last_date_play', index: 'last_date_play', width: 127,title:false,hidden:true},
		          {name: 'bonus', index: 'bonus', width: 70, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		          {name: 'balance', index: 'balance', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		          {name: 'operations', index: 'operations', width: 680,align:"center",title:false,sortable:false},
		          //{name: 'online_status', index: 'online_status', width: 120,title:false,formatter:onliveFormatter},
		          //{name: 'online_casino', index: 'online_casino', width: 110,title:false,formatter:onliveFormatter},
		         // {name: 'currency_id', index: 'currency_id', width: 0,title:false,hidden:true},
		         // {name: 'player_id', index: 'player_id', width: 0,title:false,hidden:true},
		         // {name: 'bonus', index: 'bonus', width: 0,title:false,hidden:true},
			    ],
				    ondblClickRow: function (rowid, iCol, cellcontent, e) {
				    	var myrow = grid.jqGrid('getRowData', rowid);
				    	var userlever='<?php echo Yii::app()->user->checkAccess('cashplayer.writeCashPlayerStatus');?>';
				    	if(userlever=='1'){
					    if (colNo==5){
						    if (myrow.status_name == 'Closed')
							{
						    	divTag.innerHTML = '<input type="checkbox" name="chkStat" value="Active" class="status-popup" id="stat_active"/><label for="stat_active">Active</label>';
							}
						    else if(myrow.status_name=='Active')
						    {
						    	divTag.innerHTML = '<input type="checkbox" name="chkStat" value="Closed" class="status-popup" id="stat_close"/><label for="stat_close">Closed</label>';
							}
					    	jQuery('.status-popup').click(function(event) {
					    		currenctStatusValue = jQuery(this).val();
					    		jQuery.ajax({
						    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerStatus/CashPlayerStatusProcess',
						    		method: 'POST',
						    		data: {'task': 'changeStatus', 'AccountID': myrow.account_id, 'status': jQuery(this).val()},
						    		context: jQuery(this).val(),
						    		success: function(data) {
						    			 if(currenctStatusValue == 'Active'){
						    					grid.jqGrid('setCell', rowid, colNo, currenctStatusValue);
									    		grid.jqGrid('setCell',rowid,"status_name","",{color:'green'});	
										 }else if (currenctStatusValue == 'Closed') {
										    	grid.jqGrid('setCell', rowid, colNo, currenctStatusValue);
									    		grid.jqGrid('setCell',rowid,"status_name","",{color:'#999999'});	
										 }
									     //grid.jqGrid('setCell',i,"status_name","",{'font-weight': 'bold'});	  
						    			 hideDiv("dvStatus");
							    	}
						    	});
					    	});
				    		showDiv('dvStatus',posX-5,posY-5);
					    }
				    	}
			        },
		        onCellSelect : function (rowid,iCol,cellcontent,e) {
		        	var myrow = grid.jqGrid('getRowData', rowid);
					rValue = myrow.account_id;
					colNo=iCol;
			    },
			    
			    loadtext:"",
			    loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"account_id","",{'color': '#3B240C'});
				    	grid.jqGrid('setCell',i,"account_id","",{'font-weight': 'regular'});    
				    	
				    	if(myrow.status_name == 'Active'){
				    		grid.jqGrid('setCell',i,"status_name","",{color:'green'});	
					    }else if (myrow.status_name == 'Closed') {
				    		grid.jqGrid('setCell',i,"status_name","",{color:'#999999'});	
					    }
				    	grid.jqGrid('setCell',i,"status_name","",{'font-weight': 'bold'});
				    	
					}   
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");

			    	
			    },
			    rowNum: 30,
			    rownumbers:true,
			    rowList: [30,50,100,200,500,99999],
			    pager: '#pager2',
			    sortname: 'status_name,opening_acc_date,account_id',
			    sortorder: 'ASC',
			    caption: 'Cash Player List',
			    hidegrid: false,
			    height: '100%',
			    cellEdit: true,
		        cellsubmit : 'remote',
		        cellurl: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/UpdatePlayerList&account_id='+rValue,
		        viewrecords: true,
			});

			$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del: false, search: false, refresh: false});
			jQuery("#list2").showCol("currency_id");

			function onliveFormatter(cellvalue, options, rowObject) {
		        $("cellvalue").val(cellvalue);
		        if (cellvalue=="online"){
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/online.png" />'+"  "+'<label style="color:green">'+ cellvalue+'</label>';
		        }else{
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/offline.gif" />'+"  "+'<label style="color:#A7A7A6">'+ cellvalue+'</label>';
			    }
			}; 
			function statusFormatter(cellvalue, options, rowObject) {
		        $("cellvalue").val(cellvalue);
		        if (cellvalue=="Active"){
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/active.png" /><label style="color:green">'+ cellvalue+'</label>';
		        }else{
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/inactive.png" /><label style="color:#A7A7A6">'+ cellvalue+'</label>';
			    }
			}; 
		});
	}

	
	
	function validate()
	{
		var pattern = /^-?[-\0-9]+(.[0-9]{1,2})?$/; 
		var text = document.getElementById('txtBunosAmount').value;
		level='<?php echo Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerSetBonus') ?>';
		//alert(level);
	    if (text.match(pattern)==null) 
	    {
			alert('Invalid value entered.');
			document.getElementById("submitBonus").disabled = false;
			return false;
	    }
		else
		{
			if(level!=1){
				document.getElementById("lblErrorMsgBonus").innerHTML='You are not authorized to do bonus setting!';
			}else{
				document.getElementById("submitBonus").disabled = true;
				confirmBonus();
		 	}
		}
	}
	function getAccountIDforBunos(aElement) {
			 var accountIDvar = aElement.href.split("&");
			 var accountID=accountIDvar[1].split("=");
			 document.getElementById('accountID').value=accountID[1].split(",");
				   
			    jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerGetAccount/CashPlayerGetAccountProcess',
		    		type: 'POST',
		    		datatype:'json',
		    		data: {'task':'playerBonus','account_id':document.getElementById('accountID').value},
		    		context: '',
		    		success: function(data) {
			    		var dataSplit=data.split(";");
			    		var id=dataSplit[0];
			    		
		    			 document.getElementById('accountID').value=id;
		    			 //document.getElementById('accountID1').value=id;
		    			 //document.getElementById('accountIDLogout').value=id;
		    			 //document.getElementById("currencyID").value=dataSplit[1];
		    			 //document.getElementById("playerID").value=id;
		    			 //document.getElementById("balanceAmount").value=dataSplit[2];
		    			 //document.getElementById("bonus").value=dataSplit[3];
		    			 //document.getElementById("submitBonus").disabled = false;
			    	}
		    	});
	}
	
	 
	$(function () {
		$( "#dialogBonus" ).dialog({ autoOpen: false, width:500, resizable:false, modal:true });
		$( "#dialogBonusError" ).dialog({ autoOpen: false, width:500, resizable:false, modal:true });
	});
	//DIALOG FOR BUNOS ========================================================================================
		//$(".bonus").click(function(){$(".bunos").attr("disabled","disabled");});
		function disableAnchor(obj, disable){
			//alert(obj);
			if(disable){
			var href = obj.getAttribute("href");
			if(href && href != "" && href != null){
			obj.setAttribute('href_bak', href);
			}
			obj.removeAttribute('href');
			obj.style.color="gray";
			}
			else{
			obj.setAttribute('href', obj.attributes
			['href_bak'].nodeValue);
			obj.style.color="blue";
			}
		}


	

	
	var $createForceLogoutDialog='';
	$(document).ready(function() {
		$createForceLogoutDialog = $('<div></div>')
			.html( '<div id="dialogBonusError">'
			 +'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
			 +'<tr><td class="row"><b>Warning</b></td><td class="row1"><font color="red">The player must logout from the casino lobby first before you can process this operation.</font></td></tr>'
			 +'<tr><td class="row"></td><td></td></tr>'
			 +'<tr><td class="row"><b>Account ID</b></td><td class="row1"><input id="accountIDLogout" name="accountIDLogout" class="depConfirmInput"  readonly="true"/><input id="casinoId" name="casinoId" style="display: none;" type="hidden"/></b></td></tr>'
			 +'<tr><td class="row"></td><td class="row1"><input style="width:100px;height:30px" id="btnForceLogout" type="button" value="Force Logout" onclick="javascript:logoutPlayer();" />&nbsp;&nbsp;<input style="width:100px;height:30px" id="btnCancelLogout" type="button" value="Cancel" onclick="javascript:$createForceLogoutDialog.dialog(\'close\'); $(\'.ui-dialog-titlebar-close\').show();" /></td></tr>'
			 +'</table><img id="imgLogout" style="cursor:pointer; width:470px;height:20px" src="<?php echo $this->module->assetsUrl; ?>/images/logout-loader.gif" ></div>'
				)
			.dialog({
				autoOpen: false,
				closeOnEscape: false,
				width: 500,
				title: 'Force Logout',
				resizable: false,
				modal: true,
				cache: false,
			});
	});
	var $createWaitWithdrawProcessDialog='';
	$(document).ready(function() {
		$createWaitWithdrawProcessDialog = $('<div></div>')
			.html( '<div id="dialogBonusError">'
			 +'<font color="red"><b>System is transfering balance from casino lobby to Clubvegas999 main balance.</b></font>'
			 +'</br>'
			 +'</table><img id="imgLogout1" style="cursor:pointer; width:470px;height:20px" src="<?php echo $this->module->assetsUrl; ?>/images/logout-loader.gif" ></div>'
				)
			.dialog({
				autoOpen: false,
				closeOnEscape: false,
				width: 500,
				title: 'Please wait!',
				resizable: false,
				modal: true,
				cache: false,
			});
	});
	//Logout function

	function logoutPlayer()
	{
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetBonus/Setbonus',
			type: 'POST',
			data: {'task': 'logout', 'accountID' : document.getElementById('accountIDLogout').value},
			context: '',
			success: function(data){
				document.getElementById("btnForceLogout").disabled = true;
				document.getElementById("btnCancelLogout").disabled = true;
				$('#imgLogout').show();
				startCounting();
	    	}
		});
	}
	function startCounting(){
		var check=null;
		var check =setInterval(function(){
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSetBonus/Setbonus',
			type: 'POST',
			data: {'task': 'checkIfStillProcessing', 'accountID' : document.getElementById('accountIDLogout').value,'casinoID':document.getElementById('casinoId').value},
			context: '',
			success: function(data){
				if (data==0){
					$(".ui-dialog-titlebar-close").show();
					$createForceLogoutDialog.dialog('close');
					location.reload(); 
				}
	    	}
		});
		},2000);
	}

	//==================================================================================================================
	
	
	function getAccountIDforDeduct(aElement) {
	
		var accountIDvar = aElement.href.split("&");
	
		var accountID=accountIDvar[1].split("=");
		document.getElementById('accountIDDeduct').value=accountID[1].split(",");
		
			jQuery.ajax({
		    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerGetAccount/CashPlayerGetAccountProcess',
		    		type: 'POST',
		    		data: {'task':'playerDeduction','account_id':document.getElementById('accountIDDeduct').value},
		    		context: '',
		    		success: function(data) {
			    		
			    		var dataSplit=data.split(";");
			    		var id=dataSplit[0];
		    			 document.getElementById('accountIDDeduct').value=id;
		    			 document.getElementById("currencyIDDeduct").value=dataSplit[1];
		    			 document.getElementById("playerIDDeduct").value=id;
		    			 document.getElementById("balanceAmountDeduct").value=dataSplit[2];
						
			    	}
		    });
	}
	
	
	function validateDeduction()
	{
		var pattern = /^[0-9]+(.[0-9]{1,2})?$/; 
		//var pattern = /^-?[-\0-9]+(.[0-9]{1,2})?$/; 
		var text = document.getElementById('txtDeductAmount').value;
	    if (text.match(pattern)==null) 
	    {
			alert('Invalid value entered.');
			document.getElementById("submitDeduct").disabled = false;
			return false;
	    }
		else
		{
			confirmDeduction();
		}
	}
	
	var isProcessingUnlock = false;
	function unlockPlayer(aElement){
		if(isProcessingUnlock){
		       return;
		    }
		var r=confirm("Are you sure you want to unlock this player?");
		if (r==true)
		  {
				var addUrl=aElement.toString();
				var acountID = addUrl.split("=");
				jQuery.ajax({
					url: baseUrl+'/index.php?r=CashPlayer/CashPlayerList/UnlockPlayer',
					type: 'POST',
					data: {'accountID' :acountID[2]},
					context: '',
					async: false,
					success: function(data){
						isProcessingUnlock = false;
						alert(data);
					}	
				});
		  }
		  return false;
	}
	
	function showdialogClose() { 
		$( "#dialogBonus" ).dialog('close');
		$( "#dialogBonusError" ).dialog('close');
	}
	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
	function closeDialogBonus(){
		//document.getElementById("submitBonus").disabled = true;
		alert("Bonus process complete.");
		// $('#list2').trigger("reloadGrid");
	}
	</script>
<style>
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus{
	background-color: #FFADAD !important;
}
.ui-state-highlight { background: #FFADAD !important; }
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
.ui-widget-overlay{ 
z-index: 10010!important;
} 

.ui-jqgrid-hdiv{
width:1530px!important;
}
.withdrawBG{
background-image:url("<?php echo Yii::app()->request->baseUrl;?>/images/withdraw_background.png");
height:2 00px;
font-size: 10pt;
}
.bonusBG{
background-image:url("<?php echo Yii::app()->request->baseUrl;?>/images/bonus_bg.png");
height:2 00px;
font-size: 10pt;
}
</style>
</head>
<body onload="javascript: createDiv();cashPlayerList();hideDiv('dvStatus')">
<div id="search_area">
	<div id="search_header">Search : Cash Player</div>
	<div id="search_body">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="left" style="padding-left: 5px;">STATUS</td>
				<td class="right">
				<select name="cmbStatus" id="cmbStatus">
					<option value="">All</option>
					<option value="1">Active</option>
					<option value="3">Closed</option>
				</select></td>
			</tr>
			<tr>
				<td class="left" style="padding-left: 5px;">CURRENCY</td>
				<td class="right">
					<Select name="cmbCurrency" id="cmbCurrency">
					<option value="">All</option>
					<?php 
						$dataReader = TableCurrency::model()->findAll();
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
						}
					?>
					</Select>
				</td>
			</tr>
			<tr>
				<td class="left" style="padding-left: 5px;">TYPE</td>
				<td class="right">
					<select name="cmbType" id="cmbType">
						<option value="">All</option>
						<option value="account_id">Account</option>
						<option value="balance">Balance</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="left" style="padding-left: 5px;">SEARCH</td>
				<td class="right">
					 <input type="text" id="txtAccountSearch" onkeydown="if (event.keyCode == 13){cashPlayerList(document.getElementById('cmbType').value,document.getElementById('cmbStatus').value,document.getElementById('cmbCurrency').value,document.getElementById('txtAccountSearch').value);}">
				</td>
			</tr>
			<tr><td></td><td>
				<button class="btn red" id="btnSubmitCashPlayerParams" onclick="javascript: cashPlayerList(document.getElementById('cmbType').value,document.getElementById('cmbStatus').value,document.getElementById('cmbCurrency').value,document.getElementById('txtAccountSearch').value);">Search <i class="icon-search"></i></button>
			</td></tr>
		</table>
	</div>
</div>
<div style="position: relative;left: 5px; top:5px">
<div id="qry_result"></div>

</div>
<br/>
<?php 
// Set Bonus Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		    'id'=>'set_bonus_dialog',
		    // additional javascript options for the dialog plugin
		    'options'=>array(
		        'title'=>'Set Bonus',
		        'autoOpen'=>false,
	    		'width'=>'auto',
	    		'height'=>250,
		    	'modal'=> false,
		    	'resizable'=> false,
				'draggable'=>true,
		    ),
		));
	
	echo '<input id="accountID"  type="hidden"/>';
	// Initialize dialog content
    echo '<div id="viewSetBonus"></div>';
    // End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
	
	// withdraw Dialog----------------------------------------------------------------
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'withdraw-dialog',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Withdrawal',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>250,
					'modal'=> false,
					'resizable'=> false,
					'draggable'=>true,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewWithdraw"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'deposit-dialog',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Deposit',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> false,
					'resizable'=> false,
					'draggable'=>true,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewDeposit"></div>';
	
	
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
</body>
</html>
