//active menu color
document.getElementById('systemmonitorHeader').className="start active";
document.getElementById('mnu_online_management').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>System Monitor <i class='icon-angle-right'></i></li><li><a href='#'>Online Agent Player Management</a></li>");
function tableOnlineUsers() { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);

	if(session_level=='SC' || session_level=='SMA' || session_level=='MA' || session_level=='AGT'){
		//account_id='<?php //echo yii::app()->session['account_id'];?>';
	}else{
		account_id='';
	}
	var grid=jQuery("#list1");
	grid.jqGrid({ 
		url: listURL,
		datatype: 'json',
	    mtype: 'POST',
	    colNames: ['Account ID', 'Level','CV999 Login Date','Casino','Casino Login Date','IP Address','Country','Site','Operation'],
	    colModel: [
	      {name: 'account_id', index: 'account_id',sorttype:"string", width: 100,title:false,sorttype:"string"},
          {name: 'level', index: 'level',sorttype:"string", width: 150,title:false,formatter: levelFormatter,sorttype:"string"},
          {name: 'cv999_login_date', index: 'cv999_login_date',sorttype:"string", width: 150,title:false,sorttype:"string"},
          {name: 'casino_name', index: 'casino_name',sorttype:"string", width: 100,title:false,formatter:casinoFormatter,sorttype:"string"},
          {name: 'casino_login_date', index: 'casino_login_date',sorttype:"string", width: 150,title:false,sorttype:"string"},
          {name: 'ip_address', index: 'ip_address',sorttype:"string", width: 250,title:false,sorttype:"string"}, 
          {name: 'ip_address', index: 'ip_address',sorttype:"string", width: 250,title:false,sorttype:"string"}, 
          {name: 'site', index: 'site',sorttype:"string", width: 300,title:false,sorttype:"string"}, 
          {name: 'operation', index: 'operation',sorttype:"string", width: 80,title:false,formatter:operationFormatter,sortable: false,}, 
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
    	},
	    rowNum: 100,
	    rownumbers:true,
	    rowList: [20, 50, 100,200,500,99999],
	    pager: '#pager1',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    caption: 'List',
	    hidegrid: false,
	    height: '100%',
	    loadonce: true, // to enable sorting on client side 
        sortable: true, //to enable sorting 
        viewrecords: true
	});
}
/*
 * @todo force logout player
 * @author leokarl
 * @date 2012-12-08
 */

function logout(player_id,player_level){
	jQuery.ajax({
		url: urlLogOutPlayer,
		type: 'POST',
		data: {'player_id': player_id,'player_level':player_level},
		context: '',
		success: function(msg) {
			alert(msg);
    		$createForceLogoutConfirmation.dialog('close');
    		tableOnlineUsers();//refresh list
    	}
	});
}
