<html>
<head>

	<script type="text/javascript">
	//The instanceReady event is fired, when an instance of CKEditor has finished
	//its initialization.
	var editor;
	CKEDITOR.on( 'instanceReady', function( ev ) {
		// Show this sample buttons.
		document.getElementById( 'eButtons' ).style.display = 'block';
	});
	editor.setReadOnly('True');
	</script>
</head>


<body>

	<?php 
	$language='';
	$dataReader = TableLanguage::model()->findAll();
	foreach ($dataReader as $row){
		$language=$language.",".$row['language'];
	}
	$langValue=split(",",$language);

	$enTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.forgetPassword.enSetting',null,TRUE);
	$thTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.forgetPassword.thSetting',null,TRUE);
	$vnTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.forgetPassword.vnSetting',null,TRUE);
	$cnTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.forgetPassword.cnSetting',null,TRUE);
	$hkTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.forgetPassword.hkSetting',null,TRUE);
	$this->widget('zii.widgets.jui.CJuiTabs', array(
		'tabs' => array(
				'English'=>array('content'=>$enTab, 'id'=>'tabSignupEN2'),
				'Thai'=>array('content'=>$thTab, 'id'=>'tabSignupTH2'),
				'Vietnamese'=>array('content'=>$vnTab, 'id'=>'tabSignupVN2'),
				'Traditional Chinese'=>array('content'=>$cnTab, 'id'=>'tabSignupCN2'),
				'Simplified chinese'=>array('content'=>$hkTab, 'id'=>'tabSignupHK2'),
		),
		// additional javascript options for the tabs plugin
		'options' => array(
				'collapsible' => false,
		),
		// set id for this widgets
		'id'=>'ForgetPasswordTab',
		'htmlOptions'=>array(
				'style'=>'width:1000px;'
		),
	));
?>
</body>      
</html>

