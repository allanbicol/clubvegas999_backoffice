<?php
/**
 * @todo ProcessEmailSetting Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-17
 */
class BanAgentPlayerModel extends CActiveRecord {
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function getDbConnection() {
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName() {
		return 'tbl_advisory_messages';
	}	
}

?>