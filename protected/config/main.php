<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',
        // Localization
        'sourceLanguage' => 'en_us',
        'language' => 'en', 
	// preloading 'log' component
	'preload'=>array('log','translate'),//'bootstrap'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
        'application.models.backend.*',
		'application.components.*',
		'application.modules.CashPlayer.*',
		'application.modules.Agent.*',
		'application.modules.translate.*',
		'application.modules.SecurityLogs.*',
	),

	'modules'=>array(
		'translate',
        'Agent',
		'CashPlayer',
		'Marketing',
		'Banking',
		'SystemSetting',
		'SystemMonitor',
		'GameSetting',
		'SystemTools',
		'SecurityLogs'
	),

	// application components
	'components'=>array(
		'messages'=>array(
				'class'=>'CDbMessageSource',
				'onMissingTranslation' => array('TranslateModule', 'missingTranslation'),
				
		),
		'translate'=>array(//if you name your component something else change TranslateModule
				'class'=>'translate.components.MPTranslate',
				//any avaliable options here
				'acceptedLanguages'=>array(
						'en'=>'English',
						'th'=>'ไทย',
						'vi'=>'Tiếng Việt',
						'zh_cn'=>'中文简体',
						'zh_hk'=>'中文繁体'
				),
		),
		// Enable GeoIP
		'geoip' => array(
				'class' => 'application.extensions.geoip.CGeoIP',
				// specify filename location for the corresponding database
				'filename' => dirname(__FILE__).'../../extensions/geoip/GeoIP.dat',
				// Choose MEMORY_CACHE or STANDARD mode
				'mode' => 'STANDARD',
		),
		
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// Mapping to the default controller.
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				''=>'Login/Index',
				
			),
		),

		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);