<?php
class AgentController extends MyController
{
	public function actionAgent()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new Agent;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountAgent();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgent($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("account_id","account_id","account_name","currency_name","status","opening_acc_date","total_player","balance_to_from","credit_assigned","credit","balance","online_status","login_ip_address","online_status","online_status");
		$urlAS= Yii::app()->request->baseUrl;
		$transfer='';
		$setting='';
		$transaction_history='<a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyTransHistory&account_id=\"> '.Yii::t('agent','agent.transHistory.trans').' <i class=\"icon-list\"></i></a>';
		$log='<a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyLog&account_id=\"> '.Yii::t('agent','agent.subcompanylog.logs_small').' <i class=\"icon-list\"></i></a>';
		if(Yii::app()->user->checkAccess('agent.writeTransfer') || User::getUserType()=='agent'){
			$transfer='<a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentTransfer&account_id=\" onclick=\"javascript:getAccountIDforTransfer(this);checkAgentBalance(); return false\">'.Yii::t('agent','agent.subcompanylist.transfer').' <i class=\"icon-share-alt\"></i></a>';
		}
		if(Yii::app()->user->checkAccess('agent.readSubCompanySetting') || User::getUserType()=='agent'){
			$setting='<a class=\"btn mini red\" style=\"color:white\"  href=\"index.php?r=Agent/AgentNewSubCompany&type=SC&e=1&account_id=\"> '.Yii::t('agent','agent.subcompanylist.setting').' <i class=\"icon-cogs\"></i></a>';
		}

		$htmvalue = $transfer . $transaction_history . $log .  $setting;
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,11);
	
			
	}
	
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readSubCompanyList') ==true || Yii::app()->session['level'] =='SC' ||
			Yii::app()->session['level'] =='SMA' || Yii::app()->session['level'] =='MA'){
			
			// initialize
			$model = new AgentPublicDataValidation();
				
			// data validation
			if(!isset($_GET['account_id']) || !isset($_GET['type'])){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AgentDataValidationError");
			}
			if(User::getUserType() == 'agent'){
				// check if account_id is agent's sub agent
				if(!$model->isSubAgentAccountIdValid(Yii::app()->session['account_id'], $_GET['account_id'])){
					$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AgentDataValidationError");
				}
			}
			
			if(strlen($_GET['account_id']) <> 6){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AgentDataValidationError");
			}
				
			// render index
			$this->render("index");
		}
		else
		{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
	
}