//active menu color
document.getElementById('mnu_email_setting').style.color="#5A0000";
document.getElementById('mnu_email_setting').style.fontWeight="bold";

jQuery(document).ready(function() {
    jQuery("#dtFrom").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
        //dateFormat: "dd-mm-yy"
    });

    jQuery("#dtTo").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});
function tableLog(listURL,logOperating,dtfrom,dtto) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: listURL + '&logOpt='+ logOperating +'&dtfrom='+ dtfrom + '&dtto=' + dtto, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['Operated by','Level','Operated','Level','Operation Time','Detail'],
		    colModel: [
		      {name: 'operated_by', index: 'operated_by', width: 100, sortable: false,title:false},
		      {name: 'operated_by_level', index: 'operated_by_level', width: 100, sortable: false,title:false},
		      {name: 'operated', index: 'operated', width: 100, sortable: false,title:false},
		      {name: 'operated_level', index: 'operated_level', width: 100, sortable: true,title:false},
		      {name: 'operation_time', index: 'operation_time', width: 125, sortable: true,title:false},
		      {name: 'log_details', index: 'log_details', width: 600, sortable: false,title:false},
		      
		    ],
		    loadtext:"",
		    loadComplete: function() {
		        var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	grid.jqGrid('setCell',i,"log_details","",{background:'#f7f7c8'});
			    	grid.jqGrid('setCell',i,"operated_by","",{'font-weight':'bold'});
					grid.jqGrid('setCell',i,"operated_by","",{color:'#754719'});	
				}   
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rowNum: 25,	
		    rowList: [25, 50, 75],
            rownumbers: true,
            sortable: true,
		    sortname: 'operation_time',
		    sortorder: 'DESC',
		    pager:'#pager1',
		    caption: '<b>Logs</b> <input type="text" name="" id="txtDate" readonly="true">',
		    hidegrid: false,
		    viewrecords: true
		});
		$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false});
	});
}
function submitComplete(listURL)
{
	//alert(obj.name);
	var dtfrom=(document.getElementById('dtFrom').value).replace("-","/").replace("-","/") + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtto=(document.getElementById('dtTo').value).replace("-","/").replace("-","/") + " " + document.getElementById('cmbTime2').value + ":59:59";
	tableLog(listURL,document.getElementById('cmbOperating').value,dtfrom,dtto);
	document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
	document.getElementById('btnSubmit').name=document.getElementById('cmbOperating').value;
	
}