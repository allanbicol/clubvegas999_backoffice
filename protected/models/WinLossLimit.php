<?php
/**
 * @todo winloss limit model
 * @copyright CE
 * @author Leo karl
 * @since 2013-02-14
 */
Yii::import('application.extensions.nusoap.*');
require_once('nusoap.php');

class WinLossLimit
{
	/**
	 * @todo submit wiloss limit to http://creative.livecasinodemo.com/ws/gf.php?wsdl
	 * @param string $username
	 * @param string $currency
	 * @param float $daily_max_win
	 */
	public function submitWinLossLimit($username, $currency, $daily_max_win)
	{
		// Create the client instance
		//$client = new nusoap_client(CV999Configuration::VIG_WS_URL, true);
		$vig = new Constants();
		$client = new nusoap_client($vig->VIGSetPlayerLimitAPIAddress,true);
		 
		// Check for an error
		$err = $client->getError();
		if ($err) {
			// Display the error
			//echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
			print_r($err);
			// At this point, you know the call that follows will fail
		}
		 
		$result = $client->call('SetPlayerLimits',
				array( 'request' => array(
						'ViGuser' => $vig->VIGUser,
						'ViGpassword' => $vig->VIGPassword,
						'username' => $username,
						'currency' => $currency,
						'siteID' => 'cv999_3',
						'DailyMaxLoss' => 0,
						'DailyMaxWin' => $daily_max_win,
						'WeeklyMaxLoss' => 0,
						'WeeklyMaxWin' => 0  )));
		 
		// Check for a fault
		if ($client->fault) {
			// print_r($client->fault);
			return $client->fault;
		} else {
			// Check for errors
			$err = $client->getError();
			if ($err) {
				// Display the error
				//echo '<h2>Error</h2><pre>' . $err . '</pre>';
				// print_r($err);
				return $err;
			} else {
				// Display the result
				// print_r($result);
				return $result;
			}
		}
	}
}