<?php
class AgentFundTransferHistoryController extends MyController
{
	public function actionAgentFundTransferHistory()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new AgentFundTransferHistory;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountAgentFundTransferHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentFundTransferHistory($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("counter","account_id","account_name","currency_name");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readTransHistorySummary') ||
		    Yii::app()->session['level'] =='SC' ||
			Yii::app()->session['level'] =='SMA' ||
			Yii::app()->session['level'] =='MA' ||
			Yii::app()->session['level'] =='AGT')
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionFundExcel(){
		$file="AgentFundTransferHistory".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($params[4] != 'ALL'){
			$command = $connection->createCommand("SELECT currency_name from tbl_currency  WHERE id='" . $params[4] . "'");
			$rd=$command->queryRow();
			$currency=$rd['currency_name'];
		}else{ $currency ='ALL';}
		
		$items = explode("-", $params[3]);
		if ($items[0]!='ALL'){
			$command1 = $connection->createCommand("SELECT transaction_item from tbl_transaction_item  WHERE id='" . $items[0] . "'");
			$rd1=$command1->queryRow();
			$item1=$rd1['transaction_item'];
		}else{$item1='ALL';}
		
		if ($items[1]!='ALL'){
			$command2 = $connection->createCommand("SELECT transaction_item from tbl_transaction_item  WHERE id='" . $items[1] . "'");
			$rd2=$command2->queryRow();
			$item2=$rd2['transaction_item'];
		}else{$item2='ALL';}
		
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Transaction History Summary (Fund)</label> to excel file</b>\n'.$params[0].'\n<b>Trans Type</b> = '.$params[1].'\n<b>Account Id</b> = '.$params[2].'\n<b>Trans Item</b> = '.$item1.' - '.$item2.'\n<b>Currency</b> = '.$currency.'\n<b>Test Currency</b> ='.$params[5].'\n<b>Date From</b> = '.$params[6].'\n<b>Date To</b> = '.$params[7];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	
	public function actionExportAllFundTransferHistory()
	{
		$cpHfund = new AgentFundTransferHistory;
		$result=$cpHfund->getCountAgentFundTransferHistory();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		
		$player_records = $cpHfund->getExportAgentFundTransferhistory();
		
		$fieldNames = array("account_id","account_name","currency_name");
		$headerNames = array("Account Id","Account Name","Currency");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
		
	}
	
	
}