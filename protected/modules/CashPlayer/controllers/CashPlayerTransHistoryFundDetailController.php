<?php
class CashPlayerTransHistoryFundDetailController extends MyController
{
	public function actionCashPlayerTransHistoryFundDetail()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerTransHistoryFundDetail;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerTransHistoryFundDetail();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerTransHistoryFundDetail($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("transNumber","transTypeName","currencyName","Amount","balance_before","balance_after","deposit_via","submitTime","finishTime","cashierID","status");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	
	
	public function actionCashPlayerTotalTransHistoryFundDetail()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerTransHistoryFundDetail;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountTotalPlayerTransHistoryFundDetail();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getTotalPlayerTransHistoryFundDetail();
		//$test = $records->readAll();
		$filedNames = array("currency_name","trans_number","deposit_amount","withdrawal_amount","Total");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), 1, 1, $records,$filedNames,$htmvalue,6);
	
			
	}
	public function actionGetDepositBankName()
	{
		$htmlCode='';
			$dataReader = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type and transaction_bank=:transBank',
				'params'=>array(':item_type'=>2,':transBank'=>1 ),'order'=>'transaction_item'));
			foreach ($dataReader as $row){
				$htmlCode.= $row['transaction_item'] . ':' . $row['id'] . ',';
			}
		echo $htmlCode;
	}
	public function actionGetDepositOtherName()
	{
		$htmlCode='';
		$dataReader = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type and transaction_bank=:transBank',
				'params'=>array(':item_type'=>2,':transBank'=>0 ),'order'=>'transaction_item'));
		foreach ($dataReader as $row){
			$htmlCode.= $row['transaction_item'] . ':' . $row['id'] . ',';
		}
		echo $htmlCode;
	}
	public function actionGetWithdrawBankName()
	{
		$htmlCode='';
		$dataReader = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type and transaction_bank=:transBank',
				'params'=>array(':item_type'=>4,':transBank'=>1 ),'order'=>'transaction_item'));
		foreach ($dataReader as $row){
			$htmlCode.= $row['transaction_item'] . ':' . $row['id'] . ',';
		}
		echo $htmlCode;
	}
	public function actionGetWithdrawOtherName()
	{
		$htmlCode='';
		$dataReader = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type and transaction_bank=:transBank',
				'params'=>array(':item_type'=>4,':transBank'=>0 ),'order'=>'transaction_item'));
		foreach ($dataReader as $row){
			$htmlCode.= $row['transaction_item'] . ':' . $row['id'] . ',';
		}
		echo $htmlCode;
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readTransHistoryDetails')){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
	
	
}