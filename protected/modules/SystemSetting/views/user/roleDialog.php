<script>
$(document).ready(function() {
	//$("#role_name").val($("#User_role option:selected").text());
	//alert($("#User_role option:selected").text()); 
});
	
</script>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
				 	'id'=>'roleDialog', 
				 	'options'=>array( 
				 			'title'=>'Update Role', 
				 			'autoOpen'=>true, 
				 			'modal'=>true,
				 			'width'=>'auto',
				 			'height'=>'auto',
				 	), 
		));

	echo CHtml::beginForm(); 
?>	
	 
<table border="1">
	<tr>
		<td><h3>AGENT SYSTEM</h3></td>
		<td></td>
		<td><h3>CASH PLAYER</h3></td>
		<td></td>
		<td><h3>SYSTEM SETTING</h3></td>
		<td></td>
	<tr>
	<tr>
		<td>Read</td><!-- Agent module -->
		<td>Write</td>
		<td>Read</td><!-- Cash Player module -->
		<td>Write</td>
		<td>Read</td><!-- System Setting module -->
		<td>Write</td>
	<tr>
	<tr>
		<!-- Agent module -->
		<td>
        <?php echo CHtml::checkBox('test',false); echo "Agent overview" ?>
        </td>
        <td></td>
        <!-- Cash Player module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player overview" ?>
        </td>
        <td></td>
        <!-- System Setting module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Currency Setting" ?>
        </td>
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Currency Setting" ?>
        </td>
    </tr>
    <tr>
    	<!-- Agent module -->
    	<td></td>
		<td>
        <?php echo CHtml::checkBox('test',false); echo "Create new agent" ?>
        </td>
        <!-- Cash Player module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player List" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Transaction" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Log" ?>
        </td>
		<td>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Setting" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Set Bonus" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Deposit" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Withdraw" ?>
        </td>
        <!-- System Setting module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Email Setting" ?>
        </td>
		<td>
        <?php echo CHtml::checkBox('test',false); echo "Email Setting" ?>
        </td>
    </tr>
    <tr>
    	<!-- Agent module -->
		<td>
        <?php echo CHtml::checkBox('test',false); echo "Agent List" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Agent's Transaction" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Log" ?>
        </td>
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Transfer Balance" ?><br/>
        <?php echo CHtml::checkBox('test',false); echo "Change Setting" ?>
        </td>
        <!-- Cash Player module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Information" ?>
        </td>
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Cash Player Information" ?>
        </td>
        <!-- System Setting module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "User Account" ?>
        </td>
        <td>
        <?php echo CHtml::checkBox('test',false); echo "User Account" ?>
        </td>
    </tr>
    <tr>
    	<!-- Agent module -->
    	<td>
    	<?php echo CHtml::checkBox('test',false); echo "Costa Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "HTV Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Savan Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Virtua Win/Loss" ?>
    	</td>
		<td></td>
		<!-- Cash Player module -->
		<td>
    	<?php echo CHtml::checkBox('test',false); echo "Costa Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "HTV Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Savan Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Virtua Win/Loss" ?>
    	</td>
		<td></td>
		<!-- System Setting module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Translation" ?>
        </td>
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Translation" ?>
        </td>
    </tr>
    <tr>
    	<td>
    	<?php echo CHtml::checkBox('test',false); echo "Costa Slot Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "HTV Slot Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Savan Slot Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Virtua Slot Win/Loss" ?>
    	</td>
		<td></td>
		<!-- Cash Player module -->
		<td>
    	<?php echo CHtml::checkBox('test',false); echo "Costa Slot Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "HTV Slot Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Savan Slot Win/Loss" ?><br/>
    	<?php echo CHtml::checkBox('test',false); echo "Virtua Slot Win/Loss" ?>
    	<input checked="checked" id="role_permissions_" name="role[permissions][]" type="checkbox" value="protect_wiki_pages">test
    	<input checked="checked" id="role_permissions_" name="role[permissions][]" type="checkbox" value="protect_wiki_pages">testing
    	</td>
		<td></td>
		<!-- System Setting module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "White List" ?>
        </td>
        <td>
        <?php echo CHtml::checkBox('test',false); echo "White List" ?>
        </td>
    </tr>
    <tr>
    	<!-- Agent module -->
		<td>
        <?php echo CHtml::checkBox('test',false); echo "Transaction" ?>
        </td>
        <td></td>
        <!-- Cash Player module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Consolidated Win/Loss" ?>
        </td>
        <td></td>
        <!-- System Setting module -->
        <td>
        </td>
        <td>
        </td>
    </tr>
   
    <tr>
    	<!-- Agent module -->
		<td>
        <?php echo CHtml::checkBox('test',false); echo "Agent Parameter" ?>
        </td>
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Agent Parameter" ?>
        </td>
        <!-- Cash Player module -->
        <td>
        <?php echo CHtml::checkBox('test',false); echo "Transaction Sumamary" ?>
        </td>
        <td></td>
        <!-- System Setting module -->
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
    	<td></td>
    	<td></td>
    	<td>
    		<?php echo CHtml::checkBox('test',false); echo "Cash Player Parameters" ?>
    	</td>
    	<td>
    		<?php echo CHtml::checkBox('test',false); echo "Cash Player Parameters" ?>
    	</td>
    	<!-- System Setting module -->
        <td>
        </td>
        <td>
        </td>
    </tr>
    <!-- System monitor module -->
    <tr>
		<td><h3>SYSTEM MONITOR</h3></td>
		<td></td>
		<td><h3>GAME SETTING</h3></td>
		<td></td>
		<td><h3>BANKING</h3></td>
		<td></td>
	</tr>
	<tr>
		<td>Read</td>
		<td>Write</td>
		<td>Read</td>
		<td>Write</td>
		<td>Read</td>
		<td>Write</td>
	</tr>
	<tr>
		<!-- System monitor module -->
		<td><?php echo CHtml::checkBox('Role[read_online_user]',false,array('id'=>'Role_read_online_user')); echo "Online User" ?></td>
		<td></td>
		<!-- Game setting module -->
		<td><?php echo CHtml::checkBox('Role[read_table_limit]',false,array('id'=>'Role_read_table_limit')); echo "Table Limit" ?></td>
		<td><?php echo CHtml::checkBox('Role[write_table_limit]',false,array('id'=>'Role_write_table_limit')); echo "Table Limit" ?></td>
		<!-- Banking module -->
		<td><?php echo CHtml::checkBox('test',false); echo "Payment" ?></td>
		<td><?php echo CHtml::checkBox('test',false); echo "Payment" ?></td>
	</tr>
</table> 

<center>
	<br/>
	<?php 
		echo CHtml::ajaxSubmitButton('Save',
					CHtml::normalizeUrl(array('updateRole','render'=>false)),
					array('success'=>'js: function(data) {
                        $("#roleDialog").dialog("close");
                    }'),array('id'=>'dialog-ajax'.uniqid(),'live'=>false)); 
	?>
	<?php echo CHtml::button('Select All') ?>
	<?php echo CHtml::button('Cancel') ?>
</center>

<?php echo CHtml::endForm(); ?>


<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
