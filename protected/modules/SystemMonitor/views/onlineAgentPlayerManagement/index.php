<html>
<head>
	<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/onlineagentplayermanagement.css"/>
	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/onlinemanagement.css" />
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/onlineagentplayermanagement.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/systemmonitor.js"></script>
	<script type="text/javascript">
		var urlJsAssets='<?php echo $this->module->assetsUrl; ?>';	
		var session_level='<?php echo yii::app()->session['level'];?>';
		var account_id='<?php echo yii::app()->session['account_id'];?>';
		var dtToday='<?php echo date("Y-m-d");?>';
		var listURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement/GetOnlineAgentPlayer';
		var urlLogOutPlayer='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement/ForcePlayerLogOut';
		var urlGetCasinoNameFromRedisByPlayerID='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement/GetCasinoNameFromRedisByPlayerID';
		var writeOnlineUsers = '<?php echo (Yii::app()->user->checkAccess('systemMonitor.writeOnlineUsers'))?>';
		var userType='<?php echo User::getUserType()?>';
	</script>
</head>
<body onload="javascript: tableOnlineUsers();">
	<b>Online Players Management</b>
	<div id="qry_result"></div>
</body>
</html>