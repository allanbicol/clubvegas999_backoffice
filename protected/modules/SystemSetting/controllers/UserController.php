<?php
/**
 * @todo User Account Management
 * @copyright CE
 * @author Kimny MOUK
 * @since 2012-11-09
 */
class UserController extends MyController
{
	/**
	 * @todo 	Display the user change password form
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-05-13
	 */
	public function actionUserPassword()
	{
		$this->render("userPassword");
		
	}
	/**
	 * @todo 	Change the user password
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-05-13
	 */
	public function actionChangePassword()
	{
		$oldPassword = CV999Utility::generateOneWayPassword($_POST['oldPassword']);
		$newPassword =  CV999Utility::generateOneWayPassword($_POST['newPassword']);
		$confirmPassword = CV999Utility::generateOneWayPassword($_POST['confirmPassword']);
		
		CreateUser::changePassword(Yii::app()->session['account_id'], $oldPassword, $newPassword, $confirmPassword);
		
	}
	/**
	 * @todo 	Generate User List for JqGrid
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-11-09
	 */
	public function actionUserList()
	{
		$cp = new UserAccount();
		$page = (isset($_COOKIE["page"]) && $_COOKIE["page"] != '') ? $_COOKIE["page"] : $_GET['page']; setcookie("page", "", time()-3600);
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result = $cp->getCountUserRecord();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
		
		$startIndex = $limit*$page - $limit;
		$user_records=$cp->getUserList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("account_id","account_name","account_status","level_name","opening_date_time","max_deposit","max_withdraw","account_id","account_id","account_id","account_id","account_id","account_id");
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='<a class=\'btn mini red\' href=\'index.php?r=SystemSetting/UserListLog/UserListLog&Account_ID=\'>Log <i class=\'icon-list\'></i></a><a class=\'btn mini red\' href=\'index.php?r=SystemSetting/User/update&Account_ID=\'>Setting <i class=\'icon-cogs\'></i></a>';
	
		echo JsonUtil::jsonEncode($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,7);
	}
	/**
	 * @todo 	Load a JqGrid
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-11-09
	 */
	public function actionIndex()
	{
		
		if(Yii::app()->user->checkAccess('systemSetting.readUserAccount'))
		{
			$this->render("index");
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	/**
	 * @todo 	Create User Account
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-11-09
	 */
	public function actionCreate()
	{	
		$model = new UserAccount();
		//$this->performAjaxValidation($model);
		if (isset($_POST['UserAccount']))
		{
			
			$model->attributes=$_POST['UserAccount'];
			if($model->save())
			{
				//Assign user to selected role.
				
				$auth = Yii::app()->authManager;
				$auth->assign($_POST['UserAccount']['roleType'],$_POST['UserAccount']['account_id']);
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=SystemSetting/User");
			}
		}
		
		if(Yii::app()->user->checkAccess('systemSetting.writeUserAccount'))
		{
			$this->render("create",array('model'=>$model));
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	/**
	 * @todo 	Update selected role.
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-11-09
	 */
	public function actionUpdate()
	{
		$model = UserAccount::model()->findByPk($_GET['Account_ID']); 

		if(isset($_POST['UserAccount']))
		{

			$model->attributes=$_POST['UserAccount'];
			if($model->save($_POST['UserAccount']))
			{ 
				//Remove the previouse assignment 
				$this->updateAuthAssignment($_POST['UserAccount']['account_id']);
				//Assign user to selected role.
				$auth = Yii::app()->authManager;
				$auth->assign($_POST['UserAccount']['roleType'], $_POST['UserAccount']['account_id']);
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=SystemSetting/User");
			}
		}
	
		$this->render('create',array(
				'model'=>$model,
		));
	}
	
	/**
	 * @todo	Update the user assign role
	 * @copyright	CE
	 * @author	Kimny MOUK
	 * @since	2012-11-22
	 */
	public function updateAuthAssignment($accountID)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("DELETE FROM AuthAssignment WHERE userid=:userid");
		$command->bindParam(":userid",$accountID,PDO::PARAM_STR);
		$rows = $command->execute();
	}
	/**
	 * @todo 	Display the role diaglog box.
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-11-10
	 */
	public function actionLoadRoleDialog()
	{
		// You need to pass $data or null to renderPartial as second parameter otherwise, it can't load dialog
		$this->renderPartial("role",null,false, true);

	}
	/**
	 * @todo 	Validate the model by using Ajax validation
	 * @copyright	CE
	 * @author Kimny MOUK
	 * @since	2012-11-09
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionUpdateSingleIPList() {

		//oper	edit
		if (Yii::app()->user->checkAccess('systemSetting.readUserAccount') && isset($_POST['oper']) && isset($_POST['ipAddressParam'])) {
			$userAccount = new UserAccount();
			//$userAccount
			if ($_POST['oper'] == 'del' && $_POST['ipAddressParam'] == '1') {
				echo $userAccount->deleteIPAddressByUser($_POST['myUsernameEditting'], $_POST['ipaddr']);
			} else if ($_POST['oper'] == 'edit') {
				echo $userAccount->addIPAddressByUser($_POST['myUsernameEditting'], $_POST['ipaddr']);
			}
		}
	}

	/**
	 * 
	 */
	public function actionGetSingleIPList() {

		if (Yii::app()->user->checkAccess('systemSetting.readUserAccount') && isset($_GET['ipAddressParam'])) {
			$userAccount = new UserAccount();
			$rawIPString = $userAccount->getIPAddressByUser($_GET['myUsernameEditting']);
			$ip = explode('#', $rawIPString);
			$index = 0;
			$ipStr = '';

			// validate
			if (count($ip) > 0) {
				foreach ($ip as $i) {
					$index++;
					if (strlen($ipStr) > 3) {
						$ipStr .= ',';
					}

					$ipStr .= '{"id": "' . $index .'", "cell":["' . $i . '", "' . $_GET['myUsernameEditting'] . '"]}';
				}
			}

			echo '{"total":1,"page":1,"records":' . count($ip)  . ',"rows":[' . $ipStr . ']}';
		}
	}

	/**
	 * 
	 */
	public function actionUpdateRangeIPList() {
		//oper	edit
		if (Yii::app()->user->checkAccess('systemSetting.readUserAccount') && isset($_POST['oper']) && isset($_POST['ipAddressParam'])) {
			$userAccount = new UserAccount();
			//$userAccount
			if ($_POST['oper'] == 'del' && $_POST['ipAddressParam'] == '2') {
				echo $userAccount->deleteIPRangeAddressByUser($_POST['myUsernameEditting'], $_POST['ipaddr'] . '-' . $_POST['ipaddr2']);
			} else if ($_POST['oper'] == 'edit') {
				echo $userAccount->addIPRangeAddressByUser($_POST['myUsernameEditting'], $_POST['ipaddr'] . '-' . $_POST['ipaddr2']);
			}
		}
	}
	
	/**
	 *
	 */
	public function actionGetRangeIPList() {
	
		if (Yii::app()->user->checkAccess('systemSetting.readUserAccount') && isset($_GET['ipAddressParam'])) {
			$userAccount = new UserAccount();
			$rawIPString = $userAccount->getIPRangeAddressByUser($_GET['myUsernameEditting']);

			if (strlen($rawIPString) > 3) {
			
				$ip = explode('#', $rawIPString);
				$index = 0;
				$ipStr = '';
		
				// validate
				if (count($ip) > 0) {
					foreach ($ip as $i) {
						$index++;
						if (strlen($ipStr) > 3) {
							$ipStr .= ',';
						}
						// split $i
						$rangeIP = explode('-', $i);
						$ipStr .= '{"id": "' . $index .'", "cell":["' . $rangeIP[0] . '", "' . $rangeIP[1] . '", "' . $_GET['myUsernameEditting'] . '"]}';
					}
				}
	
				echo '{"total":1,"page":1,"records":' . count($ip)  . ',"rows":[' . $ipStr . ']}';
			} else {
				echo '{"total":1,"page":1,"records":0,"rows":[]}';
			}

		}
	}
}
