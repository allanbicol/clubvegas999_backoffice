<!--@TODO: AGENT SUB COMPANY MODULE
    @AUTHOR: Allan
    @SINCE: 05082012
-->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17 27/themes/base/jquery-ui.css"/> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/statpopup.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>-->


<script type="text/javascript">

function transfer()
{
	jQuery.ajax({
		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/settlement/AgentTransferAllProcess',
		type: 'POST',
		data: '',
		context: '',
		async:false,
		success: function(data){
			//alert(data);
			document.getElementById('lblResult').innerHTML="";
			document.getElementById('lblResult').innerHTML=data;
			$('#listMaster').trigger("reloadGrid");
    	}
	});
}

var cRowNo=0;
	function tableMasterList() { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'listMaster'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultMaster").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pagerMaster'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultMaster").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#listMaster");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/Settlement/SettlementMaster', 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['<?php echo Yii::t('agent','agent.subcompanylist.accountid');?>', '<?php echo Yii::t('agent','agent.subcompanylist.accountname');?>','<?php echo Yii::t('agent','agent.subcompanylist.currency');?>','<?php echo Yii::t('agent','agent.subcompanylist.balancedue');?>','<?php echo Yii::t('agent','agent.subcompanylist.assignedcredit');?>','<?php echo Yii::t('agent','agent.subcompanylist.availablecredit');?>','<?php echo Yii::t('agent','agent.subcompanylist.balance');?>'],
			    colModel: [
					{name: 'account_id', index: 'account_id',align:"center", width: 85, search:true,title:false},
					{name: 'account_name', index: 'account_name', width: 110,title:false},
					{name: 'currency_name', index: 'currency_name',align:"center", width: 60,title:false},
					{name: 'balance_to_from', index: 'balance_to_from', width: 80, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'credit_assigned', index: 'credit_assigned', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'credit', index: 'credit', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'balance', index: 'balance', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			    ],
			    rownumbers:true,
			    loadtext:"",
			    rowNum: 100,	
			    rowList: [30, 50, 100,200,500,99999],
			    pager: '#pagerMaster',
			    sortname: 'account_id',
			    sortorder: 'ASC',
			    cellEdit: true,
			    hidegrid: false,
		        cellsubmit : 'remote',
			    caption: ' <label style="color:red"><?php echo strtoupper(Yii::app()->session['account_id']);?></label> <label id="lblTitle"><?php echo Yii::t('agent','agent.subcompanylist.master');?></label> &nbsp;&nbsp;&nbsp;&nbsp;<input  class="btn red" onclick="javascript: transfer();" type="button" value="TRANSFER">',
			    viewrecords: true,
			   
			});
			$('#listMaster').jqGrid('navGrid', '#pagerMaster', {edit: false, add: false, del:false, search: false,refresh:false});	
		});
	} 
</script>

<script type="text/javascript">

	function LoadMaster()
	{
	
		tableMasterList();
		
	}

</script>
	
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
.ui-jqgrid tr.jqgrow td {
        word-wrap: break-word; /* IE 5.5+ and CSS3 */
        white-space: pre-wrap; /* CSS3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        overflow: hidden;
        height: auto;
        vertical-align: middle;
        padding-top: 3px;
        padding-bottom: 3px
    }
.ui-jqgrid-sortable { 
	height: 33px!important; 
	white-space: normal!important; 
	vertical-align: text-middle;
}
</style>

</head>
<body onload="javascript: Load();">
<div id="lblResult"></div>
<div style="position: relative;left: 5px; top:3px">

	<div id="qry_resultMaster">
	</div>
	<div id="pagerMaster"></div>
</div>

</body>
</html>