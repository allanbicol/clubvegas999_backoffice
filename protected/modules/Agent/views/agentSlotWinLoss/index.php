<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentwinloss.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		var readVirtuaSlotWinloss="<?php if(Yii::app()->user->checkAccess('agent.readVirtuaWinLoss') || User::getUserType() === Constants::ROLE_AGENT){echo '1';}?>";
		var readHatienSlotWinloss="<?php if(Yii::app()->user->checkAccess('agent.readHTVSlotWinLoss') || User::getUserType() === Constants::ROLE_AGENT){echo '1';}?>";
		var readSavanSlotWinloss="<?php if(Yii::app()->user->checkAccess('agent.readSavanSlotWinLoss') || User::getUserType() === Constants::ROLE_AGENT){echo '1';}?>";
		var readSlotSlotWinloss="<?php if(Yii::app()->user->checkAccess('agent.readSlotSlotWinLoss') || User::getUserType() === Constants::ROLE_AGENT){echo '1';}?>";
		//active menu color
		document.getElementById('agentHeader').className="start active";
		document.getElementById('agentHeader').className="start active";
		document.getElementById('mnu_agent_slot_winloss').className="active";
		$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Agent Slot Winloss</a></li>");
		//JQGRIDs
		
		/********START: HTV999 WinLoss*******************************************************/
		function tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_htv999_total').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstTotal_htv'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_total").appendChild(divTag);
	    	var grid=jQuery("#lstTotal_htv");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTV999Total&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	var col_group_header='';
	        if(accountID.length==0){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}else if(accountID.length==8){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
					  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
					  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
	    		],
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            rownumbers: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<font color='#ab0e10'><label id='lblHTVTotalWinLossID'></label></font> HATIENVEGAS999 TOTAL <select id='currencyTypeConvertHTV' onchange='javascript: htvTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            footerrow:true,
	    	});
	    	$("#lstTotal_htv").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	
			if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[6].name);
			}
		}
		function tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_htv999_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstHTV1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrHTV1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstHTV1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTVSummaryWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	        var col_agent='';
	        var col_group_header='';
	        if(accountID.length==0){
	        	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
		    	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}
	        var fltBetCount=0;
	        var fltValidBet=0;
	        var strCurrency='';
	        function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }

	        function htv_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
	    		    		col_agent,
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.company');?>',
	    		    		'exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: htv_xrate,summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				
				    //Lower level
				    $(".account_id_htv").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
					    	htvSubAgent(account_id_val);
			    			document.getElementById('lblAgentID_htv').innerHTML=account_id_val;
			    			//document.getElementById('qry_costavegas_total').innerHTML='';
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='lblDateFrom_htv'></label> - <label id='lblDateTo_htv'></label> <input class='btn red' type='button' id='btnBack_htv' value='<<' onclick='backToParent_htv(document.getElementById(\"lblAgentID_htv\").innerHTML);'> <input class='btn red' type='button' id='btnDetails_htv' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_htv(document.getElementById(\"lblAgentID_htv\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: htvWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrHTV1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstHTV1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	$('#lstHTV1').jqGrid('navGrid', '#pgrHTV1', {edit: false, add: false, del:false, search: false});
	    	$("#lstHTV1").jqGrid('navButtonAdd','#pgrHTV1',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_htv999_total');
	    			var table2= document.getElementById('qry_htv999_result');
	    			exportWinLossToExcel('HTV999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
			}
		}
		/**
		 * @todo export to excel
		 * @author leokarl
		 * @since 2013-01-24
		 */
		function exportWinLossToExcel(filename, html_table){
			var game = $('#cmbGame')[0].value;
			var currency = $('#cmbCurrency')[0].value;
			testChecked=0;
			if (document.getElementById("chkTestCurrency").checked==true){
				 testChecked=1;
			}
			var dateFrom	=	$('#dtFrom')[0].value + ' ' + $('#cmbTime1')[0].value + ':00:00';
			var dateTo	=	$('#dtTo')[0].value + ' ' + $('#cmbTime2')[0].value + ':59:59';
			var accountId = $('#txtAccountID')[0].value;
			var filenameSplit=filename.split("_");
			
			if (document.getElementById('chkVIRTUA').checked==true && document.getElementById('chkHTV').checked!=true && document.getElementById('chkSAVAN').checked!=true){
				document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
			}else if (document.getElementById('chkVIRTUA').checked!=true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked!=true){
				document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
			}else if (document.getElementById('chkVIRTUA').checked!=true && document.getElementById('chkHTV').checked!=true && document.getElementById('chkSAVAN').checked==true){
				document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
			}else if (document.getElementById('chkVIRTUA').checked==true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked!=true){
				if(filenameSplit[0]=='VIRTUAVEGAS999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}else if (document.getElementById('chkVIRTUA').checked==true && document.getElementById('chkHTV').checked!=true && document.getElementById('chkSAVAN').checked==true){
				if(filenameSplit[0]=='SAVANVEGAS999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}else if (document.getElementById('chkVIRTUA').checked!=true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked==true){
				if(filenameSplit[0]=='HTV999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}else if (document.getElementById('chkVIRTUA').checked==true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked==true){
				if(filenameSplit[0]=='VIRTUAVEGAS999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else if (filenameSplit[0]=='HTV999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[2].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}
			
			document.forms[1].csvBuffer.value = ""; // clear buffer
			document.forms[1].csvBuffer.value = html_table;
		    document.forms[1].method='POST';
		    document.forms[1].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
		    document.forms[1].target='_top';
		    document.forms[1].submit();
		}

		function exportAllWinLossToExcel(filename,url){
		    $.ajax({	
				 url:url, 
				 async:true,
				 success: function(result) {
						var data =result.split("<BREAK>");
				
						var game = $('#cmbGame')[0].value;
						var currency = $('#cmbCurrency')[0].value;
						testChecked=0;
						if (document.getElementById("chkTestCurrency").checked==true){
							 testChecked=1;
						}
						var dateFrom	=	$('#dtFrom')[0].value + ' ' + $('#cmbTime1')[0].value + ':00:00';
						var dateTo	=	$('#dtTo')[0].value + ' ' + $('#cmbTime2')[0].value + ':59:59';
						var accountId = $('#txtAccountID')[0].value;

						document.forms[1].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
						
						document.forms[1].csvBuffer.value = ""; // clear buffer
						document.forms[1].csvBuffer.value = data[2]+''+data[0];
					    document.forms[1].method = 'POST';
					    document.forms[1].action = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
					    document.forms[1].target = '_top';
					    document.forms[1].submit();
				 },
				 error: function(XMLHttpRequest, textStatus, errorThrown) { 
			 }});
		}
		function tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_htv999_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstHTV1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrHTV1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstHTV1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTVSummaryMemberWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	    	function htv_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: htv_xrate,summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    $(".account_id_htv").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    //start: hide parent grids
						    document.getElementById('qry_htv999_result').style.display='none';
						    //document.getElementById('qry_htv999_result_details').style.display='none';
						    //end: hide parent grids
					    	var dtfrom=document.getElementById('lblDateFrom_htv').innerHTML;
		        			var dtto=document.getElementById('lblDateTo_htv').innerHTML;
		        			tableMemberWinLoss_htv999_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
		        			document.getElementById('lblBettingDetailAgentID_htv').innerHTML=account_id_val;
		        			document.getElementById('lblDDateFrom_htv').innerHTML=dtfrom;
		        			document.getElementById('lblDDateTo_htv').innerHTML=dtto;
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
	    			$('#lstHTV1').jqGrid('navGrid', '#pgrHTV1', {edit: false, add: false, del:false, search: false});
	    	    	$("#lstHTV1").jqGrid('navButtonAdd','#pgrHTV1',{
	    	            caption:"Export to Excel", 
	    	            buttonicon:"ui-icon-calculator", 
	    	            onClickButton: function(){
	    	            	var table1= document.getElementById('qry_htv999_total');
	    	    			var table2= document.getElementById('qry_htv999_result');
	    	    			exportWinLossToExcel('HTV999_SLOT_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
	    	           }, 
	    	            position:"last"
	    	        });   
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='lblDateFrom_htv'></label> - <label id='lblDateTo_htv'></label> <input class='btn red' type='button' value='<<' id='btnBack_htv' onclick='backToParent_htv(document.getElementById(\"lblAgentID_htv\").innerHTML);'> <input type='button' id='btnDetails_htv' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_htv(document.getElementById(\"lblAgentID_htv\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: htvMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrHTV1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstHTV1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
				  ]
			});
		}
		//Details
		
		function tableHTVWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_htv999_result_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstHTV2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_result_details").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrHTV2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_result_details").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstHTV2");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTVWinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.company');?>'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_bonus', index: 'mem_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_bonus', index: 'ma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_bonus', index: 'sma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_bonus', index: 'sc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    
				    //Lower level
				    $(".detail_account_id_htv").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    if(account_id_val.length!=10){
				    			showDetails_htv(account_id_val);
				    			document.getElementById('lblDetailAgentID_htv').innerHTML=account_id_val;
						    }else{
							    //start: hide parent grids
							    document.getElementById('qry_htv999_result').style.display='none';
							    document.getElementById('qry_htv999_result_details').style.display='none';
							    //end: hide parent grids
						    	var dtfrom=document.getElementById('lblDetailDateFrom_htv').innerHTML;
			        			var dtto=document.getElementById('lblDetailDateTo_htv').innerHTML;
			        			tableMemberWinLoss_htv999_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
			        			document.getElementById('lblBettingDetailAgentID_htv').innerHTML=account_id_val;
			        			document.getElementById('lblDDateFrom_htv').innerHTML=dtfrom;
			        			document.getElementById('lblDDateTo_htv').innerHTML=dtto;
							}
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	        			var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblDetailAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_winloss_details');?> <label id='lblDetailDateFrom_htv'></label> - <label id='lblDetailDateTo_htv'></label> <input type='button' id='btnDetailBack_htv' value='<<' onclick='backToParentDetail_htv(document.getElementById(\"lblDetailAgentID_htv\").innerHTML);'>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrHTV2',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstHTV2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
					{startColumnName: 'ma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.master_agent');?></label>'},
					{startColumnName: 'sma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?></label>'},
					{startColumnName: 'sc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.sub_company');?></label>'},
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.main_company');?></label>'},
				  ]
			});
	    	$('#lstHTV2').jqGrid('navGrid', '#pgrHTV2', {edit: false, add: false, del:false, search: false});
	    	$("#lstHTV2").jqGrid('navButtonAdd','#pgrHTV2',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_htv999_result_details');
	    			exportWinLossToExcel('HTV999_SLOT_WIN_LOSS_DETAILS_', table1.outerHTML);
	           }, 
	            position:"last"
	        });   
		}
		function tableMemberWinLoss_htv999_details(strAccountID,dtFrom,dtTo,intGame,testCurrency){
			document.getElementById('qry_htv999_betting_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list5'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_betting_details").appendChild(divTag);

		    var div2Tag = document.createElement("Div"); 
		    div2Tag.id = 'pager5'; 
		    div2Tag.style.margin = "0px auto"; 
		    document.getElementById("qry_htv999_betting_details").appendChild(div2Tag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list5");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTV999WinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency, 
					datatype: 'json',
				    mtype: 'POST',
				    height: 'auto',
				    colNames: ["<?php echo Yii::t('agent','agent.agentwinloss.number');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>", "<?php echo Yii::t('agent','agent.agentwinloss.currency');?>","<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>", "<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>", "<?php echo Yii::t('agent','agent.agentwinloss.commission');?>", "<?php echo Yii::t('agent','agent.agentwinloss.tips');?>","<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>","<?php echo Yii::t('agent','agent.agentwinloss.total');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.casino');?>"],
				    colModel: [
					  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
					  //{name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
					  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : '<?php echo Yii::t('agent','agent.agentwinloss.total');?>'},
				      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
				      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
				      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
				      {name: 'bet_amount', index: 'bet_amount', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      //{name: 'amount_wagers', index: 'amount_wagers', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
				      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
				      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
				      {name: 'bonus', index: 'bonus', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
				      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
				      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
				    ],
				    loadComplete: function() {
					    var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	//if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
					    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
					    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
					    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
					    	if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
					    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
					    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
						}
					  	//color for summary footer
				        //$(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
				        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
				        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
				        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
				        $(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
				        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
				        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

				        $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    	showHTVBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency);
				    },
				    loadtext:"",
	                sortable: false,
				    sortname: 'row',
				    sortorder: 'ASC',
				    pager: '#pager5',
				    rowNum: 25,	
				    rowList: [20, 50, 100, 200, 99999],
				    caption: '<b><label id="lblBettingDetailAgentID_htv"></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_betting_details_title');?></b> <label id="lblDDateFrom_htv"></label> - <label id="lblDDateTo_htv"></label> <input type="button" value="<<" onclick="hideBettingDetails_htv();">',
				    hidegrid: false,
				    viewrecords: true,
				    grouping: true,
				   	groupingView : {
				   		groupField : ['currency_name'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				});
				$('#list5').jqGrid('navGrid', '#pager5', {edit: false, add: false, del:false, search: false, refresh: false});
				$("#list5").jqGrid('setGroupHeaders', {
					  useColSpanStyle: true, 
					  groupHeaders:[
						{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					  ]
				});
				$("#list5").jqGrid('navButtonAdd','#pager5',{
		            caption:"Export to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var table1= document.getElementById('qry_htv999_betting_details');
		    			exportWinLossToExcel('HTV999_SLOT_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
		           }, 
		            position:"last"
		        });   
				
			});
		}
		/********END: HTV999 WinLoss*******************************************************/
		/********START: SAVAN VEGAS WinLoss*******************************************************/
		function tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_savan_vegas_total').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstTotal_savan'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_total").appendChild(divTag);
	    	var grid=jQuery("#lstTotal_savan");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANTotal&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	 var col_group_header='';
		        if(accountID.length==0){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
			    }else if(accountID.length==2){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
				}else if(accountID.length==4){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
				}else if(accountID.length==6){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
				}else if(accountID.length==8){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
				}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
					  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
					  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
	    		      {name: 'company', index: 'company', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
	    		],
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            rownumbers: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<font color='#ab0e10'><label id='lblSAVANTotalWinLossID'></label></font> SAVANVEGAS999 TOTAL <select id='currencyTypeConvertSAVAN' onchange='javascript: savanTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            footerrow:true,
	    	});
	    	$("#lstTotal_savan").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
			
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[6].name);
			}
		}
		function tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_savan_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstSAVAN1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrSAVAN1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstSAVAN1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANSummaryWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	        var col_agent='';
	        var col_group_header='';
	        if(accountID.length==0){
	        	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
		    	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}
	        var fltBetCount=0;
	        var fltValidBet=0;
	        var strCurrency='';
	        function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function savan_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',col_agent,'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				
				    //Lower level
				    $(".account_id_savan").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
					    	savanSubAgent(account_id_val);
			    			document.getElementById('lblAgentID_savan').innerHTML=account_id_val;
			    			//document.getElementById('qry_costavegas_total').innerHTML='';
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
	    			
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_savan'></label> - <label id='lblDateTo_savan'></label> <input class='btn red' type='button' id='btnBack_savan' value='<<' onclick='backToParent_savan(document.getElementById(\"lblAgentID_savan\").innerHTML);'> <input class='btn red' type='button' id='btnDetails_savan' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_savan(document.getElementById(\"lblAgentID_savan\").innerHTML);'> <select id='currencyTypeConvertSAVANWinLoss' onchange='javascript: savanWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrSAVAN1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstSAVAN1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	$('#lstSAVAN1').jqGrid('navGrid', '#pgrSAVAN1', {edit: false, add: false, del:false, search: false});
	    	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_savan_vegas_total');
	    			var table2= document.getElementById('qry_savan_vegas_result');
	    			exportWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSAVANSummaryWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    			exportAllWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_',url);
	           }, 
	            position:"last"
	        }); 
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
			}
		}

		function tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_savan_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstSAVAN1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrSAVAN1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstSAVAN1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANSummaryMemberWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function savan_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".savan_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    $(".account_id_savan").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    //start: hide parent grids
						    document.getElementById('qry_savan_vegas_result').style.display='none';
						    document.getElementById('qry_savan_vegas_result_details').style.display='none';
						    //end: hide parent grids
					    	var dtfrom=document.getElementById('lblDateFrom_savan').innerHTML;
		        			var dtto=document.getElementById('lblDateTo_savan').innerHTML;
		        			tableMemberWinLoss_savan_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
		        			document.getElementById('lblBettingDetailAgentID_savan').innerHTML=account_id_val;
		        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
		        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_savan'></label> - <label id='lblDateTo_savan'></label> <input class='btn red' type='button' value='<<' id='btnBack_savan' onclick='backToParent_savan(document.getElementById(\"lblAgentID_savan\").innerHTML);'> <input class='btn red' type='button' id='btnDetails_savan' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_savan(document.getElementById(\"lblAgentID_savan\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: savanMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrSAVAN1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstSAVAN1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
				  ]
			});
	    	$('#lstSAVAN1').jqGrid('navGrid', '#pgrSAVAN1', {edit: false, add: false, del:false, search: false});
	    	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_savan_vegas_total');
	    			var table2= document.getElementById('qry_savan_vegas_result');
	    			exportWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_savan_vegas_total');
	    			var table2= document.getElementById('qry_savan_vegas_result');
	    			exportWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
		}
		//Details
		
		function tableSAVANWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_savan_vegas_result_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstSAVAN2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_result_details").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrSAVAN2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_result_details").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstSAVAN2");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANWinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.company');?>'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_bonus', index: 'mem_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_bonus', index: 'ma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_bonus', index: 'sma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_bonus', index: 'sc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    
				    //Lower level
				    $(".detail_account_id_savan").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    if(account_id_val.length!=10){
				    			showDetails_savan(account_id_val);
				    			document.getElementById('lblDetailAgentID_savan').innerHTML=account_id_val;
						    }else{
							    //start: hide parent grids
							    document.getElementById('qry_savan_vegas_result').style.display='none';
							    document.getElementById('qry_savan_vegas_result_details').style.display='none';
							    //end: hide parent grids
						    	var dtfrom=document.getElementById('lblDetailDateFrom_savan').innerHTML;
			        			var dtto=document.getElementById('lblDetailDateTo_savan').innerHTML;
			        			tableMemberWinLoss_savan_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
			        			document.getElementById('lblBettingDetailAgentID_savan').innerHTML=account_id_val;
			        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
			        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
							}
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	        			var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblDetailAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_winloss_details');?> <label id='lblDetailDateFrom_savan'></label> - <label id='lblDetailDateTo_savan'></label> <input class='btn red' type='button' id='btnDetailBack_savan' value='<<' onclick='backToParentDetail_savan(document.getElementById(\"lblDetailAgentID_savan\").innerHTML);'>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrSAVAN2',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstSAVAN2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
					{startColumnName: 'ma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.master_agent');?></label>'},
					{startColumnName: 'sma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?></label>'},
					{startColumnName: 'sc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.sub_company');?></label>'},
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.main_company');?></label>'},
				  ]
			});
	    	$('#lstSAVAN2').jqGrid('navGrid', '#pgrSAVAN2', {edit: false, add: false, del:false, search: false});
	    	$("#lstSAVAN2").jqGrid('navButtonAdd','#pgrSAVAN2',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_savan_vegas_result_details');
	    			exportWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_DETAILS_',table1.outerHTML);
	           }, 
	            position:"last"
	        });
	    	$("#lstSAVAN2").jqGrid('navButtonAdd','#pgrSAVAN2',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSAVANWinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    			exportAllWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_DETAILS_',url);
	           }, 
	            position:"last"
	        });
		}
		function tableMemberWinLoss_savan_vegas_details(strAccountID,dtFrom,dtTo,intGame,testCurrency){
			document.getElementById('qry_savan_vegas_betting_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list6'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_betting_details").appendChild(divTag);

		    var div2Tag = document.createElement("Div"); 
		    div2Tag.id = 'pager6'; 
		    div2Tag.style.margin = "0px auto"; 
		    document.getElementById("qry_savan_vegas_betting_details").appendChild(div2Tag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list6");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANWinLossBettingDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency, 
					datatype: 'json',
				    mtype: 'POST',
				    height: 'auto',
				    colNames: ["<?php echo Yii::t('agent','agent.agentwinloss.number');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>", "<?php echo Yii::t('agent','agent.agentwinloss.currency');?>","<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>", "<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>", "<?php echo Yii::t('agent','agent.agentwinloss.commission');?>", "<?php echo Yii::t('agent','agent.agentwinloss.tips');?>","<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>","<?php echo Yii::t('agent','agent.agentwinloss.total');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.casino');?>"],
				    colModel: [
					  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
					  //{name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
					  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : '<?php echo Yii::t('agent','agent.agentwinloss.total');?>'},
				      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
				      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
				      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
				      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      //{name: 'amount_wagers', index: 'amount_wagers', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
				      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
				      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
				      {name: 'bonus', index: 'bonus', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
				      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
				      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
				    ],
				    loadComplete: function() {
					    var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	//if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
					    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
					    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
					    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
					    	if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
					    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
					    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
						}
					  	//color for summary footer
				       // $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
				        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
				        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
				        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
				        $(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
				        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
				        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

				        $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    	showSAVANBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency);
				    },
				    loadtext:"",
	                sortable: false,
				    sortname: 'no',
				    sortorder: 'ASC',
				    pager: '#pager6',
				    rowNum: 25,	
				    rowList: [20, 50, 100, 200, 99999],
				    caption: '<b><label id="lblBettingDetailAgentID_savan"></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_betting_details_title');?></b> <label id="lblDDateFrom_savan"></label> - <label id="lblDDateTo_savan"></label> <input class="btn red" type="button" value="<<" onclick="hideBettingDetails_savan();">',
				    hidegrid: false,
				    viewrecords: true,
				    grouping: true,
				   	groupingView : {
				   		groupField : ['currency_name'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				});
				$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false, refresh: false});
				$("#list6").jqGrid('setGroupHeaders', {
					  useColSpanStyle: true, 
					  groupHeaders:[
						{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					  ]
				});
				$("#list6").jqGrid('navButtonAdd','#pager6',{
		            caption:"Export Current Page to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var table1= document.getElementById('qry_savan_vegas_betting_details');
		    			exportWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
		           }, 
		            position:"last"
		        });  
				$("#list6").jqGrid('navButtonAdd','#pager6',{
		            caption:"Export All Pages to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSAVANWinLossBettingDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency;
		    			exportAllWinLossToExcel('SAVANVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_', url);
		           }, 
		            position:"last"
		        }); 
				
			});
		}
		/********END: SAVANVEGAS WinLoss*******************************************************/
		/********START: VIRTUAVEGAS WinLoss*******************************************************/
		function tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_virtua_vegas_total').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstTotal_virtua'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_total").appendChild(divTag);
	    	var grid=jQuery("#lstTotal_virtua");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUATotal&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	 var col_group_header='';
		        if(accountID.length==0){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
			    }else if(accountID.length==2){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
				}else if(accountID.length==4){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
				}else if(accountID.length==6){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
				}else if(accountID.length==8){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
				}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
					  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
					  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
	    		      {name: 'company', index: 'company', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
	    		],
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            rownumbers: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<font color='#ab0e10'><label id='lblVIRTUATotalWinLossID'></label></font> VIRTUAVEGAS999 TOTAL <select id='currencyTypeConvertVIRTUA' onchange='javascript: virtuaTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            footerrow:true,
	    	});
	    	$("#lstTotal_virtua").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[6].name);
			}
		}
		function tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_virtua_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstVIRTUA1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrVIRTUA1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstVIRTUA1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUASummaryWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	        var col_agent='';
	        var col_group_header='';
	        if(accountID.length==0){
	        	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
		    	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}
	        var fltBetCount=0;
	        var fltValidBet=0;
	        var strCurrency='';
	        function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function virtua_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',col_agent,'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_virtua',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: virtua_xrate,summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				
				    //Lower level
				    $(".account_id_virtua").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
					    	virtuaSubAgent(account_id_val);
			    			document.getElementById('lblAgentID_virtua').innerHTML=account_id_val;
			    			//document.getElementById('qry_costavegas_total').innerHTML='';
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
	    			
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_virtua'></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_title');?> <label id='lblDateFrom_virtua'></label> - <label id='lblDateTo_virtua'></label> <input class='btn red' type='button' id='btnBack_virtua' value='<<' onclick='backToParent_virtua(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <input class='btn red' type='button' id='btnDetails_virtua' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_virtua(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <select id='currencyTypeConvertVIRTUAWinLoss' onchange='javascript: virtuaWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrVIRTUA1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstVIRTUA1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	$('#lstVIRTUA1').jqGrid('navGrid', '#pgrVIRTUA1', {edit: false, add: false, del:false, search: false});
	    	$("#lstVIRTUA1").jqGrid('navButtonAdd','#pgrVIRTUA1',{
	            caption:"Export Currency Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_virtua_vegas_total');
	    			var table2= document.getElementById('qry_virtua_vegas_result');
	    			exportWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	$("#lstVIRTUA1").jqGrid('navButtonAdd','#pgrVIRTUA1',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllVIRTUASummaryWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    			exportAllWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_',url);
	           }, 
	            position:"last"
	        });   
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
			}
		}

		function tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_virtua_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstVIRTUA1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrVIRTUA1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstVIRTUA1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUASummaryMemberWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function virtua_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_virtua',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: virtua_xrate,summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".virtua_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    $(".account_id_virtua").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    //start: hide parent grids
						    document.getElementById('qry_virtua_vegas_result').style.display='none';
						    document.getElementById('qry_virtua_vegas_result_details').style.display='none';
						    //end: hide parent grids
					    	var dtfrom=document.getElementById('lblDateFrom_virtua').innerHTML;
		        			var dtto=document.getElementById('lblDateTo_virtua').innerHTML;
		        			tableMemberWinLoss_virtua_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
		        			document.getElementById('lblBettingDetailAgentID_virtua').innerHTML=account_id_val;
		        			document.getElementById('lblDDateFrom_virtua').innerHTML=dtfrom;
		        			document.getElementById('lblDDateTo_virtua').innerHTML=dtto;
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
	    			
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_virtua'></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_title');?> <label id='lblDateFrom_virtua'></label> - <label id='lblDateTo_virtua'></label> <input class='btn red' type='button' value='<<' id='btnBack_virtua' onclick='backToParent_virtua(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <input class='btn red' type='button' id='btnDetails_virtua' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_virtua(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: virtuaMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrVIRTUA1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstVIRTUA1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
				  ]
			});
	    	$('#lstVIRTUA1').jqGrid('navGrid', '#pgrVIRTUA1', {edit: false, add: false, del:false, search: false});
	    	$("#lstVIRTUA1").jqGrid('navButtonAdd','#pgrVIRTUA1',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_virtua_vegas_total');
	    			var table2= document.getElementById('qry_virtua_vegas_result');
	    			exportWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	$("#lstVIRTUA1").jqGrid('navButtonAdd','#pgrVIRTUA1',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_virtua_vegas_total');
	    			var table2= document.getElementById('qry_virtua_vegas_result');
	    			exportWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
		}
		//Details
		
		function tableVIRTUAWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_virtua_vegas_result_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstVIRTUA2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_result_details").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrVIRTUA2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_result_details").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstVIRTUA2");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUAWinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.company');?>'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_virtua',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_bonus', index: 'mem_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'ma_bonus', index: 'ma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'sma_bonus', index: 'sma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'sc_bonus', index: 'sc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    
				    //Lower level
				    $(".detail_account_id_virtua").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    if(account_id_val.length!=10){
				    			showDetails_virtua(account_id_val);
				    			document.getElementById('lblDetailAgentID_virtua').innerHTML=account_id_val;
						    }else{
							    //start: hide parent grids
							    document.getElementById('qry_virtua_vegas_result').style.display='none';
							    document.getElementById('qry_virtua_vegas_result_details').style.display='none';
							    //end: hide parent grids
						    	var dtfrom=document.getElementById('lblDetailDateFrom_virtua').innerHTML;
			        			var dtto=document.getElementById('lblDetailDateTo_virtua').innerHTML;
			        			tableMemberWinLoss_virtua_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
			        			document.getElementById('lblBettingDetailAgentID_virtua').innerHTML=account_id_val;
			        			document.getElementById('lblDDateFrom_virtua').innerHTML=dtfrom;
			        			document.getElementById('lblDDateTo_virtua').innerHTML=dtto;
							}
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	        			var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblDetailAgentID_virtua'></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_winloss_details');?> <label id='lblDetailDateFrom_virtua'></label> - <label id='lblDetailDateTo_virtua'></label> <input class='btn red' type='button' id='btnDetailBack_virtua' value='<<' onclick='backToParentDetail_virtua(document.getElementById(\"lblDetailAgentID_virtua\").innerHTML);'>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrVIRTUA2',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstVIRTUA2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
					{startColumnName: 'ma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.master_agent');?></label>'},
					{startColumnName: 'sma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?></label>'},
					{startColumnName: 'sc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.sub_company');?></label>'},
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.main_company');?></label>'},
				  ]
			});
	    	$('#lstVIRTUA2').jqGrid('navGrid', '#pgrVIRTUA2', {edit: false, add: false, del:false, search: false});
	    	$("#lstVIRTUA2").jqGrid('navButtonAdd','#pgrVIRTUA2',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_virtua_vegas_result_details');
	    			exportWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_DETAILS_',table1.outerHTML);
	           }, 
	            position:"last"
	        }); 
	    	$("#lstVIRTUA2").jqGrid('navButtonAdd','#pgrVIRTUA2',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllVIRTUAWinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    			exportAllWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_DETAILS_',url);
	           }, 
	            position:"last"
	        });     
		}
		function tableMemberWinLoss_virtua_vegas_details(strAccountID,dtFrom,dtTo,intGame,testCurrency){
			document.getElementById('qry_virtua_vegas_betting_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list6'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_betting_details").appendChild(divTag);

		    var div2Tag = document.createElement("Div"); 
		    div2Tag.id = 'pager6'; 
		    div2Tag.style.margin = "0px auto"; 
		    document.getElementById("qry_virtua_vegas_betting_details").appendChild(div2Tag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list6");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUAWinLossBettingDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency, 
					datatype: 'json',
				    mtype: 'POST',
				    height: 'auto',
				    colNames: ["<?php echo Yii::t('agent','agent.agentwinloss.number');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>", "<?php echo Yii::t('agent','agent.agentwinloss.currency');?>","<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>", "<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>", "<?php echo Yii::t('agent','agent.agentwinloss.commission');?>", "<?php echo Yii::t('agent','agent.agentwinloss.tips');?>","<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>","<?php echo Yii::t('agent','agent.agentwinloss.total');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.casino');?>"],
				    colModel: [
					  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
					  //{name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
					  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : '<?php echo Yii::t('agent','agent.agentwinloss.total');?>'},
				      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
				      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
				      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
				      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      //{name: 'amount_wagers', index: 'amount_wagers', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
				      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
				      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
				      {name: 'bonus', index: 'bonus', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
				      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
				      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}, hidden: true},
				      {name: 'na_balance', index: 'balance', width: 100,sortable: false},
				      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
				    ],
				    loadComplete: function() {
					    var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	//if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
					    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
					    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
					    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
					    	if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
					    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
					    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
						}
					  	//color for summary footer
				       // $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
				        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
				        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
				        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
				        $(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
				        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
				        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

				        $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    	showVIRTUABettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency);
				    },
				    loadtext:"",
	                sortable: false,
				    sortname: 'no',
				    sortorder: 'ASC',
				    pager: '#pager6',
				    rowNum: 25,	
				    rowList: [20, 50, 100, 200, 99999],
				    caption: '<b><label id="lblBettingDetailAgentID_virtua"></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_betting_details_title');?></b> <label id="lblDDateFrom_virtua"></label> - <label id="lblDDateTo_virtua"></label> <input class="btn red" type="button" value="<<" onclick="hideBettingDetails_virtua();">',
				    hidegrid: false,
				    viewrecords: true,
				    grouping: true,
				   	groupingView : {
				   		groupField : ['currency_name'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				});
				$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false, refresh: false});
				$("#list6").jqGrid('setGroupHeaders', {
					  useColSpanStyle: true, 
					  groupHeaders:[
						{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					  ]
				});
				$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false});
		    	$("#list6").jqGrid('navButtonAdd','#pager6',{
		            caption:"Export Current Page to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var table1= document.getElementById('qry_virtua_vegas_betting_details');
		    			exportWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_',table1.outerHTML);
		           }, 
		            position:"last"
		        });   
		    	$("#list6").jqGrid('navButtonAdd','#pager6',{
		            caption:"Export All pages to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllVIRTUAWinLossBettingDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency;
		    			exportAllWinLossToExcel('VIRTUAVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_',url);
		           }, 
		            position:"last"
		        });
			});
		}
		/********END: VIRTUAVEGAS WinLoss*******************************************************/
		/********START: SLOT VEGAS WinLoss*******************************************************/
		function tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_slot_vegas_total').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstTotal_slot'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_total").appendChild(divTag);
	    	var grid=jQuery("#lstTotal_slot");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SLOTTotal&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	 var col_group_header='';
		        if(accountID.length==0){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
			    }else if(accountID.length==2){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
				}else if(accountID.length==4){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
				}else if(accountID.length==6){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
				}else if(accountID.length==8){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
				}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
					  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
					  {name: 'total_stake', hidden:true, index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
	    		      {name: 'company', index: 'company', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
	    		],
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            rownumbers: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<font color='#ab0e10'><label id='lblSLOTTotalWinLossID'></label></font> SLOTVEGAS999 TOTAL <select id='currencyTypeConvertSlot' onchange='javascript: slotTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            footerrow:true,
	    	});
	    	$("#lstTotal_slot").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
			
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[6].name);
			}
		}
		function tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_slot_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstSLOT1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrSLOT1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstSLOT1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SLOTSummaryWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	        var col_agent='';
	        var col_group_header='';
	        if(accountID.length==0){
	        	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
		    	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}
	        var fltBetCount=0;
	        var fltValidBet=0;
	        var strCurrency='';
	        function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function slot_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',col_agent,'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_slot',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'total_stake', hidden:true, index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: slot_xrate,summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				
				    //Lower level
				    $(".account_id_slot").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
					    	slotSubAgent(account_id_val);
			    			document.getElementById('lblAgentID_slot').innerHTML=account_id_val;
			    			//document.getElementById('qry_costavegas_total').innerHTML='';
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
	    			
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_slot'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_slot'></label> - <label id='lblDateTo_slot'></label> <input class='btn red' type='button' id='btnBack_slot' value='<<' onclick='backToParent_slot(document.getElementById(\"lblAgentID_slot\").innerHTML);'> <input class='btn red' type='button' id='btnDetails_slot' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_slot(document.getElementById(\"lblAgentID_slot\").innerHTML);'> <select id='currencyTypeConvertSLOTWinLoss' onchange='javascript: slotWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrSLOT1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstSLOT1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	$('#lstSLOT1').jqGrid('navGrid', '#pgrSLOT1', {edit: false, add: false, del:false, search: false});
	    	$("#lstSLOT1").jqGrid('navButtonAdd','#pgrSLOT1',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_slot_vegas_total');
	    			var table2= document.getElementById('qry_slot_vegas_result');
	    			exportWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	$("#lstSLOT1").jqGrid('navButtonAdd','#pgrSLOT1',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSLOTSummaryWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    			exportAllWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_',url);
	           }, 
	            position:"last"
	        }); 
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
			}
		}

		function tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_slot_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstSLOT1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrSLOT1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstSLOT1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SLOTSummaryMemberWinLoss&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    	function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function slot_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_slot',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'total_stake', hidden:true, index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance',hidden:true, width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: slot_xrate,summaryTpl:'<label class="slot_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".slot_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    $(".account_id_slot").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    //start: hide parent grids
						    document.getElementById('qry_slot_vegas_result').style.display='none';
						    document.getElementById('qry_slot_vegas_result_details').style.display='none';
						    //end: hide parent grids
					    	var dtfrom=document.getElementById('lblDateFrom_slot').innerHTML;
		        			var dtto=document.getElementById('lblDateTo_slot').innerHTML;
		        			tableMemberWinLoss_slot_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
		        			document.getElementById('lblBettingDetailAgentID_slot').innerHTML=account_id_val;
		        			document.getElementById('lblDDateFrom_slot').innerHTML=dtfrom;
		        			document.getElementById('lblDDateTo_slot').innerHTML=dtto;
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_slot'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_slot'></label> - <label id='lblDateTo_slot'></label> <input class='btn red' type='button' value='<<' id='btnBack_slot' onclick='backToParent_slot(document.getElementById(\"lblAgentID_slot\").innerHTML);'> <input class='btn red' type='button' id='btnDetails_slot' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='showDetails_slot(document.getElementById(\"lblAgentID_slot\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: slotMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrSLOT1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstSLOT1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
				  ]
			});
	    	$('#lstSLOT1').jqGrid('navGrid', '#pgrSLOT1', {edit: false, add: false, del:false, search: false});
	    	$("#lstSLOT1").jqGrid('navButtonAdd','#pgrSLOT1',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_slot_vegas_total');
	    			var table2= document.getElementById('qry_slot_vegas_result');
	    			exportWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	$("#lstSLOT1").jqGrid('navButtonAdd','#pgrSLOT1',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_slot_vegas_total');
	    			var table2= document.getElementById('qry_slot_vegas_result');
	    			exportWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
		}
		//Details
		
		function tableSLOTWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('qry_slot_vegas_result_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'lstSLOT2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_result_details").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pgrSLOT2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_result_details").appendChild(divTag1);
		    
	    	var grid=jQuery("#lstSLOT2");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SLOTWinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;

	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.company');?>'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_slot',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total_stake', hidden:true, index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_bonus', index: 'mem_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_bonus', index: 'ma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_bonus', index: 'sma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_bonus', index: 'sc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    
				    //Lower level
				    $(".detail_account_id_slot").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    if(account_id_val.length!=10){
				    			showDetails_slot(account_id_val);
				    			document.getElementById('lblDetailAgentID_slot').innerHTML=account_id_val;
						    }else{
							    //start: hide parent grids
							    document.getElementById('qry_slot_vegas_result').style.display='none';
							    document.getElementById('qry_slot_vegas_result_details').style.display='none';
							    //end: hide parent grids
						    	var dtfrom=document.getElementById('lblDetailDateFrom_slot').innerHTML;
			        			var dtto=document.getElementById('lblDetailDateTo_slot').innerHTML;
			        			tableMemberWinLoss_slot_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
			        			document.getElementById('lblBettingDetailAgentID_slot').innerHTML=account_id_val;
			        			document.getElementById('lblDDateFrom_slot').innerHTML=dtfrom;
			        			document.getElementById('lblDDateTo_slot').innerHTML=dtto;
							}
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	        			var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv('dvSharingCommission',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('dvSharingCommission').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('dvSharingCommission').innerHTML=data;
	            				}else{
	            					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv('dvSharingCommission');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblDetailAgentID_slot'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_winloss_details');?> <label id='lblDetailDateFrom_slot'></label> - <label id='lblDetailDateTo_slot'></label> <input class='btn red' type='button' id='btnDetailBack_slot' value='<<' onclick='backToParentDetail_slot(document.getElementById(\"lblDetailAgentID_slot\").innerHTML);'>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#pgrSLOT2',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#lstSLOT2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
					{startColumnName: 'ma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.master_agent');?></label>'},
					{startColumnName: 'sma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?></label>'},
					{startColumnName: 'sc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.sub_company');?></label>'},
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.main_company');?></label>'},
				  ]
			});
	    	$('#lstSLOT2').jqGrid('navGrid', '#pgrSLOT2', {edit: false, add: false, del:false, search: false});
	    	$("#lstSLOT2").jqGrid('navButtonAdd','#pgrSLOT2',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('qry_slot_vegas_result_details');
	    			exportWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_DETAILS_',table1.outerHTML);
	           }, 
	            position:"last"
	        });
	    	$("#lstSLOT2").jqGrid('navButtonAdd','#pgrSLOT2',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSLOTWinLossDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
	    			exportAllWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_DETAILS_',url);
	           }, 
	            position:"last"
	        });
		}
		function tableMemberWinLoss_slot_vegas_details(strAccountID,dtFrom,dtTo,intGame,testCurrency){
			document.getElementById('qry_slot_vegas_betting_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list6'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_betting_details").appendChild(divTag);

		    var div2Tag = document.createElement("Div"); 
		    div2Tag.id = 'pager6'; 
		    div2Tag.style.margin = "0px auto"; 
		    document.getElementById("qry_slot_vegas_betting_details").appendChild(div2Tag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list6");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SLOTWinLossBettingDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency, 
					datatype: 'json',
				    mtype: 'POST',
				    height: 'auto',
				    colNames: ["<?php echo Yii::t('agent','agent.agentwinloss.number');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>", "<?php echo Yii::t('agent','agent.agentwinloss.currency');?>","<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>", "<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>", "<?php echo Yii::t('agent','agent.agentwinloss.commission');?>", "<?php echo Yii::t('agent','agent.agentwinloss.tips');?>","<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>","<?php echo Yii::t('agent','agent.agentwinloss.total');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.casino');?>"],
				    colModel: [
					  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
					  //{name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
					  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : '<?php echo Yii::t('agent','agent.agentwinloss.total');?>'},
				      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
				      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
				      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
				      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      //{name: 'amount_wagers', index: 'amount_wagers', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
				      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
				      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
				      {name: 'bonus', index: 'bonus', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
				      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
				      {name: 'balance', index: 'balance',hidden:true, width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
				    ],
				    loadComplete: function() {
					    var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	//if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
					    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
					    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
					    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
					    	if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
					    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
					    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
						}
					  	//color for summary footer
				       // $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
				        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
				        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
				        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
				        $(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
				        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
				        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

				        $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    	showSLOTBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency);
				    },
				    loadtext:"",
	                sortable: false,
				    sortname: 'no',
				    sortorder: 'ASC',
				    pager: '#pager6',
				    rowNum: 25,	
				    rowList: [20, 50, 100, 200, 99999],
				    caption: '<b><label id="lblBettingDetailAgentID_slot"></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_betting_details_title');?></b> <label id="lblDDateFrom_slot"></label> - <label id="lblDDateTo_slot"></label> <input class="btn red" type="button" value="<<" onclick="hideBettingDetails_slot();">',
				    hidegrid: false,
				    viewrecords: true,
				    grouping: true,
				   	groupingView : {
				   		groupField : ['currency_name'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				});
				$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false, refresh: false});
				$("#list6").jqGrid('setGroupHeaders', {
					  useColSpanStyle: true, 
					  groupHeaders:[
						{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					  ]
				});
				$("#list6").jqGrid('navButtonAdd','#pager6',{
		            caption:"Export Current Page to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var table1= document.getElementById('qry_slot_vegas_betting_details');
		    			exportWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
		           }, 
		            position:"last"
		        });  
				$("#list6").jqGrid('navButtonAdd','#pager6',{
		            caption:"Export All Pages to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSLOTWinLossBettingDetails&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency;
		    			exportAllWinLossToExcel('SLOTVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_', url);
		           }, 
		            position:"last"
		        }); 
				
			});
		}
		/********END: SLOTVEGAS WinLoss*******************************************************/
	</script>
	<script>
	//Javascript functions
	function checkIt(evt,id) {
	    evt = (evt) ? evt : window.event
	    var charCode = (evt.which) ? evt.which : evt.keyCode
	    var strText=document.getElementById(id).value;
	    var countStr=(strText.split(".").length - 1);
	    if (charCode==46){
		    //allow only one period
	    	if(countStr > 0){
		    	return false;
		    }
		}
	    if (charCode > 31 && ((charCode < 48 && charCode!=46) || charCode > 57)) {
	        status = "This field accepts numbers only.";
	        return false;
	    }
	    status = "";
	    return true;
	}
	//start sharing and commission
	
	function onMouseOutDiv(id){
		document.getElementById('id').addEventListener('mouseout',onMouseOut,true);
	}
	function onMouseOut(event) {
	    //this is the original element the event handler was assigned to
	        e = event.toElement || event.relatedTarget;
	        if (e.parentNode == this || e == this) {
	           return;
	        }
	        hideDiv("dvSharingCommission");
	    // handle mouse event here!
	}
	$(document).ready(function() { 
		var div1Tag = document.createElement("div"); 
	    div1Tag.id = "dvSharingCommission"; 
	    div1Tag.setAttribute("align", "center"); 
	    div1Tag.setAttribute("onmouseout","javascript: document.getElementById('dvSharingCommission').addEventListener('mouseout',onMouseOut,true);");
	    div1Tag.style.margin = "0px auto"; 
	    div1Tag.className = "dynamicDiv"; 
	    document.body.appendChild(div1Tag);
	    hideDiv('dvSharingCommission');
	}); 

	function hideDiv(divID) { 
		if (document.getElementById) { 
		 	document.getElementById(divID).style.visibility = 'hidden'; 
		} 
	}
		
	function showDiv(divID,objXY,x,y) { 
		//get link position
		for (var lx=0, ly=0;
		objXY != null;
        lx += objXY.offsetLeft, ly += objXY.offsetTop, objXY = objXY.offsetParent);
        
        //set div new position
		document.getElementById(divID).style.left= (x-65) +"px";
		document.getElementById(divID).style.top= (y+15) +"px";
		document.getElementById(divID).style.visibility = 'visible'; 
	}
	//end sharing and commission
	jQuery(document).ready(function() {
	    jQuery("#dtFrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	        //dateFormat: "dd-mm-yy"
	    });
	
	    jQuery("#dtTo").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});
	
	function htvSubAgent(accountID)
	{
		var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
		var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
		var strCurrency= document.getElementById('cmbCurrency').value;
		var intGame=document.getElementById('cmbGame').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
			if(accountID.length!=8){
				tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}else{
				tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}else{
						tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}
				}else{
					tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}
		if(accountID==''){
			document.getElementById('btnBack_htv').style.display='none';
		}else{
			document.getElementById('btnBack_htv').style.display='inline';
		}
		document.getElementById('lblDateFrom_htv').innerHTML=dtFrom;
		document.getElementById('lblDateTo_htv').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(document.getElementById('lblAgentID_htv').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('lblAgentID_htv').innerHTML=account_id;
					//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				}
			}
		}
	}
	function savanSubAgent(accountID)
	{
		var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
		var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
		var strCurrency= document.getElementById('cmbCurrency').value;
		var intGame=document.getElementById('cmbGame').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
			if(accountID.length!=8){
				tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}else{
				tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}else{
						tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}
				}else{
					tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}
		if(accountID==''){
			document.getElementById('btnBack_savan').style.display='none';
		}else{
			document.getElementById('btnBack_savan').style.display='inline';
		}
		document.getElementById('lblDateFrom_savan').innerHTML=dtFrom;
		document.getElementById('lblDateTo_savan').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(document.getElementById('lblAgentID_savan').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('lblAgentID_savan').innerHTML=account_id;
					//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				}
			}
		}
	}
	function virtuaSubAgent(accountID)
	{
		var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
		var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
		var strCurrency= document.getElementById('cmbCurrency').value;
		var intGame=document.getElementById('cmbGame').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
			if(accountID.length!=8){
				tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}else{
				tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}else{
						tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}
				}else{
					tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}
		if(accountID==''){
			document.getElementById('btnBack_virtua').style.display='none';
		}else{
			document.getElementById('btnBack_virtua').style.display='inline';
		}
		document.getElementById('lblDateFrom_virtua').innerHTML=dtFrom;
		document.getElementById('lblDateTo_virtua').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(document.getElementById('lblAgentID_virtua').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				}
			}
		}
	}

	function slotSubAgent(accountID)
	{
		var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
		var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
		var strCurrency= document.getElementById('cmbCurrency').value;
		var intGame=document.getElementById('cmbGame').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
			if(accountID.length!=8){
				tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}else{
				tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}else{
						tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
					}
				}else{
					tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}
		if(accountID==''){
			document.getElementById('btnBack_slot').style.display='none';
		}else{
			document.getElementById('btnBack_slot').style.display='inline';
		}
		document.getElementById('lblDateFrom_slot').innerHTML=dtFrom;
		document.getElementById('lblDateTo_slot').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(document.getElementById('lblAgentID_slot').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('lblAgentID_slot').innerHTML=account_id;
					//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				}
			}
		}
	}
	function submitComplete(accountID)
	{
		/* if(readVirtuaSlotWinloss!=1){
			document.getElementById('dv_virtuavegas').style.display='none';
			document.getElementById('chkVIRTUA').style.display='none';
			document.getElementById('lblchkVIRTUA').style.display='none';
		} */
		if(readHatienSlotWinloss!=1){
			document.getElementById('dv_hatienvegas').style.display='none';
			document.getElementById('chkHTV').style.display='none';
			document.getElementById('lblchkHTV').style.display='none';
		}

		var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
		var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
		var strCurrency= document.getElementById('cmbCurrency').value;
		var intGame=document.getElementById('cmbGame').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
// 				if(document.getElementById('chkHTV').checked==true){
// 					document.getElementById('dv_hatienvegas').style.display='block';
// 					tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 				}else{
// 					document.getElementById('dv_hatienvegas').style.display='none';
// 				}
				
// 				if(document.getElementById('chkVIRTUA').checked==true){
// 					document.getElementById('dv_virtuavegas').style.display='block';
// 					tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 				}else{
// 					document.getElementById('dv_virtuavegas').style.display='none';
// 				}
				if(document.getElementById('chkSLOT').checked==true){
					document.getElementById('dv_slotvegas').style.display='block';
					tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_slotvegas').style.display='none';
				}
			}
			if(accountID.length!=8){
// 				if(document.getElementById('chkHTV').checked==true){
// 					document.getElementById('dv_hatienvegas').style.display='block';
// 					tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 				}else{
// 					document.getElementById('dv_hatienvegas').style.display='none';
// 				}
				
// 				if(document.getElementById('chkVIRTUA').checked==true){
// 					document.getElementById('dv_virtuavegas').style.display='block';
// 					tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 				}else{
// 					document.getElementById('dv_virtuavegas').style.display='none';
// 				}
				if(document.getElementById('chkSLOT').checked==true){
					document.getElementById('dv_slotvegas').style.display='block';
					tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_slotvegas').style.display='none';
				}
				
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
// 						if(document.getElementById('chkHTV').checked==true){
// 							document.getElementById('dv_hatienvegas').style.display='block';
// 							tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 							tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 						}else{
// 							document.getElementById('dv_hatienvegas').style.display='none';
// 						}
						
// 						if(document.getElementById('chkVIRTUA').checked==true){
// 							document.getElementById('dv_virtuavegas').style.display='block';
// 							tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 							tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 						}else{
// 							document.getElementById('dv_virtuavegas').style.display='none';
// 						}
						if(document.getElementById('chkSLOT').checked==true){
							document.getElementById('dv_slotvegas').style.display='block';
							tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
							tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_slotvegas').style.display='none';
						}
					}else{
// 						if(document.getElementById('chkHTV').checked==true){
// 							document.getElementById('dv_hatienvegas').style.display='block';
// 							tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 							tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 						}else{
// 							document.getElementById('dv_hatienvegas').style.display='none';
// 						}
						
// 						if(document.getElementById('chkVIRTUA').checked==true){
// 							document.getElementById('dv_virtuavegas').style.display='block';
// 							tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 							tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 						}else{
// 							document.getElementById('dv_virtuavegas').style.display='none';
// 						}
						if(document.getElementById('chkSLOT').checked==true){
							document.getElementById('dv_slotvegas').style.display='block';
							tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
							tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_slotvegas').style.display='none';
						}
					}
				}else{
// 					if(document.getElementById('chkHTV').checked==true){
// 						document.getElementById('dv_hatienvegas').style.display='block';
// 						tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 					}else{
// 						document.getElementById('dv_hatienvegas').style.display='none';
// 					}
					
// 					if(document.getElementById('chkVIRTUA').checked==true){
// 						document.getElementById('dv_virtuavegas').style.display='block';
// 						tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 					}else{
// 						document.getElementById('dv_virtuavegas').style.display='none';
// 					}
					if(document.getElementById('chkSLOT').checked==true){
						document.getElementById('dv_slotvegas').style.display='block';
						tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
					}else{
						document.getElementById('dv_slotvegas').style.display='none';
					}
				}
			}else{
// 				if(document.getElementById('chkHTV').checked==true){
// 					document.getElementById('dv_hatienvegas').style.display='block';
// 					tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 				}else{
// 					document.getElementById('dv_hatienvegas').style.display='none';
// 				}
				
// 				if(document.getElementById('chkVIRTUA').checked==true){
// 					document.getElementById('dv_virtuavegas').style.display='block';
// 					tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
// 				}else{
// 					document.getElementById('dv_virtuavegas').style.display='none';
// 				}
				if(document.getElementById('chkSLOT').checked==true){
					document.getElementById('dv_slotvegas').style.display='block';
					tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_slotvegas').style.display='none';
				}
			}
		}
		if(accountID==''){
// 			document.getElementById('btnBack_htv').style.display='none';
// 			document.getElementById('btnBack_virtua').style.display='none';
			document.getElementById('btnBack_slot').style.display='none';
		}else{
// 			document.getElementById('btnBack_htv').style.display='inline';
// 			document.getElementById('btnBack_virtua').style.display='inline';
			document.getElementById('btnBack_slot').style.display='inline';
		}
// 		document.getElementById('lblDateFrom_htv').innerHTML=dtFrom;
// 		document.getElementById('lblDateTo_htv').innerHTML=dtTo;
// 		document.getElementById('lblDateFrom_virtua').innerHTML=dtFrom;
// 		document.getElementById('lblDateTo_virtua').innerHTML=dtTo;
		document.getElementById('lblDateFrom_slot').innerHTML=dtFrom;
		document.getElementById('lblDateTo_slot').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(document.getElementById('lblAgentID_htv').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
// 					document.getElementById('lblAgentID_htv').innerHTML=account_id;
// 					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					document.getElementById('lblAgentID_slot').innerHTML=account_id;
// 					document.getElementById('lblHTVTotalWinLossID').innerHTML=account_id;
// 					document.getElementById('lblVIRTUATotalWinLossID').innerHTML=account_id;
					document.getElementById('lblSLOTTotalWinLossID').innerHTML=account_id;
				}
			}
		}
// 		hideBettingDetails_htv();
// 		hideBettingDetails_virtua();
		hideBettingDetails_slot();
// 		if(readVirtuaSlotWinloss!=1){
// 			document.getElementById('dv_virtuavegas').style.display='none';
// 			document.getElementById('chkVIRTUA').checked==false;
// 			document.getElementById('chkVIRTUA').style.display='none';
// 			document.getElementById('lblchkVIRTUA').style.display='none';
// 		}
// 		if(readHatienSlotWinloss!=1){
// 			document.getElementById('dv_hatienvegas').style.display='none';
// 			document.getElementById('chkHTV').checked==false;
// 			document.getElementById('chkHTV').style.display='none';
// 			document.getElementById('lblchkHTV').style.display='none';
// 		}
		if(readSlotSlotWinloss!=1){
			document.getElementById('dv_slotvegas').style.display='none';
			document.getElementById('chkSLOT').checked==false;
			document.getElementById('chkSLOT').style.display='none';
			document.getElementById('lblchkSLOT').style.display='none';
		}
	}
	function btnToDay()
	{
// 		if(readVirtuaSlotWinloss!=1){
// 			document.getElementById('dv_virtuavegas').style.display='none';
// 			document.getElementById('chkVIRTUA').style.display='none';
// 			document.getElementById('lblchkVIRTUA').style.display='none';
// 		}
// 		if(readHatienSlotWinloss!=1){
// 			document.getElementById('dv_hatienvegas').style.display='none';
// 			document.getElementById('chkHTV').style.display='none';
// 			document.getElementById('lblchkHTV').style.display='none';
// 		}
		if(readSlotSlotWinloss!=1){
			document.getElementById('dv_slotvegas').style.display='none';
			document.getElementById('chkSLOT').style.display='none';
			document.getElementById('lblchkSLOT').style.display='none';
		}
		var dtToday='<?php echo date("Y-m-d");?>';
		document.getElementById('dtFrom').value=dtToday;
		document.getElementById('dtTo').value=dtToday;
		document.getElementById('cmbTime1').value='00';
		document.getElementById('cmbTime2').value='23';
		var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
		var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
		var strCurrency= document.getElementById('cmbCurrency').value;
		var intGame=document.getElementById('cmbGame').value;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}

		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
// 			if(document.getElementById('chkHTV').checked==true){
// 				document.getElementById('dv_hatienvegas').style.display='block';
// 				tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 				tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 			}else{
// 				document.getElementById('dv_hatienvegas').style.display='none';
// 			}

// 			if(document.getElementById('chkVIRTUA').checked==true){
// 				document.getElementById('dv_virtuavegas').style.display='block';
// 				tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 				tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 			}else{
// 				document.getElementById('dv_virtuavegas').style.display='none';
// 			}
			if(document.getElementById('chkSLOT').checked==true){
				document.getElementById('dv_slotvegas').style.display='block';
				tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
				tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
			}else{
				document.getElementById('dv_slotvegas').style.display='none';
			}
		}else{
// 			if(document.getElementById('chkHTV').checked==true){
// 				document.getElementById('dv_hatienvegas').style.display='block';
// 				tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 			}else{
// 				document.getElementById('dv_hatienvegas').style.display='none';
// 			}

// 			if(document.getElementById('chkVIRTUA').checked==true){
// 				document.getElementById('dv_virtuavegas').style.display='block';
// 				tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 			}else{
// 				document.getElementById('dv_virtuavegas').style.display='none';
// 			}
			if(document.getElementById('chkSLOT').checked==true){
				document.getElementById('dv_slotvegas').style.display='block';
				tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
			}else{
				document.getElementById('dv_slotvegas').style.display='none';
			}
			if(user_level!='AGT'){
// 				if(document.getElementById('chkHTV').checked==true){
// 					document.getElementById('dv_hatienvegas').style.display='block';
// 					tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_hatienvegas').style.display='none';
// 				}

// 				if(document.getElementById('chkVIRTUA').checked==true){
// 					document.getElementById('dv_virtuavegas').style.display='block';
// 					tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_virtuavegas').style.display='none';
// 				}
				if(document.getElementById('chkSLOT').checked==true){
					document.getElementById('dv_slotvegas').style.display='block';
					tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_slotvegas').style.display='none';
				}
			}else{
// 				if(document.getElementById('chkHTV').checked==true){
// 					document.getElementById('dv_hatienvegas').style.display='block';
// 					tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_hatienvegas').style.display='none';
// 				}
// 				if(document.getElementById('chkVIRTUA').checked==true){
// 					document.getElementById('dv_virtuavegas').style.display='block';
// 					tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_virtuavegas').style.display='none';
// 				}
				if(document.getElementById('chkSLOT').checked==true){
					document.getElementById('dv_slotvegas').style.display='block';
					tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_slotvegas').style.display='none';
				}
			}
		}
// 		document.getElementById('lblDateFrom_htv').innerHTML=dtFrom;
// 		document.getElementById('lblDateTo_htv').innerHTML=dtTo;
// 		document.getElementById('lblDateFrom_virtua').innerHTML=dtFrom;
// 		document.getElementById('lblDateTo_virtua').innerHTML=dtTo;
		document.getElementById('lblDateFrom_slot').innerHTML=dtFrom;
		document.getElementById('lblDateTo_slot').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(document.getElementById('lblAgentID_htv').innerHTML==''){
// 			document.getElementById('btnBack_htv').style.display='none';
// 			document.getElementById('btnBack_virtua').style.display='none';
			document.getElementById('btnBack_slot').style.display='none';
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
// 					document.getElementById('lblAgentID_htv').innerHTML=account_id;
// 					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					document.getElementById('lblAgentID_slot').innerHTML=account_id;
// 					document.getElementById('lblHTVTotalWinLossID').innerHTML=account_id;
// 					document.getElementById('lblVIRTUATotalWinLossID').innerHTML=account_id;
					document.getElementById('lblSLOTTotalWinLossID').innerHTML=account_id;
				}
			}
		}
// 		hideBettingDetails_htv();
// 		hideBettingDetails_virtua();
		hideBettingDetails_slot();
// 		if(readVirtuaSlotWinloss!=1){
// 			document.getElementById('dv_virtuavegas').style.display='none';
// 			document.getElementById('chkVIRTUA').checked==false;
// 			document.getElementById('chkVIRTUA').style.display='none';
// 			document.getElementById('lblchkVIRTUA').style.display='none';
// 		}
// 		if(readHatienSlotWinloss!=1){
// 			document.getElementById('dv_hatienvegas').style.display='none';
// 			document.getElementById('chkHTV').checked==false;
// 			document.getElementById('chkHTV').style.display='none';
// 			document.getElementById('lblchkHTV').style.display='none';
// 		}

		if(readSlotSlotWinloss!=1){
			document.getElementById('dv_slotvegas').style.display='none';
			document.getElementById('chkSLOT').checked==false;
			document.getElementById('chkSLOT').style.display='none';
			document.getElementById('lblchkSLOT').style.display='none';
		}
	}
	function btnYesterday()
	{
// 		if(readVirtuaSlotWinloss!=1){
// 			document.getElementById('dv_virtuavegas').style.display='none';
// 			document.getElementById('chkVIRTUA').style.display='none';
// 			document.getElementById('lblchkVIRTUA').style.display='none';
// 		}
// 		if(readHatienSlotWinloss!=1){
// 			document.getElementById('dv_hatienvegas').style.display='none';
// 			document.getElementById('chkHTV').style.display='none';
// 			document.getElementById('lblchkHTV').style.display='none';
// 		}

		if(readSlotSlotWinloss!=1){
			document.getElementById('dv_slotvegas').style.display='none';
			document.getElementById('chkSLOT').style.display='none';
			document.getElementById('lblchkSLOT').style.display='none';
		}
		var dtYesterday='<?php echo date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d"))));?>';
		document.getElementById('dtFrom').value=dtYesterday;
		document.getElementById('dtTo').value=dtYesterday;
		document.getElementById('cmbTime1').value='00';
		document.getElementById('cmbTime2').value='23';
		var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
		var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
		var strCurrency= document.getElementById('cmbCurrency').value;
		var intGame=document.getElementById('cmbGame').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
// 			if(document.getElementById('chkHTV').checked==true){
// 				document.getElementById('dv_hatienvegas').style.display='block';
// 				tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 				tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 			}else{
// 				document.getElementById('dv_hatienvegas').style.display='none';
// 			}
// 			if(document.getElementById('chkVIRTUA').checked==true){
// 				document.getElementById('dv_virtuavegas').style.display='block';
// 				tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 				tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
// 			}else{
// 				document.getElementById('dv_virtuavegas').style.display='none';
// 			}
			if(document.getElementById('chkSLOT').checked==true){
				document.getElementById('dv_slotvegas').style.display='block';
				tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
				tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
			}else{
				document.getElementById('dv_slotvegas').style.display='none';
			}
		}else{
// 			if(document.getElementById('chkHTV').checked==true){
// 				document.getElementById('dv_hatienvegas').style.display='block';
// 				tableHTVWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 			}else{
// 				document.getElementById('dv_hatienvegas').style.display='none';
// 			}

// 			if(document.getElementById('chkVIRTUA').checked==true){
// 				document.getElementById('dv_virtuavegas').style.display='block';
// 				tableVIRTUAWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 			}else{
// 				document.getElementById('dv_virtuavegas').style.display='none';
// 			}
			if(document.getElementById('chkSLOT').checked==true){
				document.getElementById('dv_slotvegas').style.display='block';
				tableSLOTWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
			}else{
				document.getElementById('dv_slotvegas').style.display='none';
			}
			if(user_level!='AGT'){
// 				if(document.getElementById('chkHTV').checked==true){
// 					document.getElementById('dv_hatienvegas').style.display='block';
// 					tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_hatienvegas').style.display='none';
// 				}
// 				if(document.getElementById('chkVIRTUA').checked==true){
// 					document.getElementById('dv_virtuavegas').style.display='block';
// 					tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_virtuavegas').style.display='none';
// 				}
				if(document.getElementById('chkSLOT').checked==true){
					document.getElementById('dv_slotvegas').style.display='block';
					tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_slotvegas').style.display='none';
				}
			}else{
// 				if(document.getElementById('chkHTV').checked==true){
// 					document.getElementById('dv_hatienvegas').style.display='block';
// 					tableHTVMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_hatienvegas').style.display='none';
// 				}

// 				if(document.getElementById('chkVIRTUA').checked==true){
// 					document.getElementById('dv_virtuavegas').style.display='block';
// 					tableVIRTUAMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
// 				}else{
// 					document.getElementById('dv_virtuavegas').style.display='none';
// 				}
				if(document.getElementById('chkSLOT').checked==true){
					document.getElementById('dv_slotvegas').style.display='block';
					tableSLOTMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_slotvegas').style.display='none';
				}
			}
		}
		
// 		document.getElementById('lblDateFrom_htv').innerHTML=dtFrom;
// 		document.getElementById('lblDateTo_htv').innerHTML=dtTo;
// 		document.getElementById('lblDateFrom_virtua').innerHTML=dtFrom;
// 		document.getElementById('lblDateTo_virtua').innerHTML=dtTo;
		document.getElementById('lblDateFrom_slot').innerHTML=dtFrom;
		document.getElementById('lblDateTo_slot').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(document.getElementById('lblAgentID_htv').innerHTML==''){
// 			document.getElementById('btnBack_htv').style.display='none';
// 			document.getElementById('btnBack_virtua').style.display='none';
			document.getElementById('btnBack_slot').style.display='none';
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
// 					document.getElementById('lblAgentID_htv').innerHTML=account_id;
// 					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					document.getElementById('lblAgentID_slot').innerHTML=account_id;
// 					document.getElementById('lblHTVTotalWinLossID').innerHTML=account_id;
// 					document.getElementById('lblVIRTUATotalWinLossID').innerHTML=account_id;
					document.getElementById('lblSLOTTotalWinLossID').innerHTML=account_id;
				}
			}
		}
// 		hideBettingDetails_htv();
// 		hideBettingDetails_virtua();
		hideBettingDetails_slot();
// 		if(readVirtuaSlotWinloss!=1){
// 			document.getElementById('dv_virtuavegas').style.display='none';
// 			document.getElementById('chkVIRTUA').checked==false;
// 			document.getElementById('chkVIRTUA').style.display='none';
// 			document.getElementById('lblchkVIRTUA').style.display='none';
// 		}
// 		if(readHatienSlotWinloss!=1){
// 			document.getElementById('dv_hatienvegas').style.display='none';
// 			document.getElementById('chkHTV').checked==false;
// 			document.getElementById('chkHTV').style.display='none';
// 			document.getElementById('lblchkHTV').style.display='none';
// 		}
		if(readSlotSlotWinloss!=1){
			document.getElementById('dv_slotvegas').style.display='none';
			document.getElementById('chkSLOT').checked==false;
			document.getElementById('chkSLOT').style.display='none';
			document.getElementById('lblchkSLOT').style.display='none';
		}
	}
	
	function backToParent_htv(accountID){
		var dtFrom=document.getElementById('lblDateFrom_htv').innerHTML;
		var dtTo=document.getElementById('lblDateTo_htv').innerHTML;
		var strCurrency= document.getElementById('cmbCurrency').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		tableHTVWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
		
		document.getElementById('lblDateFrom_htv').innerHTML=dtFrom;
		document.getElementById('lblDateTo_htv').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(accountID.length!=0){
			document.getElementById('lblAgentID_htv').innerHTML=account_id;
		}
		if(document.getElementById('lblAgentID_htv').innerHTML==''){
			document.getElementById('btnBack_htv').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnBack_htv').style.display='none';
				}
			}
		}
	}
	
	function backToParent_virtua(accountID){
		var dtFrom=document.getElementById('lblDateFrom_virtua').innerHTML;
		var dtTo=document.getElementById('lblDateTo_virtua').innerHTML;
		var strCurrency= document.getElementById('cmbCurrency').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		tableVIRTUAWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
		
		document.getElementById('lblDateFrom_virtua').innerHTML=dtFrom;
		document.getElementById('lblDateTo_virtua').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(accountID.length!=0){
			document.getElementById('lblAgentID_virtua').innerHTML=account_id;
		}
		if(document.getElementById('lblAgentID_virtua').innerHTML==''){
			document.getElementById('btnBack_virtua').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnBack_virtua').style.display='none';
				}
			}
		}
	}

	function backToParent_slot(accountID){
		var dtFrom=document.getElementById('lblDateFrom_slot').innerHTML;
		var dtTo=document.getElementById('lblDateTo_slot').innerHTML;
		var strCurrency= document.getElementById('cmbCurrency').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		tableSLOTWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
		
		document.getElementById('lblDateFrom_slot').innerHTML=dtFrom;
		document.getElementById('lblDateTo_slot').innerHTML=dtTo;
		document.getElementById('cmbGame').name=intGame;
		document.getElementById('cmbCurrency').name=strCurrency;
		if(accountID.length!=0){
			document.getElementById('lblAgentID_slot').innerHTML=account_id;
		}
		if(document.getElementById('lblAgentID_slot').innerHTML==''){
			document.getElementById('btnBack_slot').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnBack_slot').style.display='none';
				}
			}
		}
	}
	
	function showDetails_htv(account_id){
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var dtFrom=document.getElementById('lblDateFrom_htv').innerHTML;
		var dtTo=document.getElementById('lblDateTo_htv').innerHTML;
		var strCurrency= document.getElementById('cmbCurrency').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		if(account_id!=''){
			accountID=account_id;
		}else{
			if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
				accountID='';
			}else{
				accountID='<?php echo Yii::app()->session['account_id'];?>';
			}
		}

		jQuery("#lstHTV1").trigger("reloadGrid");//reload hatien winloss
		tableHTVWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
		jQuery("#lstTotal_htv").trigger("reloadGrid");//reload hatien winloss total
		
		
		if(accountID.length!=0){
			var myGrid = $('#lstHTV2');
			document.getElementById('lblDetailAgentID_htv').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_htv').innerHTML=dtFrom;
		document.getElementById('lblDetailDateTo_htv').innerHTML=dtTo;
		//hide back button
		if(document.getElementById('lblDetailAgentID_htv').innerHTML==''){
			document.getElementById('btnDetailBack_htv').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==accountID){
					document.getElementById('btnDetailBack_htv').style.display='none';
				}
			}
		}
	}
	
	function showDetails_virtua(account_id){
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var dtFrom=document.getElementById('lblDateFrom_virtua').innerHTML;
		var dtTo=document.getElementById('lblDateTo_virtua').innerHTML;
		var strCurrency= document.getElementById('cmbCurrency').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		if(account_id!=''){
			accountID=account_id;
		}else{
			if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
				accountID='';
			}else{
				accountID='<?php echo Yii::app()->session['account_id'];?>';
			}
		}

		jQuery("#lstVIRTUA1").trigger("reloadGrid");//reload virtua winloss
		tableVIRTUAWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
		jQuery("#lstTotal_virtua").trigger("reloadGrid");//reload virtua winloss total
		
		if(accountID.length!=0){
			var myGrid = $('#lstVIRTUA2');
			document.getElementById('lblDetailAgentID_virtua').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}
		}
		document.getElementById('lblDetailDateFrom_virtua').innerHTML=dtFrom;
		document.getElementById('lblDetailDateTo_virtua').innerHTML=dtTo;
		//hide back button
		if(document.getElementById('lblDetailAgentID_virtua').innerHTML==''){
			document.getElementById('btnDetailBack_virtua').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==accountID){
					document.getElementById('btnDetailBack_virtua').style.display='none';
				}
			}
		}
	}

	function showDetails_slot(account_id){
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var dtFrom=document.getElementById('lblDateFrom_slot').innerHTML;
		var dtTo=document.getElementById('lblDateTo_slot').innerHTML;
		var strCurrency= document.getElementById('cmbCurrency').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		if(account_id!=''){
			accountID=account_id;
		}else{
			if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
				accountID='';
			}else{
				accountID='<?php echo Yii::app()->session['account_id'];?>';
			}
		}

		jQuery("#lstSLOT1").trigger("reloadGrid");//reload savan winloss
		tableSLOTWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
		jQuery("#lstTotal_slot").trigger("reloadGrid");//reload savan winloss total
		
		if(accountID.length!=0){
			var myGrid = $('#lstSLOT2');
			document.getElementById('lblDetailAgentID_slot').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_slot').innerHTML=dtFrom;
		document.getElementById('lblDetailDateTo_slot').innerHTML=dtTo;
		//hide back button
		if(document.getElementById('lblDetailAgentID_slot').innerHTML==''){
			document.getElementById('btnDetailBack_slot').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==accountID){
					document.getElementById('btnDetailBack_slot').style.display='none';
				}
			}
		}
	}
	
	function backToParentDetail_htv(accountID){
		var dtFrom=document.getElementById('lblDetailDateFrom_htv').innerHTML;
		var dtTo=document.getElementById('lblDetailDateTo_htv').innerHTML;
		var strCurrency= document.getElementById('cmbGame').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		tableHTVWinLossDetails(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
		if(account_id.length!=0){
			var myGrid = $('#lstHTV2');
			document.getElementById('lblDetailAgentID_htv').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_htv').innerHTML=dtFrom;
		document.getElementById('lblDetailDateTo_htv').innerHTML=dtTo;
		if(accountID.length!=0){
			document.getElementById('lblDetailAgentID_htv').innerHTML=account_id;
		}
		if(document.getElementById('lblDetailAgentID_htv').innerHTML==''){
			document.getElementById('btnDetailBack_htv').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnDetailBack_htv').style.display='none';
				}
			}
		}
	}
	function backToParentDetail_savan(accountID){
		var dtFrom=document.getElementById('lblDetailDateFrom_savan').innerHTML;
		var dtTo=document.getElementById('lblDetailDateTo_savan').innerHTML;
		var strCurrency= document.getElementById('cmbGame').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		tableSAVANWinLossDetails(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
		if(account_id.length!=0){
			var myGrid = $('#lstSAVAN2');
			document.getElementById('lblDetailAgentID_savan').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_savan').innerHTML=dtFrom;
		document.getElementById('lblDetailDateTo_savan').innerHTML=dtTo;
		if(accountID.length!=0){
			document.getElementById('lblDetailAgentID_savan').innerHTML=account_id;
		}
		if(document.getElementById('lblDetailAgentID_savan').innerHTML==''){
			document.getElementById('btnDetailBack_savan').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnDetailBack_savan').style.display='none';
				}
			}
		}
	}
	function backToParentDetail_virtua(accountID){
		var dtFrom=document.getElementById('lblDetailDateFrom_virtua').innerHTML;
		var dtTo=document.getElementById('lblDetailDateTo_virtua').innerHTML;
		var strCurrency= document.getElementById('cmbGame').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		tableVIRTUAWinLossDetails(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
		if(account_id.length!=0){
			var myGrid = $('#lstVIRTUA2');
			document.getElementById('lblDetailAgentID_virtua').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}
		}
		document.getElementById('lblDetailDateFrom_virtua').innerHTML=dtFrom;
		document.getElementById('lblDetailDateTo_virtua').innerHTML=dtTo;
		if(accountID.length!=0){
			document.getElementById('lblDetailAgentID_virtua').innerHTML=account_id;
		}
		if(document.getElementById('lblDetailAgentID_virtua').innerHTML==''){
			document.getElementById('btnDetailBack_virtua').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnDetailBack_virtua').style.display='none';
				}
			}
		}
	}

	function backToParentDetail_slot(accountID){
		var dtFrom=document.getElementById('lblDetailDateFrom_slot').innerHTML;
		var dtTo=document.getElementById('lblDetailDateTo_slot').innerHTML;
		var strCurrency= document.getElementById('cmbGame').name;
		var intGame=document.getElementById('cmbGame').name;
		var testCurrency=0;
		if(document.getElementById('chkTestCurrency').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		tableSLOTWinLossDetails(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
		if(account_id.length!=0){
			var myGrid = $('#lstSLOT2');
			document.getElementById('lblDetailAgentID_slot').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_slot').innerHTML=dtFrom;
		document.getElementById('lblDetailDateTo_slot').innerHTML=dtTo;
		if(accountID.length!=0){
			document.getElementById('lblDetailAgentID_slot').innerHTML=account_id;
		}
		if(document.getElementById('lblDetailAgentID_slot').innerHTML==''){
			document.getElementById('btnDetailBack_slot').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnDetailBack_slot').style.display='none';
				}
			}
		}
	}
	function hideBettingDetails_htv(){
		//start: show parent grids
	    document.getElementById('qry_htv999_result').style.display='block';
	    document.getElementById('qry_htv999_result_details').style.display='block';
	    //end: show parent grids
	    //start: clear betting details
	    document.getElementById('qry_htv999_betting_details').innerHTML='';
	    document.getElementById('qry_htv999_betting_details_summary_footer').style.display='none';
	    //end: clear betting details
	}
	function hideBettingDetails_savan(){
		//start: show parent grids
	    document.getElementById('qry_savan_vegas_result').style.display='block';
	    document.getElementById('qry_savan_vegas_result_details').style.display='block';
	    //end: show parent grids
	    //start: clear betting details
	    document.getElementById('qry_savan_vegas_betting_details').innerHTML='';
	    document.getElementById('qry_savan_vegas_betting_details_summary_footer').style.display='none';
	    //end: clear betting details
	}
	function hideBettingDetails_virtua(){
		//start: show parent grids
	    document.getElementById('qry_virtua_vegas_result').style.display='block';
	    document.getElementById('qry_virtua_vegas_result_details').style.display='block';
	    //end: show parent grids
	    //start: clear betting details
	    document.getElementById('qry_virtua_vegas_betting_details').innerHTML='';
	    document.getElementById('qry_virtua_vegas_betting_details_summary_footer').style.display='none';
	    //end: clear betting details
	}
	function hideBettingDetails_slot(){
		//start: show parent grids
	    document.getElementById('qry_slot_vegas_result').style.display='block';
	    document.getElementById('qry_slot_vegas_result_details').style.display='block';
	    //end: show parent grids
	    //start: clear betting details
	    document.getElementById('qry_slot_vegas_betting_details').innerHTML='';
	    document.getElementById('qry_slot_vegas_betting_details_summary_footer').style.display='none';
	    //end: clear betting details
	}
	function clearContainer(){
		//document.getElementById('qry_htv999_result_details').innerHTML='';
		//document.getElementById('qry_savan_vegas_result_details').innerHTML='';
		//document.getElementById('qry_virtua_vegas_result_details').innerHTML='';
		document.getElementById('qry_slot_vegas_result_details').innerHTML='';
	}
	function htvTotalConversion(currency){
		var grid = jQuery("#lstTotal_htv");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstTotal_htv").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var company=grid.getCell(x, 'company');
		    	var total=grid.getCell(x, 'total');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 1, fltRate[0]);
		    	grid.jqGrid('setCell', x, 2, total_stake_convert);
		    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 4, win_loss_convert);
		    	grid.jqGrid('setCell', x, 5, commission_convert);
		    	grid.jqGrid('setCell', x, 6, bonus_convert);
		    	grid.jqGrid('setCell', x, 7, total_convert);
		    	grid.jqGrid('setCell', x, 8, company_convert);
		    	grid.jqGrid('setCell', x, 9, fltRate[1]);
		    	
		     	x++;
		    }

		    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
		    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
		    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
		    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
		    var bonus_sum=grid.jqGrid('getCol', 'bonus', false, 'sum');
		    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
		    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
	        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,bonus: bonus_sum,total: total_sum,company: company_sum});
		}else{
			grid.trigger("reloadGrid");
			grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',bonus:'',total: '',company: ''});
		}
	}
	function htvWinLossConversion(currency){
		var grid = jQuery("#lstHTV1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstHTV1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	//var bet_count=grid.getCell(x, 'bet_count');
		    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var total=grid.getCell(x, 'total');
		    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
		    	var mc_comm=grid.getCell(x, 'mc_comm');
		    	var mc_bonus=grid.getCell(x, 'mc_bonus');
		    	var mc_total=grid.getCell(x, 'mc_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_bonus_convert=((parseFloat(mc_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, total_convert);
		    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
		    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
		    	grid.jqGrid('setCell', x, 12, mc_bonus_convert);
		    	grid.jqGrid('setCell', x, 13, mc_total_convert);
		    	grid.jqGrid('setCell', x, 14, company_convert);
		    	grid.jqGrid('setCell', x, 15, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("htv_sum_data");
		    var i=0;
		    var xrate=9;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=10;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
		
		
	}
	function htvMemberWinLossConversion(currency){
		var grid = jQuery("#lstHTV1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstHTV1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	
		    	var average_bet=grid.getCell(x, 'average_bet');
  		    	var total_stake=grid.getCell(x, 'total_stake');
  		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var balance=grid.getCell(x, 'balance');
		    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
		    	var agt_comm=grid.getCell(x, 'agt_comm');
		    	var agt_bonus=grid.getCell(x, 'agt_bonus');
		    	var agt_total=grid.getCell(x, 'agt_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_bonus_convert=((parseFloat(agt_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, win_loss_convert);
		    	grid.jqGrid('setCell', x, 10, commission_convert);
		    	grid.jqGrid('setCell', x, 11, bonus_convert);
		    	grid.jqGrid('setCell', x, 12, total_convert);
		    	grid.jqGrid('setCell', x, 13, balance_convert);
		    	grid.jqGrid('setCell', x, 14, agt_win_loss_convert);
		    	grid.jqGrid('setCell', x, 15, agt_comm_convert);
		    	grid.jqGrid('setCell', x, 16, agt_bonus_convert);
		    	grid.jqGrid('setCell', x, 17, agt_total_convert);
		    	grid.jqGrid('setCell', x, 18, company_convert);
		    	grid.jqGrid('setCell', x, 19, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("htv_sum_data");
		    var i=0;
		    var xrate=12;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=13;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
	}
	function savanTotalConversion(currency){
		var grid = jQuery("#lstTotal_savan");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstTotal_savan").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var company=grid.getCell(x, 'company');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 1, fltRate[0]);
		    	grid.jqGrid('setCell', x, 2, total_stake_convert);
		    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 4, win_loss_convert);
		    	grid.jqGrid('setCell', x, 5, commission_convert);
		    	grid.jqGrid('setCell', x, 6, bonus_convert);
		    	grid.jqGrid('setCell', x, 7, total_convert);
		    	grid.jqGrid('setCell', x, 8, company_convert);
		    	grid.jqGrid('setCell', x, 9, fltRate[1]);
		    	
		     	x++;
		    }

		    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
		    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
		    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
		    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
		    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
		    var bonus_sum=grid.jqGrid('getCol', 'bonus', false, 'sum');
		    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
	        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum, company: company_sum, bonus: bonus_sum});
		}else{
			grid.trigger("reloadGrid");
			grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',bonus: '',total: '',company: ''});
		}
	}
	function savanWinLossConversion(currency){
		var grid = jQuery("#lstSAVAN1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstSAVAN1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
  		      
		    	//var bet_count=grid.getCell(x, 'bet_count');
		    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var total=grid.getCell(x, 'total');
		    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
		    	var mc_comm=grid.getCell(x, 'mc_comm');
		    	var mc_bonus=grid.getCell(x, 'mc_bonus');
		    	var mc_total=grid.getCell(x, 'mc_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_bonus_convert=((parseFloat(mc_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, total_convert);
		    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
		    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
		    	grid.jqGrid('setCell', x, 12, mc_bonus_convert);
		    	grid.jqGrid('setCell', x, 13, mc_total_convert);
		    	grid.jqGrid('setCell', x, 14, company_convert);
		    	grid.jqGrid('setCell', x, 15, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("savan_sum_data");
		    var i=0;
		    var xrate=9;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=10;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
		
		
	}
	function savanMemberWinLossConversion(currency){
		var grid = jQuery("#lstSAVAN1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstSAVAN1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	
		    	var average_bet=grid.getCell(x, 'average_bet');
  		    	var total_stake=grid.getCell(x, 'total_stake');
  		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var balance=grid.getCell(x, 'balance');
		    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
		    	var agt_comm=grid.getCell(x, 'agt_comm');
		    	var agt_bonus=grid.getCell(x, 'agt_bonus');
		    	var agt_total=grid.getCell(x, 'agt_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_bonus_convert=((parseFloat(agt_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, win_loss_convert);
		    	grid.jqGrid('setCell', x, 10, commission_convert);
		    	grid.jqGrid('setCell', x, 11, bonus_convert);
		    	grid.jqGrid('setCell', x, 12, total_convert);
		    	grid.jqGrid('setCell', x, 13, balance_convert);
		    	grid.jqGrid('setCell', x, 14, agt_win_loss_convert);
		    	grid.jqGrid('setCell', x, 15, agt_comm_convert);
		    	grid.jqGrid('setCell', x, 16, agt_bonus_convert);
		    	grid.jqGrid('setCell', x, 17, agt_total_convert);
		    	grid.jqGrid('setCell', x, 18, company_convert);
		    	grid.jqGrid('setCell', x, 19, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("savan_sum_data");
		    var i=0;
		    var xrate=12;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=13;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
	}

	//---------------------
	function virtuaTotalConversion(currency){
		var grid = jQuery("#lstTotal_virtua");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstTotal_virtua").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var company=grid.getCell(x, 'company');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 1, fltRate[0]);
		    	grid.jqGrid('setCell', x, 2, total_stake_convert);
		    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 4, win_loss_convert);
		    	grid.jqGrid('setCell', x, 5, commission_convert);
		    	grid.jqGrid('setCell', x, 6, bonus_convert);
		    	grid.jqGrid('setCell', x, 7, total_convert);
		    	grid.jqGrid('setCell', x, 8, company_convert);
		    	grid.jqGrid('setCell', x, 9, fltRate[1]);
		    	
		     	x++;
		    }

		    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
		    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
		    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
		    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
		    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
		    var bonus_sum=grid.jqGrid('getCol', 'bonus', false, 'sum');
		    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
	        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum, company: company_sum, bonus: bonus_sum});
		}else{
			grid.trigger("reloadGrid");
			grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',bonus: '',total: '',company: ''});
		}
	}
	function virtuaWinLossConversion(currency){
		var grid = jQuery("#lstVIRTUA1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstVIRTUA1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
  		      
		    	//var bet_count=grid.getCell(x, 'bet_count');
		    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var total=grid.getCell(x, 'total');
		    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
		    	var mc_comm=grid.getCell(x, 'mc_comm');
		    	var mc_bonus=grid.getCell(x, 'mc_bonus');
		    	var mc_total=grid.getCell(x, 'mc_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_bonus_convert=((parseFloat(mc_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, total_convert);
		    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
		    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
		    	grid.jqGrid('setCell', x, 12, mc_bonus_convert);
		    	grid.jqGrid('setCell', x, 13, mc_total_convert);
		    	grid.jqGrid('setCell', x, 14, company_convert);
		    	grid.jqGrid('setCell', x, 15, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("virtua_sum_data");
		    var i=0;
		    var xrate=9;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=10;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
		
		
	}
	function virtuaMemberWinLossConversion(currency){
		var grid = jQuery("#lstVIRTUA1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstVIRTUA1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	
		    	var average_bet=grid.getCell(x, 'average_bet');
  		    	var total_stake=grid.getCell(x, 'total_stake');
  		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var balance=grid.getCell(x, 'balance');
		    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
		    	var agt_comm=grid.getCell(x, 'agt_comm');
		    	var agt_total=grid.getCell(x, 'agt_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, win_loss_convert);
		    	grid.jqGrid('setCell', x, 10, commission_convert);
		    	grid.jqGrid('setCell', x, 11, bonus_convert);
		    	grid.jqGrid('setCell', x, 12, total_convert);
		    	grid.jqGrid('setCell', x, 13, balance_convert);
		    	grid.jqGrid('setCell', x, 14, agt_win_loss_convert);
		    	grid.jqGrid('setCell', x, 15, agt_comm_convert);
		    	grid.jqGrid('setCell', x, 16, agt_total_convert);
		    	grid.jqGrid('setCell', x, 17, company_convert);
		    	grid.jqGrid('setCell', x, 18, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("virtua_sum_data");
		    var i=0;
		    var xrate=12;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=13;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
	}

	function slotTotalConversion(currency){
		var grid = jQuery("#lstTotal_slot");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstTotal_slot").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var company=grid.getCell(x, 'company');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 1, fltRate[0]);
		    	grid.jqGrid('setCell', x, 2, total_stake_convert);
		    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 4, win_loss_convert);
		    	grid.jqGrid('setCell', x, 5, commission_convert);
		    	grid.jqGrid('setCell', x, 6, bonus_convert);
		    	grid.jqGrid('setCell', x, 7, total_convert);
		    	grid.jqGrid('setCell', x, 8, company_convert);
		    	grid.jqGrid('setCell', x, 9, fltRate[1]);
		    	
		     	x++;
		    }

		    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
		    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
		    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
		    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
		    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
		    var bonus_sum=grid.jqGrid('getCol', 'bonus', false, 'sum');
		    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
	        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum, company: company_sum, bonus: bonus_sum});
		}else{
			grid.trigger("reloadGrid");
			grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',bonus: '',total: '',company: ''});
		}
	}
	function slotWinLossConversion(currency){
		var grid = jQuery("#lstSLOT1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstSLOT1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
  		      
		    	//var bet_count=grid.getCell(x, 'bet_count');
		    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var total=grid.getCell(x, 'total');
		    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
		    	var mc_comm=grid.getCell(x, 'mc_comm');
		    	var mc_bonus=grid.getCell(x, 'mc_bonus');
		    	var mc_total=grid.getCell(x, 'mc_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_bonus_convert=((parseFloat(mc_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, total_convert);
		    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
		    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
		    	grid.jqGrid('setCell', x, 12, mc_bonus_convert);
		    	grid.jqGrid('setCell', x, 13, mc_total_convert);
		    	grid.jqGrid('setCell', x, 14, company_convert);
		    	grid.jqGrid('setCell', x, 15, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("slot_sum_data");
		    var i=0;
		    var xrate=9;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=10;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
		
		
	}
	function slotMemberWinLossConversion(currency){
		var grid = jQuery("#lstSLOT1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#lstSLOT1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	
		    	var average_bet=grid.getCell(x, 'average_bet');
  		    	var total_stake=grid.getCell(x, 'total_stake');
  		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var balance=grid.getCell(x, 'balance');
		    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
		    	var agt_comm=grid.getCell(x, 'agt_comm');
		    	var agt_bonus=grid.getCell(x, 'agt_bonus');
		    	var agt_total=grid.getCell(x, 'agt_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_bonus_convert=((parseFloat(agt_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, win_loss_convert);
		    	grid.jqGrid('setCell', x, 10, commission_convert);
		    	grid.jqGrid('setCell', x, 11, bonus_convert);
		    	grid.jqGrid('setCell', x, 12, total_convert);
		    	grid.jqGrid('setCell', x, 13, balance_convert);
		    	grid.jqGrid('setCell', x, 14, agt_win_loss_convert);
		    	grid.jqGrid('setCell', x, 15, agt_comm_convert);
		    	grid.jqGrid('setCell', x, 16, agt_bonus_convert);
		    	grid.jqGrid('setCell', x, 17, agt_total_convert);
		    	grid.jqGrid('setCell', x, 18, company_convert);
		    	grid.jqGrid('setCell', x, 19, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("slot_sum_data");
		    var i=0;
		    var xrate=12;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=13;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
	}
	</script>
	<script>
	function showHTVBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency){
		document.getElementById('lblHTVBettingSummaryPlayerID').innerHTML=strAccountID;
		document.getElementById('qry_htv999_betting_details_summary_footer').style.display='block';
		//Current Page
		var grid = jQuery("#list5");
		var noRow=$("#list5").getGridParam("reccount");
	    var x=1;
	    var bet_amount=0;
    	var win_loss=0;
    	var commission=0;
    	var bonus=0;
    	var total=0;
    	var balance=0;
	    while (x<=noRow)
	    {
	    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
	    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
	    	commission+=parseFloat(grid.getCell(x, 'commission'));
	    	bonus+=parseFloat(grid.getCell(x, 'bonus'));
	    	total+=parseFloat(grid.getCell(x, 'total'));
	    	if(x==1){
	    		balance+=parseFloat(grid.getCell(x, 'balance'));
	    	}
	     	x++;
	    }
	    document.getElementById('lblCPNoOfBet_htv').innerHTML=x-1;
	    document.getElementById('lblCPBetAmount_htv').innerHTML=parseFloat(bet_amount).toFixed(2);
	    document.getElementById('lblCPWinLoss_htv').innerHTML=parseFloat(win_loss).toFixed(2);
	    document.getElementById('lblCPCommission_htv').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('lblCPBonus_htv').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('lblCPTotal_htv').innerHTML=parseFloat(total).toFixed(2);
	    document.getElementById('lblCPBalance_htv').innerHTML=parseFloat(balance).toFixed(2);
	    if(parseFloat(bet_amount)<0){document.getElementById('lblCPBetAmount_htv').style.color='red';}else{document.getElementById('lblCPBetAmount_htv').style.color='black';}
	    if(parseFloat(win_loss)<0){document.getElementById('lblCPWinLoss_htv').style.color='red';}else{document.getElementById('lblCPWinLoss_htv').style.color='black';}
	    if(parseFloat(commission)<0){document.getElementById('lblCPCommission_htv').style.color='red';}else{document.getElementById('lblCPCommission_htv').style.color='black';}
	    if(parseFloat(bonus)<0){document.getElementById('lblCPBonus_htv').style.color='red';}else{document.getElementById('lblCPBonus_htv').style.color='black';}
	    if(parseFloat(total)<0){document.getElementById('lblCPTotal_htv').style.color='red';}else{document.getElementById('lblCPTotal_htv').style.color='black';}
	    if(parseFloat(balance)<0){document.getElementById('lblCPBalance_htv').style.color='red';}else{document.getElementById('lblCPBalance_htv').style.color='black';}
		//All Pages
		
	    jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTV999WinLossDetailsSummaryFooter&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				var fltTotal = data.split(';');
				document.getElementById('lblAPNoOfBet_htv').innerHTML=fltTotal[0];
			    document.getElementById('lblAPBetAmount_htv').innerHTML=fltTotal[1];
			    document.getElementById('lblAPWinLoss_htv').innerHTML=fltTotal[2];
			    document.getElementById('lblAPCommission_htv').innerHTML=fltTotal[3];
			    document.getElementById('lblAPBonus_htv').innerHTML=fltTotal[4];
			    document.getElementById('lblAPTotal_htv').innerHTML=fltTotal[5];
			    document.getElementById('lblAPBalance_htv').innerHTML=fltTotal[6];
			    if(parseFloat(fltTotal[1])<0){document.getElementById('lblAPBetAmount_htv').style.color='red';}else{document.getElementById('lblAPBetAmount_htv').style.color='black';}
			    if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPWinLoss_htv').style.color='red';}else{document.getElementById('lblAPWinLoss_htv').style.color='black';}
			    if(parseFloat(fltTotal[3])<0){document.getElementById('lblAPCommission_htv').style.color='red';}else{document.getElementById('lblAPCommission_htv').style.color='black';}
			    if(parseFloat(fltTotal[4])<0){document.getElementById('lblAPBonus_htv').style.color='red';}else{document.getElementById('lblAPBonus_htv').style.color='black';}
			    if(parseFloat(fltTotal[5])<0){document.getElementById('lblAPTotal_htv').style.color='red';}else{document.getElementById('lblAPTotal_htv').style.color='black';}
			    if(parseFloat(fltTotal[6])<0){document.getElementById('lblAPBalance_htv').style.color='red';}else{document.getElementById('lblAPBalance_htv').style.color='black';}
	    	}
		});
	}
	function showSAVANBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency){
		document.getElementById('lblSAVANBettingSummaryPlayerID').innerHTML=strAccountID;
		document.getElementById('qry_savan_vegas_betting_details_summary_footer').style.display='block';
		//Current Page
		var grid = jQuery("#list6");
		var noRow=$("#list6").getGridParam("reccount");
	    var x=1;
	    var bet_amount=0;
    	var win_loss=0;
    	var commission=0;
    	var bonus=0;
    	var total=0;
    	var balance=0;
	    while (x<=noRow)
	    {
	    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
	    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
	    	commission+=parseFloat(grid.getCell(x, 'commission'));
	    	bonus+=parseFloat(grid.getCell(x, 'bonus'));
	    	total+=parseFloat(grid.getCell(x, 'total'));
	    	if(x==1){
	    		balance+=parseFloat(grid.getCell(x, 'balance'));
	    	}
	     	x++;
	    }
	    document.getElementById('lblCPNoOfBet_savan').innerHTML=x-1;
	    document.getElementById('lblCPBetAmount_savan').innerHTML=parseFloat(bet_amount).toFixed(2);
	    document.getElementById('lblCPWinLoss_savan').innerHTML=parseFloat(win_loss).toFixed(2);
	    document.getElementById('lblCPCommission_savan').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('lblCPBonus_savan').innerHTML=parseFloat(bonus).toFixed(2);
	    document.getElementById('lblCPTotal_savan').innerHTML=parseFloat(total).toFixed(2);
	    document.getElementById('lblCPBalance_savan').innerHTML=parseFloat(balance).toFixed(2);
	    if(parseFloat(bet_amount)<0){document.getElementById('lblCPBetAmount_savan').style.color='red';}else{document.getElementById('lblCPBetAmount_savan').style.color='black';}
	    //if(parseFloat(amount_wagers)<0){document.getElementById('lblCPValidBet_savan').style.color='red';}else{document.getElementById('lblCPValidBet_savan').style.color='black';}
	    if(parseFloat(win_loss)<0){document.getElementById('lblCPWinLoss_savan').style.color='red';}else{document.getElementById('lblCPWinLoss_savan').style.color='black';}
	    if(parseFloat(commission)<0){document.getElementById('lblCPCommission_savan').style.color='red';}else{document.getElementById('lblCPCommission_savan').style.color='black';}
	    if(parseFloat(bonus)<0){document.getElementById('lblCPBonus_savan').style.color='red';}else{document.getElementById('lblCPBonus_savan').style.color='black';}
	    if(parseFloat(total)<0){document.getElementById('lblCPTotal_savan').style.color='red';}else{document.getElementById('lblCPTotal_savan').style.color='black';}
	    if(parseFloat(balance)<0){document.getElementById('lblCPBalance_savan').style.color='red';}else{document.getElementById('lblCPBalance_savan').style.color='black';}
		//All Pages
		
	    jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANWinLossBettingDetailsSummaryFooter&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				var fltTotal = data.split(';');
				document.getElementById('lblAPNoOfBet_savan').innerHTML=fltTotal[0];
			    document.getElementById('lblAPBetAmount_savan').innerHTML=fltTotal[1];
			    document.getElementById('lblAPWinLoss_savan').innerHTML=fltTotal[2];
			    document.getElementById('lblAPCommission_savan').innerHTML=fltTotal[3];
			    document.getElementById('lblAPBonus_savan').innerHTML=fltTotal[4];
			    document.getElementById('lblAPTotal_savan').innerHTML=fltTotal[5];
			    document.getElementById('lblAPBalance_savan').innerHTML=fltTotal[6];
			    if(parseFloat(fltTotal[1])<0){document.getElementById('lblAPBetAmount_savan').style.color='red';}else{document.getElementById('lblAPBetAmount_savan').style.color='black';}
			    //if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPValidBet_savan').style.color='red';}else{document.getElementById('lblAPValidBet_savan').style.color='black';}
			    if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPWinLoss_savan').style.color='red';}else{document.getElementById('lblAPWinLoss_savan').style.color='black';}
			    if(parseFloat(fltTotal[3])<0){document.getElementById('lblAPCommission_savan').style.color='red';}else{document.getElementById('lblAPCommission_savan').style.color='black';}
			    if(parseFloat(fltTotal[4])<0){document.getElementById('lblAPBonus_savan').style.color='red';}else{document.getElementById('lblAPBonus_savan').style.color='black';}
			    if(parseFloat(fltTotal[5])<0){document.getElementById('lblAPTotal_savan').style.color='red';}else{document.getElementById('lblAPTotal_savan').style.color='black';}
			    if(parseFloat(fltTotal[6])<0){document.getElementById('lblAPBalance_savan').style.color='red';}else{document.getElementById('lblAPBalance_savan').style.color='black';}
	    	}
		});
	}
	function showVIRTUABettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency){
		document.getElementById('lblVIRTUABettingSummaryPlayerID').innerHTML=strAccountID;
		document.getElementById('qry_virtua_vegas_betting_details_summary_footer').style.display='block';
		//Current Page
		var grid = jQuery("#list6");
		var noRow=$("#list6").getGridParam("reccount");
	    var x=1;
	    var bet_amount=0;
    	var win_loss=0;
    	var commission=0;
    	var bonus=0;
    	var total=0;
    	var balance=0;
	    while (x<=noRow)
	    {
	    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
	    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
	    	commission+=parseFloat(grid.getCell(x, 'commission'));
	    	bonus+=parseFloat(grid.getCell(x, 'bonus'));
	    	total+=parseFloat(grid.getCell(x, 'total'));
	    	if(x==1){
	    		balance+=parseFloat(grid.getCell(x, 'balance'));
	    	}
	     	x++;
	    }
	    document.getElementById('lblCPNoOfBet_virtua').innerHTML=x-1;
	    document.getElementById('lblCPBetAmount_virtua').innerHTML=parseFloat(bet_amount).toFixed(2);
	    document.getElementById('lblCPWinLoss_virtua').innerHTML=parseFloat(win_loss).toFixed(2);
	    document.getElementById('lblCPCommission_virtua').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('lblCPBonus_virtua').innerHTML=parseFloat(bonus).toFixed(2);
	    document.getElementById('lblCPTotal_virtua').innerHTML=parseFloat(total).toFixed(2);
	    //document.getElementById('lblCPBalance_virtua').innerHTML=parseFloat(balance).toFixed(2);
	    document.getElementById('lblCPBalance_virtua').innerHTML='N/A';
	    //document.getElementById('lblAPBalance_virtua').innerHTML=parseFloat(balance).toFixed(2);
	    if(parseFloat(bet_amount)<0){document.getElementById('lblCPBetAmount_virtua').style.color='red';}else{document.getElementById('lblCPBetAmount_virtua').style.color='black';}
	    //if(parseFloat(amount_wagers)<0){document.getElementById('lblCPValidBet_savan').style.color='red';}else{document.getElementById('lblCPValidBet_savan').style.color='black';}
	    if(parseFloat(win_loss)<0){document.getElementById('lblCPWinLoss_virtua').style.color='red';}else{document.getElementById('lblCPWinLoss_virtua').style.color='black';}
	    if(parseFloat(commission)<0){document.getElementById('lblCPCommission_virtua').style.color='red';}else{document.getElementById('lblCPCommission_virtua').style.color='black';}
	    if(parseFloat(bonus)<0){document.getElementById('lblCPBonus_virtua').style.color='red';}else{document.getElementById('lblCPBonus_virtua').style.color='black';}
	    if(parseFloat(total)<0){document.getElementById('lblCPTotal_virtua').style.color='red';}else{document.getElementById('lblCPTotal_virtua').style.color='black';}
	    //if(parseFloat(balance)<0){document.getElementById('lblCPBalance_virtua').style.color='red';}else{document.getElementById('lblCPBalance_virtua').style.color='black';}
	    //if(parseFloat(balance)<0){document.getElementById('lblAPBalance_virtua').style.color='red';}else{document.getElementById('lblAPBalance_virtua').style.color='black';}
		//All Pages
		
	    jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUAWinLossBettingDetailsSummaryFooter&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				var fltTotal = data.split(';');
				document.getElementById('lblAPNoOfBet_virtua').innerHTML=fltTotal[0];
			    document.getElementById('lblAPBetAmount_virtua').innerHTML=fltTotal[1];
			    document.getElementById('lblAPWinLoss_virtua').innerHTML=fltTotal[2];
			    document.getElementById('lblAPCommission_virtua').innerHTML=fltTotal[3];
			    document.getElementById('lblAPBonus_virtua').innerHTML=fltTotal[4];
			    document.getElementById('lblAPTotal_virtua').innerHTML=fltTotal[5];
			    document.getElementById('lblAPBalance_virtua').innerHTML= (fltTotal[6] != '') ? fltTotal[6] : '0.00';
			    if(parseFloat(fltTotal[1])<0){document.getElementById('lblAPBetAmount_virtua').style.color='red';}else{document.getElementById('lblAPBetAmount_virtua').style.color='black';}
			    //if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPValidBet_savan').style.color='red';}else{document.getElementById('lblAPValidBet_savan').style.color='black';}
			    if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPWinLoss_virtua').style.color='red';}else{document.getElementById('lblAPWinLoss_virtua').style.color='black';}
			    if(parseFloat(fltTotal[3])<0){document.getElementById('lblAPCommission_virtua').style.color='red';}else{document.getElementById('lblAPCommission_virtua').style.color='black';}
			    if(parseFloat(fltTotal[4])<0){document.getElementById('lblAPBonus_virtua').style.color='red';}else{document.getElementById('lblAPBonus_virtua').style.color='black';}
			    if(parseFloat(fltTotal[5])<0){document.getElementById('lblAPTotal_virtua').style.color='red';}else{document.getElementById('lblAPTotal_virtua').style.color='black';}
			    if(parseFloat(fltTotal[6])<0){document.getElementById('lblAPBalance_virtua').style.color='red';}else{document.getElementById('lblAPBalance_virtua').style.color='black';}
	    	}
		});
	}

	function showSLOTBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency){
		document.getElementById('lblSLOTBettingSummaryPlayerID').innerHTML=strAccountID;
		document.getElementById('qry_slot_vegas_betting_details_summary_footer').style.display='block';
		//Current Page
		var grid = jQuery("#list6");
		var noRow=$("#list6").getGridParam("reccount");
	    var x=1;
	    var bet_amount=0;
    	var win_loss=0;
    	var commission=0;
    	var bonus=0;
    	var total=0;
    	var balance=0;
	    while (x<=noRow)
	    {
	    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
	    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
	    	commission+=parseFloat(grid.getCell(x, 'commission'));
	    	bonus+=parseFloat(grid.getCell(x, 'bonus'));
	    	total+=parseFloat(grid.getCell(x, 'total'));
	    	if(x==1){
	    		balance+=parseFloat(grid.getCell(x, 'balance'));
	    	}
	     	x++;
	    }
	    document.getElementById('lblCPNoOfBet_slot').innerHTML=x-1;
	    document.getElementById('lblCPBetAmount_slot').innerHTML=parseFloat(bet_amount).toFixed(2);
	    document.getElementById('lblCPWinLoss_slot').innerHTML=parseFloat(win_loss).toFixed(2);
	    document.getElementById('lblCPCommission_slot').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('lblCPBonus_slot').innerHTML=parseFloat(bonus).toFixed(2);
	    document.getElementById('lblCPTotal_slot').innerHTML=parseFloat(total).toFixed(2);
	    document.getElementById('lblCPBalance_slot').innerHTML=parseFloat(balance).toFixed(2);
	    if(parseFloat(bet_amount)<0){document.getElementById('lblCPBetAmount_slot').style.color='red';}else{document.getElementById('lblCPBetAmount_slot').style.color='black';}
	    //if(parseFloat(amount_wagers)<0){document.getElementById('lblCPValidBet_savan').style.color='red';}else{document.getElementById('lblCPValidBet_savan').style.color='black';}
	    if(parseFloat(win_loss)<0){document.getElementById('lblCPWinLoss_slot').style.color='red';}else{document.getElementById('lblCPWinLoss_slot').style.color='black';}
	    if(parseFloat(commission)<0){document.getElementById('lblCPCommission_slot').style.color='red';}else{document.getElementById('lblCPCommission_slot').style.color='black';}
	    if(parseFloat(bonus)<0){document.getElementById('lblCPBonus_slot').style.color='red';}else{document.getElementById('lblCPBonus_slot').style.color='black';}
	    if(parseFloat(total)<0){document.getElementById('lblCPTotal_slot').style.color='red';}else{document.getElementById('lblCPTotal_slot').style.color='black';}
	    if(parseFloat(balance)<0){document.getElementById('lblCPBalance_slot').style.color='red';}else{document.getElementById('lblCPBalance_slot').style.color='black';}
		//All Pages
		
	    jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SLOTWinLossBettingDetailsSummaryFooter&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				var fltTotal = data.split(';');
				document.getElementById('lblAPNoOfBet_slot').innerHTML=fltTotal[0];
			    document.getElementById('lblAPBetAmount_slot').innerHTML=fltTotal[1];
			    document.getElementById('lblAPWinLoss_slot').innerHTML=fltTotal[2];
			    document.getElementById('lblAPCommission_slot').innerHTML=fltTotal[3];
			    document.getElementById('lblAPBonus_slot').innerHTML=fltTotal[4];
			    document.getElementById('lblAPTotal_slot').innerHTML=fltTotal[5];
			    document.getElementById('lblAPBalance_slot').innerHTML=fltTotal[6];
			    if(parseFloat(fltTotal[1])<0){document.getElementById('lblAPBetAmount_slot').style.color='red';}else{document.getElementById('lblAPBetAmount_slot').style.color='black';}
			    //if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPValidBet_savan').style.color='red';}else{document.getElementById('lblAPValidBet_savan').style.color='black';}
			    if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPWinLoss_slot').style.color='red';}else{document.getElementById('lblAPWinLoss_slot').style.color='black';}
			    if(parseFloat(fltTotal[3])<0){document.getElementById('lblAPCommission_slot').style.color='red';}else{document.getElementById('lblAPCommission_slot').style.color='black';}
			    if(parseFloat(fltTotal[4])<0){document.getElementById('lblAPBonus_slot').style.color='red';}else{document.getElementById('lblAPBonus_slot').style.color='black';}
			    if(parseFloat(fltTotal[5])<0){document.getElementById('lblAPTotal_slot').style.color='red';}else{document.getElementById('lblAPTotal_slot').style.color='black';}
			    if(parseFloat(fltTotal[6])<0){document.getElementById('lblAPBalance_slot').style.color='red';}else{document.getElementById('lblAPBalance_slot').style.color='black';}
	    	}
		});
	}
	</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript: btnToDay();">
	<div id="parameter_area" style="width: 600px;">
		<div class="header" style="width: 596px;"><font color="#f0e62b"><?php echo Yii::app()->session['account_id'];?></font> <?php echo Yii::t('agent','agent.agentwinloss.agent_slot_winloss');?></div>
		<div class="body">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.game');?></td>
			<td class="right">
				<select name="cmbGame" id="cmbGame" class="cmb">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. strtoupper($row['game_name']) . '</option>';
					}
					?>
				</select>
			</td></tr>
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.currency');?></td>
			<td class="right">
				<select name="cmbCurrency" id="cmbCurrency" class="cmb" onchange="if(this.value!=''){document.getElementById('chkTestCurrency').style.display='none';document.getElementById('lblTestCurrency').style.display='none';}else{document.getElementById('chkTestCurrency').style.display='inline';document.getElementById('lblTestCurrency').style.display='inline';}">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableCurrency::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['currency_name'] . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>
				<input type="checkbox" id="chkTestCurrency" checked/><label id="lblTestCurrency" for="chkTestCurrency"><?php echo Yii::t('agent','agent.agentwinloss.except_test_currency');?></label>
			</td></tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.from');?></td>
				<td class="right">
					<input type="text" id="dtFrom" value="<?php echo date("Y-m-d");?>">
					<select id="cmbTime1">
					<?php 
						for($i=0;$i<=23;$i++){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					?>
					</select> :00:00
				</td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.to');?></td>
				<td class="right">
					<input type="text" id="dtTo" value="<?php echo date("Y-m-d");?>">
					<select id="cmbTime2">
					<?php 
						for($i=0;$i<=23;$i++){
							if($i<23){
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}else{
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}
						}
					?> 
					</select> :59:59
				</td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.account_id');?></td>
				<td class="right"><input type="text" id="txtAccountID"></td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.casino');?></td>
				<?php
				//$read_virtua = (Yii::app()->user->checkAccess('agent.readVirtuaWinLoss')==1 || User::getUserType() === Constants::ROLE_AGENT) ? 'style="display:inline"' : 'style="display:none"';  
				//$read_hatien = (Yii::app()->user->checkAccess('agent.readHTVSlotWinLoss')==1 || User::getUserType() === Constants::ROLE_AGENT) ? 'style="display:inline"' : 'style="display:none"';
				//$read_savan = (Yii::app()->user->checkAccess('agent.readSavanSlotWinLoss')==1 || User::getUserType() === Constants::ROLE_AGENT) ? 'style="display:inline"' : 'style="display:none"';
				$read_slot = (Yii::app()->user->checkAccess('agent.readSlotSlotWinLoss')==1 || User::getUserType() === Constants::ROLE_AGENT) ? 'style="display:inline"' : 'style="display:none"';
				?>
				<td class="right">
					<?php echo '<input type="hidden" id="lblAgentID_htv"><input type="hidden" id="lblDateFrom_htv"><input type="hidden" id="lblDateTo_htv"><input type="hidden" id="btnBack_htv">';?>
					<!--<input type="checkbox" id="chkHTV" <?php //echo $read_hatien;?> disabled><label <?php //echo $read_hatien;?> id="lblchkHTV" for="chkHTV">Hatienvegas999</label>   -->
					
					<!--<input type="checkbox" id="chkVIRTUA" checked <?php // echo $read_virtua;?>><label <?php // echo $read_virtua;?> id="lblchkVIRTUA" for="chkVIRTUA">Virtuavegas999</label>  -->
					<input type="checkbox" id="chkSLOT" checked <?php echo $read_slot;?>><label <?php echo $read_slot;?> id="lblchkSLOT" for="chkSLOT">Slotvegas999</label>
				</td>
			</tr>
			</table>
			<div class="footer"><input class="btn red" type="button" value="<?php echo Yii::t('agent','agent.agentwinloss.submit');?>" onclick="javascript: submitComplete(''); clearContainer();"> <input class="btn red" type="button" value="<?php echo Yii::t('agent','agent.agentwinloss.yesterday');?>" onclick="javascript: btnYesterday(); clearContainer();"> <input class="btn red" type="button" value="<?php echo Yii::t('agent','agent.agentwinloss.today');?>" onclick="javascript: btnToDay(); clearContainer();"></div>
		</div>
	</div>
	<div id="agent_winloss_body">
		<!--  <div id="dv_hatienvegas">
	    	<div id="qry_htv999_total"></div>
	    	<div id="qry_htv999_result"></div>
	    	<div id="qry_htv999_result_details"></div>
	    	<div id="qry_htv999_betting_details"></div>
	    	<div id="qry_htv999_betting_details_summary_footer" style="display: none;">
	    		<div id="betting_footer_header"><label id="lblHTVBettingSummaryPlayerID"></label> HTV BETTING HISTORY SUMMARY</div>
	    		<table id="betting_footer" cellpadding="5" cellspacing="0">
	    		<tr>
	    			<td class="theader">Type</td>
	    			<td class="theader">No. of Bet</td>
	    			<td class="theader">Bet Amount</td>
	    			<td class="theader">Win/Loss</td>
	    			<td class="theader">Commission</td>
	    			<td class="theader">Jackpot</td>
	    			<td class="theader">Total</td>
	    			<td class="theader">Balance</td>
	    		</tr>
	    		<tr>
	    			<td>Current Page</td>
	    			<td><label id="lblCPNoOfBet_htv">0</label></td>
	    			<td><label id="lblCPBetAmount_htv">0</label></td>
	    			<td><label id="lblCPWinLoss_htv">0</label></td>
	    			<td><label id="lblCPCommission_htv">0</label></td>
	    			<td><label id="lblCPBonus_htv">0</label></td>
	    			<td><label id="lblCPTotal_htv">0</label></td>
	    			<td><label id="lblCPBalance_htv">0</label></td>
	    		</tr>
	    		<tr>
	    			<td>All Page</td>
	    			<td><label id="lblAPNoOfBet_htv">0</label></td>
	    			<td><label id="lblAPBetAmount_htv">0</label></td>
	    			<td><label id="lblAPWinLoss_htv">0</label></td>
	    			<td><label id="lblAPCommission_htv">0</label></td>
	    			<td><label id="lblAPBonus_htv">0</label></td>
	    			<td><label id="lblAPTotal_htv">0</label></td>
	    			<td><label id="lblAPBalance_htv">0</label></td>
	    		</tr>
	    		</table>
	    	</div>
	    </div>
	    <div id="dv_savanvegas">
	    	<div id="qry_savan_vegas_total"></div>
	    	<div id="qry_savan_vegas_result"></div>
	    	<div id="qry_savan_vegas_result_details"></div>
	    	<div id="qry_savan_vegas_betting_details"></div>
		    <div id="qry_savan_vegas_betting_details_summary_footer" style="display: none;">
	    		<div id="betting_footer_header"><label id="lblSAVANBettingSummaryPlayerID"></label> SAVANVEGAS BETTING HISTORY SUMMARY</div>
	    		<table id="betting_footer" cellpadding="5" cellspacing="0">
	    		<tr>
	    			<td class="theader">Type</td>
	    			<td class="theader">No. of Bet</td>
	    			<td class="theader">Bet Amount</td>
	    			<td class="theader">Win/Loss</td>
	    			<td class="theader">Commission</td>
	    			<td class="theader">Jackpot</td>
	    			<td class="theader">Total</td>
	    			<td class="theader">Balance</td>
	    		</tr>
	    		<tr>
	    			<td>Current Page</td>
	    			<td><label id="lblCPNoOfBet_savan">0</label></td>
	    			<td><label id="lblCPBetAmount_savan">0</label></td>
	    			<td><label id="lblCPWinLoss_savan">0</label></td>
	    			<td><label id="lblCPCommission_savan">0</label></td>
	    			<td><label id="lblCPBonus_savan">0</label></td>
	    			<td><label id="lblCPTotal_savan">0</label></td>
	    			<td><label id="lblCPBalance_savan">0</label></td>
	    		</tr>
	    		<tr>
	    			<td>All Page</td>
	    			<td><label id="lblAPNoOfBet_savan">0</label></td>
	    			<td><label id="lblAPBetAmount_savan">0</label></td>
	    			<td><label id="lblAPWinLoss_savan">0</label></td>
	    			<td><label id="lblAPCommission_savan">0</label></td>
	    			<td><label id="lblAPBonus_savan">0</label></td>
	    			<td><label id="lblAPTotal_savan">0</label></td>
	    			<td><label id="lblAPBalance_savan">0</label></td>
	    		</tr>
	    		</table>
	    	</div>
	    </div>
	    <div id="dv_virtuavegas">
	    	<div id="qry_virtua_vegas_total"></div>
	    	<div id="qry_virtua_vegas_result"></div>
	    	<div id="qry_virtua_vegas_result_details"></div>
	    	<div id="qry_virtua_vegas_betting_details"></div>
		    <div id="qry_virtua_vegas_betting_details_summary_footer" style="display: none;">
	    		<div id="betting_footer_header"><label id="lblVIRTUABettingSummaryPlayerID"></label> VIRTUAVEGAS BETTING HISTORY SUMMARY</div>
	    		<table id="betting_footer" cellpadding="5" cellspacing="0">
	    		<tr>
	    			<td class="theader">Type</td>
	    			<td class="theader">No. of Bet</td>
	    			<td class="theader">Bet Amount</td>
	    			<td class="theader">Win/Loss</td>
	    			<td class="theader">Commission</td>
	    			<td class="theader">Jackpot</td>
	    			<td class="theader">Total</td>
	    			<td class="theader">Balance</td>
	    		</tr>
	    		<tr>
	    			<td>Current Page</td>
	    			<td><label id="lblCPNoOfBet_virtua">0</label></td>
	    			<td><label id="lblCPBetAmount_virtua">0</label></td>
	    			<td><label id="lblCPWinLoss_virtua">0</label></td>
	    			<td><label id="lblCPCommission_virtua">0</label></td>
	    			<td><label id="lblCPBonus_virtua">0</label></td>
	    			<td><label id="lblCPTotal_virtua">0</label></td>
	    			<td><label id="lblCPBalance_virtua">0</label></td>
	    		</tr>
	    		<tr>
	    			<td>All Page</td>
	    			<td><label id="lblAPNoOfBet_virtua">0</label></td>
	    			<td><label id="lblAPBetAmount_virtua">0</label></td>
	    			<td><label id="lblAPWinLoss_virtua">0</label></td>
	    			<td><label id="lblAPCommission_virtua">0</label></td>
	    			<td><label id="lblAPBonus_virtua">0</label></td>
	    			<td><label id="lblAPTotal_virtua">0</label></td>
	    			<td><label id="lblAPBalance_virtua">0</label></td> 
	    		</tr>
	    		</table>
	    	</div>
	    </div>
	    <br/>-->
	    <div id="dv_slotvegas">
	    	<div id="qry_slot_vegas_total"></div>
	    	<div id="qry_slot_vegas_result"></div>
	    	<div id="qry_slot_vegas_result_details"></div>
	    	<div id="qry_slot_vegas_betting_details"></div>
		    <div id="qry_slot_vegas_betting_details_summary_footer" style="display: none;">
	    		<div id="betting_footer_header"><label id="lblSLOTBettingSummaryPlayerID"></label> SLOTVEGAS BETTING HISTORY SUMMARY</div>
	    		<table id="betting_footer" cellpadding="5" cellspacing="0">
	    		<tr>
	    			<td class="theader">Type</td>
	    			<td class="theader">No. of Bet</td>
	    			<td class="theader">Bet Amount</td>
	    			<td class="theader">Win/Loss</td>
	    			<td class="theader">Commission</td>
	    			<td class="theader">Jackpot</td>
	    			<td class="theader">Total</td>
	    			<td class="theader" style="display: none;">Balance</td>
	    		</tr>
	    		<tr>
	    			<td>Current Page</td>
	    			<td><label id="lblCPNoOfBet_slot">0</label></td>
	    			<td><label id="lblCPBetAmount_slot">0</label></td>
	    			<td><label id="lblCPWinLoss_slot">0</label></td>
	    			<td><label id="lblCPCommission_slot">0</label></td>
	    			<td><label id="lblCPBonus_slot">0</label></td>
	    			<td><label id="lblCPTotal_slot">0</label></td>
	    			<td style="display: none;"><label id="lblCPBalance_slot">0</label></td>
	    		</tr>
	    		<tr>
	    			<td>All Page</td>
	    			<td><label id="lblAPNoOfBet_slot">0</label></td>
	    			<td><label id="lblAPBetAmount_slot">0</label></td>
	    			<td><label id="lblAPWinLoss_slot">0</label></td>
	    			<td><label id="lblAPCommission_slot">0</label></td>
	    			<td><label id="lblAPBonus_slot">0</label></td>
	    			<td><label id="lblAPTotal_slot">0</label></td>
	    			<td style="display: none;"><label id="lblAPBalance_slot">0</label></td>
	    		</tr>
	    		</table>
	    	</div>
	    </div>
	</div>
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
	</form>
</body>
</html>