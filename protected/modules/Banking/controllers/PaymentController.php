<?php
/**
 * @todo	
 * @copyright	CE
 * @author	Kimny MOUK
 * @since	2012-04-24
 */

class PaymentController extends MyController
{	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('banking.readPayment'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionPaymentList()
	{	
		$modelBanking= new Banking();
		
		// Get the requested page. By default grid sets this to 1.
		$page = $_GET['page']; 
 
		// get how many rows we want to have into the grid - rowNum parameter in the grid 
		$limit = $_GET['rows']; 
		 
		// get index row - i.e. user click to sort. At first time sortname parameter -
		// after that the index from colModel 
		$orderField = $_GET['sidx']; 
		 
		// sorting order - at first time sortorder 
		$sortType = $_GET['sord']; 
				
		$result=$modelBanking->getCoundPaymentRecords();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		// calculate the starting position of the rows
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$paymentData=$modelBanking->getPayment($orderField, $sortType, $startIndex, $limit);
		
		$filedNames = array("transaction_number", "transaction_date", "transaction_type","transaction_via", "account_id", "fullname","currency_name","amount", "status_name");
		$htmValue="";
		echo JsonUtil::generateJqgridData($paymentData->readAll(), $total_pages, $page, $records,$filedNames,$htmValue,3);
	}

	public function actionLoadFormWithdraw()
	{
		$modelWithdrawFrom=new Banking;
		$bank = new Withdraw();
		$account_id=$_GET['account_id'];
		$transaction_number=$_GET['transaction_number'];
		$balance=$modelWithdrawFrom->getCashPlayerBalance($account_id);		
		$bankList = $bank->getTransactionItemBankList($bank->getWithdrawType($_GET['transaction_number']));
		$trans_item_from_history = $bank->getTransactionItemInCashplayerTransHist($_GET['transaction_number']);
		$trans_item_from_withdraw = $bank->getTransactionItemInCashPlayerWithdraw($_GET['transaction_number']);
		
		$this->renderPartial("formWithdraw",array('withdraw'=>$modelWithdrawFrom->getWithdrawInfo($account_id,$transaction_number),
				'balance'=>$balance, 'bankList'=>$bankList, 'transaction_item_name'=>$trans_item_from_history, 'transaction_item'=>$trans_item_from_withdraw,
				'withdraw_status'=>($bank->isTransactionNumberExist($transaction_number)) ? 'done' : 'in_process',
				'finish_datetime'=>$bank->getFinishTime($_GET['transaction_number'])));
		
	}
	
	public function actionLoadFormDeposit()
	{
		$modelDepositFrom=new Banking();
		
		$account_id=$_GET['account_id'];
		$transaction_number= $_GET['transaction_number'];
	
		$this->renderPartial("formDeposit",array('deposit'=>$modelDepositFrom->getDepositInfo($account_id,$transaction_number)));
	
	}
	
	public function actionLoadFormDepositPending()
	{
		$modelDepositFrom=new Banking();
		
		$account_id=$_GET['account_id'];
		$transaction_number= $_GET['transaction_number'];
	
		$this->renderPartial("formDepositPending",array('deposit'=>$modelDepositFrom->getDepositInfo($account_id,$transaction_number)));
	
	}
	/**
	 * @todo Withdrawal
	 * @copyright	CE
	 * @author	Leokarl
	 * @since	2012-05-24
	 */
	public function actionConfirmWithdraw(){
		/*
		 * @todo
		 * 1. Check if the plyaer is on the lobby
		 * 2. Display warning message force logout if player is on the lobby
		 * 3. If click ok, display count down message.
		 * 4. Process withdraw
		 */
		
		// initialize
		$model = new Withdraw();
		//$redis = new RedisManager();
		$withdraw = new Banking;
		//$balanceOnCasinoLobby = new RedisLobbyManager();
		
		// data validation
		if(!Yii::app()->user->checkAccess('banking.writePayment')){
			exit('no_permission');
		}
		
		if(!isset($_POST['account_id']) || !isset($_POST["transaction_number"])){
			exit('required_data_not_set');
		}
		if($_POST['transaction_status_id'] !=7 ){
			if($model->getWithdrawType($_POST["transaction_number"]) != 'MW' && !$model->isTransactionNumberExist($_POST['transaction_number'])){
//				if ($redis->isExistingOnLobbyByPlayer($_POST['account_id'])){
//					// display waring message 'forece logout'= > if we click Confirm => force logout => delete account_id from Redis
//					exit('force_logout_warning');
//				}
			}
		}
		if(!$model->isCashPlayerStatusActive($_POST['account_id'])){
			exit('account_inactive');
		}
		$withdraw = $withdraw->getWithdrawInfo($_POST['account_id'], $_POST["transaction_number"]);
		
		if(!$model->isCurrencyIdValid($withdraw[0]['currency_id'])){
			exit('invalid_currency_id');
		}
		if($model->isSpecialCharPresent($withdraw[0]['request_amount'])){
			exit('invalid_request_amount');
		}
		if(!is_numeric($withdraw[0]['request_amount'])){
			exit('invalid_request_amount');
		}
		if($withdraw[0]['request_amount'] <= 0){
			exit('invalid_request_amount');
		}
		if(!$model->isTransactionNumberExistInTableCashPlayerWithdraw($_POST['account_id'],$_POST["transaction_number"])){
			exit('transaction_number_not_exist_in_cashplayer_withdraw');
		}
		if(!$model->isTransactionStatusIdValid($_POST['transaction_status_id'])){
			exit('invalid_transaction_status_id');
		}
		if($withdraw[0]['transaction_status_id'] == 6){
			exit('transaction_already_done');
		}
		
		if($_POST['transaction_status_id'] !=7 ){
			if(!isset($_POST['transaction_item']) || trim($_POST['transaction_item']) ==''){
				exit('select_bank');
			}
			if(!$model->isTransactionItemValid($_POST['transaction_item'],$model->getWithdrawType($_POST["transaction_number"]))){
				exit('invalid_bank_id');
			}
		}
		if(!isset($_POST['transation_time'])){
			exit('invalid_time_format');
		}
		if(trim($_POST['transation_time']) == ''){
			exit('process_date_required');
		}
		if(!$model->sDateTimeValid($_POST['transation_time'])){
			exit('invalid_time_format');
		}
		$datetime1 = new DateTime($withdraw[0]['transaction_date']);
		$datetime2 = new DateTime($_POST['transation_time']);
		if($datetime1 > $datetime2){
			exit('cant_set_finish_time_before_transaction_date');
		}
		if((strtotime(date("Y-m-d")) - strtotime(date("Y-m-d", strtotime($_POST['transation_time'])))) < 0){
			exit('cant_set_process_date_as_tomorrow_date');
		}
		$result['errorCode'] =0; //$balanceOnCasinoLobby->withdrawFromCasinoLobby($_POST['account_id']);
		if ($result['errorCode'] != 1){ // no error
 			// then process withdraw
 			$model->updateCashPlayerWithdraw($_POST['account_id'], $_POST["transaction_number"], $_POST['transaction_status_id'],
 					$withdraw[0]['currency_id'], $withdraw[0]['request_amount'], $_POST['transaction_item'], $_POST['transation_time']);

 		}else if ($result['errorCode'] == 1 && $result['casino'] === 'HTV'){ // error
 			exit('disconnected_to_htv');
 		}else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN'){ // error
 			exit('disconnected_to_savan');
 		}
	}
	
	/**
	 * @todo process withdrawal request
	 * @author leokarl
	 * @since 2012-12-27
	 */
	public function actionProcessWithdrawalRequest()
	{
		$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
		$rd=$command->queryRow();
		if ($rd['deposit_withdrawal'] == 1){
			if ($rd['casino_id']==1 || $rd['casino_id']==2){
				$checkBalanceIfLock = new RedisLobbyManager();
				$result=$checkBalanceIfLock->withdrawFromCasinoLobby($_POST['accountID']);
				//if there's a problem in HTV and SAVAN
				if ($result['errorCode'] == 1 && $result['casino'] === 'HTV') // error
				{
					//to display message that there is an error during lobby withdraw ih HTV
					echo 'h';
				}
				else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN') // error
				{
					//to display message that there is an error during lobby withdraw ih SAVAN
					echo 's';
				}
			}
			else // balance stuck on Costavegas & VirtuaVegas
			{
				echo '1';
			}
		}
		$withdrawal = new RedisLobbyManager();
		$withdrawal->ProcessWithdrawalRequest($_POST['accountID']);
	
	}
	
	/**
	 * @todo check player online status
	 * @author leokarl
	 * @since 2012-12-27
	 */
	public function actionCheckPlayerOnlineStatus(){
		if (isset(Yii::app()->session['account_id'])){
			// data validation
			if(!isset($_POST['accountID']) || !isset($_POST['trans_number'])){
				exit('param_data_not_set');
			}
			
			// initialize
			$withdraw = new Withdraw();
			
			if($withdraw->getWithdrawType($_POST['trans_number']) != 'MW' && !$withdraw->isTransactionNumberExist($_POST['trans_number'])){ // if not manual withdraw, check online status
				$redis=new RedisManager();
				$checkLobby = $redis->isExistingOnLobbyByPlayer($_POST['accountID']);
				$casinoId = $redis->getCasinoIDByPlayer($_POST['accountID']);
				
				if ($checkLobby!=0){
					echo $casinoId;
				}else{
					$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM 
							tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
					$rd=$command->queryRow();
					if ($rd['deposit_withdrawal'] == 1){ // display waiting message for calling withdrawal API.
						echo'w';
					}else {
						//display withdrawal dialog
						echo '0';
					}
				}
			}else{ // if manual withdraw, no need to check if online
				//display withdrawal dialog
				echo '0';
			}
		}else{
			echo 'die';
		}
	}
	/**
	 * @todo check if logout is still processing
	 * @author leokarl
	 * @since 2013-01-03
	 */
	public function actionCheckIfStillProcessing(){
// 		$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM 
// 				tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
// 		$rd=$command->queryRow();
		echo 0;
	}
	/*
	 * @todo Load force logout dialog
	 * @author leokarl
	 * @date 2012-12-05
	 */
	public function actionLoadForceLogOutDialog()
	{
		$this->renderPartial("forceLogOutDialog");
	}
	
	/*
	 * @todo Force logout player on lobby
	 * @author leokarl
	 * @date 2012-12-06
	 */
	public function actionForceLogOutPlayer(){
		$logout=new RedisManager();
		$logout->forceLogoutOnLobbyByPlayer($_POST['player_id']);
	}
	public function actionBankInformation()
	{
		$bi=new Withdraw();
		$bi->getBankInformation();
	}
	public function actionCheckPlayerStat()
	{
		$cp=new Withdraw();
		$cp->checkPlayerStat();
	}
	Public function actionFinancePassword()
	{
		$cw=new Withdraw();
		$cw->getFinancePassword();
	}
	
	public function actionTest(){
		$model = new Withdraw();
 		//print_r($model->sDateTimeValid('2013-01-31 13:44:00'));
 		$datetime1 = new DateTime('2013-01-31 13:44:00');
		$datetime2 = new DateTime('2013-01-31 13:43:00');
		if($datetime1 <= $datetime2){
			echo 'valid';
		}else{
			echo 'invalid';
		}
	}
}