//active menu color
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_currency_setting').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Currency Setting</a></li>");
function validate(text)
{
	var intval=Math.round(text*100)/100;
	var regex = /^\d{1,3}(,?\d{3})*?(.\d{2})?$/g;
	//return regex.test(intval);
	return !isNaN(parseFloat(intval)) && isFinite(intval);
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}
function checkIt(evt,strValue) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    //var noOfPeriod = strValue.replace(/[^.]/g, '').length;
	//alert("There are " + (theString.replace(/[^.]/g, '').length) + "letter o's in the string");
	//if(charCode==222){alert('oo');}
    if(((charCode >= 48 && charCode <= 57 )||(charCode == 46 )||(charCode == 37 )||(charCode == 39 )||(charCode == 8 ) || (charCode == 13)))
	{
    	return true;
	}
    else
    {
    	return false;
	}
}
function get_cookie(cookie_name)
{
  var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

  if (results)
    return (unescape(results[2]));
  else
    return null;
}
var $addCurrencyDialog='';
$(document).ready(function() {
	$addCurrencyDialog = $('<div></div>')
		.html('<div id="cs_addcurrency"><table border="0" cellpadding="0" cellspacing="0">'
				+'<tr><td class="left">Currency</td><td class="right"><input type="text" style="width:170px;" name="txtCurrency" id="txtCurrency"> <font color="red">*</font></td></tr>'
				+'<tr><td class="left">Exchange rate</td><td class="right"><input type="text" style="width:170px;" name="txtExchangeRate" id="txtExchangeRate"> <font color="red">*</font></td></tr>'
				+'</table><div id="cs_footer"><input class="btn red" type="submit" id="confirm-currency" value="  Confirm  "><br/>'
				+'<input type="text" id="cs_errMessage" readonly="true"/></div></div>')
		.dialog({
			autoOpen: false,
			width: 400,
			title: 'Add Currency',
			resizable: false,
			modal: true
		});
		$('#confirm-currency').click(function(event) {
			if (trim(document.getElementById('txtCurrency').value)==''){document.getElementById('cs_errMessage').value='Currency name is required!';document.getElementById('txtCurrency').focus(); return false;}
   		 	if (trim(document.getElementById('txtExchangeRate').value)==''){document.getElementById('cs_errMessage').value='Exchange rate is required!';document.getElementById('txtExchangeRate').focus(); return false;}
   		 	if (validate(document.getElementById('txtExchangeRate').value)==false){document.getElementById('cs_errMessage').value='Invalid exchange rate!';document.getElementById('txtExchangeRate').focus(); return false;}
   		 
			jQuery.ajax({
	    		url: createCurrencyURL,
	    		method: 'POST',
	    		data: {'task': 'addNewCurrency', 'strCurrency': document.getElementById('txtCurrency').value, 'dblExchangeRate' : document.getElementById('txtExchangeRate').value},
	    		context: jQuery(this).val(),
	    		success: function(msg) {
	    			 var data=msg.split(';');
	    			 if(data[0]==0){
		    			 var addNewRow = {id:data[2], currency_name:document.getElementById('txtCurrency').value, exchange_rate:document.getElementById('txtExchangeRate').value, last_update: data[1], setting:"<a href=\'#\' onclick=\'javascript: getCurrencyName(this); $updateCurrencyDialog.dialog(\"open\");\' id=\'"+ data[2] +':'+ document.getElementById('txtCurrency').value +":"+ document.getElementById('txtExchangeRate').value +"\' class=\'update-currency\'>Setting</a>"};
		    			 jQuery("#list2").addRowData(data[2], addNewRow);
		    			 document.getElementById('txtCurrency').value='';
		    		     document.getElementById('txtExchangeRate').value='';
		    		     document.getElementById('cs_errMessage').value='';
		    			 $addCurrencyDialog.dialog('close');
	    			 }
	    			 else{
	    				 document.getElementById('cs_errMessage').value='Currency name is already exist!';
		    		 }
		    	}
	    	});
		});
	});

var $updateCurrencyDialog='';
$(document).ready(function() {
$updateCurrencyDialog = $('<div></div>')
	.html('<div id="cs_updatecurrency"><table border="0" cellpadding="0" cellspacing="0">'
			+'<tr><td class="left">Currency</td><td class="right"><input type="text" style="width:170px;" name="txtUCurrency" id="txtUCurrency" onkeydown="if(event.keyCode == 13){document.getElementById(\'confirm_update_currency\').click();}"> <font color="red">*</font></td></tr>'
			+'<tr><td class="left">Exchange rate</td><td class="right"><input type="text" style="width:170px;" name="txtUExchangeRate" id="txtUExchangeRate" maxlength="8" onkeydown="if(event.keyCode == 13){document.getElementById(\'confirm_update_currency\').click();}"> <font color="red">*</font></td></tr>'
			+'</table><div id="cs_footer"><input class="btn red" type="submit" id="confirm-update-currency1" value="  Confirm  "><br/>'
			+'<input type="text" id="csu_errMessage" readonly="true"/></div></div>')
	.dialog({
		autoOpen: false,
		width: 350,
		title: 'Setting Currency',
		resizable: false,
		modal: true
	});
	$('#confirm-update-currency1').click(function(event) {
		if (trim(document.getElementById('txtUCurrency').value)==''){document.getElementById('csu_errMessage').value='Currency name is required!';document.getElementById('txtUCurrency').focus(); return false;}
		 	if (trim(document.getElementById('txtUExchangeRate').value)==''){document.getElementById('csu_errMessage').value='Exchange rate is required!';document.getElementById('txtUExchangeRate').focus(); return false;}
		 	else{if(validate(document.getElementById('txtUExchangeRate').value)==false){document.getElementById('csu_errMessage').value='Invalid exchange rate!'; document.getElementById('txtUExchangeRate').focus();return false;}}
		 	//alert(numberWithCommas(Math.round(document.getElementById('txtUExchangeRate').value * 100)/100));
		jQuery.ajax({
    		url: updateCurrencyURL,
    		method: 'POST',
    		data: {'intUCurrencyID': document.getElementById('txtUCurrency').title, 'strUCurrency': document.getElementById('txtUCurrency').value, 'dblUExchangeRate' : document.getElementById('txtUExchangeRate').value},
    		context: jQuery(this).val(),
    		success: function(msg) {
	    		var data=msg.split(';');
	    		var numval=numberWithCommas(Math.round(document.getElementById('txtUExchangeRate').value * 100)/100);
    			if(data[0]==0){
    				//jQuery("#list2").jqGrid('setCell', cRowNo, 2, document.getElementById('txtUCurrency').value);
	    			//jQuery("#list2").jqGrid('setCell', cRowNo, 3, numval);
	    			//jQuery("#list2").jqGrid('setCell', cRowNo, 4, data[1]);
	    			$('#list2').trigger("reloadGrid");
	    			$updateCurrencyDialog.dialog('close');
	    			
    			}
    			else{
    				document.getElementById('csu_errMessage').value='Currency name is already exist!';
	    		}
    			
	    	}
    	});
	});
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
	
function getCurrencyName(aElement) {
	var rowData=aElement.id;
	//cRowNo = 
	var Currency=rowData.split(':');
	document.getElementById('txtUCurrency').title=Currency[0];
	document.getElementById('txtUCurrency').value=Currency[1];
	//document.getElementById('txtUExchangeRate').value=Currency[2];
}
var cRowNo=0;
$(document).ready(function() {
	var grid=jQuery("#list2");
	grid.jqGrid({ 
		url: currencyListURL, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['No.', 'Currency','Exchange rate','Last Update','Setting'],
	    colModel: [
	      {name: 'id', index: 'id', width: 50, search:true},
	      {name: 'currency_name', index: 'currency_name', width: 100},
	      {name: 'exchange_rate', index: 'exchange_rate', width: 100},
	      {name: 'last_update', index: 'last_update', width: 130},
	      {name: 'setting', index: 'setting', width: 100, align: 'center' , sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="text-decoration: underline"'; },}
	    ],
	    beforeSelectRow: function(rowid,e){
	    	cRowNo=rowid;
	    	var myrow = grid.jqGrid('getRowData', rowid);
	    	var intval=myrow.exchange_rate;
	    	var intval1=intval.replace(",","");
	    	var intval2=intval1.replace(",","");
	    	var intval3=intval2.replace(",","");
	    	var intval4=intval3.replace(",","");
	    	var intval5=intval4.replace(",","");
	    	var intval6=intval5.replace(",","");
	    	var intval7=intval6.replace(",","");
	    	if (intval7.length>9){
		    	var trueval=intval7.split(".");
	    		document.getElementById('txtUExchangeRate').value = trueval[0];
		    }else{
		    	document.getElementById('txtUExchangeRate').value = intval7.replace(",","");
			}
	    	
		},
		loadComplete:function(){
			$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
		},
	    rowNum: 20,
	    rownumbers: true,
	    rowList: [5,10, 20, 30],
	    pager: '#pager2',
	    sortname: 'id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: '<input type="submit" class="btn red disabled" value="Add" onclick="$addCurrencyDialog.dialog(\'open\');" disabled/> Currency List',
	    viewrecords: true
	});
	$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false,search: false, refresh: false}).navButtonAdd('#pager2',{
		   caption:"LOGS", 
		   buttonicon:"ui-icon-calculator", 
		   onClickButton: function(){ 
		     window.location=currencyListLogURL;
		   }, 
		   position:"last"
		});
	});