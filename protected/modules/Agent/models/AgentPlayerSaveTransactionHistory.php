<?php 
/**
 * This is the model class for Agent player Cashier.
 @Author: Allan Bicol
 @Since: 05/12/2012
 */



class AgentPlayerSaveTransactionHistory extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_agent_player_trans_history';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}

class AgentPlayerSaveBonus extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_agent_player_bonus';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}

class AgentSaveTransactionHistory extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_agent_trans_history';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}
