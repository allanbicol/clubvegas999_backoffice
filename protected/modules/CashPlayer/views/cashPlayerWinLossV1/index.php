<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_winloss_v1').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><a href='#'>Cash Player Win/Loss Version 1</a></li>");
</script>
<script type="text/javascript">
	
	//HTV999
	function tableWinLossTodayHTV(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list2'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultHTV").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager2'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultHTV").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list2");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/CashPlayerWinLossHTV&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','Account ID', 'Account Name','Last Bet Date','Currency','Bet Count','Total Stake','Valid Stake','Average Bet','Win/Loss','Commission','Bonus','<b>Total</b>','Balance','<b>P/L</b>'],
			    colModel: [
						{name: 'no', index: 'no', width: 25,title:false,sortable:false},
						{name: 'account_id', index: 'account_id', width: 100, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
						{name: 'account_name', index: 'account_name', width: 110,title:false},
						{name: 'last_bet_date', index: 'last_bet_date', width: 130,title:false},
						{name: 'currency', index: 'currency', width: 55,title:false},
						{name: 'bet_count', index: 'bet_count', width: 60, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bet_count">{0}</label>'},
						{name: 'total_stake', index: 'total_stake', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_stake">{0}</label>'},
						{name: 'amount_wager', index: 'amount_wager', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_wager">{0}</label>'},
						{name: 'avg_bet', index: 'avg_bet', width: 75, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_bet">{0}</label>'},
						{name: 'win_los', index: 'win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
						{name: 'commission', index: 'commission', width: 70, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="commission">{0}</label>'},
						{name: 'bonus', index: 'bonus', width: 70, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bonus">{0}</label>'},
						{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
						{name: 'current_balance', index: 'current_balance', width: 95, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="current_balance">{0}</label>'},
						{name: 'p_l', index: 'p_l', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
						],
			    
				loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list2");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailHTV&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo)+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId+'&id='+enc(document.getElementById('txtAccountId').value);

				                }

			                e.preventDefault();
			            });
			            //myGrid.jqGrid('setCell', i,'account_id' , {color:'red'});
			          
			        }
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
				    	grid.jqGrid('setCell',i,"account_id","",{'text-decoration':'underline'});
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
					    }

				    	 $(".dateSummaryFooter").each(function(i) {this.style.color='#000000';});
				    	 $(".bet_count").each(function(i) {this.style.color='#000000';});
					     $(".total_stake").each(function(i) {this.style.color='#000000';});
				    	 $(".amount_wager").each(function(i) {this.style.color='#000000';});
					     $(".avg_bet").each(function(i) {this.style.color='#000000';});
					     $(".win_los").each(function(i) {this.style.color='#000000';});
					     $(".commission").each(function(i) {this.style.color='#000000';});
					     $(".bonus").each(function(i) {this.style.color='#000000';});
					     $(".total_win_los").each(function(i) {this.style.color='#000000';});//pl
					     $(".current_balance").each(function(i) {this.style.color='#000000';});
					     $(".p_l").each(function(i) {this.style.color='#000000';});//pl

				    	 $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//total_winloss
					     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					     $(".p_l").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					} 
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			   
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager2',
			    sortname: 'currency',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">HTV </label> Member Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
				groupingView : {
			   		groupField : ['currency'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   
			   	
			});
			$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search: false});

			jQuery("#list2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_los', numberOfColumns: 4, titleText: '<label ><b>PLAYER</b></label>'},
				
				  ]
			});
			jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
	            caption:"Export current page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportHTVWinLossToExcel();
	           }, 
	            position:"last"
	        });   
			jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
	            caption:"Export all pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllHTVWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId);
	           }, 
	            position:"last"
	        });  
		});
	} 
	function exportHTVWinLossToExcel(){
		var accountId=$('#txtAccountId')[0].value;
		var tableId=$('#txtTableId')[0].value;
		var gameId =$('#txtGameId')[0].value;
		var shoesId=$('#txtShoesId')[0].value;
		var gameType=$('#gameType')[0].value;
		var currency=$('#currencyType')[0].value;
		testChecked=0;
		if (document.getElementById("chkTest").checked==true){
			 testChecked=1;
		}
		var testC	=	testChecked;
		var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
		var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
		
		var table1= document.getElementById('qry_resultHTV');
		var table2= document.getElementById('qry_result_HTV_summary');
		var html1 = table1.outerHTML;
		var html2 = table2.outerHTML;
		document.forms[2].csvBuffer.value="";
		if (tableId==''){
			document.forms[2].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
		}else{
			document.forms[2].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
		}
		document.forms[2].csvBuffer.value=html1+''+html2 ;
	    document.forms[2].method='POST';
	    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/HTVWinLossExcel';  // send it to server which will open this contents in excel file
	    document.forms[2].target='_top';
	    document.forms[2].submit(); 
	}
	function exportAllHTVWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId){
	    $.ajax({	
			 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/HTVWinLossExcelAll&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId, 
			 async:true,
			 success: function(result) {
					var data =result.split("<BREAK>");
					
					var accountId=$('#txtAccountId')[0].value;
					var tableId=$('#txtTableId')[0].value;
					var gameId =$('#txtGameId')[0].value;
					var shoesId=$('#txtShoesId')[0].value;
					var gameType=$('#gameType')[0].value;
					var currency=$('#currencyType')[0].value;
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					var testC	=	testChecked;
					var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
					var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
					
					document.forms[2].csvBuffer.value="";
					if (tableId==''){
						document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
					}else{
						document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
					}
					document.forms[2].csvBuffer.value=data[0]+""+data[2];
				    document.forms[2].method='POST';
				    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/HTVWinLossExcel';  // send it to server which will open this contents in excel file
				    document.forms[2].target='_top';
				    document.forms[2].submit(); 
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown) { 
		 }});
	}
	// HTV ALL PAGES SUMMARY
	function tableWinLossHTVTotal(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list4'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_HTV_summary").appendChild(divTag);
	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager4'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultHTV").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list4");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/CashPlayerWinLossHTVTotal&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Currency','Bet Count','Total Stake','Valid Stake','Win/Loss','Commission','Bonus','<b>Total</b>','<b>P/L</b>','Ex-Rate','UC Total Stake','UC Valid Stake','UC WinLoss','UC Commission','UC Bonus','UC Total','UC P/L'],
			    colModel: [
					{name: 'currency', index: 'currency', width: 60,title:false},
					{name: 'bet_count', index: 'bet_count', width: 70, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_stake', index: 'total_stake', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'amount_wager', index: 'amount_wager', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'bonus', index: 'bonus', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_win_los', index: 'total_win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'rate', index: 'rate', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','},hidden:true},
					{name: 'total_stake1', index: 'total_stake1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'amount_wager1', index: 'amount_wager1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'win_los1', index: 'win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>',hidden:true},
					{name: 'commission1', index: 'commission1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'bonus1', index: 'bonus1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'total_win_los1', index: 'total_win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>',hidden:true},
					{name: 'p_l1', index: 'p_l1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
				    ],

				loadtext:"",
				loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"currency","",{background:'#5A0000',color:'white'});
				    	grid.jqGrid('setCell',i,"bet_count","",{background:'#5A0000',color:'white'});
				    	grid.jqGrid('setCell',i,"total_stake","",{background:'#5A0000',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_wager","",{background:'#5A0000',color:'white'})
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#5A0000',color:'white'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#5A0000',color:'white'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#5A0000',color:'white'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#5A0000',color:'white'});
				    	grid.jqGrid('setCell',i,"p_l","",{background:'#5A0000',color:'white'});
				    	
						
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'yellow'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'yellow'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'yellow'});	
					    }			    
					}   
			    },
			    hidegrid: false,  
			    rowNum: 25,	
			    rowList: [25, 50, 75],
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">HTV </label>Win/Loss Summary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convert this to </label><select id="currencyTypeConvertHTV" style="width:80px" onchange="javascript: convertHTV();"><option value="0-0">--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>',
			    viewrecords: true,
			    footerrow:true,
			    
			});
			$('#list4').jqGrid('navGrid', '#pager4', {edit: false, add: false, del:false, search: false});

			
		});
	} 

	function convertHTV(){
    	var comboValue=document.getElementById("currencyTypeConvertHTV").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	
		
		if (comboRange==0){
   		 $('#list4').trigger("reloadGrid");
   		$('#list4').jqGrid('footerData','set', {total_stake:'',amount_wager:'',win_los:'',commission:'',total_win_los:'', bonus:'',p_l:''});   
   		 return false;
       }
	       
		var noRow=$("#list4").getGridParam("reccount");
		var rate;
		var totalStake;
		var stake;
		var winloss;
		var commission;
		var total;
		var bonus;
		var pl;
		var totalStakeconversion;
		var stakeConversion;
		var totalConversion;
		var winlossConversion;
		var commissionConversion;
		var bonusConversion;
		var plConversion;

	    var x=1;
	    while (x<=noRow)
	      {
	    	rate=$("#list4").getCell(x, 'rate');
	    	totalStake=$("#list4").getCell(x, 'total_stake1');
	    	stake=$("#list4").getCell(x, 'amount_wager1');
	    	winloss=$("#list4").getCell(x, 'win_los1');
	    	commission=$("#list4").getCell(x, 'commission1');
			total=$("#list4").getCell(x, 'total_win_los1');
			bonus=$("#list4").getCell(x, 'bonus1');
			pl=$("#list4").getCell(x, 'p_l1');
				totalStakeconversion=((parseFloat(totalStake)/parseFloat(rate)) * parseFloat(comboRange));
				stakeConversion=((parseFloat(stake)/parseFloat(rate)) * parseFloat(comboRange));
	    		winlossConversion=((parseFloat(winloss)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	commissionConversion=((parseFloat(commission)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		bonusConversion=((parseFloat(bonus)/parseFloat(rate)) * parseFloat(comboRange));
	    		plConversion=((parseFloat(pl)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list4").jqGrid('setCell', x, 0, comboName);
	    		jQuery("#list4").jqGrid('setCell', x, 2, totalStakeconversion);
	    		jQuery("#list4").jqGrid('setCell', x, 3, stakeConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 4, winlossConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 5, commissionConversion);
	    	    jQuery("#list4").jqGrid('setCell', x, 6, bonusConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 7, totalConversion);	
	    		jQuery("#list4").jqGrid('setCell', x, 8, plConversion);	
			
	     	x++;
	      }
	    var grid = $("#list4"),
	    sumTotalStake=grid.jqGrid('getCol', 'total_stake', false, 'sum');
        sumStake= grid.jqGrid('getCol', 'amount_wager', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_los', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');
        sumBonus=grid.jqGrid('getCol', 'bonus', false, 'sum');
        sumPl=grid.jqGrid('getCol', 'p_l', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake:sumTotalStake,amount_wager:sumStake,win_los:sumWinLoss,commission:sumCommission,total_win_los:sumTotal, bonus: sumBonus,p_l:sumPl});  
	}

	//SAVANVEGAS WINLOSS
		function tableWinLossTodaySAVAN(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list5'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultSAVAN").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager5'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultSAVAN").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list5");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/CashPlayerWinLossSAVAN&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','Account ID', 'Account Name','Last Bet Date','Currency','Bet Count','Total Stake','Valid Stake','Average Bet','Win/Loss','Commission','Bonus','<b>Total</b>','Balance','<b>P/L</b>'],
			    colModel: [
						{name: 'no', index: 'no', width: 25,title:false,sortable:false},
						{name: 'account_id', index: 'account_id', width: 100, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
						{name: 'account_name', index: 'account_name', width: 110,title:false},
						{name: 'last_bet_date', index: 'last_bet_date', width: 130,title:false},
						{name: 'currency', index: 'currency', width: 55,title:false},
						{name: 'bet_count', index: 'bet_count', width: 60, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bet_count">{0}</label>'},
						{name: 'total_stake', index: 'total_stake', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_stake">{0}</label>'},
						{name: 'amount_wager', index: 'amount_wager', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_wager">{0}</label>'},
						{name: 'avg_bet', index: 'avg_bet', width: 75, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_bet">{0}</label>'},
						{name: 'win_los', index: 'win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
						{name: 'commission', index: 'commission', width: 70, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="commission">{0}</label>'},
						{name: 'bonus', index: 'bonus', width: 70, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bonus">{0}</label>'},
						{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
						{name: 'current_balance', index: 'current_balance', width: 95, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="current_balance">{0}</label>'},
						{name: 'p_l', index: 'p_l', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
						],
			    
				loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list5");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavan&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo)+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId+'&id='+enc(document.getElementById('txtAccountId').value);

				                }

			                e.preventDefault();
			            });
			            //myGrid.jqGrid('setCell', i,'account_id' , {color:'red'});
			          
			        }
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
				    	grid.jqGrid('setCell',i,"account_id","",{'text-decoration':'underline'});
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
					    }
				    	$(".dateSummaryFooter").each(function(i) {this.style.color='#000000';});
				    	 $(".bet_count").each(function(i) {this.style.color='#000000';});
					     $(".total_stake").each(function(i) {this.style.color='#000000';});
				    	 $(".amount_wager").each(function(i) {this.style.color='#000000';});
					     $(".avg_bet").each(function(i) {this.style.color='#000000';});
					     $(".win_los").each(function(i) {this.style.color='#000000';});
					     $(".commission").each(function(i) {this.style.color='#000000';});
					     $(".bonus").each(function(i) {this.style.color='#000000';});
					     $(".total_win_los").each(function(i) {this.style.color='#000000';});//pl
					     $(".current_balance").each(function(i) {this.style.color='#000000';});
					     $(".p_l").each(function(i) {this.style.color='#000000';});//pl

				    	 $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//total_winloss
					     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					     $(".p_l").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					} 
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			   
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager5',
			    sortname: 'currency',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">SAVAN VEGAS</label> Member Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
				groupingView : {
			   		groupField : ['currency'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   
			   	
			});
			$('#list5').jqGrid('navGrid', '#pager5', {edit: false, add: false, del:false, search: false});

			jQuery("#list5").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_los', numberOfColumns: 4, titleText: '<label ><b>PLAYER</b></label>'},
				
				  ]
			});
			jQuery("#list5").jqGrid('navButtonAdd','#pager5',{
	            caption:"Export current page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportSavanWinLossToExcel();
	           }, 
	            position:"last"
	        });   
			jQuery("#list5").jqGrid('navButtonAdd','#pager5',{
	            caption:"Export all pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllSavanWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId);
	           }, 
	            position:"last"
	        });      
			
		});
	} 
	function exportSavanWinLossToExcel(){
		var accountId=$('#txtAccountId')[0].value;
		var tableId=$('#txtTableId')[0].value;
		var gameId =$('#txtGameId')[0].value;
		var shoesId=$('#txtShoesId')[0].value;
		var gameType=$('#gameType')[0].value;
		var currency=$('#currencyType')[0].value;
		testChecked=0;
		if (document.getElementById("chkTest").checked==true){
			 testChecked=1;
		}
		var testC	=	testChecked;
		var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
		var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
		
		var table1= document.getElementById('qry_resultSAVAN');
		var table2= document.getElementById('qry_result_SAVAN_summary');
		var html1 = table1.outerHTML;
		var html2 = table2.outerHTML;
		document.forms[2].csvBuffer.value="";
		if (tableId==''){
			document.forms[2].txtParams.value=$('.ui-paging-info')[2].innerHTML+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
		}else{
			document.forms[2].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
		}
		document.forms[2].csvBuffer.value=html1+''+html2 ;
	    document.forms[2].method='POST';
	    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/SavanWinLossExcel';  // send it to server which will open this contents in excel file
	    document.forms[2].target='_top';
	    document.forms[2].submit(); 
	}
	function exportAllSavanWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId){
	    $.ajax({	
			 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/SavanWinLossExcelAll&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId, 
			 async:true,
			 success: function(result) {
					var data =result.split("<BREAK>");
					
					var accountId=$('#txtAccountId')[0].value;
					var tableId=$('#txtTableId')[0].value;
					var gameId =$('#txtGameId')[0].value;
					var shoesId=$('#txtShoesId')[0].value;
					var gameType=$('#gameType')[0].value;
					var currency=$('#currencyType')[0].value;
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					var testC	=	testChecked;
					var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
					var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
					
					document.forms[2].csvBuffer.value="";
					if (tableId==''){
						document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
					}else{
						document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+tableId+'#'+gameId+'#'+shoesId+'#'+gameType+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
					}
					document.forms[2].csvBuffer.value=data[0]+""+data[2];
				    document.forms[2].method='POST';
				    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/SAVANWinLossExcel';  // send it to server which will open this contents in excel file
				    document.forms[2].target='_top';
				    document.forms[2].submit(); 
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown) { 
		 }});
	}
	// SAVAN ALL PAGES SUMMARY
	function tableWinLossSAVANTotal(id,currency_id,gameType,dateFrom,dateTo,testChecked,tableId,gameId,shoesId) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list6'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_SAVAN_summary").appendChild(divTag);
	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager6'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_SAVAN_summary").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list6");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossV1/CashPlayerWinLossSAVANTotal&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Currency','Bet Count','Total Stake','Valid Stake','Win/Loss','Commission','Bonus','<b>Total</b>','<b>P/L</b>','Ex-Rate','UC Total Stake','UC Valid Stake','UC WinLoss','UC Commission','UC Bonus','UC Total','UC P/L'],
			    colModel: [
					{name: 'currency', index: 'currency', width: 60,title:false},
					{name: 'bet_count', index: 'bet_count', width: 70, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_stake', index: 'total_stake', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'amount_wager', index: 'amount_wager', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'bonus', index: 'bonus', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_win_los', index: 'total_win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'rate', index: 'rate', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','},hidden:true},
					{name: 'total_stake1', index: 'total_stake1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'amount_wager1', index: 'amount_wager1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'win_los1', index: 'win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>',hidden:true},
					{name: 'commission1', index: 'commission1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'bonus1', index: 'bonus1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'total_win_los1', index: 'total_win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>',hidden:true},
					{name: 'p_l1', index: 'p_l1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
				    ],

				loadtext:"",
				loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"currency","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bet_count","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_stake","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_wager","",{background:'#D84A38',color:'white'})
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"p_l","",{background:'#D84A38',color:'white'});
				    	
						
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'yellow'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'yellow'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'yellow'});	
					    }			    
					}   
			    },
			    hidegrid: false,  
			    rowNum: 25,	
			    rowList: [25, 50, 75],
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">SAVAN VEGAS</label>Win/Loss Summary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convert this to </label><select id="currencyTypeConvertSAVAN" style="width:80px" onchange="javascript: convertSAVAN();"><option value="0-0">--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>',
			    viewrecords: true,
			    footerrow:true,
			    
			});
			$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false});

			
		});
	} 

	function convertSAVAN(){
    	var comboValue=document.getElementById("currencyTypeConvertSAVAN").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	

		if (comboRange==0){
   		 $('#list6').trigger("reloadGrid");
   		 $('#list6').jqGrid('footerData','set', {total_stake:'',amount_wager:'',win_los:'',commission:'',total_win_los:'', bonus:'',p_l:''});   
   		 return false;
       }
    
		var noRow=$("#list6").getGridParam("reccount");
		var rate;
		var totalStake;
		var stake;
		var winloss;
		var commission;
		var total;
		var bonus;
		var pl;
		var totalStakeconversion;
		var stakeConversion;
		var totalConversion;
		var winlossConversion;
		var commissionConversion;
		var bonusConversion;
		var plConversion;

	    var x=1;
	    while (x<=noRow)
	      {
	    	rate=$("#list6").getCell(x, 'rate');
	    	totalStake=$("#list6").getCell(x, 'total_stake1');
	    	stake=$("#list6").getCell(x, 'amount_wager1');
	    	winloss=$("#list6").getCell(x, 'win_los1');
	    	commission=$("#list6").getCell(x, 'commission1');
			total=$("#list6").getCell(x, 'total_win_los1');
			bonus=$("#list6").getCell(x, 'bonus1');
			pl=$("#list6").getCell(x, 'p_l1');
				totalStakeconversion=((parseFloat(totalStake)/parseFloat(rate)) * parseFloat(comboRange));
				stakeConversion=((parseFloat(stake)/parseFloat(rate)) * parseFloat(comboRange));
	    		winlossConversion=((parseFloat(winloss)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	commissionConversion=((parseFloat(commission)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		bonusConversion=((parseFloat(bonus)/parseFloat(rate)) * parseFloat(comboRange));
	    		plConversion=((parseFloat(pl)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list6").jqGrid('setCell', x, 0, comboName);
	    		jQuery("#list6").jqGrid('setCell', x, 2, totalStakeconversion);
	    		jQuery("#list6").jqGrid('setCell', x, 3, stakeConversion);
	    		jQuery("#list6").jqGrid('setCell', x, 4, winlossConversion);
	    		jQuery("#list6").jqGrid('setCell', x, 5, commissionConversion);
	    	    jQuery("#list6").jqGrid('setCell', x, 6, bonusConversion);
	    		jQuery("#list6").jqGrid('setCell', x, 7, totalConversion);	
	    		jQuery("#list6").jqGrid('setCell', x, 8, plConversion);	
			
	     	x++;
	      }
	    var grid = $("#list6"),
	    sumTotalStake=grid.jqGrid('getCol', 'total_stake', false, 'sum');
        sumStake= grid.jqGrid('getCol', 'amount_wager', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_los', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');
        sumBonus=grid.jqGrid('getCol', 'bonus', false, 'sum');
        sumPl=grid.jqGrid('getCol', 'p_l', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake:sumTotalStake,amount_wager:sumStake,win_los:sumWinLoss,commission:sumCommission,total_win_los:sumTotal, bonus: sumBonus,p_l:sumPl});  
	}
</script>

<script type="text/javascript">
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}

function validate()
{
	var pattern = /^-?[-\0-9]+(.[0-9]{1,2})?$/; 
	var text = document.getElementById('txtVIGSharing1').value;
    if (text.match(pattern)==null) 
    {
		alert('Invalid VIG sharing value.');
		return false;
    }
	else
	{
	  return true;
	}
}
var sChar=false;
function alphanumeric(inputtxt)  
{  
	var letters = /^[0-9a-zA-Z]+$/;  
	if(inputtxt.match(letters) || inputtxt=='')  
	{  
		sChar=false;  
	}else{  
		sChar=true;  
	}  
} 
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});


	function btnclick(btnname)
	{
		tableId=document.getElementById("txtTableId").value;
		gameId=document.getElementById("txtGameId").value;
		shoesId=document.getElementById("txtShoesId").value;
		var accountId='';
		if (btnname=="Load")
		{
			document.getElementById("chkTest").checked=true;
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			<?php if(isset($_GET['dateFrom'])=='') {?>
				var mydate= new Date();
				mydate.setDate(mydate.getDate());
				var theyear=mydate.getFullYear();
				var themonth=mydate.getMonth()+1;
				var theday=mydate.getDate();
				
				var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
				var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
	
				var currentTime = new Date();
				var month = currentTime.getMonth() + 1;
				var day = currentTime.getDate();
				
				var year = currentTime.getFullYear();
			
				var dayL=day.toString();
				var monthL=month.toString();
	
				if (monthL.length==1){
					month="0"+month;
				}
				if (dayL.length==1){
					day="0"+day;
				}
				document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
				document.getElementById("dateto").value=(year + "-" + month + "-" + day);
				accountId='All';
				
			<?php }else{?>
				
				var datefrom=enc('<?php echo $_GET['dateFrom'];?>');
				var dateto=enc('<?php echo $_GET['dateTo'];?>');
				id='<?php echo $_GET['id'];?>';
				var from=datefrom.split("_");
				var to=dateto.split("_");
				
				datefrom=from[0].replace("-","/").replace("-","/")+"_00:00:00";
				dateto=to[0].replace("-","/").replace("-","/")+"_23:59:59";
				document.getElementById('datefrom').value=from[0].replace("-","/").replace("-","/");
				document.getElementById('dateto').value=to[0].replace("-","/").replace("-","/");
				document.getElementById('txtAccountId').value=id;
				accountId=id;
				if (accountId==''){
					accountId='All';
				}else{
					accountId=id;
				}
			<?php }?>
			var vigSharing=document.getElementById("txtVIGSharing").value;
			
				<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVWinLoss')){?>
					document.getElementById('qry_resultHTV').innerHTML='';
					tableWinLossTodayHTV(accountId,"All","All",datefrom,dateto,testChecked,tableId,gameId,shoesId);
	
					document.getElementById('qry_result_HTV_summary').innerHTML='';
					tableWinLossHTVTotal(accountId,"All","All",datefrom,dateto,testChecked,tableId,gameId,shoesId);
				<?php }?>
				<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanWinLoss')){?>
					document.getElementById('qry_resultSAVAN').innerHTML='';
					tableWinLossTodaySAVAN(accountId,"All","All",datefrom,dateto,testChecked,tableId,gameId,shoesId);
	
					document.getElementById('qry_result_SAVAN_summary').innerHTML='';
					tableWinLossSAVANTotal(accountId,"All","All",datefrom,dateto,testChecked,tableId,gameId,shoesId);
				<?php }?>
						
		}
		else if (btnname=="Today")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			var dayL=theday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theday="0"+theday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			var vigSharing=document.getElementById("txtVIGSharing").value;
			if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
				alert('Invalid VIG sharing value.');
				return false;
			}
			alphanumeric(accountId+gameId+shoesId);
			
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVWinLoss')){?>
				if (sChar==false){
					document.getElementById('qry_resultHTV').innerHTML='';
					tableWinLossTodayHTV(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
					document.getElementById('qry_result_HTV_summary').innerHTML='';
					tableWinLossHTVTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
				}
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanWinLoss')){?>
				if (sChar==false){
					document.getElementById('qry_resultSAVAN').innerHTML='';
					tableWinLossTodaySAVAN(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
					document.getElementById('qry_result_SAVAN_summary').innerHTML='';
					tableWinLossSAVANTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
				}
			<?php }?>
		}
		else if (btnname=="Yesterday")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			var dayL=theyday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theyday="0"+theyday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theyday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theyday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			var vigSharing=document.getElementById("txtVIGSharing").value;
			var vigSharing=document.getElementById("txtVIGSharing").value;
			if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
				alert('Invalid VIG sharing value.');
				return false;
			}
			alphanumeric(accountId+gameId+shoesId);
			
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVWinLoss')){?>
				if (sChar==false){
					document.getElementById('qry_resultHTV').innerHTML='';
					tableWinLossTodayHTV(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
					document.getElementById('qry_result_HTV_summary').innerHTML='';
					tableWinLossHTVTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
				}
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanWinLoss')){?>
				if (sChar==false){
					document.getElementById('qry_resultSAVAN').innerHTML='';
					tableWinLossTodaySAVAN(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
					document.getElementById('qry_result_SAVAN_summary').innerHTML='';
					tableWinLossSAVANTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked,tableId,gameId,shoesId);
				}
			<?php }?>
		}
		else if (btnname=="Submit")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			var vigSharing=document.getElementById("txtVIGSharing").value;
			if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
				alert('Invalid VIG sharing value.');
				return false;
			}
			
			<?php if(isset($_GET['dateFrom'])!='') {?>
				var datefrom=enc('<?php echo $_GET['dateFrom'];?>');
				var dateto=enc('<?php echo $_GET['dateTo'];?>');

				var from=datefrom.split("_");
				var to=dateto.split("_");
				
				dateSubmitFrom=from[0].replace("-","/").replace("-","/")+"_00:00:00";
				dateSubmitTo=to[0].replace("-","/").replace("-","/")+"_23:59:59";
				document.getElementById('datefrom').value=from[0].replace("-","-").replace("-","-");
				document.getElementById('dateto').value=to[0].replace("-","-").replace("-","-");
			<?php }?>
			
			alphanumeric(accountId+gameId+shoesId);
			
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVWinLoss')){?>
				if (sChar==false){
					document.getElementById('qry_resultHTV').innerHTML='';
					tableWinLossTodayHTV(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked,tableId,gameId,shoesId);
					document.getElementById('qry_result_HTV_summary').innerHTML='';
					tableWinLossHTVTotal(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked,tableId,gameId,shoesId);
				}
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanWinLoss')){?>
				if (sChar==false){
				document.getElementById('qry_resultSAVAN').innerHTML='';
				tableWinLossTodaySAVAN(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked,tableId,gameId,shoesId);
				document.getElementById('qry_result_SAVAN_summary').innerHTML='';
				tableWinLossSAVANTotal(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked,tableId,gameId,shoesId);
				}
			<?php }?>
		}
		
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
div.dateSummaryFooter { text-align: Left; }
tr.jqfoot td { background-color: #AD8533; color: #FFF; }
tr.footrow-ltr td{color:black;}
</style>

</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area" style="width: 500px;height: 350px">
	<div class="header" >Member Win/Loss</div>
	<form action="">
		<table style="background-color:transparent; width: 500px;margin-top: 5px;">
		<tr>
			<td width="25%" style="padding-left: 5px;">ACCOUNT ID:</td>
			<td width="75%"><input type="text" id="txtAccountId" style="width: 150px;" onkeydown="if (event.keyCode == 13){btnclick('Submit');}"></td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">TABLE ID:</td>
			<td>
				<select id="txtTableId">
					<option value="">All</option>
	  				<?php 
					$dataReader = TableManagement::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. $row['table_name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		</tr>
		<tr>
			<td width="25%" style="padding-left: 5px;">GAME ID:</td>
			<td width="75%"><input type="text" id="txtGameId" style="width: 150px;" onkeydown="if (event.keyCode == 13){btnclick('Submit');}"></td>
		</tr>
		<tr>
			<td width="25%" style="padding-left: 5px;">SHOES ID:</td>
			<td width="75%"><input type="text" id="txtShoesId" style="width: 150px;" onkeydown="if (event.keyCode == 13){btnclick('Submit');}"></td>
		</tr>
		<tr hidden="true">
			<td width="25%" style="padding-left: 5px;">VIG SHARING:</td>
			<td width="75%"><input type="text" id="txtVIGSharing" style="width: 150px;" value="10"></td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">GAME TYPE:</td>
			<td>
				<select id="gameType">
					<option value="All">All</option>
	  				<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. $row['game_name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">CURRENCY TYPE:</td>
			<td>
				<select id="currencyType" style="width: 99px" onChange="javascript:changeCurrency();">
					<option value="All">ALL</option>
  					<?php 
					$dataReader = CashPlayerCurrencyType::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . strtoupper($row['currency_name']) . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>&nbsp;&nbsp;&nbsp;
				<input type="checkbox" id="chkTest" checked ><label id="lblExcept">Except TEST currency</label>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">FROM:</td>
			<td>
			<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px;width:50px; background-color: transparent" disabled></td>
		</tr>
		<tr><td style="padding-left: 5px;">TO:</td>
			<td>
			<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; width:50px; background-color: transparent" disabled="disabled" ondblclick="this.disabled=false;">
			</td>
		</tr>
		</table>
		<div align="center" style="padding-top: 3px;">
			<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
			<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
			<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
			<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red">
		</div>
		</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		//if (document.getElementById('defaultTimeFrom').value==''){
		document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
		document.getElementById("dateto").value=(day + "-" + month + "-" + year);
		//}else{
		//	var datefrom=enc(document.getElementById('defaultTimeFrom').value);
		//	var dateto=enc(document.getElementById('defaultTimeTo').value);
		//	document.getElementById("datefrom").value=datefrom;
		//	document.getElementById("dateto").value=dateto;
		//}

		function changeCurrency(){
			var lblExcept = document.getElementById('lblExcept');
			var chkTest = document.getElementById('chkTest');
			if (document.getElementById('currencyType').value != "All"){
				chkTest.style.visibility = 'hidden';
				lblExcept.style.visibility = 'hidden';
				chkTest.checked=false;
			 }else{
				chkTest.style.visibility = 'visible';
				lblExcept.style.visibility = 'visible';
		     }
		}
		</script>
</div>
		
	<div id="qry_resultHTV"></div>
	<br/>
	<div id="qry_result_HTV_summary"></div>
	<br/><br/><br/>
	<div id="qry_resultSAVAN"></div>
	<br/>
	<div id="qry_result_SAVAN_summary"></div>
	<!--  <div id="pager2"></div>-->
	<input type="hidden" id="defaultTimeFrom" value="<?php  if (isset($_GET['dateFrom'])){ echo $_GET['dateFrom'];}?>" > <input type="hidden" id="defaultTimeTo" value="<?php  if (isset($_GET['dateTo'])){ echo $_GET['dateTo'];}?>">
	
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
	</form>

</body>
</html>