<?php
/**
 * @todo CurrencySettingLogController
 * @copyright CE
 * @author allan
 * @since 2012-06-29
 */
class CurrencySettingLogController extends MyController
{
	public function actionCurrencySettingLog()
	{

		$cpHfund = new CurrencySettingLog();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountCurrencySettingLog();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getCurrencySettingLog($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("operated_by","operated_by_level","operated","operated_level","operation_time","log_details");
		$urlAS= Yii::app()->request->baseUrl;

		$htmvalue= '';
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,9);
	
			
	}
	
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readCurrencySetting'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	
	}
}
	