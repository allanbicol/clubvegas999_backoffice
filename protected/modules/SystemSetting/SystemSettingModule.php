<?php

class SystemSettingModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'SystemSetting.models.*',
			'SystemSetting.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	
	/**
	 * @todo getAssetsUrl Model
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-08
	 */
	private $_assetsUrl;
	public function getAssetsUrl()
	{
	 // getAssetsUrl()
	 //    return the URL for this module's assets, performing the publish operation
	 //    the first time, and caching the result for subsequent use.
		if ($this->_assetsUrl === null)
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(
					Yii::getPathOfAlias('SystemSetting.assets') );
		return $this->_assetsUrl;
	}
}
