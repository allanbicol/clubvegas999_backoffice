<?php
/**
 * @todo Online Management Model
 * @copyright CE
 * @author Leo karl
 * @since 5/16/2012
 *
 */

Class OnlineManagement
{
	public function getOnlineList($orderField, $sortType, $startIndex , $limit)
	{
		//echo $_GET['level_id'];
		
		$connection = Yii::app()->db_cv999_fd_master;
		if($_GET['dtFrom']!=''){
			if($_GET['level_id']=='All'){
				$command = $connection->createCommand("SELECT * from vwGetAllUsersOnline WHERE login_date>='" . $_GET['dtFrom'] . "' AND login_date<='" . $_GET['dtTo'] . "' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT * from vwGetAllUsersOnline WHERE level='". $_GET['level_id'] ."' AND login_date>='" . $_GET['dtFrom'] . "' AND login_date<='" . $_GET['dtTo'] . "'  ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}else{
			if($_GET['level_id']=='All'){
				$command = $connection->createCommand("SELECT * from vwGetAllUsersOnline WHERE casino_id!=0 ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT * from vwGetAllUsersOnline WHERE level='". $_GET['level_id'] ."' AND casino_id!=0 ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		$rows = $command->query();
		return $rows;
	}
	public function getCountOnlineList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if($_GET['dtFrom']!=''){
			if($_GET['level_id']=='All'){
				$command = $connection->createCommand("SELECT COUNT(0) from vwGetAllUsersOnline WHERE login_date>='" . $_GET['dtFrom'] . "' AND login_date<='" . $_GET['dtTo'] . "'");
			}else{
				$command = $connection->createCommand("SELECT COUNT(0) from vwGetAllUsersOnline WHERE level='". $_GET['level_id'] ."' AND login_date>='" . $_GET['dtFrom'] . "' AND login_date<='" . $_GET['dtTo'] . "'");
			}
		}else{
			if($_GET['level_id']=='All'){
				$command = $connection->createCommand("SELECT COUNT(0) from vwGetAllUsersOnline WHERE casino_id!=0");
			}else{
				$command = $connection->createCommand("SELECT COUNT(0) from vwGetAllUsersOnline WHERE level='". $_GET['level_id'] ."' AND casino_id!=0");
			}
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getLockList($orderField, $sortType, $startIndex , $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		//$command = $connection->createCommand("SELECT * from vwCheckPlayerLock WHERE session_date>='" . $_GET['dtFrom'] . "' AND session_date<='" . $_GET['dtTo'] . "' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$command = $connection->createCommand("SELECT * from vwCheckPlayerLock ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	public function getCountLockList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		//$command = $connection->createCommand("SELECT COUNT(0) from vwCheckPlayerLock WHERE session_date>='" . $_GET['dtFrom'] . "' AND session_date<='" . $_GET['dtTo'] . "'");
		$command = $connection->createCommand("SELECT COUNT(0) from vwCheckPlayerLock");
		$rows = $command->query();
		return $rows;
	}
	
	/*
	 * @todo check if balance transfer from lobby is still processing
	 * @author leokarl
	 * @date 2012-12-08
	 */
	public function balanceTransferStatus($account_id){
		$command = Yii::app()->db->createCommand("SELECT deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='" . $account_id . "' and deposit_withdrawal<>0");
		$rd=$command->queryRow();
		
		return ($rd['deposit_withdrawal'] == 1) ? 1 : 0;
	}
}