<?php
/**
 * @todo AgentNewSubCompany Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-10
 */
class CashPlayerSetting
{
	public function saveCashPlayerSetting()
	{
		//Get previous sharing and commission- Added by Allan |date:2012-07-12
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		$winlosslimit_model = new WinLossLimit();
		$daily_max_win_before = $this->getFieldData($_GET['account_id'],'max_win');
		$max_win__multiple_before = $this->getFieldData($_GET['account_id'],'max_win_multiple');
		$return_bool = 1;
		$transaction = $connection->beginTransaction(); // begin transaction
 		try{
			$command = $connection->createCommand("SELECT * from tbl_cash_player  WHERE account_id='" . $_GET['account_id'] . "'");
			$rd=$command->queryRow();
			$bac_limit=$rd['limit_baccarat'];
			$rou_limit=$rd['limit_roulette'];
			$dra_limit=$rd['limit_dragon_tiger'];
			$bla_limit=$rd['limit_blackjack'];
			$ame_limit=$rd['limit_american_roulette'];
			$slo_limit=$rd['limit_slots'];
			$bac_commission=$rd['commission_baccarat'];
			$rou_commission=$rd['commission_roulette'];
			$dra_commission=$rd['commission_dragon_tiger'];
			$bla_commission=$rd['commission_blackjack'];
			$ame_commission=$rd['commission_american_roulette'];
			$slo_commission=$rd['commission_slots'];
			$password=$rd['player_password'];
			$casinoAccessLevel=$rd['casino_access_level'];
			$enableTip =$rd['enable_tip'];
			
			//$post=TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
			$casinoLevel=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv and sb=:sb and svs=:svs','params'=>array(':htv'=>$_GET['htv999'],':sv'=>$_GET['savan999'],':vig'=>$_GET['costa999'],':vv'=>$_GET['virtua999'],':sb'=>$_GET['sport999'],':svs'=>$_GET['slot999'])));
			if ($casinoLevel['level_id']==''){
				$postLevelSetting= new TableCasinoAccessLevelSetting;
				$postLevelSetting->htv = $_GET['htv999'];
				$postLevelSetting->sv  = $_GET['savan999'];
				$postLevelSetting->vig = $_GET['costa999'];
				$postLevelSetting->vv  = $_GET['virtua999'];
				$postLevelSetting->sb  = $_GET['sport999'];
				$postLevelSetting->svs = $_GET['slot999'];
				$postLevelSetting->save();
				
				$casinoLevel=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv and sb=:sb and svs=:svs','params'=>array(':htv'=>$_GET['htv999'],':sv'=>$_GET['savan999'],':vig'=>$_GET['costa999'],':vv'=>$_GET['virtua999'],':sb'=>$_GET['sport999'],':svs'=>$_GET['slot999'])));
			}
			
			
			$post=TableCashPlayer::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
			$post->limit_baccarat=$_GET['selected_baccarat_limit'];
			$post->limit_roulette=$_GET['selected_roullete_limit'];
			$post->limit_dragon_tiger=$_GET['selected_dragon_tiger_limit'];
			$post->limit_blackjack=$_GET['selected_blackjack_limit'];
			$post->limit_american_roulette=$_GET['selected_american_roulette_limit'];
			$post->limit_slots=$_GET['slot'];
			$post->commission_baccarat=$_GET['baccarat_commission'];
			$post->commission_roulette=$_GET['roullete_commission'];
			$post->commission_dragon_tiger=$_GET['dragontiger_commission'];
			$post->commission_blackjack=$_GET['blackjack_commission'];
			$post->commission_american_roulette=$_GET['american_roulette_commission'];
			$post->commission_slots=$_GET['slot_commission'];
			$post->max_win_multiple=$_GET['max_win_multiple'];
			//$post->max_win=$_GET['daily_max_win'];
			if(Yii::app()->user->checkAccess('cashplayer.writeCashPlayerPassword')){
				if ($_GET['password']!=''){
					$post->player_password=CV999Utility::generateOneWayPassword($_GET['password']);
				}
			}
			$post->enable_tip=$_GET['enableTip'];
			$post->casino_access_level=$casinoLevel['level_id'];
			
			$post->save();
			
			if ($casinoAccessLevel!=$casinoLevel['level_id']){
				
				$oldCasinoLevel=TableCasinoAccessLevelSetting::model()->find(array('select'=>'*','condition'=>'level_id=:lvl_id','params'=>array(':lvl_id'=>$casinoAccessLevel)));
				$newCasinoLevel=TableCasinoAccessLevelSetting::model()->find(array('select'=>'*','condition'=>'level_id=:lvl_id','params'=>array(':lvl_id'=>$casinoLevel['level_id'])));
				
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['account_id'];
				$postLog->operated_level='Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=14;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Casino Access Level from <label style=\"color:blue\">[HTV='.$oldCasinoLevel['htv'].',SAVAN='.$oldCasinoLevel['sv'].',COSTA='.$oldCasinoLevel['vig'].',VIRTUA='.$oldCasinoLevel['vv'].',SPORTBOOK='.$oldCasinoLevel['sb'].',SLOTVEGAS='.$oldCasinoLevel['svs'].']</label> to <label style=\"color:red\">[HTV='.$newCasinoLevel['htv'].',SAVAN='.$newCasinoLevel['sv'].',COSTA='.$newCasinoLevel['vig'].',VIRTUA='.$newCasinoLevel['vv'].',SPORTBOOK='.$newCasinoLevel['sb'].',SLOTVEGAS='.$newCasinoLevel['svs'].']</label>.</b>';
				$postLog->save();
			}
			
			if($max_win__multiple_before != $_GET['max_win_multiple']){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['account_id'];
				$postLog->operated_level='Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=12;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Max Balance from <label style=\"color:blue\">'.$max_win__multiple_before.'</label> to <label style=\"color:red\">'.$_GET['max_win_multiple'].'</label>.</b>';
				$postLog->save();
			}
// 			if($daily_max_win_before != $_GET['daily_max_win']){
// 				$postLog=new TableLog;
// 				$postLog->operated_by=$AccountID;
// 				$postLog->operated_by_level=$level;
// 				$postLog->operated=$_GET['account_id'];
// 				$postLog->operated_level='Player';
// 				$postLog->operation_time=$dateTime;
// 				$postLog->log_type_id=12;
// 				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Daily Max Win from <label style=\"color:blue\">'.$daily_max_win_before.'</label> to <label style=\"color:red\">'.$_GET['daily_max_win'].'</label>.</b>';
// 				$postLog->save();
// 			}
			
			// Start commission logs
			if ($password!=CV999Utility::generateOneWayPassword($_GET['password']) && $_GET['password']!='' ){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=10;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE player <label style=\"color:green\">'.$_GET['account_id'].'</label> password.</b>';
						$postLog->save();
			}
			if ($bac_commission!=$_GET['baccarat_commission']){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=7;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE player commission from:<label style=\"color:red\">'.$bac_commission.'</label> to:<label style=\"color:red\">'.$_GET['baccarat_commission'].'</label> for baccarat.</b>';
						$postLog->save();
			}
			if ($rou_commission!=$_GET['roullete_commission']){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=7;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE player commission from:<label style=\"color:red\">'.$rou_commission.'</label> to:<label style=\"color:red\">'.$_GET['roullete_commission'].'</label> for European roullete.</b>';
						$postLog->save();
			}
			if ($dra_commission !=$_GET['dragontiger_commission']){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=7;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE player commission from:<label style=\"color:red\">'.$dra_commission.'</label> to:<label style=\"color:red\">'.$_GET['dragontiger_commission'].'</label> for Dragontiger.</b>';
						$postLog->save();
			}
			if ($bla_commission !=$_GET['blackjack_commission']){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=7;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE player commission from:<label style=\"color:red\">'.$bla_commission.'</label> to:<label style=\"color:red\">'.$_GET['blackjack_commission'].'</label> for Blackjack.</b>';
						$postLog->save();
			}
	       if ($ame_commission !=$_GET['american_roulette_commission']){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=7;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE player commission from:<label style=\"color:red\">'.$ame_commission.'</label> to:<label style=\"color:red\">'.$_GET['american_roulette_commission'].'</label> for American roulette.</b>';
						$postLog->save();
			}
			if ($slo_commission!=$_GET['slot_commission']){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=7;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE player commission from:<label style=\"color:red\">'.$slo_commission.'</label> to:<label style=\"color:red\">'.$_GET['slot_commission'].'</label> for slots.</b>';
						$postLog->save();
			}
			//end commsion logs
			
			//start table limit logs
			if ($bac_limit!=$_GET['selected_baccarat_limit'] && $_GET['selected_baccarat_limit']!=0){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=11;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> SELECTED table limit id: [<label style=\"color:red\">'.$_GET['selected_baccarat_limit'].'</label>] for baccarat.</b>';
						$postLog->save();
			}
			if ($rou_limit!=$_GET['selected_roullete_limit'] && $_GET['selected_roullete_limit']!=0){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=11;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> SELECTED table limit id: [<label style=\"color:red\">'.$_GET['selected_roullete_limit'].'</label>] for European roullete.</b>';
						$postLog->save();
			}
			if ($dra_limit!=$_GET['selected_dragon_tiger_limit'] && $_GET['selected_dragon_tiger_limit']!=0){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=11;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> SELECTED table limit id: [<label style=\"color:red\">'.$_GET['selected_dragon_tiger_limit'].'</label>] for Dragontiger.</b>';
						$postLog->save();
			}
			if ($bla_limit!=$_GET['selected_blackjack_limit'] && $_GET['selected_blackjack_limit']!=0){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=11;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> SELECTED table limit id: [<label style=\"color:red\">'.$_GET['selected_blackjack_limit'].'</label>] for Blackjack.</b>';
						$postLog->save();		
			}
			if ($ame_limit!=$_GET['selected_american_roulette_limit'] && $_GET['selected_american_roulette_limit']!=0){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=11;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> SELECTED table limit id: [<label style=\"color:red\">'.$_GET['selected_blackjack_limit'].'</label>] for American roulette.</b>';
						$postLog->save();	
			}
			if ($slo_limit!=$_GET['slot'] && $_GET['slot']!=0){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=11;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> SELECTED table limit id: [<label style=\"color:red\">'.$_GET['slot'].'</label>] for slot.</b>';
						$postLog->save();	
			}
			
			if ($enableTip!=$_GET['enableTip']){
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=32;
						if ($_GET['enableTip']==1){
							$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> Enable Tip</b>';
						}else{
							$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> Disable Tip</b>';
						}
						$postLog->save();
			}
			//end table limit logs
			
			
// 			if($daily_max_win_before != $_GET['daily_max_win']){ // submit daily winloss if change
				
// 				$return_val = $winlosslimit_model->submitWinLossLimit($_GET['account_id'], $this->getPlayerCurrencyName($_GET['account_id']), $_GET['daily_max_win']);
// 				if(isset($return_val['response']['status']) && $return_val['response']['status'] == 'OK'){
// 					$transaction->commit();
// 					return TRUE;
// 				}else{
// 					//exit($return_val['response']['description']);
// 					exit('error_submit_win_loss_limit');
// 				}
// 			}else{
				$transaction->commit();
				$return_bool = 1;//return TRUE;
				
				// submit daily winlos
				if($daily_max_win_before != $_GET['daily_max_win']){ 
				
					$return_val = $winlosslimit_model->submitWinLossLimit($_GET['account_id'], $this->getPlayerCurrencyName($_GET['account_id']), $_GET['daily_max_win']);
					if(isset($return_val['response']['status']) && $return_val['response']['status'] == 'OK'){
// 						$transaction->commit();
// 						return TRUE;
						$post=TableCashPlayer::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
						$post->max_win=$_GET['daily_max_win'];
						$post->save();
						
						$postLog=new TableLog;
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_GET['account_id'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=12;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Daily Max Win from <label style=\"color:blue\">'.$daily_max_win_before.'</label> to <label style=\"color:red\">'.$_GET['daily_max_win'].'</label>.</b>';
						$postLog->save();
					}else{
						//exit($return_val['response']['description']);
						exit('error_submit_win_loss_limit');
					}
				}
				
//			}
			
		}catch(Exception $e){
			
			$transaction->rollBack();
			$return_bool = 0;//return FALSE;
			
		}
		
		return ($return_bool == 1) ? TRUE : FALSE;
	}
	
	/**
	 * @todo get player's currency
	 * @author leokarl
	 * @since 2013-02-14
	 * @param string $account_id
	 */
	public function getPlayerCurrencyName($account_id){
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT b.currency_name FROM tbl_cash_player a INNER JOIN tbl_currency b ON b.id=a.currency_id WHERE a.account_id = '". $account_id ."'");
		$rows = $command->query();
		$currency = $rows->readAll();
		return $currency[0]['currency_name'];
	}
	
	/**
	 * @todo get player's data
	 * @author leokarl
	 * @since 2013-02-14
	 * @param string $account_id
	 */
	public function getFieldData($account_id, $field_name){
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT $field_name FROM tbl_cash_player WHERE account_id = '". $account_id ."'");
		$rows = $command->query();
		$currency = $rows->readAll();
		return $currency[0][$field_name];
	}

}