<?php

class CashPlayerSettingController extends MyController
{
	public function actionCommision()
	{
		$cpd = new CashPlayerCommision();
		$cpd->getCashPlayerCommission();
	}
	public function actionCashPlayerGameTableLimit()
	{
		$tl = new CashPlayerGameTableLimit();
		$tl->getGameTableLimit();
	}
	public function actionSaveCashPlayerSetting()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerSetting'))
		{
			if(isset($_GET['account_id'])){
				if(!is_numeric($_GET['daily_max_win'])){
					exit('invalid_daily_max_win');
				}
				if($_GET['daily_max_win'] < 0){
					exit('invalid_daily_max_win');
				}
				$setting = new CashPlayerSetting();
				if($setting->saveCashPlayerSetting()){
					
					exit('done');
				}else{
					exit('error_occured');
				}
			}else{
				exit('Data not set properly. Please try again!');
			}
		}
		else
			echo "You don't have permission to save the setting!";
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerSetting'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}