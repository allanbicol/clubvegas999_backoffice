<?php
/**
 * @todo custom report
 * @author allan
 * @since 2013-04-12
 */
class CustomReport1Controller extends MyController
{
	public function actionIndex() {
		yii::app()->session['pager']=(isset($_POST['pageSize'])) ? $_POST['pageSize'] : yii::app()->session['pager'];
		yii::app()->session['dateFrom']= (isset($_POST['dateFrom'])) ? $_POST['dateFrom'] : yii::app()->session['dateFrom'];
		yii::app()->session['dateTo']= (isset($_POST['dateTo'])) ? $_POST['dateTo'] : yii::app()->session['dateTo'];
		yii::app()->session['accountID']= (isset($_POST['accountID'])) ? $_POST['accountID'] : yii::app()->session['accountID'];
		yii::app()->session['currency']= (isset($_POST['currency'])) ? $_POST['currency'] : yii::app()->session['currency'];
		yii::app()->session['operator']= (isset($_POST['operator'])) ? $_POST['operator'] : yii::app()->session['operator'];
		yii::app()->session['winloss']= (isset($_POST['winloss'])) ? $_POST['winloss'] : yii::app()->session['winloss'];
		yii::app()->session['country']= (isset($_POST['country'])) ? $_POST['country'] : yii::app()->session['country'];
		yii::app()->session['email']= (isset($_POST['email'])) ? $_POST['email'] : yii::app()->session['email'];
		yii::app()->session['phone']= (isset($_POST['phone'])) ? $_POST['phone'] : yii::app()->session['phone'];
		yii::app()->session['deposit']= (isset($_POST['deposit'])) ? $_POST['deposit'] : yii::app()->session['deposit'];
		yii::app()->session['playertype']= (isset($_POST['playertype'])) ? $_POST['playertype'] : yii::app()->session['playertype'];
		
		yii::app()->session['selected_columns']=(isset($_POST['columns'])) ? $_POST['columns']."#".$_POST['column_names'] : yii::app()->session['selected_columns'];
		yii::app()->session['unselected_columns']=(isset($_POST['unselectedColumns'])) ? $_POST['unselectedColumns']."#".$_POST['unselectedColumn_names'] : yii::app()->session['unselected_columns'];
		//parameters initialization------------------------------------This initialization used to save parameters in session to reload in the next page in pager
		if (isset(yii::app()->session['pager'])){
			$pager=yii::app()->session['pager'];
		}else{
			$pager = 25;
			yii::app()->session['pager']=25;
		}
		
 		if (isset(yii::app()->session['dateFrom'])){
 			$dateFrom=yii::app()->session['dateFrom'];
 		}else{
			$dateFrom = date('Y-m-d H:i:s');
			yii::app()->session['dateFrom']=date('Y-m-d')." 00:00:00";
		}
		if (isset(yii::app()->session['dateTo'])){
			$dateTo=yii::app()->session['dateTo'];
		}else{
			$dateTo= date('Y-m-d H:i:s');
			yii::app()->session['dateTo']=date('Y-m-d')." 23:59:59";
		}
		if (isset(yii::app()->session['accountID'])){
			$accountID=yii::app()->session['accountID'];
		}else{
			$accountID= "";
			yii::app()->session['accountID']="";
		}
		if (isset(yii::app()->session['currency'])){
			$currency=yii::app()->session['currency'];
		}else{
			$currency= "All";
			yii::app()->session['currency']="All";
		}
		if (isset(yii::app()->session['operator'])){
			$operator=yii::app()->session['operator'];
		}else{
			$operator= 0;
			yii::app()->session['operator']=0;
		}
		if (isset(yii::app()->session['winloss'])){
			$winloss=yii::app()->session['winloss'];
		}else{
			$winloss= "";
			yii::app()->session['winloss']="";
		}
		if (isset(yii::app()->session['country'])){
			$country=yii::app()->session['country'];
		}else{
			$country= "All";
			yii::app()->session['country']="All";
		}
		if (isset(yii::app()->session['email'])){
			$email=yii::app()->session['email'];
		}else{
			$email= "";
			yii::app()->session['email']="";
		}
		if (isset(yii::app()->session['phone'])){
			$phone=yii::app()->session['phone'];
		}else{
			$phone= "";
			yii::app()->session['phone']="";
		}
		if (isset(yii::app()->session['deposit'])){
			$deposit=yii::app()->session['deposit'];
		}else{
			$deposit= "";
			yii::app()->session['deposit']="";
		}
		if (isset(yii::app()->session['playertype'])){
			$playertype=yii::app()->session['playertype'];
		}else{
			$playertype= "All";
			yii::app()->session['playertype']="All";
		}
		
		if (isset(yii::app()->session['selected_columns'])){
			$selected=yii::app()->session['selected_columns'];
		}else{
			$selected = 'opening_acc_date,account_id,account_name,country,email,phone_number#Opening Account Date,Account ID,Account Name,Country,Email,Phone';
			yii::app()->session['selected_columns']='opening_acc_date,account_id,account_name,country,email,phone_number#Opening Account Date,Account ID,Account Name,Country,Email,Phone';
			yii::app()->session['unselected_columns']='last_date_played,win_loss,currency_name,first_deposit_date#Last Date Played,Win/Loss,Currency,First Deposit Date';
		}
		//End parameters initialization-------------------------------
		
		$mod = new CustomReport1();
		$r=1;
		$rawData = $mod->getList($accountID,$playertype,$currency,$country,$email,$phone,$deposit,$dateFrom,$dateTo,$operator,$winloss,$selected);
		$arrayDataProvider=new CArrayDataProvider($rawData, array(
			'id'=>'id',
			 'sort'=>array(
				'attributes'=>array(
					'username', 'email',
				),
			), 
			'pagination'=>array(
				'pageSize'=>(isset($_POST['allPages'])) ? $_POST['allPages'] : ((isset($_POST['pageSize'])) ? $_POST['pageSize'] : $pager),
			),
			
		));
		
		
		$selectedField=split("#",$selected);
		$column = split(",",(isset($_POST['columns'])) ? $_POST['columns'] : $selectedField[0]);
		$column_name = split(",",(isset($_POST['column_names'])) ? $_POST['column_names'] : $selectedField[1]);
		$params =array(
			'arrayDataProvider'=>$arrayDataProvider,
			'pageSize'=>(isset($_POST['allPages'])) ? $_POST['allPages'] : ((isset($_POST['pageSize'])) ? $_POST['pageSize'] : $pager),
			'columns'=>array(
					array(
							'name' => 'NO',
							'type' => 'raw',
							'value' =>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							
					),
					array(
							'name' => (isset($column_name[0])) ? $column_name[0] : 'null',
							'type' => 'raw',
							'value' => (isset($column[0])) ? '$data["'.$column[0].'"]' : 'null',
							'visible'=>(isset($column[0])) ? true : false,
					),
					array(
							'name' => (isset($column_name[1])) ? $column_name[1] : 'null',
							'type' => 'raw',
							'value' => (isset($column[1])) ? '$data["'.$column[1].'"]' : 'null',
							'visible'=>(isset($column[1])) ? true : false,
					),
					array(
							'name' => (isset($column_name[2])) ? $column_name[2] : 'null',
							'type' => 'raw',
							'value' => (isset($column[2])) ? '$data["'.$column[2].'"]' : 'null',
							'visible'=>(isset($column[2])) ? true : false,
					),
					array(
							'name' => (isset($column_name[3])) ? $column_name[3] : 'null',
							'type' => 'raw',
							'value' => (isset($column[3])) ? '$data["'.$column[3].'"]' : 'null',
							'visible'=>(isset($column[3])) ? true : false,
					),
					array(
							'name' => (isset($column_name[4])) ? $column_name[4] : 'null',
							'type' => 'raw',
							'value' => (isset($column[4])) ? '$data["'.$column[4].'"]' : 'null',
							'visible'=>(isset($column[4])) ? true : false,
					),
					array(
							'name' => (isset($column_name[5])) ? $column_name[5] : 'null',
							'type' => 'raw',
							'value' => (isset($column[5])) ? '$data["'.$column[5].'"]' : 'null',
							'visible'=>(isset($column[5])) ? true : false,
					),
					array(
							'name' => (isset($column_name[6])) ? $column_name[6] : 'null',
							'type' => 'raw',
							'value' => (isset($column[6])) ? '$data["'.$column[6].'"]' : 'null',
							'visible'=>(isset($column[6])) ? true : false,
					),
					array(
							'name' => (isset($column_name[7])) ? $column_name[7] : 'null',
							'type' => 'raw',
							'value' => (isset($column[7])) ? '$data["'.$column[7].'"]' : 'null',
							'visible'=>(isset($column[7])) ? true : false,
					),
					array(
							'name' => (isset($column_name[8])) ? $column_name[8] : 'null',
							'type' => 'raw',
							'value' => (isset($column[8])) ? '$data["'.$column[8].'"]' : 'null',
							'visible'=>(isset($column[8])) ? true : false,
					),
					
				)
		);
		if (isset($_POST['backupPager'])){
			yii::app()->session['pager']=(isset($_POST['backupPager'])) ? $_POST['backupPager'] : yii::app()->session['pager'];
		}
		
// 		yii::app()->session['pager']=(isset($_POST['pageSize'])) ? $_POST['pageSize'] : yii::app()->session['pager'];
// 		yii::app()->session['dateFrom']= (isset($_POST['dateFrom'])) ? $_POST['dateFrom'] : yii::app()->session['dateFrom'];
// 		yii::app()->session['dateTo']= (isset($_POST['dateTo'])) ? $_POST['dateTo'] : yii::app()->session['dateTo'];
// 		yii::app()->session['accountID']= (isset($_POST['accountID'])) ? $_POST['accountID'] : yii::app()->session['accountID'];
// 		yii::app()->session['currency']= (isset($_POST['currency'])) ? $_POST['currency'] : yii::app()->session['currency'];
// 		yii::app()->session['operator']= (isset($_POST['operator'])) ? $_POST['operator'] : yii::app()->session['operator'];
// 		yii::app()->session['winloss']= (isset($_POST['winloss'])) ? $_POST['winloss'] : yii::app()->session['winloss'];
// 		yii::app()->session['country']= (isset($_POST['country'])) ? $_POST['country'] : yii::app()->session['country'];
// 		yii::app()->session['email']= (isset($_POST['email'])) ? $_POST['email'] : yii::app()->session['email'];
// 		yii::app()->session['phone']= (isset($_POST['phone'])) ? $_POST['phone'] : yii::app()->session['phone'];
// 		yii::app()->session['deposit']= (isset($_POST['deposit'])) ? $_POST['deposit'] : yii::app()->session['deposit'];

// 		yii::app()->session['selected_columns']=(isset($_POST['columns'])) ? $_POST['columns']."#".$_POST['column_names'] : yii::app()->session['selected_columns'];
// 		yii::app()->session['unselected_columns']=(isset($_POST['unselectedColumns'])) ? $_POST['unselectedColumns']."#".$_POST['unselectedColumn_names'] : yii::app()->session['unselected_columns'];
	
		if(!isset($_GET['ajax'])) $this->render('index', $params);
		else  $this->renderPartial('index', $params);
	}
	
	
	public function actionSaveFilter(){
		if(Yii::app()->user->checkAccess('marketing.writeCustomReport')){
			$postFilter= new TableCustomReportFilterSetting;
			$postFilter->filter_name=$_POST['filterName'];
			$postFilter->account_id=yii::app()->session['account_id'];
			$postFilter->filter_data=$_POST['sel_columnId'].'-'.$_POST['sel_columnNames'];
			$postFilter->unselected_data=$_POST['unsel_columnId'].'-'.$_POST['unsel_columnNames'];
			$postFilter->params=$_POST['account_id']."#".$_POST['playertype']."#".$_POST['currency']."#".$_POST['operator']."#".$_POST['win_loss']."#".$_POST['country']."#".$_POST['email']."#".$_POST['phone']."#".$_POST['deposit']."#".$_POST['dateFrom']."#".$_POST['dateTo'];
			$postFilter->save();
		}else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionLoadSavedFilter(){
		$criteria=new CDbCriteria;
		$criteria->select='filter_data,unselected_data,params';
		$criteria->condition='account_id=:id and filter_name=:filter';
		$criteria->params=array(':id'=>yii::app()->session['account_id'],':filter'=>$_POST['filterName']);
		$filters=TableCustomReportFilterSetting::model()->findAll($criteria);
			foreach($filters as $row){
				echo $row['filter_data'].'|'.$row['unselected_data'].'|'.$row['params'];
			}
		
	}
	public function actionDeleteSavedFilter(){
		$deleteLockedUser=TableCustomReportFilterSetting::model()->findByPk($_POST['id']);
		$deleteLockedUser->delete();
	}
	
	public function actionCostumReportCurrentPageExportExcel(){
		if(Yii::app()->user->checkAccess('marketing.exportCustomReport')){
			$file="Marketing_CashPlayer_Report".date('Y-m-d').".xls";
			$test="TheTableMarkupWillGoHere";
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=$file");
			$buffer = $_POST['csvBuffer'];
			
			$exportParams=str_replace("Displaying", "<b>Exported Page</b>", $_POST['txtParams']);
			$params=explode("#", $exportParams)	;
			
			try{
				echo $buffer;
				$postLog = new TableLog;
				$postLog->operated_by = Yii::app()->session['account_id'];
				$postLog->operated_by_level = Yii::app()->session['level_name'];
				$postLog->operated ='Custom Report';
				$postLog->operated_level = 'Clubvegas999';
				$postLog->operation_time = date('Y-m-d H:i:s');
				$postLog->log_type_id = 23;
				$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Marketing Custom Report</label> to excel file</b>\n'.$params[0].'\n<b>Opening Acc From</b> = '.$params[1].'\n<b>Opening Account to</b> = '.$params[2].'\n<b>Account Id</b> = '.$params[3].'\n<b>Currency</b> = '.$params[4].'\n<b>Winloss</b> = '.$params[5].'\n<b>Country</> = '.$params[6].'\n<b>Email</b> = '.$params[7].'\n<b>Phone</b> = '.$params[8].'\n<b>Deposit</b> = '.$params[9].'\n<b>row per page</b> = '.$params[10];
				$postLog->save();
			}catch(Exception $e){
		
			}
			exit();
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	//Reset session for customreport
	public function actionResetCustomResportSession() {
		yii::app()->session['pager']=25;
		yii::app()->session['dateFrom']=date('Y-m-d')." 00:00:00";
		yii::app()->session['dateTo']=date('Y-m-d')." 23:59:59";
		yii::app()->session['accountID']="";
		yii::app()->session['currency']="All";
		yii::app()->session['winloss']="";
		yii::app()->session['country']="All";
		yii::app()->session['email']="";
		yii::app()->session['phone']="";
		yii::app()->session['deposit']="";
		yii::app()->session['playertype']="All";
		yii::app()->session['selected_columns']='opening_acc_date,account_id,account_name,country,email,phone_number#Opening Account Date,Account ID,Account Name,Country,Email,Phone';
		yii::app()->session['unselected_columns']='last_date_played,win_loss,currency_name,first_deposit_date#Last Date Played,Win/Loss,Currency,First Deposit Date';
	}
}