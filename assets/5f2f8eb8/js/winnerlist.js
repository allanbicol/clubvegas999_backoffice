
// active menu color
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_winner_list').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i><li>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Winner List Management</a></li>");
// initialize lists onload
function onTheLoad(){
	tableWinnerList(urlWinnerList,urlUpdateWinnerList,document.getElementById('cmbCasino').value,document.getElementById('cmbGame').value,document.getElementById('txtSearch').value);

}

// search function
function searchWhiteList(){
	if($('#cmbSearchType').val()==0){
		tableCashPlayer(urlCashPlayerList + '&account_id=' + $('#txtSearch').val(),urlCashPlayerWhiteList);
		tableSubCompany(urlSubCompanyList + '&account_id=' + $('#txtSearch').val(),urlSaveSubCompanyWhiteList);
		$('#qry_cp_result').css('display','block'); // display cashplayer list 
		$('#qry_sc_result').css('display','block'); // display subcompany list
	}else if($('#cmbSearchType').val()==1){
		tableCashPlayer(urlCashPlayerList + '&account_id=' + $('#txtSearch').val(),urlCashPlayerWhiteList);
		$('#qry_sc_result').css('display','none'); // hide subcompany list
		$('#qry_cp_result').css('display','block'); // display cashplayer list
	}else{
		tableSubCompany(urlSubCompanyList + '&account_id=' + $('#txtSearch').val(),urlSaveSubCompanyWhiteList);
		$('#qry_cp_result').css('display','none'); // hide cashplayer list
		$('#qry_sc_result').css('display','block'); // display subcompany list
	}
}

// subcompany list
function tableWinnerList(listURL,urlUpdateWinnerList,casino,game,accountId) { 
	document.getElementById('qry_winnerlist_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_winnerlist_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_winnerlist_result").appendChild(divTag1);
    var grid = jQuery("#list1");
    grid.jqGrid({ 
		url: listURL+'&casino='+casino+'&game='+game+'&id='+accountId, 
		datatype: 'json',
	    mtype: 'GET',
	    colNames: ['id','Account ID','Game Name','Casino Name','Winning Amount','Enable','Operation'],
	    colModel: [	
	      {name: 'id', index: 'id', width: 10,title:false,hidden:true},
	      {name: 'account_id', index: 'account_id', width: 120,title:false},
	      {name: 'game_type', index: 'game_type', width: 123,title:false},
	      {name: 'casino_name', index: 'casino_name', width: 100,title:false},
	      {name: 'winning_amount', index: 'winning_amount',  width: 125,title:false},
	      {name: 'enable_winning', index: 'enable_winning', width: 60,title:false,align:'center', editable: true, edittype: "checkbox",editoptions: { value:"1:0"},formatter: "checkbox",formatoptions: {disabled : false}},	     
	      {name: 'operation', index: 'operation', width: 150,title:false},
	     ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    	
	    	var getColumnIndexByName = function(grid, columnName) {
	            var cm = grid.jqGrid('getGridParam', 'colModel'), i, l;
	            for (i = 1, l = cm.length; i < l; i += 1) {
	                if (cm[i].name === columnName) {
	                    return i; // return the index
	                }
	            }
	            return -1;
	        };
	    	
	    	var iCol = getColumnIndexByName ($(this), 'enable_winning'), rows = this.rows, i,
	        c = rows.length;
		    for (i = 1; i < c; i += 1) {
		        $(rows[i].cells[iCol]).change(function (e) {
		            var id = $(e.target).closest('tr')[0].id, isChecked = $(e.target).is(':checked');
			    	var account_id = grid.jqGrid('getCell', id, 'account_id');
			    	
		            var confirmMsg='';
		            if(isChecked==true){
		            	confirmMsg="Do you want to enable " + account_id + " in the winner list? ";
			        }else{
			        	confirmMsg="Do you want to disable " + account_id + " in the winner list?";
				    }
			    	var conf = confirm(confirmMsg);
			        if(conf == true){
			             jQuery.ajax({
		            			url: urlUpdateWinnerList,
		            			type: 'POST',
		            			data: {'account_id':account_id,'is_checked':isChecked},
		            			context: '',
		            			success: function(data){
		            				if(data == 'no_permission'){
		            					alert('Warning: You don\'t have permission to change white list!');
		            				}else{
		            					alert(data);	
		            				}
		            				$('#list1').trigger( 'reloadGrid' );
		            	    	}
		            	});
			        }else{
			        	$('#list1').trigger( 'reloadGrid' );
				    }
		        });
		    }
		},
	    rowNum: 25,
	    rownumbers:true,
	    rowList: [25, 50, 100, 200, 99999],
	    pager: '#pager1',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Winner List   <input type="button" class="btn red" value="Add" onclick="$createAddWinner.dialog(\'open\');">',
	    viewrecords: true,
	});
	$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del: false});
}

function SaveWinner(){
	var accountId=document.getElementById('txtAccountID').value;
	var gameName=document.getElementById('optGameName').value;
	var casinoName=document.getElementById('optCasinoName').value;
	var winningAmount=document.getElementById('txtAmount').value;
	var enable=document.getElementById('chkEnable');
	var isEnable='';
	if (enable.checked==true){
		isEnable=1;
	}else{isEnable=0;}
	
	if (accountId==''){
		alert('Account ID is required.');
		return false;
	}
	if (winningAmount==''){
		alert('Winning amount is required.');
		return false;
	}
	
	if (accountId!="" && winningAmount!="")
	  {
		jQuery.ajax({
    		url: urlSaveWinner,
    		type: 'POST',
    		datatype:'json',
    		data: {'accountID':accountId, 'gameName': gameName, 'casinoName': casinoName, 'winningAmount': winningAmount, 'isEnable': isEnable},
    		context: '',
    		success: function(data) {
		    		if (data=='suc'){
			    		alert('New Player successfully added in winner list.');
			    	}else if (data=='err'){
	    				alert('You dont have permission to add a new winner.');
	    			}
		    		$createAddWinner.dialog('close');
	    			$('#list1').trigger("reloadGrid");
		    	}
    	});
	  } else{
		  alert('Please select Bank name.');
	 }
}
function UpdateWinner(){
	var id=document.getElementById('id').value;
	var accountId=document.getElementById('txtAccountID1').value;
	var gameName=document.getElementById('optGameName1').value;
	var casinoName=document.getElementById('optCasinoName1').value;
	var winningAmount=document.getElementById('txtAmount1').value;
	var enable=document.getElementById('chkEnable1');
	var isEnable='';
	if (enable.checked==true){
		isEnable=1;
	}else{isEnable=0;}
	
	if (accountId==''){
		alert('Account ID is required.');
		return false;
	}
	if (winningAmount==''){
		alert('Winning amount is required.');
		return false;
	}
	
	if (accountId!="" && winningAmount!="")
	  {
		jQuery.ajax({
    		url: urlUpdateWinner,
    		type: 'POST',
    		datatype:'json',
    		data: {'id':id,'accountID':accountId, 'gameName': gameName, 'casinoName': casinoName, 'winningAmount': winningAmount, 'isEnable': isEnable},
    		context: '',
    		success: function(data) {
		    		if (data=='suc'){
			    		alert('Winning player successfully updated.');
			    	}else if (data=='err'){
	    				alert('You dont have permission to update a winning player.');
	    			}
		    		$createAlterWinner.dialog('close');
	    			$('#list1').trigger("reloadGrid");
		    	}
    	});
	  } else{
		  alert('Account ID and Winning Amount required.');
	 }
}
var $createAddWinner='';
$(document).ready(function() {
	$createAddWinner = $('<div></div>')
		.html('<div>'
		 +'<form name="myForm"   action="" >'
		 +'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
		 +'<tr><td class="row"></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Account ID</td><td style="padding-bottom:5px;"><input type="text" id="txtAccountID" name="txtAccountID" class="accountID" /></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Game</td><td class="row1" style="padding-bottom:5px;"><select class="gameName" id="optGameName" name="optGameName" style="width:205px"></select></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Casino</td><td class="row1" style="padding-bottom:5px;"><select class="casinoName" id="optCasinoName" name="optCasinoName" style="width:205px"></select></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Amount</td><td style="padding-bottom:5px;"><input type="text" id="txtAmount" name="txtAmount" class="amount"/></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Enable</td><td style="padding-bottom:5px;"><input type="Checkbox" id="chkEnable" name="chkEnable" class="enable"/></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;"></td><td class="row1" style="padding-bottom:5px;"><input class="btn red" id="btnOk" onclick="SaveWinner();" type="button" value="Save"/></td></tr>'
		 +'</table></center>'
		 +'</form> </div>'
			)
		.dialog({
			autoOpen: false,
			width: 350,
			title: 'Add Winner',
			resizable: false,
			modal: true,
			cache: false 
		});
 
        $('.gameName').ready(function(event) {
 			jQuery.ajax({
 	   		url:urlGameName,
 	   		type: 'POST',
 	   		data: '',
 	   		context: '',
 	   		success: function(data) {
 	   			 document.getElementById('optGameName').options.length=0;
 	   			 var newOpt = data.split(',');
 	   			 var selbox = document.getElementById('optGameName');
 	   			 var i=0;
 	   			 for (i=0;i<=newOpt.length-2;i++)
 	   			 {
 		    			 var spltOpt = newOpt[i].split(':');
 	   				 selbox.options[selbox.options.length] = new Option(spltOpt[0],spltOpt[1]);
 	       		 }
 		    	}
 	   		});
 		});

    	$('.casinoName').ready(function(event) {
 			jQuery.ajax({
 	   		url:urlCasinoName,
 	   		type: 'POST',
 	   		data: '',
 	   		context:'',
 	   		success: function(data) {
 	   			 document.getElementById('optCasinoName').options.length=0;
 	   			 var newOpt = data.split(',');
 	   			 var selbox = document.getElementById('optCasinoName');
 	   			 var i=0;
 	   			 for (i=0;i<=newOpt.length-2;i++)
 	   			 {
 		    			 var spltOpt = newOpt[i].split(':');
 	   				 selbox.options[selbox.options.length] = new Option(spltOpt[0],spltOpt[1]);
 	       		 }
 		    	}
 	   		});
 		});

});

function openAlterDialog(aElement)
{
	
	var accountIDvar = aElement.href.split("&");
	 var accountID=accountIDvar[1].split("=");
	 
	 var id=accountID[1].split(",");
	    jQuery.ajax({
   		url: urlEditDetails,
   		type: 'POST',
   		datatype:'json',
   		data: {'id':id.toString()},
   		context: '',
   		success: function(data) {
	    		var dataSplit=data.split("#");
	    		$createAlterWinner.dialog('open');
	    		document.getElementById('id').value=dataSplit[0];
   			 	document.getElementById('txtAccountID1').value=dataSplit[1];
   			 	document.getElementById('optGameName1').value=dataSplit[2];
   			 	document.getElementById('optCasinoName1').value=dataSplit[3];
   			 	document.getElementById('txtAmount1').value=dataSplit[4];
   			 	if (dataSplit[5]==1){
   			 		document.getElementById('chkEnable1').checked=true;
   			 	}else{
   			 		document.getElementById('chkEnable1').checked=false;
   			 	}
   			 	
	    	}
   	});
	//$createAlterWinner.dialog('open');
}
function openDeleteDialog(aElement)
{
	
	var accountIDvar = aElement.href.split("&");
	var accountID=accountIDvar[1].split("=");
	var id=accountID[1].split(",");

	var conf=confirm("Are you sure you want to delete player "+ id.toString());
	if (conf==true){
		jQuery.ajax({
	   		url: urlDeleteWinner,
	   		type: 'POST',
	   		datatype:'json',
	   		data: {'id':id.toString()},
	   		context: '',
	   		success: function(data) {
		    		alert(data);
		    		$('#list1').trigger("reloadGrid");
		    	}
	   	});
	}
}

var $createAlterWinner='';
$(document).ready(function() {
	$createAlterWinner = $('<div></div>')
		.html('<div>'
		 +'<form name="myForm"   action="" >'
		 +'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
		 +'<tr><td class="row"></td></tr>'
		 +'<tr style="display:none;"><td class="row" style="padding-bottom:5px;">ID</td><td style="padding-bottom:5px;"><input type="text" id="id" name="id" class="id" /></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Account ID</td><td style="padding-bottom:5px;"><input id="txtAccountID1" name="txtAccountID" class="accountID" /></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Game</td><td class="row1" style="padding-bottom:5px;"><select class="gameName" id="optGameName1" name="optGameName" style="width:205px"></select></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Casino</td><td class="row1" style="padding-bottom:5px;"><select class="casinoName" id="optCasinoName1" name="optCasinoName" style="width:205px"></select></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Amount</td><td style="padding-bottom:5px;"><input type="text" id="txtAmount1" name="txtAmount" class="amount"/></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;">Enable</td><td style="padding-bottom:5px;"><input type="Checkbox" id="chkEnable1" name="chkEnable" class="enable"/></td></tr>'
		 +'<tr><td class="row" style="padding-bottom:5px;"></td><td class="row1" style="padding-bottom:5px;"><input class="btn red" id="btnOk" onclick="UpdateWinner();" type="button" value="Update"/></td></tr>'
		 +'</table></center>'
		 +'</form> </div>'
			)
		.dialog({
			autoOpen: false,
			width: 350,
			title: 'Update Winner',
			resizable: false,
			modal: true,
			cache: false 
		});
 
        $('.gameName').ready(function(event) {
 			jQuery.ajax({
 	   		url:urlGameName,
 	   		type: 'POST',
 	   		data: '',
 	   		context: '',
 	   		success: function(data) {
 	   			 document.getElementById('optGameName1').options.length=0;
 	   			 var newOpt = data.split(',');
 	   			 var selbox = document.getElementById('optGameName1');
 	   			 var i=0;
 	   			 for (i=0;i<=newOpt.length-2;i++)
 	   			 {
 		    			 var spltOpt = newOpt[i].split(':');
 	   				 selbox.options[selbox.options.length] = new Option(spltOpt[0],spltOpt[1]);
 	       		 }
 		    	}
 	   		});
 		});

    	$('.casinoName').ready(function(event) {
 			jQuery.ajax({
 	   		url:urlCasinoName,
 	   		type: 'POST',
 	   		data: '',
 	   		context:'',
 	   		success: function(data) {
 	   			 document.getElementById('optCasinoName1').options.length=0;
 	   			 var newOpt = data.split(',');
 	   			 var selbox = document.getElementById('optCasinoName1');
 	   			 var i=0;
 	   			 for (i=0;i<=newOpt.length-2;i++)
 	   			 {
 		    			 var spltOpt = newOpt[i].split(':');
 	   				 selbox.options[selbox.options.length] = new Option(spltOpt[0],spltOpt[1]);
 	       		 }
 		    	}
 	   		});
 		});

});