<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/gamesetting.css" />
<script type="text/javascript">
function validate(text)
{
	
	var pattern = /^-?[0-9]+(.[0-9]{1,2})?$/; 
	//var text = document.getElementById('txtBunosAmount').value;
    if (text.match(pattern)==null) 
    {
		return false;
    }
	else
	{
		return true;
	}
}
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function get_cookie(cookie_name)
{
  var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

  if (results)
    return (unescape(results[2]));
  else
    return null;
}
function confirmPoolLimit(){
	if (trim(document.getElementById('txtUPoolLimit').value)==''){
		document.getElementById('txtUPoolLimitDialogErrorMsg').value='Pool Limit is required!';
		document.getElementById('txtUPoolLimit').focus();
		return false;
	}
	else{
		document.getElementById('txtUPoolLimitDialogErrorMsg').value='';
	}
	if (validate(document.getElementById('txtUPoolLimit').value)==false){
		document.getElementById('txtUPoolLimitDialogErrorMsg').value='Invalid Pool Limit!';
		return false;
	}
	else{
		document.getElementById('txtUPoolLimitDialogErrorMsg').value='';
	}
	jQuery.ajax({
		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/PoolLimitList/UpdatePoolLimit',
		method: 'POST',
		data: {'upool_id': document.getElementById('txtGame').title,'amount': document.getElementById('txtUPoolLimit').value},
		success: function(data) {
			jQuery("#list1").jqGrid('setCell', cRowNo, 3, document.getElementById('txtUPoolLimit').value);
    		jQuery("#list1").jqGrid('setCell', cRowNo, 4, data);
			$('#dialogSettingPoolLimit').dialog('close');
    	}
	});
}
</script>
<div id="dialogPoolLimitContent">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr><td class="left">Game</td><td class="right">
		<input type="text" id="txtGame" title="<?php echo $_GET['pool_id'];?>" value="<?php 
					$rd=TableGame::model()->find('id=:id', array(':id'=>$_GET['pool_game_id']));
					echo $rd['game_name'];
					?>" readonly="true"/>

		</td></tr>
		
		<tr><td class="left">Pool Limit</td><td class="right"><input type="text" id="txtUPoolLimit" value="<?php 
					$rd=TablePoolLimit::model()->find('id=:id', array(':id'=>$_GET['pool_id']));
					echo $rd['amount'];
					?>" /></td></tr>
	</table>
	<div id="dialogPoolLimitContentFooter">
		<input type="button" value="Confirm" onclick="javascript: confirmPoolLimit();"/>
	</div>
</div>
<input type="text" name="txtUPoolLimitDialogErrorMsg" id="txtUPoolLimitDialogErrorMsg" readonly="true"/>