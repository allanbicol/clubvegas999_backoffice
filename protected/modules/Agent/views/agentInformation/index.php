<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/agentinformation.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/agentinformation.js"></script>
	<script type="text/javascript">
		var urlTableAgentOverviewDetails='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentInformation/AgentOverviewDetails&test_currency=';
		var lblOverview="<?php echo Yii::t('agent','agent.overview.currency');?>";
		var lblTotalCredit="<?php echo Yii::t('agent','agent.overview.total_credit');?>";
		var lblTotalAssignedCredit="<?php echo Yii::t('agent','agent.overview.total_assigned_credit');?>";
		var lblTotalPercentageCredit="<?php echo Yii::t('agent','agent.overview.total_percentage_credit');?>";
		var lblPlayers="<?php echo Yii::t('agent','agent.overview.players');?>";
		var lblDetailTitle="<?php echo Yii::t('agent','agent.overview.detail_title');?>";
		var lblSubCompanies="<?php echo Yii::t('agent','agent.overview.sub_companies');?>";
		var lblSeniorMasterAgents="<?php echo Yii::t('agent','agent.overview.senior_master_agents');?>";
		var lblMasterAgents="<?php echo Yii::t('agent','agent.overview.master_agents');?>";
		var lblAgents="<?php echo Yii::t('agent','agent.overview.agents');?>";
		var lblTotal="<?php echo Yii::t('agent','agent.overview.total');?>";
		var strIsUser="<?php echo Yii::app()->session['account_type'];?>";
		
	</script>
</head>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
th.ui-th-column div {
    word-wrap: break-word; /* IE 5.5+ and CSS3 */
    white-space: pre-wrap; /* CSS3 */
    white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
    white-space: -pre-wrap; /* Opera 4-6 */
    white-space: -o-pre-wrap; /* Opera 7 */
    overflow: hidden;
    height: auto !important;
    vertical-align: middle;
}
.ui-jqgrid .ui-jqgrid-htable th{
padding:0px!important;
}
#list1_rn{
width:25px!important;
}
</style>
<body>
<b><?php echo Yii::t('agent','agent.overview.title');?></b>
<div id="body_wrapper">

	<?php
		if(Yii::app()->session['account_type']=='user'){
			echo "<div id='title_header'>Clubvegas999<b> <input type='button' id='btnDetails' class='btn red' value='". Yii::t('agent','agent.overview.details_button') ."' onclick='javascript: btnDetails(\"". yii::app()->session['level'] ."\")'/>";
		}
		else
		{
			echo "<div id='title_header'>Account ID " . Yii::app()->session['account_id'] . " <b> <input type='button' id='btnDetails' class='btn red' value='". Yii::t('agent','agent.overview.details_button') ."' onclick='javascript: btnDetails(\"". yii::app()->session['level'] ."\")'/>";
		}
			
	?>
	<Select id="cmbCurrency" name="cmbCurrency" onchange="javascript: cmbCurrency(this.value,'<?php echo Yii::app()->session['level'];?>','<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentInformation/AgentOverviewConvertion');">
	<?php 
		echo $this->tableCurrency();
	?>
	</Select>
	</b>
	<form style="display: inline" name="frmExceptTestCurrency" method="post" action="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentInformation">
	<input type="hidden" id="txtExceptTestCurrency" name="txtExceptTestCurrency">
	<?php if(isset($_POST['txtExceptTestCurrency'])){
		if($_POST['txtExceptTestCurrency']==0){
	?>
		<input type="checkbox" id="chkExceptTestCurrency" name="chkExceptTestCurrency" value="0" onclick="document.getElementById('txtExceptTestCurrency').value=1;this.form.submit();"/><label for="chkExceptTestCurrency"><?php echo Yii::t('agent','agent.overview.except_test_currency');?></label>	
	<?php 
		}
		else{
	?>
		<input type="checkbox" id="chkExceptTestCurrency" name="chkExceptTestCurrency" value="1" onclick="document.getElementById('txtExceptTestCurrency').value=0;this.form.submit();" checked/><label for="chkExceptTestCurrency"><?php echo Yii::t('agent','agent.overview.except_test_currency');?></label>	
	<?php 
		}
	}else{
	?>
		<input type="checkbox" id="chkExceptTestCurrency" name="chkExceptTestCurrency" value="1" onclick="document.getElementById('txtExceptTestCurrency').value=0;this.form.submit();" checked/><label for="chkExceptTestCurrency"><?php echo Yii::t('agent','agent.overview.except_test_currency');?></label>
	<?php 
	}?>
	</form>
	<?php 
	//$except_test_currency=0;
	if(isset($_POST['txtExceptTestCurrency'])){
		$except_test_currency=$_POST['txtExceptTestCurrency'];
	}else{
		$except_test_currency=1;
	}
	?>
</div>
	<table id="tblAgentOverview" border="0" cellpadding="0" cellspacing="0" width="100%">
	
	<!-- header -->
	<tr>
		<td class="td_header_left"></td><td class="td_header_right" colspan="100%"><center><?php echo Yii::t('agent','agent.overview.winloss');?></center></td>
	</tr>
	<!-- header -->
	
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.live_casino_yesterday_winloss');?></td>
		<td class="right" colspan="100%">
		<?php 
		echo '<center>';
		//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=11){
		if(Yii::app()->session['account_type']=='user'){
			$cu=TableCurrency::model()->find('id=:id', array(':id'=>1));
		}else{
			$rd=TableAgent::model()->find('account_id=:account_id', array(':account_id'=>Yii::app()->session['account_id']));
			$cu=TableCurrency::model()->find('id=:id', array(':id'=>$rd['currency_id']));
		}
		
		$currency=$cu['currency_name'];
		$currency_rate=$cu['exchange_rate'];
		
		echo $this->yesterdayWinLoss($except_test_currency);
		echo '</center>';
		?>
		
		</td>
	</tr>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.yesterday_total_slot_winloss');?></td>
		<td class="right" colspan="100%">
		<?php 
		echo '<center>';
		echo $this->yesterdaySlotWinLoss($except_test_currency);
		echo '</center>';
		?>
		
		</td>
	</tr>
	<!-- header -->
	<tr>
		<td class="td_header_left"></td><td class="td_header_right" colspan="100%"><center><?php echo Yii::t('agent','agent.overview.total');?></center></td>
	</tr>
	<!-- header -->
	
	<tr>
		<td class="left"><?php if(Yii::app()->session['account_type']=='user'){echo Yii::t('agent','agent.overview.main_company') . ' ';}?><?php echo Yii::t('agent','agent.overview.available_credit');?></td>
		<td class="right" colspan="100%">
		<?php 
		echo '<center>';
		echo $this->availableCredit($except_test_currency);
		echo '</center>';
		?>
		</td>
	</tr>
	
	
	<!-- header -->
	<tr>
		<td class="td_header_left"></td><td class="td_header_right"><center><?php echo Yii::t('agent','agent.overview.total_available_credit');?></center></td><td class="td_header_right"><center><?php echo Yii::t('agent','agent.overview.total_assigned_credit');?></center></td><td class="td_header_right"><center><?php echo Yii::t('agent','agent.overview.total_percentage_credit');?></center></td>
	</tr>
	<!-- header -->
	
	
	<?php 
	//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
	if(Yii::app()->session['account_type']=='user'){
	?>
	<tr >
		<td class="left"><?php echo Yii::t('agent','agent.overview.sub_companies');?> <?php echo Yii::t('agent','agent.overview.total_credit');?></td>
		<td class="right">
		<?php 
			$credit_percentage=0;
			$tr = new AgentInformation();
			$result=$tr->getSumTransaction('SC','',$except_test_currency)->readAll();
			foreach($result as $row){
					echo '<label id="lblSCCurrency">USD</label> : <label id="lblSCCredit">' . number_format($row['total_credit'],2).'</label>';
					$credit_percentage=number_format($row['total_percentage_credit_val'],2);
			}
		?>
		</td>
		<td class="col4">
			<label id="lblSCAssignedCreditPercentageCurrency">USD</label> : <label id="lblSCAssignedCreditPercentage"><?php echo number_format($row['total_assigned_credit'],2);?></label>
		</td>
		<td class="col3">
			<label id="lblSCCreditPercentageCurrency">USD</label> : <label id="lblSCCreditPercentage"><?php echo $credit_percentage;?></label>
		</td>
		
	</tr>
	<?php }?>
	<?php 
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8 || Yii::app()->session['level']=='SC'){
		if(Yii::app()->session['account_type']=='user' || Yii::app()->session['level']=='SC'){
	?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.senior_master_agents');?> <?php echo Yii::t('agent','agent.overview.total_credit');?></td>
		<td class="right">
		<?php 
		
			$credit_percentage=0;
			$credit_assigned=0;
			$tr = new AgentInformation();
			if(Yii::app()->session['level']!='SC'){
				$result=$tr->getSumTransaction('SMA','',$except_test_currency)->readAll();
				foreach($result as $row){
					echo '<label id="lblSMACurrency">USD</label> : <label id="lblSMACredit">' . number_format($row['total_credit'],2).'</label>';
					$credit_percentage=number_format($row['total_percentage_credit_val'],2);
					$credit_assigned=number_format($row['total_assigned_credit'],2);
				}
			}else{
				$result=$tr->getSumTransaction('SMA',Yii::app()->session['account_id'],$except_test_currency)->readAll();
				
				foreach($result as $row){
					if($currency=='USD'){
						echo '<label id="lblSMACurrency">USD</label> : <label id="lblSMACredit">' . number_format($row['total_credit'],2).'</label>';
						$credit_percentage=number_format($row['total_percentage_credit_val'],2);
						$credit_assigned=number_format($row['total_assigned_credit'],2);
					}else{
						//if manager, admin, etc. then convert to usd
						if(Yii::app()->session['account_type']=='user'){
							echo '<label id="lblSMACurrency">USD</label> : <label id="lblSMACredit">' . number_format($row['total_credit'],2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val'],2);
							$credit_assigned=number_format($row['total_assigned_credit'],2);
						}else{
							echo '<label id="lblSMACurrency">'.$currency . '</label> : <label id="lblSMACredit">' . number_format($row['total_credit']*$currency_rate,2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val']*$currency_rate,2);
							$credit_assigned=number_format($row['total_assigned_credit']*$currency_rate,2);
						}
						//echo number_format($row['total_credit']/$currency_rate,2);
					}
				}
			}
		?>
		</td>
		<td class="col4">
			<label id="lblSMAAssignedCreditPercentageCurrency"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> : <label id="lblSMAAssignedCreditPercentage"><?php echo $credit_assigned;?></label>
		</td>
		<td class="col3">
			<label id="lblSMACreditPercentageCurrency"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> : <label id="lblSMACreditPercentage"><?php echo $credit_percentage;?></label>
		</td>
	</tr>
	<?php }?>
	<?php 
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8 || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA'){
		if(Yii::app()->session['account_type']=='user' || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA'){
	?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.master_agents');?> <?php echo Yii::t('agent','agent.overview.total_credit');?></td>
		<td class="right">
		<?php 
			$credit_percentage=0;
			$credit_assigned=0;
			$tr = new AgentInformation();
			if(Yii::app()->session['level']!='SMA'){
				//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
				if(Yii::app()->session['account_type']=='user'){
					$result=$tr->getSumTransaction('MA','',$except_test_currency)->readAll();
					foreach($result as $row){
						echo '<label id="lblMACurrency">USD</label> : <label id="lblMACredit">' . number_format($row['total_credit'],2).'</label>';
						$credit_percentage=number_format($row['total_percentage_credit_val'],2);
						$credit_assigned=number_format($row['total_assigned_credit'],2);
					}
				}else{
					$result=$tr->getSumParentTransaction('MA',Yii::app()->session['account_id'],$except_test_currency)->readAll();
					foreach($result as $row){
						if($currency=='USD'){
							echo '<label id="lblMACurrency">USD</label> : <label id="lblMACredit">' . number_format($row['total_credit'],2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val'],2);
							$credit_assigned=number_format($row['total_assigned_credit'],2);
						}else{
							//if manager, admin, etc. then convert to usd
							if(Yii::app()->session['account_type']=='user'){
								echo '<label id="lblMACurrency">USD</label> : <label id="lblMACredit">' . number_format($row['total_credit']/$currency_rate,2).'</label>';
								$credit_percentage=number_format($row['total_percentage_credit_val']/$currency_rate,2);
								$credit_assigned=number_format($row['total_assigned_credit']/$currency_rate,2);
							}else{
								echo '<label id="lblMACurrency">'.$currency . '</label> : <label id="lblMACredit">' . number_format($row['total_credit'],2).'</label>';
								$credit_percentage=number_format($row['total_percentage_credit_val'],2);
								$credit_assigned=number_format($row['total_assigned_credit'],2);
							}
							//echo number_format($row['total_credit']/$currency_rate,2);
						}
					}
				}
			}else{
				$result=$tr->getSumTransaction('MA',Yii::app()->session['account_id'],$except_test_currency)->readAll();
				foreach($result as $row){
					if($currency=='USD'){
						echo '<label id="lblMACurrency">USD</label> : <label id="lblMACredit">' . number_format($row['total_credit'],2).'</label>';
						$credit_percentage=number_format($row['total_percentage_credit_val'],2);
						$credit_assigned=number_format($row['total_assigned_credit'],2);
					}else{
						//if manager, admin, etc. then convert to usd
						//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
						if(Yii::app()->session['account_type']=='user'){
							echo '<label id="lblMACurrency">USD</label> : <label id="lblMACredit">' . number_format($row['total_credit']/$currency_rate,2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val']/$currency_rate,2);
							$credit_assigned=number_format($row['total_assigned_credit']/$currency_rate,2);
						}else{
							echo '<label id="lblMACurrency">'.$currency . '</label> : <label id="lblMACredit">' . number_format($row['total_credit'],2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val'],2);
							$credit_assigned=number_format($row['total_assigned_credit'],2);
						}
						//echo number_format($row['total_credit']/$currency_rate,2);
					}
				}
			}
		?>
		</td>
		<td class="col4">
			<label id="lblMAAssignedCreditPercentageCurrency"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> : 
			<label id="lblMAAssignedCreditPercentage"><?php echo $credit_assigned;?></label>
		</td>
		<td class="col3">
			<label id="lblMACreditPercentageCurrency"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> :
			<label id="lblMACreditPercentage"><?php echo $credit_percentage;?></label>
		</td>
	</tr>
	<?php }?>
	<?php 
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8 || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA' || Yii::app()->session['level']=='MA'){
		if(Yii::app()->session['account_type']=='user' || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA' || Yii::app()->session['level']=='MA'){
	?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.agents');?> <?php echo Yii::t('agent','agent.overview.total_credit');?></td>
		<td class="right">
		<?php 
			$credit_percentage=0;
			$credit_assigned=0;
			$tr = new AgentInformation();
			if(Yii::app()->session['level']!='MA'){
				//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
				if(Yii::app()->session['account_type']=='user'){
					//get all
					$result=$tr->getSumTransaction('AGT','',$except_test_currency)->readAll();
					foreach($result as $row){
						echo '<label id="lblAGTCurrency">USD</label> : <label id="lblAGTCredit">' . number_format($row['total_credit'],2).'</label>';
						$credit_percentage=number_format($row['total_percentage_credit_val'],2);
						$credit_assigned=number_format($row['total_assigned_credit'],2);
					}
				}else{
					//get all below
					$result=$tr->getSumParentTransaction('AGT',Yii::app()->session['account_id'],$except_test_currency)->readAll();
					foreach($result as $row){
						if($currency=='USD'){
							echo '<label id="lblAGTCurrency">USD</label> : <label id="lblAGTCredit">' . number_format($row['total_credit'],2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val'],2);
							$credit_assigned=number_format($row['total_assigned_credit'],2);
						}else{
							//if manager, admin, etc. then convert to usd
							//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
							if(Yii::app()->session['account_type']=='user'){
								echo '<label id="lblAGTCurrency">USD</label> : <label id="lblAGTCredit">' . number_format($row['total_credit']/$currency_rate,2).'</label>';
								$credit_percentage=number_format($row['total_percentage_credit_val']/$currency_rate,2);
								$credit_assigned=number_format($row['total_assigned_credit']/$currency_rate,2);
							}else{
								echo '<label id="lblAGTCurrency">'.$currency . '</label> : <label id="lblAGTCredit">' . number_format($row['total_credit'],2).'</label>';
								$credit_percentage=number_format($row['total_percentage_credit_val'],2);
								$credit_assigned=number_format($row['total_assigned_credit'],2);
							}
							//echo number_format($row['total_credit']/$currency_rate,2);
						}
					}
				}
			}else{
				//get only his agent
				$result=$tr->getSumTransaction('AGT',Yii::app()->session['account_id'],$except_test_currency)->readAll();
				foreach($result as $row){
					if($currency=='USD'){
						echo '<label id="lblAGTCurrency">USD</label> : <label id="lblAGTCredit">' . number_format($row['total_credit'],2).'</label>';
						$credit_percentage=number_format($row['total_percentage_credit_val'],2);
						$credit_assigned=number_format($row['total_assigned_credit'],2);
					}else{
						//if manager, admin, etc. then convert to usd
						//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
						if(Yii::app()->session['account_type']=='user'){
							echo '<label id="lblAGTCurrency">USD</label> : <label id="lblAGTCredit">' . number_format($row['total_credit']/$currency_rate,2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val']/$currency_rate,2);
							$credit_assigned=number_format($row['total_assigned_credit']/$currency_rate,2);
						}else{
							echo '<label id="lblAGTCurrency">'.$currency . '</label> : <label id="lblAGTCredit">' . number_format($row['total_credit'],2).'</label>';
							$credit_percentage=number_format($row['total_percentage_credit_val'],2);
							$credit_assigned=number_format($row['total_assigned_credit'],2);
						}
						//echo number_format($row['total_credit']/$currency_rate,2);
					}
				}
			}
		?>
		</td>
		<td class="col4">
			<label id="lblAGTAssignedCreditPercentageCurrency"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> : 
			<label id="lblAGTAssignedCreditPercentage"><?php echo $credit_assigned;?></label>
		</td>
		<td class="col3">
			<label id="lblAGTCreditPercentageCurrency"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> :
			<label id="lblAGTCreditPercentage"><?php echo $credit_percentage;?></label>
		</td>
	</tr>
	<?php }?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.players');?> <?php echo Yii::t('agent','agent.overview.total_credit');?></td>
		<td class="right">
		<?php 
		$credit_percentage=0;
		$credit_assigned=0;
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
		if(Yii::app()->session['account_type']=='user'){
			$tr = new AgentInformation();
			$result=$tr->getSumParentTransaction('MEM','',$except_test_currency)->readAll();
			foreach($result as $row){
				echo '<label id="lblMEMCurrency">USD</label> : <label id="lblMEMCredit">' . number_format($row['total_credit'],2).'</label>';
				$credit_percentage=number_format($row['total_percentage_credit_val'],2);
				$credit_assigned=number_format($row['total_assigned_credit'],2);
			}
		}else{
			//get all under this account_id
			$tr = new AgentInformation();
			$result=$tr->getSumParentTransaction('MEM',Yii::app()->session['account_id'],$except_test_currency)->readAll();
			foreach($result as $row){
				if($currency=='USD'){
					echo '<label id="lblMEMCurrency">USD</label> : <label id="lblMEMCredit">' . number_format($row['total_credit'],2).'</label>';
					$credit_percentage=number_format($row['total_percentage_credit_val'],2);
					$credit_assigned=number_format($row['total_assigned_credit'],2);
				}else{
					//if manager, admin, etc. then convert to usd
					if(Yii::app()->session['account_type']=='user'){
						echo '<label id="lblMEMCurrency">USD</label> : <label id="lblMEMCredit">' . number_format($row['total_credit']/$currency_rate,2).'</label>';
						$credit_percentage=number_format($row['total_percentage_credit_val']/$currency_rate,2);
						$credit_assigned=number_format($row['total_assigned_credit']/$currency_rate,2);
					}else{
						echo '<label id="lblMEMCurrency">'.$currency . '</label> : <label id="lblMEMCredit">' . number_format($row['total_credit'],2).'</label>';
						$credit_percentage=number_format($row['total_percentage_credit_val'],2);
						$credit_assigned=number_format($row['total_assigned_credit'],2);
					}
					//echo number_format($row['total_credit']/$currency_rate,2);
				}
			}
		}
		?>
		</td>
		<td class="col4">
			<label id="lblMEMAssignedCreditPercentageCurrency" style="display:none;"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> 
			<label id="lblMEMAssignedCreditPercentage" style="display:none;"><?php echo $credit_assigned;?></label>
			N/A
		</td>
		<td class="col3">
			<label id="lblMEMCreditPercentageCurrency" style="display:none;"><?php if(Yii::app()->session['account_type']=='user'){echo 'USD';}else{echo $currency;}?></label> 
			<label id="lblMEMCreditPercentage" style="display:none;"><?php echo $credit_percentage; ?></label>
			N/A
		</td>
	</tr>
	<!-- header -->
	<tr>
		<td class="td_header_left"></td><td class="td_header_right" colspan="100%"><center><?php echo Yii::t('agent','agent.overview.total_agent_accounts');?></center></td>
	</tr>
	<!-- header -->
	<?php
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
	if(Yii::app()->session['account_type']=='user'){
	?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.sub_companies');?> <?php echo Yii::t('agent','agent.overview.total');?> <font style="color: #4681a3;"><?php echo Yii::t('agent','agent.overview.active');?></font> / <font style="color: #912626;"><?php echo Yii::t('agent','agent.overview.closed');?></font></td>
		<td class="right" colspan="100%">
		<?php
			echo '<center>';
			$sc_active=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'SC',':agent_status_id'=>1),));
			//$sc_suspend=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'SC',':agent_status_id'=>3),));
			$sc_closed=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'SC',':agent_status_id'=>3),));
			echo '<font style="color: #4681a3;">'. $sc_active . '</font> / ' . '<font style="color: #912626;">' . $sc_closed . '</font>';
			echo '</center>';
		?>
		</td>
	</tr>
	<?php }?>
	<?php 
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8 || Yii::app()->session['level']=='SC'){
		if(Yii::app()->session['account_type']=='user' || Yii::app()->session['level']=='SC'){
	?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.senior_master_agents');?> <?php echo Yii::t('agent','agent.overview.total');?> <font style="color: #4681a3;"><?php echo Yii::t('agent','agent.overview.active');?></font> / <font style="color: #912626;"><?php echo Yii::t('agent','agent.overview.closed');?></font></td>
		<td class="right" colspan="100%">
		<?php
		echo '<center>';
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
		if(Yii::app()->session['account_type']=='user'){
			$sma_active=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'SMA',':agent_status_id'=>1),));
			//$sma_suspend=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'SMA',':agent_status_id'=>3),));
			$sma_closed=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'SMA',':agent_status_id'=>3),));
		}else{
			$tr = new AgentInformation();
			$result1=$tr->getStatusNo('SMA',1,Yii::app()->session['account_id'])->readAll();
			foreach($result1 as $row){
				$sma_active=$row['total'];
			}
// 			$result3=$tr->getStatusNo('SMA',3,Yii::app()->session['account_id'])->readAll();
// 			foreach($result3 as $row){
// 				$sma_suspend=$row['total'];
// 			}
			$result2=$tr->getStatusNo('SMA',3,Yii::app()->session['account_id'])->readAll();
			foreach($result2 as $row){
				$sma_closed=$row['total'];
			}
		}
		echo '<font style="color: #4681a3;">' . $sma_active . '</font> / ' . '<font style="color: #912626;">' . $sma_closed . '</font>';
		echo '</center>';
		?>
		</td>
	</tr>
	<?php }?>
	<?php 
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8 || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA'){
		if(Yii::app()->session['account_type']=='user' || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA'){
	?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.master_agents');?> <?php echo Yii::t('agent','agent.overview.total');?> <font style="color: #4681a3;"><?php echo Yii::t('agent','agent.overview.active');?></font> / <font style="color: #912626;"><?php echo Yii::t('agent','agent.overview.closed');?></font></td>
		<td class="right" colspan="100%">
		<?php
		echo '<center>';
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
		if(Yii::app()->session['account_type']=='user'){
			$ma_active=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'MA',':agent_status_id'=>1),));
			//$ma_suspend=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'MA',':agent_status_id'=>3),));
			$ma_closed=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'MA',':agent_status_id'=>3),));
		}else{
			$tr = new AgentInformation();
			$result1=$tr->getStatusNo('MA',1,Yii::app()->session['account_id'])->readAll();
			foreach($result1 as $row){
				$ma_active=$row['total'];
			}
// 			$result3=$tr->getStatusNo('MA',3,Yii::app()->session['account_id'])->readAll();
// 			foreach($result3 as $row){
// 				$ma_suspend=$row['total'];
// 			}
			$result2=$tr->getStatusNo('MA',3,Yii::app()->session['account_id'])->readAll();
			foreach($result2 as $row){
				$ma_closed=$row['total'];
			}
		}
		echo '<font style="color: #4681a3;">' . $ma_active . '</font> / ' . '<font style="color: #912626;">' . $ma_closed . '</font>';
		echo '</center>';
		?>
		</td>
	</tr>
	<?php }?>
	<?php 
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8 || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA' || Yii::app()->session['level']=='MA'){
		if(Yii::app()->session['account_type']=='user' || Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA' || Yii::app()->session['level']=='MA'){
	?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.agents');?> <?php echo Yii::t('agent','agent.overview.total');?> <font style="color: #4681a3;"><?php echo Yii::t('agent','agent.overview.active');?></font> / <font style="color: #912626;"><?php echo Yii::t('agent','agent.overview.closed');?></font></td>
		<td class="right" colspan="100%">
		<?php
		echo '<center>';
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
		if(Yii::app()->session['account_type']=='user'){
			$agt_active=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'AGT',':agent_status_id'=>1),));
			//$agt_suspend=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'AGT',':agent_status_id'=>3),));
			$agt_closed=TableAgent::model()->count(array('select'=>'account_id','condition'=>'agent_type=:agent_type AND agent_status_id=:agent_status_id','params'=>array(':agent_type'=>'AGT',':agent_status_id'=>3),));
		}else{
			$tr = new AgentInformation();
			$result1=$tr->getStatusNo('AGT',1,Yii::app()->session['account_id'])->readAll();
			foreach($result1 as $row){
				$agt_active=$row['total'];
			}
// 			$result3=$tr->getStatusNo('AGT',3,Yii::app()->session['account_id'])->readAll();
// 			foreach($result3 as $row){
// 				$agt_suspend=$row['total'];
// 			}
			$result2=$tr->getStatusNo('AGT',3,Yii::app()->session['account_id'])->readAll();
			foreach($result2 as $row){
				$agt_closed=$row['total'];
			}
		}
		echo '<font style="color: #4681a3;">' . $agt_active . '</font> / ' . '<font style="color: #912626;">' . $agt_closed . '</font>';
		echo '</center>';
		?>
		</td>
	</tr>
	<?php }?>
	<tr>
		<td class="left"><?php echo Yii::t('agent','agent.overview.players');?> <?php echo Yii::t('agent','agent.overview.total');?> <font style="color: #4681a3;"><?php echo Yii::t('agent','agent.overview.active');?></font> / <font style="color: #912626;"><?php echo Yii::t('agent','agent.overview.closed');?></font></td>
		<td class="right" colspan="100%">
		<?php
		echo '<center>';
		//if(Yii::app()->session['level']==1 || Yii::app()->session['level']==2 || Yii::app()->session['level']==3 || Yii::app()->session['level']==4 || Yii::app()->session['level']==5 || Yii::app()->session['level']==7 || Yii::app()->session['level']==8){
		if(Yii::app()->session['account_type']=='user'){
			$mem_active=TableAgentPlayer::model()->count(array('select'=>'account_id','condition'=>'status_id=:status_id','params'=>array(':status_id'=>1),));
			//$mem_suspend=TableAgentPlayer::model()->count(array('select'=>'account_id','condition'=>'status_id=:status_id','params'=>array(':status_id'=>3),));
			$mem_closed=TableAgentPlayer::model()->count(array('select'=>'account_id','condition'=>'status_id=:status_id','params'=>array(':status_id'=>3),));
		}else{
			$tr = new AgentInformation();
			$result1=$tr->getStatusNo('MEM',1,Yii::app()->session['account_id'])->readAll();
			foreach($result1 as $row){
				$mem_active=$row['total'];
			}
// 			$result3=$tr->getStatusNo('MEM',3,Yii::app()->session['account_id'])->readAll();
// 			foreach($result3 as $row){
// 				$mem_suspend=$row['total'];
// 			}
			$result2=$tr->getStatusNo('MEM',2,Yii::app()->session['account_id'])->readAll();
			foreach($result2 as $row){
				$mem_closed=$row['total'];
			}
		}
		echo '<font style="color: #4681a3;">' . $mem_active . '</font> / ' . '<font style="color: #912626;">' . $mem_closed . '</font>';
		echo '</center>';
		?>
		</td>
	</tr>
	</table>
</div>
<div id="qry_result"></div>
<br/>
<br/>
</body>
</html>