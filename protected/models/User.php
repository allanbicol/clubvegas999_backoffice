<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $isenable
 * @property integer $level_id
 * @property string $position
 *
 * The followings are the available model relations:
 * @property TblLevel $level
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}
	public function tableName()
	{
		return 'tbl_user';
	}
	/**
	 * @todo get the account type for checking agent user
	 * @return string <CDbDataReader, mixed, unknown>
	 * @author Kimny MOUK
	 */
	public static function getUserType()
	{
		$connection = Yii::app()->db;
		$accountID = Yii::app()->session['account_id'];
		$command = $connection->createCommand("SELECT account_id FROM tbl_agent where account_id=:account_id");
		$command->bindParam(":account_id",$accountID,PDO::PARAM_STR);
		$userType = $command->queryScalar();
		if ($userType !=null)
		return (Yii::app()->session['account_type']!='agent_alias') ? 'agent' : 'agent_alias'; // added by leokarl (agent_alias)
	}
	/**
	 * @todo get the role type
	 * @return string <CDbDataReader, mixed, unknown>
	 * @author Kimny MOUK
	 */
	public static function getRoleType()
	{
		$connection = Yii::app()->db;
		$accountID = (Yii::app()->session['account_type']!='agent_alias') ? Yii::app()->session['account_id'] : Yii::app()->session['agent_alias_name']; // added by leokarl (agent_alias)
		$command = $connection->createCommand("SELECT itemname FROM AuthAssignment where userid=:account_id");
		$command->bindParam(":account_id",$accountID,PDO::PARAM_STR);
		$roleType = $command->queryScalar();
		return $roleType;
	}
	
	/**
	 * @todo get role home page url
	 * @author leokarl
	 * @since 2013-01-10
	 */
	public static function getRoleHomePageUrl(){
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT b.url FROM AuthItem a LEFT JOIN tbl_home_page_url b ON b.id=a.home_page_url_id WHERE a.name=:name");
		$command->bindParam(":name",User::getRoleType(),PDO::PARAM_STR);
 		$homePageUrl = $command->queryScalar();
		return $homePageUrl;
	}
}