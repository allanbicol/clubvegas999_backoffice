<?php
/**
 * @todo AgentSlotWinLossController
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-28
 */
class AgentSlotWinLossController extends MyController
{
	/**
	 * @todo Export Grid data to excel
	 * @author leokarl
	 * @since 2013-01-24
	 */
	public function actionExportToExcel(){
		if(isset($_POST['csvBuffer']) && isset($_GET['filename'])){
			$model = new ExportToExcel();
			$model->exportToExcelFromGrid($_GET['filename'] . date('Y-m-d'), $_POST['csvBuffer'],$_POST['txtParams']);
		}
	}
    /**
     * @todo actionHTV999Total
     * @copyright CE
     * @author Leo karl
     * @since 2012-05-28
     */
	public function actionHTV999Total()
	{
		$bl=new AgentSlotWinLoss();
		$records=0;
		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getHTV999CSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getHTV999SMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getHTV999MATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getHTV999AGTTotal();
		}else{
			$player_records=$bl->getHTV999Total();
		}
		$filedNames = array("row","currency_name","total_stake","valid_bet","win_loss","commission","bonus","total","pl_total","exchange_rate");
	
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);
	}
	
	/**
	 * @todo actionHTVSummaryWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTVSummaryWinLoss()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getHTVWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getHTVWinLoss($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","total","mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total","exchange_rate");
		
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionHTVSummaryMemberWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTVSummaryMemberWinLoss()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getHTVWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getHTVWinLoss($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","win_loss","commission","bonus","total","current_balance","agt_win_loss","agt_comm","agt_bonus","agt_total","pl_total","exchange_rate");
		
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionHTVWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTVWinLossDetails()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getHTVWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getHTVWinLossDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","mem_win_loss",
			"mem_comm","mem_bonus","mem_total","current_balance","agt_win_loss","agt_comm","agt_bonus","agt_total","ma_win_loss",
			"ma_comm","ma_bonus","ma_total","sma_win_loss","sma_comm","sma_bonus","sma_total","sc_win_loss","sc_comm","sc_bonus","sc_total",
			"mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionHTV999WinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTV999WinLossDetails()
	{
		$bl=new AgentSlotWinLoss();
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getHTVMemberWinLossDetailsCount();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getHTVMemberWinLossDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","bet_date","account_id","account_name","currency_name","game_type","bet_amount","win_loss","commission","amount_tips","bonus","total","balance","casino_code");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	
	/**
	 * @todo actionHTV999WinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTV999WinLossDetailsSummaryFooter()
	{
		$bl=new AgentSlotWinLoss();
		$bl->getHTVMemberWinLossDetailsSummaryFooter();
	}
	
	
	
	
	
	/**
	 * @todo actionVIRTUATotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-09
	 */
	public function actionVIRTUATotal()
	{
		$bl=new AgentSlotWinLoss();
		$records=0;
		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getVIRTUACSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getVIRTUASMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getVIRTUAMATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getVIRTUAAGTTotal();
		}else{
			$player_records=$bl->getVIRTUATotal();
		}
		$filedNames = array("row","currency_name","total_stake","valid_bet","win_loss","commission","bonus","total","pl_total","exchange_rate");
	
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);
	}
	
	/**
	 * @todo actionVIRTUASummaryWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-09
	 */
	public function actionVIRTUASummaryWinLoss()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getVIRTUAWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getVIRTUAWinLoss($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","total","mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total","exchange_rate");
	
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionVIRTUASummaryMemberWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-09
	 */
	public function actionVIRTUASummaryMemberWinLoss()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getVIRTUAWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getVIRTUAWinLoss($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","win_loss","commission","bonus","total","current_balance","agt_win_loss","agt_comm","agt_total","pl_total","exchange_rate");
	
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionVIRTUAWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-09
	 */
	public function actionVIRTUAWinLossDetails()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getVIRTUAWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getVIRTUAWinLossDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","mem_win_loss",
				"mem_comm","mem_bonus","mem_total","current_balance","agt_win_loss","agt_comm","agt_total","ma_win_loss",
				"ma_comm","ma_total","sma_win_loss","sma_comm","sma_total","sc_win_loss","sc_comm","sc_total",
				"mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionVIRTUAWinLossBettingDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-09
	 */
	public function actionVIRTUAWinLossBettingDetails()
	{
		$bl=new AgentSlotWinLoss();
		
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getVIRTUAMemberWinLossDetailsCount();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getVIRTUAMemberWinLossDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","bet_date","account_id","account_name","currency_name","game_type","bet_amount","win_loss","commission","amount_tips","bonus","total","balance","na_balance","casino_code");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	
	/**
	 * @todo actionVIRTUAWinLossBettingDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-09
	 */
	public function actionVIRTUAWinLossBettingDetailsSummaryFooter()
	{
		$bl=new AgentSlotWinLoss();
		$bl->getVIRTUAMemberWinLossDetailsSummaryFooter();
	}

	
	/**
	 * @todo actionSLOTTotal
	 * @copyright CE
	 * @author Allan
	 * @since 2014-01-10
	 */
	public function actionSLOTTotal()
	{
		$bl=new AgentSlotWinLoss();
		$records=0;
		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getSLOTCSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getSLOTSMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getSLOTMATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getSLOTAGTTotal();
		}else{
			$player_records=$bl->getSLOTTotal();
		}
		$filedNames = array("row","currency_name","total_stake","valid_bet","win_loss","commission","bonus","total","pl_total","exchange_rate");
	
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);
	}
	
	/**
	 * @todo actionSLOTSummaryWinLoss
	 * @copyright CE
	 * @author Allan Bicol
	 * @since 2014-01-10
	 */
	public function actionSLOTSummaryWinLoss()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getSLOTWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getSLOTWinLoss($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","total","mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total","exchange_rate");
	
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionSLOTSummaryMemberWinLoss
	 * @copyright CE
	 * @author Allan Bicol
	 * @since 2014-01-10
	 */
	public function actionSLOTSummaryMemberWinLoss()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getSLOTWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getSLOTWinLoss($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","win_loss","commission","bonus","total","current_balance","agt_win_loss","agt_comm","agt_bonus","agt_total","pl_total","exchange_rate");
	
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionSLOTWinLossDetails
	 * @copyright CE
	 * @author Allan Bicol
	 * @since 2014-01-10
	 */
	public function actionSLOTWinLossDetails()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getSLOTWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getSLOTWinLossDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","mem_win_loss",
				"mem_comm","mem_bonus","mem_total","current_balance","agt_win_loss","agt_comm","agt_bonus","agt_total","ma_win_loss",
				"ma_comm","ma_bonus","ma_total","sma_win_loss","sma_comm","sma_bonus","sma_total","sc_win_loss","sc_comm","sc_bonus","sc_total",
				"mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	
	/**
	 * @todo actionSLOTWinLossBettingDetails
	 * @copyright CE
	 * @author Allan Bicol
	 * @since 2014-01-10
	 */
	public function actionSLOTWinLossBettingDetails()
	{
		$bl=new AgentSlotWinLoss();
	
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
	
		$result=$bl->getSLOTMemberWinLossDetailsCount();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
		$player_records=$bl->getSLOTMemberWinLossDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("row","bet_date","account_id","account_name","currency_name","game_type","bet_amount","win_loss","commission","amount_tips","bonus","total","balance","casino_code");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	
	/**
	 * @todo actionSLOTWinLossBettingDetailsSummaryFooter
	 * @copyright CE
	 * @author Allan Bicol
	 * @since 2014-01-10
	 */
	public function actionSLOTWinLossBettingDetailsSummaryFooter()
	{
		$bl=new AgentSlotWinLoss();
		$bl->getSLOTMemberWinLossDetailsSummaryFooter();
	}
	
	
	
	
	
	
	/**	
	 * @todo actionSharingAndCommission
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSharingAndCommission()
	{
		$bl=new AgentSharingAndCommission();
		$bl->getSlotSharingAndCommission();
	}
	
	/**
	 * @todo actionIndex
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readSlotWinLoss') || User::getUserType() === Constants::ROLE_AGENT)
		{	
			$this->render("index");
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	
	//====================================EXPORT ALL TO EXCEL==================================================
	
	public function actionExportAllVIRTUASummaryWinLoss()
	{
		$bl=new AgentSlotWinLoss();
		$result=$bl->getVIRTUAWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records=$bl->getExportVIRTUAWinLoss();
	
		$fieldNames = array("account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","total","mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total");
		$headerNames=array("Account Id","Account Name","Currency","Bet Count","Average Bet","Total Stake","Valid Bet","Total","MC Winloss","MC Commission","MC Bonus","MC Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
		
		$records=0;
		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getVIRTUACSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getVIRTUASMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getVIRTUAMATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getVIRTUAAGTTotal();
		}else{
			$player_records=$bl->getVIRTUATotal();
		}
		$fieldNames = array("currency_name","total_stake","valid_bet","win_loss","commission","bonus","total","pl_total");
		$headerNames=array("Currency","Total Stake","Valid Bet","Winloss","Commission","Bonus","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
	
	public function actionExportAllVIRTUAWinLossDetails()
	{
		$bl=new AgentSlotWinLoss();
		$result=$bl->getVIRTUAWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records=$bl->getExportVIRTUAWinLossDetails();
	
		$fieldNames = array("account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","mem_win_loss",
				"mem_comm","mem_bonus","mem_total","current_balance","agt_win_loss","agt_comm","agt_total","ma_win_loss",
				"ma_comm","ma_total","sma_win_loss","sma_comm","sma_total","sc_win_loss","sc_comm","sc_total",
				"mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total");
		$headerNames=array("Account Id","Account Name","Currency","Bet Count","Average Bet","Total Stake","Valid Bet","Player Winloss","Player Commission",
				"Player Bonus","Player Total","Player Balance","Agent Winloss","Agent Commission","Agent Total","Master Agent Winloss","Master Agent Commission",
				"Master Agent Total","Senior Master Winloss","Senior Master Commission","Senior Master Total","SubCompany Winloss","SubCompany Commission",
				"SubCompany Total","MainCompany Winloss","Maincompany Commission","MainCompany Bonus","MainCompany Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
	
	public function actionExportAllVIRTUAWinLossBettingDetails()
	{
		$bl=new AgentSlotWinLoss();
		$result=$bl->getVIRTUAMemberWinLossDetailsCount();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
		$player_records=$bl->getExportVIRTUAMemberWinLossDetails();
	
		$fieldNames = array("bet_date","account_id","account_name","currency_name","game_type","bet_amount","win_loss","commission","amount_tips","bonus","total","na_balance","casino_code");
		$headerNames=array("Bet Date","Account Id","Account Name","Currency Name","Game Type","Bet Amount","Winloss","Commission","Tips","Bonus","Total","Balance","Casino");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
	
	public function actionExportAllSLOTSummaryWinLoss()
	{
		$bl=new AgentSlotWinLoss();
		$result=$bl->getSLOTWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records=$bl->getExportSLOTWinLoss();
	
		$fieldNames = array("account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","total","mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total");
		$headerNames=array("Account Id","AccountName","Currency","Bet Count","Average Bet","Total Stake","Valid Bet","Total","MC Winloss","MC Commission","MC Jackpot","MC Total","Company P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$records=0;
		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getSLOTCSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getSLOTSMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getSLOTMATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getSLOTAGTTotal();
		}else{
			$player_records=$bl->getSLOTTotal();
		}
		$fieldNames = array("currency_name","total_stake","valid_bet","win_loss","commission","bonus","total","pl_total");
		$headerNames=array("Currency","Total Stake","Valid Bet","Winloss","Commission","Bonus","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
	
	public function actionExportAllSLOTWinLossDetails()
	{
		$bl=new AgentSlotWinLoss();
	
		$result=$bl->getSLOTWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records=$bl->getExportSLOTWinLossDetails();
	
		$fieldNames = array("account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","mem_win_loss",
				"mem_comm","mem_bonus","mem_total","current_balance","agt_win_loss","agt_comm","agt_bonus","agt_total","ma_win_loss",
				"ma_comm","ma_bonus","ma_total","sma_win_loss","sma_comm","sma_bonus","sma_total","sc_win_loss","sc_comm","sc_bonus","sc_total",
				"mc_win_loss","mc_comm","mc_bonus","mc_total","pl_total");
		$headerNames=array("Account Id","Account Name","Currency","Bet Count","Average Bet","Total Stake","Valid Bet","Player Winloss","Player Commission",
				"Player Bonus","Player Total","Player Balance","Agent Winloss","Agent Commission","Agent Bonus","Agent Total","Master Agent Winloss",
				"Master Agent Commission","Master Agent Bonus","Master Agent Total","Senior Master Winloss","Senior Master Commission","Senior Master Bonus",
				"Senior Master Total","SubCompany Winloss","Subcompany commission","SubCompany Bonus","SubCompany Total","MainCompany Winloss","MainCompany Commission",
				"MainCompany Bonus","MainCompany Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
	
	public function actionExportAllSLOTWinLossBettingDetails()
	{
		$bl=new AgentSlotWinLoss();
		$result=$bl->getSLOTMemberWinLossDetailsCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records=$bl->getExportSLOTMemberWinLossDetails();
	
		$fieldNames = array("bet_date","account_id","account_name","currency_name","game_type","bet_amount","win_loss","commission","amount_tips","bonus","total","balance","casino_code");
		$headerNames=array("Bet Date","Account Id","Account Name","Currency","Game Type","Bet Count","Winloss","Commission","Amount Tips","Bonus","Total","Balance","Casino");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
}