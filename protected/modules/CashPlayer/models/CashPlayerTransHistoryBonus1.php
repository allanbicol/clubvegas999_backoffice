<?php

class CashPlayerTransHistoryBonus1 
{

	public function getPlayerTransHistoryBonus1($orderField, $sortType, $startIndex, $limit)
	{
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
		
		if ($_GET['bonusType']!='ALL'){
			$bonusType=" and thbg.transaction_item="."'".$_GET['bonusType']."'";
		}else{
			$bonusType='';
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['currencyId']=="ALL")
		{
			if ($_GET['test']!=0){
				$command = $connection->createCommand("Select '1' as counter, player_id,account_name,currency_name,sum(trans_number) as trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_name<>'TEST' and test_real_currency <>1 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,player_id order by $orderField $sortType LIMIT $startIndex , $limit" );
			}else{
				$command = $connection->createCommand("Select '1' as counter, player_id,account_name,currency_name,sum(trans_number) as trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,player_id order by $orderField $sortType LIMIT $startIndex , $limit" );
			}
		}
		else{
			$command = $connection->createCommand("Select '1' as counter,player_id,account_name,currency_name,sum(trans_number) as trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_id='".$_GET['currencyId']."' and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,player_id order by $orderField $sortType LIMIT $startIndex , $limit" );
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountPlayerTransHistoryBonus1()
	{
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
		if ($_GET['bonusType']!='ALL'){
			$bonusType=" and thbg.transaction_item="."'".$_GET['bonusType']."'";
		}else{
			$bonusType='';
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['currencyId']=="ALL")
		{
			if ($_GET['test']!=0){
				$command = $connection->createCommand("Select COUNT(0) FROM (Select COUNT(0) from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_name<>'TEST' and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by player_id ,currency_name) as TB" );
			}else{
				$command = $connection->createCommand("Select COUNT(0) FROM (Select COUNT(0) from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by player_id ,currency_name) as TB" );
			}
		}
		else{
			$command = $connection->createCommand("Select COUNT(0) FROM (Select COUNT(0) from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_id=".$_GET['currencyId']." and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by player_id ,currency_name) as TB" );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getTotalPlayerTransHistoryBonus1()
	{

		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
		
		if ($_GET['bonusType']!='ALL'){
			$bonusType=" and thbg.transaction_item="."'".$_GET['bonusType']."'";
		}else{
			$bonusType='';
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['currencyId']=="ALL")
		{
			if ($_GET['test']!=0){
				$command = $connection->createCommand("Select currency_name,SUM(trans_number) AS trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total,(SELECT exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=thbg.currency_name) AS rate from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_name<>'TEST' and test_real_currency <>1 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name" );
			}else{
				$command = $connection->createCommand("Select currency_name,SUM(trans_number) AS trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total,(SELECT exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=thbg.currency_name) AS rate from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name" );
			}
		}
		else{
			$command = $connection->createCommand("Select currency_name,SUM(trans_number) AS trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total,(SELECT exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=thbg.currency_name) AS rate from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_id='".$_GET['currencyId']."' and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name" );
		}
		$rows = $command->query();
		return $rows;
	
	}
	
	public function getCountTotalPlayerTransHistoryBonus1()
	{

	
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistoryByGroup" );
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getExportAllPlayerTransHistoryBonus1()
	{
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
	
		if ($_GET['bonusType']!='ALL'){
			$bonusType=" and thbg.transaction_item="."'".$_GET['bonusType']."'";
		}else{
			$bonusType='';
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['currencyId']=="ALL")
		{
			if ($_GET['test']!=0){
				$command = $connection->createCommand("Select  player_id,account_name,currency_name,sum(trans_number) as trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_name<>'TEST' and test_real_currency <>1 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,player_id order by currency_name " );
			}else{
				$command = $connection->createCommand("Select  player_id,account_name,currency_name,sum(trans_number) as trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,player_id order by currency_name " );
			}
		}
		else{
			$command = $connection->createCommand("Select player_id,account_name,currency_name,sum(trans_number) as trans_number,sum(added_bonus) as added_bonus,sum(removed_bonus) as removed_bonus,sum(added_bonus) + sum(removed_bonus) AS Total from vwGetTransactionHistoryByGroup thbg INNER JOIN tbl_transaction_item tti ON tti.id=thbg.transaction_item where cash_player_trans_type_id=6 AND item_type=6 $accountID $bonusType and currency_id='".$_GET['currencyId']."' and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,player_id order by currency_name" );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
}



