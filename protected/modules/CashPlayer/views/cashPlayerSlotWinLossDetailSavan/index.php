<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/winlosstotalpages.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_slot_winloss_v1').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Cash Player Slot Win/Loss Version 1</a><i class='icon-angle-right'></i></li><li><a href='#'> Slotvegas Betting History</a></li>");
</script>
<script type="text/javascript">
	function gotoBack()
	{
		var dateFrom = (document.getElementById('datefrom').value).split("-");
		
		var monthF = dateFrom[1];
		var dayF = dateFrom[2];
		var yearF = dateFrom[0];
		var dateF=(yearF + "/" + monthF + "/" + dayF);
		var dateTo =(document.getElementById('dateto').value).split("-");
		var monthT = dateTo[1];
		var dayT = dateTo[2];
		var yearT = dateTo[0];
		if (dayT.length==1){
			dayT="0"+dayT;
			}
		if (dayF.length==1){
			dayF="0"+dayF;
			}
		if (monthT.length==1){
			monthT="0"+monthT;
			}
		if (monthF.length==1){
			monthF="0"+monthF;
			}
		var dateT= (yearT + "/" + monthT + "/" +dayT);
		var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
		var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
		var accountId='<?php  if (isset($_GET['id'])){ echo $_GET['id'];}?>';
		window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLoss&dateFrom="+enc(dateSubmitFrom)+"&dateTo="+enc(dateSubmitTo)+"&id="+accountId;
	}

	var cRowNo=0;
	function tableTransHistoryToday(dateFrom,dateTo,accountID) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan/CashPlayerSlotWinLossDetailSavan&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Bet DateTime', 'Account ID','Account Name','Currency','Game Type','Bet Amount','Win/Loss','Commission','Jackpot','Total','Balance Before','Balance After','Casino','counter'],
			    colModel: [
					{name: 'bet_time', index: 'bet_time', width: 120, search:true,title:false},
					{name: 'account_id', index: 'account_id', width: 90,title:false,hidden:true},
					{name: 'account_name', index: 'account_name', width: 110,title:false,hidden:true},
					{name: 'currency', index: 'currency', width: 60, align:"center",title:false},
					{name: 'game_type', index: 'game_type', width: 90, align:"center",title:false,formatter:gameTypeFormatter},
					{name: 'bet_amount', index: 'bet_amount', width: 75, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'win_loss', index: 'win_loss', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'commission', index: 'commission', width: 75, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'jackpot_money', index: 'jackpot_money', width: 75, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'balance_before', index: 'balance_before', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'balance_after', index: 'balance_after', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'casino_name', index: 'casino_name', width:90, align:"right",title:false,hidden:true},
					{name: 'counter', index: 'counter', width:90, align:"right",title:false,hidden:true},
			    ],
			    afterInsertRow : function(rowid, rowdata)
			    {
			        if (rowdata.win_loss < 0)
			        {
			           $(this).jqGrid('setCell', rowid,10 ,'', {color:'#990000'});
			        }
			        getTotal();

			    },
			    loadComplete: function(){
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
				},
			    loadtext:"",
			    rownumbers:true,
			    rowNum: 20,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'id',
			    sortorder: 'DESC',
			    caption: ' <label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> Slot Betting History - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    footerrow: false,
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export current page", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportSavanSlotWinLossDetailToExcel();
	           }, 
	            position:"last"
	        });
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export all pages", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllSavanSlotWinLossDetailToExcel(dateFrom,dateTo,accountID);
	           }, 
	            position:"last"
	        });
		});
	} 
	function gameTypeFormatter(cellvalue, options, rowObject) {
        $("cellvalue").val(cellvalue);
        if (cellvalue==1){
        	return '<label style="color:green">JUNGLE</label>';
        }else if (cellvalue==2){
        	return '<label style="color:#663300">PYRAMID</label>';
	    }else if (cellvalue==3){
	    	return '<label style="color:#006666">SANGO</label>';
	    }else if (cellvalue==4){
	    	return '<label style="color:#00006B">CRAZY MONKEY</label>';
	    }
	}; 
	function exportSavanSlotWinLossDetailToExcel(){
		var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
		var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
		
		var table1= document.getElementById('qry_result');
		var table2= document.getElementById('tableTotalPages');
		var html1 = table1.outerHTML;
		var html2 = table2.outerHTML;
		document.forms[2].csvBuffer.value="";
		document.forms[2].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+dateFrom+'#'+dateTo;
		document.forms[2].csvBuffer.value=html1 +''+ html2;
	    document.forms[2].method='POST';
	    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan/SavanSlotWinLossDetailExcel';  // send it to server which will open this contents in excel file
	    document.forms[2].target='_top';
	    document.forms[2].submit();
	}
	function exportAllSavanSlotWinLossDetailToExcel(dateFrom,dateTo,accountID){
	    $.ajax({	
			 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan/ExportAllCashPlayerSlotWinLossDetailSavan&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID, 
			 async:true,
			 success: function(result) {
					var data =result.split("<BREAK>");
				
					var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
					var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
					
					document.forms[2].csvBuffer.value="";
					document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+dateFrom+'#'+dateTo;
					document.forms[2].csvBuffer.value=data[0];
				    document.forms[2].method='POST';
				    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan/SavanSlotWinLossDetailExcel';  // send it to server which will open this contents in excel file
				    document.forms[2].target='_top';
				    document.forms[2].submit();
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown) { 
		 }});
				    
	}
	function getTotal(){
		var grid = $("#list1"),
		sumBetCount = grid.jqGrid('getCol', 'counter', false, 'sum');
		sumBetAmount = grid.jqGrid('getCol', 'bet_amount', false, 'sum');
        sumJackpot = grid.jqGrid('getCol', 'jackpot_money', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_loss', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');

        document.getElementById('currentBetcount').innerHTML=sumBetCount;
        document.getElementById('currentBetAmount').innerHTML=addCommas(sumBetAmount.toFixed(2));
        document.getElementById('currentWinloss').innerHTML=addCommas(sumWinLoss.toFixed(2));
        document.getElementById('currentCommission').innerHTML=addCommas(sumCommission.toFixed(2));
        document.getElementById('currentJackpot').innerHTML=addCommas(sumJackpot.toFixed(2));
        document.getElementById('currentTotal').innerHTML=addCommas(sumTotal.toFixed(2));
       
        //grid.jqGrid('footerData','set', {bet_date: '<div class=\"dateSummaryFooter\">TOTAL</div>',win_loss:sumWinLoss,commission:sumCommission,jackpot_money:sumJackpot,total_win_los:sumTotal});  
	}
</script>
<script type="text/javascript">
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}

function addCommas(nStr){
    nStr += '';
    c = nStr.split(','); // Split the result on commas
    nStr = c.join('');  // Make it back to a string without the commas
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
         
function decryptparam()
{
	var strF = document.getElementById('f').value;
	var strT= document.getElementById('t').value;
	var encodeF = enc(strF);
	var encodeT = enc(strT);
	
	var dateF= encodeF.split("_");	
	var dateFrom=dateF[0];
	var dateT= encodeT.split("_");	
	var dateTo=dateT[0];
	var dateFSplit=dateFrom.split("/");
	var dateTSplit=dateTo.split("/");
	
	if (dateFSplit[1].length==1){
		omonthF="0"+ dateFSplit[1];
	}else{omonthF=dateFSplit[1]; }
	if (dateTSplit[1].length==1){
		omonthT="0"+ dateTSplit[1];
	}else{omonthT=dateTSplit[1];}
	if (dateFSplit[2].length==1){
		odayF="0"+ dateFSplit[2];
	}else{odayF=dateFSplit[2];}
	if (dateTSplit[2].length==1){
		odayT="0"+ dateTSplit[2];
	}else{odayT=dateTSplit[2];}
	document.getElementById("datefrom").value=dateFSplit[0]+"-"+omonthF+"-"+odayF;
	document.getElementById("dateto").value=dateTSplit[0]+"-"+omonthT+"-"+odayT;

}
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

	function loadTransTable(dateFrom,dateTo,accountID){
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(dateFrom,dateTo,accountID);

	}

	function btnclick(btnname)
	{
		document.getElementById('currentBetcount').innerHTML="0";
		document.getElementById('currentJackpot').innerHTML="0.00";
        document.getElementById('currentBetAmount').innerHTML="0.00";
        document.getElementById('currentWinloss').innerHTML="0.00";
        document.getElementById('currentCommission').innerHTML="0.00";
        document.getElementById('currentTotal').innerHTML="0.00";
        
		document.getElementById('allBetcount').innerHTML="0";
		document.getElementById('allJackpot').innerHTML="0.00";
        document.getElementById('allBetAmount').innerHTML="0.00";
        document.getElementById('allWinloss').innerHTML="0.00";
       	document.getElementById('allCommission').innerHTML="0.00";
       	document.getElementById('allTotal').innerHTML="0.00";
       	document.getElementById('currentBalance').innerHTML="0.00";
       	
		if (btnname=="Load")
		{
			var datefrom=((document.getElementById('datefrom').value).replace("-","/").replace("-","/")+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateto=((document.getElementById('dateto').value).replace("-","/").replace("-","/")+"_"+ document.getElementById('cbHourto').value +":59:59");
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(datefrom,dateto,accountID);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			var dayL=theday.toString;
			var monthL=themonth.toString;
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theday="0"+theday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(datefrom,dateto,accountID);

			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan/GetRecordAllPages',
	    		type: 'POST',
	    		data: {'accountId':accountID,
	        			'dateFrom':datefrom,
	        			'dateTo':dateto},
	    		context:'',
	    		success: function(data) {
	        		var resultAllPages=data.split("#");
	        		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	                document.getElementById('allBetAmount').innerHTML=resultAllPages[1];
	                document.getElementById('allWinloss').innerHTML=resultAllPages[2];
	               	document.getElementById('allCommission').innerHTML=resultAllPages[3];
	        		document.getElementById('allJackpot').innerHTML=resultAllPages[4];
	               	document.getElementById('allTotal').innerHTML=resultAllPages[5];
	               	document.getElementById('currentBalance').innerHTML=resultAllPages[6];
		    	}
	    	});
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			var dayL=theyday.toString;
			var monthL=themonth.toString;
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theyday="0"+theyday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theyday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theyday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(datefrom,dateto,accountID);

			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan/GetRecordAllPages',
	    		type: 'POST',
	    		data: {'accountId':accountID,
	        			'dateFrom':datefrom,
	        			'dateTo':dateto},
	    		context:'',
	    		success: function(data) {
	        		var resultAllPages=data.split("#");
	        		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	                document.getElementById('allBetAmount').innerHTML=resultAllPages[1];
	                document.getElementById('allWinloss').innerHTML=resultAllPages[2];
	               	document.getElementById('allCommission').innerHTML=resultAllPages[3];
	        		document.getElementById('allJackpot').innerHTML=resultAllPages[4];
	               	document.getElementById('allTotal').innerHTML=resultAllPages[5];
	               	document.getElementById('currentBalance').innerHTML=resultAllPages[6];
		    	}
	    	});
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var accountID=document.getElementById('txtAccountID').value;
			
			loadTransTable(dateSubmitFrom,dateSubmitTo,accountID);
			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan/GetRecordAllPages',
	    		type: 'POST',
	    		data: {'accountId':accountID,
	        			'dateFrom':dateSubmitFrom,
	        			'dateTo':dateSubmitTo},
	    		context:'',
	    		success: function(data) {
	        		var resultAllPages=data.split("#");
	        		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	                document.getElementById('allBetAmount').innerHTML=resultAllPages[1];
	                document.getElementById('allWinloss').innerHTML=resultAllPages[2];
	               	document.getElementById('allCommission').innerHTML=resultAllPages[3];
	        		document.getElementById('allJackpot').innerHTML=resultAllPages[4];
	               	document.getElementById('allTotal').innerHTML=resultAllPages[5];
	               	document.getElementById('currentBalance').innerHTML=resultAllPages[6];
		    	}
	    	});
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
div.dateSummaryFooter { text-align: Left; }
tr.footrow-ltr td { background-color: #AD8533; color: #FFF; }
</style>
</head>
<body onload="javascript:decryptparam();btnclick('Submit');">
<div style="position: relative;left: 5px; top:3px">
<div id="parameter_area" style="width: 500px;height: 155px">
	<div class="header" ><label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> Slotvegas Betting History</div>
	<form action="">
		<input type="text" id="txtAccountID" value="<?php echo $_GET['account_id'];?>" style="display: none;" >
		<table style=" width: 500px; margin-top:5px;">
		<tr hidden="true"><td width="25%" style="padding-left: 5px;">METHOD:</td>
			<td><select id="gameType">
				<option value="ALL">ALL</option>
  				<?php 
				$dataReader = TableGame::model()->findAll();
				foreach ($dataReader as $row){
					echo '<option value="' . $row['game_name'] . '">'. strtoupper($row['game_name']) . '</option>';
				}
				?>
			</select>
		</td></tr>
		<tr><td width="25%" style="padding-left: 5px;">FROM:</td><td><input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px; background-color: transparent;width: 50px;" disabled></td></tr>
			<tr>
			<td width="25%" style="padding-left: 5px;">TO :</td><td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; background-color: transparent;width: 50px;" disabled></td></tr>
		<tr></table>
		<div align="center" style="margin-top: 5px;" >
		<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
		<input onclick="javascript: gotoBack();" type="button" value="Back" class="btn red"></div>
		
		</form>	
		
		<br/>
		
	<div id="qry_result">
	</div>
	<div id="pager1"></div>
	<br/>
		<table id="tableTotalPages">
		<tr>
			<th class="header" colspan=8 align="left"><b>Total</b></th>
		</tr>
		<tr class="tr" >
			<th class="th" width="100px" align="left">Type</th>
			<th class="th" width="90px">Number of Bet</th>
			<th class="th" width="90px">Bet Amount</th>
			<th class="th" width="90px">Win/Loss</th>
			<th class="th" width="90px">Commission</th>
			<th class="th" width="90px">Jackpot</th>
			<th class="th" width="90px">Total</th>
			<th class="th" width="90px">Balance</th>
		</tr>
		<tr class="tr" >
			<td class="td" ><b>Current Page</b></td>
			<td class="td" id="currentBetcount" align="right"></td>
			<td class="td" id="currentBetAmount" align="right"></td>
			<td class="td" id="currentWinloss" align="right"></td>
			<td class="td" id="currentCommission" align="right"></td>
			<td class="td" id="currentJackpot" align="right"></td>
			<td class="td" id="currentTotal" align="right"></td>
			<td class="td" id="currentBalance" align="center" rowspan="2"></td>
		</tr>
		<tr class="tr" >
			<td class="td" ><b>All Pages</b></td>
			<td class="td" id="allBetcount" align="right"></td>
			<td class="td" id="allBetAmount" align="right"></td>
			<td class="td" id="allWinloss" align="right"></td>
			<td class="td" id="allCommission" align="right"></td>
			<td class="td" id="allJackpot" align="right"></td>
			<td class="td" id="allTotal" align="right"></td>
		</tr>
	</table>
	<br/>
	<!--
	<div id="qry_resultTotal"></div>
	<div id="qry_resultTotal1"></div>
	<br/>-->
	<div><input id="f" value="<?php echo $_GET['f'];?>" hidden=true><input id="t" value="<?php echo $_GET['t'];?>" hidden=true></div>
	
</div>
<form method="post" action="">
<input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
 <input type="hidden" name="txtParams" id="txtParams" value="" />
</form>
</body>
</html>