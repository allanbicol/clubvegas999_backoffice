<?php
class SelectedGameTableLimit
{
	public function getSelectedBaccaratTableLimitIds()
	{
		$account_id='';
		$baccarat_limit_ids='';
		if($_GET['s_edit']=='1'){
			if(strlen($_GET['account_id'])<10){
				$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_parent_id'];
			}else{
				$rd=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_account_id'];
			}
			
		}else{
			$account_id=$_GET['account_id'];
		}
		
		
		$connection = Yii::app()->db_cv999_fd_master;
 		$command = $connection->createCommand("SELECT limit_baccarat FROM tbl_agent WHERE account_id='". $account_id . "'");
 		$command->query();
		
 		foreach($command->query() as $rd){
 			//$baccarat_limit_ids.= $rd['baccarat_limit_id'] . '_';
 			$baccarat_limit_ids= $rd['limit_baccarat'];
 		}
		echo $baccarat_limit_ids;
		
	}
	public function getSelectedRouletteTableLimitIds()
	{
		$account_id='';
		$roulette_limit_ids='';
		if($_GET['s_edit']=='1'){
			if(strlen($_GET['account_id'])<10){
				$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_parent_id'];
			}else{
				$rd=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_account_id'];
			}
		}else{
			$account_id=$_GET['account_id'];
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT limit_roulette FROM tbl_agent WHERE account_id='". $account_id . "'");
		$command->query();
	
		foreach($command->query() as $rd){
			$roulette_limit_ids= $rd['limit_roulette'];
		}
		echo $roulette_limit_ids;
	}
	public function getSelectedDragonTableLimitIds()
	{
		$account_id='';
		$dragon_limit_ids='';
		if($_GET['s_edit']=='1'){
			if(strlen($_GET['account_id'])<10){
				$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_parent_id'];
			}else{
				$rd=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_account_id'];
			}
		}else{
			$account_id=$_GET['account_id'];
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT limit_dragon_tiger FROM tbl_agent WHERE account_id='". $account_id . "'");
		$command->query();
	
		foreach($command->query() as $rd){
			$dragon_limit_ids= $rd['limit_dragon_tiger'];
		}
		echo $dragon_limit_ids;
	}
	public function getSelectedBlackjackTableLimitIds()
	{
		$account_id='';
		$blackjack_limit_ids='';
		if($_GET['s_edit']=='1'){
			if(strlen($_GET['account_id'])<10){
				$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_parent_id'];
			}else{
				$rd=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_account_id'];
	
			}
				
		}else{
			$account_id=$_GET['account_id'];
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT limit_blackjack FROM tbl_agent WHERE account_id='". $account_id . "'");
		$command->query();
	
		foreach($command->query() as $rd){
			$blackjack_limit_ids= $rd['limit_blackjack'];
		}
		echo $blackjack_limit_ids;
	
	}
	public function getSelectedAmericanRouletteTableLimitIds()
	{
		$account_id='';
		$american_roulette_limit_ids='';
		if($_GET['s_edit']=='1'){
			if(strlen($_GET['account_id'])<10){
				$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_parent_id'];
			}else{
				$rd=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
				$account_id=$rd['agent_account_id'];
			}
		}else{
			$account_id=$_GET['account_id'];
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT limit_american_roulette FROM tbl_agent WHERE account_id='". $account_id . "'");
		$command->query();
	
		foreach($command->query() as $rd){
			$american_roulette_limit_ids= $rd['limit_american_roulette'];
		}
		echo $american_roulette_limit_ids;
	}
}