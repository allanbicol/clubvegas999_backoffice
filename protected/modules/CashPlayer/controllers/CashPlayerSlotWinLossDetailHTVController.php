<?php
class CashPlayerSlotWinLossDetailHTVController extends MyController
{
	public function actionCashPlayerSlotWinLossDetailHTV()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossDetailHTV;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerSlotWinLossDetailHTV();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerSlotWinLossDetailHTV($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("bet_time","account_id","account_name","currency","game_type","bet_amount","win_loss","commission","jackpot_money","total_win_loss","balance_before","balance_after","casino_name","counter");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionGetRecordAllPages()
	{
		$playersRecordAllPages=new CashPlayerSlotWinLossDetailHTV();
		$playersRecordAllPages->getPlayerSlotWinLossDetailHTVAllPages();
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionHTVSlotWinLossDetailExcel(){
		$file="CashPlayerHTVSlot_WinLossDetail".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Costavegas Slot Winloss details</label> to excel file</b>\n'.$params[0].'\n<b>Date From</b> = '.$params[1].'\n<b>Date To</b> = '.$params[2];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
}