<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/systemsetting.css" />
<script type="text/javascript">
	document.getElementById('systemsettingHeader').className="start active";
	document.getElementById('mnu_user_account').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Change Password</a></li>");
	$(document).ready(function(e) {
		
	});
	function changePassword()
	{
		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/UserList/ChangePassword',
    		type: 'POST',
    		data: {'oldPassword': document.getElementById('oldpassword').value, 
        		'newPassword' : document.getElementById('newpassword').value,
        		'confirmPassword' : document.getElementById('confirmnewpassword').value,
    			},
        		
    		context: '',
    		success: function(msg) {
    			 jQuery("#changePasswordMsg").html("<strong>" + msg + "</strong>");
    	         jQuery("#changePasswordMsg").css("color","red");
	    	}
    	});
	}
	
</script>
</head>
<body>
	<div id="cp_header">Change Password</div>
	<div id="body_wrapper">
	<form action="" method="POST">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left">Account ID</td><td class="right"><b><?php echo Yii::app()->session['account_id'];?></b></td></tr>
			<tr><td class="left">Old Password</td><td class="right"><input type="password" name="oldpassword" id="oldpassword" class="txt"/></td></tr>
			<tr><td class="left">New Password</td><td class="right"><input type="password" name="newpassword" id="newpassword" class="txt"/></td></tr>
			<tr><td class="left">Confirm Password</td><td class="right"><input type="password" name="confirmnewpassword" id="confirmnewpassword" class="txt"/></td></tr>
			<tr><td></td><td><input class="btn red" type="button" name="confirm" onclick="changePassword()" value="Confirm"/> <input class="btn red" type="button" name="cancel" value="Cancel"/></td></tr>
		</table>
	</form>
	<center><span id="changePasswordMsg"></span></center>
	</div>
</body>
</html>