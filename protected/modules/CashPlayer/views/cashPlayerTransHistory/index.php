<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_trans2').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Transaction History Detailed</a></li>");
</script>
<!-- <link rel="stylesheet" type="text/css" href="css/calendar/datepicker.css" />  -->
<!-- <script type="text/javascript" src="js/calendar/datepicker.js"></script> -->

<script type="text/javascript">
//===GRID FOR BONUS==================================

	function tableBonusTransHistoryToday(trans_type,status_type,dateFrom,dateTo,testChecked,accountId) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'listlogBonus'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
	
		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
	
			jQuery("#listlogBonus").jqGrid({ 
			url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/CashplayerTransHistoryForBonus&trantype='+trans_type+'&status='+status_type+'&datefrom='+dateFrom+'&dateto='+dateTo+'&test='+testChecked+'&accountId='+accountId, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['Transaction Number','Account ID','Full Name', 'Cage Cashier Account','Amount','Bonus Type','Bonus Before','Bonus After','Currency','Finished Time','Status'],
		    colModel: [
				{name: 'transNumber', index: 'transNumber', width: 125, search:true,title:false},
				{name: 'player_id', index: 'player_id', width: 100,title:false},
				{name: 'full_name', index: 'full_name', width: 120,title:false},
				{name: 'cashierID', index: 'cashierID', width: 130,title:false},
				{name: 'Amount', index: 'Amount', width: 90, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				{name: 'deposit_via', index: 'deposit_via', width: 130,title:false},
				{name: 'balance_before', index: 'balance_before', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				{name: 'balance_after', index: 'balance_after', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				{name: 'currencyName', index: 'currencyName', width: 60,title:false},
				{name: 'submitTime', index: 'submitTime', width: 125,title:false},
				{name: 'status', index: 'status', width: 70,title:false},
		    ],
		    afterInsertRow : function(rowid, rowdata)
		    {
		        if (rowdata.Amount < 0)
		        {
		           $(this).jqGrid('setCell', rowid,5 ,'', {color:'#990000'});
		        }

		    },
		    loadtext:"",
			loadComplete: function() {
				var grid=jQuery("#listlogBonus");
		        var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	grid.jqGrid('setCell',i,"balance_before","",{background:'#f7f7c8'});
			    	grid.jqGrid('setCell',i,"balance_after","",{background:'#cfe5fa'});
				}   
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rownumbers:true,
		    rowNum: 100,	
		    rowList: [20, 50, 100,200,500,99999],
		    pager: '#pager1',
		    sortname: 'submitTime',
		    sortorder: 'DESC',
		    caption: '<label style="color:#D84A38">BONUS</label> TRANSACTION HISTORY',
		    viewrecords: true
		});
		
		$('#listlogBonus').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false });	
		jQuery("#listlogBonus").jqGrid('navButtonAdd','#pager1',{
            caption:"Export current page to Excel", 
            buttonicon:"ui-icon-calculator", 
            onClickButton: function(){ 
            	exportBonusToExcel();
           }, 
            position:"last"
        });
		$('#listlogBonus').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false });	
		jQuery("#listlogBonus").jqGrid('navButtonAdd','#pager1',{
            caption:"Export all pages to Excel", 
            buttonicon:"ui-icon-calculator", 
            onClickButton: function(){ 
            	exportAllBonusToExcel(trans_type,status_type,dateFrom,dateTo,testChecked,accountId);
           }, 
            position:"last"
        });
	}

//======GRID FOR DEPOSIT AND WITHDRAWAL=========================================
	function tableFundTransHistoryToday(trans_type,status_type,dateFrom,dateTo,testChecked,accountId) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'listlogHistory'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_resultTotal").appendChild(divTag);
	
		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_resultTotal").appendChild(divTag1);
		
			jQuery("#listlogHistory").jqGrid({ 
			url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/CashplayerTransHistory&trantype='+trans_type+'&status='+status_type+'&datefrom='+dateFrom+'&dateto='+dateTo+'&test='+testChecked+'&accountId='+accountId, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['Transaction Number','Account ID','Full Name', 'Type','Currency','Amount','Balance Before','Balance After','Transaction via','Submit Time','Finish Time','Cage Cashier Account','Status'],
		    colModel: [
				{name: 'transNumber', index: 'transNumber', width: 125, search:true,title:false},
				{name: 'player_id', index: 'player_id', width: 100,title:false},
				{name: 'full_name', index: 'full_name', width: 120,title:false},
				{name: 'transTypeName', index: 'transTypeName', width: 120,title:false},
				{name: 'currencyName', index: 'currencyName', width: 60,title:false},
				{name: 'Amount', index: 'Amount', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				{name: 'balance_before', index: 'balance_before', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				{name: 'balance_after', index: 'balance_after', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				{name: 'deposit_via', index: 'deposit_via', width: 145,title:false},
				{name: 'submitTime', index: 'submitTime', width: 125,title:false},
				{name: 'finishTime', index: 'finishTime', width: 125,title:false},
				{name: 'cashierID', index: 'cashierID', width: 135,title:false},
				{name: 'status', index: 'status', width: 70,title:false},
		    ],
		    loadtext:"",
		    loadComplete: function() {
				var grid=jQuery("#listlogHistory");
		        var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	grid.jqGrid('setCell',i,"balance_before","",{background:'#f7f7c8'});
			    	grid.jqGrid('setCell',i,"balance_after","",{background:'#cfe5fa'});
				}   
		    },
		    rownumbers:true,
		    rowNum: 100,	
		    rowList: [20, 50, 100,200,500,99999],
		    pager: '#pager2',
		    sortname: 'submitTime',
		    sortorder: 'DESC',
		    caption: '<label style="color:#D84A38">COUNTER</label> TRANSACTION HISTORY',
		    viewrecords: true,
		    onCellSelect :  function(rowid, index, contents, event) {
		    	var grid=jQuery("#listlogHistory");
		    	var userlever='<?php echo Yii::app()->user->checkAccess('cashPlayer.writeTransHistorySummary');?>';
		    	if(userlever=='1'){
				    if (index==9){
				    	var myrow = grid.jqGrid('getRowData', rowid);
					    //$createAlterBankName.dialog('open');
					   	//document.getElementById('oldBank').value=myrow.deposit_via;
					   //	document.getElementById('transNum').value=myrow.transNumber;
                    }
		    	}
		    }
		});
		
		$('#listlogHistory').jqGrid('navGrid', '#pager2', { edit: false, add: false, del:false, search:false });

		function DepositViaFormatter(cellvalue, options, rowObject) {
			var celval=cellvalue.split(":");
	        $("cellvalue").val(celval[1]);

			return celval[1];
		};
		jQuery("#listlogHistory").jqGrid('navButtonAdd','#pager2',{
            caption:"Export current page to Excel", 
            buttonicon:"ui-icon-calculator", 
            onClickButton: function(){ 
            	exportFundToExcel();
           }, 
            position:"last"
        });
		jQuery("#listlogHistory").jqGrid('navButtonAdd','#pager2',{
            caption:"Export all pages to Excel", 
            buttonicon:"ui-icon-calculator", 
            onClickButton: function(){ 
            	exportAllFundToExcel(trans_type,status_type,dateFrom,dateTo,testChecked,accountId);
           }, 
            position:"last"
        });
	}

	
</script>

<script type="text/javascript">

function exportBonusToExcel(){
	var table	= 	document.getElementById('qry_result');
	var method	=	$('#selMethod')[0].value;
	var type	=	$('#selType')[0].value;
	var status	=	$('#selStatus')[0].value;
	var testC	=	$('#chkTest')[0].value;
	var accountId	=	$('#txtAccountId')[0].value;
	var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
	var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
	
	var html 	= 	table.outerHTML;
	document.forms[2].csvBuffer.value="";
	document.forms[2].exportCount.value=$('.ui-paging-info')[0].innerHTML+'#'+method+'#'+type+'#'+status+'#'+testC+'#'+accountId+'#'+dateFrom+'#'+dateTo;
	document.forms[2].csvBuffer.value=html;
    document.forms[2].method='POST';
    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/BonusExcel';  // send it to server which will open this contents in excel file
    document.forms[2].target='_top';
    document.forms[2].submit();
}

function exportFundToExcel(){
	var table= document.getElementById('qry_resultTotal');
	var method	=	$('#selMethod')[0].value;
	var type	=	$('#selType')[0].value;
	var status	=	$('#selStatus')[0].value;
	var testC	=	$('#chkTest')[0].value;
	var accountId	=	$('#txtAccountId')[0].value;
	var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
	var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
	
	var html = table.outerHTML;
	document.forms[2].csvBuffer.value="";
	document.forms[2].exportCount.value=$('.ui-paging-info')[1].innerHTML+'#'+method+'#'+type+'#'+status+'#'+testC+'#'+accountId+'#'+dateFrom+'#'+dateTo;
	document.forms[2].csvBuffer.value=html;
    document.forms[2].method='POST';
    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/FundExcel';  // send it to server which will open this contents in excel file
    document.forms[2].target='_top';
    document.forms[2].submit();
}

function exportAllFundToExcel(trans_type,status_type,dateFrom,dateTo,testChecked,accountId){
    $.ajax({
		 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/ExportAllFundTransactionHistory&trantype='+trans_type+'&status='+status_type+'&datefrom='+dateFrom+'&dateto='+dateTo+'&test='+testChecked+'&accountId='+accountId,
		 async:true,
		 success: function(result) {
				var data =result.split("<BREAK>");
				
				var method	=	$('#selMethod')[0].value;
				var type	=	$('#selType')[0].value;
				var status	=	$('#selStatus')[0].value;
				var testC	=	$('#chkTest')[0].value;
				var accountId	=	$('#txtAccountId')[0].value;
				var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
				var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
				
				document.forms[2].csvBuffer.value=data[0];
				document.forms[2].exportCount.value='<b>No. of Records:</b> '+data[1]+'#'+method+'#'+type+'#'+status+'#'+testC+'#'+accountId+'#'+dateFrom+'#'+dateTo;
				document.forms[2].method='POST';
			    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/FundExcel';
			    document.forms[2].target='_top';
			    document.forms[2].submit();
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
}

function exportAllBonusToExcel(trans_type,status_type,dateFrom,dateTo,testChecked,accountId){
    $.ajax({
		 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/ExportAllBonusTransactionHistory&trantype='+trans_type+'&status='+status_type+'&datefrom='+dateFrom+'&dateto='+dateTo+'&test='+testChecked+'&accountId='+accountId,
		 async:true,
		 success: function(result) {
				var data =result.split("<BREAK>");
				
				var method	=	$('#selMethod')[0].value;
				var type	=	$('#selType')[0].value;
				var status	=	$('#selStatus')[0].value;
				var testC	=	$('#chkTest')[0].value;
				var accountId	=	$('#txtAccountId')[0].value;
				var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
				var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';

				document.forms[2].csvBuffer.value=data[0];
				document.forms[2].exportCount.value='<b>No. of Records:</b> '+data[1]+'#'+method+'#'+type+'#'+status+'#'+testC+'#'+accountId+'#'+dateFrom+'#'+dateTo;
				document.forms[2].method='POST';
			    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory/BonusExcel';
			    document.forms[2].target='_top';
			    document.forms[2].submit();
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
}

function disableCombos()
{
	if (document.getElementById('selMethod').value=="2")
	{
		document.getElementById('selType').disabled=true;
		document.getElementById('selStatus').disabled=true;
	}
	else
	{
		document.getElementById('selType').disabled=false;
		document.getElementById('selStatus').disabled=false;
	}
}

</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});
	
	var sChar=false;
	function alphanumeric(inputtxt)  
	{  
		var letters = /^[0-9a-zA-Z]+$/;  
		if(inputtxt.match(letters) || inputtxt=='')  
		{  
			sChar=false;  
		}else{  
			sChar=true;  
		}  
	}  
	function loadTransTable(method_type,trans_type,status_type,dateFrom,dateTo,testChecked,accountId)
	{
		if (method_type=='All')
		{
			//for bonus
	    	document.getElementById('qry_result').innerHTML='';
	    	tableBonusTransHistoryToday(trans_type,status_type,dateFrom,dateTo,testChecked,accountId);
	    	//for fund total
	    	document.getElementById('qry_resultTotal').innerHTML='';
	    	tableFundTransHistoryToday(trans_type,status_type,dateFrom,dateTo,testChecked,accountId);
		}
	    else if (method_type=='1')
	    {
	    	document.getElementById('qry_result').innerHTML='';
	    	document.getElementById('qry_resultTotal').innerHTML='';
	    	tableFundTransHistoryToday(trans_type,status_type,dateFrom,dateTo,testChecked,accountId);
		}
	    else if (method_type=='2')
	    {
	    	document.getElementById('qry_resultTotal').innerHTML='';
	    	document.getElementById('qry_result').innerHTML='';
	    	tableBonusTransHistoryToday(trans_type,status_type,dateFrom,dateTo,testChecked,accountId);
		}
	}

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{
			document.getElementById("chkTest").checked=true;
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();

			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}

			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			var datefrom=(theyear+"/"+month+"/"+day + "_00:00:00");
			var dateto=(theyear+"/"+month+"/"+day+ "_23:59:59");
			var accountId=document.getElementById('txtAccountId').value;
			
			loadTransTable("All","All","All",datefrom,dateto,testChecked,accountId);
		}
		else if (btnname=="Today")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();

			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('selMethod').value;
			var transtype=document.getElementById('selType').value;
			var statustype=document.getElementById('selStatus').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(methodtype,transtype,statustype,datefrom,dateto,testChecked,accountId);
			}
		}
		else if (btnname=="Yesterday")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('selMethod').value;
			var transtype=document.getElementById('selType').value;
			var statustype=document.getElementById('selStatus').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(methodtype,transtype,statustype,datefrom,dateto,testChecked,accountId);
			}
		}
		else if (btnname=="Submit")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var methodtype=document.getElementById('selMethod').value;
			var transtype=document.getElementById('selType').value;
			var statustype=document.getElementById('selStatus').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(methodtype,transtype,statustype,dateSubmitFrom,dateSubmitTo,testChecked,accountId);
			}
			
		}
	}

</script>


</head>
<body onload="btnclick('Load')">
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
<div id="parameter_area" style="width: 500px">
	<div class="header" >TRANSACTION HISTORY</div>
	<form action="">
		<table style="background-color:transparent;  width: 500px; margin-top: 5px;">
			<tr><td style="width: 30%;padding-left:5px;">METHOD:</td>
				<td style="width: 70%">
					<select id="selMethod" onchange="disableCombos()">
  						<option value="All">All</option>
  						<option value="1">Counter</option>
 			 			<option value="2">Bonus</option>
					</select></td>
			</tr>
			<tr><td style="width: 30%;padding-left:5px;">TYPE:</td>
				<td><select id="selType">
  						<option value="All">All</option>
			  			<option value="1">Automatic Deposit</option>
			  			<option value="2">Manual Deposit</option>
			  			<option value="3">Automatic Withdrawal</option>
			  			<option value="4">Manual Withdrawal</option>
			  			<option value="11">Deduction</option>
					</select><br/>
				</td>
			</tr>
			<tr><td style="width: 30%;padding-left:5px;">STATUS:</td>
				<td><select id="selStatus">
			  			<option value="All">All</option>
			 			<option value="Completed">Complete</option>
			 			<option value="Pending">Pending</option>
					</select>&nbsp;&nbsp;
					<input type="checkbox" id="chkTest" checked><label id="lblExcept">Except TEST currency</label>
				</td>
			</tr>
			<tr>
				<td style="width: 30%;padding-left:5px;">ACCOUNT ID:</td>
				<td><input type="text" id="txtAccountId" style="width: 180px"></td>
			</tr>
			<tr><td style="width: 30%;padding-left:5px;">FROM:</td>
				<td><input style="width: 130px"  type="text" id="datefrom" name="datefrom" value="" />
					<select id="cbHourfrom">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
									echo '<option>'.$value.'</option>';
								}
						?></select>
						<input value=":00:00" style="border: 0px; background-color: transparent; width: 60px" disabled></td></tr>
			<tr><td style="width: 30%;padding-left:5px;">TO:</td>
				<td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
					<select id="cbHourto">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
									echo '<option selected="true">'.$value.'</option>';
								}
						?></select>
						<input value=":59:59" style="border: 0px; background-color: transparent; width: 60px" disabled></td></tr>
			</table><br/>
			<div align="center">
			<input onclick="javacript:btnclick(this.value);" type="button" value="Submit" class="btn red">
			<input onclick="javascript:btnclick(this.value);" type="button" value="Yesterday" class="btn red">
			<input onclick="javascript:btnclick(this.value);" type="button" value="Today" class="btn red">
			<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red"></div>
	
	</form>	
	<br>
	<script type="text/javascript">
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	document.getElementById("datefrom").value=(year + "-" + month + "-" +day );
	document.getElementById("dateto").value=(year + "-" + month + "-" + day);
	</script>
</div>
	<div id="qry_result"></div>
	<br/>
	<div id="qry_resultTotal"></div>
	
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="exportCount" id="exportCount" value="" />
	</form>
</body>


</html>