<?php

class AdvisoryController extends MyController
{
	/**
	 * 
	 * @var fields
	 */
	private $keyUpdate = array('saveOld', 'saveNew');

	/**
	 *
	 */
	public function actionDelete()
	{
		if(Yii::app()->user->checkAccess('systemSetting.writeAdvisory'))
		{
			// save
			//print_r($_POST);
			$id = isset($_POST['id']) ? $_POST['id']: 0;
			// validate
			if ($id > 0)
			{
				// instance
				$am = new AdvisoryModel();
				// delete the record and relation
				$am->deleteByID($id);
				echo '?r=' . Yii::app()->controller->module->id . '/' . Yii::app()->getController()->getId();
			}
			else
			{
				echo '#1';				
			}
		}
	}

    /**
     *
     */
    public function actionEdit()
    {
        if(Yii::app()->user->checkAccess('systemSetting.readAdvisory'))
        {
            $am = new AdvisoryModel();
            $id = $_GET['id'];

            $result = $am->getDetailByID($id);
            // print_r($result);
            $this->render("edit");
        }
        else
        {
            $this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
        }
    }

    /**
     *
     */
    public function actionNew()
    {
        if(Yii::app()->user->checkAccess('systemSetting.readAdvisory'))
        {
            $am = new AdvisoryModel();

            // print_r($result);
            $this->render("new");
        }
        else
        {
            $this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
        }
    }

	/**
	 * 
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readAdvisory'))
		{
			$this->render("index");
		}
		else
		{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}

    public function actionGetById()
    {
        // instance language
        $list = new Languages();
        $am = new AdvisoryModel();
        // validate null and integer
        $id = isset($_POST['id']) ? (is_numeric($_POST['id']) ? $_POST['id']: 0) : 0;
        // `id`, `published_category_id`, `site_code`, `account_type_id`, `schedule_category_id`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday`, `started_date`, `ended_date`
        $mainList = $am->getListByID($id);
    }

	/**
	 *
	 */
	public function actionGetById2()
	{
		// instance language
		$list = new Languages();
		$am = new AdvisoryModel();
		// validate null and integer
		$id = isset($_POST['id']) ? (is_numeric($_POST['id']) ? $_POST['id']: 0) : 0;
		// `id`, `published_category_id`, `site_code`, `account_type_id`, `schedule_category_id`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday`, `started_date`, `ended_date`
		$mainList = $am->getListByID($id);
 		if (count($mainList))
 		{
			// initial
			$mainJson = '';

			// start
			$mainJson .= '"accesses":' . $this->covertStringHadCommaToJSArray($mainList['account_type_id'])  . ',';
			$mainJson .= '"status":"' . $mainList['published_category_id'] . '",';
			$mainJson .= '"sites":' . $this->covertStringHadCommaToJSArray($mainList['site_code']) . ',';
			$mainJson .= '"servicesTypeID":"' . $mainList['services_affected_id'] . '",';
			$mainJson .= '"scheduleID":' . $mainList['schedule_category_id'] . ',';
			$mainJson .= '"scheduleStartedDate":"' . $mainList['started_date'] . '",';
			$mainJson .= '"scheduleEndedDate":"' . $mainList['ended_date'] . '",';
			$mainJson .= '"scheduleStartedDateShow":"' . $mainList['started_date_show'] . '",';
			$mainJson .= '"scheduleStartedTimeShow":"' . $mainList['started_time_show'] . '",';
			$mainJson .= '"imageID":"' . $mainList['icon_id'] . '",';
			$mainJson .= '"monday":' . $mainList['monday'] . ',';
			$mainJson .= '"tuesday":' . $mainList['tuesday'] . ',';
			$mainJson .= '"wednesday":' . $mainList['wednesday'] . ',';
			$mainJson .= '"thursday":' . $mainList['thursday'] . ',';
			$mainJson .= '"friday":' . $mainList['friday'] . ',';
			$mainJson .= '"saturday":' . $mainList['saturday'] . ',';
			$mainJson .= '"sunday":' . $mainList['sunday'];
			// main json
 			$mainJson .= ',';
 			// get data 
 			$contentList = $am->getContentListByAdvisoryID($id);
 			$json = '';
 			// extract
			foreach ($contentList as $c) {
				// check charactor and append comma
				if (strlen($json) > 3)
				{
					$json .= ',';
				}

				// by language 
				$json .= '"' . $c['language_code'] . '":{';
				$json .= '"servicetext":"' . str_replace('"', '\"', $c['services_affected']) . '",';
				$json .= '"description":"' . str_replace('"', '\"', $c['content']) . '"';
				$json .= '}';
			}
 			// content
 			echo '{' . $mainJson . $json . '}';
		}
	}

	/**
	 *
	 */
	public function actionGetList()
	{
		$am = new AdvisoryModel();
		$l = new Languages();

		// 
		$amList = $am->getList($l->getLanguageByLanguageCode(Languages::EnglishCode));

		// jqgrid
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		// count records
		$result = $am->getCountRows(); //$am->getCountRowsByLanguageCode(Languages::EnglishCode);
		$data= $am->getListToJGrid();

		// count records
		$records= $am->getCountRows();

		// check record number
		if ($records > 0 && $limit > 0)
		{
			$total_pages = ceil($records / $limit);
		}
		else
		{
			$total_pages = 0;
		}

		// check page number
		if ($page > $total_pages) $page = $total_pages;

		// url
        $urlEdit = '<a href=\"index.php?r=SystemSetting/Advisory/Edit&id=#\" class=\"btn mini red\" rel=\"#\">Edit <i class=\"icon-edit\"></i></a>';
		$urlDelete = '<a href=\"#\" class=\"btn mini red\" rel=\"#\">Delete <i class=\"icon-trash\"></i></a>';
		// field of grid
		$filedNames = array("id", "site", "published", "servicesAffected", "schedule", "startTime", "endTime", "modify");
		// echo JsonUtil::jsonEncodeSystemSetting($am->getListToJGrid(), $total_pages, $page, $records,$filedNames,$htmValue,4);
		echo JsonUtil::jsonAdvisory($am->getListToJGrid(), $total_pages, $page, $records, $filedNames, $urlEdit, $urlDelete);
	}

	/**
	 * 
	 */
	public function actionSave()
	{
		if(Yii::app()->user->checkAccess('systemSetting.writeAdvisory'))
		{

			// save
			if (isset($_POST['advisoryKeyUpdateValue']) && $this->keyUpdate[$_POST['advisoryKeyUpdateValue']] == 'saveOld')
			{
				// 
				$this->saveOld();
			}
			else if (isset($_POST['advisoryKeyUpdateValue']) && $this->keyUpdate[$_POST['advisoryKeyUpdateValue']] == 'saveNew')
			{
				$this->saveNew();
			}
			else
			{
				echo ':P';
			}
		}
	}

	/**
	 *
	 */
	private function covertStringHadCommaToJSArray($stringHasCommaSymbol) {
		// find comma in string
		if (strpos($stringHasCommaSymbol, ',') > 0) {
			$temp = '';
			$list = explode(',', $stringHasCommaSymbol);
			foreach ($list as $l) {
				$temp .= '"' . $l . '",';
			}
			return '[' . rtrim($temp, ',') . ']';
		}
	
		return '["' . $stringHasCommaSymbol . '"]';
	}

	/**
	 * 
	 */
	private function saveNew()
	{
		if(!Yii::app()->user->checkAccess('systemSetting.writeAdvisory'))
		{
			exit(0);
		}
		
		// 
		$languages = Languages::getLanguages();
		$advisoryModel = new AdvisoryModel();

 		// loop
		$scheduleSelectedID = isset($_POST['sch_radio']) ? $_POST['sch_radio'][0] : 1;
		$status = isset($_POST['status']) ? $_POST['status']: '';
		$servicesAffectedID = isset($_POST['servicesAffect']) ? $_POST['servicesAffect']: 1;
		$siteList = isset($_POST['sites']) ? implode(',', $_POST['sites']) : ''; // 1 = all
		$iconID = isset($_POST['icon_id']) ? $_POST['icon_id']: 1;
		// previlege
		$accesses = AccountTypeModel::ALL_TYPES;
		// validate access, if it is array
		if (isset($_POST['accesses']) && count($_POST['accesses']) > 0)
		{
			// assign it first
			$accesses = isset($_POST['accesses']) ? implode(',', $_POST['accesses']) : AccountTypeModel::ALL_TYPES; // 1 = all
			// check again
			foreach ($_POST['accesses'] as $a) {
				// equal 1 = all
				if ($a == AccountTypeModel::ALL_TYPES) {
					$accesses = AccountTypeModel::ALL_TYPES;
					break;
				}
			}
		}

		// days
		$monday = isset($_POST['monday']) ? 1: 0;
		$tuesday = isset($_POST['tuesday']) ? 1: 0;
		$wednesday = isset($_POST['wednesday']) ? 1: 0;
		$thursday = isset($_POST['thursday']) ? 1: 0;
		$friday = isset($_POST['friday']) ? 1: 0;
		$saturday = isset($_POST['saturday']) ? 1: 0;
		$sunday = isset($_POST['sunday']) ? 1: 0;

		// date
		$startDate = isset($_POST['startDate']) ? $_POST['startDate']: '';
		$endDate = isset($_POST['endDate']) ? $_POST['endDate']: '';

		// time
		$startTime = isset($_POST['startTime']) ? $_POST['startTime']: '';
		$endTime = isset($_POST['endTime']) ? $_POST['endTime']: '';

		// show date
		$startDateShow = isset($_POST['startDateShow']) ? $_POST['startDateShow']: '';
		$startTimeShow = isset($_POST['startTimeShow']) ? $_POST['startTimeShow']: '';

		// text
		$contents = array();
		$servicesAffectTexts = array();

		// content
		foreach ($languages as $l)
		{
			$contents[$l['language_code']] = $_POST[$l['language_code'] . '_editor'];
			$servicesAffectTexts[$l['language_code']] = $_POST[$l['language_code'] . '_services_text'];
		}

		// daily, monday, tueday, startTime
		if ($scheduleSelectedID == 1 || $scheduleSelectedID == 2)
		{
			$advisoryModel->saveNewDaily($accesses, $status, $siteList, $iconID, $startTime, $endTime, $startDateShow, $startTimeShow, $scheduleSelectedID, $servicesAffectedID, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday, preg_replace('@[\s]{2,}@','', $servicesAffectTexts), preg_replace('@[\s]{2,}@','', $contents));
		}
		else if ($scheduleSelectedID == 3)
		{
			// startDate, endDate
			$advisoryModel->saveNewSomedays($accesses, $status, $siteList, $iconID, $startDate, $endDate, $startDateShow, $startTimeShow, $scheduleSelectedID, $servicesAffectedID, preg_replace('@[\s]{2,}@','', $servicesAffectTexts), preg_replace('@[\s]{2,}@','', $contents));
		}

		// back to home
		//$this->redirect('?r=' . Yii::app()->controller->module->id . '/' . Yii::app()->getController()->getId());

		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=SystemSetting/Advisory");
	}

	/**
	 * 
	 */
	private function saveOld()
	{
		if(!Yii::app()->user->checkAccess('systemSetting.writeAdvisory'))
		{
			exit(0);
		}

		// advisory id
		$advisoryID = isset($_POST['advisoryKeyID']) ? $_POST['advisoryKeyID']: 0;
		// 
		$languages = Languages::getLanguages();
		$advisoryModel = new AdvisoryModel();

 		// loop
		$scheduleSelectedID = isset($_POST['sch_radio']) ? $_POST['sch_radio'][0]: 1;
		$status = isset($_POST['status']) ? $_POST['status']: '';
		$servicesAffectedID = isset($_POST['servicesAffect']) ? $_POST['servicesAffect']: 1;
		$siteList = isset($_POST['sites']) ? implode(',', $_POST['sites']) : ''; // 1 = all
		$iconID = isset($_POST['icon_id']) ? $_POST['icon_id']: 1;
		// previlege
		$accesses = AccountTypeModel::ALL_TYPES;
		// validate access
		if (isset($_POST['accesses']) && count($_POST['accesses']) > 0)
		{
			// assign it first
			$accesses = isset($_POST['accesses']) ? implode(',', $_POST['accesses']) : AccountTypeModel::ALL_TYPES; // 1 = all
			// check again
			foreach ($_POST['accesses'] as $a) {
				// equal 1 = all
				if ($a == AccountTypeModel::ALL_TYPES) {
					$accesses = AccountTypeModel::ALL_TYPES;
					break;
				}
			}
		}
		
		// days
		$monday = isset($_POST['monday']) ? 1: 0;
		$tuesday = isset($_POST['tuesday']) ? 1: 0;
		$wednesday = isset($_POST['wednesday']) ? 1: 0;
		$thursday = isset($_POST['thursday']) ? 1: 0;
		$friday = isset($_POST['friday']) ? 1: 0;
		$saturday = isset($_POST['saturday']) ? 1: 0;
		$sunday = isset($_POST['sunday']) ? 1: 0;

		// date
		$startDate = isset($_POST['startDate']) ? $_POST['startDate']: '';
		$endDate = isset($_POST['endDate']) ? $_POST['endDate']: '';

		// time
		$startTime = isset($_POST['startTime']) ? $_POST['startTime']: '';
		$endTime = isset($_POST['endTime']) ? $_POST['endTime']: '';

		// show date
		$startDateShow = isset($_POST['startDateShow']) ? $_POST['startDateShow']: '';
		$startTimeShow = isset($_POST['startTimeShow']) ? $_POST['startTimeShow']: '';

		// text
		$contents = array();
		$servicesAffectTexts = array();

		// content
		foreach ($languages as $l)
		{
			$contents[$l['language_code']] = $_POST[$l['language_code'] . '_editor'];
			$servicesAffectTexts[$l['language_code']] = $_POST[$l['language_code'] . '_services_text'];
		}

		// daily, monday, tueday, startTime, 1 = everyday
		if ($scheduleSelectedID == 1 || $scheduleSelectedID == 2)
		{
			$advisoryModel->updateDaily($advisoryID, $accesses, $status, $siteList, $iconID, $startTime, $endTime, $startDateShow, $startTimeShow, $scheduleSelectedID, $servicesAffectedID, $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday, preg_replace('@[\s]{2,}@','', $servicesAffectTexts), preg_replace('@[\s]{2,}@','', $contents));
		}
		else if ($scheduleSelectedID == 3)
		{
			// startDate, endDate
			$advisoryModel->updateSomedays($advisoryID, $accesses, $status, $siteList, $iconID, $startDate, $endDate, $startDateShow, $startTimeShow, $scheduleSelectedID, $servicesAffectedID, preg_replace('@[\s]{2,}@','', $servicesAffectTexts), preg_replace('@[\s]{2,}@','', $contents));
		}

		// back to landing
		// $this->redirect('?r=' . Yii::app()->controller->module->id . '/' . Yii::app()->getController()->getId());
		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=SystemSetting/Advisory");
	}
}

?>