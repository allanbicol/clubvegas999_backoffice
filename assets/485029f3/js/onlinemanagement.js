//active menu color
document.getElementById('systemmonitorHeader').className="start active";
document.getElementById('mnu_online_management').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>System Monitor <i class='icon-angle-right'></i></li><li><a href='#'>Online User Management</a></li>");
function onTheLoad(){
	tableOnlineUsers();
	//tableLockUsers();
	document.getElementById('cmbLevel').value='player';
}
function tableOnlineUsers() { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);

	var grid=jQuery("#list1");
	grid.jqGrid({ 
		url: urlOnlineUserList,
		datatype: 'json',
	    mtype: 'POST',
	    colNames: ['Account ID', 'Level','CV999 Login Date','Casino','Casino Login Date','IP Address','Country','Site','Operation'],
	    colModel: [
	      {name: 'account_id', index: 'account_id',sorttype:"string", width: 100,title:false,sorttype:"string"},
          {name: 'level', index: 'level',sorttype:"string", width: 150,title:false,formatter: levelFormatter,sorttype:"string"},
          {name: 'cv999_login_date', index: 'cv999_login_date',sorttype:"string", width: 150,title:false,sorttype:"string"},
          {name: 'casino_name', index: 'casino_name',sorttype:"string", width: 100,title:false,formatter:casinoFormatter,sorttype:"string"},
          {name: 'casino_login_date', index: 'casino_login_date',sorttype:"string", width: 150,title:false,sorttype:"string"},
          {name: 'ip_address', index: 'ip_address',sorttype:"string", width: 250,title:false,sorttype:"string"}, 
          {name: 'country', index: 'country',sorttype:"string", width: 250,title:false,sorttype:"string"}, 
          {name: 'site', index: 'site',sorttype:"string", width: 300,title:false,sorttype:"string"}, 
          {name: 'operation', index: 'operation',sorttype:"string", width: 80,title:false,formatter:operationFormatter,sortable: false,}, 
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
    	},
	    rowNum: 100,
	    rownumbers:true,
	    rowList: [20, 50, 100,200,500,99999],
	    pager: '#pager1',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    caption: 'Online Users List',
	    hidegrid: false,
	    height: '100%',
	    loadonce: true, // to enable sorting on client side
        sortable: true, //to enable sorting
        viewrecords: true
	});
	$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del: false, search: false, refresh: false});
    
} 




/*
 * @todo force logout player
 * @author leokarl
 * @date 2012-12-08
 */

function logout(player_id,player_level){
	jQuery.ajax({
		url: urlLogOutUser,
		type: 'POST',
		data: {'player_id': player_id,'player_level':player_level},
		context: '',
		success: function(msg) {
			alert(msg);
    		$createForceLogoutConfirmation.dialog('close');
    		tableOnlineUsers();//refresh list
    	}
	});
}

/*
 * @todo force logout player
 * @author leokarl
 * @date 2012-12-08
 */
/*
function logout(player_id,player_level){
	$('.logout').attr('disabled','disabled');// disable forcelogout button
	jQuery.ajax({
		url: urlLogOutUser,
		type: 'POST',
		data: {'player_id': player_id,'player_level':player_level},
		context: '',
		async: false,
		success: function(msg) {
		
			jQuery.ajax({
				url: urlWaitIfBalanceStuckedOnLobby, 
				type: 'POST',
				data: {'player_id': player_id},
				success: function(s_stuck) {
					
					if(s_stuck==0){ 
					// balance not stuck 
						
						alert(msg); // logout confirmation message
						$createForceLogoutConfirmation.dialog('close'); // close ForceLogout dialog
			    		tableOnlineUsers();//refresh list
					}else{ 
					// balance stuck
						withdrawFromCasinoLobby(player_id); // balance withdraw from Casino Lobby
						
					}
					
		    	}
			});
				
    	}
	});
}
*/

/*
function startCounting(){
	var counter=document.getElementById('counter').innerHTML;
	counter=parseFloat(counter)-1;
	document.getElementById('counter').innerHTML=counter;
	if (counter==0){
		$createCounterDialog.dialog('close');
		tableOnlineUsers();//instead of $("#list1").trigger("reloadGrid");
		$("#list2").trigger("reloadGrid"); 
	}
}
*/


/*
 * @todo open when startCounting function clicked
 * @author leokarl
 * @date 2012-12-08
 */
/*
var $createCounterDialog='';
$(document).ready(function() {
	$createCounterDialog = $('<div></div>')
		.html('<div id="dvDeposit">'
				+'<div><input id="logoutAccountId" type="hidden"></input></div>'
				+'<center><div>Player  <label style="color:red" id="logoutName1"></label> will completely logout in</div></center><br/><br/>'
				+'<center><label style="color:red; font-size:25px;" id="counter">45</label> seconds</center><br/>'
    			+'</div>')
		.dialog({
			autoOpen: false,
			width: 400,
			title: 'Logout',
			resizable: true,
			modal: true
		});
		$(".ui-dialog-titlebar-close").hide(); 
		$(".ui-dialog-titlebar-close").hide();
		setInterval(function(){startCounting();},1000);
});
*/
