
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/systemsetting.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/currencylist.js"></script>
<script type="text/javascript">
	var createCurrencyURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CreateCurrency/CreateCurrency';
	var updateCurrencyURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/UpdateCurrency/UpdateCurrency';
	var currencyListURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CurrencyList/CurrencyList';
	var currencyListLogURL="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CurrencySettingLog";
</script>
</head>
<body>
	<table id="list2" ><tr><td/></tr></table>
	<div id="pager2"></div>
</body>
</html>
