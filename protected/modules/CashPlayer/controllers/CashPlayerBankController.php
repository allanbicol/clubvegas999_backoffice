<?php
/**
 * @todo CashPlayerBankController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerBankController extends MyController
{
	public function actionCashPlayerBankProcess()
	{
		$cpd = new CashPlayerBank();
		$cpd->makeNewBank();
	}
}
