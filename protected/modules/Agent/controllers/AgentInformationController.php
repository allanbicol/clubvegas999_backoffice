<?php
/**
 * @todo AgentNewSubCompanyController
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-15
 */
class AgentInformationController extends MyController
{
	public function actionAgentOverviewDetails()
	{
		$bl=new AgentInformation();
		
		$records=0;
		$player_records=$bl->getAgentOverview();
		//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
		if(Yii::app()->session['account_type']=='user'){
			$filedNames = array("currency","sc","sc_credit_assigned","sc_percentage","sma","sma_credit_assigned","sma_percentage","ma","ma_credit_assigned","ma_percentage","agt","agt_credit_assigned","agt_percentage","mem");
		}elseif(Yii::app()->session['level']=='SC'){
			$filedNames = array("currency","sma","sma_credit_assigned","sma_percentage","ma","ma_credit_assigned","ma_percentage","agt","agt_credit_assigned","agt_percentage","mem");
		}elseif(Yii::app()->session['level']=='SMA'){
			$filedNames = array("currency","ma","ma_credit_assigned","ma_percentage","agt","agt_credit_assigned","agt_percentage","mem");
		}elseif(Yii::app()->session['level']=='MA'){
			$filedNames = array("currency","agt","agt_credit_assigned","agt_percentage","mem");
		}elseif(Yii::app()->session['level']=='AGT'){
			$filedNames = array("currency","mem");
		}
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);
	}
	public function actionAgentOverviewConvertion()
	{
		$bl=new AgentInformation();
		$bl->convertAmount();
	}
	
	public function actionIndex()
	{
		//if(Yii::app()->user->checkAccess('agent.readAgentOverview') || User::getUserType() === Constants::ROLE_AGENT)
			$this->render("index");
		//else
		//	$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	public function tableCurrency()
	{
		$cur=TableCurrency::model()->findAll();
		foreach($cur as $row){
			echo '<option value="'.$row['currency_name'].'">'.$row['currency_name'].'</option>';
		}
	}
	public function yesterdayWinLoss($except_test_currency){
		$ss=TableAgent::model()->find('account_id=:account_id', array(':account_id'=>Yii::app()->session['account_id']));
		$s1=TableCurrency::model()->find('id=:id', array(':id'=>$ss['currency_id']));
		//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
		if(Yii::app()->session['account_type']=='user'){
			$currency_name_1='USD';
		}else{
			$currency_name_1=$s1['currency_name'];
		}
	
		$ai = new AgentInformation();
		$result=$ai->getYesterdayWinLoss($currency_name_1,$except_test_currency)->readAll();
		foreach($result as $row){
			echo '<label id="lblYesterdayCurrency">'.$currency_name_1.'</label> : <label id="lblYesterdayWinLoss">' . number_format($row['total_winloss'],2).'</label>';
		}
	}
	public function yesterdaySlotWinLoss($except_test_currency){
		$sl=TableAgent::model()->find('account_id=:account_id', array(':account_id'=>Yii::app()->session['account_id']));
		$wl=TableCurrency::model()->find('id=:id', array(':id'=>$sl['currency_id']));
		//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
		if(Yii::app()->session['account_type']=='user'){
			$cur_nme='USD';
		}else{
			$cur_nme=$wl['currency_name'];
		}
	
		$ais = new AgentInformation();
		$ydsw_result=$ais->getYesterdaySlotWinLoss($cur_nme,$except_test_currency)->readAll();
		foreach($ydsw_result as $row){
			echo '<label id="lblYesterdaySlotCurrency">'.$cur_nme.'</label> : <label id="lblYesterdaySlotWinLoss">' . number_format($row['total_winloss'],2).'</label>';
		}
	}
	public function availableCredit($except_test_currency){
		//$currency='';
		//$currency_rate='';
		//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=11){
		if(Yii::app()->session['account_type']=='user'){
			$ac = new AgentInformation();
			$rsAssignedCredit=$ac->getAssignedCredit($except_test_currency)->readAll();
			foreach($rsAssignedCredit as $row){
				echo '<label id="lblAvailableCurrency">USD</label> : <label id="lblAvailableCredit">' . number_format($row['credit'],2).'</label>';
			}
		}else{
			$credit_percentage=0;
			$rd=TableAgent::model()->find('account_id=:account_id', array(':account_id'=>Yii::app()->session['account_id']));
			$cu=TableCurrency::model()->find('id=:id', array(':id'=>$rd['currency_id']));
			$currency=$cu['currency_name'];
			$currency_rate=$cu['exchange_rate'];
			if($currency=='USD'){
				echo '<label id="lblAvailableCurrency">USD</label> : <label id="lblAvailableCredit">' . number_format($rd['credit'],2).'</label>';
				$credit_percentage=number_format($rd['credit_percentage'],2);
			}else{
				//if manager, admin, etc. then convert to usd
				//if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=8){
				if(Yii::app()->session['account_type']=='user'){
					echo '<label id="lblAvailableCurrency">USD</label> : <label id="lblAvailableCredit">' . number_format($rd['credit']/$currency_rate,2).'</label>';
					$credit_percentage=number_format($rd['credit_percentage']/$currency_rate,2);
				}else{
					if($currency=='TEST'){
						if($except_test_currency==0){
							echo '<label id="lblAvailableCurrency">'.$currency . '</label> : <label id="lblAvailableCredit">' . number_format($rd['credit'],2).'</label>';
							$credit_percentage=number_format($rd['credit_percentage'],2);
						}else{
							echo '<label id="lblAvailableCurrency">'.$currency . '</label> : <label id="lblAvailableCredit">' . number_format(0,2).'</label>';
							$credit_percentage=number_format(0,2);
						}
					}else{
						echo '<label id="lblAvailableCurrency">'.$currency . '</label> : <label id="lblAvailableCredit">' . number_format($rd['credit'],2).'</label>';
						$credit_percentage=number_format($rd['credit_percentage'],2);
					}
				}
			}
		}
	}
}