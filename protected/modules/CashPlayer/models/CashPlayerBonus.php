<?php
/**
 * @todo cash player set bonus
 * @copyright CE
 * @author Leo karl
 * @since 2012-12-21
 */
class CashPlayerBonus
{
	/**
	 * @todo save set player bonus
	 * @author leokarl
	 * @since 2012-12-21
	 * @param string $account_id
	 * @param string $bonus_amount
	 */
	public function saveSetPlayerBonus($account_id, $bonus_amount, $bonus_type)
	{	
		// initialize
		$bonus_before = $this->getCashPlayerFieldValueById('bonus', $account_id);
		$totalBonus = $this->getCashPlayerFieldValueById('bonus',$account_id) + $bonus_amount;
		$transNum = TransactionNumber::generateTransactionNumber("BU");
		$dateTime = date('Y-m-d H:i:s');
		
		if(!$this->sRecordExist($account_id, Yii::app()->session['account_id'], $bonus_amount, $dateTime, $this->getCashPlayerFieldValueById('currency_id', $account_id), $bonus_type)){
			// save to tbl_cash_player_bonus
			$postPlayerBonus = new CashPlayerSetBonus();
			$postPlayerBonus->cash_player_account_id = $account_id;
			$postPlayerBonus->bonus_amount = $bonus_amount;
			$postPlayerBonus->giving_date = $dateTime;
			$postPlayerBonus->currency_id = $this->getCashPlayerFieldValueById('currency_id', $account_id);
			$postPlayerBonus->operator_id = Yii::app()->session['account_id'];
			$postPlayerBonus->trans_number = $transNum;
			$postPlayerBonus->save();
			
			// save to tbl_cash_player_balance_history
			$postPlayerBonusHistory = new CashPlayerSetBonusHistory();
			$postPlayerBonusHistory->cash_player_account_id = $account_id;
			$postPlayerBonusHistory->balance = $bonus_amount;
			$postPlayerBonusHistory->trans_type_id = 6;
			$postPlayerBonusHistory->trans_date = $dateTime;
			$postPlayerBonusHistory->transaction_number = $transNum;
			$postPlayerBonusHistory->save();
			
			
			
			// save to tbl_cash_player_trans_history
			$postPlayerTransHistory = new CashPlayerSetBonusPlayerTransHistory;
			$postPlayerTransHistory->player_id = $account_id;
			$postPlayerTransHistory->trans_number = $transNum;
			$postPlayerTransHistory->cash_player_trans_type_id = 6;
			$postPlayerTransHistory->transaction_item = $bonus_type;
			$postPlayerTransHistory->currency_id = $this->getCashPlayerFieldValueById('currency_id', $account_id);
			$postPlayerTransHistory->amount = $bonus_amount;
			$postPlayerTransHistory->balance_before = $bonus_before;
			$postPlayerTransHistory->balance_after = $totalBonus;
			$postPlayerTransHistory->submit_time = $dateTime;
			$postPlayerTransHistory->finish_time = $dateTime;
			$postPlayerTransHistory->cashier_id = Yii::app()->session['account_id'];
			$postPlayerTransHistory->status_id = 6;
			$postPlayerTransHistory->save();
			
			
			// add logs
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated = $account_id;
			$postLog->operated_level = 'Player';
			$postLog->operation_time = $dateTime;
			$postLog->log_type_id = 13;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].
				'</label> Added Bonus:<label style=\"color:red\">'.number_format($bonus_amount,2,'.',',').'</label></b>';
			$postLog->save();
			
			// update tbl_cash_player
			$val=1;
			TableCashPlayer::model()->updateAll(array('based_bonus'=>$totalBonus,'bonus'=>$totalBonus,
			'last_update_balance'=>$dateTime,'kick_off'=>$val),'account_id="'.$account_id.'"');
		}
	}
	
	/**
	 * @todo check if record is already exist
	 * @author leokarl
	 * @since 2013-01-14
	 * @param string $account_id
	 * @param string $operator_id
	 * @param float $bonus_amount
	 * @param date $giving_date
	 * @param int $currency_id
	 */
	public function sRecordExist($account_id, $operator_id, $bonus_amount, $submit_time, $currency_id, $trans_item){
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_cash_player_trans_history WHERE player_id='" . $account_id . "'
				AND cashier_id='" . $operator_id . "' AND amount=" . $bonus_amount . " AND submit_time='" . $submit_time . "'
				AND currency_id=" . $currency_id . " AND transaction_item=" . $trans_item . "");
		$rows = $command->query();
		$count= $rows->readAll();
		return ($count[0]['COUNT(0)'] > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo get currency name by account id
	 * @author leokarl
	 * @since 2012-12-27
	 * @param string $account_id
	 */
	public function getCurrencyNameByAccountId($account_id){
		$model = TableCashPlayer::model()->find(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);
		return $this->getCurrencyNameByCurrencyId($model['currency_id']);
	}
	
	/**
	 * @todo get currency name using currency id
	 * @author leokarl
	 * @since 2012-12-27
	 * @param int $currency_id
	 */
	public function getCurrencyNameByCurrencyId($currency_id){
		$model = TableCurrency::model()->find(array('condition'=>'id=:currency_id',
				'params'=>array(':currency_id' => $currency_id),)
		);
		return $model['currency_name'];
	}
	/**
	 * @todo disallow special characters
	 * @author leokarl
	 * @since 2012-12-08
	 * @param string $mystring
	 * @return boolean
	 */
	public function isSpecialCharPresent($mystring){
		if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
				&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
				&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false && strpos($mystring, '_') === false
				&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '`') === false
				&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
				&& strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
				&& strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){

			return FALSE; // special character not present
		}else{
			return TRUE; // special character present
		}
	}
	
	/**
	 * @todo is cashplayer account_id exist
	 * @author leokarl
	 * @since 2012-12-21
	 * @param string $account_id
	 * @return boolean
	 */
	public function isAccountIdExist($account_id){
		$count = TableCashPlayer::model()->count(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);
		
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo is bonus type valid
	 * @author leokarl
	 * @since 2012-12-21
	 * @param int $type
	 * @return boolean
	 */
	public function isBonusTypeValid($type){
		$count = TableTransactionItem::model()->count(array('condition'=>'id=:item_id and item_type=6',
				'params'=>array(':item_id' => $type),)
		);
		
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo get cashplayer field value
	 * @author leokarl
	 * @since 2012-12-21
	 * @param string $account_id
	 * @param string $field_name
	 */
	public function getCashPlayerFieldValueById($field_name, $account_id){
		$model = TableCashPlayer::model()->find(array('select'=>$field_name,'condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);
		
		return $model[$field_name];
	}
}