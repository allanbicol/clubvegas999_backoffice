<form action="" method="post">
		<div style="width: 950px;">
		<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='email_detail';  
		  				$criteria->condition='email_type=:type and email_desc=:desc and lang_id=:lang';
		  				$criteria->params=array(':type'=>1,':desc'=>Yii::app()->session['signupEmail'],':lang'=>5);
						$dataReader = EmailSetting::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<textarea cols="100" id="editorThai" name="editorThai" rows="20">'.$row['email_detail'].'</textarea>';
						}
		?>
		</div>
		<script>
			
			CKEDITOR.replace( 'editorThai', {
				height:"500",
				on: {
					//focus: onFocus,
					//blur: onBlur,

					// Check for availability of corresponding plugins.
					pluginsLoaded: function( evt ) {
						var doc = CKEDITOR.document, ed = evt.editor;
						if ( !ed.getCommand( 'bold' ) )
							doc.getById( 'exec-bold' ).hide();
						if ( !ed.getCommand( 'link' ) )
							doc.getById( 'exec-link' ).hide();
					}
				}
			});
			var editorTH = CKEDITOR.instances.editorThai;
		</script>
		<div style="background-color: #E6E6E6;padding: 5px 5px 5px 5px; width : 941px;">
		<div id="eButtons">
			<?php 
						$sub_type=1;
						if (Yii::app()->session['signupEmail']=='Email Verification'){
							$sub_type=1;
						}else{
							$sub_type=2;
						}

		  				$criteria=new CDbCriteria;
		  				$criteria->select='variable,variable_desc';  
		  				$criteria->condition='email_type=:type and email_sub_type=:subType';
		  				$criteria->params=array(':type'=>1,':subType'=>$sub_type);
						$dataReader = EmailVariables::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<input class="btn mini red" onclick="InsertText(this.id,\'signup\',\'th\');" id="'.$row['variable'].'" type="button" value="'.$row['variable_desc'].'">';
						}
			?>
		</div>
		</div>
		<br/>
		<br/>
		<input class="btn red" style="width: 100px; height: 40px; font-size: large;" type="button" onclick="saveSetting('1','<?php echo Yii::app()->session['signupEmail'];?>','5',editorTH.getData() );" value="Save">
	</form>