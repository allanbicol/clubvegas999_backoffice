<?php

class InstantPoolsMB02Controller extends MyController
{
	private $svInstantPool;
	
	public function actionInstantPoolsMB02()
	{
		
		$this->svInstantPool = new Constants();
		$homepage = file_get_contents($this->svInstantPool->savanInstantPool);
		
		//echo  $http_response_header;
		$homepage = json_decode($homepage, true);
		
		//BACCARAT MB-02 INSTANT POOL
		echo '<div style="padding:8px 5px;background-color: #333333; color:white;"><b>BACCARAT MB-O2</b> <font color="#85BCD7">Shoe</font> : '.$homepage["params"]["poolList"][2]["shoeNo"].' <font color="#85BCD7">Game</font> : '.$homepage["params"]["poolList"][2]["gameNo"].'</div>';
		echo '<table class="example-table">
		<tbody><th>Account Id</th><th>Banker</th><th>Player</th><th>Tie</th><th>B Pair</th><th>P Pair</th><th>Big</th><th>Small</th><th>Currency</th><th>Win/Loss</th><th>Balance</th><th>IP</th>';

	$result_bet= $homepage["params"]["poolList"][2]["playerInfos"];
		if (Yii::app()->session['account_type']=='agent'){
			if (count($result_bet)>0){
// 				if (Yii::app()->session['account_id']==$account_id){
					$agent_counter=0;
					for ($i=0;$i<=count($result_bet)-1;$i++){
						
						$account_id='';
						switch (Yii::app()->session['level']) {
							case 'SC':
								$account_id	=	substr($result_bet[$i]['userName'],0,2);
								break;
							case 'SMA':
								$account_id	=	substr($result_bet[$i]['userName'],0,4);
								break;
							case 'MA':
								$account_id	=	substr($result_bet[$i]['userName'],0,6);
								break;
							case 'AGT':
								$account_id	=	substr($result_bet[$i]['userName'],0,8);
								break;
						}
						$bets=$result_bet[$i]['bets'];
						if($account_id == Yii::app()->session['account_id'] && strlen($result_bet[$i]['userName'])==10 ){
							echo '<tr>';
							echo '<td>'.$result_bet[$i]['userName'].'</td>';
							echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
							echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
							echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
							echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
							echo '<td>'.$result_bet[$i]['currency'].'</td>';
							echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
							echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
							echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
							echo '</tr>';
							$agent_counter+=1;
						}
					}
					if ($agent_counter!=0){
						echo '</tbody></table>';
					}else{
						echo '</tbody></table>';
						echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
					}
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}
			
		}else{
		
			if (count($result_bet)>0){
				for ($i=0;$i<=count($result_bet)-1;$i++){
					$bets=$result_bet[$i]['bets'];
					echo '<tr>';
					echo '<td>'.$result_bet[$i]['userName'].'</td>';
					echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
					echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
					echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
					echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
					echo '<td>'.$result_bet[$i]['currency'].'</td>';
					echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
					echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
					echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
					echo '</tr>';
						
				}
				echo '</tbody></table>';
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}

		}
		
// 		$homepage = file_get_contents(Constants::Url_MB_02);
// 		$homepage = json_decode($homepage, true);
// 		$filedNames=array("ACCTID","BANK","PLAYER","TIE","BANKPAIR","PLAYPAIR","BIG","SMALL","CURRENCYID","WINLOSS","CREDITQUANTITY","BALANCE","IP");
// 		$result_bet= $homepage["params"]["luzi2"];
// 		$img=explode("#",$result_bet);
// 		//print_r(substr($img[0],0,-2)) ;
// 		echo '<form>';
// 		$i=0;
// 		while($i<=110){
// 			echo '<ul class="col" style="display: block; float: left;padding:3px 3px 3px 3px; border:1px solid #B2B2B2;">';
// 			$x=$i;
// 			$e=0;
// 			while ($e<=5){
// 				if (isset($img[$x])){
// 					$bet=substr($img[$x],0,-2);
// 					if ($bet!=""){
// 						if (Yii::app()->session['chooselanaguage']=="en"){
// 							$image=$bet.'e.GIF';
// 						}else{
// 							$image=$bet.'.GIF';
// 						}
// 					}else{
// 						$image='b_null.gif';
// 					}
// 					echo '<li class="row"  style="padding:.6px .6px .6px .6px; "><img src="'.$this->module->assetsUrl.'/images/baccarat/'.$image.'"</li>';
// 				}else{
// 					echo '<li class="row"  style="padding:.6px .6px.6px .6px; "><img src="'.$this->module->assetsUrl.'/images/baccarat/'.$image.'"</li>';
// 				}
// 				$x++;
// 				$e++;
// 			}
// 			echo '</ul>';
// 			$i+=6;
		
// 		}
// 		echo '</form></br></br></br></br></br></br></br></br></br></br></br>';
// 		echo '<div class="instantPools">';
// 		echo JsonUtil::jsonJqgridInstantPools($homepage["params"]["betDetail2"], 1, 1, 1,$filedNames);		
// 		echo '</div>';	
	}

	
	public function actionChecking()
	{
		echo '1';
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemMonitor.readInstantPools') || User::getUserType() === 'agent'){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
}