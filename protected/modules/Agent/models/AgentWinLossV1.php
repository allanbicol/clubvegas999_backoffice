<?php
/**
 * @todo AgentWinLoss Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-06-01
 */
class AgentWinLossV1
{
	/**
     * @todo getCostaVegasWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
	public function getCostaVegasWinLoss($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$vigSharing=0;
		if($_GET['vigSharing']==''){
			$vigSharing=0;
		}else{
			$vigSharing=$_GET['vigSharing'];
		}
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
				$strQueryWhereForBalance='';
			}else{
				$strQueryWhere="";
				$strQueryWhereForBalance='';
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT 
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency_name,
					sum(a.amount_wagers) as valid_bet,
					avg(a.amount_wagers) as average_bet,
					sum(a.amount_tips) as tips,
					
					(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
					((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
					(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
					(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
					)*-1 as total,
					
					((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					sum(a.amount_tips) as mc_tips,
					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
					
					(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
				    sum(a.amount_tips) as vig_tips,
					((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
					
					(
					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
					((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
					) as pl_total
					
					FROM tbl_costavegas_casino_history a 
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)	
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r
				order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
					#SUB COMPANY WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
					FROM  (
					    SELECT 
					    SUBSTRING(a.account_id,1,4) as account_id,
					    j.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    sum(a.amount_tips) as tips,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
						)*-1 as total,
					
					    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss, #sc_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm, #sc_comm,
						sum(a.amount_tips) as mc_tips,
					    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total, #sc_total
						
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
					    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY SUBSTRING(a.account_id,1,4)
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
					#SENIOR MASTER WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
					FROM  (
					    SELECT 
					    SUBSTRING(a.account_id,1,6) as account_id,
					    j.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    sum(a.amount_tips) as tips,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
						)*-1 as total,
					
					    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss, #sma_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm, #sma_comm,
						sum(a.amount_tips) as mc_tips,
					    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total, #sma_total
					
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
					    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY SUBSTRING(a.account_id,1,6)
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
					#MASTER WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
					FROM  (
					    SELECT 
					    SUBSTRING(a.account_id,1,8) as account_id,
					    j.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    sum(a.amount_tips) as tips,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))  
						)*-1 as total,
					
					    ((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
						sum(a.amount_tips) as mc_tips,
					    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total, #ma_total
					
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					    
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
					    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY SUBSTRING(a.account_id,1,8)
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
					#AGENT WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
					FROM  (
					    SELECT 
					    a.account_id as account_id,
					    b.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    (sum(a.amount_tips))*-1 as tips,
					
					    #sum(a.amount_settlements - a.amount_wagers) as win_loss,
						sum(a.amount_settlements - a.amount_wagers) as win_loss,
					    sum(a.commission) as commission,
					    ((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as total,
					    (SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id 
					    AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as balance,
					
					    (sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,#agt_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm, #agt_comm,
						sum(a.amount_tips) as agt_tips,
					    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total, #agt_total
					
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					
					    #(((sum(a.amount_settlements - a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)))*-1) + (sum((a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)) * (-(CASE WHEN i.game_commission IS NULL THEN 0 ELSE i.game_commission END)/100)))) as pl_total
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
						LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY a.account_id
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasWinLossCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasWinLossCount()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
				$strQueryWhereForBalance='';
			}else{
				$strQueryWhere="";
				$strQueryWhereForBalance='';
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
				    SELECT  SUBSTRING(a.account_id,1,2) as account_id,a.currency_name
				    FROM tbl_costavegas_casino_history a 
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,2)
				)t		
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT 
				    SUBSTRING(a.account_id,1,4) as account_id,
				    j.account_name
				    FROM tbl_costavegas_casino_history a 
				    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,4)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT 
				    SUBSTRING(a.account_id,1,6) as account_id,
				    j.account_name
				    FROM tbl_costavegas_casino_history a 
				    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,6)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT 
				    SUBSTRING(a.account_id,1,8) as account_id,
				    j.account_name
				    
				    FROM tbl_costavegas_casino_history a 
				    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,8)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
				SELECT 
				    a.account_id as account_id,
				    b.account_name
				    FROM tbl_costavegas_casino_history a 
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY a.account_id
				)t
			");
		}
		$rows = $command->query();
		return $rows;
	}
	
	
	/**
	 * @todo getCostaVegasWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasWinLossDetails($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$vigSharing=0;
		if($_GET['vigSharing']==''){
			$vigSharing=0;
		}else{
			$vigSharing=$_GET['vigSharing'];
		}
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
				$strQueryWhereForBalance='';
			}else{
				$strQueryWhere="";
				$strQueryWhereForBalance='';
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}
		//Agent ID
		$strAccountIDWhere='';
		$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
		
		if(trim($_GET['accountID'])==''){
			$strAccountIDWhere='';
		}else{
			if($intAccountIDLength!=10){
				$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
			}else{
				$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
			}
		}
		$pl_total='';
		if(trim($_GET['accountID'])==''){
			$pl_total="(
					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
					((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
					) as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==2){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
			)*-1 as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==4){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
			)*-1 as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==6){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
			)*-1 as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==8){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) 
			)*-1 as pl_total";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT 
				a.bet_date,
				substring(a.account_id,1,$intAccountIDLength) as account_id,
				j.account_name,
				a.game_id,
				a.currency_name,
				sum(a.amount_wagers) as valid_bet,
				avg(a.amount_wagers) as average_bet,
				(sum(a.amount_tips))*-1 as tips,
				sum(a.amount_settlements - a.amount_wagers) as mem_win_loss,
				sum(a.commission) as mem_comm,
				((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as mem_total,
				(SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
				                
				(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
			    sum(a.amount_tips) as agt_tips,
			    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
			    
			    ((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
			    sum(a.amount_tips) as ma_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
			    
			    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
			    sum(a.amount_tips) as sma_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
			    
			    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
			    sum(a.amount_tips) as sc_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
			
			    ((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
			    (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
			    sum(a.amount_tips) as mc_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
			    
				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
			    sum(a.amount_tips) as vig_tips,
				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
				
			    ". $pl_total ."
					
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY substring(a.account_id,1,$intAccountIDLength)
			)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit	
		");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasMemberWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		if($_GET['intGame']!==''){
			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
		}else{
			$addWhere="";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetCostaVegasMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc, t.id desc  LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasMemberWinLossDetailsSummaryFooter
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMemberWinLossDetailsSummaryFooter()
	{
		if($_GET['intGame']!==''){
			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
		}else{
			$addWhere="";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row,
			count(t.win_loss) as bet_count, 
			sum(t.amount_wagers) as amount_wagers, 
			sum(t.win_loss) as win_loss,
			sum(t.commission) as commission,
			sum(t.amount_tips) as amount_tips,
			sum(t.total) as total,
			(Select t.balance from vwGetCostaVegasMemberWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance 
			FROM vwGetCostaVegasMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc");
		$rows = $command->query();
		
		foreach($rows as $row){
			echo $row['bet_count'] . ';';
			echo $row['amount_wagers'] . ';';
			echo $row['win_loss'] . ';';
			echo $row['commission'] . ';';
			echo $row['amount_tips'] . ';';
			echo $row['total'] . ';';
			echo $row['balance'];
		}
	}
	
	/**
	 * @todo getCostaVegasMemberWinLossDetailsCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMemberWinLossDetailsCount()
	{
		if($_GET['intGame']!==''){
			$addWhere="AND game_id='" . $_GET['intGame'] . "'";
		}else{
			$addWhere="";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetCostaVegasMemberWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' ". $addWhere);
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasTotal()
	{
		$vigSharing=0;
		if($_GET['vigSharing']==''){
			$vigSharing=0;
		}else{
			$vigSharing=$_GET['vigSharing'];
		}
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row,
				t.currency_name,t.exchange_rate,sum(t.valid_bet) as valid_bet,sum(t.tips) as tips,
				sum(t.win_loss) as win_loss,sum(t.commission) as commission,sum(t.total) as total,
				sum(t.vig_win_loss) as vig_win_loss,sum(t.vig_tips) as vig_tips,sum(t.vig_total) as vig_total,
				sum(t.pl_total) as pl_total
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,2) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
	
				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as win_loss, #mc_win_loss,
				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission, #mc_comm,
				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as total, #mc_total
				
				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
			    sum(a.amount_tips) as vig_tips,
				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
				
				(
				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
				) as pl_total
				
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY SUBSTRING(a.account_id,1,2)
			)t, (SELECT @rownum := 0) r
			GROUP BY t.currency_name
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasCSTotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,2) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
				
				((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss, #sc_win_loss,
				((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission, #sc_comm,
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as total, #sc_total
				
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
				)*-1 as pl_total
					
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasSMATotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasSMATotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,4) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
							
				((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as total, #sma_total,
    
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
				)*-1 as pl_total
	
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasMATotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMATotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,6) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
				
				((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as total, #ma_total,
    
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			    
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
				)*-1 as pl_total
	
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasAGTTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasAGTTotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,8) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
				
				(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
			    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as total, #agt_total,
    
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) 
				)*-1 as pl_total
	
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	/**
	 * @todo getCostaVegasWinLossResultDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-15
	 */
	public function getCostaVegasWinLossResultDetails(){
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM tbl_costavegas_casino_history WHERE id=" . $_POST['winlossid']);
	}

	
	
	
	/**
	 * @todo getHTVWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getHTVWinLoss($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
	
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as total,
					
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
					
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total
					
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
					)*-1 as total,
					
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
					 
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
				$command = $connection->createCommand("
				#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
					)*-1 as total,
						
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
				");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as total,
					
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,8)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(b.bonus)  as bonus,
					((sum(a.win_loss) + sum(a.commission))) as total,
					(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
					
					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
	    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getHTVWinLossCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getHTVWinLossCount()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (SELECT
					SUBSTRING(a.account_id,1,2) as account_id
					FROM tbl_htv_casino_history a
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id
					
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
			  	)t
			");
    	}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
    				SELECT  COUNT(0)
    				FROM  (
					    SELECT
						SUBSTRING(a.account_id,1,8) as account_id
						    
						FROM tbl_htv_casino_history a
						LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
						WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
						GROUP BY SUBSTRING(a.account_id,1,8)
					)t
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
	    		SELECT  COUNT(0)
	    		FROM  (
					SELECT
					a.account_id as account_id
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
				)t
			");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999Total
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999Total()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as win_loss,
			    (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
			    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as total,
    			
    			(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    			");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999CSTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999CSTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    
    			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
    			((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
    			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as total, #sc_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
				)*-1 as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999SMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999SMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,4) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
    			((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
    			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as total, #sma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
				)*-1 as pl_total
    				
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999MATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999MATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,6) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    
    			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
    			((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as total, #ma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
				)*-1 as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999AGTTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999AGTTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,8) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			(sum(a.win_loss * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
    			((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
    			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as total, #agt_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
				)*-1 as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVWinLossDetails($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
   	 	//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}
    	//Agent ID
    	$strAccountIDWhere='';
    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    
    	if(trim($_GET['accountID'])==''){
    		$strAccountIDWhere='';
    	}else{
    		if($intAccountIDLength!=10){
    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    		}else{
    			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    		}
    	}
    	$pl_total='';
    	if(trim($_GET['accountID'])==''){
    		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    		)*-1 as pl_total";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    			SELECT  @rownum := @rownum + 1 row, t.*
    			FROM  (
	    			SELECT
	    			substring(a.account_id,1,$intAccountIDLength) as account_id,
	    			j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
	    			sum(a.win_loss) as mem_win_loss,
					sum(a.commission) as mem_comm,
					sum(b.bonus)  as mem_bonus,
					((sum(a.win_loss) + sum(a.commission))) as mem_total,
					(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
	    
	    			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
				   
				    ((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
				    ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
				    (((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
				   
				    ((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
				    ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
				    (((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
				    
				    ((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
				    ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
				    
				    ((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
				    (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
				    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
				        
				    ".$pl_total."
				        
	    			FROM tbl_htv_casino_history a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
	    			GROUP BY substring(a.account_id,1,$intAccountIDLength)
	    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetailsCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetailsCount()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetHTVMemberWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetHTVMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc,t.id desc LIMIT $startIndex , $limit");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetailsSummaryFooter
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetailsSummaryFooter()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, 
    			count(t.win_loss) as bet_count,
    			sum(t.bet_amount) as bet_amount,
				sum(t.amount_wagers) as amount_wagers, 
				sum(t.win_loss) as win_loss,
				sum(t.commission) as commission,
				sum(t.total) as total,
				(Select t.balance from vwGetHTVMemberWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
    			FROM vwGetHTVMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
    	$rows = $command->query();
    foreach($rows as $row){
			echo $row['bet_count'] . ';';
			echo $row['bet_amount'] . ';';
			echo $row['amount_wagers'] . ';';
			echo $row['win_loss'] . ';';
			echo $row['commission'] . ';';
			echo $row['total'] . ';';
			echo $row['balance'];
		}
    }
    /**
     * @todo getHTVMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHatienVegasWinLossResultDetails()
    {
		$row = TableHtvCasinoHistory::model()->find(array('select'=>'game_type,currency,bet_result_detail,bet_detail,bet_result','condition'=>'id=:id',
					'params'=>array(':id'=>$_GET['winlossid']),));
    	
		echo $this->getWinLossBettingDetail($row['game_type'], $row['bet_result_detail'], $row['bet_detail'],$row['bet_result'], $row['currency']);
		
    }
    
    
    
    

    
    
    /**
     * @todo getSAVANWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLoss($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    		$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    		$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as total,
					
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
					
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total
					
					FROM tbl_savan_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
				#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
					)*-1 as total,
					
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
					 
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$command = $connection->createCommand("
				#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
					)*-1 as total,
						
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
				");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as total,
					
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,8)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(b.bonus)  as bonus,
					((sum(a.win_loss) + sum(a.commission))) as total,
					(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
					
					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history a
	    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
	    }
	    $rows = $command->query();
	    return $rows;
    }
    
    /**
     * @todo getSAVANWinLossCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLossCount()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    		$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    		$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (SELECT
    				SUBSTRING(a.account_id,1,2) as account_id
    				FROM tbl_savan_casino_history a
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,2)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,4) as account_id						
    				FROM tbl_savan_casino_history a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,4)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,6) as account_id
    				FROM tbl_savan_casino_history a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,6)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,8) as account_id
    				FROM tbl_savan_casino_history a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,8)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				a.account_id as account_id
    				FROM tbl_savan_casino_history a
	    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY a.account_id
    			)t
    		");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as win_loss,
			    (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
			    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as total,
    			
    			(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total
    
    			FROM tbl_savan_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    			");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getSAVANCSTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
    public function getSAVANCSTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    
    			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
    			((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
    			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as total, #sc_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
				)*-1 as pl_total
    
    			FROM tbl_savan_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANSMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANSMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    		$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
	    }else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		    if($_GET['testCurrency']==1){
			    $strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
	    	}else{
	    		$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
	    	}
	    }else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
	    	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
	    }
	    $connection = Yii::app()->db_cv999_fd_master;
	    $command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,4) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
    			((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
    			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as total, #sma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
				)*-1 as pl_total
    				
    			FROM tbl_savan_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,6) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    
    			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
    			((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as total, #ma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
				)*-1 as pl_total
    
    			FROM tbl_savan_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANAGTTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANAGTTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
	    }else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
	    	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
	    }else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		    if($_GET['testCurrency']==1){
		    	$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
		    }else{
		    	$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
		    }
	    }else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
	    	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
	    }
	    $connection = Yii::app()->db_cv999_fd_master;
	    $command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,8) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			(sum(a.win_loss * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
    			((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
    			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as total, #agt_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
				)*-1 as pl_total
    
    			FROM tbl_savan_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLossDetails($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
   	 	//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}
    	//Agent ID
    	$strAccountIDWhere='';
    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    
    	if(trim($_GET['accountID'])==''){
    		$strAccountIDWhere='';
    	}else{
    		if($intAccountIDLength!=10){
    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    		}else{
    			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    		}
    	}
    	$pl_total='';
    	if(trim($_GET['accountID'])==''){
    		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    		)*-1 as pl_total";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    			SELECT  @rownum := @rownum + 1 row, t.*
    			FROM  (
	    			SELECT
	    			substring(a.account_id,1,$intAccountIDLength) as account_id,
	    			j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
	    			sum(a.win_loss) as mem_win_loss,
					sum(a.commission) as mem_comm,
					sum(b.bonus)  as mem_bonus,
					((sum(a.win_loss) + sum(a.commission))) as mem_total,
					(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
	    
	    			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
				   
				    ((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
				    ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
				    (((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
				   
				    ((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
				    ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
				    (((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
				    
				    ((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
				    ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
				    
				    ((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
				    (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
				    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
				        
				    ".$pl_total."
				        
	    			FROM tbl_savan_casino_history a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
	    			GROUP BY substring(a.account_id,1,$intAccountIDLength)
	    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetailsCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetailsCount()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetSAVANVEGASMemberWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
       	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSAVANVEGASMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc,t.id desc LIMIT $startIndex , $limit");
       	$rows = $command->query();
       	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetailsSummaryFooter
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetailsSummaryFooter()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row,
    			count(t.win_loss) as bet_count,
    			sum(t.bet_amount) as bet_amount,
    			sum(t.amount_wagers) as amount_wagers,
    			sum(t.win_loss) as win_loss,
    			sum(t.commission) as commission,
    			sum(t.total) as total,
    			(Select t.balance from vwGetSAVANVEGASMemberWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
    			FROM vwGetSAVANVEGASMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
    	$rows = $command->query();
    	foreach($rows as $row){
    		echo $row['bet_count'] . ';';
    		echo $row['bet_amount'] . ';';
    		echo $row['amount_wagers'] . ';';
    		echo $row['win_loss'] . ';';
    		echo $row['commission'] . ';';
    		echo $row['total'] . ';';
    		echo $row['balance'];
    	}
    }
    
    /**
     * @todo getSavanVegasWinLossResultDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-11-17
     */
    public function getSavanVegasWinLossResultDetails()
    {
    	$row = TableSavanCasinoHistory::model()->find(array('select'=>'game_type,currency,bet_result_detail,bet_detail,bet_result','condition'=>'id=:id',
    			'params'=>array(':id'=>$_GET['winlossid']),));
    	 
    	echo $this->getWinLossBettingDetail($row['game_type'], $row['bet_result_detail'], $row['bet_detail'],$row['bet_result'], $row['currency']);
    }
    
    public function getSavanVegasResultDetails(){
    	$row = TableSavanCasinoHistory::model()->find(array('select'=>'betrslink','condition'=>'id=:id',
    			'params'=>array(':id'=>$_GET['winlossid']),));
    	
    	return $row['betrslink'];
    }
    
    
    
    
    
    /**
     * @todo apply amount incentive if currency is incentive
     * @author leokarl
     * @since 2013-02-23
     */
    public function amountIncentive($currency, $amount){
    	$row = TableCurrency::model()->find(array('select'=>'incentive,incentive_amount','condition'=>'currency_name=:currency',
    			'params'=>array(':currency'=>$currency),));
    	
    	return ($row['incentive'] == 1) ? number_format($amount * $row['incentive_amount'],2,".",",") : number_format($amount,2,".",",");
    }
    
    /**
     * @todo return winloss betting details in html table format
     * @author leokarl
     * @since 2012-12-12
     * @param int $game_type
     * @param array $bet_result_detail_array
     */
    public function getWinLossBettingDetail($game_type, $bet_result_detail_array, $bet_detail_array, $bet_result_array, $currency){
    	
    	 
    	// table header
    	$table_data = '<table border="0" cellspacing="2" cellpadding="0" width="100%">'.
    			'<tr><th class="winlossresultheader">Type</th><th class="winlossresultheader">Bet Amount</th><th class="winlossresultheader">Win/Loss</th></tr>';
    	 
    	if($game_type == 1){ // baccarat
    		
    		/*
    		10.0#10.0#9.5^20.0#0.0#0.0^30.0#0.0#0.0^0.0#0.0#0.0^0.0#0.0#0.0^0.0#0.0#0.0^0.0#0.0#0.0^
    		
    		banker betting amount# banker return current cash amount # banker win amount
    		^
    		Player betting amount # player return current cash amount # player win amount
    		^
    		Tie betting amount # Tie return cash amount # Tie win amount
    		^
    		banker pair betting amount # Banker pair return current cash amount # Banker pair win amount
    		^
    		player pair betting amount # player pair return current cash amount # player pair win amount
    		^
    		Big betting amount # big return current cash amount # big win amount
    		^
    		small betting amount # small return current cash amount # small win amount
    		*/
    		
    		for($i = 0; $i <= 6; $i++) {
    			 
    			/*
    			 * @definition
    			* $bet_detail[0] = bet_amount
    			* $bet_detail[2] = win_loss
    			*/
    			$bet_result_detail = explode("^", $bet_result_detail_array);
    			$bet_detail = split("#",$bet_result_detail[$i]);
    			$bet_result = split("#",$bet_result_array);
    			if($bet_result[0] == 3){ // tie game
    				if($bet_detail[1] > 0){
    					$win_loss = ($bet_detail[2] == 0) ? '0' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
    				}else{
    					$win_loss = ($bet_detail[2] == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $bet_detail[0]) . '</font>' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
    				}
    			}else{
    				$win_loss = ($bet_detail[2] == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $bet_detail[0]) . '</font>' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
    			}
    			
    			 
    			if($bet_detail[0] > 0){
    				// return table row data
    				$table_data .= '<tr><td class="bet_type">' . $this->getBaccaratBetTypeByBetDetailNumber($i) . '</td><td class="bet_amount">' . $this->amountIncentive($currency, $bet_detail[0]) . '</td><td class="win_loss">' . $win_loss . '</td></tr>';
    			}
    		}
    		 
    	}else if($game_type == 4){ // dragon tiger
    		 /*
    		  * \
	          	10.0#0.0#0.0^30.0#30.0#29.1^20.0#0.0#0.0^
	          	
				Dragon betting amount # Dragon return current cash amount # Dragon win amount
			    ^
			    Tiger betting amount # Tiger return current cash amount # Tiger win amount
			    ^
			    Tie betting amount # Tie return current cash amount # Tie win amount
			    ^
    		 */
    		for($i = 0; $i <= 2; $i++) {
    			 
    			/*
    			 * @definition
    			* $bet_detail[0] = bet_amount
    			* $bet_detail[2] = win_loss
    			*/
    			$bet_result_detail = explode("^", $bet_result_detail_array);
    			$bet_detail = split("#",$bet_result_detail[$i]);
    			$win_loss = ($bet_detail[2] == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $bet_detail[0]) . '</font>' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
    			 
    			if($bet_detail[0] > 0){
    				// return table row data
    				$table_data .= '<tr><td class="bet_type">' . $this->getDragonTigerBetTypeByBetDetailNumber($i) . '</td><td class="bet_amount">' . $this->amountIncentive($currency, $bet_detail[0]) . '</td><td class="win_loss">' . $win_loss . '</td></tr>';
    			}
    		}
    		 
    	}else if($game_type == 2){ // roulette
    		
    		/*
    		00#60|08#30|10#40|11#40|12#30|16#30|19#30|20#70|22#30|23#30|30#70|
			^1619#20|
			^010203#30|101112#30|161718#20|192021#20|313233#50|
			^0
			^0
			^0
			^0
			^0
			^0
			^0
			^210
			^210
			^0
			^100
			^0
			^0
			^0
			^0
			^0^
			
			
			00#0.0#0.0|08#0.0#0.0|10#0.0#0.0|11#0.0#0.0|12#0.0#0.0|16#0.0#0.0|19#0.0#0.0|20#0.0#0.0|22#0.0#0.0|23#0.0#0.0|30#0.0#0.0|
			^1619#0.0#0.0|
			^010203#0.0#0.0|101112#0.0#0.0|161718#20.0#220.0|192021#0.0#0.0|313233#0.0#0.0|
			^0#0
			^0#0
			^0#0
			^0.0#0.0|0#0
			^0#0
			^0#0
			^0#0
			^0.0#0.0|
			^210.0#420.0|
			^0#0
			^100.0#100.0|
			^0#0
			^0#0
			^0#0
			^0#0
			^0#0^ 
    		 */
    		
    		//straight bet,	split bet,	triple bet,	conner bet,	line bet,	three number,	four number,
    		//row1,	row2,	row3,	column1,	column2,	column3,	red,	black,	odd,	even,	small,	big
    		
    		// bet_detail
    		list($straight_bet, $split_bet, $triple_bet, $corner_bet, $line_bet, $three_number, $four_number, $row1, $row2, 
    				$row3, $col1, $col2, $col3, $red, $black, $odd, $even, $small, $big) = explode("^",$bet_detail_array);
    		
    		// bet_result_detail
    		list($straight_bet_detail, $split_bet_detail, $triple_bet_detail, $corner_bet_detail, $line_bet_detail, $three_number_detail, $four_number_detail, $row1_detail, $row2_detail,
    				$row3_detail, $col1_detail, $col2_detail, $col3_detail, $red_detail, $black_detail, $odd_detail, $even_detail, $small_detail, $big_detail) = explode("^",$bet_result_detail_array);
    				
    // 1. straight_bet ...
    		$raw_straight_bet = split("\|",$straight_bet); // type#bet_amount | type#bet_amount | ...
    		$raw_straight_bet_detail = split("\|",$straight_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
    		
    		$type = ''; $bet_amount = ''; $winloss = '';
    		
    		for($i=0; $i<count($raw_straight_bet) -1; $i++){
    			list($sb_type, $sb_amount) = explode("#",$raw_straight_bet[$i]); //type # bet_amount
    			list($sb_type_detail, $sb_return, $sb_win) = explode("#",$raw_straight_bet_detail[$i]); // type # return # win
    			$separator = (count($raw_straight_bet)-1 > 1) ? ';'  : '';
    			
    			$type .= $this->getRouletteNumberTypeFromBetResultDetail($sb_type) . $separator .'<br/>';
    			$bet_amount .= $this->amountIncentive($currency, $sb_amount) . $separator .'<br/>';
    			$winloss .= ($sb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $sb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $sb_win) . '</font>'. $separator .'<br/>';
    		}
    		if($type != ''){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
    		}
     		
     		
     // 2. split bet ...
     		$raw_split_bet = split("\|",$split_bet); // type#bet_amount | type#bet_amount | ...
     		$raw_split_bet_detail = split("\|",$split_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
     		
     		$type = ''; $bet_amount = ''; $winloss = '';
     		
     		for($i=0; $i<count($raw_split_bet) -1; $i++){
     			list($sp_type, $sp_amount) = explode("#",$raw_split_bet[$i]); //type # bet_amount
     			list($sp_type_detail, $sp_return, $sp_win) = explode("#",$raw_split_bet_detail[$i]); // type # return # win
     			$separator = (count($raw_split_bet)-1 > 1) ? ';'  : '';
     			
     			$type .= $this->getRouletteNumberTypeFromBetResultDetail($sp_type) . $separator .'<br/>';
     			$bet_amount .= $this->amountIncentive($currency, $sp_amount) . $separator .'<br/>';
     			$winloss .= ($sp_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $sp_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $sp_win) . '</font>'. $separator .'<br/>';
     		}
     		if($type != ''){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
    		
    		// ... split bet
    		
     // 3. triple bet ...
     		$raw_triple_bet = split("\|",$triple_bet); // type#bet_amount | type#bet_amount | ...
     		$raw_triple_bet_detail = split("\|",$triple_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
     		 
     		$type = ''; $bet_amount = ''; $winloss = '';
     		 
     		for($i=0; $i<count($raw_triple_bet) -1; $i++){
     			list($tb_type, $tb_amount) = explode("#",$raw_triple_bet[$i]); //type # bet_amount
     			list($tb_type_detail, $tb_return, $tb_win) = explode("#",$raw_triple_bet_detail[$i]); // type # return # win
     			$separator = (count($raw_triple_bet)-1 > 1) ? ';'  : '';
     			
     			$type .= $this->getRouletteNumberTypeFromBetResultDetail($tb_type) . $separator . '<br/>';
     			$bet_amount .= $this->amountIncentive($currency, $tb_amount) . $separator .'<br/>';
     			$winloss .= ($tb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $tb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $tb_win) . '</font>'. $separator .'<br/>';
     		}
     		
     		if($type != ''){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     		// ... triple bet
     		
     // 4. corner bet ...
     		$raw_corner_bet = split("\|",$corner_bet); // type#bet_amount | type#bet_amount | ...
     		$raw_corner_bet_detail = split("\|",$corner_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
     		
     		$type = ''; $bet_amount = ''; $winloss = '';
     		
     		for($i=0; $i<count($raw_corner_bet) -1; $i++){
     			list($cb_type, $cb_amount) = explode("#",$raw_corner_bet[$i]); //type # bet_amount
     			list($cb_type_detail, $cb_return, $cb_win) = explode("#",$raw_corner_bet_detail[$i]); // type # return # win
     			$separator = (count($raw_corner_bet)-1 > 1) ? ';'  : '';
     			
     			$type .= $this->getRouletteNumberTypeFromBetResultDetail($cb_type) . $separator .'<br/>';
     			$bet_amount .= $this->amountIncentive($currency, $cb_amount) . $separator . '<br/>';
     			$winloss .= ($cb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $cb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $cb_win) . '</font>'. $separator .'<br/>';
     		}
     		
     		if($type != ''){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		 
     		// ... corner bet
     		
     // 5. line bet ...
     		$raw_line_bet = split("\|",$line_bet); // type#bet_amount | type#bet_amount | ...
     		$raw_line_bet_detail = split("\|",$line_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
     		 
     		$type = ''; $bet_amount = ''; $winloss = '';
     		 
     		for($i=0; $i<count($raw_line_bet) -1; $i++){
     			list($lb_type, $lb_amount) = explode("#",$raw_line_bet[$i]); //type # bet_amount
     			list($lb_type_detail, $lb_return, $lb_win) = explode("#",$raw_line_bet_detail[$i]); // type # return # win
     			$separator = (count($raw_line_bet)-1 > 1) ? ';'  : '';
     			
     			$type .= $this->getRouletteNumberTypeFromBetResultDetail($lb_type) . $separator .'<br/>';
     			$bet_amount .= $this->amountIncentive($currency, $lb_amount) . $separator .'<br/>';
     			$winloss .= ($lb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $lb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $lb_win) . '</font>'. $separator .'<br/>';
     		}
     		
     		if($type != ''){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     		// ... line bet
     		
     // 6. three number ...
     		$raw_three_num = split("\|",$three_number); // type#bet_amount | type#bet_amount | ...
     		$raw_three_num_detail = split("\|",$three_number_detail); // type#return#bet_amount | type#return#bet_amount | ...
     		
     		$type = ''; $bet_amount = ''; $winloss = '';
     		
     		for($i=0; $i<count($raw_three_num) -1; $i++){
     			list($three_type, $three_amount) = explode("#",$raw_three_num[$i]); //type # bet_amount
     			list($three_type_detail, $three_return, $three_win) = explode("#",$raw_three_num_detail[$i]); // type # return # win
     			$separator = (count($raw_three_num)-1 > 1) ? ';'  : '';
     			
     			$type .= $this->getRouletteNumberTypeFromBetResultDetail($three_type) . $separator .'<br/>';
     			$bet_amount .= $this->amountIncentive($currency, $three_amount) . $separator .'<br/>';
     			$winloss .= ($three_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $three_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $three_win) . '</font>'. $separator .'<br/>';
     		}
     		if($type != ''){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		 
     		// ... three number
     		
     // 7. four number
     		//$raw_four_num = split("\|",$four_number); // bet_amount | bet_amount | ...
     		$raw_four_num = $four_number;
     		$raw_four_num_detail = split("\|",$four_number_detail); 
    		list($fb_bet, $fb_win) = explode("#",$raw_four_num_detail[0]);
     		$type = 'Four Number'; 
     		$bet_amount = $this->amountIncentive($currency, $four_number); 
     		$winloss = ($fb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $four_number) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $fb_win) . '</font><br/>';
     		
     		if($four_number > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 8. Row One
     		$raw_row1_detail = split("\|",$row1_detail); 
     		list($r1_bet, $r1_win) = explode("#",$raw_row1_detail[0]);
     		$type = 'Row One';
     		$bet_amount = $this->amountIncentive($currency, $row1);
     		$winloss = ($r1_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $row1) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $r1_win) . '</font><br/>';
     		if($row1 > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 9. Row Two
     		$raw_row2_detail = split("\|",$row2_detail); 
     		list($r2_bet, $r2_win) = explode("#",$raw_row2_detail[0]);
     		$type = 'Row Two';
     		$bet_amount = $this->amountIncentive($currency, $row2);
     		$winloss = ($r2_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $row2) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $r2_win) . '</font><br/>';
     		if($row2 > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 10. Row Three
     		$raw_row3_detail = split("\|",$row3_detail); 
     		list($r3_bet, $r3_win) = explode("#",$raw_row3_detail[0]);
     		$type = 'Row Three';
     		$bet_amount = $this->amountIncentive($currency, $row3);
     		$winloss = ($r3_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $row3) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $r3_win) . '</font><br/>';
     		 
     		if($row3 > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 11. Column One
     		$raw_col1_detail = split("\|",$col1_detail); 
     		list($c1_bet, $c1_win) = explode("#",$raw_col1_detail[0]);
     		$type = 'Column One';
     		$bet_amount = $this->amountIncentive($currency, $col1);
     		$winloss = ($c1_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $col1) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $c1_win) . '</font><br/>';
     		if($col1 > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     
     // 12. Column Two
     		$raw_col2_detail = split("\|",$col2_detail);
     		list($c2_bet, $c2_win) = explode("#",$raw_col2_detail[0]);
     		$type = 'Column Two';
     		$bet_amount = $this->amountIncentive($currency, $col2);
     		$winloss = ($c2_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $col2) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $c2_win) . '</font><br/>';
     		if($col2 > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 13. Column Three
     		$raw_col3_detail = split("\|",$col3_detail);
     		list($c3_bet, $c3_win) = explode("#",$raw_col3_detail[0]);
     		$type = 'Column Three';
     		$bet_amount = $this->amountIncentive($currency, $col3);
     		$winloss = ($c3_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $col3) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $c3_win) . '</font><br/>';
     		if($col3 > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 14. Red 
     		$raw_red_detail = split("\|",$red_detail);
     		list($red_bet, $red_win) = explode("#",$raw_red_detail[0]);
     		$type = 'Red';
     		$bet_amount = $this->amountIncentive($currency, $red);
     		$winloss = ($red_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $red) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $red_win) . '</font><br/>';
     		if($red > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     
     // 15. Black
     		$raw_black_detail = split("\|",$black_detail);
     		list($black_bet, $black_win) = explode("#",$raw_black_detail[0]);
     		$type = 'Black';
     		$bet_amount = $this->amountIncentive($currency, $black);
     		$winloss = ($black_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $black) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $black_win) . '</font><br/>';
     		if($black > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 16. Odd 
     		$raw_odd_detail = split("\|",$odd_detail);
     		list($odd_bet, $odd_win) = explode("#",$raw_odd_detail[0]);
     		$type = 'Odd';
     		$bet_amount = $this->amountIncentive($currency, $odd);
     		$winloss = ($odd_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $odd) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $odd_win) . '</font><br/>';
     		if($odd > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
     // 17. Even
     		$raw_even_detail = split("\|",$even_detail);
     		list($even_bet, $even_win) = explode("#",$raw_even_detail[0]);
     		$type = 'Even';
     		$bet_amount = $this->amountIncentive($currency, $even);
     		$winloss = ($even_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $even) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $even_win) . '</font><br/>';
     		if($even > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     
	// 18. Small 
     		$raw_small_detail = split("\|",$small_detail);
     		list($small_bet, $small_win) = explode("#",$raw_small_detail[0]);
     		$type = 'Small';
     		$bet_amount = $this->amountIncentive($currency, $small);
     		$winloss = ($small_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $small) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $small_win) . '</font><br/>';
     		if($small > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
     		
    // 19. Big
     		$raw_big_detail = split("\|",$big_detail);
     		list($big_bet, $big_win) = explode("#",$raw_big_detail[0]);
     		$type = 'Big';
     		$bet_amount = $this->amountIncentive($currency, $big);
     		$winloss = ($big_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $big) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $big_win) . '</font><br/>';
     		
     		if($big > 0){
     			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
     		}
    		
//     		$bet_detail = split("\|",$bet_detail_array);
    		
//     		for($i = 0; $i <= count($bet_detail) - 1; $i++) {
//     			if(strpos($bet_detail[$i],'#') !== false){
//     				// initialize
// 	    			$data_row = split("#",$bet_detail[$i]);
// 	    			$type = $this->getRouletteNumberTypeFromBetResultDetail($data_row[0]);
// 	    			$bet_amount = $data_row[1];
// 	    			$winloss = ($this->getRouletteWinLossFromBetResultDetail($bet_result_detail_array, $i) == 0) ? '<font color="red">-' . $bet_amount . '</font>' : '<font color="#000c75">' . $this->getRouletteWinLossFromBetResultDetail($bet_result_detail_array, $i) . '</font>';
	    			
// 	    			// return table row data
// 	    			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
//     			}else{
//     				//if($i > 0){
// 	    				$data_row = split("\^",$bet_detail[$i]);
// 	    				for($e = 0; $e <= count($data_row) - 1; $e++){
// 	    					if($data_row[$e] != 0){
// 	    						$type = $this->getRouletteTypeFromBetResultDetail($e);
// 	    						$bet_amount = $data_row[$e];
// 	    						$winloss = ($this->getRouletteWinLossFromBetResultDetail1($bet_result_detail_array, $i, $e) == 0) ? '<font color="red">-' . $bet_amount . '</font>' : '<font color="#000c75">' . $this->getRouletteWinLossFromBetResultDetail1($bet_result_detail_array, $i, $e) . '</font>';
	    						
// 	    						// return table row data
// 	    						$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
// 	    					}
// 	    				}
// //     				}else{
    					
// //     				}
//     			}
//     		}
    		
    		
    	}
    	
    	$table_data .= '</table>';
    	return  $table_data;
    }
    
    /**
     * @todo extract raw bet_type and return formatted bet_type
     * @author leokarl
     * @since 2012-12-13
     * @param string $raw_type
     */
    public function getRouletteTypeFromBetResultDetail($row_number){
    	/*
    	 four number(00010203)bet^
	     line bet 1 ^ Line bet 2 ^ line bet 3 ^ Dozen 1 ^ Dozen 2 ^ Dozen 3
		 ^red^black^odd^even^small^big^
    	 */
    	//^0^0^0^0^0^0^0^210^210^0^100^0^0^0^0^0^
    	//straight bet,	split bet,	triple bet,	conner bet,	line bet,	three number,	four number,	
    	//row1,	row2,	row3,	column1,	column2,	column3,	red,	black,	odd,	even,	small,	big
    	
    	switch ($row_number -1)
    	{
    		case 0:
    			return 'Corner Bet'; break;
    		case 1:
    			return 'Line Bet'; break;
    		case 2:
    			return 'Three Number'; break;
    		case 3:
    			return 'Four Number'; break;
    		case 4:
    			return 'Row One'; break;
    		case 5:
    			return 'Row Two'; break;
    		case 6:
    			return 'Row Three'; break;
    		case 7:
    			return 'Column One'; break;
    		case 8:
    			return 'Column Two'; break;
    		case 9:
    			return 'Column Three'; break;
    		case 10:
    			return 'Red'; break;
    		case 11:
    			return 'Black'; break;
    		case 12:
    			return 'Odd'; break;
    		case 13:
    			return 'Even'; break;
    		case 14:
    			return 'Small'; break;
    		case 15:
    			return 'Big'; break;
    		default:
    			return "Undefined";
    	}
    }
    
    /**
     * @todo extract raw bet_type and return formatted number bet_type
     * @author leokarl
     * @since 2012-12-13
     * @param string $raw_type
     */
    public function getRouletteNumberTypeFromBetResultDetail($raw_type){
    	// 0^0^112233 = 11&22&33
    	$extract_type = split("\^",$raw_type);
    	$type = $extract_type[count($extract_type)-1];
    	$separator_number = 2;
    	
    	$loop_until = (strpos($raw_type,'^') === false) ? count($extract_type)-1 : count($extract_type)-2;
    	
    	for($i = 0; $i <= (strlen($type)/2)/2; $i++){
    		$type = substr_replace($type,'&',$separator_number,0);
    		$separator_number += 3;
    	}
    	$type = (substr_compare($type,"&",-1,1) == 0) ? substr($type,0,strlen($type) -1) : $type;
    	$type = str_replace('&', '<font color="#c0d2fa">&</font>', $type);
    	
    	return (strlen($raw_type) <= 2) ? $raw_type : $type;
    }
    
    /**
     * @todo return winloss from bet_result_detail
     * @author leokarl
     * @since 2012-12-13
     * @param array $bet_result_detail_array
     * @param int $row_count
     */
    public function getRouletteWinLossFromBetResultDetail($bet_result_detail_array, $row_count){
    	$bet_detail = split("\|",$bet_result_detail_array);
    	$row_detail = split("#",$bet_detail[$row_count]);
    	//$winloss = (count($row_detail) == 4) ? $row_detail[3] : $row_detail[2];
    	
    	// return winloss
    	return $row_detail[count($row_detail)-1];
    }
    public function getRouletteWinLossFromBetResultDetail1($bet_result_detail_array, $row_count, $line_number){
    	$bet_detail = split("\|",$bet_result_detail_array);
    	//$row_detail = split("#",$bet_detail[$row_count]);
    	//$winloss = (count($row_detail) == 4) ? $row_detail[3] : $row_detail[2];
    	$last = '';
    	for($i=$row_count; $i < count($bet_detail); $i++){
    		$last .= $bet_detail[$i];
    	}
    	$row_detail = split("\^",$last);
    	$final = split("#",$row_detail[$line_number]);
    	
    	// return winloss
    	//return $row_detail[count($row_detail)-1];
    	return $final[count($final) -1];
    }
    
    /**
     * @todo get baccarat bet type
     * @author leokarl
     * @since 2012-12-12
     * @param int $bet_detail_number
     */
    public function getBaccaratBetTypeByBetDetailNumber($bet_detail_number){
    	
    	switch ($bet_detail_number)
    	{
    		case 0:
    			return '<font color="red">Banker</font>'; break;
    		case 1:
    			return '<font color="blue">Player</font>'; break;
    		case 2:
    			return '<font color="green">Tie</font>'; break;
    		case 3:
    			return '<font color="red">Banker pair</font>'; break;
    		case 4:
    			return '<font color="blue">Player pair</font>'; break;
    		case 5:
    			return '<font color="red">Big</font>'; break;
    		case 6:
    			return '<font color="blue">Small</font>'; break;
    		default:
    			return "Undefined";
    	}
    }
    
    
    /**
     * @todo get dragon tiger bet type
     * @author leokarl
     * @since 2012-12-12
     * @param int $bet_detail_number
     */
    public function getDragonTigerBetTypeByBetDetailNumber($bet_detail_number){
    	if($bet_detail_number == 0){
    		return '<font color="red">Dragon</font>';
    	}
    	elseif($bet_detail_number == 1){
    		return '<font color="blue">Tiger</font>';
    	}
    	elseif($bet_detail_number == 2){
    		return '<font color="green">Tie</font>';
    	}
    }
    
    /**
     * @todo disallow special characters
     * @author leokarl
     * @since 2012-12-08
     * @param string $mystring
     */
    public function isSpecialCharPresent($mystring){
    	if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
    			&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
    			&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false && strpos($mystring, '_') === false
    			&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '-') === false && strpos($mystring, '`') === false
    			&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
    			&& strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
    			&& strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){
    
    		return FALSE; // special character not present
    	}else{
    		return TRUE; // special character present
    	}
    }
    
    //==========================================EXPORT ALL TO EXCEL==================================================
    
    public function getExportCostaVegasWinLoss()
    {

    	$vigSharing=0;
    	if($_GET['vigSharing']==''){
    		$vigSharing=0;
    	}else{
    		$vigSharing=$_GET['vigSharing'];
    	}
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    			$strQueryWhereForBalance='';
    		}else{
    			$strQueryWhere="";
    			$strQueryWhereForBalance='';
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
    		}
    		$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
    		$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
    				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    				FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,2) as account_id,
    				j.account_name,
    				a.currency_name,
    				sum(a.amount_wagers) as valid_bet,
    				avg(a.amount_wagers) as average_bet,
    				sum(a.amount_tips) as tips,
    					
    				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as total,
    					
    				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
    				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.amount_tips) as mc_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
    					
    				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
    				sum(a.amount_tips) as vig_tips,
    				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
    					
    				(
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
    				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
    		) as pl_total
    					
    				FROM tbl_costavegas_casino_history a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,2)
    		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r
    				order by t.currency_name asc,t.account_id asc 
    				");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    	$command = $connection->createCommand("
    			#SUB COMPANY WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,4) as account_id,
    			j.account_name,
    			a.currency_name,
    			sum(a.amount_wagers) as valid_bet,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_tips) as tips,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    	)*-1 as total,
    				
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss, #sc_win_loss,
    			((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm, #sc_comm,
    			sum(a.amount_tips) as mc_tips,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total, #sc_total
    
    			0 as vig_win_loss,
    			0 as vig_tips,
    			0 as vig_total,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    	)*-1 as pl_total
    				
    			FROM tbl_costavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,4)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    			");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    	$command = $connection->createCommand("
    	#SENIOR MASTER WINLOSS
    	SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    	FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,6) as account_id,
    			j.account_name,
    			a.currency_name,
    			sum(a.amount_wagers) as valid_bet,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_tips) as tips,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    			)*-1 as total,
    				
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss, #sma_win_loss,
    			((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm, #sma_comm,
    			sum(a.amount_tips) as mc_tips,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total, #sma_total
    				
    			0 as vig_win_loss,
    			0 as vig_tips,
    			0 as vig_total,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    			)*-1 as pl_total
    				
    			FROM tbl_costavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,6)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    			");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    	$command = $connection->createCommand("
    	#MASTER WINLOSS
    	SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    	FROM  (
    	SELECT
    	SUBSTRING(a.account_id,1,8) as account_id,
    	j.account_name,
    	a.currency_name,
    	sum(a.amount_wagers) as valid_bet,
    	avg(a.amount_wagers) as average_bet,
    	sum(a.amount_tips) as tips,
    		
    	(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    	)*-1 as total,
    				
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
    			((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
    			sum(a.amount_tips) as mc_tips,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total, #ma_total
    				
    			0 as vig_win_loss,
    			0 as vig_tips,
    			0 as vig_total,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    	)*-1 as pl_total
    				
    			FROM tbl_costavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,8)
    	)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    			");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    			$command = $connection->createCommand("
    			#AGENT WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    			FROM  (
    			SELECT
    			a.account_id as account_id,
    			b.account_name,
    			a.currency_name,
    			sum(a.amount_wagers) as valid_bet,
    			avg(a.amount_wagers) as average_bet,
    			(sum(a.amount_tips))*-1 as tips,
    				
    			#sum(a.amount_settlements - a.amount_wagers) as win_loss,
    			sum(a.amount_settlements - a.amount_wagers) as win_loss,
    			sum(a.commission) as commission,
    			((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as total,
    					(SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id
    					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as balance,
    						
    					(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,#agt_win_loss,
    					((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm, #agt_comm,
    					sum(a.amount_tips) as agt_tips,
    					((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total, #agt_total
    								
    							0 as vig_win_loss,
    							0 as vig_tips,
    							0 as vig_total,
    								
    							#(((sum(a.amount_settlements - a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)))*-1) + (sum((a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)) * (-(CASE WHEN i.game_commission IS NULL THEN 0 ELSE i.game_commission END)/100)))) as pl_total
    							(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    							((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    					)*-1 as pl_total
    								
    							FROM tbl_costavegas_casino_history a
    							LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    							WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    							GROUP BY a.account_id
    					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    							");
    	}
    							$rows = $command->query();
    							return $rows;
    	}
    	
    	public function getExportCostaVegasWinLossDetails()
    	{
 
    		$vigSharing=0;
    		if($_GET['vigSharing']==''){
    			$vigSharing=0;
    		}else{
    			$vigSharing=$_GET['vigSharing'];
    		}
    		//additional parameter
    		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    			if($_GET['testCurrency']==1){
    				$strQueryWhere=" AND a.currency_name!='TEST'";
    				$strQueryWhereForBalance='';
    			}else{
    				$strQueryWhere="";
    				$strQueryWhereForBalance='';
    			}
    		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    			$strQueryWhereForBalance='';
    		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    			if($_GET['testCurrency']==1){
    				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
    			}else{
    				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
    			}
    			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
    			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    		}
    		//Agent ID
    		$strAccountIDWhere='';
    		$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    	
    		if(trim($_GET['accountID'])==''){
    			$strAccountIDWhere='';
    		}else{
    			if($intAccountIDLength!=10){
    				$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    			}else{
    				$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    			}
    		}
    		$pl_total='';
    		if(trim($_GET['accountID'])==''){
    			$pl_total="(
    			(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
    			((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
    			) as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==2){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    			)*-1 as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==4){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    			)*-1 as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==6){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    			)*-1 as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==8){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    			)*-1 as pl_total";
    		}
    		$connection = Yii::app()->db_cv999_fd_master;
    		$command = $connection->createCommand("
    				SELECT  @rownum := @rownum + 1 row, t.*
    				FROM  (
    				SELECT
    				a.bet_date,
    				substring(a.account_id,1,$intAccountIDLength) as account_id,
    				j.account_name,
    				a.game_id,
    				a.currency_name,
    				sum(a.amount_wagers) as valid_bet,
    				avg(a.amount_wagers) as average_bet,
    				(sum(a.amount_tips))*-1 as tips,
    				sum(a.amount_settlements - a.amount_wagers) as mem_win_loss,
    				sum(a.commission) as mem_comm,
    				((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as mem_total,
    				(SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id
    				AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
    	
    				(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
    				((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				sum(a.amount_tips) as agt_tips,
    				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
    				 
    				((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
    				((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
    				sum(a.amount_tips) as ma_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
    				 
    				((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
    				((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
    				sum(a.amount_tips) as sma_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
    				 
    				((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
    				((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
    				sum(a.amount_tips) as sc_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
    					
    				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
    				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.amount_tips) as mc_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
    				 
    				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
    				sum(a.amount_tips) as vig_tips,
    				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
    	
    				". $pl_total ."
    					
    				FROM tbl_costavegas_casino_history a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY substring(a.account_id,1,$intAccountIDLength)
    		)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    		");
    		$rows = $command->query();
    		return $rows;
    	}
    	
    	public function getExportCostaVegasMemberWinLossDetails()
    	{
    		if($_GET['intGame']!==''){
    			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    		}else{
    			$addWhere="";
    		}
    		$connection = Yii::app()->db_cv999_fd_master;
    		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetCostaVegasMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc, t.id desc ");
    		$rows = $command->query();
    		return $rows;
    	}
    	
    	public function getExportSAVANWinLoss()
    	{
    		//additional parameter
    		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    			if($_GET['testCurrency']==1){
    				$strQueryWhere=" AND a.currency!='TEST'";
    			}else{
    				$strQueryWhere="";
    			}
    			$strQueryWhereForBalance='';
    		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    			$strQueryWhereForBalance='';
    		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    			if($_GET['testCurrency']==1){
    				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    			}else{
    				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    			}
    			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
    		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
    		}
    	
    		$connection = Yii::app()->db_cv999_fd_master;
    		if(trim($_GET['accountID'])==''){
    			$command = $connection->createCommand("
    					SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    					FROM  (
    					SELECT
    					SUBSTRING(a.account_id,1,2) as account_id,
    					j.account_name,
    					a.currency as currency_name,
    					count(a.account_id) as bet_count,
    					avg(a.available_bet) as average_bet,
    					sum(a.bet_amount) as total_stake,
    					sum(a.available_bet) as valid_bet,
    					sum(0) as tips,
    						
    					(((sum(a.win_loss) + sum(a.commission))) +
    					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    			)*-1 as total,
    						
    					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
    					(sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
    						
    					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total
    						
    					FROM tbl_savan_casino_history a
    					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    					GROUP BY SUBSTRING(a.account_id,1,2)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    					");
    		}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
    				#SUB COMPANY WINLOSS
    				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    				FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,4) as account_id,
    				j.account_name,
    				a.currency as currency_name,
    				count(a.account_id) as bet_count,
    				avg(a.available_bet) as average_bet,
    				sum(a.bet_amount) as total_stake,
    				sum(a.available_bet) as valid_bet,
    				sum(0) as tips,
    					
    				(((sum(a.win_loss) + sum(a.commission))) +
    				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    		)*-1 as total,
    					
    				((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
    				((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
    				(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
    	
    				(((sum(a.win_loss) + sum(a.commission))) +
    				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    				(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as pl_total
    					
    				FROM tbl_savan_casino_history a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,4)
    		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    				");
    		}elseif(strlen(trim($_GET['accountID']))==4){
    				$command = $connection->createCommand("
    						#SENIOR MASTER WINLOSS
    						SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    						FROM  (
    								SELECT
    								SUBSTRING(a.account_id,1,6) as account_id,
    								j.account_name,
    								a.currency as currency_name,
    								count(a.account_id) as bet_count,
    								avg(a.available_bet) as average_bet,
    								sum(a.bet_amount) as total_stake,
    								sum(a.available_bet) as valid_bet,
    								sum(0) as tips,
    									
    								(((sum(a.win_loss) + sum(a.commission))) +
    								((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    								(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    						)*-1 as total,
    	
    								((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
    								((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
    								(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
    									
    								(((sum(a.win_loss) + sum(a.commission))) +
    								((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    								(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    								(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    						)*-1 as pl_total
    									
    								FROM tbl_savan_casino_history a
    								LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    								WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    								GROUP BY SUBSTRING(a.account_id,1,6)
    								)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    								");
    		}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
    				#MASTER WINLOSS
    				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    				FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,8) as account_id,
    				j.account_name,
    				a.currency as currency_name,
    				count(a.account_id) as bet_count,
    				avg(a.available_bet) as average_bet,
    				sum(a.bet_amount) as total_stake,
    				sum(a.available_bet) as valid_bet,
    				sum(0) as tips,
    					
    				(((sum(a.win_loss) + sum(a.commission))) +
    				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    				)*-1 as total,
    					
    				((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
    				((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
    				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
    					
    				(((sum(a.win_loss) + sum(a.commission))) +
    				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    				)*-1 as pl_total
    					
    				FROM tbl_savan_casino_history a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,8)
    				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    						");
    		}elseif(strlen(trim($_GET['accountID']))==8){
    						$command = $connection->createCommand("
    						#MASTER WINLOSS
    						SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    						FROM  (
    						SELECT
    						a.account_id as account_id,
    						b.account_name,
    						a.currency as currency_name,
    						count(a.account_id) as bet_count,
    						avg(a.available_bet) as average_bet,
    						sum(a.bet_amount) as total_stake,
    						sum(a.available_bet) as valid_bet,
    						sum(0) as tips,
    							
    						sum(a.win_loss) as win_loss,
    						sum(a.commission) as commission,
    						sum(b.bonus)  as bonus,
    						((sum(a.win_loss) + sum(a.commission))) as total,
    						(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id
    						AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
    							
    						(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
    						((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    								((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
    									
    								(((sum(a.win_loss) + sum(a.commission))) +
    								((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    						)*-1 as pl_total
    									
    								FROM tbl_savan_casino_history a
    								LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    								WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    								GROUP BY a.account_id
    						)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    						");
    		    }
    		    $rows = $command->query();
    		    return $rows;
     }
     
     public function getExportSAVANWinLossDetails()
     {
     	//additional parameter
     	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     		if($_GET['testCurrency']==1){
     			$strQueryWhere=" AND a.currency!='TEST'";
     		}else{
     			$strQueryWhere="";
     		}
     		$strQueryWhereForBalance='';
     	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     		$strQueryWhereForBalance='';
     	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     		if($_GET['testCurrency']==1){
     			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
     		}else{
     			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
     		}
     		$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
     	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
     		$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
     	}
     	//Agent ID
     	$strAccountIDWhere='';
     	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
     
     	if(trim($_GET['accountID'])==''){
     		$strAccountIDWhere='';
     	}else{
     		if($intAccountIDLength!=10){
     			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
     		}else{
     			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
     		}
     	}
     	$pl_total='';
     	if(trim($_GET['accountID'])==''){
     		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total";
     	}elseif(strlen(trim($_GET['accountID']))==2){
     		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
     		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
     		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
     		)*-1 as pl_total";
     	}elseif(strlen(trim($_GET['accountID']))==4){
     		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
     		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
     		)*-1 as pl_total";
     	}elseif(strlen(trim($_GET['accountID']))==6){
     		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
     		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
     		)*-1 as pl_total";
     	}elseif(strlen(trim($_GET['accountID']))==8){
     		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
     		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
     		)*-1 as pl_total";
     	}
     	$connection = Yii::app()->db_cv999_fd_master;
     	$command = $connection->createCommand("
     			SELECT  @rownum := @rownum + 1 row, t.*
     			FROM  (
     			SELECT
     			substring(a.account_id,1,$intAccountIDLength) as account_id,
     			j.account_name,
     			a.currency as currency_name,
     			count(a.account_id) as bet_count,
     			avg(a.available_bet) as average_bet,
     			sum(a.bet_amount) as total_stake,
     			sum(a.available_bet) as valid_bet,
     			sum(0) as tips,
     			sum(a.win_loss) as mem_win_loss,
     			sum(a.commission) as mem_comm,
     			sum(b.bonus)  as mem_bonus,
     			((sum(a.win_loss) + sum(a.commission))) as mem_total,
     			(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id
     			AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     			 
     			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
     			((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
     				
     			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
     			((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
     				
     			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
     			((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
     
     			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
     			((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
     			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
     
     			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
     			(sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
     			(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
     
     			".$pl_total."
     
     			FROM tbl_savan_casino_history a
     			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
     			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY substring(a.account_id,1,$intAccountIDLength)
     			)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     			");
     			$rows = $command->query();
     			return $rows;
     }
     
     public function getExportSAVANMemberWinLossDetails()
     {
     	if($_GET['intGame']!==''){
     		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
     	}else{
     		$addWhere="";
     	}
     	$connection = Yii::app()->db_cv999_fd_master;
     	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSAVANVEGASMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc,t.id desc ");
     	$rows = $command->query();
     	return $rows;
     }
}