<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery.js"></script>

	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/ui/jquery.ui.widget.js"></script>
	
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
    <title>BE CodeEric</title>
	<style>
		ul#sliding-navigation li.sliding-element h2
			{background:#b1b1b1 url('<?php echo Yii::app()->request->baseUrl;?>/images/heading_bg.jpg') repeat-x;}
		ul#sliding-navigation li.sliding-element a
			{background:#D5D5D5}
		h2 	
			{background:url("<?php echo Yii::app()->request->baseUrl;?>/images/navigation.jpg") no-repeat;}
	</style>
    <script type="text/javascript">
    function myFunction()
    {
        if (document.getElementById('content').offsetHeight > 920){
    		document.getElementById('navmenu').style.height=(document.getElementById('content').offsetHeight + 8) + 'px';
    		//alert(document.getElementById('content').offsetHeight + ":" +document.getElementById('footer').offsetTop);
        }else{
            
        }
    }
   
    </script>    
</head>

<body onbeforeunload="getMonitorHeightWithNav();">
<div id="container">

    <div id="header"> 
            <div id="logo">
                <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentInformation"><img height="80" width="185" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo1.png" /></a>
            </div>
            <div id="userporfilebuttom">
            <form id="loginform" name="loginform" method="post" action="">
			    <input type="hidden" value="en" name="chooselan" id="chooselan" />
			    <div id="lang">
			          <a onclick="document.getElementById('chooselan').value='en';document.getElementById('loginform').submit();" href="#"><img height="25" width="25" title="English" alt="English" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/english.png"/></a>
			          <a onclick="document.getElementById('chooselan').value='th';document.getElementById('loginform').submit();" href="#"><img height="27" width="27" title="ไทย" alt="ไทย" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/thailand.png"/></a>	          
			          <a onclick="document.getElementById('chooselan').value='vi';document.getElementById('loginform').submit();" href="#"><img height="27" width="27" title="Tiếng Việt" alt="Tiếng Việt" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/vietnam.png"/></a>
			          <a onclick="document.getElementById('chooselan').value='zh_cn';document.getElementById('loginform').submit();" href="#"><img height="27" width="27" title="中文简体" alt="中文简体" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/chinaTrad.png"/></a>	          
          			  <a onclick="document.getElementById('chooselan').value='zh_hk';document.getElementById('loginform').submit();" href="#"><img height="27" width="27" title="中文繁体" alt="中文繁体" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/hongkong.png"/></a>
			    </div>
			</form>
            <label><?php echo (Yii::app()->session['account_type']!='agent_alias') ? Yii::app()->session['account_id'] : Yii::app()->session['account_id'] . ' ('. Yii::app()->session['agent_alias_name'] .')';?></label><br/><font color="black"><?php echo Yii::app()->session['level_name'];?> | 
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=SystemSetting/UserList/UserPassword" ><?php echo Yii::t('main','main.header.change_password');?></a> | 
           	<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=Login/Logout" ><?php echo Yii::t('main','main.header.logout');?></a></font>
           	
            </div>
           
    </div><!-- header -->
 
    <div class="navigationmenu" id="navmenu"> 
        <ul id="sliding-navigation">
        
        
		
			<?php if( Yii::app()->user->checkAccess('agent.addNewSubCompany') ||
						Yii::app()->user->checkAccess('agent.readAgentOverview') ||
						Yii::app()->user->checkAccess('agent.readAgentParameters') ||
						Yii::app()->user->checkAccess('agent.readSlotWinLoss') ||
						Yii::app()->user->checkAccess('agent.readSubCompanyList') ||
						Yii::app()->user->checkAccess('agent.readTransactionHistory') ||
						Yii::app()->user->checkAccess('agent.readWinLoss') ||
						User::getUserType() === Constants::ROLE_AGENT
			    	){?>
	        			<li class="sliding-element"><h2><?php echo Yii::t('main','main.agentsystem.title');?></h2></li>
	        		<?php }?>
	        <?php 
			if( Yii::app()->user->checkAccess('agent.readAgentOverview') ||	User::getUserType() === 'agent')
				{?>
				<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentInformation"><font id="mnu_agent_info"><?php echo Yii::t('main','main.agentsystem.agent_overview');?></font></a></li>
				<?php }?>
        	
        	<?php
        	
        	$linkTitle='';
        	$linkListTitle='';
			if(Yii::app()->user->checkAccess('agent.addNewSubCompany') || Yii::app()->user->checkAccess('agent.readSubCompanyList')){
				$linkTitle=Yii::t('main','main.agentsystem.new_sub_company');
				$linkListTitle=Yii::t('main','main.agentsystem.sub_company');
			}else if(Yii::app()->session['level']=='SC'){
				$linkTitle=Yii::t('main','main.agentsystem.new_senior_master_agent');
				$linkListTitle=Yii::t('main','main.agentsystem.senior_master_agent');
			}else if(Yii::app()->session['level']=='SMA'){
				$linkTitle=Yii::t('main','main.agentsystem.new_master_agent');
				$linkListTitle=Yii::t('main','main.agentsystem.master_agent');
			}else if(Yii::app()->session['level']=='MA'){
				$linkTitle=Yii::t('main','main.agentsystem.new_agent');
				$linkListTitle=Yii::t('main','main.agentsystem.agent');
			}else if(Yii::app()->session['level']=='AGT'){
				$linkTitle=Yii::t('main','main.agentsystem.new_player');
				$linkListTitle=Yii::t('main','main.agentsystem.player');
			}
        	?>
			
        	<?php if(Yii::app()->user->checkAccess('agent.addNewSubCompany') || User::getUserType() === 'agent'){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany"><font id="mnu_new_edit_agent"><?php echo $linkTitle;?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('agent.readSubCompanyList') || User::getUserType() === 'agent'){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSubCompany"><font id="mnu_agent_list_info"><?php echo $linkListTitle;?> <?php echo Yii::t('main','main.agentsystem.list');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('agent.readWinLoss') || User::getUserType() === 'agent'){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss"><font id="mnu_agent_winloss"><?php echo Yii::t('main','main.agentsystem.winloss');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('agent.readSlotWinLoss') || User::getUserType() === 'agent'){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss"><font id="mnu_agent_slot_winloss"><?php echo Yii::t('main','main.agentsystem.slot_winloss');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('agent.readTransactionHistory') || User::getUserType() === 'agent'){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentTransactionHistory"><font id="mnu_transaction_hist"><?php echo Yii::t('main','main.agentsystem.transaction_history');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('agent.readFundTransferHistory') || User::getUserType() === 'agent'){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentFundTransferHistory"><font id="mnu_afundTrans">Fund Transfer History</font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('agent.readAgentParameters')){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentParameter"><font id="mnu_agent_parameter"><?php echo Yii::t('main','main.agentsystem.agent_parameter');?></font></a></li>
            <?php }?>
        	<?php if(Yii::app()->user->checkAccess('agent.writeRevenueSharingAdjustment')){?>
        	<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentRevenueSharingAdjustment"><font id="mnu_agent_revenue_sharing_adjustment"><?php echo Yii::t('main','main.agentsystem.revenue_sharing_adjustment');?></font></a></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('agent.readTransactionHistory') || User::getUserType() === 'agent'){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSubCompanyLogAll"><font id="mnu_agent_logs">Agent Logs</font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('agent.readSettlementReport')){?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/Settlement"><font id="mnu_agent_settlement_report">Settlement Report</font></a></li>
            <?php }?>
        	<?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerOverview') ||
        			Yii::app()->user->checkAccess('cashPlayer.readCashPlayerList') ||
        			Yii::app()->user->checkAccess('cashPlayer.readCashPlayerInfo') ||
        			Yii::app()->user->checkAccess('cashPlayer.readWinLoss') ||
        			Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss') ||
        			Yii::app()->user->checkAccess('cashPlayer.readConsolidatedWinLoss') ||
        			Yii::app()->user->checkAccess('cashPlayer.readTransHistorySummary') ||
        			Yii::app()->user->checkAccess('cashPlayer.readTransHistoryDetails') ||
        			Yii::app()->user->checkAccess('cashPlayer.readCashPlayerParameters') 
        			) 
        	  {
        		echo "<li class='sliding-element'><h2>" . Yii::t('main','main.cashplayer.title') . "</h2></li>";
        	  }
        	?>       	
        	<?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerOverview')) {?>
        	<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerOverview"><font id="mnu_cash_player_overview"><?php echo Yii::t('main','main.cashplayer.cashplayer_overview');?></font></a></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerList')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList"><font id="mnu_cash_player"><?php echo Yii::t('main','main.cashplayer.cashplayer_list');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerInfo')) {?>
	           		<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo"><font id="mnu_cash_player_info"><?php echo Yii::t('main','main.cashplayer.cashplayer_information');?></font></a></li>
          	<?php }?>
          	<?php if(Yii::app()->user->checkAccess('cashPlayer.readWinLoss')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLoss"><font id="mnu_winloss"><?php echo Yii::t('main','main.cashplayer.winloss');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLoss"><font id="mnu_slot_winloss"><?php echo Yii::t('main','main.cashplayer.slot_winloss');?></font></a></li>
            <?php }?>
            
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readConsolidatedWinLoss')) {?>
            	<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerConsolidatedWinLoss"><font id="mnu_consolidated_winloss"><?php echo Yii::t('main','main.cashplayer.consolidated_winloss');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readTransHistorySummary')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1" ><font id="mnu_trans1"><?php echo Yii::t('main','main.cashplayer.transaction_history_summary');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readTransHistoryDetails')) {?>
            		<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistory" ><font id="mnu_trans2"><?php echo Yii::t('main','main.cashplayer.transaction_history_detailed');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readFundTransferHistory')) {?>
            		<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerFundTransferHistory" ><font id="mnu_fundTrans">Fund Transfer History</font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerParameters')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerMaxCommission"><font id="mnu_cashplayer_param"><?php echo Yii::t('main','main.cashplayer.cashplayer_parameter');?></font></a></li>
            <?php }?>
            
            <?php if(Yii::app()->user->checkAccess('cashPlayer.readTransactionItem')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/TransactionItems"><font id="mnu_transaction_items"><?php echo Yii::t('main','main.cashplayer.transaction_items');?></font></a></li>
       		<?php }?>
       		<?php if(Yii::app()->user->checkAccess('marketing.readCustomReport')) {?>
       		<li class="sliding-element"><h2><?php echo Yii::t('main','main.marketing.title');?></h2></li>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1"><font id="mnu_custom_report1"><?php echo Yii::t('main','main.marketing.customreport');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('marketing.readPromoItem')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PromoItem"><font id="mnu_promo_item"><?php echo yii::t('main','main.marketing.promo_items');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('marketing.readPlayerValueReport')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PlayerValueReport"><font id="mnu_player_value"><?php echo yii::t('main','main.marketing.player_value');?></font></a></li>
         	<?php }?>
       		
        	<?php if(Yii::app()->user->checkAccess('banking.readPayment')) {?>
        		<li class="sliding-element"><h2><?php echo Yii::t('main','main.banking.title');?></h2></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('banking.readPayment')) {?>
        	<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment"><font id="mnu_payment"><?php echo Yii::t('main','main.banking.payment');?></font></a></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('gameSetting.readTableLimit','gameSetting.readTableLimitLog','gameSetting.writeTableLimit')) {?>
        	<li class="sliding-element"><h2><?php echo Yii::t('main','main.gamesetting.title');?></h2></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('gameSetting.readTableLimit')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList"><font id="mnu_table_limit"><?php echo Yii::t('main','main.gamesetting.table_limit');?></font></a></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('systemMonitor.readOnlineUsers') || User::getUserType() === 'agent') {?>
            	<li class="sliding-element"><h2><?php echo Yii::t('main','main.systemmonitor.title');?></h2></li>
            <?php }?>
		    <?php if(Yii::app()->user->checkAccess('systemMonitor.readOnlineUsers')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement"><font id="mnu_online_management"><?php echo Yii::t('main','main.systemmonitor.online_users');?></font></a></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('systemMonitor.readInstantPools') || User::getUserType() === Constants::ROLE_AGENT) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPoolsMB01"><font id="mnu_instant_pools"><?php echo Yii::t('main','main.systemmonitor.instant_pools');?></font></a></li>
        	<?php }?>
        	<?php if (User::getUserType() === Constants::ROLE_AGENT) 
        	     {?>
					<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement"><font id="mnu_online_management"><?php echo Yii::t('main','main.systemmonitor.online_agent_players');?></font></a></li>
        	<?php }?>
    
    		<?php if(Yii::app()->user->checkAccess('systemSetting.readCurrencySetting') ||
    				Yii::app()->user->checkAccess('systemSetting.readTranslation') ||
    				Yii::app()->user->checkAccess('systemSetting.readUserAccount') ||
    				Yii::app()->user->checkAccess('systemSetting.readWhiteList')   ||
    				Yii::app()->user->checkAccess('systemSetting.readAnnouncement')||
    				Yii::app()->user->checkAccess('systemSetting.readWinnerList')
					) 
    			  {?>
        				<li class="sliding-element"><h2><?php echo Yii::t('main','main.systemsetting.title');?></h2></li>
        	<?php }?>
        	<?php if(Yii::app()->user->checkAccess('systemSetting.readCurrencySetting')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/CurrencyList"><font id="mnu_currency_setting"><?php echo Yii::t('main','main.systemsetting.currency_setting');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('systemSetting.readEmailSetting')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting"><font id="mnu_email_setting"><?php echo Yii::t('main','main.systemsetting.email_setting');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('systemSetting.readUserAccount')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/User"><font id="mnu_user_account"><?php echo Yii::t('main','main.systemsetting.user_account');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('systemSetting.readRoleAndPermission')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Role"><font id="mnu_user_role_information"><?php echo Yii::t('main','main.systemsetting.role_and_permission');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('systemSetting.readTranslation')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/translate/edit/admin"><font id="mnu_user_account"><?php echo Yii::t('main','main.systemsetting.translation');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('systemSetting.readWhiteList')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WhiteList"><font id="mnu_white_list"><?php echo Yii::t('main','main.systemsetting.white_list');?></font></a></li>
	        <?php }?>
			<?php if(Yii::app()->user->checkAccess('systemSetting.readWinnerList')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList"><font id="mnu_winner_list"><?php echo Yii::t('main','main.systemsetting.winner_list');?></font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('systemSetting.readAdvisory')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory"><font id="mnu_advisory">Advisory</font></a></li>
            <?php }?>
            <?php if(Yii::app()->user->checkAccess('systemSetting.readAnnouncement')) {?>
            <li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Announcement"><font id="mnu_system_setting_announcement">Announcement</font></a></li>
            <?php }?>
            
 			<?php if (Yii::app()->user->checkAccess('securityLog.readLoginAudit') ||
        	Yii::app()->user->checkAccess('securityLog.readApplicationLogs') ) { ?>
        		<li class="sliding-element"><h2><?php echo Yii::t('main','main.security_logs.title');?></h2></li>
        	<?php }?>
        	<?php if (Yii::app()->user->checkAccess('securityLog.readLoginAudit')) {?>
            	<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/LoginAudit"><font id="mnu_login_audit"><?php echo Yii::t('main','main.security_logs.login_audit');?></font></a></li>
	      	<?php } ?>
			<?php if (Yii::app()->user->checkAccess('securityLog.readApplicationLogs')) { ?>
            	<li class="sliding-element"><a href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/ApplicationLogs"><font id="mnu_application_logs"><?php echo Yii::t('main','main.security_logs.application_logs');?></font></a></li>
            <?php } ?>
        </ul>
    </div><!-- navigationmenu -->
<div id="navwrapper">
	<div id="btnHide" onclick="javascript: getElementById('navmenu').style.display='none'; getElementById('btnShow').style.display='block'; getElementById('btnHide').style.display='none';//document.getElementById('mncont').style.width='100%';"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/arrow_left.png"/></div>
	<div id="btnShow" style="display:none" onclick="javascript: getElementById('navmenu').style.display='block'; getElementById('btnHide').style.display='block'; getElementById('btnShow').style.display='none';"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/arrow_right.png"/></div>
</div>
<!--  <div class="content" id="mncont">-->
        <?php 
			echo $content;
        ?>
    <!--  </div><!-- content -->
    <!--  
    <div class="clear"></div>
    <div id="footer">
        Copyright <?php echo date('Y'); ?>. Clubvegas999 All rights reserved.
        <br/>
		Version 1.0.3 Beta 
    </div><!-- footer -->
</div> <!--  container -->
<script type="text/javascript">
$('li.sliding-element').on('click',function(){
	jQuery.ajax({
		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1/ResetCustomResportSession',
		type: 'POST',
		data: '',
		context:'',
		async:false,
		success: function(data) {
		}
	});
});

setInterval(function(){getMonitorHeightWithNav();},50);

function getMonitorHeightWithNav(){
	var myWidth;
	var myHeight;
	
	if( typeof( window.innerWidth ) == 'number' ) { 
	
	//Non-IE 
	myWidth = window.innerWidth;
	myHeight = window.innerHeight; 

	} else if( document.documentElement && 

	( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) { 

	//IE 6+ in 'standards compliant mode' 
	myWidth = document.documentElement.clientWidth; 
	myHeight = document.documentElement.clientHeight; 

	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) { 

	//IE 4 compatible 
	myWidth = document.body.clientWidth; 
	myHeight = document.body.clientHeight; 
	
	} 

	if(document.getElementById('navmenu').style.display=='none'){
		document.getElementById('content').style.width=(((myWidth-20)/myWidth)*100)+"%";//"98.6%";//
	}else{
		if (myWidth>950){
			document.getElementById('content').style.width=(((myWidth-220)/myWidth)*100)+"%"; //"84.7%"//
		}else{
			document.getElementById('content').style.width=(((myWidth-221)/myWidth)*100)+"%"; //"84.7%"//
		}
	}

		document.getElementById('content').style.height=(myHeight-95)+"px";
		document.getElementById('navmenu').style.height=(myHeight-85)+"px";
		document.getElementById('navwrapper').style.height=(myHeight-85)+"px";

}

</script>
</body>
</html>
