<?php
/**
 * @todo 
 * @copyright CE
 * @author Eric
 * @since 2012-0406
 *
 */

class CashPlayerSB extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}

class CashPlayer 
{
	
	public function getPlayerList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$whereClause='';
		$strId=htmlEntities($_GET["strvalue"],ENT_QUOTES);
		
		if($_GET['type']=='' && $_GET['status']=='' && $_GET['currency']==''){
			$whereClause="WHERE account_id like '%".$strId."%' OR account_name like '%".$strId."%'  OR balance like '%".$strId."%'";
		}else if($_GET['type']!='' && $_GET['status']=='' && $_GET['currency']==''){
			$whereClause="WHERE ".$_GET["type"]." like '%".$strId ."%'";
		}else if($_GET['type']=='' && $_GET['status']!='' && $_GET['currency']==''){
			$whereClause="WHERE status_id=".$_GET['status'] . " AND (account_id like '%".$strId."%' OR account_name like '%".$strId."%'  OR balance like '%".$strId."%')";
		}else if($_GET['type']=='' && $_GET['status']=='' && $_GET['currency']!=''){
			$whereClause="WHERE currency_id=".$_GET['currency'] . " AND (account_id like '%".$strId."%' OR account_name like '%".$strId."%'  OR balance like '%".$strId."%')";
		}else if($_GET['type']!='' && $_GET['status']!='' && $_GET['currency']==''){
			$whereClause="WHERE ".$_GET["type"]." like '%".$strId ."%' AND status_id=".$_GET["status"];
		}else if($_GET['type']!='' && $_GET['status']=='' && $_GET['currency']!=''){
			$whereClause="WHERE ".$_GET["type"]." like '%".$strId ."%' AND currency_id=".$_GET['currency'];
		}else if($_GET['type']=='' && $_GET['status']!='' && $_GET['currency']!=''){
			$whereClause="WHERE status_id=".$_GET["status"]. " AND currency_id=".$_GET["currency"] . " AND (account_id like '%".$strId."%' OR account_name like '%".$strId."%'  OR balance like '%".$strId."%')";
		}
		//$command = $connection->createCommand("Select * from tbl_cash_player");
		$command = $connection->createCommand("Select * from vwGetAllPlayerList $whereClause ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;		
	}
	public function getCountPlayerList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$whereClause='';
		$strId=htmlEntities($_GET["strvalue"],ENT_QUOTES);
		
		if($_GET['type']=='' && $_GET['status']=='' && $_GET['currency']==''){
			$whereClause="WHERE account_id like '%".$strId."%' OR account_name like '%".$strId."%'  OR balance like '%".$strId."%'";
		}else if($_GET['type']!='' && $_GET['status']=='' && $_GET['currency']==''){
			$whereClause="WHERE ".$_GET["type"]." like '%".$strId ."%'";
		}else if($_GET['type']=='' && $_GET['status']!='' && $_GET['currency']==''){
			$whereClause="WHERE status_id=".$_GET['status'] . " AND (account_id like '%".$strId."%' OR account_name like '%".$strId."%'  OR balance like '%".$strId."%')";
		}else if($_GET['type']=='' && $_GET['status']=='' && $_GET['currency']!=''){
			$whereClause="WHERE currency_id=".$_GET['currency'] . " AND (account_id like '%".$strId."%' OR account_name like '%".$strId."%' OR  balance like '%".$strId."%' )";
		}else if($_GET['type']!='' && $_GET['status']!='' && $_GET['currency']==''){
			$whereClause="WHERE ".$_GET["type"]." like '%".$strId ."%' AND status_id=".$_GET["status"];
		}else if($_GET['type']!='' && $_GET['status']=='' && $_GET['currency']!=''){
			$whereClause="WHERE ".$_GET["type"]." like '%".$strId ."%' AND currency_id=".$_GET['currency'];
		}else if($_GET['type']=='' && $_GET['status']!='' && $_GET['currency']!=''){
			$whereClause="WHERE status_id=".$_GET["status"]. " AND currency_id=".$_GET["currency"] . " AND (account_id like '%".$strId."%' OR account_name like '%".$strId."%'  OR balance like '%".$strId."%')";
		}
		//$command = $connection->createCommand("Select * from tbl_cash_player");
		$command = $connection->createCommand("Select COUNT(0) from vwGetAllPlayerList $whereClause");
		$rows = $command->query();
		return $rows;
	}
	
	/**Begin Add===========================================================================
	 * @todo: Query for the display of cash player info
	 * @author: Allan
	 * @Since: 04072012
	 */
	public function getPlayerInfo($orderField, $sortType, $startIndex , $limit)
	{
		
		$connection = Yii::app()->db_cv999_fd_master;
		$whereClause='';
		if ($_GET['test']==0){
			if ($_GET['id']=='All' && $_GET['name']=='All'){	
				$whereClause='WHERE account_id <> "" ';
			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' ";
			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
				$whereClause="WHERE account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' OR id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ";
			}else{
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and (account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' OR id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ) ";
			}
		}else{
			if ($_GET['id']=='All' && $_GET['name']=='All'){
				$whereClause="WHERE currency_name <>'TEST'";
			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and currency_name <>'TEST' ";
			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
				$whereClause="WHERE (account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' OR id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ) and currency_name <>'TEST'";
			}else{
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and (account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' OR id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ) and currency_name <>'TEST' ";
			}
		}
		$idnum="";
		if ($_GET['idnum']==''){
			$idnum="";
		}else{
			$idnum=" and id='".$_GET['idnum']."' ";
		}
		
		$type='';
		if ($_GET['type']=="All"){
			$type='';
		}else{
			if($_GET['type']=="Cash"){
				$type="AND account_type='".$_GET['type']."' ";
			}else{
				$type="AND account_type='".$_GET['type']."'";
			}
		}
		$status='';
		if ($_GET['status']=="All"){
			$status='';
		}else{
			if($_GET['status']=="Active"){
				$status="AND status_name='".$_GET['status']."' ";
			}else{
				$status="AND status_name='".$_GET['status']."'";
			}
		}
		
		$command = $connection->createCommand("Select * from vwGetAllPlayerInfo $whereClause $type $status $idnum ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();

        $resultSet = $rows->readAll();

        foreach($resultSet as $key => $value){
            if($value['afftoken']!=''){
                $affCode = CV999Utility::prefix_decode($value['afftoken']);
                if(is_array($affCode)){
                    $resultSet[$key]['afftoken'] = $affCode['aff_code'];
                }else{
                    $resultSet[$key]['afftoken'] = '';
                }
            }
        }

        return $resultSet;
	}
	
	
	public function getCountPlayerInfo()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$whereClause='';
		if ($_GET['test']==0){
			if ($_GET['id']=='All' && $_GET['name']=='All'){	
				$whereClause='WHERE account_id<> "" ';
			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' ";
			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
				$whereClause="WHERE account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%'";
			}else{
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%'";
			}
		}else{
			if ($_GET['id']=='All' && $_GET['name']=='All'){
				$whereClause="WHERE currency_name <>'TEST'";
			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and currency_name <>'TEST' ";
			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
				$whereClause="WHERE account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' and currency_name <>'TEST'";
			}else{
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%'  and currency_name <>'TEST' ";
			}
		}
		$idnum="";
		if ($_GET['idnum']==''){
			$idnum="";
		}else{
			$idnum=" and id='".$_GET['idnum']."' ";
		}
		
		$type='';
		if ($_GET['type']=="All"){
			$type='';
		}else{
			if($_GET['type']=="Cash"){
				$type="AND account_type='".$_GET['type']."' ";
			}else{
				$type="AND account_type='".$_GET['type']."'";
			}
		}
		$status='';
		if ($_GET['status']=="All"){
			$status='';
		}else{
			if($_GET['status']=="Active"){
				$status="AND status_name='".$_GET['status']."' ";
			}else{
				$status="AND status_name='".$_GET['status']."'";
			}
		}
		
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetAllPlayerInfo $whereClause $type $status $idnum");
		$rows = $command->query();
		return $rows;
	}
	//End added by Allan===================================================================
	/**Begin Add===========================================================================
	 * @todo: Query for the Export Cash player info to excel file
	* @author: Allan
	* @Since: 2013-08-23
	*/
	public function getExporAllPlayerInfo()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		$whereClause='';
		if ($_GET['test']==0){
			if ($_GET['id']=='All' && $_GET['name']=='All'){
				$whereClause='WHERE account_id <> "" ';
			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' ";
			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
				$whereClause="WHERE account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ";
			}else{
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ";
			}
		}else{
			if ($_GET['id']=='All' && $_GET['name']=='All'){
				$whereClause="WHERE currency_name <>'TEST'";
			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and currency_name <>'TEST' ";
			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
				$whereClause="WHERE account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' and currency_name <>'TEST'";
			}else{
				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' and currency_name <>'TEST' ";
			}
		}
	
		$type='';
		if ($_GET['type']=="All"){
			$type='';
		}else{
			if($_GET['type']=="Cash"){
				$type="AND account_type='".$_GET['type']."' ";
			}else{
				$type="AND account_type='".$_GET['type']."'";
			}
		}
		$status='';
		if ($_GET['status']=="All"){
			$status='';
		}else{
			if($_GET['status']=="Active"){
				$status="AND status_name='".$_GET['status']."' ";
			}else{
				$status="AND status_name='".$_GET['status']."'";
			}
		}
	
		$command = $connection->createCommand("Select * from vwGetAllPlayerInfo $whereClause $type $status ");
		$rows = $command->query();
		return $rows;
	}
	/**Begin Add===========================================================================
	 * @todo: Query for the display of cash player info
	* @author: Allan
	* @Since: 04072012
	*/
	public function getPlayerLog()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		
		$command = $connection->createCommand("Select * from vwGetAllPlayerList ");
		$rows = $command->query();
		return $rows;
	}
	
	//End added by Allan===================================================================
	
	public function getPlayerBonus()
	{
		$connection = Yii::app()->db_cv999_fd_master;
	
		//$command = $connection->createCommand("Select * from tbl_cash_player");
		$command = $connection->createCommand("Select * from vwGetAllPlayerList");
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayerSetBonus()
	{
		$connection = Yii::app()->db_cv999_fd_master;
	
		//$command = $connection->createCommand("Select * from tbl_cash_player");
		$command = $connection->createCommand("Select * from tbl_cash_player_bonus");
		$rows = $command->query();
		//return $rows;
	}
	
	public function getBankList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select * from tbl_bank ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	public function getCountBankList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from tbl_bank");
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayerInformation(){
		$connection = Yii::app()->db_cv999_fd_master;
		$index_id=$_POST['index_id'];
		if ($_POST['method']=='next'){
			$command = $connection->createCommand("SELECT account_id FROM tbl_cash_player tcp WHERE tcp.id>$index_id ORDER BY tcp.id ASC LIMIT 1");
			$rd=$command->queryRow();
			$accountID=$rd['account_id'];
			$where='tcp.id>'.$index_id.' ORDER BY tcp.id ASC LIMIT 1';
		}elseif ($_POST['method']=='first'){
			$command = $connection->createCommand("SELECT MIN(id) as id FROM tbl_cash_player");
			$rd=$command->queryRow();
			$ID=$rd['id'];
			$where='tcp.id='.$rd['id'].' ORDER BY tcp.id ASC LIMIT 1';
			$command1 = $connection->createCommand("SELECT account_id FROM tbl_cash_player tcp WHERE tcp.id=$ID ORDER BY tcp.id ASC LIMIT 1");
			$rd1=$command1->queryRow();
			$accountID=$rd1['account_id'];
		}elseif ($_POST['method']=='previous'){
			$command = $connection->createCommand("SELECT account_id FROM tbl_cash_player tcp WHERE tcp.id<$index_id ORDER BY tcp.id DESC  LIMIT 1");
			$rd=$command->queryRow();
			$accountID=$rd['account_id'];
			$where='tcp.id<'.$index_id .' ORDER BY tcp.id DESC LIMIT 1';
		}elseif ($_POST['method']=='last'){
			$command = $connection->createCommand("SELECT MAX(id) as id FROM tbl_cash_player");
			$rd=$command->queryRow();
			$ID=$rd['id'];
			$where='tcp.id='.$rd['id'].' ORDER BY tcp.id ASC LIMIT 1';
			$command1 = $connection->createCommand("SELECT account_id FROM tbl_cash_player tcp WHERE tcp.id=$ID ORDER BY tcp.id ASC LIMIT 1");
			$rd1=$command1->queryRow();
			$accountID=$rd1['account_id'];
		}
		$command = $connection->createCommand("(SELECT IF(tcp.id IS NULL,'N/A',tcp.id) AS id,IF(tcp.account_id IS NULL,'N/A',tcp.account_id) AS account_id,IF(tcp.account_name IS NULL,'N/A',tcp.account_name) AS account_name,IF(tcp.email IS NULL,'N/A',tcp.email) AS email,
													IF(tcp.phone_number IS NULL,'N/A',tcp.phone_number) AS phone_number,IF(tc.currency_name IS NULL,'N/A',tc.currency_name) AS currency_name,IF(tcp.country IS NULL,'N/A',tcp.country) AS country,IF(tcp.sex IS NULL,'N/A',tcp.sex) AS sex,
													IF(tcp.dob IS NULL,'N/A',tcp.dob) AS dob,IF(tcp.address IS NULL,'N/A',tcp.address) AS address,IF(tcp.zip_code IS NULL,'N/A',tcp.zip_code) AS zip_code,
													IF(tcp.city IS NULL,'N/A',tcp.city) AS city,IF(tcp.opening_acc_date IS NULL,'N/A',tcp.opening_acc_date) AS opening_acc_date,IF(ts.status_name IS NULL,'N/A',ts.status_name) AS status_name,IF(tcp.bank_account_number IS NULL,'N/A',tcp.bank_account_number) AS bank_account_number,
													IF(tcp.bank_account_name IS NULL,'N/A',tcp.bank_account_name) AS bank_account_name,IF(tcp.bank_used IS NULL,'N/A',tcp.bank_used) AS bank_used,
													IF(tcp.bank_branch IS NULL,'N/A',tcp.bank_branch) AS bank_branch,IF(tcp.skrill_email IS NULL,'N/A',tcp.skrill_email) AS skrill_email,IF(tcp.neteller_account IS NULL,'N/A',tcp.neteller_account) AS neteller_account,IF(tcp.digit_security_num IS NULL,'N/A',tcp.digit_security_num) AS security_num,
													(SELECT IF(MAX(SESSION)IS NULL,'N/A',MAX(SESSION)) FROM tbl_player_login_session_history WHERE account_id =tcp.account_id) AS last_access_date,
													(SELECT casino_name FROM (SELECT COUNT(tcbt.casino_code) AS cnt,tc.casino_name 
													FROM (SELECT * FROM tbl_casino_balance_transmitter WHERE account_id='".$accountID."' GROUP BY transaction_number) AS tcbt
													INNER JOIN tbl_casino tc ON tc.casino_code=tcbt.casino_code
													WHERE tcbt.account_id='".$accountID."' GROUP BY tcbt.casino_code ORDER BY cnt DESC LIMIT 1) AS primary_casino) AS primary_casino,
													tcp.value_score AS value_score,
													((SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(valid_stake)IS NULL,0,SUM(valid_stake)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(amount_wagers)IS NULL,0,SUM(amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(total_wager_amount)IS NULL,0,SUM(total_wager_amount)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."')
													) AS lifetime_turnover,
													((SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(valid_stake)IS NULL,0,SUM(valid_stake)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(amount_wagers)IS NULL,0,SUM(amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
													+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(total_wager_amount)IS NULL,0,SUM(total_wager_amount)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."')	
													) AS turnover_2012,
													((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."')
													) AS GGR,
													((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."' )
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."' )
													+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."')
													) AS GGR_2012,
													(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."')
													)-(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."')) 
													AS NGR,
													(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."' )
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."' )
													+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
													+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."')
													)-(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."' AND SUBSTRING(submit_time,1,4)='".date("Y")."')) 
													AS NGR_2012,
													(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 1 AND 2 AND player_id='".$accountID."') AS lifetime_deposit,
													(SELECT COUNT(amount) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 1 AND 2 AND player_id='".$accountID."') AS count_deposit,
													(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 3 AND 4 AND player_id='".$accountID."') AS lifetime_withdraw,
													(SELECT COUNT(amount) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 3 AND 4 AND player_id='".$accountID."') AS count_withdraw,
													(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."') AS lifetime_bonus,
													(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."' AND SUBSTRING(submit_time,1,4)='".date("Y")."') AS bonus_yearly
													
												FROM tbl_cash_player tcp
												INNER JOIN tbl_currency tc ON tc.id= tcp.currency_id
												INNER JOIN tbl_status ts ON ts.id=tcp.status_id
												WHERE $where )");
		$rd=$command->queryRow();
		$indexId=$rd['id'];
		$accountId=$rd['account_id'];
		$accountName=$rd['account_name'];
		$email=$rd['email'];
		$phoneNumber=$rd['phone_number'];
		$currency=$rd['currency_name'];
		$country=$rd['country'];
		$sex=$rd['sex'];
		$dob=$rd['dob'];
		$address=$rd['address'];
		$zipCode=$rd['zip_code'];
		$city=$rd['city'];
		$openingAccountDate=$rd['opening_acc_date'];
		$status=$rd['status_name'];
		$bankAccountNumber=$rd['bank_account_number'];
		$bankAccountName=$rd['bank_account_name'];
		$bankName=$rd['bank_used'];
		$bankBranch=$rd['bank_branch'];
		$skrillEmail=$rd['skrill_email'];
		$netellerAccount=$rd['neteller_account'];
		$securityNumber=$rd['security_num'];
		$lastAccess=$rd['last_access_date'];
		if ($rd['primary_casino']==''){
			$primaryCasino='N/A';
		}else{
			$primaryCasino=$rd['primary_casino'];
		}
		$valueScore=$rd['value_score'];
		$lifetimeTurnover=$rd['lifetime_turnover'];
		$turnover2012=$rd['turnover_2012'];
		$GGR=$rd['GGR'];
		$GGR2012=$rd['GGR_2012'];
		$NGR=$rd['NGR'];
		$NGR2012=$rd['NGR_2012'];
		$lifetimeDeposit=$rd['lifetime_deposit'];
		$countDeposit=$rd['count_deposit'];
		$lifetimeWithdraw=$rd['lifetime_withdraw'];
		$countWithdraw=$rd['count_withdraw'];
		$lifetimeBonus=$rd['lifetime_bonus'];
		$yearlyBonus=$rd['bonus_yearly'];
		
		//return $rd;
		//print_r($rd);
		//echo json_encode($rd);
		echo $indexId."##".$accountId."##".$accountName."##".$email."##".$phoneNumber."##".$currency."##".$country."##".$sex."##".$dob.
		"##".str_replace("\\","",str_replace("\\\'","'",$address))."##".$zipCode."##".$city."##".$openingAccountDate."##".$status."##".$bankAccountNumber."##".$bankAccountName.
		"##".$bankName."##".$bankBranch."##".$skrillEmail."##".$netellerAccount."##".$securityNumber."##".$lastAccess."##".$primaryCasino."##".$valueScore.
		"##".number_format($lifetimeTurnover,2)."##".number_format($turnover2012,2)."##".number_format($GGR,2)."##".number_format($GGR2012,2)."##".number_format($NGR,2)."##".number_format($NGR2012,2)."##".number_format($lifetimeDeposit,2)."##".$countDeposit.
		"##".number_format($lifetimeWithdraw,2)."##".$countWithdraw."##".number_format($lifetimeBonus,2)."##".$yearlyBonus;
	}
	
	public function getPlayerInformationById(){
		$connection = Yii::app()->db_cv999_fd_master;
		$accountID=htmlEntities($_POST['account_id'],ENT_QUOTES);
		$account_name=htmlEntities($_POST['account_name'],ENT_QUOTES);
		if ($accountID!='' && $account_name!=''){
			$accountID=$_POST['account_id'];
		}elseif ($accountID !='' && $account_name==''){
			$accountID=$_POST['account_id'];
		}elseif ($accountID =='' && $account_name!=''){
			$command = $connection->createCommand("SELECT account_id FROM tbl_cash_player  WHERE account_name='".$account_name."'");
			$rd=$command->queryRow();
			$accountID=$rd['account_id'];
		}elseif($accountID =='' && $account_name==''){
			$command = $connection->createCommand("SELECT account_id FROM tbl_cash_player order by id ASC limit 1");
			$rd=$command->queryRow();
			$accountID=$rd['account_id'];
		}
		$command = $connection->createCommand("(SELECT IF(tcp.id IS NULL,'N/A',tcp.id) AS id,IF(tcp.account_id IS NULL,'N/A',tcp.account_id) AS account_id,IF(tcp.account_name IS NULL,'N/A',tcp.account_name) AS account_name,IF(tcp.email IS NULL,'N/A',tcp.email) AS email,
				IF(tcp.phone_number IS NULL,'N/A',tcp.phone_number) AS phone_number,IF(tc.currency_name IS NULL,'N/A',tc.currency_name) AS currency_name,IF(tcp.country IS NULL,'N/A',tcp.country) AS country,IF(tcp.sex IS NULL,'N/A',tcp.sex) AS sex,
				IF(tcp.dob IS NULL,'N/A',tcp.dob) AS dob,IF(tcp.address IS NULL,'N/A',tcp.address) AS address,IF(tcp.zip_code IS NULL,'N/A',tcp.zip_code) AS zip_code,
				IF(tcp.city IS NULL,'N/A',tcp.city) AS city,IF(tcp.opening_acc_date IS NULL,'N/A',tcp.opening_acc_date) AS opening_acc_date,IF(ts.status_name IS NULL,'N/A',ts.status_name) AS status_name,IF(tcp.bank_account_number IS NULL,'N/A',tcp.bank_account_number) AS bank_account_number,
				IF(tcp.bank_account_name IS NULL,'N/A',tcp.bank_account_name) AS bank_account_name,IF(tcp.bank_used IS NULL,'N/A',tcp.bank_used) AS bank_used,
				IF(tcp.bank_branch IS NULL,'N/A',tcp.bank_branch) AS bank_branch,IF(tcp.skrill_email IS NULL,'N/A',tcp.skrill_email) AS skrill_email,IF(tcp.neteller_account IS NULL,'N/A',tcp.neteller_account) AS neteller_account,IF(tcp.digit_security_num IS NULL,'N/A',tcp.digit_security_num) AS security_num,
				(SELECT IF(MAX(SESSION)IS NULL,'N/A',MAX(SESSION)) FROM tbl_player_login_session_history WHERE account_id =tcp.account_id) AS last_access_date,
				(SELECT casino_name FROM (SELECT COUNT(tcbt.casino_code) AS cnt,tc.casino_name
				FROM (SELECT * FROM tbl_casino_balance_transmitter WHERE account_id='".$accountID."' GROUP BY transaction_number) AS tcbt
				INNER JOIN tbl_casino tc ON tc.casino_code=tcbt.casino_code
				WHERE tcbt.account_id='".$accountID."' GROUP BY tcbt.casino_code ORDER BY cnt DESC LIMIT 1) AS primary_casino) AS primary_casino,
				tcp.value_score AS value_score,
				((SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(valid_stake)IS NULL,0,SUM(valid_stake)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(amount_wagers)IS NULL,0,SUM(amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(total_wager_amount)IS NULL,0,SUM(total_wager_amount)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."')
		) AS lifetime_turnover,
				((SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(available_bet)IS NULL,0,SUM(available_bet)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(valid_stake)IS NULL,0,SUM(valid_stake)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(amount_wagers)IS NULL,0,SUM(amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
				+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(bet_amount)IS NULL,0,SUM(bet_amount)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(total_wager_amount)IS NULL,0,SUM(total_wager_amount)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."')
		) AS turnover_2012,
				((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."')
		) AS GGR,
				((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."')
		) AS GGR_2012,
				IF(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."'))>0,
				(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."'))
				-(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."')),
				(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."'))
				+(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."'))
				)
				AS NGR,
				IF(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."')) >0,
				(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."'))
				-(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."' AND SUBSTRING(submit_time,1,4)='".date("Y")."')),
				(((SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_casino_history_v2 WHERE account_id='".$accountID."' AND SUBSTRING(end_time,1,4)='".date("Y")."' )
				+(SELECT IF(SUM(amount_settlements - amount_wagers)IS NULL,0,SUM(amount_settlements - amount_wagers)) FROM tbl_costavegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_date,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_htv_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_savan_bet_slot_record_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(bet_time,1,4)='".date("Y")."')
				+(SELECT IF(SUM(win_loss)IS NULL,0,SUM(win_loss)) FROM tbl_slotsvegas_casino_history WHERE account_id='".$accountID."' AND SUBSTRING(create_time,1,4)='".date("Y")."'))
				+(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."' AND SUBSTRING(submit_time,1,4)='".date("Y")."'))
				)
				AS NGR_2012,
				(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 1 AND 2 AND player_id='".$accountID."') AS lifetime_deposit,
				(SELECT COUNT(amount) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 1 AND 2 AND player_id='".$accountID."') AS count_deposit,
				(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 3 AND 4 AND player_id='".$accountID."') AS lifetime_withdraw,
				(SELECT COUNT(amount) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id BETWEEN 3 AND 4 AND player_id='".$accountID."') AS count_withdraw,
				(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."') AS lifetime_bonus,
				(SELECT IF(SUM(amount)IS NULL,0,SUM(amount)) FROM tbl_cash_player_trans_history WHERE cash_player_trans_type_id=6 AND player_id='".$accountID."' AND SUBSTRING(submit_time,1,4)='".date("Y")."') AS bonus_yearly
					
				FROM tbl_cash_player tcp
				INNER JOIN tbl_currency tc ON tc.id= tcp.currency_id
				INNER JOIN tbl_status ts ON ts.id=tcp.status_id
				WHERE tcp.account_id='".$accountID."' ORDER BY tcp.account_id ASC LIMIT 1 )");
		$rd=$command->queryRow();
		$indexId=$rd['id'];
		$accountId=$rd['account_id'];
		$accountName=$rd['account_name'];
		$email=$rd['email'];
		$phoneNumber=$rd['phone_number'];
		$currency=$rd['currency_name'];
		$country=$rd['country'];
		$sex=$rd['sex'];
		$dob=$rd['dob'];
		$address=$rd['address'];
		$zipCode=$rd['zip_code'];
		$city=$rd['city'];
		$openingAccountDate=$rd['opening_acc_date'];
		$status=$rd['status_name'];
		$bankAccountNumber=$rd['bank_account_number'];
		$bankAccountName=$rd['bank_account_name'];
		$bankName=$rd['bank_used'];
		$bankBranch=$rd['bank_branch'];
		$skrillEmail=$rd['skrill_email'];
		$netellerAccount=$rd['neteller_account'];
		$securityNumber=$rd['security_num'];
		$lastAccess=$rd['last_access_date'];
		if ($rd['primary_casino']==''){
			$primaryCasino='N/A';
		}else{
			$primaryCasino=$rd['primary_casino'];
		}
		$valueScore=$rd['value_score'];
		$lifetimeTurnover=$rd['lifetime_turnover'];
		$turnover2012=$rd['turnover_2012'];
		$GGR=$rd['GGR'];
		$GGR2012=$rd['GGR_2012'];
		$NGR=$rd['NGR'];
		$NGR2012=$rd['NGR_2012'];
		$lifetimeDeposit=$rd['lifetime_deposit'];
		$countDeposit=$rd['count_deposit'];
		$lifetimeWithdraw=$rd['lifetime_withdraw'];
		$countWithdraw=$rd['count_withdraw'];
		$lifetimeBonus=$rd['lifetime_bonus'];
		$yearlyBonus=$rd['bonus_yearly'];
	
	    //print_r($rd);
     	//echo json_encode($rd);
		echo $indexId."##".$accountId."##".$accountName."##".$email."##".$phoneNumber."##".$currency."##".$country."##".$sex."##".$dob.
		"##".str_replace("\\","",str_replace("\\\'","'",$address))."##".$zipCode."##".$city."##".$openingAccountDate."##".$status."##".$bankAccountNumber."##".$bankAccountName.
		"##".$bankName."##".$bankBranch."##".$skrillEmail."##".$netellerAccount."##".$securityNumber."##".$lastAccess."##".$primaryCasino."##".$valueScore.
		"##".number_format($lifetimeTurnover,2)."##".number_format($turnover2012,2)."##".number_format($GGR,2)."##".number_format($GGR2012,2)."##".number_format($NGR,2)."##".number_format($NGR2012,2)."##".number_format($lifetimeDeposit,2)."##".$countDeposit.
		"##".number_format($lifetimeWithdraw,2)."##".$countWithdraw."##".number_format($lifetimeBonus,2)."##".$yearlyBonus;
	}
}

