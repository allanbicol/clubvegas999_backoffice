//active menu color
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_user_role_information').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>User Role</a></li>");
function userList(listURL)
{
	var grid=jQuery("#listlog");
	grid.jqGrid({ 
		url: listURL, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['Name', 'Role'],
	    colModel: [
	      {name: 'user_name', index: 'user_name', width: 50, search:true},
	      {name: 'edit', index: 'edit', width: 6,formatter: editRole},
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
		},
		width: 500,
	    rowNum: 25,
	    rownumbers: true,
	    rowList: [25,50, 75, 100],
	    pager: '#pager2',
	    sortname: 'user_name',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Role List',
	    viewrecords: true
	});
	
	$('#listlog').jqGrid('navGrid', '#pager2', {edit: false, add: permissionWrite, del:false, search: false, refresh: false});
	
	// add button
	$('.ui-pg-div').click(function(){
		document.location.href = baseUrl + '/index.php?r=SystemSetting/Role/FormCreate';
	});
}
function editRole(cellvalue, options, rowObject) {
	var role_name =  (rowObject[1] === undefined) ? rowObject.account_id : rowObject[1];
	if(permissionView){
		return "<a class='btn mini red' href='"+ baseUrl +"/index.php?r=SystemSetting/Role/FormUpdate&role_id="+ role_name +"&pg="+ $('.ui-pg-input') +"'>Edit <i class='icon-edit'></i></a>";
	}else{
		return "<a class='btn mini red'>Edit <i class='icon-edit'></i></a>";
	}
}