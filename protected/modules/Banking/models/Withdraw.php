<?php
/**
 * @todo Withdraw Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-0406
 *
 */
class Withdraw
{
	Public function updateCashPlayerWithdraw($account_id, $transaction_number, $transaction_status_id, $request_currency_id, $request_amount, $transaction_item, $transaction_time)
	{
		if(Yii::app()->user->checkAccess('banking.writePayment')){
			
			$cp=TableCashPlayer::model()->find('account_id=:account_id', array(':account_id' => $account_id));
		
			if($transaction_status_id == 7){ // cancel withdraw
				$statid = 7; // cancelled
				TableCashPlayerWithdraw::model()->updateAll(array('transaction_status_id' => 7,'bank_name'=>$transaction_item,'last_update_transaction_date' => new CDbExpression('NOW()')),
						'transaction_number="' . $transaction_number . '"');

				TableCashPlayer::model()->updateAll(array('balance'=>$cp['balance'] + $request_amount),'account_id="'.$account_id.'"');
				
			}elseif($transaction_status_id == 6){ // accept/complete withdraw
				$current_status = $this->getCashPlayerWithdrawTrasStatus($transaction_number); // current transaction status
				if($current_status == 1){
					$statid = 5; // pending
				}else if($current_status == 5){
					$statid = 6; // complete
				}else if($current_status == 7 && !$this->isTransactionNumberExist($transaction_number)){
					$statid = 5; // pending
				}else if($current_status == 7 && $this->isTransactionNumberExist($transaction_number)){
					$statid = 6; // complete
				}
				
				TableCashPlayerWithdraw::model()->updateAll(array('transaction_status_id' => $statid,'bank_name'=>$transaction_item,'last_update_transaction_date' => new CDbExpression('NOW()')),
						'transaction_number="' . $transaction_number . '"');
				
				// @todo deduction process
				if($cp['currency_id'] == $request_currency_id){
					if(substr($transaction_number, 0, 2) != 'MW'){ // automatic withdraw
						
						if($statid == 5){
							TableCashPlayer::model()->updateAll(array('balance' => $cp['balance']-$request_amount,
								'last_update_transaction_date' => new CDbExpression('NOW()')),
								'account_id="' . $account_id . '"');
						}else{ 
							TableCashPlayer::model()->updateAll(array('balance' => $cp['balance'],
								'last_update_transaction_date' => new CDbExpression('NOW()')),
								'account_id="' . $account_id . '"');
						}
						
					}
					
				}else{
					$cu = TableCurrency::model()->find('id=:id', array(':id' => $cp['currency_id']));//player's currency
					$rcu = TableCurrency::model()->find('id=:id', array(':id' => $request_currency_id));//request currency
					
					if(substr($transaction_number, 0, 2) != 'MW'){ // frontend withdraw
						
						if($statid == 5){
							// if pending, deduct the requested withdraw amount
							TableCashPlayer::model()->updateAll(array('balance' => $cp['balance']-(($request_amount/$rcu['exchange_rate'])*$cu['exchange_rate']),
								'last_update_transaction_date' => new CDbExpression('NOW()')),
								'account_id="' . $account_id . '"');
						}else{ 
							// if complete, update last_update_transaction_date only
							TableCashPlayer::model()->updateAll(array('balance' => $cp['balance'],
								'last_update_transaction_date' => new CDbExpression('NOW()')),
								'account_id="' . $account_id . '"');
						}
					}
				}
				
			}
					
			$bal_after = TableCashPlayer::model()->find('account_id=:account_id', array(':account_id' => $account_id));
			
			if(!$this->isTransactionNumberExist($transaction_number)){
				if($statid != 7){
					$post = new TableCashPlayerTransHistory();
					$post->player_id = $account_id;
					$post->trans_number = $transaction_number;
					$post->cash_player_trans_type_id = 4;
					$post->currency_id = $request_currency_id;
					$post->amount = $request_amount;
					$post->submit_time = new CDbExpression('NOW()');
					$post->finish_time = $transaction_time;
					$post->cashier_id = Yii::app()->session['account_id'];
					$post->status_id = $statid;
					$post->transaction_item = $transaction_item;
					$post->balance_before = $cp['balance'];
					$post->balance_after = $bal_after['balance'];
					$post->save();
				}
			}else{
 				TableCashPlayerTransHistory::model()->updateAll(array('status_id' => $statid,
 						'transaction_item' => $transaction_item, 
 						'finish_time' => $transaction_time),
 							'trans_number = "' . $transaction_number . '"');
			}
			
			// return message
			if($statid == 5){
				exit('withdraw_pending');
			}else if($statid == 6){
				exit('withdraw_done');
			}else if($statid == 7){
				exit('withdraw_cancelled');
			}
				
		}
	}
	
	/**
	 * @todo get finish time
	 * @author leokarl
	 * @since 2013-01-17
	 * @param string $trans_number
	 * @return array
	 */
	public function getFinishTime($trans_number){
		$model = TableCashPlayerTransHistory::model()->find('trans_number=:trans_number', array(':trans_number'=>$trans_number));
// 		list($date, $time) = explode(" ",$model['finish_time']);
// 		list($hour, $minute, $second) = explode(":",$time);
		
		//return array('date'=>$date, 'hour'=>$hour, 'minute'=>$minute, 'second'=>$second);
		return $model['finish_time'];
	}
	
	/**
	 * @todo check date time
	 * @author leokarl
	 * @since 2013-01-17
	 * @param string $datetime
	 */
	public function sDateTimeValid($datetime){
		// check date and time separator
		if(strpos(trim($datetime), ' ') === false){
			return FALSE;
		}
		
		list($date, $time) = explode(" ", $datetime);
		// check date separator
		if(substr_count($date,"-") <> 2){
			return FALSE;
		}
		
		// check time separator
		if(substr_count($time,":") <> 2){
			return FALSE;
		}
		list($year, $month, $day) = explode("-", $date);
		list($hour, $minute, $second) = explode(":", $time);
		// check date values
		if(trim($year) == ''){
			return FALSE;
		}
		if(trim($month) == ''){
			return FALSE;
		}
		if(trim($day) == ''){
			return FALSE;
		}
		if(!checkdate($month, $day, $year)){
			return FALSE;
		}
		if(trim($hour) == ''){
			return FALSE;
		}
		if(trim($minute) == ''){
			return FALSE;
		}
		if(trim($second) == ''){
			return FALSE;
		}
		if($hour < 0 || $hour > 23){
			return FALSE;
		}
		if($minute < 0 || $minute > 59){
			return FALSE;
		}
		if($second < 0 || $second > 59){
			return FALSE;
		}
		return TRUE;
	}
	/**
	 * @todo get transaction status id from tbl_cash_player_withdraw
	 * @author leokarl
	 * @since 2013-01-05
	 * @param string $trans_number
	 */
	public function getCashPlayerWithdrawTrasStatus($trans_number){
		$model = TableCashPlayerWithdraw::model()->find('transaction_number=:transaction_number', array(':transaction_number'=>$trans_number));
		return $model['transaction_status_id'];
	}
	/**
	 * @todo get currency exchange rate
	 * @author leokarl
	 * @since 2012-12-27
	 * @param int $currency_id
	 * @return float
	 */
	public function getExchangeRateByCurrencyId($currency_id){
		$currency = TableCurrency::model()->find('id=:id', array(':id'=>$currency_id));
		return $currency['exchange_rate'];
	}
	/**
	 * @todo check currency id
	 * @author leokarl
	 * @since 2012-12-27
	 * @param int $currency_id
	 * @return boolean
	 */
	public function isCurrencyIdValid($currency_id){
		$count = TableCurrency::model()->count('id=:id', array(':id'=>$currency_id));
		return ($count > 0) ? TRUE : FALSE;
	}
	/**
	 * @todo check transaction status id
	 * @author leokarl
	 * @since 2012-12-27
	 * @param int $trans_stat_id
	 * @return boolean
	 */
	public function isTransactionStatusIdValid($trans_stat_id){
		/*
		 * $trans_stat_id defination
		 * 6 == complete transaction
		 * 7 == cancelled transaction
		 */
		return ($trans_stat_id == 6 || $trans_stat_id == 7) ? TRUE : FALSE;
	}
	/**
	 * @todo check transaction number in tbl_cash_player_withdraw
	 * @author leokarl
	 * @since 2012-12-27
	 * @param string $transaction_number
	 * @return boolean
	 */
	public function isTransactionNumberExistInTableCashPlayerWithdraw($cash_player_account_id,$transaction_number){
		$count = TableCashPlayerWithdraw::model()->count('transaction_number=:transaction_number AND cash_player_account_id=:cash_player_account_id', 
				array(':transaction_number'=>$transaction_number, ':cash_player_account_id'=>$cash_player_account_id));
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo check transaction item id
	 * @author leokarl
	 * @since 2013-01-03
	 * @param int $trans_id
	 */
	public function isTransactionItemValid($trans_id, $withdraw_type){
// 		if($withdraw_type != 'MW'){
// 			$count = TableTransactionItem::model()->count('id=:id AND item_type=4 AND transaction_bank=1',
// 					array(':id'=>$trans_id));
// 		}else{
			$count = TableTransactionItem::model()->count('id=:id AND item_type=4',
					array(':id'=>$trans_id));
//		}
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo get transaction_item name in tbl_cash_player_trans_history
	 * @author leokarl
	 * @since 2013-01-03
	 * @param string $trans_number
	 */
	public function getTransactionItemInCashplayerTransHist($trans_number){
		// get transaction_item id
		$model = TableCashPlayerTransHistory::model()->find('trans_number=:trans_number',
				array(':trans_number'=>$trans_number));
		
		// get transaction_item name
		$trans_item = TableTransactionItem::model()->find('id=:id',
				array(':id'=>$model['transaction_item']));
		
		return $trans_item['transaction_item'];
	}
	
	/**
	 * @todo get transaction_item in tbl_cash_player_withdraw
	 * @author leokarl
	 * @since 2013-01-04
	 * @param string $trans_number
	 */
	public function getTransactionItemInCashPlayerWithdraw($trans_number){
		// get transaction_item id
		$model = TableCashPlayerWithdraw::model()->find('transaction_number=:trans_number',
				array(':trans_number'=>$trans_number));
		
		return $model['bank_name'];
	}
	
	/**
	 * @todo check cashplayer status
	 * @author leokarl
	 * @since 2012-12-27
	 * @param string $account_id
	 * @return boolean
	 */
	public function isCashPlayerStatusActive($account_id){
		$model = TableCashPlayer::model()->find('account_id=:account_id', array(':account_id'=>$account_id));
		return ($model['status_id'] == 1) ? TRUE : FALSE;
	}
	
	
	/**
	 * @todo disallow special characters
	 * @author leokarl
	 * @since 2012-12-08
	 * @param string $mystring
	 * @return boolean
	 */
	public function isSpecialCharPresent($mystring){
		if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
				&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
				&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false && strpos($mystring, '_') === false
				&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '`') === false && strpos($mystring, '-') === false
				&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
				&& strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
				&& strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){
	
			return FALSE; // special character not present
		}else{
			return TRUE; // special character present
		}
	}
	
	/**
	 * @todo is cashplayer account_id exist
	 * @author leokarl
	 * @since 2012-12-21
	 * @param string $account_id
	 * @return boolean
	 */
	public function isAccountIdExist($account_id){
		$count = TableCashPlayer::model()->count(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);
	
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo check transaction number
	 * @author leokarl
	 * @since 2012-12-24
	 * @param unknown_type $trans_number
	 */
	public function isTransactionNumberExist($trans_number){
		$count = TableCashPlayerTransHistory::model()->count('trans_number=:trans_number', array(':trans_number'=>$trans_number));
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo get bank list
	 * @author leokarl
	 * @since 2013-01-03
	 */
	public function getTransactionItemBankList($withdraw_type)
	{
		$criteria=new CDbCriteria;
// 		if($withdraw_type != 'MW'){
// 			$criteria->condition='item_type=:item_type AND transaction_bank=:transaction_bank';
// 			$criteria->params=array(':item_type'=>4,':transaction_bank'=>1);
// 		}else{
			$criteria->condition='item_type=:item_type';
			$criteria->params=array(':item_type'=>4);
		//}
		
		$model = TableTransactionItem::model()->findAll($criteria);
	
		// generate array data
		$list = CHtml::listData($model, 'id', 'transaction_item');
		return $list;
	}
	
	/**
	 * @todo get withdraw type from transaction number
	 * @author leokarl
	 * @since 2013-01-03
	 * @param string $trans_number
	 */
	public function getWithdrawType($trans_number){
		return substr($trans_number, 0, 2);
	}
	
	public function getBankInformation()
	{
		$ss=TableCashPlayerWithdraw::model()->find('transaction_number=:transaction_number', array(':transaction_number'=>$_POST['transaction_number']));
		echo $ss['bank_account_number'] . ';' . $ss['bank_account_name'] . ';' . $ss['bank_name'] . ';' . $ss['bank_branch'] . ';' . $ss['email'];
	}
	
	public function checkPlayerStat()
	{
		// initialize
		$redis = new RedisManager();
		$s_player_on_lobby = ($redis->isExistingOnLobbyByPlayer($_POST["accountID"])) ? 1 : 0;
		$model = TableCashPlayer::model()->find('account_id=:account_id', array(':account_id'=>$_POST["accountID"]));
		
		// return casino_id = 0 => player is not on the lobby. status_id = 1 => player is active
		echo $s_player_on_lobby . ';' . $model['status_id'];
	}
	public function getFinancePassword()
	{
		echo CV999Utility::generateOneWayPassword($_GET['finance_password']);
	}
}
