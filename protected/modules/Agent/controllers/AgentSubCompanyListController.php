<?php
class AgentSubCompanyListController extends MyController
{
	public function actionAgentSubCompanyList()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new AgentSubCompanyList();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountAgentSubCompanyList();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentSubCompanyList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("account_id","account_id","account_id","account_name","currency_name","status","opening_acc_date","total_player","balance_to_from","credit_assigned","credit","balance","online_status","login_ip_address","online_status","login_ip_address");
		$urlAS= Yii::app()->request->baseUrl;

		if(Yii::app()->user->checkAccess('agent.writeTransfer') && Yii::app()->user->checkAccess('agent.readSubCompanySetting')){
			$htmvalue='<a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentTransfer&account_id=\" onclick=\"javascript:getAccountIDforTransfer(this); return false\">'.Yii::t('agent','agent.subcompanylist.transfer').' <i class=\"icon-share-alt\"></i></a><a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyTransHistory&account_id=\"> '.Yii::t('agent','agent.transHistory.trans').' <i class=\"icon-list\"></i></a><a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyLog&account_id=\"> '.Yii::t('agent','agent.subcompanylog.logs_small').' <i class=\"icon-list\"></i></a><a><a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentNewSubCompany&type=SC&e=1&account_id=\"> '.Yii::t('agent','agent.subcompanylist.setting').' <i class=\"icon-cogs\"></i></a>';
		}else if(!Yii::app()->user->checkAccess('agent.writeTransfer') && Yii::app()->user->checkAccess('agent.readSubCompanySetting')){
			$htmvalue='<a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyTransHistory&account_id=\"> '.Yii::t('agent','agent.transHistory.trans').' <i class=\"icon-list\"></i></a><a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyLog&account_id=\"> '.Yii::t('agent','agent.subcompanylog.logs_small').' <i class=\"icon-list\"></i></a><a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentNewSubCompany&type=SC&e=1&account_id=\"> '.Yii::t('agent','agent.subcompanylist.setting').' <i class=\"icon-cogs\"></i></a>';
		}else if(Yii::app()->user->checkAccess('agent.writeTransfer') && !Yii::app()->user->checkAccess('agent.readSubCompanySetting')){
			$htmvalue='<a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentTransfer&account_id=\" onclick=\"javascript:getAccountIDforTransfer(this);checkAgentBalance(); return false\">'.Yii::t('agent','agent.subcompanylist.transfer').' <i class=\"icon-share-alt\"></i></a><a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyTransHistory&account_id=\"> '.Yii::t('agent','agent.transHistory.trans').' <i class=\"icon-list\"></i></a><a class=\"btn	 red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyLog&account_id=\"> '.Yii::t('agent','agent.subcompanylog.logs_small').' <i class=\"icon-list\"></i></a>';
		}else if(!Yii::app()->user->checkAccess('agent.writeTransfer') && !Yii::app()->user->checkAccess('agent.readSubCompanySetting')){
			$htmvalue='<a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyTransHistory&account_id=\"> '.Yii::t('agent','agent.transHistory.trans').' <i class=\"icon-list\"></i></a><a class=\"btn mini red\" style=\"color:white\" href=\"index.php?r=Agent/AgentSubCompanyLog&account_id=\"> '.Yii::t('agent','agent.subcompanylog.logs_small').' <i class=\"icon-list\"></i></a>';
		}

		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,12);
	
			
	}
	
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readSubCompanyList') || User::getUserType() === Constants::ROLE_AGENT)
		{	
			$this->render("index");
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		
	}
	
	
}