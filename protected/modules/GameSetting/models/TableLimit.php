<?php
class TableLimit
{
	public function addNewBacarratLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['banker_player_max']."#".$_GET['banker_player_min']."#".$_GET['tie_max']."#".$_GET['tie_min']."#".$_GET['pair_max']."#".$_GET['pair_min']."#".$_GET['big_max']."#".$_GET['big_min']."#".$_GET['small_max']."#".$_GET['small_min']."#";
		
		$post=new TableGameLimit();
		$post->game_type=1;
		$post->currency_id=$_GET['currency_id'];
		$post->limit=$limit;
		$post->creator_id=Yii::app()->session['id'];
		$post->created_date=new CDbExpression('NOW()');;
		$post->sort_id=$_GET['all_min'].$_GET['all_max'];
		$post->save();
		
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ADDED bacarrat table limit [All-max:'.$_GET['all_max'].'|All-min:'.$_GET['banker_player_min'].'|Banker-max:'.$_GET['banker_player_max'].'|Banker-min:'.$_GET['banker_player_min'].'|Tie-max:'.$_GET['tie_max'].'|Tie-min:'.$_GET['tie_min'].'|Pair-max:'.$_GET['pair_max'].'|Pair-min:'.$_GET['pair_min'].'|Big-max:'.$_GET['big_max'].'|Big-min:'.$_GET['big_min'].'|Small-max:'.$_GET['small_max'].'|Small-min:'.$_GET['small_min'].']</b>';
		$postLog->save();
		
		$rd=TableGameLimit::model()->find(array('select'=>'id','order'=>'id DESC','limit'=>1));
		echo $rd['id'];
	}
	public function updateBacarratLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get baccarat table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" .$_GET['baccarat_limit_id']. "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		list($allmax,$allmin,$bankermax,$bankermin,$tiemax,$tiemin,$pairmax,$pairmin,$bigmax,$bigmin,$smallmax,$smallmin)=split('#',$result);

		//--------------------------------------------------------------------
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['banker_player_max']."#".$_GET['banker_player_min']."#".$_GET['tie_max']."#".$_GET['tie_min']."#".$_GET['pair_max']."#".$_GET['pair_min']."#".$_GET['big_max']."#".$_GET['big_min']."#".$_GET['small_max']."#".$_GET['small_min']."#";
		TableGameLimit::model()->updateAll(array(
				'limit'=>$limit,
				'creator_id'=>Yii::app()->session['id'],
				'sort_id'=>$_GET['all_min'].$_GET['all_max'],
				),
				'id="'.$_GET['baccarat_limit_id'].'"');
				
		if ($result!=$limit){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Clubvegas999';
			$postLog->operated_level='Main Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=18;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE bacarrat table limit from:[All-max:'.$allmax.'|All-min:'.$allmin.'|Banker-max:'.$bankermax.'|Banker-min:'.$bankermin.'|Tie-max:'.$pairmax.'|Tie-min:'.$tiemin.'|Pair-max:'.$pairmax.'|Pair-min:'.$pairmin.'|Big-max:'.$bigmax.'|Big-min:'.$bigmin.'|Small-max:'.$smallmax.'|Small-min:'.$smallmin.']to:[All-max:'.$_GET['all_max'].'|All-min:'.$_GET['banker_player_min'].'|Banker-max:'.$_GET['banker_player_max'].'|Banker-min:'.$_GET['banker_player_min'].'|Tie-max:'.$_GET['tie_max'].'|Tie-min:'.$_GET['tie_min'].'|Pair-max:'.$_GET['pair_max'].'|Pair-min:'.$_GET['pair_min'].'|Big-max:'.$_GET['big_max'].'|Big-min:'.$_GET['big_min'].'|Small-max:'.$_GET['small_max'].'|Small-min:'.$_GET['small_min'].']</b>';
			$postLog->save();
		}	
	}
	
	public function deleteBacarratLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get baccarat table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" .$_GET['baccarat_limit_id']. "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		list($allmax,$allmin,$bankermax,$bankermin,$tiemax,$tiemin,$pairmax,$pairmin,$bigmax,$bigmin,$smallmax,$smallmin)=split('#',$result);

		//--------------------------------------------------------------------
		TableGameLimit::model()->updateAll(array(
				'unused_table_limit'=>1),
				'id="'.$_GET['baccarat_limit_id'].'"');
			
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> DELETE bacarrat table limit [All-max:'.$allmax.'|All-min:'.$allmin.'|Banker-max:'.$bankermax.'|Banker-min:'.$bankermin.'|Tie-max:'.$pairmax.'|Tie-min:'.$tiemin.'|Pair-max:'.$pairmax.'|Pair-min:'.$pairmin.'|Big-max:'.$bigmax.'|Big-min:'.$bigmin.'|Small-max:'.$smallmax.'|Small-min:'.$smallmin.']</b>';
		$postLog->save();
	
	}
	
	public function addNewDragonTigerLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['dragon_tiger_max']."#".$_GET['dragon_tiger_min']."#".$_GET['tie_max']."#".$_GET['tie_min']."#";
		
		$post=new TableGameLimit();
		$post->game_type=4;
		$post->currency_id=$_GET['currency_id'];
		$post->limit=$limit;
		$post->creator_id=Yii::app()->session['id'];
		$post->created_date=new CDbExpression('NOW()');;
		$post->sort_id=$_GET['all_min'].$_GET['all_max'];
		$post->save();
		
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ADDED dragontiger table limit [All-max:'.$_GET['all_max'].'|All-min:'.$_GET['all_min'].'|Dragontiger-max:'.$_GET['dragon_tiger_max'].'|Dragontiger-min:'.$_GET['dragon_tiger_min'].'|Tie-max:'.$_GET['tie_max'].'|Tie-min:'.$_GET['tie_min'].']</b>';
		$postLog->save();
	
		$rd=TableGameLimit::model()->find(array('select'=>'id','order'=>'id DESC','limit'=>1));
		echo $rd['id'];
	}
	public function updateDragonTigerLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get dragontiger table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" . $_GET['dragon_limit_id'] . "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		list($allmax,$allmin,$dragontigermax,$dragontigermin,$tiemax,$tiemin)=split('#',$result);
		//--------------------------------------------------------------------
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['dragon_tiger_max']."#".$_GET['dragon_tiger_min']."#".$_GET['tie_max']."#".$_GET['tie_min']."#";
		TableGameLimit::model()->updateAll(array(
				'limit'=>$limit,
				'creator_id'=>Yii::app()->session['id'],
				'sort_id'=>$_GET['all_min'].$_GET['all_max'],
		),
				'id="'.$_GET['dragon_limit_id'].'"');
		
		if ($result!=$limit){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Clubvegas999';
			$postLog->operated_level='Main Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=18;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE dragontiger table limit from:[All-max:'.$allmax.'|All-min:'.$allmin.'|Dragontiger-max:'.$dragontigermax.'|Dragontiger-min:'.$dragontigermin.'|Tie-max:'.$tiemax.'|Tie-min:'.$tiemin.']to:[All-max:'.$_GET['all_max'].'|All-min:'.$_GET['all_min'].'|Dragontiger-max:'.$_GET['dragon_tiger_max'].'|Dragontiger-min:'.$_GET['dragon_tiger_min'].'|Tie-max:'.$_GET['tie_max'].'|Tie-min:'.$_GET['tie_min'].']</b>';
			$postLog->save();
		}
		
	}
	
	public function deleteDragonTigerLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get dragontiger table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" .$_GET['dragon_limit_id']. "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		list($allmax,$allmin,$dragontigermax,$dragontigermin,$tiemax,$tiemin)=split('#',$result);
		//--------------------------------------------------------------------
		TableGameLimit::model()->updateAll(array(
				'unused_table_limit'=>1),
				'id="'.$_GET['dragon_limit_id'].'"');

			
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> DELETE dragontiger table limit [All-max:'.$allmax.'|All-min:'.$allmin.'|Dragontiger-max:'.$dragontigermax.'|Dragontiger-min:'.$dragontigermin.'|Tie-max:'.$tiemax.'|Tie-min:'.$tiemin.']</b>';
		$postLog->save();
	}
	
	public function addNewRouletteLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['35_to_1_max']."#".$_GET['35_to_1_min']."#".$_GET['18_to_1_max']."#".$_GET['18_to_1_min']."#".$_GET['12_to_1_max']."#".$_GET['12_to_1_min']."#".$_GET['9_to_1_max']."#".$_GET['9_to_1_min']."#".$_GET['6_to_1_max']."#".$_GET['6_to_1_min']."#".$_GET['2_to_1_max']."#".$_GET['2_to_1_min']."#".$_GET['1_to_1_max']."#".$_GET['1_to_1_min']."#";
		
		$post=new TableGameLimit();
		$post->game_type=2;
		$post->currency_id=$_GET['currency_id'];
		$post->limit=$limit;
		$post->creator_id=Yii::app()->session['id'];
		$post->created_date=new CDbExpression('NOW()');;
		$post->sort_id=$_GET['all_min'].$_GET['all_max'];
		$post->save();
		
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ADDED EURO-roulette table limit [All-max:'.$_GET['all_max'].'|All-min:'.$_GET['all_min'].'|35to1-max:'.$_GET['35_to_1_max'].'|35to1-min:'.$_GET['35_to_1_min'].'|18to1-max:'.$_GET['18_to_1_max'].'|18to1-min:'.$_GET['18_to_1_min'].'|12to1-max:'.$_GET['12_to_1_max'].'|12to1-min:'.$_GET['12_to_1_min'].'|9to1-max:'.$_GET['9_to_1_max'].'|9to1-min:'.$_GET['9_to_1_min'].'|6to1-max:'.$_GET['6_to_1_max'].'|6to1-min:'.$_GET['6_to_1_min'].'|2to1-max:'.$_GET['2_to_1_max'].'|2to1-min:'.$_GET['2_to_1_min'].'|1to1-max:'.$_GET['1_to_1_max'].'|1to1-min:'.$_GET['1_to_1_min'].']</b>';
		$postLog->save();
		
		$rd=TableGameLimit::model()->find(array('select'=>'id','order'=>'id DESC','limit'=>1));
		echo $rd['id'];
	}
	public function updateRouletteLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get roullete table limit- Added by Allan |date:2012-06-28
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" .$_GET['roulette_limit_id']. "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		list($allmax,$allmin,$max35,$min35,$max18,$min18,$max12,$min12,$max9,$min9,$max6,$min6,$max2,$min2,$max1,$min1)=split('#',$result);
		//--------------------------------------------------------------------
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['35_to_1_max']."#".$_GET['35_to_1_min']."#".$_GET['18_to_1_max']."#".$_GET['18_to_1_min']."#".$_GET['12_to_1_max']."#".$_GET['12_to_1_min']."#".$_GET['9_to_1_max']."#".$_GET['9_to_1_min']."#".$_GET['6_to_1_max']."#".$_GET['6_to_1_min']."#".$_GET['2_to_1_max']."#".$_GET['2_to_1_min']."#".$_GET['1_to_1_max']."#".$_GET['1_to_1_min']."#";
		TableGameLimit::model()->updateAll(array(
				'limit'=>$limit,
				'creator_id'=>Yii::app()->session['id'],
				'sort_id'=>$_GET['all_min'].$_GET['all_max'],
		),
				'id="'.$_GET['roulette_limit_id'].'"');
		
		if ($result!=$limit){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Clubvegas999';
			$postLog->operated_level='Main Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=18;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE EURO-roulette table limit from:[All-max:'.$allmax.'|All-min:'.$allmin.'|35to1-max:'.$max35.'|35to1-min:'.$min35.'|18to1-max:'.$max18.'|18to1-min:'.$min18.'|12to1-max:'.$max12.'|12to1-min:'.$min12.'|9to1-max:'.$max9.'|9to1-min:'.$min9.'|6to1-max:'.$max6.'|6to1-min:'.$min6.'|2to1-max:'.$max2.'|2to1-min:'.$min2.'|1to1-max:'.$max1.'|1to1-min:'.$min1.']to:[All-max:'.$_GET['all_max'].'|All-min:'.$_GET['all_min'].'|35to1-max:'.$_GET['35_to_1_max'].'|35to1-min:'.$_GET['35_to_1_min'].'|18to1-max:'.$_GET['18_to_1_max'].'|18to1-min:'.$_GET['18_to_1_min'].'|12to1-max:'.$_GET['12_to_1_max'].'|12to1-min:'.$_GET['12_to_1_min'].'|9to1-max:'.$_GET['9_to_1_max'].'|9to1-min:'.$_GET['9_to_1_min'].'|6to1-max:'.$_GET['6_to_1_max'].'|6to1-min:'.$_GET['6_to_1_min'].'|2to1-max:'.$_GET['2_to_1_max'].'|2to1-min:'.$_GET['2_to_1_min'].'|1to1-max:'.$_GET['1_to_1_max'].'|1to1-min:'.$_GET['1_to_1_min'].']</b>';
			$postLog->save();
		}
	}
	
	public function deleteRouletteLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get European Roulette table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" .$_GET['roulette_limit_id']. "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		
		list($allmax,$allmin,$max35,$min35,$max18,$min18,$max12,$min12,$max9,$min9,$max6,$min6,$max2,$min2,$max1,$min1)=split('#',$result);
		//--------------------------------------------------------------------
		TableGameLimit::model()->updateAll(array(
				'unused_table_limit'=>1),
				'id="'.$_GET['roulette_limit_id'].'"');
			
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> DELETE EURO-roulette table limit [All-max:'.$allmax.'|All-min:'.$allmin.'|35to1-max:'.$max35.'|35to1-min:'.$min35.'|18to1-max:'.$max18.'|18to1-min:'.$min18.'|12to1-max:'.$max12.'|12to1-min:'.$min12.'|9to1-max:'.$max9.'|9to1-min:'.$min9.'|6to1-max:'.$max6.'|6to1-min:'.$min6.'|2to1-max:'.$max2.'|2to1-min:'.$min2.'|1to1-max:'.$max1.'|1to1-min:'.$min1.']</b>';
		$postLog->save();
	
	}
	
	public function addNewBlackjackLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['pair_max']."#".$_GET['pair_min']."#".$_GET['rummy_max']."#".$_GET['rummy_min']."#".$_GET['insurance_max']."#".$_GET['insurance_min']."#";
		
		$post=new TableGameLimit();
		$post->game_type=3;
		$post->currency_id=$_GET['currency_id'];
		$post->limit=$limit;
		$post->creator_id=Yii::app()->session['id'];
		$post->created_date=new CDbExpression('NOW()');;
		$post->sort_id=$_GET['all_min'].$_GET['all_max'];
		$post->save();
		
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ADDED blackjack table limit [All-max:'.$_GET['all_max'].'|All-min:'.$_GET['all_min'].'|Pair-max:'.$_GET['pair_max'].'|Pair-main:'.$_GET['pair_min'].'|Rummy-max:'.$_GET['rummy_max'].'|Rummy-min:'.$_GET['rummy_min'].'|Insurance-max:'.$_GET['insurance_max'].'|Insurance-min:'.$_GET['insurance_min'].']</b>';
		$postLog->save();
	
		$rd=TableGameLimit::model()->find(array('select'=>'id','order'=>'id DESC','limit'=>1));
		echo $rd['id'];
	}
	public function updateBlackjackLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get blackjack table limit- Added by Allan |date:2012-06-28
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" . $_GET['blackjack_limit_id'] . "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		list($allmax,$allmin,$rummymax,$rummymin,$insmax,$insmin,$pairmax,$pairmin)=split('#',$result);
		//-------------------------------------------------------------------
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['pair_max']."#".$_GET['pair_min']."#".$_GET['rummy_max']."#".$_GET['rummy_min']."#".$_GET['insurance_max']."#".$_GET['insurance_min']."#";	
		TableGameLimit::model()->updateAll(array(
				'limit'=>$limit,
				'creator_id'=>Yii::app()->session['id'],
				'sort_id'=>$_GET['all_min'].$_GET['all_max'],
		),
				'id="'.$_GET['blackjack_limit_id'].'"');
		if ($result!=$limit){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Clubvegas999';
			$postLog->operated_level='Main Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=18;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE blackjack table limit from:[All-max:'.$allmax.'|All-min:'.$allmin.'|Pair-max:'.$pairmax.'|Pair-main:'.$pairmin.'|Rummy-max:'.$rummymax.'|Rummy-min:'.$rummymin.'|Insurance-max:'.$insmax.'|Insurance-min:'.$insmin.']to[All-max:'.$_GET['all_max'].'|All-min:'.$_GET['all_min'].'|Pair-max:'.$_GET['pair_max'].'|Pair-main:'.$_GET['pair_min'].'|Rummy-max:'.$_GET['rummy_max'].'|Rummy-min:'.$_GET['rummy_min'].'|Insurance-max:'.$_GET['insurance_max'].'|Insurance-min:'.$_GET['insurance_min'].']</b>';
			$postLog->save();
		}
	}
	
	public function deleteBlackjackLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get Blackjack table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" .$_GET['blackjack_limit_id']. "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		
		list($allmax,$allmin,$rummymax,$rummymin,$insmax,$insmin,$pairmax,$pairmin)=split('#',$result);
		//-------------------------------------------------------------------
		TableGameLimit::model()->updateAll(array(
				'unused_table_limit'=>1),
				'id="'.$_GET['blackjack_limit_id'].'"');
			
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> DELETE blackjack table limit [All-max:'.$allmax.'|All-min:'.$allmin.'|Pair-max:'.$pairmax.'|Pair-main:'.$pairmin.'|Rummy-max:'.$rummymax.'|Rummy-min:'.$rummymin.'|Insurance-max:'.$insmax.'|Insurance-min:'.$insmin.']</b>';
		$postLog->save();
	
	}
	
	public function addNewAmericanRouletteLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['35_to_1_max']."#".$_GET['35_to_1_min']."#".$_GET['18_to_1_max']."#".$_GET['18_to_1_min']."#".$_GET['12_to_1_max']."#".$_GET['12_to_1_min']."#".$_GET['9_to_1_max']."#".$_GET['9_to_1_min']."#".$_GET['6_to_1_max']."#".$_GET['6_to_1_min']."#".$_GET['2_to_1_max']."#".$_GET['2_to_1_min']."#".$_GET['1_to_1_max']."#".$_GET['1_to_1_min']."#";
		
		$post=new TableGameLimit();
		$post->game_type=5;
		$post->currency_id=$_GET['currency_id'];
		$post->limit=$limit;
		$post->creator_id=Yii::app()->session['id'];
		$post->created_date=new CDbExpression('NOW()');;
		$post->sort_id=$_GET['35_to_1_min'].$_GET['35_to_1_max'];
		$post->save();
		
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ADDED AME-roulette table limit [All-max:'.$_GET['all_max'].'|All-min:'.$_GET['all_min'].'|35to1-max:'.$_GET['35_to_1_max'].'|35to1-min:'.$_GET['35_to_1_min'].'|18to1-max:'.$_GET['18_to_1_max'].'|18to1-min:'.$_GET['18_to_1_min'].'|12to1-max:'.$_GET['12_to_1_max'].'|12to1-min:'.$_GET['12_to_1_min'].'|9to1-max:'.$_GET['9_to_1_max'].'|9to1-min:'.$_GET['9_to_1_min'].'|6to1-max:'.$_GET['6_to_1_max'].'|6to1-min:'.$_GET['6_to_1_min'].'|2to1-max:'.$_GET['2_to_1_max'].'|2to1-min:'.$_GET['2_to_1_min'].'|1to1-max:'.$_GET['1_to_1_max'].'|1to1-min:'.$_GET['1_to_1_min'].']</b>';
		$postLog->save();
	
		$rd=TableGameLimit::model()->find(array('select'=>'id','order'=>'id DESC','limit'=>1));
		echo $rd['id'];
	}
	public function updateAmericanRouletteLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get roullete table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" . $_GET['roulette_limit_id'] . "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		list($allmax,$allmin,$max35,$min35,$max18,$min18,$max12,$min12,$max9,$min9,$max6,$min6,$max2,$min2,$max1,$min1)=split('#',$result);
		//--------------------------------------------------------------------
		
		$limit=$_GET['all_max']."#".$_GET['all_min']."#".$_GET['35_to_1_max']."#".$_GET['35_to_1_min']."#".$_GET['18_to_1_max']."#".$_GET['18_to_1_min']."#".$_GET['12_to_1_max']."#".$_GET['12_to_1_min']."#".$_GET['9_to_1_max']."#".$_GET['9_to_1_min']."#".$_GET['6_to_1_max']."#".$_GET['6_to_1_min']."#".$_GET['2_to_1_max']."#".$_GET['2_to_1_min']."#".$_GET['1_to_1_max']."#".$_GET['1_to_1_min']."#";			
		TableGameLimit::model()->updateAll(array(
				'limit'=>$limit,
				'creator_id'=>Yii::app()->session['id'],
				'sort_id'=>$_GET['35_to_1_min'].$_GET['35_to_1_max'],
		),
				'id="'.$_GET['roulette_limit_id'].'"');
		
		if ($result!=$limit){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Clubvegas999';
			$postLog->operated_level='Main Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=18;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE AME-roulette table limit from:[35to1-max:'.$max35.'|35to1-min:'.$min35.'|18to1-max:'.$max18.'|18to1-min:'.$min18.'|12to1-max:'.$max12.'|12to1-min:'.$min12.'|9to1-max:'.$max9.'|9to1-min:'.$min9.'|6to1-max:'.$max6.'|6to1-min:'.$min6.'|2to1-max:'.$max2.'|2to1-min:'.$min2.'|1to1-max:'.$max1.'|1to1-min:'.$min1.']to:[35to1-max:'.$_GET['35_to_1_max'].'|35to1-min:'.$_GET['35_to_1_min'].'|18to1-max:'.$_GET['18_to_1_max'].'|18to1-min:'.$_GET['18_to_1_min'].'|12to1-max:'.$_GET['12_to_1_max'].'|12to1-min:'.$_GET['12_to_1_min'].'|9to1-max:'.$_GET['9_to_1_max'].'|9to1-min:'.$_GET['9_to_1_min'].'|6to1-max:'.$_GET['6_to_1_max'].'|6to1-min:'.$_GET['6_to_1_min'].'|2to1-max:'.$_GET['2_to_1_max'].'|2to1-min:'.$_GET['2_to_1_min'].'|1to1-max:'.$_GET['1_to_1_max'].'|1to1-min:'.$_GET['1_to_1_min'].']</b>';
			$postLog->save();
		}
		
	}
	public function deleteAmericanRouletteLimit()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		//Get Blackjack table limit- Added by Allan |date:2012-06-29
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_game_table_limit  WHERE id='" .$_GET['roulette_limit_id']. "'");
		$rd=$command->queryRow();
		$result=$rd['limit'];
		
		list($allmax,$allmin,$max35,$min35,$max18,$min18,$max12,$min12,$max9,$min9,$max6,$min6,$max2,$min2,$max1,$min1)=split('#',$result);
		//--------------------------------------------------------------------
		TableGameLimit::model()->updateAll(array(
				'unused_table_limit'=>1),
				'id="'.$_GET['roulette_limit_id'].'"');
			
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Clubvegas999';
		$postLog->operated_level='Main Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=18;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> DELETE AME-roulette table limit from:[35to1-max:'.$max35.'|35to1-min:'.$min35.'|18to1-max:'.$max18.'|18to1-min:'.$min18.'|12to1-max:'.$max12.'|12to1-min:'.$min12.'|9to1-max:'.$max9.'|9to1-min:'.$min9.'|6to1-max:'.$max6.'|6to1-min:'.$min6.'|2to1-max:'.$max2.'|2to1-min:'.$min2.'|1to1-max:'.$max1.'|1to1-min:'.$min1.']</b>';
		$postLog->save();
	
	}
}