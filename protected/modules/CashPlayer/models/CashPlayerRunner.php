<?php
/**
 * @todo CashPlayerRunner Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerRunner
{
	public function makeNewRunner()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['task']=='newRunner')
		{
			$criteria=new CDbCriteria;
			$criteria->select='runner_name';  // only select the 'title' column
			$criteria->condition='runner_name=:runner_name';
			$criteria->params=array(':runner_name'=>$_GET['runnerName']);
			$n=TableRunner::model()->count($criteria);
				
			if ($n==0){
				$post1=new TableRunner;
				$post1->runner_name=$_GET['runnerName'];
				$post1->contact_phone=$_GET['runnerContactPhone'];
				$post1->email=$_GET['runnerEmail'];
				$post1->runner_address=$_GET['runnerAddress'];
				$post1->zip_code=$_GET['runnerZipCode'];
				$post1->city=$_GET['runnerCity'];
				$post1->country=$_GET['runnerCountry'];
				$post1->save();
				echo 1;
			}
			else{
				echo 0;
			}
		}
	}
}