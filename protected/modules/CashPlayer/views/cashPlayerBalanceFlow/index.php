<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/button.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('mnu_cash_player').style.color="#5A0000";
	document.getElementById('mnu_cash_player').style.fontWeight="bold";
</script>

<script type="text/javascript">
function tableTransHistoryFund(accountId) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'listlog'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
	$(document).ready(function() {
	jQuery("#listlog").jqGrid({ 
		url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerBalanceFlow/CashPlayerBalanceFlow&id='+accountId, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['Transaction Type','Amount','balance_before','balance_after','Submit Time'],
	    colModel: [
			{name: 'transNumber', index: 'transNumber', width: 140, search:true,title:false,formatter:typeFormatter},
			{name: 'Amount', index: 'Amount', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'balance_before', index: 'balance_before', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'balance_after', index: 'balance_after', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'submitTime', index: 'submitTime', width: 125,title:false},
	    ],
	    loadtext:"",
	    loadComplete: function() {
			var grid=jQuery("#listlog");
	        var i=0; 
		    for(i=0;i<=grid.getGridParam("reccount");i++)
		    {
		    	var myrow = grid.jqGrid('getRowData', i);
		    	grid.jqGrid('setCell',i,"balance_before","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"balance_after","",{background:'#cfe5fa'});
			}   
		    $("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    },
	    rownumbers:true,
	    rowNum: 20,	
	    rowList: [20, 50, 100,200,500,99999],
	    pager: '#pager2',
	    sortname: 'submitTime',
	    sortorder: 'DESC',
	    caption: '<input type="button" id="btnBack" value="Back" onclick="javascript:window.history.back();">',
	    viewrecords: true
	});
	
	$('#listlog').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });

	function typeFormatter(cellvalue, options, rowObject) {
        $("cellvalue").val(cellvalue);
        if (cellvalue==1){
        	return '<label style="color:green">Automatic Deposit</label>';
        }else if (cellvalue==2){
        	return '<label style="color:blue">Manual Deposit</label>';
	    }else if (cellvalue==3){
        	return '<label style="color:red">Automatic Withdraw</label>';
	    }else if (cellvalue==4){
        	return '<label style="color:red">Manual Withdraw</label>';
	    }else{
		    return cellvalue;
		}
	};
	
	});
}
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	});

	function btnclick()
	{
		var accountId=document.getElementById('txtAccountID').value;
		tableTransHistoryFund(accountId);
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/3d-loader.gif");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript: btnclick();">
<div><b><?php echo $_GET["Account_ID"]; ?> Balance Flow</b></div>
<input style=" width: 30px" type="text" value="<?php echo $_GET["Account_ID"]; ?>" hidden="True" id="txtAccountID">
</br>
<div id="qry_result"></div>
<div id="pager2"></div>
<br/>

</body>
</html>
