<?php
/**
 * @todo CashPlayerGetAccountController
 * @copyright CE
 * @author Allan Bicol
 * @since 2012-07-14
 */
class CashPlayerGetAccountController extends MyController
{
	public function actionCashPlayerGetAccountProcess()
	{
		$cpd = new CashPlayerGetAccount();
		$cpd->makeWithdrawAndBonus();
	}
}
