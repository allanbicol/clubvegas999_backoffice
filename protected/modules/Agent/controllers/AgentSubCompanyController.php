<?php
class AgentSubCompanyController extends MyController
{

	public function actionIndex()
	{
		$account_type=Yii::app()->session['level'];
		$account_id=Yii::app()->session['account_id'];
		
		if (Yii::app()->user->checkAccess('agent.readSubCompanyList'))
		{
			header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Agent/AgentSubCompanyList' ) ;
		}
		elseif (Yii::app()->session['level']=='SC')
		{
			header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Agent/AgentSeniorMaster&account_id='.$account_id.'&type='.$account_type ) ;
		}
		elseif (Yii::app()->session['level']=='SMA')
		{
			header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Agent/AgentMaster&account_id='.$account_id.'&type='.$account_type ) ;
		}
		elseif (Yii::app()->session['level']=='MA')
		{
			header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Agent/Agent&account_id='.$account_id.'&type='.$account_type ) ;
		}
		elseif (Yii::app()->session['level']=='AGT')
		{
			header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Agent/AgentMember&account_id='.$account_id.'&type='.$account_type ) ;
		}
	}


}