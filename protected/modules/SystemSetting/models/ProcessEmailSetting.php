<?php
/**
 * @todo ProcessEmailSetting Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-17
 */
class ProcessEmailSetting
{
	
	public function getSignUpEmailList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT id,email_desc from tbl_email_setting_new where email_type=1 GROUP BY email_desc ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}

	public function getCountSignUpEmailList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) from tbl_email_setting_new where email_type=1 GROUP BY email_desc");
		$rows = $command->query();
		return $rows;
	}
	
    /**
     * @todo getEmailSetting
     * @copyright CE
     * @author Leo karl
     * @since 2012-05-17
     */
	public function getEmailSetting()
	{
		//$rd=EmailSetting::model()->find(array('select'=>'*','condition'=>'content_language=:content_language AND email_setting_type=:email_setting_type','params'=>array(':content_language'=>1),':email_setting_type'=>1));
		$rd=EmailSetting::model()->find(array('select'=>'*','condition'=>'content_language=:content_language AND email_setting_type=:email_setting_type','params'=>array(':content_language'=>$_GET['emailChoseLanguage'],':email_setting_type'=>$_GET['email_setting_type']),));
		echo $rd['email_subject'] . ' ;;; ' . $rd['content1'] . ' ;;; ' . $rd['content2'] . ' ;;; ' . $rd['content3'];
	}
	
	/**
	 * @todo saveEmailSetting
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-17
	 */
	public function saveEmailSetting()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		//Get previous email setting- Added by Allan |date:2012-06-30
		$command = $connection->createCommand("SELECT * from tbl_email_setting WHERE content_language='" . $_GET['emailChoseLanguage'] . "' and email_setting_type='".$_GET['email_setting_type']."' ");
		$rd=$command->queryRow();
		$subject=$rd['email_subject'];
		$content1=$rd['content1'];
		$content2=$rd['content2'];
		$content3=$rd['content3'];
		$language=$rd['content_language'];
		$emailType=$rd['email_setting_type'];
		
		if ($language==1){
			$language_name="English";
		}elseif ($language==2){
			$language_name="Chinese1";
		}elseif ($language==3){
			$language_name="Chinese2";
		}elseif ($language==4){
			$language_name="Vietnamese";
		}elseif ($language==5){
			$language_name="Thailand";
		}
		
		if ($emailType==1){
			$emailTypeName="Registration";
		}else{$emailTypeName="Reset Password";}
			
		$post=EmailSetting::model()->find(array('select'=>'*','condition'=>'content_language=:content_language AND email_setting_type=:email_setting_type','params'=>array(':content_language'=>$_GET['emailChoseLanguage'],':email_setting_type'=>$_GET['email_setting_type']),));
		$post->email_subject=$_GET['email_subject'];
		$post->content1=$_GET['content1'];
		$post->content2=$_GET['content2'];
		$post->content3=$_GET['content3'];
		$post->save();
		
		if ($subject!=$_GET['email_subject']){
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Company';
		$postLog->operated_level='Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=16;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE email subject for:<label style=\"color:red\">'.$emailTypeName.'</label> in <label style=\"color:red\">'.$language_name.'</label> language.</b>';
		$postLog->save();
		}

		if ($content1!=$_GET['content1']){
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Company';
		$postLog->operated_level='Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=16;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Content1 for:<label style=\"color:red\">'.$emailTypeName.'</label> in <label style=\"color:red\">'.$language_name.'</label> language.</b>';
		$postLog->save();
		}
		
		if ($content2!=$_GET['content2']){
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Company';
		$postLog->operated_level='Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=16;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Content2 for:<label style=\"color:red\">'.$emailTypeName.'</label> in <label style=\"color:red\">'.$language_name.'</label> language.</b>';
		$postLog->save();
		}
		
		if ($content3!=$_GET['content3']){
		$postLog=new TableLog;
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated='Company';
		$postLog->operated_level='Company';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=16;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Content3 for:<label style=\"color:red\">'.$emailTypeName.'</label> in <label style=\"color:red\">'.$language_name.'</label> language.</b>';
		$postLog->save();
		}
		
		echo 'Email Setting successfully saved.';
	}
	
	//Allan bicol
	public function saveEmailSettingNew()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		
		$command = $connection->createCommand("SELECT * from tbl_email_setting_new WHERE email_type='" . $_POST["emailType"] . "' and email_desc='".$_POST["desc"]."' and lang_id='".$_POST["lang"]."' ");
		$rd=$command->queryRow();
		$content=$rd['email_detail'];
		
		$command1 = $connection->createCommand("SELECT * from tbl_language WHERE id='" . $_POST["lang"] . "'");
		$rd1=$command1->queryRow();
		$lang=$rd1['language_code'];
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('email_type='.$_POST["emailType"].' and email_desc="'.$_POST["desc"].'" and lang_id='.$_POST["lang"]);
		EmailSetting::model()->updateAll(array('email_detail'=>$_POST['content']),$criteria);
		
		
		
		if ($content!=$_POST['content']){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Company';
			$postLog->operated_level='Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=31;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Content for:<label style=\"color:red\">'.$_POST["desc"].'</label> in <label style=\"color:red\">'.$lang.'</label> language.</b>';
			$postLog->save();
		}
		
		
		echo 'Content Saved!';
	}
}