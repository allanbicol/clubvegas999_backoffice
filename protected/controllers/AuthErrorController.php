<?php
/**
 * 
 * @author Kimny MOUK
 * @since	2012-10-08
 * @todo	To display the unauthorize page.
 *
 */
Class AuthErrorController extends MyController
{
	
	// should use the server ip to prevent calling it
	public function actionIndex()
	{
		$this->render("index");
	}
}