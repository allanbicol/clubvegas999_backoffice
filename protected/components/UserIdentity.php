<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    
    /**
     * @author  Kimny MOUK
     * @date    2012-03-22
     * 
     */
	const ERROR_DISABLE_ACCOUNT = 5;
	const ERROR_ACCOUNT_ALREADY_LOGIN = 6;
	const ERROR_WHITE_LIST = 7;
	const ERROR_ACCOUNT_LOCKED = 8;
	/**
	 * (non-PHPdoc)
	 * @see CUserIdentity::authenticate()
	 */
    public function authenticate() {
    	// 
    	$backendUser = BackendUser::getBackendUsers($this->username, md5($this->password), $_SERVER['REMOTE_ADDR']);
    	
    	// 
    	if ($backendUser[0]['@po_authentication'] == 1) {
	        //$backendUser = BackendUser::model()->findByAttributes(array('account_id'=>$this->username));
	        //echo CV999Utility::generateOneWayPassword($this->password);

	    	//if ($backendUser['account_id'] != null) {
	    		/*
	    		$userSessionModel = new UserLoginSession();
	    		$dbUserSession = $userSessionModel->count('account_id=:account_id', array(':account_id'=>$this->username));
	    		$memCacheSession = Yii::app()->cache->get($this->username);
	    		
	    		if ($memCacheSession == 1 ) 
	    		{
	    			$this->errorCode=self::ERROR_ACCOUNT_ALREADY_LOGIN;
	    
	    		}
	    		else if ($memCacheSession == 0 && $dbUserSession >= 1)
	    		{
	    			// Remove the previouse login session
	    			$criteria=new CDbCriteria;
	    			$criteria->condition='account_id=:account_id';
	    			$criteria->params=array(':account_id'=>$this->username);
	    			$userSessionModel->deleteAll($criteria);
	    			$this->errorCode=self::ERROR_NONE;
	    		}
	    		else*/
	    		if ($backendUser[0]['@po_status'] != 1) {
	    			$this->errorCode=self::ERROR_DISABLE_ACCOUNT;
	    		} else {
	    			if ($backendUser[0]['@po_is_allowed'] == 0) {
	    				$this->errorCode = self::ERROR_WHITE_LIST;
	    			} else {
	    				$this->errorCode=self::ERROR_NONE;
	    			}
	    		}
	    	//}
 	
    	}

    	return !$this->errorCode;
    }
}