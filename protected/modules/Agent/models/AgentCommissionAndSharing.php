<?php

/**
 * @todo AgentCommissionAndSharing Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-06-27
 */
Class AgentCommissionAndSharing
{ 
   /**
    * @todo getAgentCommissionAndSharing
    * @copyright CE
    * @author Leo karl
    * @since 2012-06-27
    */
	public function getAgentCommissionAndSharing()
	{
		$rd = GameMaxSharingAndCommission::model()->find("id = 1");
		$autoBaccaratComList='';
		for($i=0;$i<=$rd['baccarat_max_commission'];$i+=0.1){
				$autoBaccaratComList.=number_format($i, 1, '.','') .',';
		}
		$autoRouletteComList='';
		for($i=0;$i<=$rd['roulette_max_commission'];$i+=0.1){
			$autoRouletteComList.=number_format($i, 1, '.','') .',';
		}
		$autoDragonTigerComList='';
		for($i=0;$i<=$rd['dragon_tiger_max_commission'];$i+=0.1){
			$autoDragonTigerComList.=number_format($i, 1, '.','') .',';
		}
		
		$autoBaccaratSharingList='';
		for($i=0;$i<=$rd['baccarat_max_sharing'];$i+=0.1){
			$autoBaccaratSharingList.=number_format($i, 1, '.','') .',';
		}
		$autoRouletteSharingList='';
		for($i=0;$i<=$rd['roulette_max_sharing'];$i+=0.1){
			$autoRouletteSharingList.=number_format($i, 1, '.','') .',';
		}
		$autoDragonTigerSharingList='';
		for($i=0;$i<=$rd['dragon_tiger_max_sharing'];$i+=0.1){
			$autoDragonTigerSharingList.=number_format($i, 1, '.','') .',';
		}
		
		$autoSlotsSharingList='';
		for($i=0;$i<=$rd['blackjack_max_sharing'];$i+=0.1){
			$autoSlotsSharingList.=number_format($i, 1, '.','') .',';
		}
		$autoSlotsCommissionList='';
		for($i=0;$i<=$rd['blackjack_max_commission'];$i+=0.1){
			$autoSlotsCommissionList.=number_format($i, 1, '.','') .',';
		}
		
		$autoAmericanRouletteSharingList='';
		for($i=0;$i<=$rd['american_roulette_max_sharing'];$i+=0.1){
			$autoAmericanRouletteSharingList.=number_format($i, 1, '.','') .',';
		}
		$autoAmericanRouletteCommissionList='';
		for($i=0;$i<=$rd['american_roulette_max_commission'];$i+=0.1){
			$autoAmericanRouletteCommissionList.=number_format($i, 1, '.','') .',';
		}
		$autoSlotSharingList='';
		for($i=0;$i<=$rd['slot_max_sharing'];$i+=0.1){
			$autoSlotSharingList.=number_format($i, 1, '.','') .',';
		}
		$autoSlotCommissionList='';
		for($i=0;$i<=$rd['slot_max_commission'];$i+=0.1){
			$autoSlotCommissionList.=number_format($i, 1, '.','') .',';
		}
		echo $autoBaccaratComList . ';' . $autoRouletteComList . ';' . $autoDragonTigerComList . ';' . $autoBaccaratSharingList . ';' . $autoRouletteSharingList . ';' . $autoDragonTigerSharingList . ';' . $autoSlotsSharingList . ';' . $autoSlotsCommissionList . ';' . $autoAmericanRouletteSharingList . ';' . $autoAmericanRouletteCommissionList . ';' . $autoSlotSharingList . ';' . $autoSlotCommissionList;
	}
}