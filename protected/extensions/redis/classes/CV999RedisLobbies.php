<?php
/**
 * @TODO store all variables CV999
 * @copyright Creative Entertainment Ltd.
 * @author sitthykun
 * @since 2012-04-04
 */

class CV999RedisLobbies extends CV999RedisMain implements ICV999RedisCore {
	private $casinoID		= '';
	private $expiration		= 45; // second

	/**
	 * 
	 */
	function CV999RedisLobbies($expiration = NULL) {
		// call super class
		parent::CV999RedisMain(parent::LOBBY);

		// set expiration 
		if ($expiration != NULL) {
			$this->setExpiration($expiration);
		} else {
			$this->setExpiration($this->expiration);
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see ICV999RedisCore::equalTo()
	 */
	public function equalTo($value) {
		return ($this->redis->get($this->getKey()) == $value) ? TRUE : FALSE;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see ICV999RedisCore::getCoreKey()
	 */
	public function getCoreKey() {
		return $this->getKey();
	}

	/**
	 * (non-PHPdoc)
	 * @see ICV999RedisCore::isExisting()
	 */
	public function isExisting($playerTypeID = NULL, $playerName = NULL, $casinoID = NULL) {
		// validate
		if ($playerTypeID == NULL && $playerName == NULL) {
			return ($this->redis->exists($this->getKey()) > 0 ? TRUE : FALSE);
		}

		// key temp
		$keyTemp = $this->generateKeyTemp(array($casinoID, $playerTypeID, $playerName));
		return ($this->redis->exists($keyTemp) > 0 ? TRUE : FALSE);
		// return $this->redis->exists($keyTemp);
	}

	/**
	 * 
	 * @param string $playerTypeID
	 * @param string $playerName
	 * @param string $casinoID
	 */
	public function isOnLobby($playerTypeID = NULL, $playerName = NULL) {
		return $this->isExistingKeys($playerTypeID, $playerName);
	}

	/**
	 * (non-PHPdoc)
	 * @see ICV999RedisCore::length()
	 */
	public function length() {
		return $this->redis->strlen($this->getKey());	
	}
	
	/**
	 * 
	 * @param integer $casinoID
	 */
	public function setCasinoID($casinoID) {
		$this->casinoID = $casinoID;
	}

	/**
	 *
	 * @param string $playerName
	 */
	public function setPlayerName($playerName) {
		$this->playerName = $playerName;
	}

	/**
	 * 
	 * @param integer $playerTypeID
	 */
	public function setPlayerTypeID($playerTypeID) {
		$this->playerTypeID = $playerTypeID;
	}

}


?>