<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agenttransactionhistory.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		//active menu color
		document.getElementById('agentHeader').className="start active";
		document.getElementById('mnu_agent_list_info').className="active";
		$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><li><a href='#'>Sub Company List</a></li>");
	</script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		    jQuery("#dtFrom").datepicker({
		        changeMonth: true,
		        changeYear: true,
		        dateFormat: "yy-mm-dd"
		        //dateFormat: "dd-mm-yy"
		    });
		
		    jQuery("#dtTo").datepicker({
		        changeMonth: true,
		        changeYear: true,
		        dateFormat: "yy-mm-dd"
		    });
		});
		function tableLog(logType,operation,id,dtfrom,dtto) { 
			document.getElementById('qry_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSubCompanyLog/AgentSubCompanyLog&lt='+ logType +'&dtfrom='+ dtfrom + '&dtto=' + dtto +'&op='+ operation +'&id='+ id, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['<?php echo Yii::t('agent','agent.subcompanylog.operatedby');?>','<?php echo Yii::t('agent','agent.subcompanylog.level');?>','<?php echo Yii::t('agent','agent.subcompanylog.operated');?>','<?php echo Yii::t('agent','agent.subcompanylog.level');?>','<?php echo Yii::t('agent','agent.subcompanylog.operationtime');?>','<?php echo Yii::t('agent','agent.subcompanylog.details');?>'],
				    colModel: [
				      {name: 'operated_by', index: 'operated_by', width: 100, sortable: false,title:false},
				      {name: 'operated_by_level', index: 'operated_by_level', width: 100, sortable: false,title:false},
				      {name: 'operated', index: 'operated', width: 100, sortable: false,title:false},
				      {name: 'operated_level', index: 'operated_level', width: 100, sortable: true,title:false},
				      {name: 'operation_time', index: 'operation_time', width: 125, sortable: true,title:false},
				      {name: 'log_details', index: 'log_details', width: 950, sortable: false,title:false},
				      
				    ],
				    loadtext:"",
				    loadComplete: function() {
				        var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	grid.jqGrid('setCell',i,"log_details","",{background:'#f7f7c8'});
					    	grid.jqGrid('setCell',i,"operated_by","",{'font-weight':'bold'});
							grid.jqGrid('setCell',i,"operated_by","",{color:'#754719'});	
						}   
					  //grid stripe
					    $("tr.jqgrow:odd").css("background", "#DDDDDC");
					    $("tr.jqgrow:even").css("background", "#ffffff");
				    },
				    rowNum: 25,	
				    rowList: [25, 50, 75,100],
                    rownumbers: true,
                    sortable: true,
				    sortname: 'operation_time',
				    sortorder: 'DESC',
				    pager:'pager1',
				    caption: '<b><?php echo Yii::t('agent','agent.subcompanylog.logs');?></b> <input type="text" name="" id="txtDate" readonly="true">',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false});
			});
		} 
		
		function submitComplete()
		{
			//alert(obj.name);
			var dtfrom=(document.getElementById('dtFrom').value).replace("-","/").replace("-","/") + " " + document.getElementById('cmbTime1').value + ":00:00";
			var dtto=(document.getElementById('dtTo').value).replace("-","/").replace("-","/") + " " + document.getElementById('cmbTime2').value + ":59:59";
			tableLog(document.getElementById('cmbLogType').value,document.getElementById('cmbOperation').value,document.getElementById('txtAccountID').value,dtfrom,dtto);
			document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
			document.getElementById('btnSubmit').name=document.getElementById('cmbLogType').value;
			
		}
		
		function return_to_main()
		{
			var str_date=document.getElementById('txtDDate').value.split(" >> ");
			tableLog(document.getElementById('btnSubmit').name,str_date[0],str_date[1]);
			document.getElementById('txtDate').value=str_date[0] + " >> " + str_date[1];
			
		}
	</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
.ui-jqgrid tr.jqgrow td {
        word-wrap: break-word; /* IE 5.5+ and CSS3 */
        white-space: pre-wrap; /* CSS3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        overflow: hidden;
        height: auto;
        vertical-align: middle;
        padding-top: 3px;
        padding-bottom: 3px
    }
</style>
</head>
<body onload="javascript: submitComplete();">
<div id="body_wraper">
	<div id="transaction_parameter_box1">
		<div class="header"><?php echo Yii::t('agent','agent.subcompanylog.accountid');?> <font color="#f0e62b"><?php echo $_GET['account_id'];?></font>: <?php echo Yii::t('agent','agent.subcompanylog.logs');?></div>
		<div class="body">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left"><?php echo Yii::t('agent','agent.subcompanylist.type');?></td>
			<td class="right">
				<Select name="cmbLogType" id="cmbLogType">
					<option value="0">All</option>
					<?php 
						$dataReader = TableLogType::model()->findAll('agent_log=:agentLog',array(':agentLog'=>1));
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['log_type_name']) . '</option>';
						}
					?>
				</Select>
			</td></tr>
			<tr><td class="left"><?php echo $_GET['account_id'];?></td>
			<td class="right">
				<Select name="cmbOperation" id="cmbOperation">
					<option value="1">Operated</option>
					<option value="0">Operating</option>
				</Select>
			</td></tr>
			<tr><td class="left"><?php echo Yii::t('agent','agent.transHistory.from');?></td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtFrom" name="dtFrom" class="dtText">
				<select id="cmbTime1">
				<?php 
					for($i=0;$i<=23;$i++){
						echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
					}
				?>
				</select> :00:00
			</td></tr>
			<tr><td class="left">
				<?php echo Yii::t('agent','agent.transHistory.to');?>
			</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtTo" class="dtText">
				<select id="cmbTime2">
				<?php 
					for($i=0;$i<=23;$i++){
						if($i<23){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}else{
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					}
				?> 
				</select> :59:59
			</td>
			</tr>
		</table>
		
		
		</div>
		<div class="row">
		</div>
		<center><input id="btnSubmit" class="btn red" name="" type="button" value="<?php echo Yii::t('agent','agent.transHistory.submit');?>" onclick="javascript: submitComplete();"> <input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.subcompanylist.back');?>" onclick="javascript: history.go(-1);"></center>
		<br/>
	</div>
	<div id="qry_result">
	<div id="pager1"></div>
	</div>
	<input id="txtAccountID" name="txtAccountID" value="<?php echo $_GET['account_id'];?>" hidden>
</div>
</body>
</html>