document.getElementById('marketingHeader').className="start active";
document.getElementById('mnu_player_value').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Marketing <i class='icon-angle-right'></i></li><li><a href='#'>Player Value Report</a></li>");
	
function displayPlayerValue(){
	document.getElementById('qry_resultPlayerValue').innerHTML='';
	document.getElementById('qry_result_details').innerHTML='';
	var dateFrom = (document.getElementById('datefrom').value).split("-");
	var monthF = dateFrom[1];
	var dayF = dateFrom[2];
	var yearF = dateFrom[0];
	var dateF=(yearF + "/" + monthF + "/" + dayF);
	var dateTo =(document.getElementById('dateto').value).split("-");
	var monthT = dateTo[1];
	var dayT = dateTo[2];
	var yearT = dateTo[0];
	var dateT= (yearT + "/" + monthT + "/" +dayT);
	var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
	var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
	
	
	var dateFrom	=dateSubmitFrom;	//$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
	var dateTo	= dateSubmitTo;	//$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
	
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_resultPlayerValue").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_resultPlayerValue").appendChild(divTag1);

	jQuery("#list1").jqGrid({ 
		url: playerValueListURL+'&dateFrom='+dateFrom+'&dateTo='+dateTo, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['Account Id', 'Player Value','Value Redeem','Available Value','Player Points','Points Redeem','Available Points','Redeem Count','Operation'],
	    colModel: [
	      {name: 'account_id', index: 'account_id', width: 130, search:true,formatter:function (cellvalue, options, rowObject) {
			    var cellPrefix = '';
			    	return cellPrefix + '<a style="text-decoration:none; color:blue; font-size:12px; cursor:pointer" href="#" onclick="viewRedeemDetails(\''+cellvalue+'\');">' + cellvalue + '</a>';       
			}},
	      {name: 'player_value', index: 'player_value', width: 150, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'value_redeem', index: 'value_redeem', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'available_value', index: 'available_value', width: 150, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'player_points', index: 'player_points', width: 150, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'point_redeem', index: 'point_redeem', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'available_points', index: 'available_points', width: 150, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'redeem_count', index: 'redeem_count', width: 90, align:"right",title:false},
	      {name: 'operation', index: 'operation', width: 100, align: 'center' , sortable: false,formatter:function (cellvalue, options, rowObject) {
			    var cellPrefix = '';
		    	return cellPrefix + '<a class="btn red" href="#" onclick="redeemTrans(\''+rowObject[0]+'\',\''+rowObject[3]+'\',\''+rowObject[6]+'\');">' + cellvalue + '</a>';       
		}},
	    ],
		loadComplete:function(){
			$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    	
	    	var grid=jQuery("#list1");
	        var i=0; 
		    for(i=0;i<=grid.getGridParam("reccount");i++)
		    {
		    	var myrow = grid.jqGrid('getRowData', i);
		    	grid.jqGrid('setCell',i,"value_redeem","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"available_value","",{background:'#cfe5fa'});
		    	grid.jqGrid('setCell',i,"point_redeem","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"available_points","",{background:'#cfe5fa'});
			} 
		    
	    	var getColumnIndexByName = function(grid, columnName) {
	            var cm = grid.jqGrid('getGridParam', 'colModel'), i, l;
	            for (i = 1, l = cm.length; i < l; i += 1) {
	                if (cm[i].name === columnName) {
	                    return i; // return the index
	                }
	            }
	            return -1;
	        };
	    	
	    	var iCol = getColumnIndexByName ($(this), 'available'), rows = this.rows, i,
	        c = rows.length;
		    for (i = 1; i < c; i += 1) {
		        $(rows[i].cells[iCol]).change(function (e) {
		            var id = $(e.target).closest('tr')[0].id, isChecked = $(e.target).is(':checked');
			    	var item_id = grid.jqGrid('getCell', id, 'id');
			    	var item_name = grid.jqGrid('getCell', id, 'item_name');
			    	
		            var confirmMsg='';
		            if(isChecked==true){
		            	confirmMsg="Do you want " + item_name + " to be available as promo item? ";
			        }else{
			        	confirmMsg="Do you want " + item_name + " to be unavailable in a promo item?";
				    }
			    	var conf = confirm(confirmMsg);
			        if(conf == true){
			             jQuery.ajax({
		            			url: updateAvailableURL,
		            			type: 'POST',
		            			data: {'id':item_id,'is_checked':isChecked,'item_name':item_name},
		            			context: '',
		            			success: function(data){
		            				if(data == 'no_permission'){
		            					alert('Warning: You don\'t have permission to change Promo Item list!');
		            				}else{
		            					alert(data);	
		            				}
		            				$('#list1').trigger( 'reloadGrid' );
		            	    	}
		            	});
			        }else{
			        	$('#list1').trigger( 'reloadGrid' );
				    }
		        });
		    }
		},
	    rowNum: 20,
	    rownumbers: true,
	    rowList: [20,50, 100, 500],
	    pager: '#pager1',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Player Value List',
	    viewrecords: true
	});

}

var cRowNo=0;

function viewRedeemDetails(aElement){
	
	document.getElementById('qry_result_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result_details").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result_details").appendChild(divTag1);
	
	var grid=jQuery("#list2");
	grid.jqGrid({ 
		url: playerValueListDetailsURL + '&account_id='+aElement, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['Redeem Date', 'Item Name','Quantity','Value Redeem','Points Redeem'],
	    colModel: [
	      {name: 'redeem_date', index: 'redeem_date', width: 130, search:true},
	      {name: 'item_name', index: 'item_name', width: 150, align:"right",title:false},
	      {name: 'quantity', index: 'quantity', width: 120, align:"right",title:false},
	      {name: 'total_value', index: 'total_value', width: 150, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	      {name: 'total_points', index: 'total_points', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	    ],
		loadComplete:function(){
			$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    	var grid=jQuery("#list2");
	        var i=0; 
		    for(i=0;i<=grid.getGridParam("reccount");i++)
		    {
		    	var myrow = grid.jqGrid('getRowData', i);
		    	grid.jqGrid('setCell',i,"total_value","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"available_value","",{background:'#cfe5fa'});
		    	grid.jqGrid('setCell',i,"total_points","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"available_points","",{background:'#cfe5fa'});
			} 
		},
	    rowNum: 20,
	    rownumbers: true,
	    rowList: [20,50, 100, 500],
	    pager: '#pager2',
	    sortname: 'redeem_date',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: '<b>'+ aElement +'</> Redeem details&nbsp;&nbsp;<button onclick="document.getElementById(\'qry_result_details\').innerHTML=\'\'">Hide</button>',
	    viewrecords: true
	});

}
var $addRedeemDialog='';
$(document).ready(function() {
	$addRedeemDialog = $('<div></div>')
		.html('<div id="cs_addcurrency"><table border="0" cellpadding="0" cellspacing="0">'
				+'<tr><td class="left"><b>Account Id</b></td><td class="right"><label name="txtAccountId" id="txtAccountId"></label></td></tr>'
				+'<tr><td class="left"><b>Player Value</b></td><td class="right"><input id="optValue" type="radio" name="radRedeemType" checked="checked" value="v"><label id="lblValue"></label></td></tr>'
				+'<tr><td class="left"><b>Player Points</b></td><td class="right"><input id="optPoints" type="radio" name="radRedeemType"  value="p"><label id="lblPoints"></label></td></tr>'
				+'<tr><td class="left"><b>Quantity</b></td><td class="right"><input type="text" name="txtQty" id="txtQty"> <font color="red">*</font></td></tr>'
				+'<tr><td class="left"><b>Select Item</b></td><td class="right"><Select name="sltStock" id="sltStock" onfocus="getPromoItems();"><option>Select Item</option></select></td></tr>'
				+'</table><br/><div id="cs_footer"><input class="btn red" type="submit" id="confirm-redeem" value="  Confirm  ">&nbsp;&nbsp;<input class="btn red" type="button" id="cancel-redeem" value="  Cancel  "><br/>'
				+'<input type="text" id="cs_errMessage" readonly="true"/></div></div>')
		.dialog({
			autoOpen: false,
			width: 390,
			title: 'Redeem',
			resizable: false,
			modal: true
		});
		$('#confirm-redeem').click(function(event) {
			if (trim(document.getElementById('txtQty').value)==''){document.getElementById('cs_errMessage').value='Quantity is required!';document.getElementById('txtQty').focus(); return false;}
			if (validate(document.getElementById('txtQty').value)==false){document.getElementById('cs_errMessage').value='Invalid Quantity!';document.getElementById('txtQty').focus(); return false;}
	   		 
			if ((document.getElementById('sltStock').value)=='Select Item'){document.getElementById('cs_errMessage').value='Item name is required!';document.getElementById('sltStock').focus(); return false;}
			
			var redeemType=$('input[name="radRedeemType"]:checked').val();
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			
			jQuery.ajax({
	    		url: redeemURL,
	    		type: 'POST',
	    		data: {'item_id': document.getElementById('sltStock').value, 'qty' : document.getElementById('txtQty').value,
	    			'redeem_type': redeemType,'account_id': document.getElementById('txtAccountId').innerHTML,'dateFrom':dateSubmitFrom,'dateTo':dateSubmitTo},
	    		context: '',
	    		success: function(msg) {
	    			alert(msg);
	    			$('#list1').trigger( 'reloadGrid' );
	    			$addRedeemDialog.dialog('close');
//	    			 if (msg=='suc'){
//	    				 alert('New promo item successfully added.');
//	    				 $('#list2').trigger( 'reloadGrid' );
//	    				 $addRedeemDialog.dialog('close');
//	    			 }else{
//	    				 alert('You don\'t have permision for adding new promo item.');
//	    			 }
		    	}
	    	});
		});
		$('#cancel-redeem').click(function(event) {
			$addRedeemDialog.dialog('close');
		});
	});

function redeemTrans(id,value,points){
	$addRedeemDialog.dialog('open');
	document.getElementById('txtAccountId').innerHTML=id;
	document.getElementById('lblValue').innerHTML=parseFloat(value).toFixed(2);
	document.getElementById('lblPoints').innerHTML=parseFloat(points).toFixed(2);
}

function getPromoItems(){
	 $("#sltStock").empty();
	jQuery.ajax({
		url: itemListURL,
		type: 'POST',
		data: '',
		context: '',
		dataType: "json",
		success: function(msg) {
			$.each(msg, function () {
	            $("#sltStock").append($("<option></option>").val(this['id']).html(this['item_name']));
	        });
    	}
	});
}

function validate(text)
{
	var intval=Math.round(text*100)/100;
	var regex = /^\d{1,3}(,?\d{3})*?(.\d{2})?$/g;
	//return regex.test(intval);
	return !isNaN(parseFloat(intval)) && isFinite(intval);
}
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}