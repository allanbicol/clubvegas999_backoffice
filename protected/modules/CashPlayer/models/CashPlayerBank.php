<?php
/**
 * @todo CashPlayerBank Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerBank
{
	public function makeNewBank()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['task']=='newBank')
		{
			$criteria=new CDbCriteria;
			$criteria->select='bank_name';  // only select the 'title' column
			$criteria->condition='bank_name=:bank_name';
			$criteria->params=array(':bank_name'=>$_GET['bankName']);
			$n=TableBank::model()->count($criteria);
			
			if ($n==0){
				$post1=new TableBank;
				$post1->bank_name=$_GET['bankName'];
				$post1->bank_account=$_GET['bankAccount'];
				$post1->bank_address=$_GET['bankAddress'];
				$post1->contact_phone=$_GET['bankContactPhone'];
				$post1->zip_code=$_GET['bankZipCode'];
				$post1->city=$_GET['bankCity'];
				$post1->country=$_GET['bankCountry'];
				$post1->save();
				echo 1;
			}
			else{
				echo 0;
			}
			
			
		}
		if ($_GET['task']=='editBank')
		{
			$postEditBank=new TableBank;
			$postEditBank=TableBank::model()->findByPk($_GET['id']);
			$postEditBank->bank_name=$_GET['bankName'];
			$postEditBank->bank_account=$_GET['bankAccount'];
			$postEditBank->bank_address=$_GET['bankAddress'];
			$postEditBank->contact_phone=$_GET['bankContactPhone'];
			$postEditBank->zip_code=$_GET['bankZipCode'];
			$postEditBank->city=$_GET['bankCity'];
			$postEditBank->country=$_GET['bankCountry'];
			$postEditBank->save();
			echo 1;
		}
		if ($_GET['task']=='deleteBank')
		{
			$postDeleteBank=new TableBank;
			$postDeleteBank=TableBank::model()->DeleteByPk($_GET['id']);
			//$postDeleteBank->save();
			echo 1;
		}
	}
}