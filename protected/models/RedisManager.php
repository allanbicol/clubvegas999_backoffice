<?php
/**
 * @todo		To manage user on Redis server
 * @copyright	CE
 * @author		Kimny MOUK
 * @since	 	2012-12-04
 *
 */

//$rootPath = dirname(Yii::app()->request->scriptFile);

Yii::import('application.extensions.redis.*');
require_once('libs/Predis/Commands/ICommand.php');
require_once('libs/Predis/Commands/Command.php');
require_once('libs/Predis/Commands/IPrefixable.php');
require_once('libs/Predis/Commands/PrefixableCommand.php');
require_once('libs/Predis/Commands/KeyExists.php');
require_once('libs/Predis/Commands/KeyKeys.php');
require_once('libs/Predis/Commands/StringGet.php');
require_once('libs/Predis/Commands/ConnectionSelect.php');
require_once('libs/Predis/Commands/ConnectionAuth.php');
require_once('libs/Predis/Helpers.php');
require_once('libs/Predis/Commands/HashGetMultiple.php');
require_once('libs/Predis/Client.php');
require_once('libs/Predis/IConnectionFactory.php');
require_once('libs/Predis/IConnectionParameters.php');
require_once('libs/Predis/ConnectionParameters.php');
require_once('libs/Predis/Network/IConnection.php');
require_once('libs/Predis/Network/IConnectionSingle.php');
require_once('libs/Predis/Network/ConnectionBase.php');
require_once('libs/Predis/Network/StreamConnection.php');
require_once('libs/Predis/Options/IClientOptions.php');
require_once('libs/Predis/Options/IOption.php');
require_once('libs/Predis/Options/Option.php');
require_once('libs/Predis/Options/ClientConnectionFactory.php');
require_once('libs/Predis/Options/ClientCluster.php');
require_once('libs/Predis/Options/ClientReplication.php');
require_once('libs/Predis/Commands/Processors/IProcessingSupport.php');
require_once('libs/Predis/Profiles/IServerProfile.php');
require_once('libs/Predis/Profiles/ServerProfile.php');
require_once('libs/Predis/Profiles/ServerVersion24.php');
require_once('libs/Predis/ConnectionFactory.php');
require_once('libs/Predis/Options/ClientPrefix.php');
require_once('libs/Predis/Options/ClientProfile.php');
require_once('libs/Predis/Options/ClientOptions.php');

require_once('classes/ICV999RedisStaff.php');
require_once('classes/ICV999RedisCore.php');
require_once('classes/CV999RedisCore.php');
require_once('classes/CV999RedisMain.php');
require_once('classes/CV999RedisLobbies.php');
require_once('classes/CV999OnlinePlayersYii.php');
require_once('libs/Predis/Commands/KeyDelete.php');


class RedisManager extends CV999OnlinePlayersYii
{
	/**
	 * @todo forcePlayerLogout
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-26
	 */
	private $onlinePlayer;
	
	public function forcePlayerLogout()
	{
		//initialize
		$this->onlinePlayer = new CV999OnlinePlayersYii();
		$level_name= ($_POST['player_level']=='a') ? $level_name='Agent Player' : $level_name='Cash Player';
		$casino='';
	
		//check user permission
		if(Yii::app()->user->checkAccess('systemMonitor.writeOnlineUsers') || User::getUserType() === Constants::ROLE_AGENT){
				
			//check if online to lobby or not
			if($this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['player_id'])==0){
				// logged out from site
				$casino='ClubVegas999 site.';
				// force logout from clubvegas999 site
				$this->forceLogoutOnLoginByPlayer($_POST['player_id']);
				
			}else{
				// logged out from lobby
				$casino= $this->getCasinoNameFromRedisByPlayerID($_POST['player_id']) . ' lobby.';
				// force logout from lobby
				$this->forceLogoutOnLobbyByPlayer($_POST['player_id']);
			}
				
			//set log_detail
			$log_detail='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Forced Logout '. $level_name .' <label style=\"color:green\">'. $_POST['player_id'] .'</label> from the '. $casino .'</b>';
				
			//insert log
			$this->insertLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'],
					$_POST['player_id'], $level_name, date('Y-m-d H:i:s'), 30, $log_detail
			);
				
			//return message
			echo $_POST['player_id'] . ' has been successfully logged out from the ' . $casino;	
		}else{
			//return message
			echo 'You have no permission to this operation!';
		}
	}
	
	/**
	 * @todo getCasinoNameFromRedisByPlayerID
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-26
	 */
	public function getCasinoNameFromRedisByPlayerID($player_id){
		//initialize
		$this->onlinePlayer = new CV999OnlinePlayersYii();
	
		//check if player is logged in to the lobby or not
		if($this->onlinePlayer->isExistingOnLobbyByPlayer($player_id)==1){
	
			//if player is logged in to lobby
			return $this->getCasinoNameByID($this->onlinePlayer->getCasinoIDByPlayer($player_id));
		}else{
	
			//if player is not logged in to lobby
			return 'ClubVegas999';
		}
	}
	
	/**
	 * @todo getCasinoNameByID
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-26
	 */
	public function getCasinoNameByID($casino_id){
		$model=TableCasino::model()->find(array('select'=>'casino_name','condition'=>'id=:casino_id','params'=>array(':casino_id'=>$casino_id),));
		return $model['casino_name'];
	}
	/**
	 * @todo insertLog
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-26
	 */
	public function insertLog($operated_by,$operated_by_level,$operated,$operated_level,$date_time,$log_type_id,$log_details){
		$postLog=new TableLog;
		$postLog->operated_by=$operated_by;
		$postLog->operated_by_level=$operated_by_level;
		$postLog->operated=$operated;
		$postLog->operated_level=$operated_level;
		$postLog->operation_time=$date_time;
		$postLog->log_type_id=$log_type_id;
		$postLog->log_details=$log_details;
		$postLog->save();
	}
	
	/**
	 *
	 * @param string $playerName
	 */
	public function forceLogoutOnLobbyByPlayer($playerName) {
		// initialize
		$onlinePlayer = new CV999OnlinePlayersYii();
		$onlinePlayer->deleteLobbyByPlayer($playerName);
		$onlinePlayer = NULL;
	}
	
	/**
	 *
	 * @param string $playerName
	 */
	public function forceLogoutOnLoginByPlayer($playerName) {
		// initialize
		$onlinePlayer = new CV999OnlinePlayersYii();
		$onlinePlayer->deleteLoginByPlayer($playerName);
		$onlinePlayer = NULL;
	}
}