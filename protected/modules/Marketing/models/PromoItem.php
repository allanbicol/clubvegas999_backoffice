<?php
/**
 * @todo UserAccountList Model
* @copyright CE
* @author Leo karl
* @since 2012-04-06
*
*/
class PromoItem
{
	/**
	 * @todo getPromoItem
	 * @copyright CE
	 * @author Allan Bicol
	 * @since 2013-09-07
	 *
	 */
	public function getPromoItemList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_promo_items ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}

	/**
	 * @todo getCountCurrencyRecord
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-04-06
	 *
	 */
	public function getCountPromoItemList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_promo_items");
		$rows = $command->query();
		return $rows;
	}
}