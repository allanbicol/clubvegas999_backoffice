<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/adjustment.css" />

<b><font style="font-size: 11pt;"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.revenue_sharing_adjustment');?></font></b><br/><br/>
<div id="adjustment_header">
	
</div>
<div id="adjustment_content">
	
	<?php
		// Sharing 	
		list($vig_share_baccarat, $htv_share_baccarat, $sv_share_baccarat, $vv_share_baccarat) = explode("#",$info['share_baccarat']);
		list($vig_share_roulette, $htv_share_roulette, $sv_share_roulette, $vv_share_roulette) = explode("#",$info['share_roulette']);
		list($vig_share_dragontiger, $htv_share_dragontiger, $sv_share_dragontiger, $vv_share_dragontiger) = explode("#",$info['share_dragon_tiger']);
		list($vig_share_blackjack, $htv_share_blackjack, $sv_share_blackjack, $vv_share_blackjack) = explode("#",$info['share_blackjack']);
		list($vig_share_americanroulette, $htv_share_americanroulette, $sv_share_americanroulette, $vv_share_americanroulette) = explode("#",$info['share_american_roulette']);
		list($vig_share_slots, $htv_share_slots, $sv_share_slots, $vv_share_slots) = explode("#",$info['share_slots']);
		
		// Commission
		list($vig_comm_baccarat, $htv_comm_baccarat, $sv_comm_baccarat, $vv_comm_baccarat) = explode("#",$info['commission_baccarat']);
		list($vig_comm_roulette, $htv_comm_roulette, $sv_comm_roulette, $vv_comm_roulette) = explode("#",$info['commission_roulette']);
		list($vig_comm_dragontiger, $htv_comm_dragontiger, $sv_comm_dragontiger, $vv_comm_dragontiger) = explode("#",$info['commission_dragon_tiger']);
		list($vig_comm_blackjack, $htv_comm_blackjack, $sv_comm_blackjack, $vv_comm_blackjack) = explode("#",$info['commission_blackjack']);
		list($vig_comm_americanroulette, $htv_comm_americanroulette, $sv_comm_americanroulette, $vv_comm_americanroulette) = explode("#",$info['commission_american_roulette']);
		list($vig_comm_slots, $htv_comm_slots, $sv_comm_slots, $vv_comm_slots) = explode("#",$info['commission_slots']);
		
		
	?>
	<!-- COSTAVEGAS999 -->
	<?php if($casino_level['vig']==1){?>
		<table border="0">
		<tr>
			<td class="left"><b><font color="#9a1107">COSTAVEGAS999</font></b></td>
			<td class=""><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></b></td>
			<td class="middle"><input type="text" class="apply_all" id="txtVIGShareApplyAll" value="0">% <input type="button" class="btn mini		 red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="costaApplySharing(document.getElementById('txtVIGShareApplyAll').value);"></td>
			<td style="display:none" class="label"><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></b></td>
			<td style="display:none" class="right"><input type="text" class="apply_all" id="txtVIGCommApplyAll" value="0">% <input type="button" class="btn mini red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="costaApplyCommission(document.getElementById('txtVIGCommApplyAll').value);"></td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtVIGShareBaccarat" id="txtVIGShareBaccarat" value="<?php echo  number_format((float)$vig_share_baccarat,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_share_baccarat,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_share_baccarat_from'],2);?>% - <?php echo  ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_share_baccarat_to'],2) : number_format((float)$vig_share_baccarat,2);?>%)</td>
			<td style="display:none" td class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtVIGCommBaccarat" id="txtVIGCommBaccarat" value="<?php echo  number_format((float)$vig_comm_baccarat,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_comm_baccarat,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_comm_baccarat_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_comm_baccarat_to'],2) : number_format((float)$vig_comm_baccarat,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.european_roulette');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtVIGShareRoulette" id="txtVIGShareRoulette" value="<?php echo  number_format((float)$vig_share_roulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_share_roulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_share_roulette_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_share_e_roulette_to'],2) : number_format((float)$vig_share_roulette,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtVIGCommRoulette" id="txtVIGCommRoulette" value="<?php echo  number_format((float)$vig_comm_roulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_comm_roulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_comm_roulette_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_comm_e_roulette_to'],2) : number_format((float)$vig_comm_roulette,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.blackjack');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtVIGShareBlackjack" id="txtVIGShareBlackjack" value="<?php echo  number_format((float)$vig_share_blackjack,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_share_blackjack,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_share_blackjack_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_share_blackjack_to'],2) : number_format((float)$vig_share_blackjack,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtVIGCommBlackjack" id="txtVIGCommBlackjack" value="<?php echo  number_format((float)$vig_comm_blackjack,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_comm_blackjack,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_comm_blackjack_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_comm_blackjack_to'],2) : number_format((float)$vig_comm_blackjack,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.american_roulette');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtVIGShareAmericanRoulette" id="txtVIGShareAmericanRoulette" value="<?php echo  number_format((float)$vig_share_americanroulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_share_americanroulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_share_americanroulette_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_share_a_roulette_to'],2) : number_format((float)$vig_share_americanroulette,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtVIGCommAmericanRoulette" id="txtVIGCommAmericanRoulette" value="<?php echo  number_format((float)$vig_comm_americanroulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vig_comm_americanroulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vig_comm_americanroulette_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vig_comm_a_roulette_to'],2) : number_format((float)$vig_comm_americanroulette,2);?>%)</td>
		</tr>
		</table>
	<?php }?>
	
	
	<!-- HATIENVEGAS999 -->
	<?php if($casino_level['htv']==1){?>
		<table border="0">
		<tr>
			<td class="left"><b><font color="#9a1107">HATIENVEGAS999</font></b></td>
			<td class=""><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></b></td>
			<td class="middle"><input type="text" class="apply_all" id="txtHTVShareApplyAll" value="0">% <input type="button" class="btn mini red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="hatienApplySharing(document.getElementById('txtHTVShareApplyAll').value);"></td>
			<td style="display:none" class="label"><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></b></td>
			<td style="display:none" class="right"><input type="text" class="apply_all" id="txtHTVCommApplyAll" value="0">% <input type="button" class="btn mini red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="hatienApplyCommission(document.getElementById('txtHTVCommApplyAll').value);"></td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtHTVShareBaccarat" id="txtHTVShareBaccarat" value="<?php echo  number_format((float)$htv_share_baccarat,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_share_baccarat,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_share_baccarat_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_share_baccarat_to'],2) : number_format((float)$htv_share_baccarat,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtHTVCommBaccarat" id="txtHTVCommBaccarat" value="<?php echo  number_format((float)$htv_comm_baccarat,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_comm_baccarat,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_comm_baccarat_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_comm_baccarat_to'],2) : number_format((float)$htv_comm_baccarat,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.european_roulette');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtHTVShareRoulette" id="txtHTVShareRoulette" value="<?php echo  number_format((float)$htv_share_roulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_share_roulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_share_roulette_from'],2);?>% - <?php echo  ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_share_e_roulette_to'],2) : number_format((float)$htv_share_roulette,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtHTVCommRoulette" id="txtHTVCommRoulette" value="<?php echo  number_format((float)$htv_comm_roulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_comm_roulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_comm_roulette_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_comm_e_roulette_to'],2) : number_format((float)$htv_comm_roulette,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.dragon_tiger');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtHTVShareDragonTiger" id="txtHTVShareDragonTiger" value="<?php echo  number_format((float)$htv_share_dragontiger,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_share_dragontiger,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_share_dragontiger_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_share_dragon_tiger_to'],2) : number_format((float)$htv_share_dragontiger,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtHTVCommDragonTiger" id="txtHTVCommDragonTiger" value="<?php echo  number_format((float)$htv_comm_dragontiger,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_comm_dragontiger,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_comm_dragontiger_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_comm_dragon_tiger_to'],2) : number_format((float)$htv_comm_dragontiger,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtHTVShareSlots" id="txtHTVShareSlots" value="<?php echo  number_format((float)$htv_share_slots,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_share_slots,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_share_slots_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_share_slots_to'],2) : number_format((float)$htv_share_slots,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtHTVCommSlots" id="txtHTVCommSlots" value="<?php echo  number_format((float)$htv_comm_slots,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$htv_comm_slots,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['htv_comm_slots_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['htv_comm_slots_to'],2) : number_format((float)$htv_comm_slots,2);?>%)</td>
		</tr>
		</table>
	<?php }?>
	
	<!-- SAVANVEGAS999 -->
	<?php if($casino_level['sv']==1){?>
		<table border="0">
		<tr>
			<td class="left"><b><font color="#9a1107">SAVANVEGAS999</font></b></td>
			<td class=""><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></b></td>
			<td class="middle"><input type="text" class="apply_all" id="txtSVShareApplyAll" value="0">% <input type="button" class="btn mini red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="savanApplySharing(document.getElementById('txtSVShareApplyAll').value);"></td>
			<td style="display:none" class="label"><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></b></td>
			<td style="display:none" class="right"><input type="text" class="apply_all" id="txtSVCommApplyAll" value="0">% <input type="button" class="btn mini red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="savanApplyCommission(document.getElementById('txtSVCommApplyAll').value);"></td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtSVShareBaccarat" id="txtSVShareBaccarat" value="<?php echo  number_format((float)$sv_share_baccarat,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_share_baccarat,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_share_baccarat_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_share_baccarat_to'],2) : number_format((float)$sv_share_baccarat,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtSVCommBaccarat" id="txtSVCommBaccarat" value="<?php echo  number_format((float)$sv_comm_baccarat,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_comm_baccarat,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_comm_baccarat_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_comm_baccarat_to'],2) : number_format((float)$sv_comm_baccarat,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.european_roulette');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtSVShareRoulette" id="txtSVShareRoulette" value="<?php echo  number_format((float)$sv_share_roulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_share_roulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_share_roulette_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_share_e_roulette_to'],2) : number_format((float)$sv_share_roulette,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtSVCommRoulette" id="txtSVCommRoulette" value="<?php echo  number_format((float)$sv_comm_roulette,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_comm_roulette,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_comm_roulette_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_comm_e_roulette_to'],2) : number_format((float)$sv_comm_roulette,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.dragon_tiger');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtSVShareDragonTiger" id="txtSVShareDragonTiger" value="<?php echo  number_format((float)$sv_share_dragontiger,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_share_dragontiger,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_share_dragontiger_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_share_dragon_tiger_to'],2) : number_format((float)$sv_share_dragontiger,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtSVCommDragonTiger" id="txtSVCommDragonTiger" value="<?php echo  number_format((float)$sv_comm_dragontiger,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_comm_dragontiger,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_comm_dragontiger_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_comm_dragon_tiger_to'],2) : number_format((float)$sv_comm_dragontiger,2);?>%)</td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtSVShareSlots" id="txtSVShareSlots" value="<?php echo  number_format((float)$sv_share_slots,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_share_slots,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_share_slots_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_share_slots_to'],2) : number_format((float)$sv_share_slots,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtSVCommSlots" id="txtSVCommSlots" value="<?php echo  number_format((float)$sv_comm_slots,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$sv_comm_slots,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['sv_comm_slots_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['sv_comm_slots_to'],2) : number_format((float)$sv_comm_slots,2);?>%)</td>
		</tr>
		</table>
	<?php }?>
	
	<!-- VIRTUAVEGAS999 -->
	<?php if($casino_level['vv']==1){?>
		<table border="0">
		<tr>
			<td class="left"><b><font color="#9a1107">VIRTUAVEGAS999</font></b></td>
			<td class=""><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></b></td>
			<td class="middle"><input type="text" class="apply_all" id="txtVVShareApplyAll" value="0">% <input type="button" class="btn mini red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="virtuaApplySharing(document.getElementById('txtVVShareApplyAll').value);"></td>
			<td style="display:none" class="label"><b><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></b></td>
			<td style="display:none" class="right"><input type="text" class="apply_all" id="txtVVCommApplyAll" value="0">% <input type="button" class="btn mini red" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.apply_all');?>" onclick="virtuaApplyCommission(document.getElementById('txtVVCommApplyAll').value);"></td>
		</tr>
		<tr>
			<td class="left"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots');?></td>
			<td class=""><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing');?></td>
			<td class="middle"><input type="text" class="text" name="txtVVShareSlots" id="txtVVShareSlots" value="<?php echo  number_format((float)$vv_share_slots,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vv_share_slots,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vv_share_slots_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vv_share_slots_to'],2) : number_format((float)$vv_share_slots,2);?>%)</td>
			<td style="display:none" class="label"><?php echo Yii::t('agent','agent.revenue_sharing_adjustment.commission');?></td>
			<td style="display:none" class="right"><input type="text" class="text" name="txtVVCommSlots" id="txtVVCommSlots" value="<?php echo  number_format((float)$vv_comm_slots,2);?>">% <font color="red">*</font> <?php echo  number_format((float)$vv_comm_slots,2);?>%, <?php echo Yii::t('agent','agent.revenue_sharing_adjustment.limit');?>(<?php echo  number_format((float)$limit_from['vv_comm_slots_from'],2);?>% - <?php echo ($info['revenue_sharing_limit_to'] != '') ? number_format((float)$limit_to['vv_comm_slots_to'],2) : number_format((float)$vv_comm_slots,2);?>%)</td>
		</tr>
		
		</table>
	<?php }?>
	
</div>
<br/>
	<input type="submit" class="btn red" id="btnUpdate" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.update');?>">
	<input type="submit" class="btn red" id="btnReset" value="<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.reset');?>">


<script type="text/javascript">

//active menu color
document.getElementById('agentHeader').className="start active";
document.getElementById('mnu_agent_revenue_sharing_adjustment').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Revenue Sharing Adjustment</a></li>");

$('#btnReset').click(function(){
	document.location.reload();
});

$('#btnUpdate').click(function(){
	jQuery.ajax({
		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentRevenueSharingAdjustment/Update',
		type: 'POST',
		async: false,
		data: {'txtVIGShareBaccarat': $('#txtVIGShareBaccarat').val(),
			'txtVIGCommBaccarat':$('#txtVIGCommBaccarat').val(),
			'txtVIGShareRoulette':$('#txtVIGShareRoulette').val(),
			'txtVIGCommRoulette':$('#txtVIGCommRoulette').val(),
			'txtVIGShareBlackjack':$('#txtVIGShareBlackjack').val(),
			'txtVIGCommBlackjack':$('#txtVIGCommBlackjack').val(),
			'txtVIGShareAmericanRoulette':$('#txtVIGShareAmericanRoulette').val(),
			'txtVIGCommAmericanRoulette':$('#txtVIGCommAmericanRoulette').val(),
			'txtHTVShareBaccarat':$('#txtHTVShareBaccarat').val(),
			'txtHTVCommBaccarat':$('#txtHTVCommBaccarat').val(),
			'txtHTVShareRoulette':$('#txtHTVShareRoulette').val(),
			'txtHTVCommRoulette':$('#txtHTVCommRoulette').val(),
			'txtHTVShareDragonTiger':$('#txtHTVShareDragonTiger').val(),
			'txtHTVCommDragonTiger':$('#txtHTVCommDragonTiger').val(),
			'txtHTVShareSlots':$('#txtHTVShareSlots').val(),
			'txtHTVCommSlots':$('#txtHTVCommSlots').val(),
			'txtSVShareBaccarat':$('#txtSVShareBaccarat').val(),
			'txtSVCommBaccarat':$('#txtSVCommBaccarat').val(),
			'txtSVShareRoulette':$('#txtSVShareRoulette').val(),
			'txtSVCommRoulette':$('#txtSVCommRoulette').val(),
			'txtSVShareDragonTiger':$('#txtSVShareDragonTiger').val(),
			'txtSVCommDragonTiger':$('#txtSVCommDragonTiger').val(),
			'txtSVShareSlots':$('#txtSVShareSlots').val(),
			'txtSVCommSlots':$('#txtSVCommSlots').val(),
			'txtVVShareSlots':$('#txtVVShareSlots').val(),
			'txtVVCommSlots':$('#txtVVCommSlots').val(),
    		},
		success: function(msg) 
		{
			if(msg=='invalid_vig_share_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat_invalid_sharing');?>'); $('#txtVIGShareBaccarat').focus();return false;}
			else if(msg=='invalid_vig_comm_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat_invalid_commission');?>'); $('#txtVIGCommBaccarat').focus(); return false;}
			else if(msg=='invalid_vig_share_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.roulette_invalid_sharing');?>'); $('#txtVIGShareRoulette').focus(); return false;}
			else if(msg=='invalid_vig_comm_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.roulette_invalid_commission');?>'); $('#txtVIGCommRoulette').focus(); return false;}
			else if(msg=='invalid_vig_share_blackjack'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.blackjack_invalid_sharing');?>'); $('#txtVIGShareBlackjack').focus(); return false;} 
			else if(msg=='invalid_vig_comm_blackjack'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.blackjack_invalid_commission');?>'); $('#txtVIGCommBlackjack').focus(); return false; }
			else if(msg=='invalid_vig_share_american_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.american_roulette_invalid_sharing');?>'); $('#txtVIGShareAmericanRoulette').focus();return false; }
			else if(msg=='invalid_vig_comm_american_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.american_roulette_invalid_commission');?>'); $('#txtVIGCommAmericanRoulette').focus(); return false;}
			else if(msg=='invalid_htv_share_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat_invalid_sharing');?>'); $('#txtHTVShareBaccarat').focus(); return false; }
			else if(msg=='invalid_htv_comm_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat_invalid_commission');?>'); $('#txtHTVCommBaccarat').focus(); return false;}
			else if(msg=='invalid_htv_share_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.roulette_invalid_sharing');?>'); $('#txtHTVShareRoulette').focus(); return false; }
			else if(msg=='invalid_htv_comm_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.roulette_invalid_commission');?>'); $('#txtHTVCommRoulette').focus(); return false; }
			else if(msg=='invalid_htv_share_dragon_tiger'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.dragon_tiger_invalid_sharing');?>'); $('#txtHTVShareDragonTiger').focus(); return false; }
			else if(msg=='invalid_htv_comm_dragon_tiger'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.dragon_tiger_invalid_commission');?>'); $('#txtHTVCommDragonTiger').focus(); return false; }
			else if(msg=='invalid_htv_share_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots_invalid_sharing');?>'); $('#txtHTVShareSlots').focus(); return false; }
			else if(msg=='invalid_htv_comm_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots_invalid_commission');?>'); $('#txtHTVCommSlots').focus(); return false; }
			else if(msg=='invalid_sv_share_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat_invalid_sharing');?>'); $('#txtSVShareBaccarat').focus(); return false; }
			else if(msg=='invalid_sv_comm_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.baccarat_invalid_commission');?>'); $('#txtSVCommBaccarat').focus(); return false; }
			else if(msg=='invalid_sv_share_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.roulette_invalid_sharing');?>'); $('#txtSVShareRoulette').focus(); return false;}
			else if(msg=='invalid_sv_comm_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.roulette_invalid_commission');?>'); $('#txtSVCommRoulette').focus(); return false;}
			else if(msg=='invalid_sv_share_dragon_tiger'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.dragon_tiger_invalid_sharing');?>'); $('#txtSVShareDragonTiger').focus(); return false; }
			else if(msg=='invalid_sv_comm_dragon_tiger'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.dragon_tiger_invalid_commission');?>'); $('#txtSVCommDragonTiger').focus(); return false;}
			else if(msg=='invalid_sv_share_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots_invalid_sharing');?>'); $('#txtSVShareSlots').focus(); return false; }
			else if(msg=='invalid_sv_comm_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots_invalid_commission');?>'); $('#txtSVCommSlots').focus(); return false;}
			else if(msg=='invalid_vv_share_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots_invalid_sharing');?>'); $('#txtVVShareSlots').focus(); return false;}
			else if(msg=='invalid_vv_comm_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.slots_invalid_commission');?>'); $('#txtVVCommSlots').focus(); return false;}
			else if(msg=='vig_share_baccarat_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_baccarat_beyond_limit');?>'); $('#txtVIGShareBaccarat').focus(); return false;}
			else if(msg=='vig_comm_baccarat_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_baccarat_beyond_limit');?>'); $('#txtVIGCommBaccarat').focus(); return false;}
			else if(msg=='vig_share_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_e_roulette_beyond_limit');?>'); $('#txtVIGShareRoulette').focus(); return false;}
			else if(msg=='vig_comm_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_e_roulette_beyond_limit');?>'); $('#txtVIGCommRoulette').focus(); return false;}
			else if(msg=='vig_share_blackjack_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_blackjack_beyond_limit');?>'); $('#txtVIGShareBlackjack').focus(); return false;}
			else if(msg=='vig_comm_blackjack_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_blackjack_beyond_limit');?>'); $('#txtVIGCommBlackjack').focus(); return false;}
			else if(msg=='vig_share_american_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_a_roulette_beyond_limit');?>'); $('#txtVIGShareAmericanRoulette').focus(); return false;}
			else if(msg=='vig_comm_american_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_a_roulette_beyond_limit');?>'); $('#txtVIGCommAmericanRoulette').focus(); return false;}
			else if(msg=='htv_share_baccarat_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_baccarat_beyond_limit');?>'); $('#txtHTVShareBaccarat').focus(); return false;}
			else if(msg=='htv_comm_baccarat_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_baccarat_beyond_limit');?>'); $('#txtHTVCommBaccarat').focus(); return false;}
			else if(msg=='htv_share_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_e_roulette_beyond_limit');?>'); $('#txtHTVShareRoulette').focus(); return false;}
			else if(msg=='htv_comm_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_e_roulette_beyond_limit');?>'); $('#txtHTVCommRoulette').focus(); return false;}
			else if(msg=='htv_share_dragontiger_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_dragontiger_beyond_limit');?>'); $('#txtHTVShareDragonTiger').focus(); return false;}
			else if(msg=='htv_comm_dragontiger_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_dragontiger_beyond_limit');?>'); $('#txtHTVCommDragonTiger').focus(); return false;}
			else if(msg=='htv_share_slots_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_slots_beyond_limit');?>'); $('#txtHTVShareSlots').focus(); return false;}
			else if(msg=='htv_comm_slots_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_slots_beyond_limit');?>'); $('#txtHTVCommSlots').focus(); return false;}

			else if(msg=='sv_share_baccarat_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_baccarat_beyond_limit');?>'); $('#txtSVShareBaccarat').focus(); return false;}
			else if(msg=='sv_comm_baccarat_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_baccarat_beyond_limit');?>'); $('#txtSVCommBaccarat').focus(); return false;}
			else if(msg=='sv_share_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_e_roulette_beyond_limit');?>'); $('#txtSVShareRoulette').focus(); return false;}
			else if(msg=='sv_comm_roulette_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_e_roulette_beyond_limit');?>'); $('#txtSVCommRoulette').focus(); return false;}
			else if(msg=='sv_share_dragontiger_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_dragontiger_beyond_limit');?>'); $('#txtSVShareDragonTiger').focus(); return false;}
			else if(msg=='sv_comm_dragontiger_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_dragontiger_beyond_limit');?>'); $('#txtSVCommDragonTiger').focus(); return false;}
			else if(msg=='sv_share_slots_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_slots_beyond_limit');?>'); $('#txtSVShareSlots').focus(); return false;}
			else if(msg=='sv_comm_slots_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_slots_beyond_limit');?>'); $('#txtSVCommSlots').focus(); return false;}

			else if(msg=='vv_share_slots_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.share_slots_beyond_limit');?>'); $('#txtVVShareSlots').focus(); return false;}
			else if(msg=='vv_comm_slots_beyond_limit'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.comm_slots_beyond_limit');?>'); $('#txtVVCommSlots').focus(); return false;}

			else if(msg=='lessthan10_vig_share_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtVIGShareBaccarat').focus(); return false;}
			else if(msg=='lessthan10_vig_share_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtVIGShareRoulette').focus(); return false;}
			else if(msg=='lessthan10_vig_share_blackjack'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtVIGShareBlackjack').focus(); return false;} 
			else if(msg=='lessthan10_vig_share_american_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtVIGShareAmericanRoulette').focus();return false; }
			else if(msg=='lessthan10_htv_share_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtHTVShareBaccarat').focus(); return false; }
			else if(msg=='lessthan10_htv_share_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtHTVShareRoulette').focus(); return false; }
			else if(msg=='lessthan10_htv_share_dragon_tiger'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtHTVShareDragonTiger').focus(); return false; }
			else if(msg=='lessthan10_htv_share_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtHTVShareSlots').focus(); return false; }
			else if(msg=='lessthan10_sv_share_baccarat'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtSVShareBaccarat').focus(); return false; }
			else if(msg=='lessthan10_sv_share_roulette'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtSVShareRoulette').focus(); return false;}
			else if(msg=='lessthan10_sv_share_dragon_tiger'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtSVShareDragonTiger').focus(); return false; }
			else if(msg=='lessthan10_sv_share_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtSVShareSlots').focus(); return false; }
			else if(msg=='lessthan10_vv_share_slots'){ alert('<?php echo Yii::t('agent','agent.revenue_sharing_adjustment.sharing_lessthan10_error');?>'); $('#txtVVShareSlots').focus(); return false;}
			

			
			else if(msg=='save_date_limit'){alert('You are not allowed to change your sharing/commission more than once a day!'); document.location.reload();}

			else{
				alert('Update complete!');
				document.location.reload();
			}
    		
    	}
	});
});
function costaApplySharing(val){
	document.getElementById('txtVIGShareBaccarat').value=val;
	document.getElementById('txtVIGShareRoulette').value=val;
	document.getElementById('txtVIGShareBlackjack').value=val;
	document.getElementById('txtVIGShareAmericanRoulette').value=val;
}
function costaApplyCommission(val){
	document.getElementById('txtVIGCommBaccarat').value=val;
	document.getElementById('txtVIGCommRoulette').value=val;
	document.getElementById('txtVIGCommBlackjack').value=val;
	document.getElementById('txtVIGCommAmericanRoulette').value=val;
}
function hatienApplySharing(val){
	document.getElementById('txtHTVShareBaccarat').value=val;
	document.getElementById('txtHTVShareRoulette').value=val;
	document.getElementById('txtHTVShareDragonTiger').value=val;
	document.getElementById('txtHTVShareSlots').value=val;
}
function hatienApplyCommission(val){
	document.getElementById('txtHTVCommBaccarat').value=val;
	document.getElementById('txtHTVCommRoulette').value=val;
	document.getElementById('txtHTVCommDragonTiger').value=val;
	document.getElementById('txtHTVCommSlots').value=val;
}
function savanApplySharing(val){
	document.getElementById('txtSVShareBaccarat').value=val;
	document.getElementById('txtSVShareRoulette').value=val;
	document.getElementById('txtSVShareDragonTiger').value=val;
	document.getElementById('txtSVShareSlots').value=val;
}
function savanApplyCommission(val){
	document.getElementById('txtSVCommBaccarat').value=val;
	document.getElementById('txtSVCommRoulette').value=val;
	document.getElementById('txtSVCommDragonTiger').value=val;
	document.getElementById('txtSVCommSlots').value=val;
}
function virtuaApplySharing(val){
	document.getElementById('txtVVShareSlots').value=val;
}
function virtuaApplyCommission(val){
	document.getElementById('txtVVCommSlots').value=val;
}
</script>