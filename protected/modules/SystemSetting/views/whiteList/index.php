<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/whitelist.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/whitelist.css" />
	<script type="text/javascript">
		var urlSubCompanyList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WhiteList/SubCompany';
		var urlSaveSubCompanyWhiteList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WhiteList/SaveSubCompanyWhiteList';
		var urlCashPlayerList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WhiteList/CashPlayer';
		var urlCashPlayerWhiteList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WhiteList/CashPlayerWhiteList';
	</script>
</head>
<body onload="javascript: onTheLoad();">
	<b>White List Management</b>
	<!-- Search Box -->
	<div id="search_cont">
		<!-- Title -->
		<div class="title">Search White List</div>
		
		<!-- Body -->
		<div class="body">
			<table border="0" cellpadding="0" cellspacing="0">
			
			<!-- Cash Player or Sub Company -->
			<tr>
				<td class="left">Search Type</td>
				<td class="right">
					<?php echo CHtml::dropDownList('cmbSearchType','0', Array('0'=>'All','1'=>'Cash Player','2'=>'Sub Company'));?>
				</td>
			</tr>
			
			<!-- Search Textbox -->
			<tr>
				<td class="left">Account ID</td>
				<td class="right">
					<?php echo CHtml::TextField('txtSearch',''); ?>
				</td>
			</tr>
			
			<!-- Search Button -->
			<tr>
				<td class="left"></td>
				<td class="right">
					<?php echo CHtml::button('Search',array('id'=>'btnSearch','onClick'=>'searchWhiteList()','class'=>'btn red')); ?>
				</td>
			</tr>
			</table>
		</div>
	</div>
	<div id="qry_cp_result"></div>
	<div id="qry_sc_result"></div>
	<br/>
	<br/>
</body>
</html>