<?php
/**
 * @todo AgentPlayerTransferController
 * @copyright CE
 * @author Allan
 * @since 2012-06-13
 */
class AgentPlayerTransferController extends MyController
{
	public function actionAgentPlayerTransferProcess()
	{
		if(Yii::app()->user->checkAccess('agent.writeTransfer') || User::getUserType() === Constants::ROLE_AGENT)
		{	
			$cpd = new AgentPlayerTransfer();
			$cpd->makeAgentPlayerTransfer();
		}
	}
	
	public function actionAgentPlayerLobbyWithdrawProcess()
	{
		if(Yii::app()->user->checkAccess('agent.writeTransfer') || User::getUserType() === Constants::ROLE_AGENT)
		{
			$cpd = new AgentPlayerTransfer();
			$cpd->makeProcessWithdrawalRequest();
		}
	}
}
