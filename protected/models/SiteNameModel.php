<?php
class SiteNameModel
{

	/**
	 * 
	 * @param integer $siteID
	 */
	public static function getNameById($siteID)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetCompanySiteNameById(" . $siteID . ")");

		$temp = $command->queryRow();
		return $temp['site'];
	} 

	/**
	 *
	 * @param string $langCode en
	 */
	public static function getNames()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetCompanySiteNames()");
		//$command->execute();
	
		//$command = $connection->createCommand("SELECT " . $outputParam . ";");
		return $command->queryAll();
	}
}

