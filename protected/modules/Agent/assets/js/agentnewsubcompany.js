
//active menu color
document.getElementById('agentHeader').className="start active";
document.getElementById('mnu_new_edit_agent').className="active";
//$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><li>"+headerTitle+"</li>");

function checkIt(evt,id) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    var strText=document.getElementById(id).value;
    var countStr=(strText.split(".").length - 1);
    if (charCode==46){
	    //allow only one period
    	if(countStr > 0){
	    	return false;
	    }
	}
    var charStr = String.fromCharCode(charCode);
    if (charStr == "-") {
    	return true;
    }
    //if (charCode > 31 && ((charCode < 48 && charCode!=46) || charCode > 57)) {
    if (charCode > 31 && ((charCode < 48 && charCode!=37 && charCode!=39 && charCode!=46) || charCode > 57)) {
        status = "This field accepts numbers only.";
        return false;
    }
    status = "";
    return true;
}

function checkIt_unallow_special_char(evt,id){
	evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    var strText=document.getElementById(id).value;
    var countStr=(strText.split(".").length - 1);
    
    if ((charCode>=33 && charCode<=47) || (charCode>=58 && charCode<=64) || (charCode>=91 && charCode<=96) || (charCode>=123 && charCode<=126)){
        status = "This field accepts numbers only.";
        return false;
    }
    status = "";
    return true;
}
var hide= true;
function hideRows(clicker,tableId,trNum,trNumHatien,trNumSavan,trNumVirtua,trNumSlots,s_costa_checked,s_htv_checked,s_savan_checked,s_virtua_checked,s_slots_checked){
	var t = document.getElementById(tableId);
	if (clicker.checked==false){
		t.rows[trNum].style.visibility = 'hidden';
		t.rows[trNum].style.display = 'none';
		t.rows[trNumHatien].style.visibility = 'hidden';
		t.rows[trNumHatien].style.display = 'none';
		t.rows[trNumSavan].style.visibility = 'hidden';
		t.rows[trNumSavan].style.display = 'none';
		t.rows[trNumVirtua].style.visibility = 'hidden';
		t.rows[trNumVirtua].style.display = 'none';
		t.rows[trNumSlots].style.visibility = 'hidden';
		t.rows[trNumSlots].style.display = 'none';
	}else{
		//costavegas999
		if(s_costa_checked==true){
			if(trNum<=6){
				t.rows[trNum].style.visibility = 'visible';
				t.rows[trNum].style.display = 'table-row';
			}
		}else{
			if(trNum<=6){
				t.rows[trNum].style.visibility = 'hidden';
				t.rows[trNum].style.display = 'none';
			}
		}
		//hatienvegas999
//		if(s_htv_checked==true){
//			if(trNumHatien>6 && trNumHatien<=13){
//				t.rows[trNumHatien].style.visibility = 'visible';
//				t.rows[trNumHatien].style.display = 'table-row';
//			}
//		}else{
//			if(trNumHatien>6 && trNumHatien<=13){
//				t.rows[trNumHatien].style.visibility = 'hidden';
//				t.rows[trNumHatien].style.display = 'none';
//			}
//		}
		//savanvegas999
		if(s_savan_checked==true){
			if(trNumSavan>13 && trNumSavan<=20){
				t.rows[trNumSavan].style.visibility = 'visible';
				t.rows[trNumSavan].style.display = 'table-row';
			}
		}else{
			if(trNumSavan>13 && trNumSavan<=20){
				t.rows[trNumSavan].style.visibility = 'hidden';
				t.rows[trNumSavan].style.display = 'none';
			}
		}
		//virtuavegas999
//		if(s_virtua_checked==true){
//			if(trNumVirtua>20 && trNumVirtua<=28){
//				t.rows[trNumVirtua].style.visibility = 'visible';
//				t.rows[trNumVirtua].style.display = 'table-row';
//			}
//		}else{
//			if(trNumVirtua>20 && trNumVirtua<=28){
//				t.rows[trNumVirtua].style.visibility = 'hidden';
//				t.rows[trNumVirtua].style.display = 'none';
//			}
//		}
		
		//Slotsvegas999 Disable for the meantime
		if(s_slots_checked==true){
			if(trNumSlots>34 && trNumSlots<=41){
				t.rows[trNumSlots].style.visibility = 'visible';
				t.rows[trNumSlots].style.display = 'table-row';
			}
		}else{
			if(trNumSlots>34 && trNumSlots<=41){
				t.rows[trNumSlots].style.visibility = 'hidden';
				t.rows[trNumSlots].style.display = 'none';
			}
		}
	}
	if(document.getElementById('chkBacarratGame').checked==false && document.getElementById('chkRouletteGame').checked==false && document.getElementById('chkDragonTigerGame').checked==false){
		document.getElementById('dv_limits_msg').style.display='none';
	}else{
		if(document.getElementById('chkHtv999').checked==false && document.getElementById('chkSavanVegas999').checked==false){
			document.getElementById('dv_limits_msg').style.display='none';
		}else{
			document.getElementById('dv_limits_msg').style.display='block';	
		}
	}
}

/**
 * @todo remove sharing and commission of the downline agents.
 * @author leokarl
 * @since 2012-08-10
 */
function rowValues(){
	if(document.getElementById('chkCostaVegas999').checked==false){
		document.getElementById('txtBacarratSharing').value='0';
		document.getElementById('txtBacarratCommision').value='0';
		document.getElementById('txtRouletteSharing').value='0';
		document.getElementById('txtRouletteCommision').value='0';
		document.getElementById('txtDragonTigerSharing').value='0';
		document.getElementById('txtDragonTigerCommision').value='0';
		document.getElementById('txtSlotsSharing').value='0';
		document.getElementById('txtSlotsCommision').value='0';
		document.getElementById('txtAmericanRouletteSharing').value='0';
		document.getElementById('txtAmericanRouletteCommision').value='0';
		document.getElementById('txtSlotSharing').value='0';
		document.getElementById('txtSlotCommision').value='0';
	}
	if(document.getElementById('chkHtv999').checked==false){
		document.getElementById('txtBacarratSharing_htv').value='0';
		document.getElementById('txtBacarratCommision_htv').value='0';
		document.getElementById('txtRouletteSharing_htv').value='0';
		document.getElementById('txtRouletteCommision_htv').value='0';
		document.getElementById('txtDragonTigerSharing_htv').value='0';
		document.getElementById('txtDragonTigerCommision_htv').value='0';
		document.getElementById('txtSlotsSharing_htv').value='0';
		document.getElementById('txtSlotsCommision_htv').value='0';
		document.getElementById('txtAmericanRouletteSharing_htv').value='0';
		document.getElementById('txtAmericanRouletteCommision_htv').value='0';
		document.getElementById('txtSlotSharing_htv').value='0';
		document.getElementById('txtSlotCommision_htv').value='0';
	}
	if(document.getElementById('chkSavanVegas999').checked==false){
		document.getElementById('txtBacarratSharing_savan').value='0';
		document.getElementById('txtBacarratCommision_savan').value='0';
		document.getElementById('txtRouletteSharing_savan').value='0';
		document.getElementById('txtRouletteCommision_savan').value='0';
		document.getElementById('txtDragonTigerSharing_savan').value='0';
		document.getElementById('txtDragonTigerCommision_savan').value='0';
		document.getElementById('txtSlotsSharing_savan').value='0';
		document.getElementById('txtSlotsCommision_savan').value='0';
		document.getElementById('txtAmericanRouletteSharing_savan').value='0';
		document.getElementById('txtAmericanRouletteCommision_savan').value='0';
		document.getElementById('txtSlotSharing_savan').value='0';
		document.getElementById('txtSlotCommision_savan').value='0';
	}
	if(document.getElementById('chkVirtuaVegas999').checked==false){
		document.getElementById('txtBacarratSharing_virtua').value='0';
		document.getElementById('txtBacarratCommision_virtua').value='0';
		document.getElementById('txtRouletteSharing_virtua').value='0';
		document.getElementById('txtRouletteCommision_virtua').value='0';
		document.getElementById('txtDragonTigerSharing_virtua').value='0';
		document.getElementById('txtDragonTigerCommision_virtua').value='0';
		document.getElementById('txtSlotsSharing_virtua').value='0';
		document.getElementById('txtSlotsCommision_virtua').value='0';
		document.getElementById('txtAmericanRouletteSharing_virtua').value='0';
		document.getElementById('txtAmericanRouletteCommision_virtua').value='0';
		document.getElementById('txtSlotSharing_virtua').value='0';
		document.getElementById('txtSlotCommision_virtua').value='0';
	}
}

function hideRowsCosta(id){
	var t = document.getElementById('tblBettingInformation');
	if(document.getElementById(id).checked==true){
		
		document.getElementById('btnApplyCostaSharing').disabled=false;//enable apply button
		document.getElementById('txtCostaSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplyCostaCommission').disabled=false;//enable apply button
		document.getElementById('txtCostaCommissionForAll').disabled=false;//enable apply box
		
		if(document.getElementById('chkBacarratGame').checked==false){
			t.rows[1].style.visibility = 'hidden';
			t.rows[1].style.display = 'none';
		}else{
			t.rows[1].style.visibility = 'visible';
			t.rows[1].style.display = 'table-row';
			
		}
		if(document.getElementById('chkRouletteGame').checked==false){
			t.rows[2].style.visibility = 'hidden';
			t.rows[2].style.display = 'none';
		}else{
			t.rows[2].style.visibility = 'visible';
			t.rows[2].style.display = 'table-row';
		}
		if(document.getElementById('chkDragonTigerGame').checked==false){
			t.rows[3].style.visibility = 'hidden';
			t.rows[3].style.display = 'none';
		}else{
			//t.rows[3].style.visibility = 'visible';
			//t.rows[3].style.display = 'table-row';
			t.rows[3].style.visibility = 'hidden';
			t.rows[3].style.display = 'none';
		}
		if(document.getElementById('chkSlotsGame').checked==false){
			t.rows[4].style.visibility = 'hidden';
			t.rows[4].style.display = 'none';
		}else{
			t.rows[4].style.visibility = 'visible';
			t.rows[4].style.display = 'table-row';
			
		}
		if(document.getElementById('chkAmericanRouletteGame').checked==false){
			t.rows[5].style.visibility = 'hidden';
			t.rows[5].style.display = 'none';
		}else{
			t.rows[5].style.visibility = 'visible';
			t.rows[5].style.display = 'table-row';
			
		}
		if(document.getElementById('chkSlotGame').checked==false){
			t.rows[6].style.visibility = 'hidden';
			t.rows[6].style.display = 'none';
		}else{
			//t.rows[6].style.visibility = 'visible';
			//t.rows[6].style.display = 'table-row';
			t.rows[6].style.visibility = 'hidden';
			t.rows[6].style.display = 'none';
			
		}
	}else{
		document.getElementById('btnApplyCostaSharing').disabled=true;//disable apply button
		document.getElementById('txtCostaSharingForAll').disabled=true;//disable apply box
		document.getElementById('btnApplyCostaCommission').disabled=true;//disable apply button
		document.getElementById('txtCostaCommissionForAll').disabled=true;//disable apply box
		t.rows[1].style.visibility = 'hidden';
		t.rows[1].style.display = 'none';
		t.rows[2].style.visibility = 'hidden';
		t.rows[2].style.display = 'none';
		t.rows[3].style.visibility = 'hidden';
		t.rows[3].style.display = 'none';
		t.rows[4].style.visibility = 'hidden';
		t.rows[4].style.display = 'none';
		t.rows[5].style.visibility = 'hidden';
		t.rows[5].style.display = 'none';
		t.rows[6].style.visibility = 'hidden';
		t.rows[6].style.display = 'none';
		
	}
}

function hideRowsHatien(id){
	document.getElementById('btnApplyHatienSharing').disabled=true;//enable apply button
	document.getElementById('txtHatienSharingForAll').disabled=true;//enable apply box
	document.getElementById('btnApplyHatienCommission').disabled=true;//enable apply button
	document.getElementById('txtHatienCommissionForAll').disabled=true;//enable apply box
	t.rows[8].style.visibility = 'hidden';
	t.rows[8].style.display = 'none';
	t.rows[9].style.visibility = 'hidden';
	t.rows[9].style.display = 'none';
	t.rows[10].style.visibility = 'hidden';
	t.rows[10].style.display = 'none';
	t.rows[11].style.visibility = 'hidden';
	t.rows[11].style.display = 'none';
	t.rows[12].style.visibility = 'hidden';
	t.rows[12].style.display = 'none';
	t.rows[13].style.visibility = 'hidden';
	t.rows[13].style.display = 'none';
//Disable Hatien Selection
//	var t = document.getElementById('tblBettingInformation');
//	if(document.getElementById(id).checked==true){
//		document.getElementById('btnApplyHatienSharing').disabled=false;//enable apply button
//		document.getElementById('txtHatienSharingForAll').disabled=false;//enable apply box
//		document.getElementById('btnApplyHatienCommission').disabled=false;//enable apply button
//		document.getElementById('txtHatienCommissionForAll').disabled=false;//enable apply box
//		
//		if(document.getElementById('chkBacarratGame').checked==false){
//			t.rows[8].style.visibility = 'hidden';
//			t.rows[8].style.display = 'none';
//		}else{
//			t.rows[8].style.visibility = 'visible';
//			t.rows[8].style.display = 'table-row';
//		}
//		if(document.getElementById('chkRouletteGame').checked==false){
//			t.rows[9].style.visibility = 'hidden';
//			t.rows[9].style.display = 'none';
//		}else{
//			t.rows[9].style.visibility = 'visible';
//			t.rows[9].style.display = 'table-row';
//		}
//		if(document.getElementById('chkDragonTigerGame').checked==false){
//			t.rows[10].style.visibility = 'hidden';
//			t.rows[10].style.display = 'none';
//		}else{
//			t.rows[10].style.visibility = 'visible';
//			t.rows[10].style.display = 'table-row';
//		}
//		if(document.getElementById('chkSlotsGame').checked==false){
//			t.rows[11].style.visibility = 'hidden';
//			t.rows[11].style.display = 'none';
//		}else{
//			//t.rows[11].style.visibility = 'visible';
//			//t.rows[11].style.display = 'table-row';
//			t.rows[11].style.visibility = 'hidden';
//			t.rows[11].style.display = 'none';
//		}
//		if(document.getElementById('chkAmericanRouletteGame').checked==false){
//			t.rows[12].style.visibility = 'hidden';
//			t.rows[12].style.display = 'none';
//		}else{
//			//t.rows[12].style.visibility = 'visible';
//			//t.rows[12].style.display = 'table-row';
//			t.rows[12].style.visibility = 'hidden';
//			t.rows[12].style.display = 'none';
//		}
//		if(document.getElementById('chkSlotGame').checked==false){
//			t.rows[13].style.visibility = 'hidden';
//			t.rows[13].style.display = 'none';
//		}else{
//			t.rows[13].style.visibility = 'visible';
//			t.rows[13].style.display = 'table-row';
//		}
//	}else{
//		document.getElementById('btnApplyHatienSharing').disabled=true;//enable apply button
//		document.getElementById('txtHatienSharingForAll').disabled=true;//enable apply box
//		document.getElementById('btnApplyHatienCommission').disabled=true;//enable apply button
//		document.getElementById('txtHatienCommissionForAll').disabled=true;//enable apply box
//		t.rows[8].style.visibility = 'hidden';
//		t.rows[8].style.display = 'none';
//		t.rows[9].style.visibility = 'hidden';
//		t.rows[9].style.display = 'none';
//		t.rows[10].style.visibility = 'hidden';
//		t.rows[10].style.display = 'none';
//		t.rows[11].style.visibility = 'hidden';
//		t.rows[11].style.display = 'none';
//		t.rows[12].style.visibility = 'hidden';
//		t.rows[12].style.display = 'none';
//		t.rows[13].style.visibility = 'hidden';
//		t.rows[13].style.display = 'none';
//		
//	}
//	//hide baccarat limit if hatien and savan are disabled
//	if(document.getElementById('chkHtv999').checked==false && document.getElementById('chkSavanVegas999').checked==false){
//		document.getElementById('qry_baccarat').style.display='none';
//		document.getElementById('qry_roullete').style.display='none';
//		document.getElementById('qry_dragon_tiger').style.display='none';
//		document.getElementById('dv_limits_msg').style.display='none';
//	}else{
//		document.getElementById('qry_baccarat').style.display='block';
//		document.getElementById('qry_roullete').style.display='block';
//		document.getElementById('qry_dragon_tiger').style.display='block';
//		if(document.getElementById('chkBacarratGame').checked==false && document.getElementById('chkRouletteGame').checked==false && document.getElementById('chkDragonTigerGame').checked==false){
//			document.getElementById('dv_limits_msg').style.display='none';
//		}else{
//			document.getElementById('dv_limits_msg').style.display='block';
//		}
//	}
}

function hideRowsSavan(id){
	var t = document.getElementById('tblBettingInformation');
	if(document.getElementById(id).checked==true){
		document.getElementById('btnApplySavanSharing').disabled=false;//enable apply button
		document.getElementById('txtSavanSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplySavanCommission').disabled=false;//enable apply button
		document.getElementById('txtSavanCommissionForAll').disabled=false;//enable apply box
		
		if(document.getElementById('chkBacarratGame').checked==false){
			t.rows[15].style.visibility = 'hidden';
			t.rows[15].style.display = 'none';
		}else{
			t.rows[15].style.visibility = 'visible';
			t.rows[15].style.display = 'table-row';
		}
		if(document.getElementById('chkRouletteGame').checked==false){
			t.rows[16].style.visibility = 'hidden';
			t.rows[16].style.display = 'none';
		}else{
			t.rows[16].style.visibility = 'visible';
			t.rows[16].style.display = 'table-row';
		}
		if(document.getElementById('chkDragonTigerGame').checked==false){
			t.rows[17].style.visibility = 'hidden';
			t.rows[17].style.display = 'none';
		}else{
			t.rows[17].style.visibility = 'visible';
			t.rows[17].style.display = 'table-row';
		}
		if(document.getElementById('chkSlotsGame').checked==false){
			t.rows[18].style.visibility = 'hidden';
			t.rows[18].style.display = 'none';
		}else{
			//t.rows[18].style.visibility = 'visible';
			//t.rows[18].style.display = 'table-row';
			t.rows[18].style.visibility = 'hidden';
			t.rows[18].style.display = 'none';
		}
		if(document.getElementById('chkAmericanRouletteGame').checked==false){
			t.rows[19].style.visibility = 'hidden';
			t.rows[19].style.display = 'none';
		}else{
			//t.rows[19].style.visibility = 'visible';
			//t.rows[19].style.display = 'table-row';
			t.rows[19].style.visibility = 'hidden';
			t.rows[19].style.display = 'none';
		}
		if(document.getElementById('chkSlotGame').checked==false){
			t.rows[20].style.visibility = 'hidden';
			t.rows[20].style.display = 'none';
		}else{
			t.rows[20].style.visibility = 'visible';
			t.rows[20].style.display = 'table-row';
		}
	}else{
		document.getElementById('btnApplySavanSharing').disabled=true;//enable apply button
		document.getElementById('txtSavanSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplySavanCommission').disabled=true;//enable apply button
		document.getElementById('txtSavanCommissionForAll').disabled=true;//enable apply box
		t.rows[15].style.visibility = 'hidden';
		t.rows[15].style.display = 'none';
		t.rows[16].style.visibility = 'hidden';
		t.rows[16].style.display = 'none';
		t.rows[17].style.visibility = 'hidden';
		t.rows[17].style.display = 'none';
		t.rows[18].style.visibility = 'hidden';
		t.rows[18].style.display = 'none';
		t.rows[19].style.visibility = 'hidden';
		t.rows[19].style.display = 'none';
		t.rows[20].style.visibility = 'hidden';
		t.rows[20].style.display = 'none';
		
	}
	
	//hide baccarat limit if hatien and savan are disabled
	if(document.getElementById('chkHtv999').checked==false && document.getElementById('chkSavanVegas999').checked==false){
		document.getElementById('qry_baccarat').style.display='none';
		document.getElementById('qry_roullete').style.display='none';
		document.getElementById('qry_dragon_tiger').style.display='none';
		document.getElementById('dv_limits_msg').style.display='none';
	}else{
		document.getElementById('qry_baccarat').style.display='block';
		document.getElementById('qry_roullete').style.display='block';
		document.getElementById('qry_dragon_tiger').style.display='block';
		if(document.getElementById('chkBacarratGame').checked==false && document.getElementById('chkRouletteGame').checked==false && document.getElementById('chkDragonTigerGame').checked==false){
			document.getElementById('dv_limits_msg').style.display='none';
		}else{
			document.getElementById('dv_limits_msg').style.display='block';
		}
	}
}

function hideRowsVirtua(id){
	var t = document.getElementById('tblBettingInformation');
	if(document.getElementById(id).checked==true){
		document.getElementById('btnApplyVirtuaSharing').disabled=false;//enable apply button
		document.getElementById('txtVirtuaSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplyVirtuaCommission').disabled=false;//enable apply button
		document.getElementById('txtVirtuaCommissionForAll').disabled=false;//enable apply box
		
		if(document.getElementById('chkBacarratGame').checked==false){
			t.rows[22].style.visibility = 'hidden';
			t.rows[22].style.display = 'none';
		}else{
			//t.rows[22].style.visibility = 'visible';
			//t.rows[22].style.display = 'table-row';
			t.rows[22].style.visibility = 'hidden';
			t.rows[22].style.display = 'none';
		}
		if(document.getElementById('chkRouletteGame').checked==false){
			t.rows[23].style.visibility = 'hidden';
			t.rows[23].style.display = 'none';
		}else{
			//t.rows[23].style.visibility = 'visible';
			//t.rows[23].style.display = 'table-row';
			t.rows[23].style.visibility = 'hidden';
			t.rows[23].style.display = 'none';
		}
		if(document.getElementById('chkDragonTigerGame').checked==false){
			t.rows[24].style.visibility = 'hidden';
			t.rows[24].style.display = 'none';
		}else{
			//t.rows[24].style.visibility = 'visible';
			//t.rows[24].style.display = 'table-row';
			t.rows[24].style.visibility = 'hidden';
			t.rows[24].style.display = 'none';
		}
		if(document.getElementById('chkSlotsGame').checked==false){
			t.rows[25].style.visibility = 'hidden';
			t.rows[25].style.display = 'none';
		}else{
			//t.rows[25].style.visibility = 'visible';
			//t.rows[25].style.display = 'table-row';
			t.rows[25].style.visibility = 'hidden';
			t.rows[25].style.display = 'none';
		}
		if(document.getElementById('chkAmericanRouletteGame').checked==false){
			t.rows[26].style.visibility = 'hidden';
			t.rows[26].style.display = 'none';
		}else{
			//t.rows[26].style.visibility = 'visible';
			//t.rows[26].style.display = 'table-row';
			t.rows[26].style.visibility = 'hidden';
			t.rows[26].style.display = 'none';
		}
		if(document.getElementById('chkSlotGame').checked==false){
			t.rows[27].style.visibility = 'hidden';
			t.rows[27].style.display = 'none';
		}else{
			t.rows[27].style.visibility = 'visible';
			t.rows[27].style.display = 'table-row';
		}
	}else{
		document.getElementById('btnApplyVirtuaSharing').disabled=true;//enable apply button
		document.getElementById('txtVirtuaSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplyVirtuaCommission').disabled=true;//enable apply button
		document.getElementById('txtVirtuaCommissionForAll').disabled=true;//enable apply box
		t.rows[22].style.visibility = 'hidden';
		t.rows[22].style.display = 'none';
		t.rows[23].style.visibility = 'hidden';
		t.rows[23].style.display = 'none';
		t.rows[24].style.visibility = 'hidden';
		t.rows[24].style.display = 'none';
		t.rows[25].style.visibility = 'hidden';
		t.rows[25].style.display = 'none';
		t.rows[26].style.visibility = 'hidden';
		t.rows[26].style.display = 'none';
		t.rows[27].style.visibility = 'hidden';
		t.rows[27].style.display = 'none';
		
	}
	
	//hide baccarat limit if hatien and savan are disabled
	if(document.getElementById('chkHtv999').checked==false && document.getElementById('chkSavanVegas999').checked==false){
		document.getElementById('qry_baccarat').style.display='none';
		document.getElementById('qry_roullete').style.display='none';
		document.getElementById('qry_dragon_tiger').style.display='none';
		document.getElementById('dv_limits_msg').style.display='none';
	}else{
		document.getElementById('qry_baccarat').style.display='block';
		document.getElementById('qry_roullete').style.display='block';
		document.getElementById('qry_dragon_tiger').style.display='block';
		if(document.getElementById('chkBacarratGame').checked==false && document.getElementById('chkRouletteGame').checked==false && document.getElementById('chkDragonTigerGame').checked==false){
			document.getElementById('dv_limits_msg').style.display='none';
		}else{
			document.getElementById('dv_limits_msg').style.display='block';
		}
	}
}
function hideRowsSportsBook(id){
	var t = document.getElementById('tblBettingInformation');
	if(document.getElementById(id).checked==true){
		document.getElementById('btnApplySportsBookSharing').disabled=false;//enable apply button
		document.getElementById('txtSportsBookSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplySportsBookCommission').disabled=false;//enable apply button
		document.getElementById('txtSportsBookCommissionForAll').disabled=false;//enable apply box
		
		if(document.getElementById('chkBacarratGame').checked==false){
			t.rows[29].style.visibility = 'hidden';
			t.rows[29].style.display = 'none';
		}else{
			//t.rows[22].style.visibility = 'visible';
			//t.rows[22].style.display = 'table-row';
			t.rows[29].style.visibility = 'hidden';
			t.rows[29].style.display = 'none';
		}
		if(document.getElementById('chkRouletteGame').checked==false){
			t.rows[30].style.visibility = 'hidden';
			t.rows[30].style.display = 'none';
		}else{
			//t.rows[23].style.visibility = 'visible';
			//t.rows[23].style.display = 'table-row';
			t.rows[30].style.visibility = 'hidden';
			t.rows[30].style.display = 'none';
		}
		if(document.getElementById('chkDragonTigerGame').checked==false){
			t.rows[31].style.visibility = 'hidden';
			t.rows[31].style.display = 'none';
		}else{
			//t.rows[24].style.visibility = 'visible';
			//t.rows[24].style.display = 'table-row';
			t.rows[31].style.visibility = 'hidden';
			t.rows[31].style.display = 'none';
		}
		if(document.getElementById('chkSlotsGame').checked==false){
			t.rows[32].style.visibility = 'hidden';
			t.rows[32].style.display = 'none';
		}else{
			//t.rows[25].style.visibility = 'visible';
			//t.rows[25].style.display = 'table-row';
			t.rows[32].style.visibility = 'hidden';
			t.rows[32].style.display = 'none';
		}
		if(document.getElementById('chkAmericanRouletteGame').checked==false){
			t.rows[33].style.visibility = 'hidden';
			t.rows[33].style.display = 'none';
		}else{
			//t.rows[26].style.visibility = 'visible';
			//t.rows[26].style.display = 'table-row';
			t.rows[33].style.visibility = 'hidden';
			t.rows[33].style.display = 'none';
		}
		if(document.getElementById('chkSlotGame').checked==false){
			t.rows[34].style.visibility = 'hidden';
			t.rows[34].style.display = 'none';
		}else{
			t.rows[34].style.visibility = 'visible';
			t.rows[34].style.display = 'table-row';
		}
	}else{
		document.getElementById('btnApplySportsBookSharing').disabled=true;//enable apply button
		document.getElementById('txtSportsBookSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplySportsBookCommission').disabled=true;//enable apply button
		document.getElementById('txtSportsBookCommissionForAll').disabled=true;//enable apply box
		t.rows[29].style.visibility = 'hidden';
		t.rows[29].style.display = 'none';
		t.rows[30].style.visibility = 'hidden';
		t.rows[30].style.display = 'none';
		t.rows[31].style.visibility = 'hidden';
		t.rows[31].style.display = 'none';
		t.rows[32].style.visibility = 'hidden';
		t.rows[32].style.display = 'none';
		t.rows[33].style.visibility = 'hidden';
		t.rows[33].style.display = 'none';
		t.rows[34].style.visibility = 'hidden';
		t.rows[34].style.display = 'none';
		
	}
	
	//hide baccarat limit if hatien and savan are disabled
	if(document.getElementById('chkHtv999').checked==false && document.getElementById('chkSavan999').checked==false){
		document.getElementById('qry_baccarat').style.display='none';
		document.getElementById('qry_roullete').style.display='none';
		document.getElementById('qry_dragon_tiger').style.display='none';
		document.getElementById('dv_limits_msg').style.display='none';
	}else{
		document.getElementById('qry_baccarat').style.display='block';
		document.getElementById('qry_roullete').style.display='block';
		document.getElementById('qry_dragon_tiger').style.display='block';
		if(document.getElementById('chkBacarratGame').checked==false && document.getElementById('chkRouletteGame').checked==false && document.getElementById('chkDragonTigerGame').checked==false){
			document.getElementById('dv_limits_msg').style.display='none';
		}else{
			document.getElementById('dv_limits_msg').style.display='block';
		}
	}
}
function hideRowsSlotsVegas(id){
	var t = document.getElementById('tblBettingInformation');
	if(document.getElementById(id).checked==true){
		document.getElementById('btnApplySlotsVegasSharing').disabled=false;//enable apply button
		document.getElementById('txtSlotsVegasSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplySlotsVegasCommission').disabled=false;//enable apply button
		document.getElementById('txtSlotsVegasCommissionForAll').disabled=false;//enable apply box
		
		if(document.getElementById('chkBacarratGame').checked==false){
			t.rows[36].style.visibility = 'hidden';
			t.rows[36].style.display = 'none';
		}else{
			//t.rows[22].style.visibility = 'visible';
			//t.rows[22].style.display = 'table-row';
			t.rows[36].style.visibility = 'hidden';
			t.rows[36].style.display = 'none';
		}
		if(document.getElementById('chkRouletteGame').checked==false){
			t.rows[37].style.visibility = 'hidden';
			t.rows[37].style.display = 'none';
		}else{
			//t.rows[23].style.visibility = 'visible';
			//t.rows[23].style.display = 'table-row';
			t.rows[37].style.visibility = 'hidden';
			t.rows[37].style.display = 'none';
		}
		if(document.getElementById('chkDragonTigerGame').checked==false){
			t.rows[38].style.visibility = 'hidden';
			t.rows[38].style.display = 'none';
		}else{
			//t.rows[24].style.visibility = 'visible';
			//t.rows[24].style.display = 'table-row';
			t.rows[38].style.visibility = 'hidden';
			t.rows[38].style.display = 'none';
		}
		if(document.getElementById('chkSlotsGame').checked==false){
			t.rows[39].style.visibility = 'hidden';
			t.rows[39].style.display = 'none';
		}else{
			//t.rows[25].style.visibility = 'visible';
			//t.rows[25].style.display = 'table-row';
			t.rows[39].style.visibility = 'hidden';
			t.rows[39].style.display = 'none';
		}
		if(document.getElementById('chkAmericanRouletteGame').checked==false){
			t.rows[40].style.visibility = 'hidden';
			t.rows[40].style.display = 'none';
		}else{
			//t.rows[26].style.visibility = 'visible';
			//t.rows[26].style.display = 'table-row';
			t.rows[40].style.visibility = 'hidden';
			t.rows[40].style.display = 'none';
		}
		
		//hide for the maintime
		if(document.getElementById('chkSlotGame').checked==false){
			t.rows[41].style.visibility = 'hidden';
			t.rows[41].style.display = 'none';
		}else{
			t.rows[41].style.visibility = 'visible';
			t.rows[41].style.display = 'table-row';
		}
	}else{
		document.getElementById('btnApplySlotsVegasSharing').disabled=true;//enable apply button
		document.getElementById('txtSlotsVegasSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplySlotsVegasCommission').disabled=true;//enable apply button
		document.getElementById('txtSlotsVegasCommissionForAll').disabled=true;//enable apply box
		t.rows[36].style.visibility = 'hidden';
		t.rows[36].style.display = 'none';
		t.rows[37].style.visibility = 'hidden';
		t.rows[37].style.display = 'none';
		t.rows[38].style.visibility = 'hidden';
		t.rows[38].style.display = 'none';
		t.rows[39].style.visibility = 'hidden';
		t.rows[39].style.display = 'none';
		t.rows[40].style.visibility = 'hidden';
		t.rows[40].style.display = 'none';
		t.rows[41].style.visibility = 'hidden';
		t.rows[41].style.display = 'none';
		
	}
	
	//hide baccarat limit if hatien and savan are disabled
	if(document.getElementById('chkHtv999').checked==false && document.getElementById('chkSavan999').checked==false){
		document.getElementById('qry_baccarat').style.display='none';
		document.getElementById('qry_roullete').style.display='none';
		document.getElementById('qry_dragon_tiger').style.display='none';
		document.getElementById('dv_limits_msg').style.display='none';
	}else{
		document.getElementById('qry_baccarat').style.display='block';
		document.getElementById('qry_roullete').style.display='block';
		document.getElementById('qry_dragon_tiger').style.display='block';
		if(document.getElementById('chkBacarratGame').checked==false && document.getElementById('chkRouletteGame').checked==false && document.getElementById('chkDragonTigerGame').checked==false){
			document.getElementById('dv_limits_msg').style.display='none';
		}else{
			document.getElementById('dv_limits_msg').style.display='block';
		}
	}
}
$(function() {
	//var autoComList;
	jQuery.ajax({
		url: urlGameSharingAndCommission,
		method: 'POST',
		data: {'task': 'getCommission'},
		context: '',
		success: function(msg){
			var data=msg.split(';');
			var autoBaccaratSharingList=data[3].split(',');
			$('#txtBacarratSharing').autocomplete({
				source: autoBaccaratSharingList
			});
			$('#txtBacarratSharing_htv').autocomplete({
				source: autoBaccaratSharingList
			});
			$('#txtBacarratSharing_savan').autocomplete({
				source: autoBaccaratSharingList
			});
			$('#txtBacarratSharing_virtua').autocomplete({
				source: autoBaccaratSharingList
			});
			var autoBaccaratComList=data[0].split(',');
			$("#txtBacarratCommision").autocomplete({
				source: autoBaccaratComList
			});
			$("#txtBacarratCommision_htv").autocomplete({
				source: autoBaccaratComList
			});
			$("#txtBacarratCommision_savan").autocomplete({
				source: autoBaccaratComList
			});
			$("#txtBacarratCommision_virtua").autocomplete({
				source: autoBaccaratComList
			});
			
			var autoRouletteSharingList=data[4].split(',');
			$("#txtRouletteSharing").autocomplete({
				source: autoRouletteSharingList
			});
			$("#txtRouletteSharing_htv").autocomplete({
				source: autoRouletteSharingList
			});
			$("#txtRouletteSharing_savan").autocomplete({
				source: autoRouletteSharingList
			});
			$("#txtRouletteSharing_virtua").autocomplete({
				source: autoRouletteSharingList
			});
			var autoRouletteComList=data[1].split(',');
			$("#txtRouletteCommision").autocomplete({
				source: autoRouletteComList
			});
			$("#txtRouletteCommision_htv").autocomplete({
				source: autoRouletteComList
			});
			$("#txtRouletteCommision_savan").autocomplete({
				source: autoRouletteComList
			});
			$("#txtRouletteCommision_virtua").autocomplete({
				source: autoRouletteComList
			});
			
			var autoDragonTigerSharingList=data[5].split(',');
			$("#txtDragonTigerSharing").autocomplete({
				source: autoDragonTigerSharingList
			});
			$("#txtDragonTigerSharing_htv").autocomplete({
				source: autoDragonTigerSharingList
			});
			$("#txtDragonTigerSharing_savan").autocomplete({
				source: autoDragonTigerSharingList
			});
			$("#txtDragonTigerSharing_virtua").autocomplete({
				source: autoDragonTigerSharingList
			});
			var autoDragonTigerComList=data[2].split(',');
			$("#txtDragonTigerCommision").autocomplete({
				source: autoDragonTigerComList
			});
			$("#txtDragonTigerCommision_htv").autocomplete({
				source: autoDragonTigerComList
			});
			$("#txtDragonTigerCommision_savan").autocomplete({
				source: autoDragonTigerComList
			});
			$("#txtDragonTigerCommision_virtua").autocomplete({
				source: autoDragonTigerComList
			});
			
			var autoSlotsSharingList=data[6].split(',');
			$("#txtSlotsSharing").autocomplete({
				source: autoSlotsSharingList
			});
			$("#txtSlotsSharing_htv").autocomplete({
				source: autoSlotsSharingList
			});
			$("#txtSlotsSharing_savan").autocomplete({
				source: autoSlotsSharingList
			});
			$("#txtSlotsSharing_virtua").autocomplete({
				source: autoSlotsSharingList
			});
			var autoSlotsComList=data[7].split(',');
			$("#txtSlotsCommision").autocomplete({
				source: autoSlotsComList
			});
			$("#txtSlotsCommision_htv").autocomplete({
				source: autoSlotsComList
			});
			$("#txtSlotsCommision_savan").autocomplete({
				source: autoSlotsComList
			});
			$("#txtSlotsCommision_virtua").autocomplete({
				source: autoSlotsComList
			});
			var autoAmericanRouletteSharingList=data[8].split(',');
			$("#txtAmericanRouletteSharing").autocomplete({
				source: autoAmericanRouletteSharingList
			});
			$("#txtAmericanRouletteSharing_htv").autocomplete({
				source: autoAmericanRouletteSharingList
			});
			$("#txtAmericanRouletteSharing_savan").autocomplete({
				source: autoAmericanRouletteSharingList
			});
			$("#txtAmericanRouletteSharing_virtua").autocomplete({
				source: autoAmericanRouletteSharingList
			});
			var autoAmericanRouletteComList=data[9].split(',');
			$("#txtAmericanRouletteCommision").autocomplete({
				source: autoAmericanRouletteComList
			});
			$("#txtAmericanRouletteCommision_htv").autocomplete({
				source: autoAmericanRouletteComList
			});
			$("#txtAmericanRouletteCommision_savan").autocomplete({
				source: autoAmericanRouletteComList
			});
			$("#txtAmericanRouletteCommision_virtua").autocomplete({
				source: autoAmericanRouletteComList
			});

			var autoSlotSharingList=data[10].split(',');
			$("#txtSlotSharing").autocomplete({
				source: autoSlotSharingList
			});
			$("#txtSlotSharing_htv").autocomplete({
				source: autoSlotSharingList
			});
			$("#txtSlotSharing_savan").autocomplete({
				source: autoSlotSharingList
			});
			$("#txtSlotSharing_virtua").autocomplete({
				source: autoSlotSharingList
			});
			var autoSlotComList=data[11].split(',');
			$("#txtSlotCommision").autocomplete({
				source: autoSlotComList
			});
			$("#txtSlotCommision_htv").autocomplete({
				source: autoSlotComList
			});
			$("#txtSlotCommision_savan").autocomplete({
				source: autoSlotComList
			});
			$("#txtSlotCommision_virtua").autocomplete({
				source: autoSlotComList
			});
    	}
	});
	
});
function checkAvailability(urlCheckAvailability){
	var account_ID='';
	if(trim(document.getElementById('txtPreAccountID').value)==''){
		account_ID=document.getElementById('cmbAccountID1').value + document.getElementById('cmbAccountID2').value;
	}else{
		account_ID=trim(document.getElementById('txtPreAccountID').value) + document.getElementById('cmbAccountID1').value + document.getElementById('cmbAccountID2').value;
	}
	jQuery.ajax({
		url: urlCheckAvailability,
		type: 'POST',
		data: {'type': checkAvailabilityIssetType,'account_ID': account_ID},
		success: function(msg) {
			if(msg==1){
    			alert(lblAccountExist);
    		}else{
        		alert(lblAccountAvailable);
        	}
    	}
	});
	
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function validate(text)
{
	var pattern = /^-?[0-9]+(.[0-9]{1,2})?$/; 
    if (text.match(pattern)==null) 
    {
		return false;
    }
	else
	{
		return true;
	}
}

function defaultValueForSharingAndCommission(){
	//costa
	if(document.getElementById('txtBacarratSharing').value==''){document.getElementById('txtBacarratSharing').value=0;}
	if(document.getElementById('txtBacarratCommision').value==''){document.getElementById('txtBacarratCommision').value=0;}
	if(document.getElementById('txtRouletteSharing').value==''){document.getElementById('txtRouletteSharing').value=0;}
	if(document.getElementById('txtRouletteCommision').value==''){document.getElementById('txtRouletteCommision').value=0;}
	if(document.getElementById('txtDragonTigerSharing').value==''){document.getElementById('txtDragonTigerSharing').value=0;}
	if(document.getElementById('txtDragonTigerCommision').value==''){document.getElementById('txtDragonTigerCommision').value=0;}
	if(document.getElementById('txtSlotsSharing').value==''){document.getElementById('txtSlotsSharing').value=0;}
	if(document.getElementById('txtSlotsCommision').value==''){document.getElementById('txtSlotsCommision').value=0;}
	if(document.getElementById('txtAmericanRouletteSharing').value==''){document.getElementById('txtAmericanRouletteSharing').value=0;}
	if(document.getElementById('txtAmericanRouletteCommision').value==''){document.getElementById('txtAmericanRouletteCommision').value=0;}
	if(document.getElementById('txtSlotSharing').value==''){document.getElementById('txtSlotSharing').value=0;}
	if(document.getElementById('txtSlotCommision').value==''){document.getElementById('txtSlotCommision').value=0;}

	//hatien
	if(document.getElementById('txtBacarratSharing_htv').value==''){document.getElementById('txtBacarratSharing_htv').value=0;}
	if(document.getElementById('txtBacarratCommision_htv').value==''){document.getElementById('txtBacarratCommision_htv').value=0;}
	if(document.getElementById('txtRouletteSharing_htv').value==''){document.getElementById('txtRouletteSharing_htv').value=0;}
	if(document.getElementById('txtRouletteCommision_htv').value==''){document.getElementById('txtRouletteCommision_htv').value=0;}
	if(document.getElementById('txtDragonTigerSharing_htv').value==''){document.getElementById('txtDragonTigerSharing_htv').value=0;}
	if(document.getElementById('txtDragonTigerCommision_htv').value==''){document.getElementById('txtDragonTigerCommision_htv').value=0;}
	if(document.getElementById('txtSlotsSharing_htv').value==''){document.getElementById('txtSlotsSharing_htv').value=0;}
	if(document.getElementById('txtSlotsCommision_htv').value==''){document.getElementById('txtSlotsCommision_htv').value=0;}
	if(document.getElementById('txtAmericanRouletteSharing_htv').value==''){document.getElementById('txtAmericanRouletteSharing_htv').value=0;}
	if(document.getElementById('txtAmericanRouletteCommision_htv').value==''){document.getElementById('txtAmericanRouletteCommision_htv').value=0;}
	if(document.getElementById('txtSlotSharing_htv').value==''){document.getElementById('txtSlotSharing_htv').value=0;}
	if(document.getElementById('txtSlotCommision_htv').value==''){document.getElementById('txtSlotCommision_htv').value=0;}

	//savan
	if(document.getElementById('txtBacarratSharing_savan').value==''){document.getElementById('txtBacarratSharing_savan').value=0;}
	if(document.getElementById('txtBacarratCommision_savan').value==''){document.getElementById('txtBacarratCommision_savan').value=0;}
	if(document.getElementById('txtRouletteSharing_savan').value==''){document.getElementById('txtRouletteSharing_savan').value=0;}
	if(document.getElementById('txtRouletteCommision_savan').value==''){document.getElementById('txtRouletteCommision_savan').value=0;}
	if(document.getElementById('txtDragonTigerSharing_savan').value==''){document.getElementById('txtDragonTigerSharing_savan').value=0;}
	if(document.getElementById('txtDragonTigerCommision_savan').value==''){document.getElementById('txtDragonTigerCommision_savan').value=0;}
	if(document.getElementById('txtSlotsSharing_savan').value==''){document.getElementById('txtSlotsSharing_savan').value=0;}
	if(document.getElementById('txtSlotsCommision_savan').value==''){document.getElementById('txtSlotsCommision_savan').value=0;}
	if(document.getElementById('txtAmericanRouletteSharing_savan').value==''){document.getElementById('txtAmericanRouletteSharing_savan').value=0;}
	if(document.getElementById('txtAmericanRouletteCommision_savan').value==''){document.getElementById('txtAmericanRouletteCommision_savan').value=0;}
	if(document.getElementById('txtSlotSharing_savan').value==''){document.getElementById('txtSlotSharing_savan').value=0;}
	if(document.getElementById('txtSlotCommision_savan').value==''){document.getElementById('txtSlotCommision_savan').value=0;}

	//virtua
	if(document.getElementById('txtBacarratSharing_virtua').value==''){document.getElementById('txtBacarratSharing_virtua').value=0;}
	if(document.getElementById('txtBacarratCommision_virtua').value==''){document.getElementById('txtBacarratCommision_virtua').value=0;}
	if(document.getElementById('txtRouletteSharing_virtua').value==''){document.getElementById('txtRouletteSharing_virtua').value=0;}
	if(document.getElementById('txtRouletteCommision_virtua').value==''){document.getElementById('txtRouletteCommision_virtua').value=0;}
	if(document.getElementById('txtDragonTigerSharing_virtua').value==''){document.getElementById('txtDragonTigerSharing_virtua').value=0;}
	if(document.getElementById('txtDragonTigerCommision_virtua').value==''){document.getElementById('txtDragonTigerCommision_virtua').value=0;}
	if(document.getElementById('txtSlotsSharing_virtua').value==''){document.getElementById('txtSlotsSharing_virtua').value=0;}
	if(document.getElementById('txtSlotsCommision_virtua').value==''){document.getElementById('txtSlotsCommision_virtua').value=0;}
	if(document.getElementById('txtAmericanRouletteSharing_virtua').value==''){document.getElementById('txtAmericanRouletteSharing_virtua').value=0;}
	if(document.getElementById('txtAmericanRouletteCommision_virtua').value==''){document.getElementById('txtAmericanRouletteCommision_virtua').value=0;}
	if(document.getElementById('txtSlotSharing_virtua').value==''){document.getElementById('txtSlotSharing_virtua').value=0;}
	if(document.getElementById('txtSlotCommision_virtua').value==''){document.getElementById('txtSlotCommision_virtua').value=0;}
	
	//Slotsvegas
	if(document.getElementById('txtBacarratSharing_slots').value==''){document.getElementById('txtBacarratSharing_slots').value=0;}
	if(document.getElementById('txtBacarratCommision_slots').value==''){document.getElementById('txtBacarratCommision_slots').value=0;}
	if(document.getElementById('txtRouletteSharing_slots').value==''){document.getElementById('txtRouletteSharing_slots').value=0;}
	if(document.getElementById('txtRouletteCommision_slots').value==''){document.getElementById('txtRouletteCommision_slots').value=0;}
	if(document.getElementById('txtDragonTigerSharing_slots').value==''){document.getElementById('txtDragonTigerSharing_slots').value=0;}
	if(document.getElementById('txtDragonTigerCommision_slots').value==''){document.getElementById('txtDragonTigerCommision_slots').value=0;}
	if(document.getElementById('txtSlotsSharing_slots').value==''){document.getElementById('txtSlotsSharing_slots').value=0;}
	if(document.getElementById('txtSlotsCommision_slots').value==''){document.getElementById('txtSlotsCommision_slots').value=0;}
	if(document.getElementById('txtAmericanRouletteSharing_slots').value==''){document.getElementById('txtAmericanRouletteSharing_slots').value=0;}
	if(document.getElementById('txtAmericanRouletteCommision_slots').value==''){document.getElementById('txtAmericanRouletteCommision_slots').value=0;}
	if(document.getElementById('txtSlotSharing_slots').value==''){document.getElementById('txtSlotSharing_slots').value=0;}
	if(document.getElementById('txtSlotCommision_slots').value==''){document.getElementById('txtSlotCommision_slots').value=0;}
}

function isSelectGameNull(){
	var s_select=0;
	if (document.getElementById('chkBacarratGame').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkRouletteGame').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkDragonTigerGame').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkSlotsGame').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkAmericanRouletteGame').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkSlotGame').checked==true){
		s_select+=1;
	}
	if (s_select==0){
		return false;
	}
	else{
		return true;
	}
}

function isSelectCasinoNull(){
	var s_select=0;
	if (document.getElementById('chkHtv999').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkSavanVegas999').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkCostaVegas999').checked==true){
		s_select+=1;
	}
	if (document.getElementById('chkVirtuaVegas999').checked==true){
		s_select+=1;
	}
	if (s_select==0){
		return false;
	}
	else{
		return true;
	}
}

function clearTextBox(){
	//Clear textboxes if not selected
	if(document.getElementById('chkBacarratGame').checked==false){
		document.getElementById('txtBacarratSharing').value='';
		document.getElementById('txtBacarratSharing_htv').value='';
		document.getElementById('txtBacarratSharing_savan').value='';
		document.getElementById('txtBacarratSharing_virtua').value='';
		document.getElementById('txtBacarratSharing_slots').value='';
		document.getElementById('txtBacarratCommision').value='';
		document.getElementById('txtBacarratCommision_htv').value='';
		document.getElementById('txtBacarratCommision_savan').value='';
		document.getElementById('txtBacarratCommision_virtua').value='';
		document.getElementById('txtBacarratCommision_slots').value='';
	}
	if(document.getElementById('chkRouletteGame').checked==false){
		document.getElementById('txtRouletteSharing').value='';
		document.getElementById('txtRouletteSharing_htv').value='';
		document.getElementById('txtRouletteSharing_savan').value='';
		document.getElementById('txtRouletteSharing_virtua').value='';
		document.getElementById('txtRouletteSharing_slots').value='';
		document.getElementById('txtRouletteCommision').value='';
		document.getElementById('txtRouletteCommision_htv').value='';
		document.getElementById('txtRouletteCommision_savan').value='';
		document.getElementById('txtRouletteCommision_virtua').value='';
		document.getElementById('txtRouletteCommision_slots').value='';
	}
	if(document.getElementById('chkDragonTigerGame').checked==false){
		document.getElementById('txtDragonTigerSharing').value='';
		document.getElementById('txtDragonTigerSharing_htv').value='';
		document.getElementById('txtDragonTigerSharing_savan').value='';
		document.getElementById('txtDragonTigerSharing_virtua').value='';
		document.getElementById('txtDragonTigerSharing_slots').value='';
		document.getElementById('txtDragonTigerCommision').value='';
		document.getElementById('txtDragonTigerCommision_htv').value='';
		document.getElementById('txtDragonTigerCommision_savan').value='';
		document.getElementById('txtDragonTigerCommision_virtua').value='';
		document.getElementById('txtDragonTigerCommision_slots').value='';
	}
	if(document.getElementById('chkSlotsGame').checked==false){
		document.getElementById('txtSlotsSharing').value='';
		document.getElementById('txtSlotsSharing_htv').value='';
		document.getElementById('txtSlotsSharing_savan').value='';
		document.getElementById('txtSlotsSharing_virtua').value='';
		document.getElementById('txtSlotsSharing_slots').value='';
		document.getElementById('txtSlotsCommision').value='';
		document.getElementById('txtSlotsCommision_htv').value='';
		document.getElementById('txtSlotsCommision_savan').value='';
		document.getElementById('txtSlotsCommision_virtua').value='';
		document.getElementById('txtSlotsCommision_slots').value='';
	}
	if(document.getElementById('chkAmericanRouletteGame').checked==false){
		document.getElementById('txtAmericanRouletteSharing').value='';
		document.getElementById('txtAmericanRouletteSharing_htv').value='';
		document.getElementById('txtAmericanRouletteSharing_savan').value='';
		document.getElementById('txtAmericanRouletteSharing_virtua').value='';
		document.getElementById('txtAmericanRouletteSharing_slots').value='';
		document.getElementById('txtAmericanRouletteCommision').value='';
		document.getElementById('txtAmericanRouletteCommision_htv').value='';
		document.getElementById('txtAmericanRouletteCommision_savan').value='';
		document.getElementById('txtAmericanRouletteCommision_virtua').value='';
		document.getElementById('txtAmericanRouletteCommision_slots').value='';
	}
	if(document.getElementById('chkSlotGame').checked==false){
		document.getElementById('txtSlotSharing').value='';
		document.getElementById('txtSlotSharing_htv').value='';
		document.getElementById('txtSlotSharing_savan').value='';
		document.getElementById('txtSlotSharing_virtua').value='';
		document.getElementById('txtSlotSharing_slots').value='';
		document.getElementById('txtSlotCommision').value='';
		document.getElementById('txtSlotCommision_htv').value='';
		document.getElementById('txtSlotCommision_savan').value='';
		document.getElementById('txtSlotCommision_virtua').value='';
		document.getElementById('txtSlotCommision_slots').value='';
	}
}
function reSet()
{
	location.reload(true);
}

function casinoCostaCheck(id){
	/*
	if(document.getElementById(id).checked==false){
		if(document.getElementById('chkSlotsGame').disabled==false){
			document.getElementById('chkSlotsGame').checked=false;
			document.getElementById('chkSlotsGame').disabled=true;
		}
		if(document.getElementById('chkAmericanRouletteGame').disabled==false){
			document.getElementById('chkAmericanRouletteGame').checked=false;
			document.getElementById('chkAmericanRouletteGame').disabled=true;
		}
	}else{
		if(document.getElementById('chkSlotsGame').disabled==false){
			document.getElementById('chkSlotsGame').disabled=false;
		}
		if(document.getElementById('chkAmericanRouletteGame').disabled==false){
			document.getElementById('chkAmericanRouletteGame').disabled=false;
		}
	}
	*/
}

function costaApplySharing(val){
	document.getElementById('txtBacarratSharing').value=val;
	document.getElementById('txtRouletteSharing').value=val;
	document.getElementById('txtSlotsSharing').value=val;
	document.getElementById('txtAmericanRouletteSharing').value=val;
	document.getElementById('txtSlotSharing').value=val;
}
function costaApplyCommission(val){
	document.getElementById('txtBacarratCommision').value=val;
	document.getElementById('txtRouletteCommision').value=val;
	document.getElementById('txtSlotsCommision').value=val;
	document.getElementById('txtAmericanRouletteCommision').value=val;
	document.getElementById('txtSlotCommision').value=val;
}
function hatienApplySharing(val){
	document.getElementById('txtBacarratSharing_htv').value=val;
	document.getElementById('txtRouletteSharing_htv').value=val;
	document.getElementById('txtDragonTigerSharing_htv').value=val;
	document.getElementById('txtSlotSharing_htv').value=val;
}
function hatienApplyCommission(val){
	document.getElementById('txtBacarratCommision_htv').value=val;
	document.getElementById('txtRouletteCommision_htv').value=val;
	document.getElementById('txtDragonTigerCommision_htv').value=val;
	document.getElementById('txtSlotCommision_htv').value=val;
}
function savanApplySharing(val){
	document.getElementById('txtBacarratSharing_savan').value=val;
	document.getElementById('txtRouletteSharing_savan').value=val;
	document.getElementById('txtDragonTigerSharing_savan').value=val;
	document.getElementById('txtSlotSharing_savan').value=val;
}
function savanApplyCommission(val){
	document.getElementById('txtBacarratCommision_savan').value=val;
	document.getElementById('txtRouletteCommision_savan').value=val;
	document.getElementById('txtDragonTigerCommision_savan').value=val;
	document.getElementById('txtSlotCommision_savan').value=val;
}
function virtuaApplySharing(val){
	document.getElementById('txtBacarratSharing_virtua').value=val;
	document.getElementById('txtRouletteSharing_virtua').value=val;
	document.getElementById('txtDragonTigerSharing_virtua').value=val;
	document.getElementById('txtSlotSharing_virtua').value=val;
}
function virtuaApplyCommission(val){
	document.getElementById('txtBacarratCommision_virtua').value=val;
	document.getElementById('txtRouletteCommision_virtua').value=val;
	document.getElementById('txtDragonTigerCommision_virtua').value=val;
	document.getElementById('txtSlotCommision_virtua').value=val;
}

function slotsVegasApplySharing(val){
	document.getElementById('txtBacarratSharing_slots').value=val;
	document.getElementById('txtRouletteSharing_slots').value=val;
	document.getElementById('txtDragonTigerSharing_slots').value=val;
	document.getElementById('txtSlotSharing_slots').value=val;
}
function slotsVegasApplyCommission(val){
	document.getElementById('txtBacarratCommision_slots').value=val;
	document.getElementById('txtRouletteCommision_slots').value=val;
	document.getElementById('txtDragonTigerCommision_slots').value=val;
	document.getElementById('txtSlotCommision_slots').value=val;
}

function percentValue()
{
	if(strPercentValueUserLevel!='MEM'){
		var fltCredit=document.getElementById('txtCredit').value;
		var intPercent=document.getElementById('txtCreditPercent').value;
		//document.getElementById('txtCreditPercentValue').value=fltCredit - (intPercent*fltCredit/100);
		if(document.getElementById('txtCredit').name==''){
			document.getElementById('txtAssignedCredit').value=fltCredit;
		}else{
			var fltOldCredit=parseFloat(document.getElementById('txtCredit').name);
			var fltAssignedCredit=parseFloat(document.getElementById('txtAssignedCredit').name);
			var fltTotalAssignedCredit=fltAssignedCredit + (parseFloat(fltCredit) - fltOldCredit);
			document.getElementById('txtAssignedCredit').value=fltTotalAssignedCredit;
		}
	}
}
function onTheLoad(){
	//get currency exchange_rate
	jQuery.ajax({
		url: urlGetCurrencyExchageRate,
		method: 'POST',
		data: {'currency_id': document.getElementById('cmbCurrency').value},
		success: function(msg) {
			document.getElementById('dvExchangeRate').innerHTML=msg;
    	}
	});
	document.getElementById('qry_baccarat').innerHTML='';
	document.getElementById('qry_roullete').innerHTML='';
	document.getElementById('qry_dragon_tiger').innerHTML='';
	document.getElementById('qry_blackjack').innerHTML='';
	document.getElementById('qry_american_roulette').innerHTML='';
	
	hideRows(document.getElementById('chkBacarratGame'),'tblBettingInformation',1,8,15,15,15,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked);
	hideRows(document.getElementById('chkRouletteGame'),'tblBettingInformation',2,9,16,16,16,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked);
	hideRows(document.getElementById('chkDragonTigerGame'),'tblBettingInformation',10,10,17,17,17,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked);
	hideRows(document.getElementById('chkSlotsGame'),'tblBettingInformation',4,4,4,4,4,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked);
	hideRows(document.getElementById('chkAmericanRouletteGame'),'tblBettingInformation',5,5,5,5,5,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked);
	hideRows(document.getElementById('chkSlotGame'),'tblBettingInformation',13,13,20,27,41,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked);

	
	tableBacarrat(document.getElementById('cmbCurrency').value);
	tableRoulette(document.getElementById('cmbCurrency').value);
	tableDragonTiger(document.getElementById('cmbCurrency').value);
	
	//hide baccarat limit if hatien and savan are disabled
	if(document.getElementById('chkHtv999').checked==false && document.getElementById('chkSavanVegas999').checked==false){
		document.getElementById('qry_baccarat').style.display='none';
		document.getElementById('qry_roullete').style.display='none';
		document.getElementById('qry_dragon_tiger').style.display='none';
		document.getElementById('dv_limits_msg').style.display='none';
	}else{
		document.getElementById('qry_baccarat').style.display='block';
		document.getElementById('qry_roullete').style.display='block';
		document.getElementById('qry_dragon_tiger').style.display='block';
		if(document.getElementById('chkBacarratGame').checked==false && document.getElementById('chkRouletteGame').checked==false && document.getElementById('chkDragonTigerGame').checked==false){
			document.getElementById('dv_limits_msg').style.display='none';
		}else{
			document.getElementById('dv_limits_msg').style.display='block';
		}
		
	}
	
	if (s_edit==0){
		percentValue();
	}else{
		jQuery.ajax({
    		url: urlGetAssignedCredit,
    		method: 'POST',
    		data: {'account_id': strAccountId},
    		success: function(data) {
    			
    			if(strPercentValueUserLevel!='MEM'){
    				var fltCredit=data;
    				var intPercent=document.getElementById('txtCreditPercent').value;
    				//document.getElementById('txtCreditPercentValue').value=fltCredit - (intPercent*fltCredit/100);
    			}else{
    				winMax();
    				winMaxDaily();
	    		}
	    	}
    	});
	}
	if(document.getElementById('chkCostaVegas999').checked==false){
		document.getElementById('btnApplyCostaSharing').disabled=true;//enable apply button
		document.getElementById('txtCostaSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplyCostaCommission').disabled=true;//enable apply button
		document.getElementById('txtCostaCommissionForAll').disabled=true;//enable apply box
	}else{
		document.getElementById('btnApplyCostaSharing').disabled=false;//enable apply button
		document.getElementById('txtCostaSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplyCostaCommission').disabled=false;//enable apply button
		document.getElementById('txtCostaCommissionForAll').disabled=false;//enable apply box
	}
	if(document.getElementById('chkHtv999').checked==false){
		document.getElementById('btnApplyHatienSharing').disabled=true;//enable apply button
		document.getElementById('txtHatienSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplyHatienCommission').disabled=true;//enable apply button
		document.getElementById('txtHatienCommissionForAll').disabled=true;//enable apply box
	}else{
		document.getElementById('btnApplyHatienSharing').disabled=false;//enable apply button
		document.getElementById('txtHatienSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplyHatienCommission').disabled=false;//enable apply button
		document.getElementById('txtHatienCommissionForAll').disabled=false;//enable apply box
	}
	if(document.getElementById('chkSavanVegas999').checked==false){
		document.getElementById('btnApplySavanSharing').disabled=true;//enable apply button
		document.getElementById('txtSavanSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplySavanCommission').disabled=true;//enable apply button
		document.getElementById('txtSavanCommissionForAll').disabled=true;//enable apply box
	}else{
		document.getElementById('btnApplySavanSharing').disabled=false;//enable apply button
		document.getElementById('txtSavanSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplySavanCommission').disabled=false;//enable apply button
		document.getElementById('txtSavanCommissionForAll').disabled=false;//enable apply box
	}
	if(document.getElementById('chkVirtuaVegas999').checked==false){
		document.getElementById('btnApplyVirtuaSharing').disabled=true;//enable apply button
		document.getElementById('txtVirtuaSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplyVirtuaCommission').disabled=true;//enable apply button
		document.getElementById('txtVirtuaCommissionForAll').disabled=true;//enable apply box
	}else{
		document.getElementById('btnApplyVirtuaSharing').disabled=false;//enable apply button
		document.getElementById('txtVirtuaSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplyVirtuaCommission').disabled=false;//enable apply button
		document.getElementById('txtVirtuaCommissionForAll').disabled=false;//enable apply box
	}
	if(document.getElementById('chkSlotsVegas999').checked==false){
		document.getElementById('btnApplySlotsVegasSharing').disabled=true;//enable apply button
		document.getElementById('txtSlotsVegasSharingForAll').disabled=true;//enable apply box
		document.getElementById('btnApplySlotsVegasCommission').disabled=true;//enable apply button
		document.getElementById('txtSlotsVegasCommissionForAll').disabled=true;//enable apply box
	}else{
		document.getElementById('btnApplySlotsVegasSharing').disabled=false;//enable apply button
		document.getElementById('txtSlotsVegasSharingForAll').disabled=false;//enable apply box
		document.getElementById('btnApplySlotsVegasCommission').disabled=false;//enable apply button
		document.getElementById('txtSlotsVegasCommissionForAll').disabled=false;//enable apply box
	}
}

function tableBacarrat(currency_id) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    if (document.getElementById('chkBacarratGame').checked==true){
    	document.getElementById("qry_baccarat").appendChild(divTag);
    }else{
    	document.getElementById("qry_baccarat").innerHTML='';
	}
	if(trim(document.getElementById('txtPreAccountID').value)!=''){
		//if(agent_type!='SC' && (agent_type!='1' || agent_type!='2' || agent_type!='3' || agent_type!='4' || agent_type!='5' || agent_type!='7' || agent_type!='8' || agent_type!='9' || agent_type!='10' || agent_type!='11')){
		if(agent_type!='SC' && (agent_type!='user')){
		    jQuery.ajax({
	    		url: urlGetSelectedGameTableLimit,
	    		method: 'POST',
	    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
	    		success: function(data) {
	    			tableBacarrat_grid_query(urlTableBaccaratLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
		    	}
	    	});
		}else{
			//if (agent_type=='1' || agent_type=='2' || agent_type=='3' || agent_type=='4' || agent_type=='5' || agent_type=='7' || agent_type=='8' || agent_type=='9' || agent_type=='10' || agent_type=='11'){
			if(agent_type=='user'){
				tableBacarrat_grid_query(urlTableBaccaratLimit + '&currency_id=' + currency_id);
			}else if(agent_type=='SC'){
				if(for_edit==1){
					jQuery.ajax({
			    		url: urlGetSelectedGameTableLimit,
			    		method: 'POST',
			    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
			    		success: function(data) {
			    			tableBacarrat_grid_query(urlTableBaccaratLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
				    	}
			    	});
				}else{
					//if(user_level=='1' || user_level=='2' || user_level=='3' || user_level=='4' || user_level=='5' || user_level=='7' || user_level=='8' || user_level=='9' || user_level=='10' || user_level=='11'){
					if(agent_type=='user'){
						jQuery.ajax({
				    		url: urlGetSelectedGameTableLimit,
				    		method: 'POST',
				    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
				    		success: function(data) {
				    			tableBacarrat_grid_query(urlTableBaccaratLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
					    	}
				    	});
					}
				}
			}
		}
	}else{
		tableBacarrat_grid_query(urlTableBaccaratLimit + '&currency_id=' + currency_id);
	}
}
function tableBacarrat_grid_query(qry_url)
{
	$(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: qry_url, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['Limit ID', lblAll,lblBankerPlayer,lblTie,lblPair,lblBig,lblSmall],
		    colModel: [
		      {name: 'no', index: 'no', width: 20, search:true, hidden: true},
		      {name: 'all', index: 'all', width: 70, sortable: false},
		      {name: 'banker_player', index: 'banker_player', width: 90, sortable: false},
		      {name: 'tie', index: 'tie', width: 70, sortable: false},
		      {name: 'pair', index: 'pair', width: 70,sortable: false},
		      {name: 'big', index: 'big', width: 65,sortable: false},
		      {name: 'small', index: 'small', width: 60,sortable: false}
		    ],
		    loadComplete: function(){
		    	jQuery.ajax({
		    		url: urlAgentGameTableLimit,
		    		method: 'POST',
		    		data: {'Account_ID': document.getElementById('txtPreAccountID').value},
		    		success: function(msg) {
		    			var data= msg.split(';');
		    			var bacarratLimitId=data[0].split(',');
		    			var i=0;
				    	var countRows=grid.jqGrid('getGridParam', 'records');
				    	if(s_edit=='1'){
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=bacarratLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == bacarratLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
					    }
			    	}
		    	});
		    	//grid stripe
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    $("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rowNum: 99999,
			width: 1098,
            multiselect: true,
            sortable: false,
		    sortname: 'no',
		    sortorder: 'ASC',
		    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>'+ lblBaccarat +' '+ lblLimit +'<font color="red">*</font></b> ('+ lblGameLimitNote +')</div>',
		    hidegrid: false,
		    viewrecords: true
		});
		$('#list1').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false});
	});
}

function tableRoulette(currency_id) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'list3'; 
    divTag.style.margin = "0px auto"; 
    if (document.getElementById('chkRouletteGame').checked==true){
    	document.getElementById("qry_roullete").appendChild(divTag);
    }else{
    	document.getElementById("qry_roullete").innerHTML='';
	}
    if(trim(document.getElementById('txtPreAccountID').value)!=''){
		//if(agent_type!='SC' && (agent_type!='1' || agent_type!='2' || agent_type!='3' || agent_type!='4' || agent_type!='5' || agent_type!='7' || agent_type!='8' || agent_type!='9' || agent_type!='10' || agent_type!='11')){
    	if(agent_type!='SC' && (agent_type!='user')){
		    jQuery.ajax({
	    		url: urlGetSelectedRouletteTableLimit,
	    		method: 'POST',
	    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
	    		success: function(data) {
	    			tableRoulette_grid_query(urlTableRouletteLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
		    	}
	    	});
		}else{
			//if(agent_type=='1' || agent_type=='2' || agent_type=='3' || agent_type=='4' || agent_type=='5' || agent_type=='7' || agent_type=='8' || agent_type=='9' || agent_type=='10' || agent_type=='11'){
			if(agent_type=='user'){
				tableRoulette_grid_query(urlTableRouletteLimit + '&currency_id=' + currency_id);
			}else if(agent_type=='SC'){
				if(for_edit==1){
				 	jQuery.ajax({
			    		url: urlGetSelectedRouletteTableLimit,
			    		method: 'POST',
			    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
			    		success: function(data) {
			    			tableRoulette_grid_query(urlTableRouletteLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
				    	}
			    	});
				}else{
					//if(user_level=='1' || user_level=='2' || user_level=='3' || user_level=='4' || user_level=='5' || user_level=='7' || user_level=='8' || user_level=='9' || user_level=='10' || user_level=='11'){
					if(agent_type=='user'){
						jQuery.ajax({
				    		url: urlGetSelectedRouletteTableLimit,
				    		method: 'POST',
				    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
				    		success: function(data) {
				    			tableRoulette_grid_query(urlTableRouletteLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
					    	}
				    	});
					}
				}
			}
		}	
	}else{
		tableRoulette_grid_query(urlTableRouletteLimit + '&currency_id=' + currency_id);
	}
} 

function tableRoulette_grid_query(qry_url){
	$(document).ready(function() {
		var grid=jQuery("#list3");
		grid.jqGrid({ 
			url: qry_url,
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['Limit ID', lblAll,'35 to 1','17 to 1','11 to 1','8 to 1','5 to 1','2 to 1','1 to 1'],
		    colModel: [
		      {name: 'no', index: 'no', width: 20, search:true, hidden: true},
		      {name: 'all', index: 'all', width: 50, sortable: false},
		      {name: 'r_35_1', index: 'r_35_1', width: 50, sortable: false},
		      {name: 'r_18_1', index: 'r_18_1', width: 50, sortable: false},
		      {name: 'r_12_1', index: 'r_12_1', width: 50, sortable: false},
		      {name: 'r_9_1', index: 'r_9_1', width: 50, sortable: false},
		      {name: 'r_6_1', index: 'r_6_1', width: 50, sortable: false},
		      {name: 'r_2_1', index: 'r_2_1', width: 50, sortable: false},
		      {name: 'r_1_1', index: 'r_1_1', width: 50, sortable: false}
		    ],
		    loadComplete: function(){
		    	jQuery.ajax({
		    		url: urlAgentGameTableLimit,
		    		method: 'POST',
		    		data: {'Account_ID': document.getElementById('txtPreAccountID').value},
		    		success: function(msg) {
		    			var data= msg.split(';');
		    			var bacarratLimitId=data[1].split(',');
		    			var i=0;
				    	var countRows=grid.jqGrid('getGridParam', 'records');
				    	if(s_edit=='1'){
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=bacarratLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == bacarratLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	}
		    	});
		    	//grid stripe
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    $("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rowNum: 99999,
			width: 1098,
			multiselect: true,
		    sortname: 'id',
		    sortorder: 'ASC',
		    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>'+ lblRoulette +' '+ lblLimit +'<font color="red">*</font></b> ('+ lblGameLimitNote +')</div>',
		    hidegrid: false,
		    viewrecords: true
		});
		$('#list3').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });
	});
}

function tableDragonTiger(currency_id) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'list2'; 
    divTag.style.margin = "0px auto"; 
    if (document.getElementById('chkDragonTigerGame').checked==true){
    	document.getElementById("qry_dragon_tiger").appendChild(divTag);
    }else{
    	document.getElementById("qry_dragon_tiger").innerHTML='';
	}
    if(trim(document.getElementById('txtPreAccountID').value)!=''){
		//if(agent_type!='SC' && (agent_type!='1' || agent_type!='2' || agent_type!='3' || agent_type!='4' || agent_type!='5' || agent_type!='7' || agent_type!='8' || agent_type!='9' || agent_type!='10' || agent_type!='11')){
    	if(agent_type!='SC' && (agent_type!='user')){
		    jQuery.ajax({
	    		url: urlGetSelectedDragonTableLimit,
	    		method: 'POST',
	    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
	    		success: function(data) {
	    			tableDragonTiger_grid_query(urlTableDragonTigerLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
		    	}
	    	});
		}else{
			//if(agent_type=='1' || agent_type=='2' || agent_type=='3' || agent_type=='4' || agent_type=='5' || agent_type=='7' || agent_type=='8' || agent_type=='9' || agent_type=='10' || agent_type=='11'){
			if(agent_type=='user'){
				tableDragonTiger_grid_query(urlTableDragonTigerLimit + '&currency_id=' + currency_id);
			}else if(agent_type=='SC'){
				if(for_edit==1){
					jQuery.ajax({
			    		url: urlGetSelectedDragonTableLimit,
			    		method: 'POST',
			    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
			    		success: function(data) {
			    			tableDragonTiger_grid_query(urlTableDragonTigerLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
				    	}
			    	});
				}else{
					if(agent_type=='user'){
					//if(user_level=='1' || user_level=='2' || user_level=='3' || user_level=='4' || user_level=='5' || user_level=='7' || user_level=='8' || user_level=='9' || user_level=='10' || user_level=='11'){
						jQuery.ajax({
				    		url: urlGetSelectedDragonTableLimit,
				    		method: 'POST',
				    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
				    		success: function(data) {
				    			tableDragonTiger_grid_query(urlTableDragonTigerLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
				    			
					    	}
				    	});
					}
				}
			}
		}
	}else{
		tableDragonTiger_grid_query(urlTableDragonTigerLimit + '&currency_id=' + currency_id);
	}
}

function tableDragonTiger_grid_query(qry_url){
	$(document).ready(function() {
		var grid=jQuery("#list2");
		grid.jqGrid({ 
			url: qry_url, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['Limit ID', lblAll,lblDragonTiger,lblTie],
		    colModel: [
		      {name: 'no', index: 'no', width: 20, search:true, hidden: true},
		      {name: 'all', index: 'all', width: 120, sortable: false},
		      {name: 'dragon_tiger', index: 'dragon_tiger', width: 120, sortable: false},
		      {name: 'tie', index: 'tie', width: 120, sortable: false}
		    ],
		    loadComplete: function(){
		    	jQuery.ajax({
		    		url: urlAgentGameTableLimit,
		    		method: 'POST',
		    		data: {'Account_ID': document.getElementById('txtPreAccountID').value},
		    		success: function(msg) {
		    			var data= msg.split(';');
		    			var bacarratLimitId=data[2].split(',');
		    			var i=0;
				    	var countRows=grid.jqGrid('getGridParam', 'records');
				    	if(s_edit=='1'){
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=bacarratLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == bacarratLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	}
		    	});
		    	//grid stripe
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    $("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rowNum: 99999,
			width: 1098,
			multiselect: true,
		    sortname: 'no',
		    sortorder: 'ASC',
		    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>'+ lblDragonTiger +' '+ lblLimit +'<font color="red">*</font></b> ('+ lblGameLimitNote +')</div>',
		    hidegrid: false,
		    viewrecords: true
		});
		$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });
	});
}
function tableBlackjack(currency_id) {
	var divTag = document.createElement("Table"); 
    divTag.id = 'list5'; 
    divTag.style.margin = "0px auto"; 
    if (document.getElementById('chkSlotsGame').checked==true){
    	document.getElementById("qry_blackjack").appendChild(divTag);
    }else{
    	document.getElementById("qry_blackjack").innerHTML='';
	}
	if(trim(document.getElementById('txtPreAccountID').value)!=''){
		if(agent_type!='SC' && (agent_type!='user')){
		//if(agent_type!='SC' && (agent_type!='1' || agent_type!='2' || agent_type!='3' || agent_type!='4' || agent_type!='5' || agent_type!='7' || agent_type!='8' || agent_type!='9' || agent_type!='10' || agent_type!='11')){
		    jQuery.ajax({
	    		url: urlGetSelectedBlackjackTableLimit,
	    		method: 'POST',
	    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
	    		success: function(data) {
	    			tableBlackjack_grid_query(urlTableBlackjackLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
		    	}
	    	});
		  
		}else{
			if(agent_type=='user'){
			//if (agent_type=='1' || agent_type=='2' || agent_type=='3' || agent_type=='4' || agent_type=='5' || agent_type=='7' || agent_type=='8' || agent_type=='9' || agent_type=='10' || agent_type=='11'){
				tableBlackjack_grid_query(urlTableBlackjackLimit + '&currency_id=' + currency_id);
			}else if(agent_type=='SC'){
				
				if(for_edit==1){
					jQuery.ajax({
			    		url: urlGetSelectedBlackjackTableLimit,
			    		method: 'POST',
			    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
			    		success: function(data) {
			    			tableBlackjack_grid_query(urlTableBlackjackLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
				    	}
			    	});
				}else{
					
					if(agent_type=='user'){
					//if(user_level=='1' || user_level=='2' || user_level=='3' || user_level=='4' || user_level=='5' || user_level=='7' || user_level=='8' || user_level=='9' || user_level=='10' || user_level=='11'){
						jQuery.ajax({
				    		url: urlGetSelectedBlackjackTableLimit,
				    		method: 'POST',
				    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
				    		success: function(data) {
				    			tableBlackjack_grid_query(urlTableBlackjackLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
					    	}
				    	});
					}
				}
			}
		}
	}else{
		tableBlackjack_grid_query(urlTableBlackjackLimit + '&currency_id=' + currency_id);
	}
}
function tableBlackjack_grid_query(qry_url)
{
	$(document).ready(function() {
		var grid=jQuery("#list5");
		grid.jqGrid({ 
			url: qry_url, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['No.', lblMain,lblPair,lblRummy,lblInsurance],
		    colModel: [
				{name: 'no', index: 'no', width: 30, search:true, hidden: true},
				{name: 'all', index: 'all', width: 50, sortable: false},
				{name: 'pair', index: 'pair', width: 50, sortable: false},
				{name: 'rummy', index: 'rummy', width: 50, sortable: false},
				{name: 'insurance', index: 'insurance', width: 50, sortable: false},
		    ],
		    loadComplete: function(){
		    	jQuery.ajax({
		    		url: urlAgentGameTableLimit,
		    		method: 'POST',
		    		data: {'Account_ID': document.getElementById('txtPreAccountID').value},
		    		success: function(msg) {
		    			var data= msg.split(';');
		    			var blackjackLimitId=data[3].split(',');
		    			var i=0;
				    	var countRows=grid.jqGrid('getGridParam', 'records');
				    	if(s_edit=='1'){
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=blackjackLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == blackjackLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	}
		    	});
		    	//grid stripe
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    $("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rowNum: 99999,
			width: 1098,
            multiselect: true,
            sortable: false,
		    sortname: 'no',
		    sortorder: 'ASC',
		    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>'+ lblBlackjack +' '+ lblLimit +'<font color="red">*</font></b> ('+ lblGameLimitNote +')</div>',
		    hidegrid: false,
		    viewrecords: true
		});
		$('#list5').jqGrid('navGrid', '#pager5', {edit: false, add: false, del:false});
	});
}

//Table for American Rouletee
function tableAmericanRoulette(currency_id) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'list6'; 
    divTag.style.margin = "0px auto"; 
    if (document.getElementById('chkAmericanRouletteGame').checked==true){
    	document.getElementById("qry_american_roulette").appendChild(divTag);
    }else{
    	document.getElementById("qry_american_roulette").innerHTML='';
	}
    if(trim(document.getElementById('txtPreAccountID').value)!=''){
		//if(agent_type!='SC' && (agent_type!='1' || agent_type!='2' || agent_type!='3' || agent_type!='4' || agent_type!='5' || agent_type!='7' || agent_type!='8' || agent_type!='9' || agent_type!='10' || agent_type!='11')){
    	if(agent_type!='SC' && (agent_type!='user')){
		    jQuery.ajax({
	    		url: urlGetSelectedAmericanRouletteTableLimit,
	    		method: 'POST',
	    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
	    		success: function(data) {
	    			tableAmericanRoulette_grid_query(urlTableAmericanRouletteLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
	    			
		    	}
	    	});
		}else{
			//if(agent_type=='1' || agent_type=='2' || agent_type=='3' || agent_type=='4' || agent_type=='5' || agent_type=='7' || agent_type=='8' || agent_type=='9' || agent_type=='10' || agent_type=='11'){
			if(agent_type=='user'){
				tableAmericanRoulette_grid_query(urlTableAmericanRouletteLimit + '&currency_id=' + currency_id);
			}else if(agent_type=='SC'){
				if(for_edit==1){
				 	jQuery.ajax({
			    		url: urlGetSelectedAmericanRouletteTableLimit,
			    		method: 'POST', 
			    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
			    		success: function(data) {
			    			tableAmericanRoulette_grid_query(urlTableAmericanRouletteLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
				    	}
			    	});
				}else{
					if(agent_type=='user'){
					//if(user_level=='1' || user_level=='2' || user_level=='3' || user_level=='4' || user_level=='5' || user_level=='7' || user_level=='8' || user_level=='9' || user_level=='10' || user_level=='11'){
						jQuery.ajax({
				    		url: urlGetSelectedAmericanRouletteTableLimit,
				    		method: 'POST',
				    		data: {'account_id': trim(document.getElementById('txtPreAccountID').value), 's_edit': s_edit},
				    		success: function(data) {
				    			tableAmericanRoulette_grid_query(urlTableAmericanRouletteLimit + '&currency_id=' + currency_id + '&limit_ids=' + data);
					    	}
				    	});
					}
				}
			}
		}	
	}else{
		tableAmericanRoulette_grid_query(urlTableAmericanRouletteLimit + '&currency_id=' + currency_id);
	}
}

function tableAmericanRoulette_grid_query(qry_url){
	$(document).ready(function() {
		var grid=jQuery("#list6");
		grid.jqGrid({ 
			url: qry_url,
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['No.', lblAll,'35 to 1','17 to 1','11 to 1','8 to 1','5 to 1','2 to 1','1 to 1'],
		    colModel: [
		      {name: 'no', index: 'no', width: 30, search:true, hidden: true},
		      {name: 'all', index: 'all', width: 50, sortable: false,hidden:true},
		      {name: 'r_35_1', index: 'r_35_1', width: 50, sortable: false},
		      {name: 'r_18_1', index: 'r_18_1', width: 50, sortable: false},
		      {name: 'r_12_1', index: 'r_12_1', width: 50, sortable: false},
		      {name: 'r_9_1', index: 'r_9_1', width: 50, sortable: false},
		      {name: 'r_6_1', index: 'r_6_1', width: 50, sortable: false},
		      {name: 'r_2_1', index: 'r_2_1', width: 50, sortable: false},
		      {name: 'r_1_1', index: 'r_1_1', width: 50, sortable: false}
		    ],
		    loadComplete: function(){
		    	jQuery.ajax({
		    		url: urlAgentGameTableLimit,
		    		method: 'POST',
		    		data: {'Account_ID': document.getElementById('txtPreAccountID').value},
		    		success: function(msg) {
		    			var data= msg.split(';');
		    			var bacarratLimitId=data[4].split(',');
		    			var i=0;
				    	var countRows=grid.jqGrid('getGridParam', 'records');
				    	if(s_edit=='1'){
					    	for(i=1;i<=countRows;i++){
								var e=0;
								for(e=0;e<=bacarratLimitId.length-1;e++){
						    		if (grid.jqGrid('getRowData', i).no == bacarratLimitId[e]){
						    			grid.setSelection(i,true);
							    	}
								}
						    }
				    	}
			    	}
		    	});
		    	//grid stripe
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    $("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rowNum: 99999,
			width: 1098,
			multiselect: true,
		    sortname: 'id',
		    sortorder: 'ASC',
		    caption: '<div id="cash_player_game_table_limit_grid_caption"><b>'+ lblAmericanRoulette +' '+ lblLimit +'<font color="red">*</font></b> ('+ lblGameLimitNote +')</div>',
		    hidegrid: false,
		    viewrecords: true
		});
		$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search:true });
	});
}

function winMax()
{
	var fltParam=0;
	if(document.getElementById('txtWinMaxMultiple').name!=''){
		fltParam=parseFloat(document.getElementById('txtWinMaxMultiple').name);
	}else{
		fltParam=parseFloat(document.getElementById('txtCredit').value);
	}
	if(parseFloat(document.getElementById('txtWinMaxMultiple').value)!=-1){
		
		if(parseFloat(document.getElementById('txtWinMaxMultiple').value)<=fltParam || parseFloat(document.getElementById('txtWinMaxMultiple').value) ==0.00)
		{
			if(parseFloat(document.getElementById('txtWinMaxMultiple').value)==0.00){
				document.getElementById('lblWinMax').innerHTML='<font color="black">You can login but cannot play any casino.</font>';
			}else{
				document.getElementById('lblWinMax').innerHTML='<font color="red">'+ lblWinMaxError1 +' '+ fltParam.toFixed(2) +'. '+ lblWinMaxError2 +'</font>';
			}
		}else{
			if(document.getElementById('txtWinMaxMultiple').value!=''){
				document.getElementById('lblWinMax').innerHTML=lblPlayerCanWin + ' ' + (parseFloat(document.getElementById('txtWinMaxMultiple').value)-fltParam).toFixed(2) + '.';
			}else{
				document.getElementById('lblWinMax').innerHTML='';
			}
		}
	}else{
		document.getElementById('lblWinMax').innerHTML=lblPlayerCanWinUnlimited;
	}
}
function winMaxDaily(){
	if(parseFloat(document.getElementById('txtDailyMaxWin').value)!=0){
		if(document.getElementById('txtDailyMaxWin').value!=''){
			document.getElementById('lblDailyMaxWin').innerHTML=lblPlayerCanWin + ' ' + parseFloat(document.getElementById('txtDailyMaxWin').value).toFixed(2) + ' ' + lblDaily + '.';
		}else{
			document.getElementById('lblDailyMaxWin').innerHTML='';
		}
	}else{
		document.getElementById('lblDailyMaxWin').innerHTML=lblPlayerCanWinUnlimited.replace(".","") + ' ' + lblDaily + '.';
	}
}
function checkRequiredFields(errCont){
	if(s_edit=='0'){
		if (trim(document.getElementById('txtPassword').value)==''){
			alert(lblPasswordRequired);
			document.getElementById('txtPassword').focus();
			return false;
		}else{
			errCont.value='';
		}
		if (trim(document.getElementById('txtPassword').value).length < 6 || trim(document.getElementById('txtPassword').value).length > 14){
			alert(lblInvalidPasswordLength);
			document.getElementById('txtPassword').focus();
			return false;
		}else{
			errCont.value='';
		}
		if (trim(document.getElementById('txtConfirmPassword').value)==''){
			alert(lblPleaseConfirmPassword);
			document.getElementById('txtConfirmPassword').focus();
			return false;
		}else{
			errCont.value='';
		}
		if (trim(document.getElementById('txtPassword').value) != trim(document.getElementById('txtConfirmPassword').value)){
			alert(lblTwoPassword);
			document.getElementById('txtConfirmPassword').focus();
			return false;
		}else{
			errCont.value='';
		}
	}else{
		if (trim(document.getElementById('txtPassword').value)!=''){
			
			if (trim(document.getElementById('txtPassword').value).length < 6 || trim(document.getElementById('txtPassword').value).length > 14){
				alert(lblInvalidPasswordLength);
				document.getElementById('txtPassword').focus();
				return false;
			}else{
				errCont.value='';
			}
			if (trim(document.getElementById('txtConfirmPassword').value)==''){
				alert(lblPleaseConfirmPassword);
				document.getElementById('txtConfirmPassword').focus();
				return false;
			}else{
				errCont.value='';
			}
			if (trim(document.getElementById('txtPassword').value) != trim(document.getElementById('txtConfirmPassword').value)){
				alert(lblTwoPassword);
				document.getElementById('txtConfirmPassword').focus();
				return false;
			}else{
				errCont.value='';
			}
		}
	}
	if (trim(document.getElementById('txtAccountName').value)==''){
		alert(lblAccountNameRequired);
		document.getElementById('txtAccountName').focus();
		return false;
	}else{//+=-`;':",.<>/?\|
		var strAccountName=document.getElementById('txtAccountName').value;
		if(strAccountName.search("~")>=0 || strAccountName.search("!")>=0 || strAccountName.search("@")>=0 || strAccountName.search("#")>=0
				|| RegExp("["+ strAccountName + "]").test("$")==true || RegExp("["+ strAccountName + "]").test("%")==true || RegExp("["+ strAccountName + "]").test("^")==true
				|| RegExp("["+ strAccountName + "]").test("&")==true || RegExp("["+ strAccountName + "]").test("*")==true || RegExp("["+ strAccountName + "]").test("(")==true
				|| RegExp("["+ strAccountName + "]").test(")")==true || RegExp("["+ strAccountName + "]").test("_")==true || RegExp("["+ strAccountName + "]").test("+")==true
				|| RegExp("["+ strAccountName + "]").test("=")==true || RegExp("["+ strAccountName + "]").test("-")==true || RegExp("["+ strAccountName + "]").test("`")==true
				|| RegExp("["+ strAccountName + "]").test(";")==true || RegExp("["+ strAccountName + "]").test("'")==true || RegExp("["+ strAccountName + "]").test(":")==true
				|| RegExp("["+ strAccountName + "]").test('"')==true || RegExp("["+ strAccountName + "]").test(",")==true || RegExp("["+ strAccountName + "]").test(".")==true
				|| RegExp("["+ strAccountName + "]").test("<")==true || RegExp("["+ strAccountName + "]").test(">")==true || RegExp("["+ strAccountName + "]").test("/")==true
				|| RegExp("["+ strAccountName + "]").test("?")==true || RegExp("["+ strAccountName.replace('\\','\\\\') + "]").test('\\')==true || RegExp("["+ strAccountName + "]").test("|")==true){
			
			alert('Special character(s) not allowed! \nSpecial Characters are ~!@#$%^&*()_+=-`;\':",.<>/?\\|');
			
			document.getElementById('txtAccountName').focus();
			return false;
		}
		errCont.value='';
	}
	if (trim(document.getElementById('txtAccountName').value).length > 14){
		alert(lblInvalidAccountName);
		document.getElementById('txtAccountName').focus();
		return false;
	}else{
		errCont.value='';
	}
	
	if (trim(document.getElementById('txtPhoneNumber').value)!=''){
		if(validate(trim(document.getElementById('txtPhoneNumber').value))==false){
			alert(lblInvalidPhoneNumber);
			document.getElementById('txtPhoneNumber').focus();
			return false;
		}else{
			errCont.value='';
		}
	}
	if (validate(document.getElementById('txtCredit').value)==false){
		alert(lblInvalidCredit);
		document.getElementById('txtCredit').focus();
		return false;
	}
	else{
		errCont.value='';
	}
	
	if(strPercentValueUserLevel!='MEM'){
		if (validate(document.getElementById('txtCreditPercent').value)==false){
			alert(lblInvalidSecurityDeposit);
			document.getElementById('txtCreditPercent').focus();
			return false;
		}
		else{
			errCont.value='';
		}
	}
	
//validation here
	if ((parseFloat(trim(document.getElementById('txtCredit').value)))!=parseFloat(trim(document.getElementById('txtCredit').name))){
		if ((parseFloat(trim(document.getElementById('txtCredit').value)))>parseFloat(trim(document.getElementById('txtCredit').title))){
			alert(lblCreditMustNotExceedToLimit);
			document.getElementById('txtCredit').focus();
			return false;
		}else{
			errCont.value='';
		}
		
		if (parseFloat(trim(document.getElementById('txtCredit').title))<1){
			alert(lblInsufficientCredit);
			document.getElementById('txtCredit').focus();
			return false;
		}else{
			errCont.value='';
		}
//lblCreditStartLimit
		if ((parseFloat(trim(document.getElementById('txtCredit').value)))<parseFloat(trim(document.getElementById('lblCreditStartLimit').innerHTML))){
			alert(lblCreditMustNotExceedToLimit);
			document.getElementById('txtCredit').focus();
			return false;
		}else{
			errCont.value='';
		}
	}
	
	if (s_member=='1'){
		if (trim(document.getElementById('txtWinMaxMultiple').value)!=''){
			if (validate(document.getElementById('txtWinMaxMultiple').value)==false){
				alert(lblInvalidWinMax);
				document.getElementById('txtWinMaxMultiple').focus();
				return false;
			}
			else{
				errCont.value='';
			}
		}else{
			alert(lblWinMaxIsRequired);
			document.getElementById('txtWinMaxMultiple').focus();
			return false;
		}
	}
	if(document.getElementById('chkBacarratGame').checked==true){
	//Check if limit is greater that the "to" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtBacarratSharing').value))>parseFloat(trim(document.getElementById('txtBacarratSharing').title))){
			alert(lblBaccaratSharingErrorMessage);
			document.getElementById('txtBacarratSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtBacarratCommision').value))>parseFloat(trim(document.getElementById('txtBacarratCommision').title))){
			alert(lblBaccaratCommissionErrorMessage);
			document.getElementById('txtBacarratCommision').focus();
			return false;
		}else{
			errCont.value='';
		}
	//htv
//		if(parseFloat(trim(document.getElementById('txtBacarratSharing_htv').value))>parseFloat(trim(document.getElementById('txtBacarratSharing_htv').title))){
//			alert(lblBaccaratSharingErrorMessage);
//			document.getElementById('txtBacarratSharing_htv').focus();
//			return false;
//		}else if(parseFloat(trim(document.getElementById('txtBacarratCommision_htv').value))>parseFloat(trim(document.getElementById('txtBacarratCommision_htv').title))){
//			alert(lblBaccaratCommissionErrorMessage);
//			document.getElementById('txtBacarratCommision_htv').focus();
//			return false;
//		}else{
//			errCont.value='';
//		}
	//savan
		if(parseFloat(trim(document.getElementById('txtBacarratSharing_savan').value))>parseFloat(trim(document.getElementById('txtBacarratSharing_savan').title))){
			alert(lblBaccaratSharingErrorMessage);
			document.getElementById('txtBacarratSharing_savan').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtBacarratCommision_savan').value))>parseFloat(trim(document.getElementById('txtBacarratCommision_savan').title))){
			alert(lblBaccaratCommissionErrorMessage);
			document.getElementById('txtBacarratCommision_savan').focus();
			return false;
		}else{
			errCont.value='';
		}
	
	//Check if limit is lesser that the "from" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtBacarratSharing').value))<parseFloat(trim(document.getElementById('lblShareBaccaratFrom').innerHTML))){
			alert(lblBaccaratSharingErrorMessage);
			document.getElementById('txtBacarratSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtBacarratCommision').value))<parseFloat(trim(document.getElementById('lblCommissionBaccaratFrom').innerHTML))){
			alert(lblBaccaratCommissionErrorMessage);
			document.getElementById('txtBacarratCommision').focus();
			return false;
		}else{
			errCont.value='';
		}
	//htv
//		if(parseFloat(trim(document.getElementById('txtBacarratSharing_htv').value))<parseFloat(trim(document.getElementById('lblShareBaccaratFrom_htv').innerHTML))){
//			alert(lblBaccaratSharingErrorMessage);
//			document.getElementById('txtBacarratSharing_htv').focus();
//			return false;
//		}else if(parseFloat(trim(document.getElementById('txtBacarratCommision_htv').value))<parseFloat(trim(document.getElementById('lblCommissionBaccaratFrom_htv').innerHTML))){
//			alert(lblBaccaratCommissionErrorMessage);
//			document.getElementById('txtBacarratCommision_htv').focus();
//			return false;
//		}else{
//			errCont.value='';
//		}
	//savan
		if(parseFloat(trim(document.getElementById('txtBacarratSharing_savan').value))<parseFloat(trim(document.getElementById('lblShareBaccaratFrom_savan').innerHTML))){
			alert(lblBaccaratSharingErrorMessage);
			document.getElementById('txtBacarratSharing_savan').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtBacarratCommision_savan').value))<parseFloat(trim(document.getElementById('lblCommissionBaccaratFrom_savan').innerHTML))){
			alert(lblBaccaratCommissionErrorMessage);
			document.getElementById('txtBacarratCommision_savan').focus();
			return false;
		}else{
			errCont.value='';
		}
	
	
	}
	
	if(document.getElementById('chkRouletteGame').checked==true){
	//Check if limit is greater that the "to" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtRouletteSharing').value)) > parseFloat(trim(document.getElementById('txtRouletteSharing').title))){
			alert(lblEuropeanSharingErrorMessage);
			document.getElementById('txtRouletteSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtRouletteCommision').value)) > parseFloat(trim(document.getElementById('txtRouletteCommision').title))){
			alert(lblEuropeanCommissionErrorMessage);
			document.getElementById('txtRouletteCommision').focus();
			return false;
		}else{
			errCont.value='';
		}
	//htv
//		if(parseFloat(trim(document.getElementById('txtRouletteSharing_htv').value)) > parseFloat(trim(document.getElementById('txtRouletteSharing_htv').title))){
//			alert(lblEuropeanSharingErrorMessage);
//			document.getElementById('txtRouletteSharing_htv').focus();
//			return false;
//		}else if(parseFloat(trim(document.getElementById('txtRouletteCommision_htv').value)) > parseFloat(trim(document.getElementById('txtRouletteCommision_htv').title))){
//			alert(lblEuropeanCommissionErrorMessage);
//			document.getElementById('txtRouletteCommision_htv').focus();
//			return false;
//		}else{
//			errCont.value='';
//		}
	//savan
		if(parseFloat(trim(document.getElementById('txtRouletteSharing_savan').value)) > parseFloat(trim(document.getElementById('txtRouletteSharing_savan').title))){
			alert(lblEuropeanSharingErrorMessage);
			document.getElementById('txtRouletteSharing_savan').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtRouletteCommision_savan').value)) > parseFloat(trim(document.getElementById('txtRouletteCommision_savan').title))){
			alert(lblEuropeanCommissionErrorMessage);
			document.getElementById('txtRouletteCommision_savan').focus();
			return false;
		}else{
			errCont.value='';
		}
	
	//Check if limit is lesser that the "from" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtRouletteSharing').value)) < parseFloat(trim(document.getElementById('lblShareRouletteFrom').innerHTML))){
			alert(lblEuropeanSharingErrorMessage);
			document.getElementById('txtRouletteSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtRouletteCommision').value)) < parseFloat(trim(document.getElementById('lblCommissionRouletteFrom').innerHTML))){
			alert(lblEuropeanCommissionErrorMessage);
			document.getElementById('txtRouletteCommision').focus();
			return false;
		}else{
			errCont.value='';
		}
	//htv
//		if(parseFloat(trim(document.getElementById('txtRouletteSharing_htv').value)) < parseFloat(trim(document.getElementById('lblShareRouletteFrom_htv').innerHTML))){
//			alert(lblEuropeanSharingErrorMessage);
//			document.getElementById('txtRouletteSharing_htv').focus();
//			return false;
//		}else if(parseFloat(trim(document.getElementById('txtRouletteCommision_htv').value)) < parseFloat(trim(document.getElementById('lblCommissionRouletteFrom_htv').innerHTML))){
//			alert(lblEuropeanCommissionErrorMessage);
//			document.getElementById('txtRouletteCommision_htv').focus();
//			return false;
//		}else{
//			errCont.value='';
//		}
	//savan
		if(parseFloat(trim(document.getElementById('txtRouletteSharing_savan').value)) < parseFloat(trim(document.getElementById('lblShareRouletteFrom_savan').innerHTML))){
			alert(lblEuropeanSharingErrorMessage);
			document.getElementById('txtRouletteSharing_savan').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtRouletteCommision_savan').value)) < parseFloat(trim(document.getElementById('lblCommissionRouletteFrom_savan').innerHTML))){
			alert(lblEuropeanCommissionErrorMessage);
			document.getElementById('txtRouletteCommision_savan').focus();
			return false;
		}else{
			errCont.value='';
		}
	
	}
	
	if(document.getElementById('chkDragonTigerGame').checked==true){
	//Check if limit is greater that the "to" limit

	//htv
//		if(parseFloat(trim(document.getElementById('txtDragonTigerSharing_htv').value)) > parseFloat(trim(document.getElementById('txtDragonTigerSharing_htv').title))){
//			alert(lblDragonSharingErrorMessage);
//			document.getElementById('txtDragonTigerSharing_htv').focus();
//			return false;
//		}else if(parseFloat(trim(document.getElementById('txtDragonTigerCommision_htv').value)) > parseFloat(trim(document.getElementById('txtDragonTigerCommision_htv').title))){
//			alert(lblDragonCommissionErrorMessage);
//			document.getElementById('txtDragonTigerCommision_htv').focus();
//			return false;
//		}else{
//			errCont.value='';
//		}
	//savan
		if(parseFloat(trim(document.getElementById('txtDragonTigerSharing_savan').value)) > parseFloat(trim(document.getElementById('txtDragonTigerSharing_savan').title))){
			alert(lblDragonSharingErrorMessage);
			document.getElementById('txtDragonTigerSharing_savan').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtDragonTigerCommision_savan').value)) > parseFloat(trim(document.getElementById('txtDragonTigerCommision_savan').title))){
			alert(lblDragonCommissionErrorMessage);
			document.getElementById('txtDragonTigerCommision_savan').focus();
			return false;
		}else{
			errCont.value='';
		}

	//htv
//		if(parseFloat(trim(document.getElementById('txtDragonTigerSharing_htv').value)) < parseFloat(trim(document.getElementById('lblShareDragonTigerFrom_htv').innerHTML))){
//			alert(lblDragonSharingErrorMessage);
//			document.getElementById('txtDragonTigerSharing_htv').focus();
//			return false;
//		}else if(parseFloat(trim(document.getElementById('txtDragonTigerCommision_htv').value)) < parseFloat(trim(document.getElementById('lblCommissionDragonTigerFrom_htv').innerHTML))){
//			alert(lblDragonCommissionErrorMessage);
//			document.getElementById('txtDragonTigerCommision_htv').focus();
//			return false;
//		}else{
//			errCont.value='';
//		}
	//savan
		if(parseFloat(trim(document.getElementById('txtDragonTigerSharing_savan').value)) < parseFloat(trim(document.getElementById('lblShareDragonTigerFrom_savan').innerHTML))){
			alert(lblDragonSharingErrorMessage);
			document.getElementById('txtDragonTigerSharing_savan').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtDragonTigerCommision_savan').value)) < parseFloat(trim(document.getElementById('lblCommissionDragonTigerFrom_savan').innerHTML))){
			alert(lblDragonCommissionErrorMessage);
			document.getElementById('txtDragonTigerCommision_savan').focus();
			return false;
		}else{
			errCont.value='';
		}
	
	}
	
	if(document.getElementById('chkSlotsGame').checked==true){
	//Check if limit is greater that the "to" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtSlotsSharing').value)) > parseFloat(trim(document.getElementById('txtSlotsSharing').title))){
			alert(lblBlackjackSharingErrorMessage);
			document.getElementById('txtSlotsSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtSlotsCommision').value)) > parseFloat(trim(document.getElementById('txtSlotsCommision').title))){
			alert(lblBlackjackCommissionErrorMessage);
			document.getElementById('txtSlotsCommision').focus();
			return false;
		}else{
			errCont.value='';
		}

	//Check if limit is lesser that the "from" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtSlotsSharing').value)) < parseFloat(trim(document.getElementById('lblShareBlackjackFrom').innerHTML))){
			alert(lblBlackjackSharingErrorMessage);
			document.getElementById('txtSlotsSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtSlotsCommision').value)) < parseFloat(trim(document.getElementById('lblCommissionBlackjackFrom').innerHTML))){
			alert(lblBlackjackCommissionErrorMessage);
			document.getElementById('txtSlotsCommision').focus();
			return false;
		}else{
			errCont.value='';
		}
		
		//Slotsvegas
		if(parseFloat(trim(document.getElementById('txtSlotSharing_slots').value)) < parseFloat(trim(document.getElementById('lblShareSlotsFrom_slots').innerHTML))){
			alert(lblSlotsSharingErrorMessage);
			document.getElementById('txtSlotSharing_slots').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtSlotCommision_slots').value)) < parseFloat(trim(document.getElementById('lblCommissionSlotsFrom_slots').innerHTML))){
			alert(lblSlotsCommissionErrorMessage);
			document.getElementById('txtSlotCommision_slots').focus();
			return false;
		}else{
			errCont.value='';
		}

	}
	
	if(document.getElementById('chkAmericanRouletteGame').checked==true){
	//Check if limit is greater that the "to" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtAmericanRouletteSharing').value)) > parseFloat(trim(document.getElementById('txtAmericanRouletteSharing').title))){
			alert(lblAmericanSharingErrorMessage);
			document.getElementById('txtAmericanRouletteSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtAmericanRouletteCommision').value)) > parseFloat(trim(document.getElementById('txtAmericanRouletteCommision').title))){
			alert(lblAmericanCommissionErrorMessage);
			document.getElementById('txtAmericanRouletteCommision').focus();
			return false;
		}else{
			errCont.value='';
		}

	//Check if limit is lesser that the "from" limit
	//costa
		if(parseFloat(trim(document.getElementById('txtAmericanRouletteSharing').value)) < parseFloat(trim(document.getElementById('lblShareAmericanRouletteFrom').innerHTML))){
			alert(lblAmericanSharingErrorMessage);
			document.getElementById('txtAmericanRouletteSharing').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtAmericanRouletteCommision').value)) < parseFloat(trim(document.getElementById('lblCommissionAmericanRouletteFrom').innerHTML))){
			alert(lblAmericanCommissionErrorMessage);
			document.getElementById('txtAmericanRouletteCommision').focus();
			return false;
		}else{
			errCont.value='';
		}

	
	}
	
	if(document.getElementById('chkSlotGame').checked==true){
	//Check if limit is greater that the "to" limit
	//costa
	//htv
//		if(parseFloat(trim(document.getElementById('txtSlotSharing_htv').value)) > parseFloat(trim(document.getElementById('txtSlotSharing_htv').title))){
//			alert(lblSlotsSharingErrorMessage);
//			document.getElementById('txtSlotSharing_htv').focus();
//			return false;
//		}else if(parseFloat(trim(document.getElementById('txtSlotCommision_htv').value)) > parseFloat(trim(document.getElementById('txtSlotCommision_htv').title))){
//			alert(lblSlotsCommissionErrorMessage);
//			document.getElementById('txtSlotCommision_htv').focus();
//			return false;
//		}else{
//			errCont.value='';
//		}
	//savan
		if(parseFloat(trim(document.getElementById('txtSlotSharing_savan').value)) > parseFloat(trim(document.getElementById('txtSlotSharing_savan').title))){
			alert(lblSlotsSharingErrorMessage);
			document.getElementById('txtSlotSharing_savan').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtSlotCommision_savan').value)) > parseFloat(trim(document.getElementById('txtSlotCommision_savan').title))){
			alert(lblSlotsCommissionErrorMessage);
			document.getElementById('txtSlotCommision_savan').focus();
			return false;
		}else{
			errCont.value='';
		}
	//virtua
		if(parseFloat(trim(document.getElementById('txtSlotSharing_virtua').value)) > parseFloat(trim(document.getElementById('txtSlotSharing_virtua').title))){
			alert(lblSlotsSharingErrorMessage);
			document.getElementById('txtSlotSharing_virtua').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtSlotCommision_virtua').value)) > parseFloat(trim(document.getElementById('txtSlotCommision_virtua').title))){
			alert(lblSlotsCommissionErrorMessage);
			document.getElementById('txtSlotCommision_virtua').focus();
			return false;
		}else{
			errCont.value='';
		}


		//Slotsvegas
		if(parseFloat(trim(document.getElementById('txtSlotSharing_slots').value)) > parseFloat(trim(document.getElementById('txtSlotSharing_slots').title))){
			alert(lblSlotsSharingErrorMessage);
			document.getElementById('txtSlotSharing_slots').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtSlotCommision_slots').value)) > parseFloat(trim(document.getElementById('txtSlotCommision_slots').title))){
			alert(lblSlotsCommissionErrorMessage);
			document.getElementById('txtSlotCommision_slots').focus();
			return false;
		}else{
			errCont.value='';
		}
		//Slotsvegas
		if(parseFloat(trim(document.getElementById('txtSlotSharing_slots').value)) < parseFloat(trim(document.getElementById('lblShareSlotsFrom_slots').innerHTML))){
			alert(lblSlotsSharingErrorMessage);
			document.getElementById('txtSlotSharing_slots').focus();
			return false;
		}else if(parseFloat(trim(document.getElementById('txtSlotCommision_slots').value)) < parseFloat(trim(document.getElementById('lblCommissionSlotsFrom_slots').innerHTML))){
			alert(lblSlotsCommissionErrorMessage);
			document.getElementById('txtSlotCommision_slots').focus();
			return false;
		}else{
			errCont.value='';
		}
	}
}

function cashierConfirm(){
	if(s_session_active==1){
		if(checkRequiredFields(document.getElementById('txtErrMessage'))==false){return false;}
		
		//Do not continue saving agent information if no casino selected.
		if(isSelectGameNull()==false){
			alert(lblSelectGameErrorMessage);
			return false;
		}
		//Clear textboxes if not selected
		clearTextBox();
		//permissions
		var permission=0;
		
		if(document.getElementById('chkAddPermission').checked==true && document.getElementById('chkModifyPermission').checked==false && document.getElementById('chkViewPermission').checked==false){
			permission=2;
		}else if(document.getElementById('chkAddPermission').checked==false && document.getElementById('chkModifyPermission').checked==true && document.getElementById('chkViewPermission').checked==false){
			permission=1;
		}else if(document.getElementById('chkAddPermission').checked==false && document.getElementById('chkModifyPermission').checked==false && document.getElementById('chkViewPermission').checked==true){
			permission=3;
		}else if(document.getElementById('chkAddPermission').checked==true && document.getElementById('chkModifyPermission').checked==true && document.getElementById('chkViewPermission').checked==false){
			permission=5;
		}else if(document.getElementById('chkAddPermission').checked==true && document.getElementById('chkModifyPermission').checked==false && document.getElementById('chkViewPermission').checked==true){
			permission=7;
		}else if(document.getElementById('chkAddPermission').checked==false && document.getElementById('chkModifyPermission').checked==true && document.getElementById('chkViewPermission').checked==true){
			permission=6;
		}else if(document.getElementById('chkAddPermission').checked==true && document.getElementById('chkModifyPermission').checked==true && document.getElementById('chkViewPermission').checked==true){
			permission=4;
		}
		//casino
		var htv999='';
		var savan999='';
		var costa999='';
		var virtua999='';
		var sportsbook999='';
		var slotsvegas999='';
		if (document.getElementById('chkHtv999').checked==true){
			htv999='1';
		}else{
			htv999='0';
		}
		if (document.getElementById('chkSavanVegas999').checked==true){
			savan999='1';
		}else{
			savan999='0';
		}
		if (document.getElementById('chkCostaVegas999').checked==true){
			costa999='1';
		}else{
			costa999='0';
		}
		if (document.getElementById('chkVirtuaVegas999').checked==true){
			virtua999='0';// Set as zero for now
		}else{
			virtua999='0';
		}
		if (document.getElementById('chkSportsBook999').checked==true){
			sportsbook999='0';// Set as zer for now
		}else{
			sportsbook999='0';
		}
		if (document.getElementById('chkSlotsVegas999').checked==true){
			slotsvegas999='1';// Set as zero for now
		}else{
			slotsvegas999='0';
		}
		if(s_edit=='0' && document.getElementById('cmbCurrency').value=='0'){
			alert('Please select currency!');
			document.getElementById('cmbCurrency').focus();
			return false;
		}
		
		//game
		var baccarat='';
		var roullete='';
		var dragontiger='';
		var blackjack='';
		var american_roulette='';
		var slot='';

		if (document.getElementById('chkBacarratGame').checked==true){
			//force user to select atleast one game limit when it's particular game selection is selected
			if(document.getElementById('qry_baccarat').style.display=='block' && jQuery('#list1').jqGrid('getGridParam','selarrrow').length==0){
				alert(lblSelectBaccaratError);
				return false;
			}
			baccarat='1';
		}else{
			baccarat='0';
		}
		if (document.getElementById('chkRouletteGame').checked==true){
			if(document.getElementById('qry_roullete').style.display=='block' && jQuery('#list3').jqGrid('getGridParam','selarrrow').length==0){
				alert(lblSelectEuropeanError);
				return false;
			}
			roullete='2';
		}else{
			roullete='0';
		}
		if (document.getElementById('chkDragonTigerGame').checked==true){
			if(document.getElementById('qry_dragon_tiger').style.display=='block' && jQuery('#list2').jqGrid('getGridParam','selarrrow').length==0){
				alert(lblSelectDragonError);
				return false;
			}
			dragontiger='4';
		}else{
			dragontiger='0';
		}
		if (document.getElementById('chkSlotsGame').checked==true){
			blackjack='3';
		}else{
			blackjack='0';
		}
		if (document.getElementById('chkAmericanRouletteGame').checked==true){
			american_roulette='5';
		}else{
			american_roulette='0';
		}

		if (document.getElementById('chkSlotGame').checked==true){
			slot='6';
		}else{
			slot='0';
		}
		
		var selected_baccarat_limit='';
		var selected_roullete_limit='';
		var selected_dragon_tiger_limit='';
		var selected_blackjack_limit=0;
		var selected_american_roulette_limit=0;

		
		//player can only select 4 limits per table limit.
		if(strPercentValueUserLevel=='MEM'){
			//baccarat
			if(jQuery('#list1').jqGrid('getGridParam','selarrrow')!=null){
				if(jQuery('#list1').jqGrid('getGridParam','selarrrow').length>4){
					alert(lblPlayerBaccaratLimitErrorMessage);
					return false;
				}
			}
			//roullete
			if(jQuery('#list3').jqGrid('getGridParam','selarrrow')!=null){
				if(jQuery('#list3').jqGrid('getGridParam','selarrrow').length>4){
					alert(lblPlayerRoulleteLimitErrorMessage);
					return false;
				}
			}
			//dragon tiger
			if(jQuery('#list2').jqGrid('getGridParam','selarrrow')!=null){
				if(jQuery('#list2').jqGrid('getGridParam','selarrrow').length>4){
					alert(lblPlayerDragonLimitErrorMessage);
					return false;
				}
			}
			//blackjack
			if(jQuery('#list5').jqGrid('getGridParam','selarrrow')!=null){
				if(jQuery('#list5').jqGrid('getGridParam','selarrrow').length>4){
					alert(lblPlayerBlackjackLimitErrorMessage);
					return false;
				}
			}
			//american roullete
			if(jQuery('#list6').jqGrid('getGridParam','selarrrow')!=null){
				if(jQuery('#list6').jqGrid('getGridParam','selarrrow').length>4){
					alert(lblPlayerAmericanLimitErrorMessage);
					return false;
				}
			}
		}
		
		if(document.getElementById('qry_baccarat').style.display=='block' && jQuery('#list1').jqGrid('getGridParam','selarrrow')!=null){
			var i=0;
			for (i=0;i<=jQuery('#list1').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_baccarat_limit+=jQuery('#list1').jqGrid('getRowData', jQuery('#list1').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_baccarat_limit=0;
		}
		if(document.getElementById('qry_roullete').style.display=='block' && jQuery('#list3').jqGrid('getGridParam','selarrrow')!=null){
			var i=0;
			for (i=0;i<=jQuery('#list3').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_roullete_limit+=jQuery('#list3').jqGrid('getRowData', jQuery('#list3').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_roullete_limit=0;
		}
		if(document.getElementById('qry_dragon_tiger').style.display=='block' && jQuery('#list2').jqGrid('getGridParam','selarrrow')!=null){
			var i=0;
			for (i=0;i<=jQuery('#list2').jqGrid('getGridParam','selarrrow').length-1;i++)
			{
				selected_dragon_tiger_limit+=jQuery('#list2').jqGrid('getRowData', jQuery('#list2').jqGrid('getGridParam','selarrrow')[i]).no + ",";
			}
		}else{
			selected_dragon_tiger_limit=0;
		}
		
		var account_ID='';
		
		if(trim(document.getElementById('txtPreAccountID').value)==''){
			account_ID=document.getElementById('cmbAccountID1').value + document.getElementById('cmbAccountID2').value;
		}else{
			
			if(s_edit==0){
				account_ID=trim(document.getElementById('txtPreAccountID').value) + document.getElementById('cmbAccountID1').value + document.getElementById('cmbAccountID2').value;
			}else{
				account_ID=trim(document.getElementById('txtPreAccountID').value);
			}
		}
		
		var intCreditPercent=0;
		var fltAssignedCredit=0;
		if(strPercentValueUserLevel!='MEM'){
			intCreditPercent=document.getElementById('txtCreditPercent').value;
			fltAssignedCredit=document.getElementById('txtAssignedCredit').value;
		}else{
			if(parseFloat(document.getElementById('txtWinMaxMultiple').value)!=-1 && parseFloat(document.getElementById('txtWinMaxMultiple').value)!=0){
				if(document.getElementById('txtWinMaxMultiple').name!=''){
					if(parseFloat(document.getElementById('txtWinMaxMultiple').value)<=parseFloat(document.getElementById('txtWinMaxMultiple').name)){
			    		alert(lblWinMaxError1 +' '+ document.getElementById('txtWinMaxMultiple').name +'. ' + lblWinMaxError2);
			    		document.getElementById('txtWinMaxMultiple').focus();
				    	return false;
				    }
				}else{
					if(parseFloat(document.getElementById('txtWinMaxMultiple').value)<=parseFloat(document.getElementById('txtCredit').value)){
			    		document.getElementById('txtWinMaxMultiple').focus();
				    	return false;
					}
				}
			}
		}
		
		defaultValueForSharingAndCommission();
		//rowValues();//Set sharing and commission to 0 if casino is unselected.
		var marketType = $('input[name=rdoMarketType]:checked').val();
		
		jQuery.ajax({
    		url: urlSaveNewSubCompany,
    		type: 'POST',
    		async: false,
    		data: {'account_id': account_ID,
        			'agent_parent_id': trim(document.getElementById('txtPreAccountID').value), 
        			'agent_type': strPercentValueUserLevel,
        			'account_name':document.getElementById('txtAccountName').value,
        			'agent_password':document.getElementById('txtPassword').value,
        			'phone_number':document.getElementById('txtPhoneNumber').value,
        			'agent_email':document.getElementById('txtEmail').value,
        			'credit':document.getElementById('txtCredit').value,
        			'credit_assigned':fltAssignedCredit,
        			'credit_before': document.getElementById('txtCredit').name,
        			'permission':permission,
        			'currency_id':document.getElementById('cmbCurrency').value,
        			'credit_percentage': intCreditPercent,
        			'baccarat' : baccarat,
            		'baccarat_commission': document.getElementById('txtBacarratCommision').value + '#' + document.getElementById('txtBacarratCommision_htv').value + '#' + document.getElementById('txtBacarratCommision_savan').value + '#0#0', 
            		'baccarat_sharing': document.getElementById('txtBacarratSharing').value + '#' + document.getElementById('txtBacarratSharing_htv').value + '#' + document.getElementById('txtBacarratSharing_savan').value + '#0#0',
            		'selected_baccarat_limit': selected_baccarat_limit,
            		'roullete':roullete, 
            		'roullete_commission': document.getElementById('txtRouletteCommision').value + '#' + document.getElementById('txtRouletteCommision_htv').value + '#' + document.getElementById('txtRouletteCommision_savan').value + '#0#0',
            		'roullete_sharing': document.getElementById('txtRouletteSharing').value + '#' + document.getElementById('txtRouletteSharing_htv').value + '#' + document.getElementById('txtRouletteSharing_savan').value + '#0#0',
            		'selected_roullete_limit': selected_roullete_limit, 
            		'dragontiger':dragontiger,
            		'dragontiger_commission': document.getElementById('txtDragonTigerCommision').value + '#' + document.getElementById('txtDragonTigerCommision_htv').value + '#' + document.getElementById('txtDragonTigerCommision_savan').value + '#0#0',
            		'dragontiger_sharing': document.getElementById('txtDragonTigerSharing').value + '#' + document.getElementById('txtDragonTigerSharing_htv').value + '#' + document.getElementById('txtDragonTigerSharing_savan').value + '#0#0',
            		'selected_dragon_tiger_limit': selected_dragon_tiger_limit,
            		'blackjack':blackjack,
            		'blackjack_commission': document.getElementById('txtSlotsCommision').value + '#' + document.getElementById('txtSlotsCommision_htv').value + '#' + document.getElementById('txtSlotsCommision_savan').value + '#0#0',
            		'blackjack_sharing': document.getElementById('txtSlotsSharing').value + '#' + document.getElementById('txtSlotsSharing_htv').value + '#' + document.getElementById('txtSlotsSharing_savan').value + '#0#0',
            		'selected_blackjack_limit': selected_blackjack_limit,
            		'american_roulette':american_roulette,
            		'american_roulette_commission': document.getElementById('txtAmericanRouletteCommision').value + '#' + document.getElementById('txtAmericanRouletteCommision_htv').value + '#' + document.getElementById('txtAmericanRouletteCommision_savan').value + '#0#0',
            		'american_roulette_sharing': document.getElementById('txtAmericanRouletteSharing').value + '#' + document.getElementById('txtAmericanRouletteSharing_htv').value + '#' + document.getElementById('txtAmericanRouletteSharing_savan').value + '#0#0',
            		'selected_american_roulette_limit': selected_american_roulette_limit,
            		'slot':slot,
            		'slot_commission':'0#' + document.getElementById('txtSlotCommision_htv').value + '#' + document.getElementById('txtSlotCommision_savan').value + '#' + document.getElementById('txtSlotCommision_virtua').value+ '#' + document.getElementById('txtSlotCommision_slots').value,
            		'slot_sharing': '0#' + document.getElementById('txtSlotSharing_htv').value + '#' + document.getElementById('txtSlotSharing_savan').value + '#' + document.getElementById('txtSlotSharing_virtua').value+ '#' + document.getElementById('txtSlotSharing_slots').value,
            		's_edit': s_edit,
            		'max_win_multiple': document.getElementById('txtWinMaxMultiple').value,
            		'max_win_daily':document.getElementById('txtDailyMaxWin').value,
            		'htv999': htv999,
            		'savan999': savan999,
            		'costa999': costa999,
            		'virtua999': virtua999,
            		'sportsbook999': sportsbook999,
            		'slotsvegas999': slotsvegas999,
            		'marketType': marketType,
        		},
        		
    		success: function(msg) 
    		{
        		if(msg=='session_expired'){
            		alert('Session expired!');
            		return false;
            	}else if (msg==0){
            		alert(lblAccountExist);
        			//document.getElementById('txtMessage').value='Account exist!';
        			document.getElementById('btnChkAvailability').focus();
            	}else if(msg==1)
            	{
        			if(strPercentValueUserLevel=='SC'){
	        			$('#vwDialogNewSubCompany').load(urlLoadDialogNewSubCompany + '&account_id='+account_ID);
	        			$('#dialogNewSubCompany').dialog('open');
        			}else if(strPercentValueUserLevel=='SMA'){
        				$('#vwDialogSeniorMasterAgent').load(urlLoadDialogSeniorMaster + '&account_id='+ account_ID + '&agent_parent_id=' + trim(document.getElementById('txtPreAccountID').value));
	        			$('#dialogSeniorMasterAgent').dialog('open');
        			}else if(strPercentValueUserLevel=='MA'){
        				$('#vwDialogMasterAgent').load(urlLoadDialogMaster +'&account_id='+account_ID + '&agent_parent_id=' + trim(document.getElementById('txtPreAccountID').value));
	        			$('#dialogMasterAgent').dialog('open');
        			}else if(strPercentValueUserLevel=='AGT'){
        				$('#vwDialogAgent').load(urlLoadDialogAgent +'&account_id='+account_ID + '&agent_parent_id=' + trim(document.getElementById('txtPreAccountID').value));
	        			$('#dialogAgent').dialog('open');
        			}else if(strPercentValueUserLevel=='MEM'){
        				$('#vwDialogMember').load(urlLoadDialogMember +'&account_id='+account_ID + '&agent_parent_id=' + trim(document.getElementById('txtPreAccountID').value));
	        			$('#dialogMember').dialog('open');
        			}
            	}else if (msg == 'player_is_on_lobby'){
            		$createForceLogoutDialog.dialog('open');
					$('#imgLogout').hide();
					$(".ui-dialog-titlebar-close").hide();
            		//alert(lblPlayerIsOnTheLobby);
            		return false;
            	}else if (msg == 'player_is_transfering_balance'){
            		//alert(lblPlayerIsTransfetingBalance);
            		checkPlayerOnlineStat();
            		return false;
            	}else if (msg == 'invalid_account_name'){
            		alert(lblInvalidAccountName);
            		document.getElementById('txtAccountName').focus();
            		return false;
            	}else if (msg == 'invalid_sharing_commission'){
            		alert(lblInvalidSharingCommission);
            		return false;
            	}else if (msg == 'invalid_data_method'){
            		alert(lblInvalidDataMethod);
            		return false;
            	}else if(msg == 'credit_must_be_number'){
            		alert(lblCreditMustBeNumeric);
            		return false;
            	}else if(msg == 'negative_credit_not_allowed'){
            		alert(lblNegativeCreditNotAllowed);
            		return false;
            	}else if(msg == 'invalid_currency'){
            		alert(lblInvalidCurrencyId);
            		return false;
            	}else if(msg == 'conflict_account_with_cashplayer'){
            		alert(lblConflictAccountWithCashPlayer);
            		return false;
            	}else if (msg == 'account_doenst_exist'){
            		alert(lblAccountNotExist);
            		return false;
            	}else if(msg == 'invalid_account_id'){
            		alert(lblInvalidAccountId);
            		return false;
            	}else if(msg == 'credit_exceeded'){
            		alert(lblCreditExceeded);
            		return false;
            	}else if(msg == 'invalid_max_win_multiple'){
            		alert(lblInvalidMaxWinMultiple);
            		document.getElementById('txtWinMaxMultiple').focus();
            		return false;
            	}else if(msg == 'invalid_daily_max_win'){
            		alert(lblInvalidDailyMaxWin);
            		document.getElementById('txtDailyMaxWin').focus();
            		return false;
            	}else if(msg.replace(/[0-9]/g, '') == 'error_submit_win_loss_limit'){
            		alert(lblUCSystemCantSetDailyMaxWin);
            		document.location.reload(true);
            		return false;
            	}else{
            		document.getElementById('txtCredit').name = document.getElementById('txtCredit').value; 
            	}
        		if (s_edit==1){
        			document.getElementById('btnConfirm').disabled=true;
        			alert(lblUpdateComplete);
        			document.location.reload();
        		}
        		
	    	}
	});
}else{
	document.location.reload();
}
}
