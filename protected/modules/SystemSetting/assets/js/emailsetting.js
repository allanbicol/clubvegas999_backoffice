//active menu color
document.getElementById('mnu_email_setting').style.color="#5A0000";
document.getElementById('mnu_email_setting').style.fontWeight="bold";
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
//function emailChangeLanguage(actionURL)
//{
//	//document.forms["frmEmailSetting"].submit();
//	var email_setting_type='';
//	if(document.getElementById('chkRegister').checked==true){
//		email_setting_type=1;
//	}else{
//		email_setting_type=2;
//	}
//	//alert(email_setting_type);
//	jQuery.ajax({
//		url: actionURL,
//		method: 'POST',
//		data: {'emailChoseLanguage': document.getElementById('emailLang').value,'email_setting_type':email_setting_type},
//		success: function(msg) {
//    		var data=msg.split(" ;;; ");
//    		document.getElementById('txtEmailSubject').value= data[0];
//    		document.getElementById('txtContent1').value= data[1];
//    		document.getElementById('txtContent2').value= data[2];
//    		document.getElementById('txtContent3').value= data[3];
//    	}
//	});
//}
function emailConfirm(actionURL)
{
	var err_message = document.getElementById('txtErrMessage');
	if(trim(document.getElementById('txtEmailSubject').value)==''){
		err_message.value='Email Subject is required!';
		document.getElementById('txtEmailSubject').focus();
		return false;
	}else{
		err_message.value='';
	}
	if(trim(document.getElementById('txtContent1').value)==''){
		err_message.value='Content 1 is required!';
		document.getElementById('txtContent1').focus();
		return false;
	}else{
		err_message.value='';
	}
	if(trim(document.getElementById('txtContent2').value)==''){
		err_message.value='Content 2 is required!';
		document.getElementById('txtContent2').focus();
		return false;
	}else{
		err_message.value='';
	}
	if(trim(document.getElementById('txtContent3').value)==''){
		err_message.value='Content 3 is required!';
		document.getElementById('txtContent3').focus();
		return false;
	}else{
		err_message.value='';
	}
	var email_setting_type='';
	if(document.getElementById('chkRegister').checked==true){
		email_setting_type=1;
	}else{
		email_setting_type=2;
	}
	jQuery.ajax({
		url: actionURL,
		method: 'POST',
		data: {'emailChoseLanguage': document.getElementById('emailLang').value,
    		'email_setting_type':email_setting_type,
    		'email_subject': document.getElementById('txtEmailSubject').value,
    		'content1': document.getElementById('txtContent1').value,
    		'content2': document.getElementById('txtContent2').value,
    		'content3': document.getElementById('txtContent3').value,
    		},
		success: function(msg) {
			err_message.value=msg;
    	}
	});
}
function viewLogs(actionURL){
	window.location=actionURL;
}

function saveSetting(type,desc,lang,content){

	jQuery.ajax({
		url: saveEmailSettingURL,
		type: 'POST',
		data: {'emailType':type,'desc':document.getElementById('signupName').textContent,'lang':lang,'content':content,
    		},
		success: function(msg) {
			alert(msg);
    	}
	});
}

//CKEDITOR JAVASCRIPT=========================================

function InsertText(btnName,type,lang) {
	// Get the editor instance that we want to interact with.
	var editor='';
	switch(type){
	case "signup":
		switch(lang){
		case "en":
			editor = CKEDITOR.instances.editorEng;
			break;
		case "hk":
			editor = CKEDITOR.instances.editorHK;
			break;
		case "th":
			editor = CKEDITOR.instances.editorThai;
			break;
		case "vn":
			editor = CKEDITOR.instances.editorViet;
			break;
		case "cn":
			editor = CKEDITOR.instances.editorCN;
			break;
		}
		break;
	case "changepassword":
		switch(lang){
		case "en":
			editor = CKEDITOR.instances.editorEng1;
			break;
		case "hk":
			editor = CKEDITOR.instances.editorHK1;
			break;
		case "th":
			editor = CKEDITOR.instances.editorThai1;
			break;
		case "vn":
			editor = CKEDITOR.instances.editorViet1;
			break;
		case "cn":
			editor = CKEDITOR.instances.editorCN1;
			break;
		}
		break;
	case "forgetpassword":
		switch(lang){
		case "en":
			editor = CKEDITOR.instances.editorEng2;
			break;
		case "hk":
			editor = CKEDITOR.instances.editorHK2;
			break;
		case "th":
			editor = CKEDITOR.instances.editorThai2;
			break;
		case "vn":
			editor = CKEDITOR.instances.editorViet2;
			break;
		case "cn":
			editor = CKEDITOR.instances.editorCN2;
			break;
		}
		break;
	case "payment":
		switch(lang){
		case "en":
			editor = CKEDITOR.instances.editorEng3;
			break;
		case "hk":
			editor = CKEDITOR.instances.editorHK3;
			break;
		case "th":
			editor = CKEDITOR.instances.editorThai3;
			break;
		case "vn":
			editor = CKEDITOR.instances.editorViet3;
			break;
		case "cn":
			editor = CKEDITOR.instances.editorCN3;
			break;
		}
		break;
	}
	var value =btnName; //document.getElementById( 'txtArea' ).value;

	// Check the active editing mode.
	if ( editor.mode == 'wysiwyg' )
	{
		// Insert as plain text.
		// http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-insertText
		editor.insertText( value );
	}
	else
		alert( 'You must be in WYSIWYG mode!' );
}

