<?php
/**
 * @TODO store all variables CV999
 * @copyright Creative Entertainment Ltd.
 * @author sitthykun
 * @since 2012-04-04
 */



interface ICV999RedisStaff {
	// main key
	const LOBBY			= 'lobby';
	const LOBBY_RISK	= 'risk';
	const LOGIN			= 'login';
	const LOBBY_TICKET	= 'ticket';
	const LOBBY_OTP		= 'otp';
	const LOBBY_REQUEST_TEMP	= 'reqtemp';
	const LOBBY_REQUEST_POPUP	= 'popuptemp';
	const TRANS			= 'api:trans';
	const CASINO_MAINTERNANCE	= 'casino:mainternance:';
	// player type
	const PLAYER_TYPE_ALL	= '*';
	const PLAYER_TYPE_AGENT = 'a';
	const PLAYER_TYPE_CASH	= 'c';
	// agent type
	const BACKEND_AUTH_TYPE_ADMIN	= '*';
	const BACKEND_AUTH_TYPE_AGENT	= 'a';
	const BACKEND_AUTH_TYPE_AGENT_PLAYER	= 'p';
}

?>