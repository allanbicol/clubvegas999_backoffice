<?php
class EfficiaWinlossController extends MyController
{
	//for Costa
	public function actionEfficiaWinloss()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new EfficiaWinloss;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountEfficiaWinloss();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(cnt)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getEfficiaWinloss($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("num","account_id","account_name","currency","bet_count","total_stake","amount_wager","avg_bet","win_los");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	public function actionEfficiaWinloss1()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new EfficiaWinloss;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountEfficiaWinloss1();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(cnt)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getEfficiaWinloss1($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("num","account_id","account_name","currency","bet_count","total_stake","amount_wager","avg_bet","win_los");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	public function actionIndex()
	{
		$this->render("index");
	}
	
	
}