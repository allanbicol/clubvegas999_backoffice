<head>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/winnerlist.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/whitelist.css" />
	<script type="text/javascript">
		var urlSaveWinner='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/SaveWinner';
		var urlUpdateWinner='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/UpdateWinner';
		var urlDeleteWinner='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/DeleteWinner';
		var urlUpdateWinnerList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/UpdateWinnerList';
		var urlWinnerList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/WinnerList';
		var urlGameName='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/GetGameName';
		var urlCasinoName='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/GetCasinoName';
		var urlEditDetails='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/WinnerList/GetWinnerDetails';
	</script>
</head>
<body onload="javascript: onTheLoad();">
	<b>Winner List Management</b>
	<!-- Search Box -->
	<div id="search_cont">
		<!-- Title -->
		<div class="title">Search Winner List</div>
		
		<!-- Body -->
		<div class="body">
			<table border="0" cellpadding="0" cellspacing="0">
			
			<!-- Cash Player or Sub Company -->
			<tr>
				<td class="left">Casino</td>
				<td class="right">
					<select id="cmbCasino">
						<option value="ALL">ALL</option>
		  				<?php 
						$dataReader = TableCasino::model()->findAll();
						foreach ($dataReader as $row){
							if ($row['id']!=4){
								echo '<option value="' . $row['casino_name'] . '">'. strtoupper($row['casino_name']) . '</option>';
							}
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="left">Game</td>
				<td class="right">
					<select id="cmbGame">
						<option value="ALL">ALL</option>
		  				<?php 
						$dataReader = TableGame::model()->findAll();
						foreach ($dataReader as $row){
							echo '<option value="' . $row['game_name'] . '">'. strtoupper($row['game_name']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<!-- Search Textbox -->
			<tr>
				<td class="left">Account ID</td>
				<td class="right">
					<?php echo CHtml::TextField('txtSearch',''); ?>
				</td>
			</tr>
			
			<!-- Search Button -->
			<tr>
				<td class="left"></td>
				<td class="right">
					<?php echo CHtml::button('Search',array('id'=>'btnSearch','onClick'=>'onTheLoad()','class'=>'btn red')); ?>
				</td>
			</tr>
			</table>
		</div>
	</div>
	<div id="qry_winnerlist_result"></div>

	<!--  <div id="qry_sc_result"></div>-->
</body>