<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/announcement.css"/>
<script>
	//active menu color
	document.getElementById('systemsettingHeader').className="start active";
	document.getElementById('mnu_system_setting_announcement').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i><li>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Anouncement</a></li>");
</script>

<div id="body_wrapper">
<div id='title_header1'>
<?php 
	echo "<b>Announcement Management</b>";
?>
</div>
<div class="form">
<div id="table_body">
<table>
<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'user-form',
		'enableAjaxValidation'=>false,
		//'enableClientValidation'=>true,
)); ?>
    
     <?php
    	// store account id when perform an update 
    	//echo $form->hiddenField($model,'id',array('size'=>10,'maxlength'=>10)); 
    	// echo $form->errorSummary($model);
    ?>
    
    <div class="row">
    	<tr>
    		<td>
    		<font color="red">*</font>	
        	<?php echo $form->label($model,'Content English'); ?>
        	</td>
        	<td width="50%">
        		<?php echo $form->textArea($model,'contentEN',array('cols'=>100,'rows'=>5));?> 
        	</td>
        	<td class="errorText">
				<?php echo $form->error($model,'contentEN'); ?>
			</td>
        </tr>
    </div>
    <div class="row">
    	<tr>
    		<td>
    			<font color="red">*</font>
        		<?php echo $form->label($model,'Content Thai'); ?>
        	</td>
        	<td>
        		<?php echo $form->textArea($model,'contentTH',array('cols'=>100,'rows'=>5)); ?>
        	</td>
        	<td class="errorText">
				<?php echo $form->error($model,'contentTH'); ?>
			</td>
        </tr>
    </div>
    
	<div class="row">
		<tr>
			<td>
				<font color="red">*</font>
				<?php echo $form->label($model,'Content Vietnamese'); ?>
			</td>
			<td>
				<?php echo $form->textArea($model,'contentVN',array('cols'=>100,'rows'=>5)); ?>
			</td>
			<td class="errorText">
				<?php echo $form->error($model,'contentVN'); ?>
			</td>
		</tr>
	</div>
	<div class="row">
		<tr>
			<td>
				<font color="red">*</font>
				<?php echo $form->label($model,'Content Chinese Hong Kong'); ?>
			</td>
			<td>
				<?php echo $form->textArea($model,'contentCN',array('cols'=>100,'rows'=>5)); ?>
			</td>
			<td class="errorText">
				<?php echo $form->error($model,'contentCN'); ?>
			</td>
		</tr>
	</div>
	<div class="row">
		<tr>
			<td>
				<font color="red">*</font>
				<?php echo $form->label($model,'Content Chinese Taiwan'); ?>
			</td>
			<td>
				<?php echo $form->textArea($model,'contentHK',array('cols'=>100,'rows'=>5)); ?>
			</td>
			<td class="errorText">
				<?php echo $form->error($model,'contentHK'); ?>
			</td>
		</tr>
	</div>
	<div class="row">
		<tr>
			<td>
				<font color="red">*</font>
				<?php echo $form->label($model,'Status'); ?>
			</td>
			<td>
				<?php 
					if ($model->getIsNewRecord())
						echo $form->dropDownList($model,'content_status',array(0=>'unpublish',1=>'published'));
					else
					{
						$statusName=Announcement::model()->getStatusName($_GET['Account_ID']);
						//exit("stus=". $statusName);
						echo $form->dropDownList($model,'content_status',array(0=>'unpublish',1=>'published'),
						array('options'=>array($statusName=>array('selected'=>'selected'))));
					}
				?>
			</td>
			<td class="errorText">
				<?php echo $form->error($model,'content_status'); ?>
			</td>
		</tr>
	</div>
	
	<div class="row buttons">
		<tr>
			<td></td>
			<td>
				<?php
		//		if(Yii::app()->user->checkAccess('systemSetting.writeUserAccount')){
					echo CHtml::submitButton('Save',array('name'=>'create','class'=>'btn red'));
				//}
				?>
				<?php // echo CHtml::button('Cancel', array('name'=>'User')); ?>
				<?php echo CHtml::Button('Cancel', array('submit'=>array('..?r=SystemSetting/Announcement'),'class'=>'btn red'));?>
			</td>
		</tr>
	</div>

<?php $this->endWidget(); ?>
</table>
</div>
</div></div><!-- form -->
<?php echo Yii::app()->user->getFlash("announce_error"); ?>

<br/>
<br/>