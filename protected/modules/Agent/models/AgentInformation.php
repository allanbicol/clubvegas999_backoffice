<?php
/**
 * @todo AgentInformation Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-06-27
 */
class AgentInformation
{
    /**
     * @todo getYesterdayWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-27
     */
	public function getYesterdayWinLoss($currency_name,$except_test_currency)
	{
		$accountID='';
		if(Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA' || Yii::app()->session['level']=='MA' || Yii::app()->session['level']=='AGT')
		{
			$accountID=Yii::app()->session['account_id'];
		}else{
			$accountID='';
		}
		$dtFfrom=date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d")))) . ' 00:00:00';
		$dtTo=date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d")))) . ' 23:59:59';
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spYesterdayWinLoss('".$currency_name."','".$accountID."','".$dtFfrom."','".$dtTo."',$except_test_currency);");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getYesterdaySlotWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-27
	 */
	public function getYesterdaySlotWinLoss($currency_name,$except_test_currency)
	{
		$accountID='';
		if(Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA' || Yii::app()->session['level']=='MA' || Yii::app()->session['level']=='AGT')
		{
			$accountID=Yii::app()->session['account_id'];
		}else{
			$accountID='';
		}
		$dtFfrom=date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d")))) . ' 00:00:00';
		$dtTo=date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d")))) . ' 23:59:59';
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spYesterdaySlotWinLoss('".$currency_name."','".$accountID."','".$dtFfrom."','".$dtTo."',$except_test_currency);");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getAssignedCredit
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-27
	 */
	public function getAssignedCredit($except_test_currency)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		//$command = $connection->createCommand("SELECT SUM(credit_usd) as credit FROM (SELECT * FROM vwGetAssignedCredit) t;");
		if($except_test_currency==1){
			$command = $connection->createCommand("Select credit from tbl_main_company");
		}else{
			$command = $connection->createCommand("Select ((SELECT credit FROM tbl_main_company) + (Select (case when sum(credit_assigned)!=0 then sum(credit_assigned) else 0 end) from tbl_agent where agent_type='SC' 
					and currency_id=9)) as credit");
		}
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getAgentOverview
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-27
	 */
	public function getAgentOverview()
	{
		$accountID='';
		if(Yii::app()->session['level']=='SC' || Yii::app()->session['level']=='SMA' || Yii::app()->session['level']=='MA' || Yii::app()->session['level']=='AGT')
		{
			$accountID=Yii::app()->session['account_id'];
		}else{
			$accountID='';
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spAgentOverview('".$accountID."',". $_GET['test_currency'] .");");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo convertAmount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-27
	 */
	public function convertAmount()
	{
		$cu=TableCurrency::model()->find('currency_name=:currency_name', array(':currency_name'=>$_POST['current_currency_name']));
		$tu=TableCurrency::model()->find('currency_name=:currency_name', array(':currency_name'=>$_POST['currency_name']));
		$tu_currency=$tu['currency_name'];
		$tu_currency_rate=$tu['exchange_rate'];
		
		$flt_yesterday_converted_value=0;
		$flt_yesterday_slot_converted_value=0;
		$flt_available_converted_value=0;
		if($_POST['current_currency_name']=='USD'){
			$flt_yesterday_converted_value=$_POST['flt_yesterday'] * $tu['exchange_rate'];
			$flt_yesterday_slot_converted_value=$_POST['flt_yesterday_slot'] * $tu['exchange_rate'];
			$flt_available_converted_value=$_POST['flt_available'] * $tu['exchange_rate'];
			$flt_sc_converted_value=$_POST['flt_SC'] * $tu['exchange_rate'];
			$flt_sma_converted_value=$_POST['flt_SMA'] * $tu['exchange_rate'];
			$flt_ma_converted_value=$_POST['flt_MA'] * $tu['exchange_rate'];
			$flt_agt_converted_value=$_POST['flt_AGT'] * $tu['exchange_rate'];
			$flt_mem_converted_value=$_POST['flt_MEM'] * $tu['exchange_rate'];
			//percentage
			$flt_sc_percentage_converted_value=$_POST['flt_SC_percentage'] * $tu['exchange_rate'];
			$flt_sma_percentage_converted_value=$_POST['flt_SMA_percentage'] * $tu['exchange_rate'];
			$flt_ma_percentage_converted_value=$_POST['flt_MA_percentage'] * $tu['exchange_rate'];
			$flt_agt_percentage_converted_value=$_POST['flt_AGT_percentage'] * $tu['exchange_rate'];
			$flt_mem_percentage_converted_value=$_POST['flt_MEM_percentage'] * $tu['exchange_rate'];
			//assigned credit
			$flt_sc_assigned_converted_value=$_POST['flt_SC_assigned'] * $tu['exchange_rate'];
			$flt_sma_assigned_converted_value=$_POST['flt_SMA_assigned'] * $tu['exchange_rate'];
			$flt_ma_assigned_converted_value=$_POST['flt_MA_assigned'] * $tu['exchange_rate'];
			$flt_agt_assigned_converted_value=$_POST['flt_AGT_assigned'] * $tu['exchange_rate'];
			$flt_mem_assigned_converted_value=$_POST['flt_MEM_assigned'] * $tu['exchange_rate'];
		}else{
			$flt_yesterday_converted_value=($_POST['flt_yesterday']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_yesterday_slot_converted_value=($_POST['flt_yesterday_slot']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_available_converted_value=($_POST['flt_available']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_sc_converted_value=($_POST['flt_SC']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_sma_converted_value=($_POST['flt_SMA']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_ma_converted_value=($_POST['flt_MA']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_agt_converted_value=($_POST['flt_AGT']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_mem_converted_value=($_POST['flt_MEM']/$cu['exchange_rate']) * $tu['exchange_rate'];
			//percentage
			$flt_sc_percentage_converted_value=($_POST['flt_SC_percentage']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_sma_percentage_converted_value=($_POST['flt_SMA_percentage']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_ma_percentage_converted_value=($_POST['flt_MA_percentage']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_agt_percentage_converted_value=($_POST['flt_AGT_percentage']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_mem_percentage_converted_value=($_POST['flt_MEM_percentage']/$cu['exchange_rate']) * $tu['exchange_rate'];
			//assigned credit
			$flt_sc_assigned_converted_value=($_POST['flt_SC_assigned']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_sma_assigned_converted_value=($_POST['flt_SMA_assigned']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_ma_assigned_converted_value=($_POST['flt_MA_assigned']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_agt_assigned_converted_value=($_POST['flt_AGT_assigned']/$cu['exchange_rate']) * $tu['exchange_rate'];
			$flt_mem_assigned_converted_value=($_POST['flt_MEM_assigned']/$cu['exchange_rate']) * $tu['exchange_rate'];
		}
		echo number_format($flt_yesterday_converted_value,2) 
		. ':'. number_format($flt_available_converted_value,2) 
		. ':' . number_format($flt_sc_converted_value,2)
		. ':' . number_format($flt_sma_converted_value,2) 
		. ':' . number_format($flt_ma_converted_value,2) 
		. ':' . number_format($flt_agt_converted_value,2) 
		. ':' . number_format($flt_mem_converted_value,2)
		//percentage
		. ':' . number_format($flt_sc_percentage_converted_value,2)
		. ':' . number_format($flt_sma_percentage_converted_value,2)
		. ':' . number_format($flt_ma_percentage_converted_value,2)
		. ':' . number_format($flt_agt_percentage_converted_value,2)
		. ':' . number_format($flt_mem_percentage_converted_value,2)
		//assigned credit
		. ':' . number_format($flt_sc_assigned_converted_value,2)
		. ':' . number_format($flt_sma_assigned_converted_value,2)
		. ':' . number_format($flt_ma_assigned_converted_value,2)
		. ':' . number_format($flt_agt_assigned_converted_value,2)
		. ':' . number_format($flt_mem_assigned_converted_value,2)
		//yesterday slot winloss
		. ':' . number_format($flt_yesterday_slot_converted_value,2)
		;
	}
	
	public function getSumTransaction($type,$account_id,$except_test_currency)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$qry_test_currency='';
		if($except_test_currency==0){
			$qry_test_currency='';
		}else{
			$qry_test_currency=' AND b.currency_name!="TEST"';
		}
		if($account_id==''){
			$command = $connection->createCommand("SELECT sum(total_credit) as total_credit,sum(total_assigned_credit) as total_assigned_credit,sum(total_percentage_credit_val) as total_percentage_credit_val FROM (
					SELECT (case when b.currency_name='USD' then a.credit else a.credit/b.exchange_rate end) as total_credit,
					(case when b.currency_name='USD' then a.credit_assigned else a.credit_assigned/b.exchange_rate end) as total_assigned_credit,
					(case when b.currency_name='USD' then a.credit_percentage else a.credit_percentage/b.exchange_rate end) as total_percentage_credit_val
					FROM tbl_agent a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE a.agent_type='" . $type . "' $qry_test_currency) t");
		}else{
			$command = $connection->createCommand("SELECT sum(total_credit) as total_credit,sum(total_assigned_credit) as total_assigned_credit,sum(total_percentage_credit_val) as total_percentage_credit_val FROM (
					SELECT (case when b.currency_name='USD' then a.credit else a.credit/b.exchange_rate end) as total_credit,
					(case when b.currency_name='USD' then a.credit_assigned else a.credit_assigned/b.exchange_rate end) as total_assigned_credit,
					(case when b.currency_name='USD' then a.credit_percentage else a.credit_percentage/b.exchange_rate end) as total_percentage_credit_val
					FROM tbl_agent a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE a.agent_type='" . $type . "' AND a.agent_parent_id='" . $account_id . "' $qry_test_currency) t");
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getSumParentTransaction($type,$account_id,$except_test_currency)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$qry_test_currency='';
		if($except_test_currency==0){
			$qry_test_currency='';
		}else{
			$qry_test_currency=' AND currency_id!=9';
		}
		if($type!='MEM'){
			//$command = $connection->createCommand("SELECT SUM(credit) as total_credit,SUM(credit_assigned) as total_assigned_credit,SUM(credit_assigned*(credit_percentage/100)) as total_percentage_credit_val FROM tbl_agent WHERE agent_type='" . $type . "' AND account_id like '" . $account_id . "%' $qry_test_currency");
			$command = $connection->createCommand("SELECT SUM(credit) as total_credit,SUM(credit_assigned) as total_assigned_credit,SUM(credit_percentage) as total_percentage_credit_val FROM tbl_agent WHERE agent_type='" . $type . "' AND account_id like '" . $account_id . "%' $qry_test_currency");
		}else{
			if($account_id!=''){
				$command = $connection->createCommand("SELECT SUM(credit) as total_credit,0 as total_assigned_credit,0 as total_percentage_credit_val FROM tbl_agent_player WHERE account_id like '" . $account_id . "%' $qry_test_currency");
			}else{
				if($qry_test_currency!=''){
					$command = $connection->createCommand("SELECT SUM(CASE WHEN b.currency_name='USD' THEN a.credit ELSE a.credit/b.exchange_rate END) as total_credit,0 as total_assigned_credit,0 as total_percentage_credit_val FROM tbl_agent_player a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE currency_id!=9");
				}else{
					$command = $connection->createCommand("SELECT SUM(CASE WHEN b.currency_name='USD' THEN a.credit ELSE a.credit/b.exchange_rate END) as total_credit,0 as total_assigned_credit,0 as total_percentage_credit_val FROM tbl_agent_player a INNER JOIN tbl_currency b ON a.currency_id=b.id");
				}
			}
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getStatusNo($type,$status_id,$account_id)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if($type!='MEM'){
			$command = $connection->createCommand("SELECT COUNT(account_id) as total FROM tbl_agent WHERE agent_type='" . $type . "' AND agent_status_id=". $status_id ." AND account_id like '". $account_id ."%'");
		}else{
			$command = $connection->createCommand("SELECT COUNT(account_id) as total FROM tbl_agent_player WHERE status_id=". $status_id ." AND account_id like '". $account_id ."%'");
		}
		$rows = $command->query();
		return $rows;
	}
}






