<?php
/**
 * @todo WhiteListController
 * @copyright CE
 * @author Leo karl
 * @since 2012-11-02
 */
class WhiteListController extends MyController
{
	public function actionSubCompany()
	{
		$model_name = new WhiteList();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$model_name->countSubCompanyList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $model_name->getSubCompanyList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("account_id","account_name","currency_name","status","opening_acc_date","white_list");
		$urlAS= Yii::app()->request->baseUrl;
		
		$htmvalue='';
		
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,0);
	}
	public function actionCashPlayer()
	{
		$model_name = new WhiteList();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$model_name->countCashPlayerList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $model_name->getcashPlayerList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("account_id","account_name","currency_name","status","opening_acc_date","white_list");
		$urlAS= Yii::app()->request->baseUrl;
	
		$htmvalue='';
	
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,0);
	}
	public function actionSaveSubCompanyWhiteList(){
		if(Yii::app()->user->checkAccess('systemSetting.writeWhiteList'))
		{
			$sa = new WhiteList();
			$sa->saveSubCompanyWhiteList();
		}else{
			exit('no_permission');
		}
	}
	public function actionCashPlayerWhiteList(){
		if(Yii::app()->user->checkAccess('systemSetting.writeWhiteList'))
		{
			$sa = new WhiteList();
			$sa->saveCashPlayerWhiteList();
		}else{
			exit('no_permission');
		}
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readWhiteList')){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
}
