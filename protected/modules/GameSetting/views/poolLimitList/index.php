<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/gamesetting.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		//active menu color
		document.getElementById('mnu_pool_limit').style.color="#5A0000";
		document.getElementById('mnu_pool_limit').style.fontWeight="bold";
	</script>
	<script type="text/javascript">
		var cRowNo=0;
		var gameId=0;
		function tableBacarrat(currency_id) { 
			document.getElementById('qry_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/PoolLimitList/PoolLimit&currency_id='+currency_id, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['Id', 'Game','Pool Limit','Last Update','Setting'],
				    colModel: [
				      {name: 'id', index: 'id', width: 30, search:true, hidden: true},
				      {name: 'game_name', index: 'game_name', width: 70, sortable: false},
				      {name: 'amount', index: 'amount', width: 90, sortable: false},
				      {name: 'last_update', index: 'last_update', width: 70, sortable: false},
				      {name: 'setting', index: 'setting', width: 70,sortable: false}
				    ],
				    beforeSelectRow: function(rowid,e){
				    	var data = grid.jqGrid('getRowData', rowid);
				    	cRowNo = rowid;
				    	gameId=data.game_name;
					},
					loadComplete: function(){
						$("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
					},
					width: 600,
                    rownumbers: true,
                    sortable: false,
				    sortname: 'no',
				    sortorder: 'ASC',
				    caption: 'Pool Limit (Min - Max) <input type="button" value="Add" onclick="javascript: $(\'#viewContent\').load(\'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/PoolLimitList/LoadPoolLimitContent&currency_id='+ document.getElementById('cmbCurrencyType').value +'\');$(\'#dialogAddPoolLimit\').dialog(\'open\');"/>',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false});
			});
		} 
	</script>
</head>
<body onload="javascript: tableBacarrat(document.getElementById('cmbCurrencyType').value)">
	<div id="tl_header">Pool Limit</div>
	<div id="qry_selector">
		Currency Type:
		<select name="cmbCurrencyType" id="cmbCurrencyType" class="nput" onchange="javascript: tableBacarrat(this.value);">
			<?php 
				$dataReader = TableCurrency::model()->findAll();
				foreach ($dataReader as $row){
					echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
				}
			?>
		</select>
	</div>
	<div id="qry_result"></div>
	
	<?php //Add PoolLimit dialog
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id'=>'dialogAddPoolLimit',
			    // additional javascript options for the dialog plugin
			    'options'=>array(
			        'title'=>'Add Pool Limit',
			        'autoOpen'=>false,
		    		'width'=>'auto',
		    		'height'=>'auto',
			    	'modal'=> true,
			    	'resizable'=> false,
			    ),
			));
	    echo '<div id="viewContent"></div>';
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
	<?php //Setting PoolLimit dialog
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id'=>'dialogSettingPoolLimit',
			    // additional javascript options for the dialog plugin
			    'options'=>array(
			        'title'=>'Setting Pool Limit',
			        'autoOpen'=>false,
		    		'width'=>'auto',
		    		'height'=>'auto',
			    	'modal'=> true,
			    	'resizable'=> false,
			    ),
			));
	    echo '<div id="viewContent1"></div>';
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
	
</body>
</html>