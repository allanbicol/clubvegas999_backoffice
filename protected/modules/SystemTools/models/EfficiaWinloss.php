<?php

class EfficiaWinloss
{

	public function getEfficiaWinloss($orderField, $sortType, $startIndex, $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".$_GET['accountId']."'";
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT '0' as num, account_id,
														(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
														currency,
														COUNT(bet_amount) AS bet_count,
														SUM(bet_amount) AS total_stake,
														SUM(available_bet) AS amount_wager,
														AVG(available_bet) AS avg_bet,
														SUM(win_loss) AS win_los
														FROM tbl_htv_casino_history AS CH where bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ".$andAccountId." GROUP BY currency,account_id
														 order by $orderField $sortType LIMIT $startIndex , $limit
													");
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT '0' as num, account_id,
														(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
														currency,
														COUNT(bet_amount) AS bet_count,
														AVG(available_bet) AS avg_bet,
														SUM(bet_amount) AS total_stake,
														SUM(available_bet) AS amount_wager,
														SUM(win_loss) AS win_los
														FROM tbl_htv_casino_history AS CH WHERE (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id
														 order by $orderField $sortType LIMIT $startIndex , $limit
													");
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT '0' as num, account_id,
														(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
														currency,
														COUNT(bet_amount) AS bet_count,
														AVG(available_bet) AS avg_bet,
														SUM(bet_amount) AS total_stake,
														SUM(available_bet) AS amount_wager,
														SUM(win_loss) AS win_los
														FROM tbl_htv_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id
														 order by $orderField $sortType LIMIT $startIndex , $limit
													");
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT account_id,
														(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
														currency,
														COUNT(bet_amount) AS bet_count,
														AVG(available_bet) AS avg_bet,
														SUM(bet_amount) AS total_stake,
														SUM(available_bet) AS amount_wager,
														SUM(win_loss) AS win_los
														FROM tbl_htv_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id
														 order by $orderField $sortType LIMIT $startIndex , $limit
													");
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT account_id,
														(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
														currency,
														COUNT(bet_amount) AS bet_count,
														AVG(available_bet) AS avg_bet,
														SUM(bet_amount) AS total_stake,
														SUM(available_bet) AS amount_wager,
														SUM(win_loss) AS win_los
														FROM tbl_htv_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id
														 order by $orderField $sortType LIMIT $startIndex , $limit
													");
		}
		else{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."' and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT account_id,
														(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
														currency,
														COUNT(bet_amount) AS bet_count,
														AVG(available_bet) AS avg_bet,
														SUM(bet_amount) AS total_stake,
														SUM(available_bet) AS amount_wager,
														SUM(win_loss) AS win_los
														FROM tbl_htv_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id
														 order by $orderField $sortType LIMIT $startIndex , $limit
													");
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountEfficiaWinloss()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".$_GET['accountId']."'";
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
						FROM tbl_htv_casino_history AS CH where bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ".$andAccountId." GROUP BY currency,account_id) as HTV");
			}else{
				$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
						FROM tbl_htv_casino_history AS CH WHERE (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) as HTV");
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
						FROM tbl_htv_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id");
			}else{
				$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
						FROM tbl_htv_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) as HTV");
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
					FROM tbl_htv_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) as HTV");
		}
		else{
			$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
					FROM tbl_htv_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) as HTV");
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getEfficiaWinloss1($orderField, $sortType, $startIndex, $limit)
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".$_GET['accountId']."'";
		}
	
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT '0' as num, account_id,
						(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
						currency,
						COUNT(bet_amount) AS bet_count,
						SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,
						AVG(available_bet) AS avg_bet,
						SUM(win_loss) AS win_los
						FROM tbl_savan_casino_history AS CH where bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ".$andAccountId." GROUP BY currency,account_id
						order by $orderField $sortType LIMIT $startIndex , $limit
				");
			}else{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT '0' as num, account_id,
					(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
					currency,
					COUNT(bet_amount) AS bet_count,
					AVG(available_bet) AS avg_bet,
					SUM(bet_amount) AS total_stake,
					SUM(available_bet) AS amount_wager,
					SUM(win_loss) AS win_los
					FROM tbl_savan_casino_history AS CH WHERE (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id
					order by $orderField $sortType LIMIT $startIndex , $limit
					");
			}
			}
					elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
			{
			if($_GET['test']==0){
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT '0' as num, account_id,
						(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
				currency,
				COUNT(bet_amount) AS bet_count,
				AVG(available_bet) AS avg_bet,
				SUM(bet_amount) AS total_stake,
				SUM(available_bet) AS amount_wager,
				SUM(win_loss) AS win_los
				FROM tbl_savan_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id
				order by $orderField $sortType LIMIT $startIndex , $limit
				");
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT account_id,
						(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
						currency,
						COUNT(bet_amount) AS bet_count,
						AVG(available_bet) AS avg_bet,
						SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,
						SUM(win_loss) AS win_los
						FROM tbl_savan_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id
						order by $orderField $sortType LIMIT $startIndex , $limit
				");
			}
			}
			elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
			{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT account_id,
						(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
						currency,
						COUNT(bet_amount) AS bet_count,
						AVG(available_bet) AS avg_bet,
						SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,
						SUM(win_loss) AS win_los
						FROM tbl_savan_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id
						order by $orderField $sortType LIMIT $startIndex , $limit
						");
			}
			else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."' and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT account_id,
						(SELECT CP.account_name FROM tbl_cash_player AS CP WHERE CH.account_id = CP.account_id ) AS account_name,
						currency,
						COUNT(bet_amount) AS bet_count,
						AVG(available_bet) AS avg_bet,
						SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,
						SUM(win_loss) AS win_los
						FROM tbl_savan_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id
				order by $orderField $sortType LIMIT $startIndex , $limit
				");
			}
			$rows = $command->query();
			return $rows;
			}
	
			public function getCountEfficiaWinloss1()
			{
			$connection = Yii::app()->db_cv999_fd_master;
			if ($_GET['accountId']=="All"){
			$andAccountId='';
			}else{
			$andAccountId="and account_id="."'".$_GET['accountId']."'";
			}
	
			if ($_GET['currencyId']=="All" && $_GET['game']=="All")
				{
				if ($_GET['test']==0){
					$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
					FROM tbl_savan_casino_history AS CH where bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ".$andAccountId." GROUP BY currency,account_id) as HTV");
					}else{
					$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
							FROM tbl_savan_casino_history AS CH WHERE (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) as HTV");
					}
				}
			elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
			{
						if ($_GET['test']==0){
						$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
								FROM tbl_savan_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id");
						}else{
								$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
						FROM tbl_savan_casino_history AS CH where game_id='".$_GET['game']."' ".$andAccountId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) as HTV");
						}
				}
			elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
							{
							$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
							FROM tbl_savan_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) as HTV");
						}
			else{
						$command = $connection->createCommand("SELECT COUNT(cnt) FROM (SELECT COUNT(0) as cnt
							FROM tbl_savan_casino_history AS CH where currency='".$_GET['currencyId']."' ".$andAccountId." and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) as HTV");
							}
							$rows = $command->query();
							return $rows;
				}
}