<html>
<head>

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>


    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/advisory.css" />
    <script src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/ui/minified/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-timepicker-addon.js"></script>
    <!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/ui/i18n/jquery.ui.timepicker.js"></script> -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/advisory.js"></script>
    <script>
        var advisoryListURL  ='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/GetList';
        var createAdvisoryURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/New';
        var editAdvisoryURL  = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/GetById';
        var deleteAdvisoryURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/Delete';
        var advisoryLanguages = <?php echo Languages::getLanguageCodeAsJSON(); ?>;
    </script>
    <style>
        .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word!important; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap!important; /* CSS3 */
            white-space: -moz-pre-wrap!important; /* Mozilla, since 1999 */
            white-space: -pre-wrap!important; /* Opera 4-6 */
            white-space: -o-pre-wrap!important; /* Opera 7 */
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px
        }
    </style>
</head>
<?php
/**
 * models
 */
// access level
$atm = new AccountTypeModel();
$atmList = $atm->getAllTypes();
// publishes categories
$apc = new AdvisoryPublishedCategories();
$apcList = $apc->getAllStatus();
// languages
$lang = new Languages();
$langList = $lang->getLanguages();
// sitename
$siteName = new SiteNameModel();
$siteList = $siteName->getNames();
// schedules
$schedule = new AdvisorySchedulesModel();
// print_r($schedule->getList());
// schedule day list
// AdvisoryContentCategories
$contentCategory = new AdvisoryContentCategories();
$contentCateList = $contentCategory->getCategories();
// icons
$icons = new AdvisoryIconsModel();
$iconList = $icons->getList();

// detail
$advisoryModel = new AdvisoryModel();
$getFirstRecord = $advisoryModel->getListByID($_GET['id']);
$getRecordDetail = $advisoryModel->getDetailByID($_GET['id']);

// print_r($getFirstRecord);

?>
<body>
<section class="advisory-container">
    <div class="advisory-box">
        <header class="advisory-header">Advisory Management <a href="index.php?r=SystemSetting/Advisory/New" id="advisoryAddbtn" class="btn red">Add</a></header>
        <table id="advisoryList"><tr><td /></tr></table>
        <div id="advisoryPaper"></div>
    </div>
    <div class="advisory-box" id="advisoryModifiedBox">
        <header class="advisory-header"><span class="darkRedText">Detail: </span><span id="advisoryActionTitleBox">Modifies ID: <?php echo $_GET['id']; ?></span></header>
        <div class="advisory-detail-box">
            <form action="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=SystemSetting/Advisory/Save" method="post">
                <input type="hidden" name="advisoryKeyUpdateValue" id="advisoryKeyUpdateValue" value="0" />
                <input type="hidden" name="advisoryKeyID" id="advisoryKeyID" value="<?php echo $_GET['id']; ?>" />
                <ul class="advisory-detail-box-top">
                    <!-- Access All  -->
                    <li style="display:none;">
                        <fieldset>
                            <legend>Previlege/Access</legend>
                            <div>
                                <?php
                                // access
                                foreach ($atmList as $a) {
                                    echo '<input type="checkbox" class="advisory-access-checkbox" id="access_' . $a['account_code'] . '" name="accesses[]" value="' . $a['account_code'] . '" checked="checked" /><label for="acc_' . $a['account_code'] . '">' . $a['account'] . '</label>';
                                }
                                ?>
                            </div>
                        </fieldset>
                    </li>
                    <!-- unpublish, published all, published advisory page, published advisory   -->
                    <li>
                        <fieldset>
                            <legend>Status</legend>
                            <div>
                                <label>Published </label>
                                <select name="status" class="advisory-dropdown" id="status" size="1">
                                    <?php
                                    // status
                                    foreach ($apcList as $a) {
                                        echo '<option value="'. $a['id'] . '"';

                                        if ($a['id'] == $getFirstRecord['published_category_id']) {
                                            echo ' selected="selected"';
                                        }

                                        echo  '>'. $a['content'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend>Schedule Updat Date-Time*</legend>
                            <fieldset id="scheGroupEveryday">
                                <?php
                                $getStartTimeByDateTimeString = '';
                                $getEndTimeByDateTimeString = '';
                                $checkedStringRadio = '';

                                // get time from DateTime + AM
                                if ($getFirstRecord['schedule_category_id'] == 2) {
                                    // $getStartTimeByDateTime = preg_split('#\s+#', $getFirstRecord['started_date'], 2);
                                    // $getStartTimeByDateTimeString = isset($getStartTimeByDateTime[1]) ? $getStartTimeByDateTime[1] : '';
                                    $getStartTimeByDateTimeString = $getFirstRecord['started_date'];

                                    // $getEndTimeByDateTime = preg_split('#\s+#', $getFirstRecord['ended_date'], 2);
                                    // $getEndTimeByDateTimeString = isset($getEndTimeByDateTime[1]) ? $getEndTimeByDateTime[1] : '';
                                    $getEndTimeByDateTimeString = $getFirstRecord['ended_date'];

                                    $checkedStringRadio = ' checked="checked" ';
                                }
                                ?>
                                <legend><input type="radio" name="sch_radio[]" id="sch_radio2" value="2" <?php echo $checkedStringRadio; ?> />Anydays</legend>
                                <div class="list-schedule">
                                    <?php
                                    // extract
                                    foreach ($schedule->getDaysOfWeek() as $key => $value) {
                                        echo '<input type="checkbox" class="advisory-checkbox" id="days_' . $key . '" name="' . $key . '" value="' . $key . '"';
                                        if ($getFirstRecord[$key] == 1) {
                                            echo ' checked ';
                                        }

                                        echo '/><label id="label_days_' . $key . '" for="acc_' . $key . '">' . $value . '</label>';
                                    }
                                    ?>
                                </div>
                                <div class="list-schedule">
                                    <label>Start Time*</label><input type="text" class="txt" name="startTime" id="startDateTime2" value="<?php echo $getStartTimeByDateTimeString; ?>" />
                                    <label>End Time*</label><input type="text" class="txt" name="endTime" id="endDateTime2" value="<?php echo $getEndTimeByDateTimeString; ?>" />
                                </div>
                            </fieldset>
                            <fieldset id="scheGroupSomeday">
                                <?php
                                $getStartDateString = '';
                                $getEndDateString = '';
                                $checkedStringRadio = '';

                                // get time from DateTime + AM
                                if ($getFirstRecord['schedule_category_id'] == 3 || $getFirstRecord['schedule_category_id'] == 1) {
                                    $getStartDateString = $getFirstRecord['started_date'];
                                    $getEndDateString = $getFirstRecord['ended_date'];

                                    $checkedStringRadio = ' checked="checked" ';
                                }

                                ?>
                                <legend><input type="radio" name="sch_radio[]" id="sch_radio3" value="3" <?php echo $checkedStringRadio; ?> />Somedays</legend>
                                <label>Start Date*</label><input type="text" class="txt" name="startDate" id="startDateTime3" value="<?php echo $getStartDateString; ?>" />
                                <label>End Date*</label><input type="text" class="txt" name="endDate" id="endDateTime3" value="<?php echo $getEndDateString; ?>" />
                            </fieldset>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend>Schedule Execute Alert Date-Time*</legend>
                            <div class="list-schedule">
                                <label id="startDateShowLabel">Start Date <br />(Somedays)*</label><input type="text" class="txt" name="startDateShow" id="startDateShow" value="<?php echo $getFirstRecord['started_date_show']; ?>" />
                                <label>Start Time*</label><input type="text" class="txt" name="startTimeShow" id="startTimeShow"  value="<?php echo $getFirstRecord['started_time_show']; ?>"/>
                            </div>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend>Site</legend>
                            <div>
                                <?php
                                // status
                                foreach ($siteList as $s) {
                                    echo '<input type="checkbox" class="advisory-checkbox" id="site_' . $s['id'] . '" name="sites[]" value="' . $s['id'] . '"';

                                    if (in_array($s['id'], explode(',', $getFirstRecord['site_code']))) {
                                        echo ' checked ';
                                    }

                                     echo '/><label for="site_' . $s['id'] . '">' . $s['site'] . '</label>';
                                }
                                ?>
                            </div>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend>Notice TYPE</legend>
                            <select name="servicesAffect" class="advisory-dropdown" id="serviceAffected" size="1">
                                <?php
                                foreach ($contentCateList as $a) {
                                    echo '<option value="'. $a['id'] . '"';

                                    if ($getFirstRecord['services_affected_id'] == $a['id']) {
                                        echo ' selected="selected" ';
                                    }
                                    echo '>'. $a['type_serives'] . '</option>';
                                }
                                ?>
                            </select>
                        </fieldset>
                    </li>
                    <li>
                        <fieldset>
                            <legend>Notice ICON</legend>
                            <select name="icon_id" class="advisory-dropdown" id="iconID" size="1">
                                <?php
                                foreach ($iconList as $ic) {
                                    echo '<option value="'. $ic['id'] . '"';

                                    if ($getFirstRecord['icon_id'] == $ic['id']) {
                                        echo ' selected="selected" ';
                                    }

                                    echo '>'. $ic['title'] . '</option>';
                                }
                                ?>
                            </select>
                        </fieldset>
                    </li>
                </ul>
                <div id="tabLangs" class="advisory-tab-container">
                    <?php
                    // tabs
                    echo '<ul>';
                    // loop
                    foreach ($langList as $l) {
                        echo '<li><a href="#tabs-'. $l['language_code'] . '">'. $l['language'] . '</a></li>';
                    }
                    echo '</ul>';
                    // loop
                    foreach ($langList as $l) {
                        // get content
                        $detailRecord = array();
                        for($iRD = 0; $iRD < count($getRecordDetail); $iRD++) {
                            if ($getRecordDetail[$iRD]['language_code'] == $l['language_code']) {
                                $detailRecord = $getRecordDetail[$iRD];
                            }
                        }

                    echo '<div class="advisory-tab-lang" id="tabs-'. $l['language_code'] . '">';
                    ?>
                    <ul>
                        <!-- unpublish, published all, published advisory page, published advisory   -->
                        <li>
                            <label class="main-label">Services Affected*</label><div>&nbsp;</div>
                        </li>
                        <li>
                            <!-- <textarea class="service-affected" id="<?php echo $l['language_code']; ?>_services_text" name="<?php echo $l['language_code']; ?>_services_text" rows="2" cols="125"></textarea> //-->
                            <textarea cols="100" id="<?php echo $l['language_code']; ?>_editor1" name="<?php echo $l['language_code']; ?>_services_text" rows="10"></textarea>
                            <script type="text/javascript">
                                // Replace the <textarea id="editor1"> with an CKEditor instance.
                                CKEDITOR.replace( '<?php echo $l['language_code']; ?>_editor1', {
                                    on: {
                                        //focus: onFocus,
                                        //blur: onBlur,

                                        // Check for availability of corresponding plugins.
                                        pluginsLoaded: function(evt) {
                                            var doc = CKEDITOR.document, ed = evt.editor;
                                            if (!ed.getCommand( 'bold' ))
                                                doc.getById( 'exec-bold' ).hide();
                                            if ( !ed.getCommand( 'link' ) )
                                                doc.getById( 'exec-link' ).hide();
                                        }
                                    }
                                });
                                CKEDITOR.instances["<?php echo $l['language_code']; ?>_editor1"].setData("<?php echo addslashes(str_replace(array("\n","\r"), '', $detailRecord['services_affected'])); ?>");
                            </script>
                        </li>
                        <li>
                            <label class="main-label">Description*</label><div>&nbsp;</div>
                        </li>
                        <li>
                            <textarea cols="100" id="<?php echo $l['language_code']; ?>_editor2" name="<?php echo $l['language_code']; ?>_editor" rows="10"></textarea>
                            <script type="text/javascript">
                                // Replace the <textarea id="editor1"> with an CKEditor instance.
                                CKEDITOR.replace( '<?php echo $l['language_code']; ?>_editor2', {
                                    on: {
                                        //focus: onFocus,
                                        //blur: onBlur,

                                        // Check for availability of corresponding plugins.
                                        pluginsLoaded: function(evt) {
                                            var doc = CKEDITOR.document, ed = evt.editor;
                                            if (!ed.getCommand( 'bold' ))
                                                doc.getById( 'exec-bold' ).hide();
                                            if ( !ed.getCommand( 'link' ) )
                                                doc.getById( 'exec-link' ).hide();
                                        }
                                    }
                                });
                                CKEDITOR.instances["<?php echo $l['language_code']; ?>_editor2"].setData("<?php echo addslashes(str_replace(array("\n", "\r"), '', $detailRecord['content'])); ?>");
                            </script>
                        </li>
                    </ul>
                </div>
                <?php
                } // end loop
                ?>
        </div>
        <div class="bottom">
            <input class="btn red" type="submit" value="Save" />
            <input class="btn red" type="button" id="advisoryCancelbtn" value="Cancel" />
        </div>
        </form>
    </div>
    </div>
</section>
</body>
</html>
