<?php
/**
 * @todo CashPlayerStatus Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerStatus
{
	public function changeCashPlayerStatus()
	{
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		//$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['status']=='Active'){
			$intStatID='1';
		}
		elseif($_GET['status']=='Closed'){
			$intStatID='3';
		}
		
		
		
			
		if ($_GET['task']=='changeStatus')
		{
			//$command = $connection->createCommand("UPDATE tbl_cash_player SET status_id=" .  $intStatID . " WHERE account_id='" . $_GET['AccountID'] . "'");
			//$rows = $command->query();
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT status_id from tbl_cash_player  WHERE account_id='" . $_GET['AccountID'] . "'");
			$rd=$command->queryRow();
			$statusVal=$rd['status_id'];
			if ($statusVal==1){
				$status='Active';
			}else{$status='Closed';}
			
			TableCashPlayer::model()->updateAll(array('status_id'=>$intStatID),'account_id="'.$_GET['AccountID'].'"');
			
			$postLog=new TableLog();
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level='Agent Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=8;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE status of <label style=\"color:green\">Agent Player</label> <label style=\"color:#7A5C00\">'.$_GET['AccountID'].'</label> from <label style=\"color:red\">'.$status.'</label> to <label style=\"color:red\">'.$_GET['status'].'</label></b>';
				$postLog->save();
		}
	}
}