<?php
/**
@todo: This is the model class for table "tbl_agent_parameter".
@Author: Allan Bicol
@Since: 05/08/2012
*/



class GameMaxSharingAndCommission extends CActiveRecord
{


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_agent_parameter';
	}



}
