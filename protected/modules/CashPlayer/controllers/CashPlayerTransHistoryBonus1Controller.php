<?php
class CashPlayerTransHistoryBonus1Controller extends MyController
{
	public function actionCashPlayerTransHistoryBonus1()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerTransHistoryBonus1;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerTransHistoryBonus1();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerTransHistoryBonus1($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("counter","player_id","account_name","currency_name","trans_number","added_bonus","removed_bonus","Total");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerTotalTransHistoryBonus1()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerTransHistoryBonus1;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountTotalPlayerTransHistoryBonus1();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getTotalPlayerTransHistoryBonus1();
		//$test = $records->readAll();
		$filedNames = array("currency_name","trans_number","added_bonus","removed_bonus","Total","rate","added_bonus","removed_bonus","Total");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), 1, 1, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readTransHistorySummary'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionBonusExcel(){
		$file="CashPlayerTrans_BonusSummary".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($params[3] != 'ALL'){
			$command = $connection->createCommand("SELECT currency_name from tbl_currency  WHERE id='" . $params[3] . "'");
			$rd=$command->queryRow();
			$currency=$rd['currency_name'];
		}else{ $currency ='ALL';}
		
		if ($params[2]!='ALL'){
			$command1 = $connection->createCommand("SELECT transaction_item from tbl_transaction_item  WHERE id='" . $params[2] . "'");
			$rd1=$command1->queryRow();
			$item=$rd1['transaction_item'];
		}else{$item='ALL';}
		
		
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Transaction History Summary (Bonus)</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> = '.$params[1].'\n<b>Bonus Type</b> = '.$item.'\n<b>Currency</b> = '.$currency.'\n<b>Test Currency</b> ='.$params[4].'\n<b>Date From</b> = '.$params[5].'\n<b>Date To</b> = '.$params[6];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionExportAllBonusTransactionHistory()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerTransHistoryBonus1;
		$result=$cpHfund->getCountPlayerTransHistoryBonus1();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records = $cpHfund->getExportAllPlayerTransHistoryBonus1();
		//$test = $records->readAll();
	
		$fieldNames = array("player_id","account_name","currency_name","trans_number","added_bonus","removed_bonus","Total");
		$headerNames=array("Player Id","Account Name","Currency","Trans Numver","Added Bonus","Removed Bonus","Total");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	

		$result=$cpHfund->getCountTotalPlayerTransHistoryBonus1();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		$player_records = $cpHfund->getTotalPlayerTransHistoryBonus1();
		//$test = $records->readAll();
		$fieldNames = array("currency_name","trans_number","added_bonus","removed_bonus","Total");
		$headerNames=array("Currency","No. of Transaction","Add","Remove","Total");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
}