<?php
/**
 * @todo transaction items controller
 * @author leokarl
 * @since 2012-12-19
 */

class TransactionItemsController extends MyController
{
	/**
	 * @todo transaction items list
	 * @author leokarl
	 * @since 2012-12-19
	 */
	public function actionTransactionItemsList(){
		$model= new TransactionItems();
		
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'transaction_item asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';
		
		$result = $model->getTransactionItemsCount();
		$data = $result->readAll();
		$records = $data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex = 0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records = $model->getTransactionItemsList($orderField, $sortType, $startIndex, $limit);
		$filedNames = array("id","transaction_item","type_name","transaction_bank");
		
		echo JsonUtil::jsonJqgridData($player_records->readAll(),$total_pages, $page, $records,$filedNames);
	}
	
	
	/**
	 * @todo load add transaction item dialog
	 * @author leokarl
	 * @since 2012-12-20
	 */
	public function actionLoadAddTransactionItem(){
		$this->renderPartial('addDialog');
	}
	
	
	/**
	 * @todo add transaction item
	 * @author leokarl
	 * @since 2012-12-20
	 */
	public function actionAddTransactionItem(){
		if(Yii::app()->user->checkAccess('cashPlayer.writeTransactionItem')){
			$model = new TransactionItems();
			
			// validate post data
			if(!isset($_POST['item']) || trim($_POST['item']) == ''){
				exit('item_not_set');
			}
			if(!isset($_POST['type']) || !$model->isTransactionTypeIdValid($_POST['type'])){
				exit('type_not_set');
			}
			if(!isset($_POST['s_bank_checked']) || !$model->isBankTransactionValid($_POST['s_bank_checked'])){
				exit('bank_not_set');
			}
			if($model->isSpecialCharPresent($_POST['item'])){
				exit('special_char');
			}
			
			// continue saving data
			$model->addTransactionItem($_POST['item'], $_POST['type'], $_POST['s_bank_checked']);
		}else{
			exit('You have no permission!');
		}
	}
	
	
	/**
	 * @todo update item bank transaction
	 * @author leokarl
	 * @since 2012-12-20
	 */
	public function actionUpdateItemBankTransaction(){
		if(Yii::app()->user->checkAccess('cashPlayer.writeTransactionItem')){
			$model = new TransactionItems();
			
			// validate post data
			if(!isset($_POST['bank_transaction'])){
				exit('Bank Transaction not set!');
			}
			if(!isset($_POST['item_id'])){
				exit('Item ID not set!');
			}
			if(!$model->isBankTransactionValid($_POST['bank_transaction'])){
				exit('Invalid Bank Transaction value!');
			}
			if(!$model->isTransactionItemIDValid($_POST['item_id'])){
				exit('Invalid Item ID!');
			}
			
			// initialize
			$bank_transaction = ($_POST['bank_transaction'] == 'true') ? 1 : 0;
			
			// continue saving
			$model->updateItemBankTransaction($_POST['item_id'], $bank_transaction);
		}else{
			exit('You have no permission!');
		}
	}
	
	
	public function actionIndex(){
		if(Yii::app()->user->checkAccess('cashPlayer.readTransactionItem')){
			$this->render('index');
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
}