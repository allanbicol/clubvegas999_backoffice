<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/button.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('mnu_agent_list_info').style.color="#5A0000";
	document.getElementById('mnu_agent_list_info').style.fontWeight="bold";
</script>
<script type="text/javascript">
function tableTransHistoryFund(status,accountId,dateFrom,dateTo) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'listlog'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
	$(document).ready(function() {
	jQuery("#listlog").jqGrid({ 
		url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransHistory/AgentPlayerTransHistory&stat='+status+'&id='+accountId+'&datefrom='+dateFrom+'&dateto='+dateTo, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['Transaction Number', 'Type','Currency','Amount','Credit Before','Credit After','Balance Before','Balance After','Transaction Date','Cage Cashier Account'],
	    colModel: [
			{name: 'transaction_number', index: 'transaction_number', width: 140, search:true,title:false},
			{name: 'trans_type_id', index: 'trans_type_id', width: 100,title:false,formatter:TypeFormatter},
			{name: 'currency', index: 'currency', width: 60,title:false},
			{name: 'amount', index: 'amount', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'credit_before', index: 'credit_before', width: 125, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'credit_after', index: 'credit_after', width: 125, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'balance_before', index: 'balance_before', width: 125, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'balance_after', index: 'balance_after', width: 125, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
			{name: 'transaction_date', index: 'transaction_date', width: 125,title:false},
			{name: 'operator_id', index: 'operator_id', width: 150,title:false},
	    ],
	    loadtext:"",
	    loadComplete: function() {
	    	var grid = $("#listlog");
	        var i=0; 
		    for(i=0;i<=grid.getGridParam("reccount");i++)
		    {
		    	var myrow = grid.jqGrid('getRowData', i);
		    	grid.jqGrid('setCell',i,"balance_before","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"balance_after","",{background:'#cfe5fa'});
		    	grid.jqGrid('setCell',i,"credit_before","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"credit_after","",{background:'#cfe5fa'});
			} 
		  //grid stripe
		    $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    $("tr.jqgrow:even").css("background", "#ffffff");
	    },
	    rownumbers:true,
	    rowNum: 20,	
	    rowList: [20, 50, 100,200,500,99999],
	    pager: '#pager2',
	    sortname: 'transaction_number',
	    sortorder: 'ASC',
	    caption: '<?php echo '<label style="color:red">' .strtoupper($_GET["account_id"]) .'</label>'; ?> FUND - &nbsp;&nbsp;&nbsp;'+ document.getElementById('datefrom').value + '&nbsp;' + document.getElementById('cbHourfrom').value + ':00:00 to '+ document.getElementById('dateto').value + '&nbsp;' + document.getElementById('cbHourto').value +':59:59',
	    viewrecords: true
	});
	
	$('#listlog').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:true });

	function TypeFormatter(cellvalue, options, rowObject) {
        $("cellvalue").val(cellvalue);
        if (cellvalue==2){
        	return '<label>Deposit</label>';
        }else if (cellvalue==4){
        	return '<label>Withdrawal</label>';
		}else if (cellvalue==6){
        	return '<label>Bonus</label>';
		}else if (cellvalue==7){
        	return '<label>Add Credit</label>';
		}else if (cellvalue==8){
        	return '<label>Remove Credit</label>';
		}
	}; 
	
	});
}
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	});

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");
			document.getElementById('qry_result').innerHTML='';
			var accountId= document.getElementById('txtAccountID').value;
			tableTransHistoryFund("All",accountId,datefrom,dateto);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
	
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");
			document.getElementById('datefrom').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var statustype=document.getElementById('statusType').value;
			var accountId=document.getElementById('txtAccountID').value;
			document.getElementById('qry_result').innerHTML='';
			tableTransHistoryFund(statustype,accountId,datefrom,dateto);
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			
			var datefrom=(theyear+"-"+themonth+"-"+theyday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theyday+ " 23:59:59");
			document.getElementById('datefrom').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var statustype=document.getElementById('statusType').value;
			var accountId=document.getElementById('txtAccountID').value;
			document.getElementById('qry_result').innerHTML='';
			tableTransHistoryFund(statustype,accountId,datefrom,dateto);
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[0];
			var yearF = dateFrom[2];
			var dateF=(yearF + "-" + monthF + "-" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[0];
			var yearT = dateTo[2];
			var dateT= (yearT + "-" + monthT + "-" +dayT);
			var dateSubmitFrom= (dateF+" "+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ " "+ document.getElementById('cbHourto').value +":59:59");
			var statustype=document.getElementById('statusType').value;
			var accountId=document.getElementById('txtAccountID').value;
			document.getElementById('qry_result').innerHTML='';
			tableTransHistoryFund(statustype,accountId,dateSubmitFrom,dateSubmitTo);
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area">
	<div class="header">Transaction History</div>
	<form action="">
		<table >
		<tr><td style=" height:10px; border-bottom: groove; ">
		<b>FUND/BONUS</b>
		<input style=" width: 30px" type="text" value="<?php echo $_GET["account_id"]; ?>" hidden="True" id="txtAccountID">
		</td></tr>
		<tr><td>METHOD:
			<select id="statusType">
  				<option value="All">All</option>
  				<option value="2">Deposit</option>
  				<option value="4">Withdraw</option>
  				<option value="6">Bonus</option>
  				<option value="7">Add Credit</option>
  				<option value="8">Remove Credit</option>
			</select>

		</td></tr>
		<tr><td>FROM  :<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px; background-color: transparent" disabled></td></tr><tr><td>TO &nbsp;&nbsp;&nbsp;&nbsp;:<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; background-color: transparent" disabled></td></tr>
			<tr><td><div align="center">
		<input onclick="javacript:btnclick(this.value);" type="button" value="Submit" class="btn red">
		<input onclick="javascript:btnclick(this.value);" type="button" value="Yesterday" class="btn red">
		<input onclick="javascript:btnclick(this.value);" type="button" value="Today" class="btn red">
		<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red"></div>
		</td></tr>
		</table>
		</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
		document.getElementById("dateto").value=(day + "-" + month + "-" + year);
		</script>
	</div>

	<div id="qry_result"></div>
	<div id="pager2"></div>
	<br/>


</body>
</html>