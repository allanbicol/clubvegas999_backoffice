<?php

class PlayerLoginAuditController extends MyController
{
	public function actionPlayerLoginAudit()
	{
	
		$loginAudit = new PlayerLoginAudit;
		$page = $_GET['page'];
	
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
	
		$sortType = $_GET['sord'];
	
		$result=$loginAudit->getCountPlayerLoginAudit();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		// calculate the starting position of the rows
		//$startIndex = $limit*$page - $limit;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
	
		$player_records = $loginAudit->getPlayerLoginAudit($orderField, $sortType, $startIndex, $limit);
		$filedNames = array("account_id","player_type","onlive","session","ip_address","site","isp","browser_name");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionPlayerLoginAuditSummary()
	{
	
		$loginAudit = new PlayerLoginAudit;
		$page = $_GET['page'];
	
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
	
		$sortType = $_GET['sord'];
	
		$result=$loginAudit->getCountPlayerLoginAudit();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		// calculate the starting position of the rows
		//$startIndex = $limit*$page - $limit;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
	
		$player_records = $loginAudit->getPlayerLoginAuditSummary($orderField, $sortType, $startIndex, $limit);
		$filedNames = array("blank","url_domain","login_count");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}

	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('securityLog.readPlayerLoginAudit'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionExcel(){
		$file="PlayerLoginAudit".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
	
		header("Content-type: application/vnd.ms-excel;charset=UTF-8");
		header("Content-Disposition: attachment; filename=$file");
		HEADER("Pragma: no-cache");
		HEADER("Expires: 0");
	
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Player Login Audit';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Player Login Audit</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> = '.$params[1].'\n<b>Player Type</b> = '.$params[2].'\n<b>Status</b> = '.$params[3].'\n<b>DateFrom</b> = '.$params[4].'\n<b>DateTo</b> = '.$params[5];
			$postLog->save();
	
	
		}catch(Exception $e){
	
		}
	}
	
	public function actionExportExcelAll()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new PlayerLoginAudit;
		$result=$cpHfund->getCountPlayerLoginAudit();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		
		$player_records = $cpHfund->getPlayerLoginAuditSummaryExport();
		
		$fieldNames = array("url_domain","login_count");
		$headerNames=array("Website Used","Login Count");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerLoginAuditExport();
	
		$fieldNames = array("account_id","player_type","onlive","session","ip_address","site","isp","browser_name");
		$headerNames=array("Account Id","Player Type","Status","Session Date","IP Address","Site","ISP","Browser");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
}