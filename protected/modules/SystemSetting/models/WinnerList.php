<?php
/**
 * @todo Winner List Controller
 * @copyright CE
 * @author Allan Bicol
 * @since 2013-03-19
 */
class WinnerList
{
	public function getWinnerList($orderField, $sortType, $startIndex , $limit)
	{
		//initialize
		if($_GET['casino']=='ALL'){
			$casino="";
		}else{
			$casino=" and casino_name="."'".$_GET['casino']."'";
		}
		if($_GET['game']=='ALL'){
			$game="";
		}else{
			$game=" and game_type="."'".$_GET['game']."'";
		}
		
		if($_GET['id']==''){
			$accountId=" Where account_id<>''";
		}else{
			$accountId=" Where account_id="."'".$_GET['id']."'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT id,account_id,game_type,casino_name,winning_amount,enable_winning,'' as operation from tbl_latest_winner_list $accountId $casino $game ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	
	public function countWinnerList()
	{
		//initialize
		if($_GET['casino']=='ALL'){
			$casino="";
		}else{
			$casino=" and casino_name="."'".$_GET['casino']."'";
		}
		if($_GET['game']=='ALL'){
			$game="";
		}else{
			$game=" and game_type="."'".$_GET['game']."'";
		}
		
		if($_GET['id']==''){
			$accountId=" Where account_id<>''";
		}else{
			$accountId=" Where account_id="."'".$_GET['id']."'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_latest_winner_list $accountId $casino $game");
		$rows = $command->query();
		return $rows;
	}
}