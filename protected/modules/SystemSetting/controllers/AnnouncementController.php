<?php
/**
 * @todo		Create gaming lobby announcement
 * @author 		kimny
 * @since		2013-08-15
 * @copyright	CE
 */
class AnnouncementController extends MyController
{
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readAnnouncement'))
		{
			$this->render('index');
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionAnnouncementList()
	{
		
		$model = new Announcement();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$model->getRecordCount();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
	
		$startIndex = $limit*$page - $limit;
		$announcementList=$model->getAnnouncementList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("id","contentEN","content_status","operation");
		$htmValue='<a class=\"btn mini red\" href=\'index.php?r=SystemSetting/Announcement/Update&Account_ID=\' >Edit <i class=\"icon-edit\"></i></a><a class=\"btn mini red\" href=\'index.php?r=SystemSetting/Announcement/delete&Account_ID=\' onclick=\'javascript:openDeleteDialog(this); return false;\'>Delete <i class=\"icon-trash\"></i></a>';
		echo JsonUtil::generateJqgridData($announcementList->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,3);
	}
	
	public function actionCreate(){

		$model = new Announcement();
		if(Yii::app()->user->checkAccess('systemSetting.writeAnnouncement'))
		{
			if(isset($_POST['Announcement']))
			{
				$isPublished = $_POST['Announcement'];
				if ($isPublished['content_status'] == 1)
					$model->unpublishAll();
				
				$model->attributes=$_POST['Announcement'];
				if ($model->save())
				{
					/* Saving the log */
					$announcementName = substr($isPublished['contentEN'], 0, strlen($isPublished['contentEN']));
					
					if ($isPublished['content_status'] == 1)
					{
						$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Create new announcement and published it. <label style=\"color:green\">'.$announcementName.'</label></b>';
					}
					else 
					{
						$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Create new announcement<label style=\"color:green\">'.$announcementName.'</label></b>';
					}
						
					$logs = new SystemSetting();
					$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'],'', '', 26, $log_details);
					
					for($i=1;$i<=5;$i++){
						$lang = TableLanguage::model()->findByPk($i);
						// call to Announcement API
						$session = curl_init(Constants::ANNOUNCEMENT_API_ADR.'&language='.$lang->language_code);
						// Don't return HTTP headers. Do return the contents of the call
						curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
						// get otp from api
						$rSession = curl_exec($session);
						curl_close($session);
					}
					if (trim($rSession) == 'no') 
					{
						Yii::app()->user->setFlash("announce_error", "WARNING - Can't connect to Announcement API.");
					}
					else if (trim($rSession) == 'yes') 
					{
						Yii::app()->user->setFlash("announce_success", "Update success");
					}
					
					$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=SystemSetting/Announcement");
				}
			}
		}
		
		if(Yii::app()->user->checkAccess('systemSetting.readAnnouncement'))
		{
			$this->render('create',array('model'=>$model));
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		
	}
	
	public function actionDelete()
	{
		
		if(Yii::app()->user->checkAccess('systemSetting.writeAnnouncement'))
		{
			$model = Announcement::model()->findByPk($_POST['id']);
			$announcementContent = $model->contentEN;
			Announcement::model()->deleteAll(array('condition'=>'id=:id','params'=>array(':id'=>$_POST['id'])));

			/* Saving the log */
				
			$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Delete a announcement: <label style=\"color:green\">'.$announcementContent.'</label></b>';
			$logs = new SystemSetting();
			$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'],'', '', 26, $log_details);
			
			$msg = 'The announcement successfully deleted.';

		}
		else
		{ 
			$msg='You dont have permission to do this operation.';
		}
		
		echo $msg;
	
	}
	
	public function actionUpdate(){
	
		$model = Announcement::model()->findByPk($_GET['Account_ID']);
		

		if(Yii::app()->user->checkAccess('systemSetting.writeAnnouncement'))
		{
			if(isset($_POST['Announcement']))
			{
				$isPublished = $_POST['Announcement'];
				if ($model->content_status != $isPublished['content_status'])
				{
					if ($isPublished['content_status'] == 0)
						$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Change the announcement from published to unpublish: <label style=\"color:green\"> ID:'.$_GET['Account_ID'].'</label></b>';
					else
						$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Change the announcement from unpublish to published: <label style=\"color:green\"> ID:'.$_GET['Account_ID'].'</label></b>';
					
					/* Saving the log */
					$logs = new SystemSetting();
					$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'],'', '', 26, $log_details);
				}
				
				
				$model->attributes=$_POST['Announcement'];
				if ($model->save($_POST['Announcement']))
				{
					if ($isPublished['content_status'] == 1)
					{
						$model->updateAllStatus($_GET['Account_ID']);						
					}
										
					for($i=1;$i<=5;$i++){
						$lang = TableLanguage::model()->findByPk($i);
						// call to Announcement API
						$session = curl_init(Constants::ANNOUNCEMENT_API_ADR.'&language='.$lang->language_code);
						// Don't return HTTP headers. Do return the contents of the call
						curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
						// get otp from api
						$rSession = curl_exec($session);
						curl_close($session);
					}
					
					if (trim($rSession) == 'no') 
					{
						Yii::app()->user->setFlash("announce_error", "WARNING - Can't connect to Announcement API.");
					}
					else if (trim($rSession) == 'yes') 
					{
						Yii::app()->user->setFlash("announce_success", "Update success");
					}
					$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=SystemSetting/Announcement");
				}
			}
		}
	
		if(Yii::app()->user->checkAccess('systemSetting.readAnnouncement'))
		{
			$this->render('create',array(
					'model'=>$model,
			));
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	
	}
}
