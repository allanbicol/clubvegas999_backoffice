
// active menu color
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_white_list').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i><li>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>White List Management</a></li>");
// initialize lists onload
function onTheLoad(){
	tableSubCompany(urlSubCompanyList,urlSaveSubCompanyWhiteList);
	tableCashPlayer(urlCashPlayerList,urlCashPlayerWhiteList);
}

// search function
function searchWhiteList(){
	if($('#cmbSearchType').val()==0){
		tableCashPlayer(urlCashPlayerList + '&account_id=' + $('#txtSearch').val(),urlCashPlayerWhiteList);
		tableSubCompany(urlSubCompanyList + '&account_id=' + $('#txtSearch').val(),urlSaveSubCompanyWhiteList);
		$('#qry_cp_result').css('display','block'); // display cashplayer list 
		$('#qry_sc_result').css('display','block'); // display subcompany list
	}else if($('#cmbSearchType').val()==1){
		tableCashPlayer(urlCashPlayerList + '&account_id=' + $('#txtSearch').val(),urlCashPlayerWhiteList);
		$('#qry_sc_result').css('display','none'); // hide subcompany list
		$('#qry_cp_result').css('display','block'); // display cashplayer list
	}else{
		tableSubCompany(urlSubCompanyList + '&account_id=' + $('#txtSearch').val(),urlSaveSubCompanyWhiteList);
		$('#qry_cp_result').css('display','none'); // hide cashplayer list
		$('#qry_sc_result').css('display','block'); // display subcompany list
	}
}

// subcompany list
function tableSubCompany(listURL,actionURL) { 
	document.getElementById('qry_sc_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_sc_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_sc_result").appendChild(divTag1);
    var grid = jQuery("#list1");
    grid.jqGrid({ 
		url: listURL, 
		datatype: 'json',
	    mtype: 'GET',
	    colNames: ['Account ID','Account Name','Currency','Status','Opening Date/Time','Whitelist'],
	    colModel: [		   	    
	      {name: 'account_id', index: 'account_id', width: 120,title:false},
	      {name: 'account_name', index: 'account_name', width: 123,title:false},
	      {name: 'currency_name', index: 'currency_name', width: 100,title:false},
	      {name: 'status', index: 'status',  width: 125,title:false},
	      {name: 'opening_acc_date', index: 'opening_acc_date', width: 150,title:false},
	      {name: 'white_list', index: 'white_list', width: 60,title:false,align:'center', editable: true, edittype: "checkbox",editoptions: { value:"1:0"},formatter: "checkbox",formatoptions: {disabled : false}},	     
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    	
	    	var getColumnIndexByName = function(grid, columnName) {
	            var cm = grid.jqGrid('getGridParam', 'colModel'), i, l;
	            for (i = 1, l = cm.length; i < l; i += 1) {
	                if (cm[i].name === columnName) {
	                    return i; // return the index
	                }
	            }
	            return -1;
	        };
	    	
	    	var iCol = getColumnIndexByName ($(this), 'white_list'), rows = this.rows, i,
	        c = rows.length;
		    for (i = 1; i < c; i += 1) {
		        $(rows[i].cells[iCol]).change(function (e) {
		            var id = $(e.target).closest('tr')[0].id, isChecked = $(e.target).is(':checked');
			    	var account_id = grid.jqGrid('getCell', id, 'account_id');
			    	
		            var confirmMsg='';
		            if(isChecked==true){
		            	confirmMsg="Do you want to add " + account_id + " to white list? \n\nNote: " + account_id + "'s sub agents and players will be added too.";
			        }else{
			        	confirmMsg="Do you want to remove " + account_id + " from the white list? \n\nNote: " + account_id + "'s sub agents and players will be removed too.";
				    }
			    	var conf = confirm(confirmMsg);
			        if(conf == true){
			             jQuery.ajax({
		            			url: actionURL,
		            			type: 'POST',
		            			data: {'account_id':account_id,'is_checked':isChecked},
		            			context: '',
		            			success: function(data){
		            				if(data == 'no_permission'){
		            					alert('Warning: You don\'t have permission to change white list!');
		            				}else{
		            					alert(data);	
		            				}
		            				document.location.reload(true);
		            	    	}
		            	});
			        }else{
			        	$('#list1').trigger( 'reloadGrid' );
				    }
		        });
		    }
		},
	    rowNum: 25,
	    rownumbers:true,
	    rowList: [25, 50, 100, 200, 99999],
	    pager: '#pager1',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Sub Company List',
	    viewrecords: true,
	});
	$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del: false});
}

// cashplayer list
function tableCashPlayer(listURL,actionURL) { 
	document.getElementById('qry_cp_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_cp_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_cp_result").appendChild(divTag1);

    var grid = jQuery("#list2");
    grid.jqGrid({ 
		url: listURL, 
		datatype: 'json',
	    mtype: 'GET',
	    colNames: ['Account ID','Account Name','Currency','Status','Opening Date/Time','Whitelist'],
	    colModel: [		   	    
			{name: 'account_id', index: 'account_id', width: 120,title:false},
			{name: 'account_name', index: 'account_name', width: 123,title:false},
			{name: 'currency_name', index: 'currency_name', width: 100,title:false},
			{name: 'status', index: 'status',  width: 125,title:false},
			{name: 'opening_acc_date', index: 'opening_acc_date', width: 150,title:false},
			{name: 'white_list', index: 'white_list', width: 60,title:false,align:'center', editable: true, edittype: "checkbox",editoptions: { value:"1:0"},formatter: "checkbox",formatoptions: {disabled : false}},	     
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");

	    	var getColumnIndexByName = function(grid, columnName) {
	            var cm = grid.jqGrid('getGridParam', 'colModel'), i, l;
	            for (i = 1, l = cm.length; i < l; i += 1) {
	                if (cm[i].name === columnName) {
	                    return i; // return the index
	                }
	            }
	            return -1;
	        };
	    	
	    	var iCol = getColumnIndexByName ($(this), 'white_list'), rows = this.rows, i,
	        c = rows.length;
	        
		    for (i = 1; i < c; i += 1) {
		        $(rows[i].cells[iCol]).change(function (e) {
		            var id = $(e.target).closest('tr')[0].id, isChecked = $(e.target).is(':checked');
			    	var account_id = grid.jqGrid('getCell', id, 'account_id');
		            
			    	var confirmMsg='';
		            if(isChecked==true){
		            	confirmMsg="Do you want to add " + account_id + " to white list?";
			        }else{
			        	confirmMsg="Do you want to remove " + account_id + " from the white list?";
				    }
			    	var conf = confirm(confirmMsg);
			        if(conf == true){
			             jQuery.ajax({
		            			url: actionURL,
		            			type: 'POST',
		            			data: {'account_id':account_id,'is_checked':isChecked},
		            			context: '',
		            			success: function(data){
		            				if(data == 'no_permission'){
		            					alert('Warning: You don\'t have permission to change white list!');
		            				}else{
		            					alert(data);	
		            				}
		            				document.location.reload(true);
		            	    	}
		            	});
			        }else{
			        	$('#list2').trigger( 'reloadGrid' );
				    }
		            
		        });
		    }
		},
	    rowNum: 25,
	    rownumbers:true,
	    rowList: [25, 50, 100, 200, 99999],
	    pager: '#pager2',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Cash Player List',
	    viewrecords: true,
	});
	$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del: false});
}
