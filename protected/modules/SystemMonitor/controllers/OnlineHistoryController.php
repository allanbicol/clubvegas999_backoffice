<?php

class OnlineHistoryController extends MyController
{
	public function actionGetOnlineHistory()
	{
		$cp = new OnlineHistory;
		$page = $_POST['page'];
		$limit = $_POST['rows'];
		$orderField = $_POST['sidx'];
		$sortType = $_POST['sord'];
	
		$result=$cp->countOnlineHistory();
		$data=$result->readAll();
	
		$records=$data[0]['reccount'];
		//$records=1;
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cp->getOnlineHistory($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("account_id","account_type","clubvegas","htv","savan");
		$urlAS= Yii::app()->request->baseUrl;
	
		$htmValue='<a href=\"#\" id=\"accountid\" onclick=\"playerDetails(this.id);\"><u>accountid</u></a>';
		echo JsonUtil::jsonOnlineHist($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionGetOnlineHistoryDetails()
	{
		$cp = new OnlineHistory;
		$page = $_POST['page'];
		$limit = $_POST['rows'];
		$orderField = $_POST['sidx'];
		$sortType = $_POST['sord'];
	
		$result=$cp->countOnlineHistoryDetails();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
		//$records=1;
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cp->getOnlineHistoryDetails($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("account_id","account_type","clubvegas_status","casino_status","login_date","ip_address");
		$urlAS= Yii::app()->request->baseUrl;
	
		$htmValue='';
		echo JsonUtil::jsonOnlineHist($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemMonitor.readOnlineHistory')){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
}