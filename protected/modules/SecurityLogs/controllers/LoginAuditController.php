<?php

class LoginAuditController extends MyController
{
	public function actionLoginAudit()
	{
	
		$loginAudit = new LoginAudit;
		$page = $_GET['page'];
	
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
	
		$sortType = $_GET['sord'];
	
		$result=$loginAudit->getCountLoginAudit();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		// calculate the starting position of the rows
		//$startIndex = $limit*$page - $limit;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
	
	
		$player_records = $loginAudit->getLoginAudit($orderField, $sortType, $startIndex, $limit);
		$filedNames = array("account_id","session_type","session_date","login_ip_address","browser","event","fail_reason");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}

	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('securityLog.readLoginAudit'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}