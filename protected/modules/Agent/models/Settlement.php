<?php

/**
* @todo	Manger user role.
* @copyright	CE
* @author Kimny MOUK
* @since	2012-11-15
*/
class Settlement
{

	/**
	 * @todo Get the role list
	 * @copyright CE
	 * @author Kimny MOUK
	 * @since 2012-11-15
	 *
	 */
	public function getPlayerStatusList($accountID,$orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT a.account_id,a.account_name,
							IF((a.kick_off = 1),'Active','In-Active') AS `kick_off`,a.kick_off_time,a.kick_on_time 
							FROM tbl_agent_player a
							WHERE a.account_id LIKE '$accountID%' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}

	public function getRecordCount($accountID)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT COUNT(0)
					FROM tbl_agent_player a
					LEFT JOIN tbl_status s
					ON a.`status_id` = s.`id`
					WHERE a.account_id LIKE '$accountID%'");
		$rows = $command->query();
		return $rows;
	}

	
	
	
	public function getAgentSubCompanyList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select * from vwSubCompanyList WHERE agent_type='SC' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountAgentSubCompanyList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwSubCompanyList WHERE agent_type='SC' " );
		$rows = $command->query();
		return $rows;
	}
	
	public function getAgentSeniorMasterList($orderField, $sortType, $startIndex , $limit)
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select * from vwSeniorMasterList WHERE agent_type='SMA' and agent_parent_id='".$accountId."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountAgentSeniorMasterList()
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwSeniorMasterList WHERE agent_type='SMA' and agent_parent_id='".$accountId."'" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getAgentMasterList($orderField, $sortType, $startIndex , $limit)
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select * from vwAgentMasterList WHERE agent_type='MA' and agent_parent_id='".$accountId."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountAgentMasterList()
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwAgentMasterList WHERE agent_type='MA' and agent_parent_id='".$accountId."'" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getAgentList($orderField, $sortType, $startIndex , $limit)
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select * from vwAgentList WHERE agent_type='AGT' and agent_parent_id='".$accountId."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountAgentList()
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwAgentList WHERE agent_type='AGT' and agent_parent_id='".$accountId."'" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getAgentPlayerList($orderField, $sortType, $startIndex , $limit)
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select * from vwAgentMemberList WHERE  agent_account_id='".$accountId."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountAgentPlayerList()
	{
		$accountId= Yii::app()->session['account_id'];
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwAgentMemberList WHERE  agent_account_id='".$accountId."'" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getUpperAgentBalanceAndCredit($upperAccountId)
	{
		$connection = Yii::app()->db_cv999_fd_master;
	
		$command = $connection->createCommand("Select balance,credit,currency_id,currency_name,agent_type from tbl_agent INNER JOIN tbl_currency ON tbl_agent.currency_id=tbl_currency.id WHERE account_id='".$upperAccountId."'" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getAgentCreditAndBalance($upperAccountId)
	{
		$connection = Yii::app()->db_cv999_fd_master;
	
		$command = $connection->createCommand("Select account_id,credit,balance,market_type from tbl_agent WHERE agent_parent_id='".$upperAccountId."' AND (balance-credit)<>0" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getAgentPlayerCreditAndBalance($upperAccountId)
	{
		$connection = Yii::app()->db_cv999_fd_master;
	
		$command = $connection->createCommand("Select account_id,credit,balance,market_type from tbl_agent_player WHERE agent_account_id='".$upperAccountId."' AND (balance-credit)<>0" );
		$rows = $command->query();
		return $rows;
	}
	
	public function agentTransferAll()
	{
		$creditM_amount_to_withdraw	=	0;
		$creditM_amount_to_deposit	=	0;
		$cashM_amount_to_withdraw	=	0;
		$cashM_amount_to_deposit	=	0;
		
		$balanceDueToForm=0;
		$upperAccountId=Yii::app()->session['account_id'];
		
		$cp=new Settlement();
		$getUpperAgentBalanceAndCredit	=	$cp->getUpperAgentBalanceAndCredit($upperAccountId);
		foreach ($getUpperAgentBalanceAndCredit as $row){
			$upperAgentBalance=$row['balance'];
			$upperAgentCredit=$row['credit'];
			$currency=$row['currency_name'];
			$currencyId=$row['currency_id'];
			$agentType=$row['agent_type'];
		}
		
		//$creditMarketResult=
		
		
		$cp=new Settlement();
		$getCreditAndBalance=$cp->getAgentCreditAndBalance($upperAccountId);
		
		foreach ($getCreditAndBalance as $row){
			if ($row['market_type']==1){
				if (($row['balance']-$row['credit']) > 0){
					$creditM_amount_to_withdraw	= $creditM_amount_to_withdraw	+	($row['balance']-$row['credit']);
				}else{
					$creditM_amount_to_deposit	= $creditM_amount_to_deposit	+	($row['balance']-$row['credit']);
				}
			}else{
				if ($row['balance']-$row['credit'] > 0){
					$cashM_amount_to_withdraw	= $cashM_amount_to_withdraw	+	($row['balance']-$row['credit']);
				}else{
					$cashM_amount_to_deposit	= $cashM_amount_to_deposit	+	($row['balance']-$row['credit']);
				}
			}
		}
		
		$creditMarketResult	=	($creditM_amount_to_withdraw + $upperAgentBalance) - ($creditM_amount_to_deposit*-1);
		$cashMarketResult	=	(($cashM_amount_to_deposit*-1) + $upperAgentCredit) - $cashM_amount_to_withdraw;
		
		if ($creditMarketResult < 0 || $cashMarketResult <0){
			if ($creditMarketResult < 0 && $cashMarketResult > 0){
				exit('<div onclick="$(this).hide(500);" class="alert alert-error"><span>Error: </span>You don\'t have enough balance to deposit CREDIT MARKET players.You need <label style="color:red">'.($creditMarketResult*-1).' '.$currency.'</label> balance to complete the transaction.</div>');
			}
			if ($cashMarketResult < 0 && $creditMarketResult > 0){
				exit('<div onclick="$(this).hide(500);" class="alert alert-error"><span>Error: </span>You don\'t have enough credit to deposit CASH MARKET players.You need <label style="color:red">'.($cashMarketResult*-1).' '.$currency.'</label> credit to complete the transaction.<.div>');
			}
			if ($cashMarketResult < 0 && $creditMarketResult < 0){
				exit('<div onclick="$(this).hide(500);" class="alert alert-error"><span>Error: </span>You don\'t have enough credit and balance to deposit CASH and CREDIT MARKET agents.You need <label style="color:red">'.($cashMarketResult*-1).'</label> <label style="color:red">'.$currency.' credit and '.($creditMarketResult*-1).' '.$currency.'</label> balance to complete the transaction.</div>');
			}
		}else{
			
			$agentInfo		=	new Settlement();
			$getAgentInfo	=	$agentInfo->getAgentCreditAndBalance($upperAccountId);
			
			foreach ($getAgentInfo as $t_row){
				if (($t_row['balance']-$t_row['credit']) !=0){
					
					$cp1=new Settlement();
					$getUpperAgentBalanceAndCredit1	=	$cp1->getUpperAgentBalanceAndCredit($upperAccountId);
					foreach ($getUpperAgentBalanceAndCredit1 as $row1){
						$upperAgentBalance=$row1['balance'];
						$upperAgentCredit=$row1['credit'];
					}
					
					if ($t_row['market_type']==1){
						if (($t_row['balance']-$t_row['credit']) > 0){
							
							TableAgent::model()->updateAll(array('balance'=>$upperAgentBalance + ($t_row['balance']-$t_row['credit'])),'account_id="'.$upperAccountId.'"');
							TableAgent::model()->updateAll(array('balance'=>$t_row['balance'] - ($t_row['balance']-$t_row['credit'])),'account_id="'.$t_row['account_id'].'"');
							Settlement::historyAndLogs($t_row['account_id'], $currencyId, $t_row['balance']-$t_row['credit'],'4', '2', 'Credit Market', $t_row['balance'], $t_row['balance'] - ($t_row['balance']-$t_row['credit']), $t_row['credit'], $t_row['credit'], $upperAgentBalance, $upperAgentBalance + ($t_row['balance']-$t_row['credit']), $upperAgentCredit, $upperAgentCredit, $agentType, 'MW');
						}else{
							
							TableAgent::model()->updateAll(array('balance'=>$upperAgentBalance - (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$upperAccountId.'"');
							TableAgent::model()->updateAll(array('balance'=>$t_row['balance'] +  (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$t_row['account_id'].'"');
							Settlement::historyAndLogs($t_row['account_id'], $currencyId, (($t_row['balance']-$t_row['credit'])*-1),'2', '3', 'Credit Market', $t_row['balance'], $t_row['balance'] - (($t_row['balance']-$t_row['credit'])*-1), $t_row['credit'], $t_row['credit'], $upperAgentBalance, $upperAgentBalance + (($t_row['balance']-$t_row['credit'])*-1), $upperAgentCredit, $upperAgentCredit, $agentType, 'MD');
						}
					}else{
						if (($t_row['balance']-$t_row['credit']) > 0){
							
							TableAgent::model()->updateAll(array('credit'=>$upperAgentCredit - ($t_row['balance']-$t_row['credit'])),'account_id="'.$upperAccountId.'"');
							TableAgent::model()->updateAll(array('credit'=>$t_row['credit'] + ($t_row['balance']-$t_row['credit'])),'account_id="'.$t_row['account_id'].'"');
							Settlement::historyAndLogs($t_row['account_id'], $currencyId, $t_row['balance']-$t_row['credit'],'4', '2', 'Cash Market', $t_row['balance'], $t_row['balance'], $t_row['credit'], $t_row['credit'] + ($t_row['balance']-$t_row['credit']), $upperAgentBalance, $upperAgentBalance, $upperAgentCredit, $upperAgentCredit - ($t_row['balance']-$t_row['credit']), $agentType, 'MW');
						}else{
							
							TableAgent::model()->updateAll(array('credit'=>$upperAgentCredit +  (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$upperAccountId.'"');
							TableAgent::model()->updateAll(array('credit'=>$t_row['credit'] -  (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$t_row['account_id'].'"');
							Settlement::historyAndLogs($t_row['account_id'], $currencyId, (($t_row['balance']-$t_row['credit'])*-1),'2', '3', 'Cash Market', $t_row['balance'], $t_row['balance'], $t_row['credit'], $t_row['credit'] - (($t_row['balance']-$t_row['credit'])*-1), $upperAgentBalance, $upperAgentBalance, $upperAgentCredit, $upperAgentCredit +  (($t_row['balance']-$t_row['credit'])*-1), $agentType, 'MW');
						}
					}
				}
			}
			echo '<div onclick="$(this).hide(500);" class="alert alert-success"><span>Success: </span>Transfer process complete.</div>';
		}
		

	}
	
	public function historyAndLogs($accountId,$currency,$amount,$transType,$logType,$marketType,$agentBB,$agentBA,$agentCB,$agentCA,$upperBB,$upperBA,$upperCB,$upperCA,$agentLevel,$transCode){
		
		$connection = Yii::app()->db_cv999_fd_master;
		$operatorId = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		//Save to Agent and upper agent transaction history
		//$postDepositUA =new AgentSaveTransactionHistory;
		
		$postLog=new TableLog;
		$postLog1=new TableLog;
		$transNum= TransactionNumber::generateTransactionNumber("MD");	
		
		if ($agentLevel=='Player'){
			$postDepositA=new AgentPlayerSaveTransactionHistory;
			
			$postDepositA->transaction_number=$transNum;
			$postDepositA->agent_account_id=$accountId;
			$postDepositA->currency_id=$currency;
			$postDepositA->trans_type_id=$transType;
			$postDepositA->amount= $amount;
			$postDepositA->market_type= $marketType;
			$postDepositA->balance_before=$agentBB;
			$postDepositA->balance_after=$agentBA;
			$postDepositA->credit_before=$agentCB;
			$postDepositA->credit_after=$agentCA;
			$postDepositA->upper_agent_balance_before=$upperBB;
			$postDepositA->upper_agent_balance_after=$upperBA;
			$postDepositA->upper_agent_credit_before=$upperCB;
			$postDepositA->upper_agent_credit_after=$upperCA;
			$postDepositA->transaction_date=$dateTime;
			$postDepositA->operator_id=$operatorId;
		}else{
			$postDepositA=new AgentSaveTransactionHistory;
				
			$postDepositA->transaction_number=$transNum."A";
			$postDepositA->agent_account_id=$accountId;
			$postDepositA->currency_id=$currency;
			$postDepositA->trans_type_id=$transType;
			$postDepositA->amount= $amount;
			$postDepositA->market_type= $marketType;
			$postDepositA->balance_before=$agentBB;
			$postDepositA->balance_after=$agentBA;
			$postDepositA->credit_before=$agentCB;
			$postDepositA->credit_after=$agentCA;
			$postDepositA->upper_agent_balance_before=$upperBB;
			$postDepositA->upper_agent_balance_after=$upperBA;
			$postDepositA->upper_agent_credit_before=$upperCB;
			$postDepositA->upper_agent_credit_after=$upperCA;
			$postDepositA->transaction_date=$dateTime;
			$postDepositA->operator_id=$operatorId;
		}
		$postLog->operated_by=$operatorId;
		$postLog->operated_by_level=$level;
		$postLog->operated=$accountId;
		$postLog->operated_level=$agentLevel;
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=$logType;
		$postLog->log_details='<b>'.$marketType.' : '.$agentLevel.' <label style=\"color:#7A5C00\">'.$accountId.'</label> Deposit:<label style=\"color:red\">'.number_format($amount,2,'.',',').'</label></b>';
			
		$postLog1->operated_by=$operatorId;
		$postLog1->operated_by_level=$level;
		$postLog1->operated=$operatorId;
		$postLog1->operated_level=$level;
		$postLog1->operation_time=$dateTime;
		$postLog1->log_type_id=$logType;
		$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$operatorId.'</label> Send:<label style=\"color:red\">'.number_format($amount,2,'.',',').'</label> to '.$marketType.' '.$accountId.'</b>';
			
		//$postDepositUA->save();
		$postDepositA->save();
		$postLog->save();
		$postLog1->save();
	}
	
	public function agentPlayerTransferAll()
	{
		$creditM_amount_to_withdraw	=	0; 
		$creditM_amount_to_deposit	=	0;
		$cashM_amount_to_withdraw	=	0;
		$cashM_amount_to_deposit	=	0;
	
		$balanceDueToForm=0;
		$upperAccountId=Yii::app()->session['account_id'];
	
		$cp=new Settlement();
		$getUpperAgentBalanceAndCredit	=	$cp->getUpperAgentBalanceAndCredit($upperAccountId);
		foreach ($getUpperAgentBalanceAndCredit as $row){
			$upperAgentBalance=$row['balance'];
			$upperAgentCredit=$row['credit'];
			$currency=$row['currency_name'];
			$currencyId=$row['currency_id'];
			$agentType='Player';
		}
	
		$cp=new Settlement();
		$getCreditAndBalance=$cp->getAgentPlayerCreditAndBalance($upperAccountId);
		
		$this->onlinePlayer=new RedisManager();
		
		foreach ($getCreditAndBalance as $row){
			$checkPlayerLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($row['account_id']);
			
			if ($checkPlayerLobby==0){
				if ($row['market_type']==1){
					if ($row['balance']-$row['credit'] > 0){
						$creditM_amount_to_withdraw	= $creditM_amount_to_withdraw	+	($row['balance']-$row['credit']);
					}else{
						$creditM_amount_to_deposit	= $creditM_amount_to_deposit	+	($row['balance']-$row['credit']);
					}
				}else{
					if ($row['balance']-$row['credit'] > 0){
						$cashM_amount_to_withdraw	= $cashM_amount_to_withdraw	+	($row['balance']-$row['credit']);
					}else{
						$cashM_amount_to_deposit	= $cashM_amount_to_deposit	+	($row['balance']-$row['credit']);
					}
				}
			}else{
				exit('<div onclick="$(this).hide(500);" class="alert-box error"><span>error: </span>Transfer Failed! There\'s a player online on a lobby.</div>');
			}
			
			
		}
	
		$creditMarketResult	=	($creditM_amount_to_withdraw + $upperAgentBalance) - ($creditM_amount_to_deposit*-1);
		$cashMarketResult	=	(($cashM_amount_to_deposit*-1) + $upperAgentCredit) - $cashM_amount_to_withdraw;
	
		if ($creditMarketResult < 0 || $cashMarketResult <0){
			if ($creditMarketResult < 0 && $cashMarketResult > 0){
				exit('<div onclick="$(this).hide(500);" class="alert-box error"><span>error: </span>You don\'t have enough balance to deposit CREDIT MARKET players.You need  <label style="color:red">'.($creditMarketResult*-1).' '.$currency.'</label> balance to complete the transaction.</div>');
			}
			if ($cashMarketResult < 0 && $creditMarketResult > 0){
				exit('<div onclick="$(this).hide(500);" class="alert-box error"><span>error: </span>You don\'t have enough credit to deposit CASH MARKET players.You need  <label style="color:red">'.($cashMarketResult*-1).' '.$currency.'</label> credit to complete the transaction.</div>');
			}
			if ($cashMarketResult < 0 && $creditMarketResult < 0){
				exit('<div onclick="$(this).hide(500);" class="alert-box error"><span>error: </span>You don\'t have enough credit and balance to deposit CASH and CREDIT MARKET players.You need  <label style="color:red">'.($cashMarketResult*-1).' '.$currency.'</label> credit and  <label style="color:red">'.($creditMarketResult*-1).' '.$currency.'</label> balance to complete the transaction.</div>');
			}
			
		}else{
				
			$agentInfo		=	new Settlement();
			$getAgentInfo	=	$agentInfo->getAgentPlayerCreditAndBalance($upperAccountId);
		
			foreach ($getAgentInfo as $t_row){
				if (($t_row['balance']-$t_row['credit']) !=0){
					
					$cp1=new Settlement();
					$getUpperAgentBalanceAndCredit1	=	$cp1->getUpperAgentBalanceAndCredit($upperAccountId);
					foreach ($getUpperAgentBalanceAndCredit1 as $row){
						$upperAgentBalance=$row['balance'];
						$upperAgentCredit=$row['credit'];
					}
					
					//check if balance of the player still in a lobby
					$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer
							WHERE account_id='".$t_row['account_id']."' and deposit_withdrawal<>0");
					$rd = $command->queryRow();
					if ($rd['deposit_withdrawal'] == 1)
					{
						//$cp_=new Settlement();
						//$getPlayers=$cp_->getAgentPlayerCreditAndBalance($upperAccountId);
					
						//foreach ($getPlayers as $row_){
							//$command1 = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer
							//		WHERE account_id='".$t_row['account_id']."' and deposit_withdrawal<>0");
							//$rd1 = $command1->queryRow();
							//if ($rd1['deposit_withdrawal'] == 1){
								echo '<div onclick="$(this).hide(500);" class="alert-box warning"><span>warning: </span>Still transferring balance from the lobby for player <label style="color:red">'.$t_row['account_id'].'</label></div>';
							//}
						//}
						// display wating message.
						//exit('<div onclick="$(this).hide(500);" class="alert-box notice"><span>notice: Try again later...</div>');
					
					}else{
						if ($t_row['market_type']==1){
							if (($t_row['balance']-$t_row['credit']) > 0){
			
								TableAgent::model()->updateAll(array('balance'=>$upperAgentBalance + ($t_row['balance']-$t_row['credit'])),'account_id="'.$upperAccountId.'"');
								TableAgentPlayer::model()->updateAll(array('balance'=>$t_row['balance'] - ($t_row['balance']-$t_row['credit'])),'account_id="'.$t_row['account_id'].'"');
								Settlement::historyAndLogs($t_row['account_id'], $currencyId, $t_row['balance']-$t_row['credit'],'4', '2', 'Credit Market', $t_row['balance'], $t_row['balance'] - ($t_row['balance']-$t_row['credit']), $t_row['credit'], $t_row['credit'], $upperAgentBalance, $upperAgentBalance + ($t_row['balance']-$t_row['credit']), $upperAgentCredit, $upperAgentCredit, $agentType, 'MW');
								echo '<div onclick="$(this).hide(500);" class="alert-box success"><span>success: </span>Transfer process complete for player '.$t_row['account_id'].'.</div>';
							}else{
				
								TableAgent::model()->updateAll(array('balance'=>$upperAgentBalance - (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$upperAccountId.'"');
								TableAgentPlayer::model()->updateAll(array('balance'=>$t_row['balance'] +  (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$t_row['account_id'].'"');
								Settlement::historyAndLogs($t_row['account_id'], $currencyId, (($t_row['balance']-$t_row['credit'])*-1),'2', '3', 'Credit Market', $t_row['balance'], $t_row['balance'] - (($t_row['balance']-$t_row['credit'])*-1), $t_row['credit'], $t_row['credit'], $upperAgentBalance, $upperAgentBalance + (($t_row['balance']-$t_row['credit'])*-1), $upperAgentCredit, $upperAgentCredit, $agentType, 'MD');
								echo '<div onclick="$(this).hide(500);" class="alert-box success"><span>success: </span>Transfer process complete for player '.$t_row['account_id'].'.</div>';
							}
						}else{
							if (($t_row['balance']-$t_row['credit']) > 0){
			
								TableAgent::model()->updateAll(array('credit'=>$upperAgentCredit - ($t_row['balance']-$t_row['credit'])),'account_id="'.$upperAccountId.'"');
								TableAgentPlayer::model()->updateAll(array('credit'=>$t_row['credit'] + ($t_row['balance']-$t_row['credit'])),'account_id="'.$t_row['account_id'].'"');
								Settlement::historyAndLogs($t_row['account_id'], $currencyId, $t_row['balance']-$t_row['credit'],'4', '2', 'Cash Market', $t_row['balance'], $t_row['balance'], $t_row['credit'], $t_row['credit'] + ($t_row['balance']-$t_row['credit']), $upperAgentBalance, $upperAgentBalance, $upperAgentCredit, $upperAgentCredit - ($t_row['balance']-$t_row['credit']), $agentType, 'MW');
								echo '<div onclick="$(this).hide(500);" class="alert-box success"><span>success: </span>Transfer process complete for player '.$t_row['account_id'].'.</div>';
							}else{
			
								TableAgent::model()->updateAll(array('credit'=>$upperAgentCredit +  (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$upperAccountId.'"');
								TableAgentPlayer::model()->updateAll(array('credit'=>$t_row['credit'] -  (($t_row['balance']-$t_row['credit'])*-1)),'account_id="'.$t_row['account_id'].'"');
								Settlement::historyAndLogs($t_row['account_id'], $currencyId, (($t_row['balance']-$t_row['credit'])*-1),'2', '3', 'Cash Market', $t_row['balance'], $t_row['balance'], $t_row['credit'], $t_row['credit'] - (($t_row['balance']-$t_row['credit'])*-1), $upperAgentBalance, $upperAgentBalance, $upperAgentCredit, $upperAgentCredit +  (($t_row['balance']-$t_row['credit'])*-1), $agentType, 'MW');
								echo '<div onclick="$(this).hide(500);" class="alert-box success"><span>success: </span>Transfer process complete for player '.$t_row['account_id'].'.</div>';
							}
						}
					}
				}
			}	
		}
	
	
	}
	
}