<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

<script type="text/javascript">
	//active menu color
	document.getElementById('securitylogHeader').className="start active";
	document.getElementById('mnu_player_login_audit').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i><li>Security Log <i class='icon-angle-right'></i></li><li><a href='#'>Player Login Audit</a></li>");
</script>
<!-- <link rel="stylesheet" type="text/css" href="css/calendar/datepicker.css" />  -->
<!-- <script type="text/javascript" src="js/calendar/datepicker.js"></script> -->

<script type="text/javascript">
//===GRID FOR BONUS==================================
	
		function tableLoginAuditSummary(sessiontype,statustype,accountId,datefrom,dateto,browser) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'listLoginAudit_summary'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result_summary").appendChild(divTag);
	
		    var divTag1 = document.createElement("div"); 
		    //divTag1.id = 'pager2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result_summary").appendChild(divTag1);
	
			jQuery("#listLoginAudit_summary").jqGrid({ 
			url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/PlayerLoginAudit/PlayerLoginAuditSummary&session='+sessiontype+'&status='+statustype+'&accountId='+accountId+'&datefrom='+datefrom+'&dateto='+dateto+'&browser='+browser, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['No.','Website Used','Login Count'],
		    colModel: [
				{name: 'No', index: 'No', width:20, search:true,title:false,align:"left"},
				{name: 'url_domain', index: 'url_domain', width:300, search:true,title:false,align:"left"},
				{name: 'login_count', index: 'login_count', width: 100,title:false,align:"center"},
		    ],
		    loadtext:"",
			loadComplete: function() {
				var grid=jQuery("#listLoginAudit_summary");
		        var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	grid.jqGrid('setCell',i,"No",i,{background:'#E6E6E6'});
			    	
				}   
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    hidegrid: false,
		    rownumbers:true,
		    rowNum: 100,	
		    rowList: [100,200,500,99999],
		    //pager: '#pager2',
		    sortname: 'session',
		    sortorder: 'DESC',
		    caption: 'PLAYER LOGIN SUMMARY',
		    viewrecords: true,
		    grouping: true,
		});
		
		//$('#listLoginAudit_summary').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:false });	
	}

	function tableLoginAudit(sessiontype,statustype,accountId,datefrom,dateto,browser) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'listLoginAudit'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);
	
		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
	
			jQuery("#listLoginAudit").jqGrid({ 
			url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/PlayerLoginAudit/PlayerLoginAudit&session='+sessiontype+'&status='+statustype+'&accountId='+accountId+'&datefrom='+datefrom+'&dateto='+dateto+'&browser='+browser, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['Account ID','Player Type','Status','Session DateTime', 'IP','Site','ISP','Browser'],
		    colModel: [
				{name: 'account_id', index: 'account_id', width: 110, search:true,title:false},
				{name: 'player_type', index: 'player_type', width: 80,title:false,formatter:statusFormatter,align:"center"},
				{name: 'onlive', index: 'onlive', width: 80,title:false,formatter:sessionFormatter,align:"center"},
				{name: 'session', index: 'session', width: 130,title:false},
				{name: 'ip_address', index: 'ip_address', width: 230,title:false},
				{name: 'site', index: 'site', width: 300,title:false},
				{name: 'isp', index: 'isp', width: 200,title:false},
				{name: 'browser', index: 'browser', width: 200,title:false},
		    ],
		    loadtext:"",
			loadComplete: function() {
				var grid=jQuery("#listLoginAudit");
		        var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	grid.jqGrid('setCell',i,"balance_before","",{background:'#f7f7c8'});
			    	grid.jqGrid('setCell',i,"balance_after","",{background:'#cfe5fa'});
				}   
			    $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    },
		    rownumbers:true,
		    rowNum: 100,	
		    rowList: [100,200,500,99999],
		    pager: '#pager1',
		    sortname: 'session',
		    sortorder: 'DESC',
		    caption: 'LOGIN AND LOGOUT AUDIT',
		    viewrecords: true,
		    grouping: true,
		   	groupingView : {
		   		groupField : ['account_id'],
		   		groupColumnShow : [true],
		   		groupText : ['<b>{0}</b>'],
		   		groupCollapse : false,
				groupOrder: ['asc'],
				//groupSummary : [true],
				groupDataSorted : true
		   	},
		});
		
		$('#listLoginAudit').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false });	

		jQuery("#listLoginAudit").jqGrid('navButtonAdd','#pager1',{
            caption:"Export Current Page to Excel", 
            buttonicon:"ui-icon-calculator", 
            onClickButton: function(){ 
            	exportToExcel();
           }, 
            position:"last"
        });
		jQuery("#listLoginAudit").jqGrid('navButtonAdd','#pager1',{
		caption:"Export All Pages to Excel", 
		buttonicon:"ui-icon-calculator", 
		onClickButton: function(){ 
			exportAllToExcel(sessiontype,statustype,accountId,datefrom,dateto,browser);
		}, 
		position:"last"
		});

		function sessionFormatter(cellvalue, options, rowObject) {
			var celval=cellvalue;
	        if (celval==1){
		        return '<label style="color:green">Login</label>';
		    }else{
		    	return '<label style="color:red">Logout</label>';
			}	
		};
		function statusFormatter(cellvalue, options, rowObject) {
			var celval=cellvalue;
	        if (celval=='c'){
		        return '<label style="color:blue">Cash Player</label>';
		    }else{
		    	return '<label style="color:red">Agent Player</label>';
			}	
		};

		function exportToExcel(){
			 var accountId=$('#txtAccountId')[0].value;
			 var playerType=$('#selSession')[0].value;
			 var status=$('#selStatus')[0].value;
			 var dateFrom = $('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value+':00:00';
			 var dateto = $('#dateto')[0].value + ' ' + $('#cbHourto')[0].value+':59:59';

			 var table= document.getElementById('qry_result');
			 var table1= document.getElementById('qry_result_summary');
			 var html = table1.outerHTML + table.outerHTML;
			 document.forms[2].csvBuffer.value=html;
			 document.forms[2].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+accountId+'#'+playerType+'#'+status+'#'+dateFrom+'#'+dateto;
	         document.forms[2].method='POST';
	         document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/PlayerLoginAudit/Excel';  // send it to server which will open this contents in excel file
	         document.forms[2].target='_top';
	         document.forms[2].submit();
			
		}

		function exportAllToExcel(sessiontype,statustype,accountId,datefrom,dateto,browser){
	        $.ajax({
	         url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/PlayerLoginAudit/ExportExcelAll&session='+sessiontype+'&status='+statustype+'&accountId='+accountId+'&datefrom='+datefrom+'&dateto='+dateto+'&browser='+browser, 
	   		 async:true,
	   		 success: function(result) {
	   				var data =result.split("<BREAK>");
	   				
	   				var accountId=$('#txtAccountId')[0].value;
	   			 var playerType=$('#selSession')[0].value;
	   			 var status=$('#selStatus')[0].value;
	   			 var dateFrom = $('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value+':00:00';
	   			 var dateto = $('#dateto')[0].value + ' ' + $('#cbHourto')[0].value+':59:59';
	   			 
	   			 document.forms[2].csvBuffer.value=data[0]+""+data[2];
	   			 document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+playerType+'#'+status+'#'+dateFrom+'#'+dateto;
	   	         document.forms[2].method='POST';
	   	         document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/PlayerLoginAudit/Excel';  
	   	         document.forms[2].target='_top';
	   	         document.forms[2].submit();
	   		 },
	   		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	   	 }});
			
		}
	}

</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});
	
	var sChar=false;
	function alphanumeric(inputtxt)  
	{  
		var letters = /^[0-9a-zA-Z]+$/;  
		if(inputtxt.match(letters) || inputtxt=='')  
		{  
			sChar=false;  
		}else{  
			sChar=true;  
		}  
	}  

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{

			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();

			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}

			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			var datefrom=(theyear+"/"+month+"/"+day + "_00:00:00");
			var dateto=(theyear+"/"+month+"/"+day+ "_23:59:59");
			var accountId=document.getElementById('txtAccountId').value;

			document.getElementById('qry_result_summary').innerHTML='';
	    	tableLoginAuditSummary("All","All",accountId,datefrom,dateto,"All");
			document.getElementById('qry_result').innerHTML='';
	    	tableLoginAudit("All","All",accountId,datefrom,dateto,"All");
	    	
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();

			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var sessiontype=document.getElementById('selSession').value;
			var statustype=document.getElementById('selStatus').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				document.getElementById('qry_result_summary').innerHTML='';
				tableLoginAuditSummary(sessiontype,statustype,accountId,datefrom,dateto,"All");
				document.getElementById('qry_result').innerHTML='';
				tableLoginAudit(sessiontype,statustype,accountId,datefrom,dateto,"All");
				
			}
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var sessiontype=document.getElementById('selSession').value;
			var statustype=document.getElementById('selStatus').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				document.getElementById('qry_result_summary').innerHTML='';
				tableLoginAuditSummary(sessiontype,statustype,accountId,datefrom,dateto,"All");
				document.getElementById('qry_result').innerHTML='';
				tableLoginAudit(sessiontype,statustype,accountId,datefrom,dateto,"All");
				
			}
		}
		else if (btnname=="Submit")
		{
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var sessiontype=document.getElementById('selSession').value;
			var statustype=document.getElementById('selStatus').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				document.getElementById('qry_result_summary').innerHTML='';
				tableLoginAuditSummary(sessiontype,statustype,accountId,dateSubmitFrom,dateSubmitTo,"All");
				document.getElementById('qry_result').innerHTML='';
				tableLoginAudit(sessiontype,statustype,accountId,dateSubmitFrom,dateSubmitTo,"All");
				
			}
			
		}else{
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var sessiontype=document.getElementById('selSession').value;
			var statustype=document.getElementById('selStatus').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				document.getElementById('qry_result_summary').innerHTML='';
				tableLoginAuditSummary(sessiontype,statustype,accountId,dateSubmitFrom,dateSubmitTo,btnname);
				document.getElementById('qry_result').innerHTML='';
				tableLoginAudit(sessiontype,statustype,accountId,dateSubmitFrom,dateSubmitTo,btnname);
				
			}
		}
	}

</script>


</head>
<body onload="btnclick('Load')">
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
<div id="parameter_area" style="width: 500px">
	<div class="header" >AUDIT TRAIL</div>
	<form action="">
		<table style="background-color:transparent;  width: 500px;margin-top:10px;">
			<tr><td style="width: 30%;padding-left: 5px;">PLAYER TYPE:</td>
				<td style="width: 70%">
					<select id="selSession"">
  						<option value="All">All</option>
  						<option value="c">Cash Player</option>
 			 			<option value="a">Agent Player</option>
					</select></td>
			</tr>
			<tr><td style="width: 30%;padding-left: 5px;">STATUS:</td>
				<td><select id="selStatus">
  						<option value="All">All</option>
			  			<option value="0">Logout</option>
			  			<option value="1">Login</option>
					</select><br/>
				</td>
			</tr>
			<tr>
				<td style="width: 30%;padding-left: 5px;">BROWSER:</td>
				<td>
				<img name="Chrome" onclick="btnclick(this.name);" title="Chrome" src="<?php echo $this->module->assetsUrl; ?>/images/chrome.png" width="29px" height="29px">
				<img name="Mozilla Firefox" onclick="btnclick(this.name);" title="Firefox" src="<?php echo $this->module->assetsUrl; ?>/images/firefox.png">
				<img name="Safari" onclick="btnclick(this.name);" title="Safari" src="<?php echo $this->module->assetsUrl; ?>/images/safari.png">
				<img name="Opera" onclick="btnclick(this.name);" title="Opera" src="<?php echo $this->module->assetsUrl; ?>/images/opera.png">
				<img name="Internet Explorer" onclick="btnclick(this.name);" title="IE" src="<?php echo $this->module->assetsUrl; ?>/images/ie.png">
				<img name="Netscape" onclick="btnclick(this.name);" title="Netscape"src="<?php echo $this->module->assetsUrl; ?>/images/netscafe.png">
				<img name="Handheld Browser" onclick="btnclick(this.name);" title="Mobile" src="<?php echo $this->module->assetsUrl; ?>/images/mobile.png">
				</td>
			</tr>
			<tr>
				<td style="width: 30%;padding-left: 5px;">ACCOUNT ID:</td>
				<td><input type="text" id="txtAccountId" style="width: 180px"></td>
			</tr>
			<tr><td style="width: 30%;padding-left: 5px;">FROM:</td>
				<td><input style="width: 130px"  type="text" id="datefrom" name="datefrom" value="" />
					<select id="cbHourfrom">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
									echo '<option>'.$value.'</option>';
								}
						?></select>
						<input value=":00:00" style="border: 0px; background-color: transparent; width: 60px" disabled></td></tr>
			<tr><td style="width: 30%;padding-left: 5px;">TO:</td>
				<td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
					<select id="cbHourto">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
									echo '<option selected="true">'.$value.'</option>';
								}
						?></select>
						<input value=":59:59" style="border: 0px; background-color: transparent; width: 60px" disabled></td></tr>
			</table><br/>
			<div align="center">
			<input onclick="javacript:btnclick(this.value);" type="button" value="Submit" class="btn red">
			<input onclick="javascript:btnclick(this.value);" type="button" value="Yesterday" class="btn red">
			<input onclick="javascript:btnclick(this.value);" type="button" value="Today" class="btn red">
			<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red"></div>
	
	</form>	
	<br>
	<script type="text/javascript">
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	document.getElementById("datefrom").value=(year + "-" + month + "-" +day );
	document.getElementById("dateto").value=(year + "-" + month + "-" + day);
	</script>
</div>
	<div id="qry_result_summary"></div>
	<br>
	<div id="qry_result"></div>
	<br/>

</body>
<form method="post" action="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/Excel">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
</form>

</html>