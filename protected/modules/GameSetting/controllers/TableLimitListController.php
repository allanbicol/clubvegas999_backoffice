<?php
/**
 * @todo CurrencyListController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-27
 */
class TableLimitListController extends MyController
{
	public function actionTableBacarratLimit()
	{
		$bl=new GetTableLimit();
		$result = $bl->getCountBacarratLimitRecord();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		$player_records=$bl->getTableBacarratLimit();
		
		$filedNames = array("id","bac_all","banker_player","tie","pair","big","small");
		$urlAS= Yii::app()->request->baseUrl;
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$htmValue='<a class=\'btn mini red\' href=\'#\' id=\'limit_id\' onclick=\'javascript: $updateBacarratLimitDialog.dialog(\"open\");document.getElementById(\"update_bacarrat_err_message\").value=\"\";\'>Setting <i class=\'icon-cogs\'></i></a>';
		}else{
			$htmValue='Setting';
		}
		echo JsonUtil::jsonEncodeSystemSetting($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,7);
	}
	public function actionTableDragonTigerLimit()
	{
		$bl=new GetTableLimit();
		$result = $bl->getCountDragonTigerRecord();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		$player_records=$bl->getTableDragonTigerLimit();
	
		$filedNames = array("id","dra_all","dragon_tiger","tie","id");
		$urlAS= Yii::app()->request->baseUrl;
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$htmValue='<a class=\'btn mini red\' href=\'#\' id=\'limit_id\' onclick=\'javascript: $updateDragonTigerLimitDialog.dialog(\"open\");document.getElementById(\"update_dragon_err_message\").value=\"\";\'>Setting <i class=\'icon-cogs\'></i></a>';
		}else{
			$htmValue='Setting';
		}
		echo JsonUtil::jsonEncodeSystemSetting($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,4);
	}
	public function actionTableRouletteLimit()
	{
		$bl=new GetTableLimit();
		$result = $bl->getCountRouletteRecord();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		$player_records=$bl->getTableRouletteLimit();
	
		$filedNames = array("id","roul_all","35_to_1","18_to_1","12_to_1","9_to_1","6_to_1","2_to_1","1_to_1");
		$urlAS= Yii::app()->request->baseUrl;
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$htmValue='<a class=\'btn mini red\' href=\'#\' id=\'limit_id\' onclick=\'javascript: $updateRouletteLimitDialog.dialog(\"open\");document.getElementById(\"update_roulette_err_message\").value=\"\";\'>Setting <i class=\'icon-cogs\'></i></a>';
		}else{
			$htmValue='Setting';
		}
		echo JsonUtil::jsonEncodeSystemSetting($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,9);
	}
	public function actionTableBlackjackLimit()
	{
		$bl=new GetTableLimit();
		$result = $bl->getCountTableBlackjackLimit();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		$player_records=$bl->getTableBlackjackLimit();
	
		$filedNames = array("id","blc_all","pair","rummy","insurance");
		$urlAS= Yii::app()->request->baseUrl;
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$htmValue='<a class=\'btn mini red\' href=\'#\' id=\'limit_id\' onclick=\'javascript: $updateBlackjackLimitDialog.dialog(\"open\");document.getElementById(\"update_roulette_err_message\").value=\"\";\'>Setting <i class=\'icon-cogs\'></i></a>';
		}else{
			$htmValue='Setting';
		}
		echo JsonUtil::jsonEncodeSystemSetting($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,5);
	}
	public function actionTableAmericanRouletteLimit()
	{
		$bl=new GetTableLimit();
		$result = $bl->getCountAmericanRouletteRecord();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		$player_records=$bl->getTableAmericanRouletteLimit();
	
		$filedNames = array("id","amr_all","35_to_1","18_to_1","12_to_1","9_to_1","6_to_1","2_to_1","1_to_1");
		$urlAS= Yii::app()->request->baseUrl;
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$htmValue='<a class=\'btn mini red\' href=\'#\' id=\'limit_id\' onclick=\'javascript: $updateAmericanRouletteLimitDialog.dialog(\"open\");document.getElementById(\"update_american_roulette_err_message\").value=\"\";\'>Setting <i class=\'icon-cogs\'></i>
					.
					</a>';
		}else{
			$htmValue='Setting';
		}
		echo JsonUtil::jsonEncodeSystemSetting($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,9);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('gameSetting.readTableLimit'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}