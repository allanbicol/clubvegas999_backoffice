<?php
/**
 * @todo AgentTransactionHistoryController
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-15
 */
class AgentTransactionHistoryController extends MyController
{
	public function actionAgentTransactionHistory()
	{
		$bl=new TransactionHistory();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$bl->getCountAgentTransactionHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(counter)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records=$bl->getTransactionHistoryList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("agent_account_id","currency_name","number","total_amnt","total_amnt","currency_id");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a href=\"#\" name=\"currency_id\" title=\"agentaccountid\" onclick=\"javascript: extract_details(this);\"><u>agentaccountid</u></a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	
	public function actionAvailableCreditAgentTransactionHistory()
	{
		$bl=new TransactionHistory();
		$page = $_GET['page'];

		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];

		$sortType = $_GET['sord'];
		
		$result=$bl->getCountAvailableCreditAgentTransactionHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(counter)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records=$bl->getAvailableCreditTransactionHistoryList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("agent_account_id","currency_name","number","total_amnt","total_amnt","currency_id");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a href=\"#\" name=\"currency_id\" title=\"agentaccountid\" onclick=\"javascript: extract_details(this);\">agentaccountid</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	
	public function actionBonusAgentTransactionHistory()
	{
		$bl=new TransactionHistory();
		$page = $_GET['page'];

		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];

		$sortType = $_GET['sord'];
		
		$result=$bl->getCountBonusAgentTransactionHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(counter)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records=$bl->getBonusTransactionHistoryList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("agent_account_id","currency_name","number","total_amnt","total_amnt","currency_id");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a href=\"#\" name=\"currency_id\" title=\"agentaccountid\" onclick=\"javascript: extract_details(this);\">agentaccountid</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	
	public function actionAgentTransactionHistoryDetails()
	{
		$bl=new TransactionHistory();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result = $bl->getCountTransactionHistory();
		$data=$result->readAll();
		
		$records=$data[0]['cnt'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
		$player_records=$bl->getTransactionHistoryDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("transaction_number","agent_account_id","currency_name","trans_type_id","amount","market_type","credit_before","credit_after","balance_before","balance_after","transaction_date","operator_id");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readTransactionHistory') || User::getUserType() === Constants::ROLE_AGENT)
		{	
			$this->render("index");
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}