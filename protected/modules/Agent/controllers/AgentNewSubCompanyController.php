<?php
/**
 * @todo AgentNewSubCompanyController
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-08
 */
class AgentNewSubCompanyController extends MyController
{
	/**
	 * @todo compare player_win_max to balance
	 * @author
	 * @since 2012-05-10
	 */
	public function actionComplaerWinMaxToBalance()
	{
		$sa = new AgentNewSubCompany();
		$sa->comparePlayerWinMaxToBalance();
	}
	
	/**
	 * @todo get agen't assigned credit
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-10
	 */
	public function actionGetAssignedCredit()
	{
		$sa = new AgentNewSubCompany();
		$sa->getAgentAssignedCredit();
	}
	/**
	 * @todo Check player balance still on a lobby
	 * @copyright CE
	 * @author allan
	 * @since 2013-08-20
	 */
	public function actionIsStockOnTheLobby()
	{
		$sa = new AgentNewSubCompany();
		$sa->isStockOnTheLobby();
	}
	
	/**
	 * @todo check account_id availability
	 * @copyright CE
	 * @author 
	 * @since 
	 */
	public function actionCheckAccountIdAvailability()
	{
// 		$ca = new AccountIdAvailability();
// 		$ca->checkAvailability();
		$model = new AgentNewSubCompany();
		echo ($model->isAccountAvailable($_POST['account_ID'])) ? 0 : 1;
	}
	
	/**
	 * @todo save agent's setting
	 * @author leokarl
	 * @since 2012-05-08
	 */
	public function actionSaveAgentNewSubCompany()
	{
		if(Yii::app()->user->checkAccess('agent.addNewSubCompany') || Yii::app()->user->checkAccess('agent.writeSubCompanySetting') || Yii::app()->session['account_type'] ==='agent')
		{
			$sa = new AgentNewSubCompany();
			$sa->saveAgentNewSubCompany();
		}
		else 
		{
			echo "You don't have permission to update the setting!";	
		}
	}
	
	public function actionCommisionAndSharing()
	{
		$cpd = new AgentCommissionAndSharing();
		$cpd->getAgentCommissionAndSharing();
	}
	public function actionGetCurrencyExchangeRate()
	{
		$cpd = new CurrencyExchangeRate();
		$cpd->getRate();
	}
	public function actionLoadDialogNewSubCompany()
	{
		$this->renderPartial("dialogNewSubCompany");
	}
	public function actionLoadDialogSeniorMaster()
	{
		$this->renderPartial("dialogSeniorMasterAgent");
	}
	public function actionLoadDialogMaster()
	{
		$this->renderPartial("dialogMasterAgent");
	}
	public function actionLoadDialogAgent()
	{
		$this->renderPartial("dialogAgent");
	}
	public function actionLoadDialogMember()
	{
		$this->renderPartial("dialogMember");
	}
	
	
	public function actionIndex()
	{
		$editMode = isset($_GET['e'])? $_GET['e'] : 0;
		$model = new AgentNewSubCompany();
		
		// account_id length must always be 2 to 10
		if(isset($_GET['account_id'])){
			if(strlen($_GET['account_id']) < 2){
				$this->render("validationError",array('label' => 'Warning','message' => 'Account doesn\'t exist!'));
				exit();
			}
		}
		
		if ($editMode == 1) //edit agent setting
		{
			if(Yii::app()->user->checkAccess('agent.readSubCompanySetting') || User::getUserType() === Constants::ROLE_AGENT)
			{
				/*
				 * @todo URL Validation
				 * @todo 1. if transaction is for updates, $_GET['type'] must be present.
				 * @todo 2. if transaction is for updates, $_GET['account_id'] must be present.
				 * @todo 3. validate $_GET['account_id'] vs $_GET['type']
				 * @todo 4. check if agent or player exist before edit/updates.
				 * @author leokarl
				 * @since 2012-12-17
				 */
				if(isset($_GET['e'])){
					// 1. if transaction is for updates, $_GET['type'] must be present.
					if(!isset($_GET['type'])){
						$this->render("validationError",array('label' => 'Error','message' => 'Invalid URL!'));
						exit();
					}
					
					// 2. if transaction is for updates, $_GET['account_id'] must be present.
					if(!isset($_GET['account_id'])){
						$this->render("validationError",array('label' => 'Error','message' => 'Invalid URL!'));
						exit();
					}
					
					// 3. validate $_GET['account_id'] vs $_GET['type']
					if(!$model->isAccountIdVStypeValid($_GET['account_id'], $_GET['type'])){
						$this->render("validationError",array('label' => 'Warning','message' => 'Account doesn\'t exist!'));
						exit();
					}
					
					// 4. check if agent or player exist before edit/updates.
					if($_GET['type'] != 'MEM'){
						if(!$model->isAccountExistedInAgent($_GET['account_id'])){
							// exit if account id doesn't exist
							$this->render("validationError",array('label' => 'Warning','message' => 'Account doesn\'t exist!'));
							exit();
						}
					}else{
						if(!$model->isAccountExistedInAgentPlayer($_GET['account_id'])){
							// exit if account id doesn't exist
							$this->render("validationError",array('label' => 'Warning','message' => 'Account doesn\'t exist!'));
							exit();
						}
					}
				}
				// Render index
				$this->render("index");
			}
			else
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
		else if ($editMode === 0) // creat new agent
		{
			if(Yii::app()->user->checkAccess('agent.addNewSubCompany') || User::getUserType() === Constants::ROLE_AGENT)
			{
				/*
				 * @todo URL Validation
				 * @todo 1. if $_GET['type'] is set, $_GET['account_id'] must set too.
				 * @todo 2. if $_GET['account_id'] is set, $_GET['type'] must set too.
				 * @todo 3. check upper agent account_id if exist.
				 * @todo 4. logged-in agent can only add lower agent(s).
				 * @todo 5. $_GET['type'] vs $_GET['account_id'] validation.
				 * @author leokarl
				 * @since 2012-12-18
				 */
				
				// 1. if $_GET['type'] is set, $_GET['account_id'] must set too
				if(isset($_GET['type'])){
					if(!isset($_GET['account_id'])){
						$this->render("validationError",array('label' => 'Error','message' => 'Invalid URL!'));
						exit();
					}
				}	
				
				
				if(isset($_GET['account_id'])){
					// 2. if $_GET['account_id'] is set, $_GET['type'] must set too
					if(!isset($_GET['type'])){
						$this->render("validationError",array('label' => 'Error','message' => 'Invalid URL!'));
						exit();
					}
					
					// 3. check upper agent account_id if exist
					if(!$model->isAccountExistedInAgent($_GET['account_id'])){
						// exit if account id doesn't exist
						$this->render("validationError",array('label' => 'Warning','message' => 'Account doesn\'t exist!'));
						exit();
					}
				}
				
				if(isset($_GET['type']) && $_GET['account_id']){
					
					if(User::getUserType() == 'agent'){
						// 4. logged-in agent can only add lower agent(s).
						if(strlen($_GET['account_id']) < strlen(Yii::app()->session['account_id'])){ 
							$this->render("validationError",array('label' => 'Warning','message' => 'You are not authorized to access this account!'));
							exit();
						}
					}
					
					// 5. $_GET['type'] vs $_GET['account_id'] validation
					if($_GET['type'] != $model->getAccountTypeByCharLength(strlen($_GET['account_id']) + 2)){
						$this->render("validationError",array('label' => 'Warning','message' => 'Account doesn\'t exist!'));
						exit();
					}
				}
				
				// Render index
				$this->render("index");
			}
			else
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
		
	}
}