<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/role.css" />

<?php 
echo '<b>'. CHtml::link('Role',Yii::app()->request->baseUrl . '/index.php?r=SystemSetting/Role',array('class'=>'link')) .' >> ' . $_GET['role_id'] . '</b><br/><br/>';
?>
<div id="update_header">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="left"><font color="red">*</font>Role ID: </td>
		<td class="right">
		<?php 
			if(Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission')){
				echo CHtml::textField('txtRoleID',$_GET['role_id'],array('id'=>'txtRoleID'));
			}else{
				echo CHtml::textField('txtRoleID',$_GET['role_id'],array('id'=>'txtRoleID','readonly'=>true));
			}
		?>
		<i>(<b>Note:</b> Role ID will be converted to lower case)</i>
		</td>
	</tr>
	<tr>
		<td class="left"><font color="red">*</font>Role Name : </td>
		<td class="right">
		<?php 
			if(Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission')){
				echo CHtml::textField('txtRoleName',$role_name,array('id'=>'txtRoleName'));
			}else{
				echo CHtml::textField('txtRoleName',$role_name,array('id'=>'txtRoleName','readonly'=>true));
			}
		?>
		</td>
	</tr>
	<tr>
		<td class="left"><font color="red">*</font>Home Page : </td>
		<td class="right">
		<?php 
			if(Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission')){
				echo CHtml::dropDownList('ddlHomePageUrl', $home_page_url_id ,$home_page_list, array('empty' => '(Select Home Page)'));
			}else{
				echo CHtml::dropDownList('ddlHomePageUrl', $home_page_url_id ,$home_page_list,array('disabled' =>true));
			}
		?>
		</td>
	</tr>
	</table>
</div>
<b>Permissions</b>
<div id="update_body">
	<fieldset><legend>Agent</legend>
	
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($agent); $i++){
// this separates the permission name
// 				list($module_name,$role) = explode('.',$agent[$i]['name']); // get role from role_name. ex: addNewSubCompany from agent. addNewSubCompany
// 				preg_match_all('/[A-Z][^A-Z]*/',ucfirst($role), $results); // separate words by caps letter
// 				$label = implode(' ', $results[0]); // concat words
				echo '<li><label>' . CHtml::checkBox('chkAgent',$agent[$i]['s_checked'],array('value'=>$agent[$i]['name'])) . $agent[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	
	<fieldset><legend>CashPlayer</legend>
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($cashplayer); $i++){
				echo '<li><label>' . CHtml::checkBox('chkCashplayer',$cashplayer[$i]['s_checked'],array('value'=>$cashplayer[$i]['name'])) . $cashplayer[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	
	<fieldset><legend>Banking</legend>
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($banking); $i++){
				echo '<li><label>' . CHtml::checkBox('chkBanking',$banking[$i]['s_checked'],array('value'=>$banking[$i]['name'])) . $banking[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	
	<fieldset><legend>Game Setting</legend>
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($gamesetting); $i++){
				echo '<li><label>' . CHtml::checkBox('chkGameSetting',$gamesetting[$i]['s_checked'],array('value'=>$gamesetting[$i]['name'])) . $gamesetting[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	
	<fieldset><legend>System Monitor</legend>
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($systemmonitor); $i++){
				echo '<li><label>' . CHtml::checkBox('chkSystemMonitor',$systemmonitor[$i]['s_checked'],array('value'=>$systemmonitor[$i]['name'])) . $systemmonitor[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	
	<fieldset><legend>System Setting</legend>
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($systemsetting); $i++){
				echo '<li><label>' . CHtml::checkBox('chkSystemSetting',$systemsetting[$i]['s_checked'],array('value'=>$systemsetting[$i]['name'])) . $systemsetting[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	
	<fieldset><legend>Security Log</legend>
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($securitylog); $i++){
				echo '<li><label>' . CHtml::checkBox('chkSecurityLog',$securitylog[$i]['s_checked'],array('value'=>$securitylog[$i]['name'])) . $securitylog[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	
	<fieldset><legend>Marketing Custom Report</legend>
		<ul class="role_list_horizontal">
			<?php 
			for($i=0; $i<count($marketingcustomreport); $i++){
				echo '<li><label>' . CHtml::checkBox('chkMarketingCustomReport',$marketingcustomreport[$i]['s_checked'],array('value'=>$marketingcustomreport[$i]['name'])) . $marketingcustomreport[$i]['description']. '</label></li>'; // list
			}
			?>
		</ul>
	</fieldset>
	<br/><br/>
	<?php 
		echo CHtml::link('Check All','',array('class'=>'link','onclick'=>'checkAll()','class'=>'btn mini red','style'=>'margin:5px;'));
		echo CHtml::link('Uncheck All','',array('class'=>'link','onclick'=>'unCheckAll()','class'=>'btn mini red','style'=>'margin:5px;'));
	?>
</div>
<?php
if(Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission')){ 
	echo CHtml::button('Save',array('id'=>'btnSave','class'=>'btn red','onclick'=>'saveRole("'. $_GET['role_id'] .'")'));
	echo CHtml::button('Reset',array('id'=>'btnReset','class'=>'btn red','onclick'=>'location.reload();'));
}else{
	echo CHtml::button('Save',array('id'=>'btnSave','class'=>'btn red disabled'));
	echo CHtml::button('Reset',array('id'=>'btnReset','class'=>'btn red disabled'));
}
?>

<script>
	//active menu color
	document.getElementById('systemsettingHeader').className="start active";
	document.getElementById('mnu_user_role_information').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i><li>System Setting <i class='icon-angle-right'></i></li><li>User Role <i class='icon-angle-right'></i></li><li><a href='#'>Edit</a></li>");

	/**
	 * @todo check all checkboxes
	 * @author leokarl
	 * @since 2013-01-08
	 */
	function checkAll(){
		$('input[name="chkAgent"]').attr('checked', true);
		$('input[name="chkCashplayer"]').attr('checked', true);
		$('input[name="chkBanking"]').attr('checked', true);
		$('input[name="chkGameSetting"]').attr('checked', true);
		$('input[name="chkSystemMonitor"]').attr('checked', true);
		$('input[name="chkSystemSetting"]').attr('checked', true);
		$('input[name="chkSecurityLog"]').attr('checked', true);
		$('input[name="chkMarketingCustomReport"]').attr('checked', true);
	}

	/**
	 * @todo uncheck all checkboxes
	 * @author leokarl
	 * @since 2013-01-08
	 */
	function unCheckAll(){
		$('input[name="chkAgent"]').attr('checked', false);
		$('input[name="chkCashplayer"]').attr('checked', false);
		$('input[name="chkBanking"]').attr('checked', false);
		$('input[name="chkGameSetting"]').attr('checked', false);
		$('input[name="chkSystemMonitor"]').attr('checked', false);
		$('input[name="chkSystemSetting"]').attr('checked', false);
		$('input[name="chkSecurityLog"]').attr('checked', false);
		$('input[name="chkMarketingCustomReport"]').attr('checked', false);
	}

	function saveRole(role_name){
		// initialize
		var agent_roles = [];
		var cashplayer_roles = [];
		var banking_roles = [];
		var game_setting_roles = [];
		var system_monitor_roles = [];
		var system_setting_roles = [];
		var security_log_roles = [];
		var marketing_customreport_roles = [];

		// get all checked roles
	    $('input[name="chkAgent"]:checked').each(function(){ agent_roles.push($(this).val());});
	    $('input[name="chkCashplayer"]:checked').each(function(){ cashplayer_roles.push($(this).val());});
	    $('input[name="chkBanking"]:checked').each(function(){ banking_roles.push($(this).val());});
	    $('input[name="chkGameSetting"]:checked').each(function(){ game_setting_roles.push($(this).val());});
	    $('input[name="chkSystemMonitor"]:checked').each(function(){ system_monitor_roles.push($(this).val());});
	    $('input[name="chkSystemSetting"]:checked').each(function(){ system_setting_roles.push($(this).val());});
	    $('input[name="chkSecurityLog"]:checked').each(function(){ security_log_roles.push($(this).val());});
	    $('input[name="chkMarketingCustomReport"]:checked').each(function(){ marketing_customreport_roles.push($(this).val());});

	    agent_roles = (agent_roles == '') ? '' : agent_roles;
	    cashplayer_roles = (cashplayer_roles == '') ? '' : cashplayer_roles; 
	    banking_roles = (banking_roles == '') ? '' : banking_roles;
	    game_setting_roles = (game_setting_roles == '') ? '' : game_setting_roles;
	    system_monitor_roles = (system_monitor_roles == '') ? '' : system_monitor_roles;
	    system_setting_roles = (system_setting_roles == '') ? '' : system_setting_roles;
	    security_log_roles = (security_log_roles == '') ? '' : security_log_roles;
	    marketing_customreport_roles = (marketing_customreport_roles == '') ? '' : marketing_customreport_roles;
	    
	    // process
		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl?>' + '/index.php?r=SystemSetting/Role/UpdateRole',
    		type: 'POST',
    		data: {'role_id_before':role_name, 'role_id': $('#txtRoleID').val(), 
        		'home_page_url_id' : $('#ddlHomePageUrl').val(),
        		'role_name': $('#txtRoleName').val(),
        		'agent_roles': agent_roles,
        		'cashplayer_roles': cashplayer_roles,
        		'banking_roles': banking_roles,
        		'game_setting_roles': game_setting_roles,
        		'system_monitor_roles': system_monitor_roles,
        		'system_setting_roles': system_setting_roles,
        		'security_log_roles': security_log_roles,
        		'marketing_customreport_roles': marketing_customreport_roles
        		},
        		
    		success: function(msg) {
        		if(msg == 'no_permission'){
            		alert('Warning: You don\'t have permission to update user role!');
    			}else if(msg == 'session_expired'){
        			alert('Session expired!');
    				document.location.href = '<?php echo Yii::app()->request->baseUrl?>';
    			}else if(msg == 'data_param_not_set'){
        			alert('Warning: Data parameter(s) not set properly. Please refresh your page!');
        		}else if(msg == 'role_name_not_exist'){
            		alert('Warning: Invalid role name!');
            	}else if(msg == 'role_id_blank'){
            		alert('Warning: Role ID is required!');
            		$('#txtRoleID').focus();
            	}else if(msg == 'role_name_blank'){
            		alert('Warning: Role name is required!');
            		$('#txtRoleName').focus();
            	}else if(msg == 'role_name_already_exist'){
                	alert('Warning: Role name is already exist!');
                	$('#txtRoleID').focus();
            	}else if(msg == 'special_char_present'){
            		alert('Warning: Role ID must not contain any special character including white space!');
                	$('#txtRoleID').focus();
            	}else if(msg == 'special_char_present_in_role_name'){
            		alert('Warning: Role name must not contain any special character!');
                	$('#txtRoleName').focus();
            	}else if(msg == 'invalid_permission'){
                	alert('Error: Invalid permission exist!');
            	}else if(msg == 'blank_home_page_url'){
                	alert('Warning: Please select home page!');
                	$('#ddlHomePageUrl').focus();
            	}else if(msg == 'completed'){
                	alert('User role update completed!');
                	document.location.href = '<?php echo Yii::app()->request->baseUrl?>' + '/index.php?r=SystemSetting/Role/FormUpdate&role_id=' + $('#txtRoleID').val();
                }
	    	}
    	});
	}
</script>
<br/>
<br/>
</html>