<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentwinloss.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('agentHeader').className="start active";
	document.getElementById('agentHeader').className="start active";
	document.getElementById('mnu_afundTrans').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Agent Fund Transfer History</a></li>");
</script>

<script type="text/javascript">

	var cRowNo=0;
	function tableTransHistoryToday(currency_id,dateFrom,dateTo,testChecked,accountId,optSelect,transItem) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentFundTransferHistory/AgentFundTransferHistory&currencyId='+currency_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&accountId='+accountId, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>', '<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>'],
			    colModel: [
			        {name: 'no', index: 'no', width: 25,title:false,sortable:false},
					{name: 'account_id', index: 'account_id', width: 200, search:true,title:false,formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
					{name: 'account_name', index: 'account_name', width: 200,title:false},
					{name: 'currency_name', index: 'currency_name', width: 150,title:false},
				],
			    loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list1");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentFundTransferHistoryDetail&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo)+"&opt="+optSelect+"&item="+transItem+'&s_id='+document.getElementById("txtAccountId").value;
			                }
			                e.preventDefault();
			            });
				    
			        } 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"player_id","",{'text-decoration':'underline'});
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
					}     
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
	
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <?php echo Yii::t('agent','agent.agentsystem.agent_fund_transfer_history');?> - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					//groupSummary : [true],
					groupDataSorted : true
			   	},
			
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});

			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export current page", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportFundToExcel(optSelect);
	           }, 
	            position:"last"
	        }); 
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
	            caption:"Export All", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllFundToExcel(currency_id,dateFrom,dateTo,testChecked,accountId,optSelect,transItem);
	           }, 
	            position:"last"
	        });   
			  
		});
	} 

</script>
<script type="text/javascript">

function exportFundToExcel(optSelect){
	var accountId = $('#txtAccountId')[0].value;
	var transItem ="";
	
	if (optSelect=='others'){
		transItem = $('#depositType')[0].value + '-' + $('#withdrawType')[0].value;
	}else if (optSelect=='bank'){
		transItem = $('#selectBank')[0].value + '-' + $('#selectBank1')[0].value;
	}else{ 
		transItem='ALL-ALL';
	}
	
	var currency = $('#currencyType')[0].value;
	testChecked=0;
	if (document.getElementById("chkTest").checked==true){
		 testChecked=1;
	}
	var testC	=	testChecked;
	var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
	var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
	
	var table1= document.getElementById('qry_result');
	var table2= document.getElementById('qry_resultTotal');
	var html1 = table1.outerHTML;
	var html2 = table2.outerHTML;
	document.forms[2].csvBuffer.value="";
	document.forms[2].txtParams.value="";
	document.forms[2].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+optSelect+'#'+accountId+'#'+transItem+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
	document.forms[2].csvBuffer.value=html1+''+html2 ;
    document.forms[2].method='POST';
    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentFundTransferHistory/FundExcel';  // send it to server which will open this contents in excel file
    document.forms[2].target='_top';
    document.forms[2].submit();
}

function exportAllFundToExcel(currency_id,dateFrom,dateTo,testChecked,accountId,optSelect,transItem){

    $.ajax({
		 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentFundTransferHistory/ExportAllFundTransferHistory&currencyId='+currency_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&accountId='+accountId+'&optSel='+optSelect+'&transItem='+transItem,
		 async:true,
		 success: function(result) {
				var data =result.split("<BREAK>");
				
				var accountId = $('#txtAccountId')[0].value;
				var transItem ="";
				
				if (optSelect=='others'){
					transItem = $('#depositType')[0].value + '-' + $('#withdrawType')[0].value;
				}else if (optSelect=='bank'){
					transItem = $('#selectBank')[0].value + '-' + $('#selectBank1')[0].value;
				}else{ 
					transItem='ALL-ALL';
				}
				
				var currency = $('#currencyType')[0].value;
				testChecked=0;
				if (document.getElementById("chkTest").checked==true){
					 testChecked=1;
				}
				var testC	=	testChecked;
				var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
				var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';

				document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+optSelect+'#'+accountId+'#'+transItem+'#'+currency+'#'+testC+'#'+dateFrom+'#'+dateTo;
				document.forms[2].csvBuffer.value=data[0]+""+data[2] ;
				document.forms[2].method='POST';
			   	document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentFundTransferHistory/FundExcel';  // send it to server which will open this contents in excel file
			    document.forms[2].target='_top';
			   	document.forms[2].submit();
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
}

function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
var sChar=false;
function alphanumeric(inputtxt)  
{  
	var letters = /^[0-9a-zA-Z]+$/;  
	if(inputtxt.match(letters) || inputtxt=='')  
	{  
		sChar=false;  
	}else{  
		sChar=true;  
	}  
}  

function changeCurrency(){
	var lblExcept = document.getElementById('lblExcept');
	var chkTest = document.getElementById('chkTest');
	if (document.getElementById('currencyType').value != "ALL"){
		chkTest.style.visibility = 'hidden';
		lblExcept.style.visibility = 'hidden';
		chkTest.checked=false;
	 }else{
		chkTest.style.visibility = 'visible';
		lblExcept.style.visibility = 'visible';
     }
}
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

	function loadTransTable(selected_currency,dateFrom,dateTo,testChecked,accountId){
		if (selected_currency=='ALL'){
			//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_currency,dateFrom,dateTo,testChecked,accountId);

		}
	    else
	    {
	    	//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_currency,dateFrom,dateTo,testChecked,accountId);

		}
	}

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{

			document.getElementById("chkTest").checked=true;
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			var option='';
			var itemSelected='';
			
			<?php if (isset($_GET['dateFrom'])==''){?>
				var mydate= new Date();
				mydate.setDate(mydate.getDate());
				var theyear=mydate.getFullYear();
				var themonth=mydate.getMonth()+1;
				var theday=mydate.getDate();
				
				var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
				var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
				var accountId=document.getElementById('txtAccountId').value;
				
				alphanumeric(accountId);
				
				if (sChar==false){
					loadTransTable("ALL",datefrom,dateto,testChecked,accountId);
				}
				
			<?php }else{?>
				var datefrom=enc('<?php echo $_GET['dateFrom'];?>');
				var dateto=enc('<?php echo $_GET['dateTo'];?>');
				var id=enc('<?php echo $_GET['id'];?>');
				
				
				var from=datefrom.split("_");
				var to=dateto.split("_");
				document.getElementById('datefrom').value=from[0].replace("/","-").replace("/","-");
				document.getElementById('dateto').value=to[0].replace("/","-").replace("/","-");
				document.getElementById('txtAccountId').value=id;
				var accountId=id;

				loadTransTable("ALL",datefrom,dateto,testChecked,accountId);
				
				
			<?php }?>
			
		}
		else if (btnname=="Today")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var currencytype=document.getElementById('currencyType').value;
			var accountId=document.getElementById('txtAccountId').value;
			
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(currencytype,datefrom,dateto,testChecked,accountId);
			}
		}
		else if (btnname=="Yesterday")
		{
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var currencytype=document.getElementById('currencyType').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(currencytype,datefrom,dateto,testChecked,accountId);
			}
		}
		else if (btnname=="Submit")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var currencytype=document.getElementById('currencyType').value;
			var accountId=document.getElementById('txtAccountId').value;
			alphanumeric(accountId);
			if (sChar==false){
				loadTransTable(currencytype,dateSubmitFrom,dateSubmitTo,testChecked,accountId);
			}
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area" style="width: 500px">
	<div class="header" ><?php echo Yii::t('agent','agent.agentsystem.agent_fund_transfer_history');?></div>
	<form action="" style="padding-top: 5px;">
		<table>
			<?php if(Yii::app()->session['level'] !='SC' && Yii::app()->session['level'] !='SMA' && Yii::app()->session['level'] !='MA' && Yii::app()->session['level'] !='AGT'){ ?>
				<tr><td style="width: 30%;padding-left:5px;"><?php echo Yii::t('agent','agent.agentwinloss.account_id');?>:</td>
					<td>
						<input type="text" id="txtAccountId" style="width: 130px">
					</td>
				</tr>
			<?php }else{ ?>
				<tr style="display:none;"><td style="width: 30%;padding-left:5px;">ACCOUNT ID:</td>
					<td>
						<input type="text" id="txtAccountId" style="width: 130px">
					</td>
				</tr>
			<?php }?>
			<tr><td style="width: 30%;padding-left:5px;"><?php echo Yii::t('agent','agent.agentwinloss.currency');?>:</td>
				<td><select id="currencyType" onChange="javascript:changeCurrency();">
						<option value="ALL">ALL</option>
		  				<?php 
						$dataReader = TableCurrency::model()->findAll();
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
						}
						?>
					</select>&nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="chkTest" checked="checked" ><label id="lblExcept"><?php echo Yii::t('agent','agent.agentwinloss.except_test_currency');?></label>

				</td>
			</tr>
			<tr><td style="width: 30%;padding-left:5px;"><?php echo Yii::t('agent','agent.agentwinloss.from');?> :</td>
				<td><input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />
						<select id="cbHourfrom">
							<?php 
								$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
								echo '<option>'.$value.'</option>';
								}
							?>
						</select>
					<input value=":00:00" style="border: 0px; background-color: transparent ; width: 50px" disabled>
				</td></tr>
			<tr><td style="width: 30%;padding-left:5px;"><?php echo Yii::t('agent','agent.agentwinloss.to');?> :</td>
				<td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
						<select id="cbHourto">
							<?php 
								$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
								echo '<option selected="true">'.$value.'</option>';
								}
							?>
						</select>
					<input value=":59:59" style="border: 0px; background-color: transparent; width: 50px" disabled >
				</td>
			</tr>
			</table>
			<div align="center" style="height: 35px; v-align: middle;padding-top:10px;">
				<input onclick="javacript: btnclick(this.title);" title="Submit"  id="Submit" type="button" value="<?php echo Yii::t('agent','agent.agentwinloss.submit');?>" class="btn red">
				<input onclick="javascript: btnclick(this.title);" title="Yesterday" id="txtYesterday" type="button" value="<?php echo Yii::t('agent','agent.agentwinloss.yesterday');?>" class="btn red">
				<input onclick="javascript: btnclick(this.title);" title="Today" id="txtToday" type="button" value="<?php echo Yii::t('agent','agent.agentwinloss.today');?>" class="btn red">
				<input onclick="javascript:history.go(-1);" type="button" value="<?php echo Yii::t('agent','agent.subcompanylist.back');?>" class="btn red">
			</div>
	
	</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		month=month.toString();
		day=day.toString();
		
		if (month.length==1){
			month='0'+month;
		}
		if (day.length==1){
			day='0'+day;
		}
		document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
		document.getElementById("dateto").value=(year + "-" + month + "-" + day);
		</script>

</div>		
	<div id="qry_result"></div>
	<div id="pager1"></div>
	<br/>
	<div id="qry_resultTotal"></div>

	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
	</form>
</body>
</html>