<?php
/**
 * @todo AgentParameterController
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-14
 */
class AgentParameterController extends MyController
{
	public function actionSaveAgentParameter()
	{
		if(Yii::app()->user->checkAccess('agent.writeAgentParameters'))
		{
			$ap = new AgentParameter();
			$ap->saveAgentParameter();
			echo Yii::t('agent','agent.parameter.title') . Yii::t('agent','agent.parameter.saved');
		}
		else
			echo "You don't have permission to update the setting!";
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readAgentParameters'))
		{	
			$data = TableAgentParameter::model()->find("id=1");
			$this->render("index",array("rd" => $data));
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}