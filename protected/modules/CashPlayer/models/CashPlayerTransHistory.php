<?php

class CashPlayerTransHistory 
{


	public function getPlayerTransHistory($orderField, $sortType, $startIndex , $limit)
	{
		if ($_GET['test']!=0){
			$test="and currencyName<>'TEST' and test_real_currency <>1";
		}else{
			$test="";
		}
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['trantype']=="All" && $_GET['status']=="All")
		{
			$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id<>6 $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		}
		elseif ($_GET['trantype']=="All" && $_GET['status']<>"All")
		{
			$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id<>6 $test $accountID and status='". $_GET['status'] ."'  and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		}
		elseif ($_GET['trantype']<>"All" && $_GET['status']=="All")
		{
			$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id='". $_GET['trantype'] ."' $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		}
		elseif($_GET['trantype']<>"All" && $_GET['status']<>"All")
		{
			$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id='". $_GET['trantype'] ."' $test $accountID and status='". $_GET['status'] ."'  and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		}

		$rows = $command->query();
		return $rows;
	

	}
	public function getCountPlayerTransHistory()
	{
		if ($_GET['test']!=0){
			$test="and currencyName<>'TEST'  and test_real_currency <>1";
		}else{
			$test="";
		}
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['trantype']=="All" && $_GET['status']=="All")
		{
			$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory where cash_player_trans_type_id<>6 $test $accountID  and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		elseif ($_GET['trantype']=="All" && $_GET['status']<>"All")
		{
			$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory where cash_player_trans_type_id<>6 $test $accountID and status='". $_GET['status'] ."'  and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		elseif ($_GET['trantype']<>"All" && $_GET['status']=="All")
		{
			$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory where cash_player_trans_type_id='". $_GET['trantype'] ."' $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		elseif($_GET['trantype']<>"All" && $_GET['status']<>"All")
		{
			$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory where cash_player_trans_type_id='". $_GET['trantype'] ."' $test $accountID and status='". $_GET['status'] ."'  and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		
		$rows = $command->query();
		return $rows;
	
	}

	public function getPlayerTransHistoryForBonus($orderField, $sortType, $startIndex , $limit)
	{

	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and currencyName<>'TEST' and test_real_currency <>1";
		}else{
			$test="";
		}
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
		
		$command = $connection->createCommand("Select * from vwGetTransactionHistory WHERE  cash_player_trans_type_id=6 $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		$rows = $command->query();
		return $rows;
	
	}
	public function getCountPlayerTransHistoryForBonus()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and currencyName<>'TEST'  and test_real_currency <>1";
		}else{
			$test="";
		}
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
		
		$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory WHERE  cash_player_trans_type_id=6 $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'" );
		$rows = $command->query();
		return $rows;
	
	}
	
	public function exportAllPlayerTransHistory()
	{
		if ($_GET['test']!=0){
			$test="and currencyName<>'TEST' and test_real_currency <>1";
		}else{
			$test="";
		}
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
	
		if ($_GET['trantype']=="All" && $_GET['status']=="All")
		{
			$command = $connection->createCommand("Select transNumber,player_id,full_name,transTypeName,currencyName,Amount,balance_before,balance_after,deposit_via,submitTime,finishTime,cashierID,status from vwGetTransactionHistory where cash_player_trans_type_id<>6 $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		elseif ($_GET['trantype']=="All" && $_GET['status']<>"All")
		{
			$command = $connection->createCommand("Select transNumber,player_id,full_name,transTypeName,currencyName,Amount,balance_before,balance_after,deposit_via,submitTime,finishTime,cashierID,status from vwGetTransactionHistory where cash_player_trans_type_id<>6 $test $accountID and status='". $_GET['status'] ."'  and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		elseif ($_GET['trantype']<>"All" && $_GET['status']=="All")
		{
			$command = $connection->createCommand("Select transNumber,player_id,full_name,transTypeName,currencyName,Amount,balance_before,balance_after,deposit_via,submitTime,finishTime,cashierID,status from vwGetTransactionHistory where cash_player_trans_type_id='". $_GET['trantype'] ."' $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		elseif($_GET['trantype']<>"All" && $_GET['status']<>"All")
		{
			$command = $connection->createCommand("Select transNumber,player_id,full_name,transTypeName,currencyName,Amount,balance_before,balance_after,deposit_via,submitTime,finishTime,cashierID,status from vwGetTransactionHistory where cash_player_trans_type_id='". $_GET['trantype'] ."' $test $accountID and status='". $_GET['status'] ."'  and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'");
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function exportAllPlayerTransHistoryForBonus()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and currencyName<>'TEST' and test_real_currency <>1";
		}else{
			$test="";
		}
		if ($_GET['accountId']!=""){
			$accountID="and player_id="."'".$_GET['accountId']."'";
		}else{
			$accountID='';
		}
	
		$command = $connection->createCommand("Select transNumber,player_id,full_name,cashierID,Amount,deposit_via,balance_before,balance_after,currencyName,submitTime,status from vwGetTransactionHistory WHERE  cash_player_trans_type_id=6 $test $accountID and finishTime between '". $_GET['datefrom'] ."' and '".  $_GET['dateto'] ."'" );
		$rows = $command->query();
		return $rows;
	}
}