<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/newsubcompany.css" />
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.7.2/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	

	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/agentnewsubcompany.js"></script>
	<script type="text/javascript">
	    var urlGameSharingAndCommission='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/CommisionAndSharing';
	    var urlGetCurrencyExchageRate='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/GetCurrencyExchangeRate';
		var urlGetAssignedCredit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/GetAssignedCredit';
		var urlGetSelectedGameTableLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSelectedGameTableLimit/GetSelectedGameTableLimit';
		var urlTableBaccaratLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableBacarratLimit';
	    var checkAvailabilityIssetType='<?php if(isset($_GET['type'])){echo $_GET['type'];}else{echo 'SC';}?>';
	    var lblAccountExist='<?php echo Yii::t('agent','agent.newsubcompany.account_exist');?>';
		var lblAccountAvailable='<?php echo Yii::t('agent','agent.newsubcompany.account_available');?>';
		var strAgentType='<?php if(isset($_GET['type'])){echo $_GET['type'];}?>';
		var agent_level='<?php Yii::app()->session['level'];?>';
		var s_edit='<?php if(isset($_GET['e'])){echo '1';}else{echo '0';}?>';
		var strAccountId='<?php if(isset($_GET['account_id'])){echo $_GET['account_id'];}?>';
		//var agent_type='<?php if(isset($_GET['type'])){echo $_GET['type'];}else{echo Yii::app()->session['level'];}?>';
		var agent_type='<?php if(isset($_GET['type'])){echo $_GET['type'];}else{echo Yii::app()->session['account_type'];}?>';
		//var for_edit='<?php if(isset($_GET['e'])){echo '0';}else{echo '1';}?>';
		var for_edit='<?php if(isset($_GET['e'])){echo '1';}else{echo '0';}?>';
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var lblAll='<?php echo Yii::t('agent','agent.newsubcompany.all');?>';
		var lblBankerPlayer='<?php echo Yii::t('agent','agent.newsubcompany.banker_player');?>';
		var lblTie='<?php echo Yii::t('agent','agent.newsubcompany.tie');?>';
		var lblPair='<?php echo Yii::t('agent','agent.newsubcompany.pair');?>';
		var lblBig='<?php echo Yii::t('agent','agent.newsubcompany.big');?>';
		var lblSmall='<?php echo Yii::t('agent','agent.newsubcompany.small');?>';
	    var urlAgentGameTableLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSelectedGameTableLimit/AgentGameTableLimit';
		var lblBaccarat='<?php echo Yii::t('agent','agent.newsubcompany.baccarat');?>';
		var lblLimit='<?php echo Yii::t('agent','agent.newsubcompany.limit');?>';
		var lblGameLimitNote='<?php echo Yii::t('agent','agent.newsubcompany.game_limit_notes');?>';
		var urlGetSelectedRouletteTableLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSelectedGameTableLimit/GetSelectedRouletteTableLimit';
		var urlTableRouletteLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableRouletteLimit';
		var lblRoulette='<?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?>';
		var urlGetSelectedDragonTableLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSelectedGameTableLimit/GetSelectedDragonTableLimit';
		var urlTableDragonTigerLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableDragonTigerLimit';
		var lblDragonTiger='<?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?>';
		var urlGetSelectedBlackjackTableLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSelectedGameTableLimit/GetSelectedBlackjackTableLimit';
	    var urlTableBlackjackLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableBlackjackLimit';
		var lblMain='<?php echo Yii::t('agent','agent.newsubcompany.main');?>';
		var lblRummy='<?php echo Yii::t('agent','agent.newsubcompany.rummy');?>';
		var lblInsurance='<?php echo Yii::t('agent','agent.newsubcompany.insurance');?>';
		var lblBlackjack='<?php echo Yii::t('agent','agent.newsubcompany.blackjack');?>';
		var urlGetSelectedAmericanRouletteTableLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSelectedGameTableLimit/GetSelectedAmericanRouletteTableLimit';
		var urlTableAmericanRouletteLimit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=GameSetting/TableLimitList/TableAmericanRouletteLimit';
		var lblAmericanRoulette='<?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?>';
		var lblWinMaxError1='<?php echo Yii::t('agent','agent.newsubcompany.win_max_error1');?>';
		var lblWinMaxError2='<?php echo Yii::t('agent','agent.newsubcompany.win_max_error2');?>';
		var lblPlayerCanWin='<?php echo Yii::t('agent','agent.newsubcompany.player_can_win');?>';
		var lblPlayerCanWinUnlimited='<?php echo Yii::t('agent','agent.newsubcompany.player_can_win_unlimited');?>';
		var lblDaily='<?php echo Yii::t('agent','agent.newsubcompany.daily');?>';
		var strPercentValueUserLevel='<?php if(isset($_GET['type'])){echo $_GET['type'];}else{if(Yii::app()->session['account_type']=='user'){echo 'SC';}else if(Yii::app()->session['level']=='SC'){echo 'SMA';}else if(Yii::app()->session['level']=='SMA'){echo 'MA';}else if(Yii::app()->session['level']=='MA'){echo 'AGT';}else if(Yii::app()->session['level']=='AGT'){echo 'MEM';}}?>';
		var lblPasswordRequired='<?php echo Yii::t('agent','agent.newsubcompany.password_required');?>';
		var lblInvalidPasswordLength='<?php echo Yii::t('agent','agent.newsubcompany.invalid_password_length');?>';
		var lblPleaseConfirmPassword='<?php echo Yii::t('agent','agent.newsubcompany.please_confirm_password');?>';
		var lblTwoPassword='<?php echo Yii::t('agent','agent.newsubcompany.two_password');?>';
		var lblAccountNameRequired='<?php echo Yii::t('agent','agent.newsubcompany.account_name_required');?>';
		var lblInvalidAccountName='<?php echo Yii::t('agent','agent.newsubcompany.invalid_account_name');?>';
		var lblInvalidPhoneNumber='<?php echo Yii::t('agent','agent.newsubcompany.invalid_phone_number');?>';
		var lblInvalidCredit='<?php echo Yii::t('agent','agent.newsubcompany.invalid_credit');?>';
		var lblInvalidSecurityDeposit='<?php echo Yii::t('agent','agent.newsubcompany.invalid_security_deposit');?>';
		var lblCreditMustNotExceedToLimit='<?php echo Yii::t('agent','agent.newsubcompany.credit_must_not_exceed_to_limit');?>';
		var lblInsufficientCredit='<?php echo Yii::t('agent','agent.newsubcompany.insufficient_credit');?>';
		var s_member='<?php if(isset($_GET['type'])){if($_GET['type']!='MEM'){echo "0";}else{ echo '1';}}else if(Yii::app()->session['level']!='AGT'){echo '0';}else{ echo '1';}?>';
		var lblInvalidWinMax='<?php echo Yii::t('agent','agent.newsubcompany.invalid_win_max');?>';
		var lblWinMaxIsRequired='<?php echo Yii::t('agent','agent.newsubcompany.win_max_is_required');?>';
		var lblBaccaratSharingErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.baccarat_sharing_error_message');?>';
		var lblBaccaratCommissionErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.baccarat_commission_error_message');?>';
		var lblEuropeanSharingErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.european_sharing_error_message');?>';
		var lblEuropeanCommissionErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.european_commission_error_message');?>';
		var lblDragonSharingErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.dragon_sharing_error_message');?>';
		var lblDragonCommissionErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.dragon_commission_error_message');?>';
		var lblBlackjackSharingErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.blackjack_sharing_error_message');?>';
		var lblBlackjackCommissionErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.blackjack_commission_error_message');?>';
		var lblAmericanSharingErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.american_sharing_error_message');?>';
		var lblAmericanCommissionErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.american_commission_error_message');?>';
		var lblSlotsSharingErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.slots_sharing_error_message');?>';
		var lblSlotsCommissionErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.slots_commission_error_message');?>';
		var s_session_active='<?php if(isset(Yii::app()->session['level'])){echo '1';}else{echo '0';}?>';
		var lblSelectGameErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.select_game_error_message');?>';
		var lblSelectBaccaratError='<?php echo Yii::t('agent','agent.newsubcompany.select_baccarat_error');?>';
		var lblSelectEuropeanError='<?php echo Yii::t('agent','agent.newsubcompany.select_european_error');?>';
		var lblSelectDragonError='<?php echo Yii::t('agent','agent.newsubcompany.select_dragon_error');?>';
		var lblPlayerBaccaratLimitErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.player_baccarat_limit_error_msg');?>';
		var lblPlayerRoulleteLimitErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.player_roullete_limit_error_msg');?>';
		var lblPlayerDragonLimitErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.player_dragon_limit_error_msg');?>';
		var lblPlayerBlackjackLimitErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.player_blackjack_limit_error_msg');?>';
		var lblPlayerAmericanLimitErrorMessage='<?php echo Yii::t('agent','agent.newsubcompany.player_american_limit_error_msg');?>';
		var urlSaveNewSubCompany='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/SaveAgentNewSubCompany';
		var urlLoadDialogNewSubCompany='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/LoadDialogNewSubCompany';
		var urlLoadDialogSeniorMaster='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/LoadDialogSeniorMaster';
		var urlLoadDialogMaster='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/LoadDialogMaster';
		var urlLoadDialogAgent='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/LoadDialogAgent';
		var urlLoadDialogMember='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/LoadDialogMember';
		var strIsUser="<?php echo Yii::app()->session['account_type'];?>";
		var lblPlayerIsOnTheLobby = '<?php echo Yii::t('agent','agent.newsubcompany.player_on_the_lobby');?>';
		var lblPlayerIsTransfetingBalance = 'Still transfering balance from the lobby.';
		var lblInvalidSharingCommission = '<?php echo Yii::t('agent','agent.newsubcompany.invalid_sharing_commission');?>';
		var lblInvalidDataMethod = '<?php echo Yii::t('agent','agent.newsubcompany.invalid_data_method');?>';
		var lblCreditMustBeNumeric = '<?php echo Yii::t('agent','agent.newsubcompany.credit_must_be_numeric');?>';
		var lblCreditMustNotBeLesserThanOne = '<?php echo Yii::t('agent','agent.newsubcompany.credit_must_be_greater_than_one');?>';
		var lblInvalidCurrencyId = '<?php echo Yii::t('agent','agent.newsubcompany.invalid_currency_id');?>';
		var lblConflictAccountWithCashPlayer = '<?php echo Yii::t('agent','agent.newsubcompany.conflict_account_with_cashplayer');?>';
		var lblUpdateComplete = '<?php echo Yii::t('agent','agent.newsubcompany.update_complete');?>';
		var lblUCSystemCantSetDailyMaxWin = '<?php echo Yii::t('agent','agent.newsubcompany.system_cant_set_daily_max_win');?>';
		var lblInvalidDailyMaxWin = '<?php echo Yii::t('agent','agent.newsubcompany.invalid_daily_max_win');?>';
		var lblInvalidMaxWinMultiple = '<?php echo Yii::t('agent','agent.newsubcompany.invalid_max_win_multiple');?>';
		var lblCreditExceeded = '<?php echo Yii::t('agent','agent.newsubcompany.credit_exceeded');?>';
		var lblInvalidAccountId = '<?php echo Yii::t('agent','agent.newsubcompany.invalid_account_id');?>';
		var lblAccountNotExist = '<?php echo Yii::t('agent','agent.newsubcompany.account_does_not_exist');?>';
		var lblNegativeCreditNotAllowed = '<?php echo Yii::t('agent','agent.newsubcompany.negative_credit_not_allowed');?>';

	</script>
	<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript: onTheLoad();">
<input type="text" id="txtAgentType" value="<?php if(isset($_GET['type'])){echo $_GET['type'];}?>" style="display: none;"/>
<div id="new_sub_comp_body">
	<div id="comp_header">
		<?php
		$title='';
		$buttonTitle='';
		$link='';
		$use_account_id='';
		if(isset($_GET['type'])){
			if ($_GET['type']=='SMA'){$use_account_id=substr($_GET['account_id'],0,2);}
			if ($_GET['type']=='MA'){$use_account_id=substr($_GET['account_id'],0,4);}
			if ($_GET['type']=='AGT'){$use_account_id=substr($_GET['account_id'],0,6);}
		}
		if(isset($_GET['e'])){
			$eType=$_GET['e'];
		}else{
			$eType=0;
		}
		if ($eType==1){
			//if(Yii::app()->session['level']=='1' || Yii::app()->session['level']=='2' || Yii::app()->session['level']=='3' || Yii::app()->session['level']=='4' || Yii::app()->session['level']=='5' || Yii::app()->session['level']=='7' || Yii::app()->session['level']=='8'){
			if(Yii::app()->session['account_type']=='user'){
				if (isset($_GET['type'])){
					if($_GET['type']=='SC'){
						$title=Yii::t('agent','agent.newsubcompany.sub_company_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.sub_company_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSubCompanyList";
					}else if($_GET['type']=='SMA'){
						$title=Yii::t('agent','agent.newsubcompany.senior_master_agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.senior_master_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSeniorMaster&type=SC&account_id=" . $use_account_id;
					}else if($_GET['type']=='MA'){
						$title=Yii::t('agent','agent.newsubcompany.master_agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . $use_account_id;
					}else if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.player_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.sub_company_setting');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.sub_company_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSubCompanyList";
				}
			}else if(Yii::app()->session['level']=='SC'){
				if (isset($_GET['type'])){
					if($_GET['type']=='SMA'){
						$title=Yii::t('agent','agent.newsubcompany.senior_master_agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.senior_master_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSeniorMaster&type=SC&account_id=" . $use_account_id;
					}else if($_GET['type']=='MA'){
						$title=Yii::t('agent','agent.newsubcompany.master_agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . $use_account_id;
					}else if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.player_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.senior_master_agent_setting');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.senior_master_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSeniorMaster&type=SC&account_id=" . Yii::app()->session['account_id'];
				}
			}else if(Yii::app()->session['level']=='SMA'){
				if(isset($_GET['type'])){
					if($_GET['type']=='MA'){
						$title=Yii::t('agent','agent.newsubcompany.master_agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . $use_account_id;
					}else if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.player_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.master_agent_setting');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . Yii::app()->session['account_id'];
				}
			}else if(Yii::app()->session['level']=='MA'){
				if(isset($_GET['type'])){
					if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.agent_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.player_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.agent_setting');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . Yii::app()->session['account_id'];
				}
			}else if(Yii::app()->session['level']=='AGT'){
				$title='Members';
				if(isset($_GET['type'])){
					if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.player_setting');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.player_setting');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . Yii::app()->session['account_id'];
				}
			}
			
		}else{
			
			//if(Yii::app()->session['level']=='1' || Yii::app()->session['level']=='2' || Yii::app()->session['level']=='3' || Yii::app()->session['level']=='4' || Yii::app()->session['level']=='5' || Yii::app()->session['level']=='7' || Yii::app()->session['level']=='8'){
			if(Yii::app()->session['account_type']=='user'){
				if (isset($_GET['type'])){
					if($_GET['type']=='SC'){
						$title=Yii::t('agent','agent.newsubcompany.title');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.sub_company_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSubCompanyList";
					}else if($_GET['type']=='SMA'){
						$title=Yii::t('agent','agent.newsubcompany.new_senior_master_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.senior_master_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSeniorMaster&type=SC&account_id=" . $use_account_id;
					}else if($_GET['type']=='MA'){
						$title=Yii::t('agent','agent.newsubcompany.new_master_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . $use_account_id;
					}else if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.new_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.new_player');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.title');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.sub_company_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSubCompanyList";
				}
			}else if(Yii::app()->session['level']=='SC'){
				if (isset($_GET['type'])){
					if($_GET['type']=='SMA'){
						$title=Yii::t('agent','agent.newsubcompany.new_senior_master_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.senior_master_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSeniorMaster&type=SC&account_id=" . $use_account_id;
					}else if($_GET['type']=='MA'){
						$title=Yii::t('agent','agent.newsubcompany.new_master_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . $use_account_id;
					}else if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.new_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.new_player');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.new_senior_master_agent');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.senior_master_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentSeniorMaster&type=SC&account_id=" . Yii::app()->session['account_id'];
				}
			}else if(Yii::app()->session['level']=='SMA'){
				if(isset($_GET['type'])){
					if($_GET['type']=='MA'){
						$title=Yii::t('agent','agent.newsubcompany.new_master_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . $use_account_id;
					}else if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.new_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.new_player');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.new_master_agent');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.master_agent_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMaster&type=SMA&account_id=" . Yii::app()->session['account_id'];
				}
			}else if(Yii::app()->session['level']=='MA'){
				if(isset($_GET['type'])){
					if($_GET['type']=='AGT'){
						$title=Yii::t('agent','agent.newsubcompany.new_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . $use_account_id;
					}else if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.new_player');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title="New Agent";
					$buttonTitle="Agent List";
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/Agent&type=MA&account_id=" . Yii::app()->session['account_id'];
				}
			}else if(Yii::app()->session['level']=='AGT'){
				$title='New Player';
				if(isset($_GET['type'])){
					if($_GET['type']=='MEM'){
						$title=Yii::t('agent','agent.newsubcompany.new_agent');
						$buttonTitle=Yii::t('agent','agent.newsubcompany.agent_list');
						$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . substr($_GET['account_id'],0,8);
					}
				}
				else{
					$title=Yii::t('agent','agent.newsubcompany.new_player');
					$buttonTitle=Yii::t('agent','agent.newsubcompany.player_list');
					$link=Yii::app()->request->baseUrl . "/index.php?r=Agent/AgentMember&type=AGT&account_id=" . Yii::app()->session['account_id'];
				}
			}
		}
		
		echo $title;
		?>
		<input type="text" id="txtMessage" readonly="true">
		<script>
		var headerTitle='<?php echo $title; ?>';
		$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><li><a href='#'>"+headerTitle+"</a></li>");
		</script>
	</div>
	<div id="comp_body">
		<div id="table_header"><?php echo Yii::t('agent','agent.newsubcompany.basic_information');?> <input class="btn red" type="button" value="<?php echo $buttonTitle;?>" onclick="javascript: window.location='<?php echo $link;?>'"> </div>
		<table id="tblBasicInformation" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.account_id');?></td>
				<td class="right">
				
				<input style="display: none" type="text" id="txtPreAccountID" value="<?php if(isset($_GET['account_id'])){echo trim($_GET['account_id']);}else{if(Yii::app()->session['account_type']=='agent'){echo Yii::app()->session['account_id'];}}?>" readonly="true"/>
				<label id="lblPreAccountID"><b><font color="black"><?php if(isset($_GET['account_id'])){echo trim($_GET['account_id']);}else{if(Yii::app()->session['account_type']=='agent'){echo Yii::app()->session['account_id'];}}?></font></b></label>
				
				<?php
				if(!isset($_GET['e'])){ 
					for($i=1;$i<=2;$i++){
				?>
					<Select id="cmbAccountID<?php echo $i;?>">
						<option value="0">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="A">A</option>
						<option value="B">B</option>
						<option value="C">C</option>
						<option value="D">D</option>
						<option value="E">E</option>
						<option value="F">F</option>
						<option value="G">G</option>
						<option value="H">H</option>
						<option value="I">I</option>
						<option value="J">J</option>
						<option value="K">K</option>
						<option value="L">L</option>
						<option value="M">M</option>
						<option value="N">N</option>
						<option value="O">O</option>
						<option value="P">P</option>
						<option value="Q">Q</option>
						<option value="R">R</option>
						<option value="S">S</option>
						<option value="T">T</option>
						<option value="U">U</option>
						<option value="V">V</option>
						<option value="W">W</option>
						<option value="X">X</option>
						<option value="Y">Y</option>
						<option value="Z">Z</option>
					</Select>
					<?php 
						}
					}
					?>
					<?php if (!isset($_GET['e']))
						echo '<input type="button" class="btn red" id="btnChkAvailability" value="'. Yii::t('agent','agent.newsubcompany.check_availability') .'" onclick="javascript: checkAvailability(\'' . Yii::app()->request->baseUrl . '/index.php?r=Agent/AgentNewSubCompany/CheckAccountIdAvailability\');"/>';
					?>
					
				</td>
				<?php 
				if(isset($_GET['e'])){
					if ($_GET['type']!='MEM'){
						$rd=TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					}else{
						$rd=TableAgentPlayer::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					}
				}
				?>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.account_name');?></td><td class="right"><input class="txtbox" type="text" maxlength="14" id="txtAccountName" onKeyPress="return checkIt_unallow_special_char(event,'txtAccountName')"  value="<?php if(isset($_GET['e'])){echo $rd['account_name'];}?>"> <font color="red">*</font> (1~14 <?php echo Yii::t('agent','agent.newsubcompany.characters');?>)</td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.password');?></td><td class="right"><input class="txtbox" type="Password" id="txtPassword"> <font color="red">*</font> (6~14 <?php echo Yii::t('agent','agent.newsubcompany.characters');?>) <?php if(isset($_GET['e'])){echo Yii::t('agent','agent.newsubcompany.leave_it_blank_for_no_change');}?></td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.phone_number');?></td><td class="right"><input class="txtbox" type="text" id="txtPhoneNumber" value="<?php if(isset($_GET['e'])){echo $rd['phone_number'];}?>"></td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.confirm_password');?></td><td class="right"><input class="txtbox" type="Password" id="txtConfirmPassword"> <font color="red">*</font> (6~14 <?php echo Yii::t('agent','agent.newsubcompany.characters');?>)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.please_input_mail');?></td><td class="right"><input class="txtbox" type="text" id="txtEmail" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){echo $rd['agent_email'];}else{echo $rd['email'];}}?>"></td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.new_subcompany_list.market_type');?></td>
				<?php if(isset($_GET['e']))
					  {
					  		if ($rd['market_type'] == 1)
					  		{
					  			echo "<td class='right'><input type='radio' checked='true' name='rdoMarketType' id='rdoCreditMarket' value='1'>". Yii::t('agent','agent.new_subcompany_list.credit_market') . "</td>";
					  			echo "<td class='right'><input type='radio' name='rdoMarketType' id='rdoCashMarket' value='2'>". Yii::t('agent','agent.new_subcompany_list.cash_market') . "</td>";
					  		}
					  		else if ($rd['market_type'] == 2)
					  		{
					  			echo "<td class='right'><input type='radio' name='rdoMarketType' id='rdoCreditMarket' value='1' >". Yii::t('agent','agent.new_subcompany_list.credit_market') . "</td>";
					  			echo "<td class='right'><input type='radio' checked='true' name='rdoMarketType' id='rdoCashMarket' value='2' >". Yii::t('agent','agent.new_subcompany_list.cash_market') . "</td>";
					  		}
					  		
					  }
					  else 
					  {
					  		echo "<td class='right'><input type='radio' checked='true' name='rdoMarketType' id='rdoCreditMarket' value='1' >". Yii::t('agent','agent.new_subcompany_list.credit_market') . "</td>";
					  		echo "<td class='right'><input type='radio' name='rdoMarketType' id='rdoCashMarket' value='2' >". Yii::t('agent','agent.new_subcompany_list.cash_market') . "</td>";
					  }
					  
					  ?>
			</tr>
		</table>
		<div id="table_header1"><?php echo Yii::t('agent','agent.newsubcompany.betting_information');?> </div>
		<table id="tblBettingInformation11" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<?php 
				$parent_credit='';
				$agent_balance='';
				$credit_val_limit_to=0;
				if(isset($_GET['type'])){
					if($_GET['type']!='SC'){
						if($_GET['type']!='MEM'){
							$parent_credit=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
							$credit_va= $parent_credit['credit'];
							if($parent_credit['credit'] <= $parent_credit['balance']){
								$credit_val_limit_to=$parent_credit['credit'];
							}else{
								$credit_val_limit_to=$parent_credit['balance'];
							}
						}else{
							$parent_credit=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>substr($_GET['account_id'],0,8)),));
							$credit_va= $parent_credit['credit'];
							if($parent_credit['credit'] <= $parent_credit['balance']){
								$credit_val_limit_to=$parent_credit['credit'];
							}else{
								$credit_val_limit_to=$parent_credit['balance'];
							}
							if(isset($_GET['e'])){
								$agtbal=TableAgentPlayer::model()->find(array('select'=>'balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
								$agent_balance=$agtbal['balance'];
								
								$p=TableAgentPlayer::model()->find(array('select'=>'credit,balance,agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
								$credit_va=$p['credit'];
							}else{
								$agent_balance='';
							}
						}
					}else{
						//$ac = new AgentInformation();
						//$rsAssignedCredit=$ac->getAssignedCredit(1)->readAll();
						//foreach($rsAssignedCredit as $row){
						//	$credit_va=$row['credit'];
						//}
						$mc_credit=TableMainCompany::model()->find(array('select'=>'credit,balance','condition'=>'company_id=:company_id','params'=>array(':company_id'=>1),));
						$credit_va=$mc_credit['credit'];
						if($mc_credit['credit'] <= $mc_credit['balance']){
							$credit_val_limit_to=$mc_credit['credit'];
						}else{
							$credit_val_limit_to=$mc_credit['balance'];
						}
					}
				}else{
					//if(Yii::app()->session['level']!='SC' && Yii::app()->session['level']!='1' && Yii::app()->session['level']!='2' && Yii::app()->session['level']!='3' && Yii::app()->session['level']!='4' && Yii::app()->session['level']!='5' && Yii::app()->session['level']!='7' && Yii::app()->session['level']!='8'){
					if(Yii::app()->session['level']!='SC' && Yii::app()->session['account_type']=='agent'){
						$parent_id=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
						$parent_credit=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
 						$credit_va= $parent_credit['credit'];
 						if($parent_credit['credit'] <= $parent_credit['balance']){
 							$credit_val_limit_to=$parent_credit['credit'];
 						}else{
 							$credit_val_limit_to=$parent_credit['balance']; 
 						}
					}else{
						//if(Yii::app()->session['level']=='1' || Yii::app()->session['level']=='2' || Yii::app()->session['level']=='3' || Yii::app()->session['level']=='4' || Yii::app()->session['level']=='5' || Yii::app()->session['level']=='7' || Yii::app()->session['level']=='8'){
						if(Yii::app()->session['account_type']=='user'){
							//$ac = new AgentInformation();
							//$rsAssignedCredit=$ac->getAssignedCredit(1)->readAll();
							//foreach($rsAssignedCredit as $row){
							//	$credit_va=$row['credit'];
							//}
							$mc_credit=TableMainCompany::model()->find(array('select'=>'credit,balance','condition'=>'company_id=:company_id','params'=>array(':company_id'=>1),));
							$credit_va=$mc_credit['credit'];
							if($mc_credit['credit'] <= $mc_credit['balance']){
								$credit_val_limit_to=$mc_credit['credit'];
							}else{
								$credit_val_limit_to=$mc_credit['balance'];
							}
						}else if(Yii::app()->session['level']=='SC'){
							$parent_credt=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
							$credit_va=$parent_credt['credit'];
							if($parent_credt['credit'] <= $parent_credt['balance']){
								$credit_val_limit_to=$parent_credt['credit'];
							}else{
								$credit_val_limit_to=$parent_credt['balance'];
							}
						}
					}
				}
				if(isset($_GET['e'])){
					
					if($_GET['type']!='MEM'){
						$prnt_id=TableAgent::model()->find(array('select'=>'agent_parent_id,credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
						if($_GET['type']!='SC'){
							$rd_credit=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$prnt_id['agent_parent_id']),));
						}else{
							$rd_credit=TableMainCompany::model()->find(array('select'=>'credit,balance','condition'=>'company_id=:company_id','params'=>array(':company_id'=>1),));
							$credit_va=$prnt_id['credit'];
							if($prnt_id['credit'] <= $prnt_id['balance']){
								$credit_val_limit_to=$prnt_id['credit'];
							}else{
								$credit_val_limit_to=$prnt_id['balance'];
							}
						}
					}else{
						$prnt_id=TableAgentPlayer::model()->find(array('select'=>'credit,balance,agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
						//$rd_credit=TableAgentPlayer::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
						$rd_credit=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$prnt_id['agent_account_id']),));
					}
					
					
					$fltCreditLimitFrom=0;
					if(($prnt_id['credit']-$prnt_id['balance'])>=0){
						$fltCreditLimitFrom=($prnt_id['credit']-$prnt_id['balance']);
					}else{
						$fltCreditLimitFrom=0;
					}
					$parent_credit_or_balance=0;
					if($rd_credit['credit']<=$rd_credit['balance']){
						$parent_credit_or_balance=$rd_credit['credit'];
					}else{
						$parent_credit_or_balance=$rd_credit['balance'];
					}
				}
				?>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.available_credit');?></td><td class="right"><input class="txtbox" type="text" id="txtCredit" onKeyPress="return checkIt(event,'txtCredit')" onkeyup="percentValue();" name="<?php if(isset($_GET['e'])){echo $rd['credit'];}?>" value="<?php if(isset($_GET['e'])){echo $rd['credit'];}?>" 
				title="<?php if(isset($_GET['e'])){echo $credit_va+$parent_credit_or_balance;}else{echo $credit_val_limit_to;}?>"> <font color="red">*</font> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCreditStartLimit"><?php if(isset($_GET['e'])){echo $fltCreditLimitFrom;}else{echo '0';}?></label> - <?php if(isset($_GET['e'])){echo $credit_va+$parent_credit_or_balance;}else{echo $credit_val_limit_to;}?>)
				<?php 
				$agent_type='';
				if(isset($_GET['type']))
				{
					$agent_type= $_GET['type'];
				}else{
					//if(Yii::app()->session['level']=='1' || Yii::app()->session['level']=='2' || Yii::app()->session['level']=='3' || Yii::app()->session['level']=='4' || Yii::app()->session['level']=='5' || Yii::app()->session['level']=='7' || Yii::app()->session['level']=='8')
					//{
					if(Yii::app()->session['account_type']=='user'){
						$agent_type= 'SC';
					}
					else if(Yii::app()->session['level']=='SC')
					{
						$agent_type= 'SMA';
					}
					else if(Yii::app()->session['level']=='SMA')
					{
						$agent_type= 'MA';
					}
					else if(Yii::app()->session['level']=='MA')
					{
						$agent_type= 'AGT';
					}
					else if(Yii::app()->session['level']=='AGT')
					{
						$agent_type= 'MEM';
					}
				}
				if($agent_type!='MEM'){
				?>
				<label id="lblAssignedCredit"><?php echo Yii::t('agent','agent.newsubcompany.assigned_credit');?></label>
				<input type="text" id="txtAssignedCredit" value="<?php if(isset($_GET['e'])){echo $rd['credit_assigned'];}?>" name="<?php if(isset($_GET['e'])){echo $rd['credit_assigned'];}?>" readonly/>
				
				<?php 
				}?>
				</td>
			</tr>
			<?php 
				if($agent_type!='MEM'){
			?>
			<tr>
				<td class="left">
					<?php echo Yii::t('agent','agent.newsubcompany.security_deposit');?>
				</td>
				<td class="right">
					<input type="text" id="txtCreditPercent" onKeyPress="return checkIt(event,'txtCreditPercent');" onkeyup="percentValue();"  value="<?php if(isset($_GET['e'])){if(isset($_GET['type'])){if($_GET['type']!='MEM'){echo $rd['credit_percentage'];}}}?>">&nbsp;<font color="red">*</font> 
				</td>
			</tr>
			<?php }?>
			<tr style="<?php if(isset($_GET['e'])){if(isset($_GET['type'])){if($_GET['type']!='MEM'){echo "display: none;";}}else if(Yii::app()->session['level']!='AGT'){echo 'display: none';}}else{echo 'display: none';}?>">
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.balance');?></td><td><input class="txtbox" type="text" id="txtBalance" value='<?php echo $agent_balance;?>' disabled style="margin-bottom: 3px;"/>
			</tr>
			<tr style="<?php if(isset($_GET['type'])){if($_GET['type']!='MEM'){echo "display: none;";}}else if(Yii::app()->session['level']!='AGT'){echo 'display: none';}?>">
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.win_max_balance');?> (<font color="#961111">Hatien & Savan</font>)</td><td><input class="txtbox" type="text" id="txtWinMaxMultiple" name='<?php echo $agent_balance;?>' value="<?php if(isset($_GET['type']) && isset($_GET['e'])){if($_GET['type']=='MEM'){echo $rd['max_win_multiple'];}}else{ echo '-1';}?>" onkeyup="javascript: winMax();" onKeyPress="return checkIt(event,'txtWinMaxMultiple')"/> <font color="red">*</font> <label id="lblWinMax">Player can win unlimited.</label></td>
			</tr>
			<tr style="<?php if(isset($_GET['type'])){if($_GET['type']!='MEM'){echo "display: none;";}}else if(Yii::app()->session['level']!='AGT'){echo 'display: none';}?>">
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.daily_max_win');?> (<font color="#961111">Costa</font>)</td><td><input class="txtbox" type="text" id="txtDailyMaxWin" value="<?php if(isset($_GET['type']) && isset($_GET['e'])){if($_GET['type']=='MEM'){echo $rd['max_win'];}}?>" onkeyup="javascript: winMaxDaily();" onKeyPress="return checkIt(event,'txtDailyMaxWin')"/> <font color="red">*</font> <label id="lblDailyMaxWin"></label></td>
			</tr>
			<!-- <tr style="<?php if(isset($_GET['type']) && $_GET['type']=='MEM'){echo "display: none;";}?>"> -->
			<tr style="display: none">
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.permissions');?></td><td class="right">
					<input type="checkbox" name="chkPermissions" id="chkAddPermission" value="Add" <?php if(isset($_GET['e'])){if($_GET['type']!='MEM'){if($rd['permission']==2 || $rd['permission']==4 || $rd['permission']==5 || $rd['permission']==7){echo 'checked';}}}else{echo 'checked';}?> <?php if(!isset($_GET['e'])){echo 'disabled';}?>><label for="chkAddPermission"><?php echo Yii::t('agent','agent.newsubcompany.add');?></label> 
					<input type="checkbox" name="chkPermissions" id="chkModifyPermission" value="Modify" <?php if(isset($_GET['e'])){if($_GET['type']!='MEM'){if($rd['permission']==1 || $rd['permission']==4 || $rd['permission']==5 || $rd['permission']==6){echo 'checked';}}}else{echo 'checked';}?> <?php if(!isset($_GET['e'])){echo 'disabled';}?>><label for="chkModifyPermission"><?php echo Yii::t('agent','agent.newsubcompany.modify');?></label> 
					<input type="checkbox" name="chkPermissions" id="chkViewPermission" value="View" checked disabled><label for="chkViewPermission"><?php echo Yii::t('agent','agent.newsubcompany.view');?></label>
				</td>
			</tr>
			<tr <?php if(isset($_GET['e'])){echo 'style=""';}?>>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.currency');?></td>
				<td class="right">
				<?php 
				$parent_currency_id='';
				if (isset($_GET['type'])){
					if(strlen($_GET['account_id'])<10){
						$parent_currency_id=TableAgent::model()->find(array('select'=>'currency_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					}else{
						$parent_act_id=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
						$parent_currency_id=TableAgent::model()->find(array('select'=>'currency_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$parent_act_id['agent_account_id']),));
					}
				}
				//else if(Yii::app()->session['level']!=1 || Yii::app()->session['level']!=2 || Yii::app()->session['level']!=3 || Yii::app()->session['level']!=4 || Yii::app()->session['level']!=5 || Yii::app()->session['level']!=7 || Yii::app()->session['level']!=8){
				if(Yii::app()->session['account_type']=='agent'){
					$parent_currency_id=TableAgent::model()->find(array('select'=>'currency_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
				}
				?>
					<Select name="cmbCurrency" id="cmbCurrency" onchange="javascript: onTheLoad();" <?php if ($parent_currency_id!=''){echo 'disabled';}?>>
						<?php 
						$curr_exchange_rate='';
							$dataReader = TableCurrency::model()->findAll();
							if(!isset($_GET['e'])){
								echo '<option value="0">-- select currency --</option>';
							}
							foreach ($dataReader as $row){
								if($parent_currency_id!=''){
									if ($row['id']==$parent_currency_id['currency_id']){
									$curr_exchange_rate=$row['exchange_rate'];
									echo '<option value="' . $row['id'] . '" selected>'. strtoupper($row['currency_name']) . '</option>';}
								}else{
									echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
								}
							}
						?>
					</Select>
					<font color="red">*</font> <?php echo Yii::t('agent','agent.newsubcompany.exchange_rate');?>: <label id="dvExchangeRate"></label>
				</td>
			</tr>
<!--Start: Hide Select Casino -->
			<tr style="display: none;">
<!--End: Hide Select Casino -->
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.select_casino');?></td>
				<td class="right">
				<?php 
			$s_htv999_select='';
			$s_savan999_select='';
			$s_costa999_select='';
			$s_virtua999_select='';
			$s_sportsbook999_select='';
			$s_slotsvegas999_select='';
			
			if (isset($_GET['type'])){
				if($_GET['type']!='SC'){
					if($_GET['type']!='MEM'){
						$accid='';
						//get parent id
						$agt_account_id=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
						//if($agt_account_id['agent_parent_id']==''){$accid=$_GET['account_id'];}else{$accid=$agt_account_id['agent_parent_id'];}
						if(isset($_GET['e'])){
							$accid=$agt_account_id['agent_parent_id'];
						}else{
							$accid=$_GET['account_id'];
						}
						//new structure
						$cl=TableAgent::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$accid),));
						$cls=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id','params'=>array(':level_id'=>$cl['casino_access_level']),));
						$s_htv999_select=$cls['htv'];
						$s_savan999_select=$cls['sv'];
						$s_costa999_select=$cls['vig'];
						$s_virtua999_select=$cls['vv'];
						$s_sportsbook999_select=$cls['sb'];
						$s_slotsvegas999_select=$cls['svs'];
						//$s_virtua999_select=0;
					}else{
						if(isset($_GET['e'])){
							$agtplyr_account_id=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					
							//new structure
							$cl=TableAgent::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$agtplyr_account_id['agent_account_id']),));
							$cls=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id','params'=>array(':level_id'=>$cl['casino_access_level']),));
							$s_htv999_select=$cls['htv'];
							$s_savan999_select=$cls['sv'];
							$s_costa999_select=$cls['vig'];
							$s_virtua999_select=$cls['vv'];
							$s_sportsbook999_select=$cls['sb'];
							$s_slotsvegas999_select=$cls['svs'];
							//$s_virtua999_select=0;
						}else{
							//new structure
							$cl=TableAgent::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
							$cls=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id','params'=>array(':level_id'=>$cl['casino_access_level']),));
							$s_htv999_select=$cls['htv'];
							$s_savan999_select=$cls['sv'];
							$s_costa999_select=$cls['vig'];
							$s_virtua999_select=$cls['vv'];
							$s_sportsbook999_select=$cls['sb'];
							$s_slotsvegas999_select=$cls['svs'];
							//$s_virtua999_select=0;
						}
					}
				}
			}else{
				//if(Yii::app()->session['level']!='1' && Yii::app()->session['level']!='2' && Yii::app()->session['level']!='3' && Yii::app()->session['level']!='4' && Yii::app()->session['level']!='5' && Yii::app()->session['level']!='7' && Yii::app()->session['level']!='8'){
				if(Yii::app()->session['account_type']=='agent'){				
					//new structure
					$cl=TableAgent::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$cls=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id','params'=>array(':level_id'=>$cl['casino_access_level']),));
					$s_htv999_select=$cls['htv'];
					$s_savan999_select=$cls['sv'];
					$s_costa999_select=$cls['vig'];
					$s_virtua999_select=$cls['vv'];
					$s_sportsbook999_select=$cls['sb'];
					$s_slotsvegas999_select=$cls['svs'];
					//$s_virtua999_select=0;
				}
			}
			$s_htv999_checked='';
			$s_savan999_checked='';
			$s_costa999_checked='';
			$s_virtua999_checked='';
			$s_sportsbook999_checked='';
			$s_slotsvegas_checked='';
			if (isset($_GET['e'])){
				if($_GET['type']!='MEM'){
					//new field structure
					$cl=TableAgent::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$cls=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id','params'=>array(':level_id'=>$cl['casino_access_level']),));
					$s_htv999_checked=$cls['htv'];
					$s_savan999_checked=$cls['sv'];
					$s_costa999_checked=$cls['vig'];
					$s_virtua999_checked=$cls['vv'];
					$s_sportsbook999_checked=$cls['sb'];
					$s_slotsvegas_checked=$cls['svs'];
					//$s_virtua999_checked=0;
				}else{
					//new field structure
					$cl=TableAgentPlayer::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$cls=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id','params'=>array(':level_id'=>$cl['casino_access_level']),));
					$s_htv999_checked=$cls['htv'];
					$s_savan999_checked=$cls['sv'];
					$s_costa999_checked=$cls['vig'];
					$s_virtua999_checked=$cls['vv'];
					$s_sportsbook999_checked=$cls['sb'];
					$s_slotsvegas_checked=$cls['svs'];
					//$s_virtua999_checked=0;
				}
			}
			?>
<!-- Start: Hide Select Casino (old) 
					<input type="checkbox" id="chkHtv999" <?php if($s_htv999_checked=='' || $s_htv999_checked==1){if($s_htv999_select!='' && $s_htv999_select==0){}else{echo 'checked';}}?> <?php if($s_htv999_select!='' && $s_htv999_select==0){echo 'disabled';}?>><label for="chkHtv999">HTV999</label>
					<input type="checkbox" id="chkSavanVegas999" <?php if($s_savan999_checked=='' || $s_savan999_checked==1){if($s_savan999_select!='' && $s_savan999_select==0){}else{echo 'checked';}}?> <?php if($s_savan999_select!='' && $s_savan999_select==0){echo 'disabled';}?>><label for="chkSavanVegas999">Savanvegas999</label>
					<input type="checkbox" id="chkCostaVegas999" <?php if($s_costa999_checked=='' || $s_costa999_checked==1){if($s_costa999_select!='' && $s_costa999_select==0){}else{echo 'checked';}}?> <?php if($s_costa999_select!='' && $s_costa999_select==0){echo 'disabled';}?> onclick="casinoCostaCheck(this.id);"><label for="chkCostaVegas999">Costavegas999</label>
					<input type="checkbox" id="chkVirtuaVegas999" style="display: none" <?php if($s_virtua999_checked=='' || $s_virtua999_checked==1){if($s_virtua999_select!='' && $s_virtua999_select==0){}else{}}?> <?php if($s_virtua999_select!='' && $s_virtua999_select==0){echo 'disabled';}?>><label style="display: none" for="chkVirtuaVegas999">Virtuavegas999</label>
 End: Hide Select Casino (old) -->
				</td>
			</tr>
			<tr>
			<?php 
			$s_baccarat_select='';
			$s_roullete_select='';
			$s_dragontiger_select='';
			$s_slots_select='';
			$s_americanroulette_select='';
			$s_slot_select='';
			if (isset($_GET['type'])){
				if($_GET['type']!='SC'){
					if($_GET['type']!='MEM'){
						$accid='';
						//get parent id
						$agt_account_id=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
						//if($agt_account_id['agent_parent_id']==''){$accid=$_GET['account_id'];}else{$accid=$agt_account_id['agent_parent_id'];}
						if(isset($_GET['e'])){
							$accid=$agt_account_id['agent_parent_id'];
						}else{
							$accid=$_GET['account_id'];
						}
						//new structure
						$s_baccarat_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_baccarat is not null AND limit_baccarat!=0','params'=>array(':account_id'=>$accid),));
						$s_roullete_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_roulette is not null AND limit_roulette!=0','params'=>array(':account_id'=>$accid),));
						$s_dragontiger_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_dragon_tiger is not null AND limit_dragon_tiger!=0','params'=>array(':account_id'=>$accid),));
						$s_slots_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_blackjack is not null AND limit_blackjack!=0','params'=>array(':account_id'=>$accid),));
						$s_americanroulette_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_american_roulette is not null AND limit_american_roulette!=0','params'=>array(':account_id'=>$accid),));
						$s_slot_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_slots!=0','params'=>array(':account_id'=>$accid),));
					}else{
						if(isset($_GET['e'])){
							$agtplyr_account_id=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					
							//new structure
							$s_baccarat_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_baccarat is not null AND limit_baccarat!=0','params'=>array(':account_id'=>$agtplyr_account_id['agent_account_id']),));
							$s_roullete_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_roulette is not null AND limit_roulette!=0','params'=>array(':account_id'=>$agtplyr_account_id['agent_account_id']),));
							$s_dragontiger_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_dragon_tiger is not null AND limit_dragon_tiger!=0','params'=>array(':account_id'=>$agtplyr_account_id['agent_account_id']),));
							$s_slots_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_blackjack is not null AND limit_blackjack!=0','params'=>array(':account_id'=>$agtplyr_account_id['agent_account_id']),));
							$s_americanroulette_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_american_roulette is not null AND limit_american_roulette!=0','params'=>array(':account_id'=>$agtplyr_account_id['agent_account_id']),));
							$s_slot_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_slots!=0','params'=>array(':account_id'=>$agtplyr_account_id['agent_account_id']),));
						}else{
							//new structure
							$s_baccarat_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_baccarat is not null AND limit_baccarat!=0','params'=>array(':account_id'=>$_GET['account_id']),));
							$s_roullete_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_roulette is not null AND limit_roulette!=0','params'=>array(':account_id'=>$_GET['account_id']),));
							$s_dragontiger_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_dragon_tiger is not null AND limit_dragon_tiger!=0','params'=>array(':account_id'=>$_GET['account_id']),));
							$s_slots_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_blackjack is not null AND limit_blackjack!=0','params'=>array(':account_id'=>$_GET['account_id']),));
							$s_americanroulette_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_american_roulette is not null AND limit_american_roulette!=0','params'=>array(':account_id'=>$_GET['account_id']),));
							$s_slot_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_slots!=0','params'=>array(':account_id'=>$_GET['account_id']),));
						}
					}
				}
			}else{
				//if(Yii::app()->session['level']!='1' && Yii::app()->session['level']!='2' && Yii::app()->session['level']!='3' && Yii::app()->session['level']!='4' && Yii::app()->session['level']!='5' && Yii::app()->session['level']!='7' && Yii::app()->session['level']!='8'){
				if(Yii::app()->session['account_type']=='agent'){				
					//new structure
					$s_baccarat_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_baccarat is not null  AND limit_baccarat!=0','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$s_roullete_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_roulette is not null  AND limit_roulette!=0','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$s_dragontiger_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_dragon_tiger is not null  AND limit_dragon_tiger!=0','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$s_slots_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_blackjack is not null  AND limit_blackjack!=0','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$s_americanroulette_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_american_roulette is not null  AND limit_american_roulette!=0','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$s_slot_select=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_slots!=0','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
				}
			}
			$s_baccarat_checked='';
			$s_roullete_checked='';
			$s_dragontiger_checked='';
			$s_slots_checked='';
			$s_americanroulette_checked='';
			$s_slot_checked='';
			if (isset($_GET['e'])){
				if($_GET['type']!='MEM'){
					//new field structure
					$s_baccarat_checked=TableAgent::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_baccarat is not null AND limit_baccarat!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_roullete_checked=TableAgent::model()->count(array('select'=>'limit_roulette','condition'=>'account_id=:account_id AND limit_roulette is not null AND limit_roulette!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_dragontiger_checked=TableAgent::model()->count(array('select'=>'limit_dragon_tiger','condition'=>'account_id=:account_id AND limit_dragon_tiger is not null AND limit_dragon_tiger!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_slots_checked=TableAgent::model()->count(array('select'=>'limit_blackjack','condition'=>'account_id=:account_id AND limit_blackjack is not null AND limit_blackjack!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_americanroulette_checked=TableAgent::model()->count(array('select'=>'limit_american_roulette','condition'=>'account_id=:account_id AND limit_american_roulette is not null AND limit_american_roulette!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_slot_checked=TableAgent::model()->count(array('select'=>'limit_slots','condition'=>'account_id=:account_id AND limit_slots!=0','params'=>array(':account_id'=>$_GET['account_id']),));
				}else{
					//new field structure
					$s_baccarat_checked=TableAgentPlayer::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_baccarat is not null AND limit_baccarat!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_roullete_checked=TableAgentPlayer::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_roulette is not null AND limit_roulette!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_dragontiger_checked=TableAgentPlayer::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_dragon_tiger is not null AND limit_dragon_tiger!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_slots_checked=TableAgentPlayer::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_blackjack is not null AND limit_blackjack!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_americanroulette_checked=TableAgentPlayer::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_american_roulette is not null AND limit_american_roulette!=0','params'=>array(':account_id'=>$_GET['account_id']),));
					$s_slot_checked=TableAgentPlayer::model()->count(array('select'=>'limit_baccarat','condition'=>'account_id=:account_id AND limit_slots!=0','params'=>array(':account_id'=>$_GET['account_id']),));
				}
			}
			
			?>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.select_game');?></td><td class="right">
					<input type="checkbox" <?php if($s_baccarat_checked!='' && $s_baccarat_checked==1){echo 'checked';}?> <?php if($s_baccarat_select!='' && $s_baccarat_select==0){echo 'disabled';}?> name="chkSelectGame"  id="chkBacarratGame" value="Add" onclick="javascript: hideRows(this,'tblBettingInformation',1,8,15,15,15,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked); tableBacarrat(document.getElementById('cmbCurrency').value);"><label for="chkBacarratGame"><?php echo Yii::t('agent','agent.newsubcompany.baccarat');?></label> 
					<input type="checkbox" <?php if($s_roullete_checked!='' && $s_roullete_checked==1){echo 'checked';}?> <?php if($s_roullete_select!='' && $s_roullete_select==0){echo 'disabled';}?> name="chkSelectGame" id="chkRouletteGame" value="Modify" onclick="javascript: hideRows(this,'tblBettingInformation',2,9,16,16,16,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked); tableRoulette(document.getElementById('cmbCurrency').value);"><label for="chkRouletteGame"><?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?></label> 
					<input type="checkbox" <?php if($s_dragontiger_checked!='' && $s_dragontiger_checked==1){echo 'checked';}?> <?php if($s_dragontiger_select!='' && $s_dragontiger_select==0){echo 'disabled';}?> name="chkSelectGame" id="chkDragonTigerGame" value="View" onclick="javascript: hideRows(this,'tblBettingInformation',10,10,17,17,17,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked); tableDragonTiger(document.getElementById('cmbCurrency').value);"><label for="chkDragonTigerGame"><?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?></label>
					<input type="checkbox" <?php if($s_slots_checked!='' && $s_slots_checked==1){echo 'checked';}?> <?php if($s_slots_select!='' && $s_slots_select==0){echo 'disabled';}?> name="chkSelectGame" id="chkSlotsGame" value="View" onclick="javascript: hideRows(this,'tblBettingInformation',4,4,4,4,4,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked); //tableBlackjack(document.getElementById('cmbCurrency').value);"><label for="chkSlotsGame"><?php echo Yii::t('agent','agent.newsubcompany.blackjack');?></label>
					<input type="checkbox" <?php if($s_americanroulette_checked!='' && $s_americanroulette_checked==1){echo 'checked';}?> <?php if($s_americanroulette_select!='' && $s_americanroulette_select==0){echo 'disabled';}?> name="chkSelectGame" id="chkAmericanRouletteGame" value="View" onclick="javascript: hideRows(this,'tblBettingInformation',5,5,5,5,5,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked); //tableAmericanRoulette(document.getElementById('cmbCurrency').value);"><label for="chkAmericanRouletteGame"><?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?></label>
					<input type="checkbox" <?php if($s_slot_checked!='' && $s_slot_checked==1){echo 'checked';}?> <?php if($s_slot_select!='' && $s_slot_select==0){echo 'disabled';}?> name="chkSelectGame" id="chkSlotGame" value="View" onclick="javascript: hideRows(this,'tblBettingInformation',13,13,20,27,41,document.getElementById('chkCostaVegas999').checked,document.getElementById('chkHtv999').checked,document.getElementById('chkSavanVegas999').checked,document.getElementById('chkVirtuaVegas999').checked,document.getElementById('chkSlotsVegas999').checked);"><label for="chkSlotGame"><?php echo Yii::t('agent','agent.newsubcompany.slots');?></label>
				</td>
			</tr>
			
			<!-- separated table -->
			</table>
			<table id="tblBettingInformation" border="0" cellpadding="0" cellspacing="0">
			<!-- separated table -->
			
			<?php 
			if (isset($_GET['type'])){
				if($_GET['type']=='SC'){
					$sharing_commission = GameMaxSharingAndCommission::model()->find("id = 1");
					//costa
					$bac_shr[0]=$sharing_commission['baccarat_max_sharing'];
					$rou_shr[0]=$sharing_commission['roulette_max_sharing'];
					$dra_shr[0]=$sharing_commission['dragon_tiger_max_sharing'];
					$bkj_shr[0]=$sharing_commission['blackjack_max_sharing'];
					$amr_shr[0]=$sharing_commission['american_roulette_max_sharing'];
					$slt_shr[0]=$sharing_commission['slot_max_sharing'];
					$bac_com[0]=$sharing_commission['baccarat_max_commission'];
					$rou_com[0]=$sharing_commission['roulette_max_commission'];
					$dra_com[0]=$sharing_commission['dragon_tiger_max_commission'];
					$bkj_com[0]=$sharing_commission['blackjack_max_commission'];
					$amr_com[0]=$sharing_commission['american_roulette_max_commission'];
					$slt_com[0]=$sharing_commission['slot_max_commission'];
					//hatien
					$bac_shr[1]=$sharing_commission['baccarat_max_sharing'];
					$rou_shr[1]=$sharing_commission['roulette_max_sharing'];
					$dra_shr[1]=$sharing_commission['dragon_tiger_max_sharing'];
					$bkj_shr[1]=$sharing_commission['blackjack_max_sharing'];
					$amr_shr[1]=$sharing_commission['american_roulette_max_sharing'];
					$slt_shr[1]=$sharing_commission['slot_max_sharing'];
					$bac_com[1]=$sharing_commission['baccarat_max_commission'];
					$rou_com[1]=$sharing_commission['roulette_max_commission'];
					$dra_com[1]=$sharing_commission['dragon_tiger_max_commission'];
					$bkj_com[1]=$sharing_commission['blackjack_max_commission'];
					$amr_com[1]=$sharing_commission['american_roulette_max_commission'];
					$slt_com[1]=$sharing_commission['slot_max_commission'];
					//savan
					$bac_shr[2]=$sharing_commission['baccarat_max_sharing'];
					$rou_shr[2]=$sharing_commission['roulette_max_sharing'];
					$dra_shr[2]=$sharing_commission['dragon_tiger_max_sharing'];
					$bkj_shr[2]=$sharing_commission['blackjack_max_sharing'];
					$amr_shr[2]=$sharing_commission['american_roulette_max_sharing'];
					$slt_shr[2]=$sharing_commission['slot_max_sharing'];
					$bac_com[2]=$sharing_commission['baccarat_max_commission'];
					$rou_com[2]=$sharing_commission['roulette_max_commission'];
					$dra_com[2]=$sharing_commission['dragon_tiger_max_commission'];
					$bkj_com[2]=$sharing_commission['blackjack_max_commission'];
					$amr_com[2]=$sharing_commission['american_roulette_max_commission'];
					$slt_com[2]=$sharing_commission['slot_max_commission'];
					//virtua
					$bac_shr[3]=$sharing_commission['baccarat_max_sharing'];
					$rou_shr[3]=$sharing_commission['roulette_max_sharing'];
					$dra_shr[3]=$sharing_commission['dragon_tiger_max_sharing'];
					$bkj_shr[3]=$sharing_commission['blackjack_max_sharing'];
					$amr_shr[3]=$sharing_commission['american_roulette_max_sharing'];
					$slt_shr[3]=$sharing_commission['slot_max_sharing'];
					$bac_com[3]=$sharing_commission['baccarat_max_commission'];
					$rou_com[3]=$sharing_commission['roulette_max_commission'];
					$dra_com[3]=$sharing_commission['dragon_tiger_max_commission'];
					$bkj_com[3]=$sharing_commission['blackjack_max_commission'];
					$amr_com[3]=$sharing_commission['american_roulette_max_commission'];
					$slt_com[3]=$sharing_commission['slot_max_commission'];
					
					//Slotsvegas
					$bac_shr[4]=$sharing_commission['baccarat_max_sharing'];
					$rou_shr[4]=$sharing_commission['roulette_max_sharing'];
					$dra_shr[4]=$sharing_commission['dragon_tiger_max_sharing'];
					$bkj_shr[4]=$sharing_commission['blackjack_max_sharing'];
					$amr_shr[4]=$sharing_commission['american_roulette_max_sharing'];
					$slt_shr[4]=$sharing_commission['slot_max_sharing'];
					$bac_com[4]=$sharing_commission['baccarat_max_commission'];
					$rou_com[4]=$sharing_commission['roulette_max_commission'];
					$dra_com[4]=$sharing_commission['dragon_tiger_max_commission'];
					$bkj_com[4]=$sharing_commission['blackjack_max_commission'];
					$amr_com[4]=$sharing_commission['american_roulette_max_commission'];
					$slt_com[4]=$sharing_commission['slot_max_commission'];
				}else{
 					$account_id='';
 					if($_GET['type']!='MEM'){
 						if(isset($_GET['e'])){
							$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
							$account_id=$rd['agent_parent_id'];
 						}
 					}else{
 						if(isset($_GET['e'])){
	 						$rd=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
	 						//$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
	 						$account_id=$rd['agent_account_id'];
	 					}
 						
 					}

 					if (trim($account_id)==''){
 						$account_id=$_GET['account_id'];
 					}
 					
					$sharing_commission=TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$account_id),));
					$bac_shr=split('#',$sharing_commission['share_baccarat']);
					$bac_com=split('#',$sharing_commission['commission_baccarat']);
					
					$rou_shr=split('#',$sharing_commission['share_roulette']);
					$rou_com=split('#',$sharing_commission['commission_roulette']);
					
					$dra_shr=split('#',$sharing_commission['share_dragon_tiger']);
					$dra_com=split('#',$sharing_commission['commission_dragon_tiger']);
					
					$bkj_shr=split('#',$sharing_commission['share_blackjack']);
					$bkj_com=split('#',$sharing_commission['commission_blackjack']);
					
					$amr_shr=split('#',$sharing_commission['share_american_roulette']);
					$amr_com=split('#',$sharing_commission['commission_american_roulette']);
					
					$slt_shr=split('#',$sharing_commission['share_slots']);
					$slt_com=split('#',$sharing_commission['commission_slots']);
				}
			}else{
				
				//if(Yii::app()->session['level']!='SC' && Yii::app()->session['level']!='1' && Yii::app()->session['level']!='2' && Yii::app()->session['level']!='3' && Yii::app()->session['level']!='4' && Yii::app()->session['level']!='5' && Yii::app()->session['level']!='7' && Yii::app()->session['level']!='8'){
				if(Yii::app()->session['level']!='SC' && Yii::app()->session['account_type']=='agent'){
					$rd=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					
					$sharing_commission=TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$bac_shr=split('#',$sharing_commission['share_baccarat']);
					$bac_com=split('#',$sharing_commission['commission_baccarat']);
					
					$rou_shr=split('#',$sharing_commission['share_roulette']);
					$rou_com=split('#',$sharing_commission['commission_roulette']);
					
					$dra_shr=split('#',$sharing_commission['share_dragon_tiger']);
					$dra_com=split('#',$sharing_commission['commission_dragon_tiger']);
					
					$bkj_shr=split('#',$sharing_commission['share_blackjack']);
					$bkj_com=split('#',$sharing_commission['commission_blackjack']);
					
					$amr_shr=split('#',$sharing_commission['share_american_roulette']);
					$amr_com=split('#',$sharing_commission['commission_american_roulette']);
					
					$slt_shr=split('#',$sharing_commission['share_slots']);
					$slt_com=split('#',$sharing_commission['commission_slots']);
			
				}else{
					
					//if(Yii::app()->session['level']=='1' || Yii::app()->session['level']=='2' || Yii::app()->session['level']=='3' || Yii::app()->session['level']=='4' || Yii::app()->session['level']=='5' || Yii::app()->session['level']=='7' || Yii::app()->session['level']=='8'){
					if(Yii::app()->session['account_type']=='user'){
						$sharing_commission = GameMaxSharingAndCommission::model()->find("id = 1");
						
						//costa
						$bac_shr[0]=$sharing_commission['baccarat_max_sharing'];
						$rou_shr[0]=$sharing_commission['roulette_max_sharing'];
						$dra_shr[0]=$sharing_commission['dragon_tiger_max_sharing'];
						$bkj_shr[0]=$sharing_commission['blackjack_max_sharing'];
						$amr_shr[0]=$sharing_commission['american_roulette_max_sharing'];
						$slt_shr[0]=$sharing_commission['slot_max_sharing'];
						$bac_com[0]=$sharing_commission['baccarat_max_commission'];
						$rou_com[0]=$sharing_commission['roulette_max_commission'];
						$dra_com[0]=$sharing_commission['dragon_tiger_max_commission'];
						$bkj_com[0]=$sharing_commission['blackjack_max_commission'];
						$amr_com[0]=$sharing_commission['american_roulette_max_commission'];
						$slt_com[0]=$sharing_commission['slot_max_commission'];
						//hatien
						$bac_shr[1]=$sharing_commission['baccarat_max_sharing'];
						$rou_shr[1]=$sharing_commission['roulette_max_sharing'];
						$dra_shr[1]=$sharing_commission['dragon_tiger_max_sharing'];
						$bkj_shr[1]=$sharing_commission['blackjack_max_sharing'];
						$amr_shr[1]=$sharing_commission['american_roulette_max_sharing'];
						$slt_shr[1]=$sharing_commission['slot_max_sharing'];
						$bac_com[1]=$sharing_commission['baccarat_max_commission'];
						$rou_com[1]=$sharing_commission['roulette_max_commission'];
						$dra_com[1]=$sharing_commission['dragon_tiger_max_commission'];
						$bkj_com[1]=$sharing_commission['blackjack_max_commission'];
						$amr_com[1]=$sharing_commission['american_roulette_max_commission'];
						$slt_com[1]=$sharing_commission['slot_max_commission'];
						//savan
						$bac_shr[2]=$sharing_commission['baccarat_max_sharing'];
						$rou_shr[2]=$sharing_commission['roulette_max_sharing'];
						$dra_shr[2]=$sharing_commission['dragon_tiger_max_sharing'];
						$bkj_shr[2]=$sharing_commission['blackjack_max_sharing'];
						$amr_shr[2]=$sharing_commission['american_roulette_max_sharing'];
						$slt_shr[2]=$sharing_commission['slot_max_sharing'];
						$bac_com[2]=$sharing_commission['baccarat_max_commission'];
						$rou_com[2]=$sharing_commission['roulette_max_commission'];
						$dra_com[2]=$sharing_commission['dragon_tiger_max_commission'];
						$bkj_com[2]=$sharing_commission['blackjack_max_commission'];
						$amr_com[2]=$sharing_commission['american_roulette_max_commission'];
						$slt_com[2]=$sharing_commission['slot_max_commission'];
						//virtua
						$bac_shr[3]=$sharing_commission['baccarat_max_sharing'];
						$rou_shr[3]=$sharing_commission['roulette_max_sharing'];
						$dra_shr[3]=$sharing_commission['dragon_tiger_max_sharing'];
						$bkj_shr[3]=$sharing_commission['blackjack_max_sharing'];
						$amr_shr[3]=$sharing_commission['american_roulette_max_sharing'];
						$slt_shr[3]=$sharing_commission['slot_max_sharing'];
						$bac_com[3]=$sharing_commission['baccarat_max_commission'];
						$rou_com[3]=$sharing_commission['roulette_max_commission'];
						$dra_com[3]=$sharing_commission['dragon_tiger_max_commission'];
						$bkj_com[3]=$sharing_commission['blackjack_max_commission'];
						$amr_com[3]=$sharing_commission['american_roulette_max_commission'];
						$slt_com[3]=$sharing_commission['slot_max_commission'];
						//Slotvegas
						$bac_shr[4]=$sharing_commission['baccarat_max_sharing'];
						$rou_shr[4]=$sharing_commission['roulette_max_sharing'];
						$dra_shr[4]=$sharing_commission['dragon_tiger_max_sharing'];
						$bkj_shr[4]=$sharing_commission['blackjack_max_sharing'];
						$amr_shr[4]=$sharing_commission['american_roulette_max_sharing'];
						$slt_shr[4]=$sharing_commission['slot_max_sharing'];
						$bac_com[4]=$sharing_commission['baccarat_max_commission'];
						$rou_com[4]=$sharing_commission['roulette_max_commission'];
						$dra_com[4]=$sharing_commission['dragon_tiger_max_commission'];
						$bkj_com[4]=$sharing_commission['blackjack_max_commission'];
						$amr_com[4]=$sharing_commission['american_roulette_max_commission'];
						$slt_com[4]=$sharing_commission['slot_max_commission'];
					}else if(Yii::app()->session['level']=='SC' && !isset($_GET['e'])){
						$sharing_commission=TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
						$bac_shr=split('#',$sharing_commission['share_baccarat']);
						$bac_com=split('#',$sharing_commission['commission_baccarat']);
							
						$rou_shr=split('#',$sharing_commission['share_roulette']);
						$rou_com=split('#',$sharing_commission['commission_roulette']);
							
						$dra_shr=split('#',$sharing_commission['share_dragon_tiger']);
						$dra_com=split('#',$sharing_commission['commission_dragon_tiger']);
							
						$bkj_shr=split('#',$sharing_commission['share_blackjack']);
						$bkj_com=split('#',$sharing_commission['commission_blackjack']);
						
						$amr_shr=split('#',$sharing_commission['share_american_roulette']);
						$amr_com=split('#',$sharing_commission['commission_american_roulette']);
						
						$slt_shr=split('#',$sharing_commission['share_slots']);
						$slt_com=split('#',$sharing_commission['commission_slots']);
					}
				}
			}
			
			?>
			<?php 
			$mod = new AgentNewSubCompany();
			$share_baccarat[0]=0; $share_baccarat[1]=0; $share_baccarat[2]=0; $share_baccarat[3]=0; $share_baccarat[4]=0;
			$commission_baccarat[0]=0; $commission_baccarat[1]=0; $commission_baccarat[2]=0; $commission_baccarat[3]=0; $commission_baccarat[4]=0;
			$share_roulette[0]=0; $share_roulette[1]=0; $share_roulette[2]=0; $share_roulette[3]=0; $share_roulette[4]=0;
			$commission_roulette[0]=0; $commission_roulette[1]=0; $commission_roulette[2]=0; $commission_roulette[3]=0; $commission_roulette[4]=0;
			$share_dragon_tiger[0]=0; $share_dragon_tiger[1]=0; $share_dragon_tiger[2]=0; $share_dragon_tiger[3]=0;  $share_dragon_tiger[4]=0;
			$commission_dragon_tiger[0]=0; $commission_dragon_tiger[1]=0; $commission_dragon_tiger[2]=0; $commission_dragon_tiger[3]=0; $commission_dragon_tiger[4]=0;
			$share_blackjack[0]=0; $share_blackjack[1]=0; $share_blackjack[2]=0; $share_blackjack[3]=0; $share_blackjack[4]=0;
			$commission_blackjack[0]=0; $commission_blackjack[1]=0; $commission_blackjack[2]=0; $commission_blackjack[3]=0; $commission_blackjack[4]=0;
			$share_american_roulette[0]=0; $share_american_roulette[1]=0; $share_american_roulette[2]=0; $share_american_roulette[3]=0; $share_american_roulette[4]=0;
			$commission_american_roulette[0]=0; $commission_american_roulette[1]=0; $commission_american_roulette[2]=0; $commission_american_roulette[3]=0; $commission_american_roulette[4]=0;
			$share_slots[0]=0; $share_slots[1]=0; $share_slots[2]=0; $share_slots[3]=0;  $share_slots[4]=0;
			$commission_slots[0]=0; $commission_slots[1]=0; $commission_slots[2]=0; $commission_slots[3]=0; $commission_slots[4]=0;
			if(isset($_GET['e'])){
				if ($_GET['type']!='MEM'){
					$baccarat_val=TableAgent::model()->find(array('select'=>'share_baccarat,commission_baccarat','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$roullete_val=TableAgent::model()->find(array('select'=>'share_roulette,commission_roulette','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$dragontiger_val=TableAgent::model()->find(array('select'=>'share_dragon_tiger,commission_dragon_tiger','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$bkj_val=TableAgent::model()->find(array('select'=>'share_blackjack,commission_blackjack','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$americanroulette_val=TableAgent::model()->find(array('select'=>'share_american_roulette,commission_american_roulette','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$slt_val=TableAgent::model()->find(array('select'=>'share_slots,commission_slots','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					//baccarat limit from
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_baccarat,1,locate("#",share_baccarat)-1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'substring(share_baccarat,1,locate("#",share_baccarat)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_baccarat[0]= ($cs['share_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 1, 1)) ? $cs['share_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 1, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat)+1))-1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_baccarat[1]= ($cs['share_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 1, 2)) ? $cs['share_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 1, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))-1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_baccarat[2]= ($cs['share_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 1, 3)) ? $cs['share_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 1, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))+1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_baccarat[3]= ($cs['share_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 1, 4)) ? $cs['share_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 1, 4);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_baccarat[0]= ($cs['commission_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 2, 1)) ? $cs['commission_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 2, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1)  as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_baccarat[1]= ($cs['commission_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 2, 2)) ? $cs['commission_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 2, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1)  as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_baccarat[2]= ($cs['commission_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 2, 3)) ? $cs['commission_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 2, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_baccarat[3]= ($cs['commission_baccarat'] > $mod->getRevenueValue($_GET['account_id'], 1, 2, 4)) ? $cs['commission_baccarat'] : $mod->getRevenueValue($_GET['account_id'], 1, 2, 4);
					
					//roulette limit from
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_roulette,1,locate("#",share_roulette)-1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_roulette,1,locate("#",share_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_roulette[0]= ($cs['share_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 1, 1)) ? $cs['share_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 1, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette)+1))-1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_roulette[1]= ($cs['share_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 1, 2)) ? $cs['share_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 1, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))-1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_roulette[2]= ($cs['share_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 1, 3)) ? $cs['share_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 1, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))+1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_roulette[3]= ($cs['share_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 1, 4)) ? $cs['share_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 1, 4);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_roulette[0]= ($cs['commission_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 2, 1)) ? $cs['commission_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 2, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_roulette[1]= ($cs['commission_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 2, 2)) ? $cs['commission_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 2, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_roulette[2]= ($cs['commission_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 2, 3)) ? $cs['commission_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 2, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_roulette[3]= ($cs['commission_roulette'] > $mod->getRevenueValue($_GET['account_id'], 3, 2, 4)) ? $cs['commission_roulette'] : $mod->getRevenueValue($_GET['account_id'], 3, 2, 4);
					//dragon tiger limit from
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_dragon_tiger,1,locate("#",share_dragon_tiger)-1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_dragon_tiger,1,locate("#",share_dragon_tiger)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_dragon_tiger[0]= ($cs['share_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 1, 1)) ? $cs['share_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 1, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1))-1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_dragon_tiger[1]= ($cs['share_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 1, 2)) ? $cs['share_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 1, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))-1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_dragon_tiger[2]= ($cs['share_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 1, 3)) ? $cs['share_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 1, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))+1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_dragon_tiger[3]= ($cs['share_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 1, 4)) ? $cs['share_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 1, 4);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[0]= ($cs['commission_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 2, 1)) ? $cs['commission_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 2, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[1]= ($cs['commission_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 2, 2)) ? $cs['commission_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 2, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[2]= ($cs['commission_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 2, 3)) ? $cs['commission_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 2, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))+1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[3]= ($cs['commission_dragon_tiger'] > $mod->getRevenueValue($_GET['account_id'], 2, 2, 4)) ? $cs['commission_dragon_tiger'] : $mod->getRevenueValue($_GET['account_id'], 2, 2, 4);
					
					//blackjack limit from
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_blackjack,1,locate("#",share_blackjack)-1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_blackjack,1,locate("#",share_blackjack)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_blackjack[0]= ($cs['share_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 1, 1)) ? $cs['share_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 1, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack)+1))-1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_blackjack[1]= ($cs['share_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 1, 2)) ? $cs['share_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 1, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))-1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_blackjack[2]= ($cs['share_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 1, 3)) ? $cs['share_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 1, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))+1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_blackjack[3]= ($cs['share_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 1, 4)) ? $cs['share_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 1, 4);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_blackjack[0]= ($cs['commission_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 2, 1)) ? $cs['commission_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 2, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_blackjack[1]= ($cs['commission_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 2, 2)) ? $cs['commission_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 2, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_blackjack[2]= ($cs['commission_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 2, 3)) ? $cs['commission_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 2, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_blackjack[3]= ($cs['commission_blackjack'] > $mod->getRevenueValue($_GET['account_id'], 5, 2, 4)) ? $cs['commission_blackjack'] : $mod->getRevenueValue($_GET['account_id'], 5, 2, 4);
					
					//american roulette limit from
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_american_roulette,1,locate("#",share_american_roulette)-1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_american_roulette,1,locate("#",share_american_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_american_roulette[0]= ($cs['share_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 1, 1)) ? $cs['share_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 1, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette)+1),1,locate("#",substring(share_american_roulette,locate("#",share_american_roulette)+1))-1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette)+1),1,locate("#",substring(share_american_roulette,locate("#",share_american_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_american_roulette[1]= ($cs['share_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 1, 2)) ? $cs['share_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 1, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1),1,locate("#",substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1))-1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1,length(share_american_roulette)) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_american_roulette[2]= ($cs['share_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 1, 3)) ? $cs['share_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 1, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1),locate("#",substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1))+1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1),locate("#",substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_american_roulette[3]= ($cs['share_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 1, 4)) ? $cs['share_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 1, 4);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_american_roulette[0]= ($cs['commission_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 2, 1)) ? $cs['commission_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 2, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_american_roulette[1]= ($cs['commission_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 2, 2)) ? $cs['commission_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 2, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_american_roulette[2]= ($cs['commission_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 2, 3)) ? $cs['commission_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 2, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_american_roulette[3]= ($cs['commission_american_roulette'] > $mod->getRevenueValue($_GET['account_id'], 4, 2, 4)) ? $cs['commission_american_roulette'] : $mod->getRevenueValue($_GET['account_id'], 4, 2, 4);
					
					//slots limit from
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_slots,1,locate("#",share_slots)-1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_slots,1,locate("#",share_slots)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_slots[0]= ($cs['share_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 1, 1)) ? $cs['share_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 1, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_slots,locate("#",share_slots)+1),1,locate("#",substring(share_slots,locate("#",share_slots)+1))-1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_slots,locate("#",share_slots)+1),1,locate("#",substring(share_slots,locate("#",share_slots)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_slots[1]= ($cs['share_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 1, 2)) ? $cs['share_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 1, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),1,locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))-1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),1,locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_slots[2]= ($cs['share_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 1, 3)) ? $cs['share_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 1, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))+1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$share_slots[3]= ($cs['share_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 1, 4)) ? $cs['share_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 1, 4);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_slots[0]= ($cs['commission_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 2, 1)) ? $cs['commission_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 2, 1);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_slots[1]= ($cs['commission_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 2, 2)) ? $cs['commission_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 2, 2);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_slots[2]= ($cs['commission_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 2, 3)) ? $cs['commission_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 2, 3);
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_slots[3]= ($cs['commission_slots'] > $mod->getRevenueValue($_GET['account_id'], 6, 2, 4)) ? $cs['commission_slots'] : $mod->getRevenueValue($_GET['account_id'], 6, 2, 4);
				}else{
					$baccarat_val=TableAgentPlayer::model()->find(array('select'=>'commission_baccarat','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$roullete_val=TableAgentPlayer::model()->find(array('select'=>'commission_roulette','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$dragontiger_val=TableAgentPlayer::model()->find(array('select'=>'commission_dragon_tiger','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$bkj_val=TableAgentPlayer::model()->find(array('select'=>'commission_blackjack','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$americanroulette_val=TableAgentPlayer::model()->find(array('select'=>'commission_american_roulette','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));
					$slt_val=TableAgentPlayer::model()->find(array('select'=>'commission_slots','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_GET['account_id']),));	
				}
				if($_GET['type']=='AGT'){
					//baccarat limit from
					//$cs=TableAgentPlayer::model()->find(array('select'=>'commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'commission_baccarat desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					//$commission_baccarat=split('#',$cs['commission_baccarat']);
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) as commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_baccarat[0]=$cs['commission_baccarat'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1)  as commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_baccarat[1]=$cs['commission_baccarat'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1)  as commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_baccarat[2]=$cs['commission_baccarat'];
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_baccarat[3]=$cs['commission_baccarat'];
					//roulette limit from
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) as commission_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_roulette[0]=$cs['commission_roulette'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) as commission_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_roulette[1]=$cs['commission_roulette'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) as commission_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_roulette[2]=$cs['commission_roulette'];
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_roulette[3]=$cs['commission_roulette'];
					//dragon tiger limit from
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) as commission_dragon_tiger','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[0]=$cs['commission_dragon_tiger'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) as commission_dragon_tiger','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[1]=$cs['commission_dragon_tiger'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) as commission_dragon_tiger','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[2]=$cs['commission_dragon_tiger'];
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))+1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_dragon_tiger[3]=$cs['commission_dragon_tiger'];
					//blackjack limit from
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) as commission_blackjack','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_blackjack[0]=$cs['commission_blackjack'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) as commission_blackjack','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_blackjack[1]=$cs['commission_blackjack'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) as commission_blackjack','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_blackjack[2]=$cs['commission_blackjack'];
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_blackjack[3]=$cs['commission_blackjack'];
					//american roulette limit from
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) as commission_american_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_american_roulette[0]=$cs['commission_american_roulette'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) as commission_american_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_american_roulette[1]=$cs['commission_american_roulette'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) as commission_american_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_american_roulette[2]=$cs['commission_american_roulette'];
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_american_roulette[3]=$cs['commission_american_roulette'];
					//slots limit from
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) as commission_slots','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_slots[0]=$cs['commission_slots'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) as commission_slots','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_slots[1]=$cs['commission_slots'];
					$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) as commission_slots','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>$_GET['account_id']),));
					$commission_slots[2]=$cs['commission_slots'];
					$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>$_GET['account_id']),));
					$commission_slots[3]=$cs['commission_slots'];
				}
				
			}else{
				$baccarat_val=TableAgent::model()->find(array('select'=>'share_baccarat,commission_baccarat','condition'=>'account_id=:account_id','params'=>array(':account_id'=>''),));
				$roullete_val=TableAgent::model()->find(array('select'=>'share_roulette,commission_roulette','condition'=>'account_id=:account_id','params'=>array(':account_id'=>''),));
				$dragontiger_val=TableAgent::model()->find(array('select'=>'share_dragon_tiger,commission_dragon_tiger','condition'=>'account_id=:account_id','params'=>array(':account_id'=>''),));
				$bkj_val=TableAgent::model()->find(array('select'=>'share_blackjack,commission_blackjack','condition'=>'account_id=:account_id','params'=>array(':account_id'=>''),));
				$americanroulette_val=TableAgent::model()->find(array('select'=>'share_american_roulette,commission_american_roulette','condition'=>'account_id=:account_id','params'=>array(':account_id'=>''),));
				$slt_val=TableAgent::model()->find(array('select'=>'share_slots,commission_slots','condition'=>'account_id=:account_id','params'=>array(':account_id'=>''),));
			}
			if(isset($baccarat_val['share_baccarat'])){
				$baccarat_sharing_val=split('#',$baccarat_val['share_baccarat']);
			}
			$baccarat_commission_val=split('#',$baccarat_val['commission_baccarat']);
			if(isset($roullete_val['share_roulette'])){
				$roullete_sharing_val=split('#',$roullete_val['share_roulette']);
			}
			$roullete_commission_val=split('#',$roullete_val['commission_roulette']);
			if(isset($dragontiger_val['share_dragon_tiger'])){
				$dragontiger_sharing_val=split('#',$dragontiger_val['share_dragon_tiger']);
			}
			$dragontiger_commission_val=split('#',$dragontiger_val['commission_dragon_tiger']);
			if(isset($bkj_val['share_blackjack'])){
				$bkj_sharing_val=split('#',$bkj_val['share_blackjack']);
			}
			$bkj_commission_val=split('#',$bkj_val['commission_blackjack']);
			if(isset($americanroulette_val['share_american_roulette'])){
				$americanroulette_sharing_val=split('#',$americanroulette_val['share_american_roulette']);
			}
			$americanroulette_commission_val=split('#',$americanroulette_val['commission_american_roulette']);
			if(isset($slt_val['share_slots'])){
				$slt_sharing_val=split('#',$slt_val['share_slots']);
			}
			$slt_commission_val=split('#',$slt_val['commission_slots']);
			?>
<!-- Start: Additional rows for commission and sharing each casinos -->
		<!-- Start: COSTAVEGAS999 -->
			<tr>
				<td class="casinolabel"><input type="checkbox" id="chkCostaVegas999" <?php if($s_costa999_checked=='' || $s_costa999_checked==1){if($s_costa999_select!='' && $s_costa999_select==0){}else{echo 'checked';}}?> <?php if($s_costa999_select!='' && $s_costa999_select==0){echo 'disabled';}?> onclick="hideRowsCosta(this.id);"><label for="chkCostaVegas999">COSTAVEGAS999</label></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><b><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></b></td>
				<td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input type="text" id="txtCostaSharingForAll" onKeyPress="return checkIt(event,'txtCostaSharingForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplyCostaSharing" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: costaApplySharing(document.getElementById('txtCostaSharingForAll').value);"></td>
				<td class="left"><b><?php echo Yii::t('agent','agent.newsubcompany.commission');?></b></td>
				<td class="right"><input type="text" id="txtCostaCommissionForAll" onKeyPress="return checkIt(event,'txtCostaCommissionForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplyCostaCommission" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: costaApplyCommission(document.getElementById('txtCostaCommissionForAll').value);"></td>
			</tr>
			
			<tr style="display: none;"><!-- Baccarat -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.baccarat');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox"  onKeyPress="return checkIt(event,'txtBacarratSharing')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($baccarat_sharing_val[0]!=''){ echo number_format((float)$baccarat_sharing_val[0],2);}else{echo '0';}}}?>" title="<?php if(isset($bac_shr[0])){echo $bac_shr[0];}else{echo '0.00';}?>" id="txtBacarratSharing"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($baccarat_sharing_val[0]!=''){ echo number_format((float)$baccarat_sharing_val[0],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBaccaratFrom"><?php if($share_baccarat[0]!=''){echo number_format((float)$share_baccarat[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($bac_shr[0])){echo number_format((float)$bac_shr[0],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtBacarratCommision')" type="text" value="<?php if(isset($_GET['e'])){if($baccarat_commission_val[0]!=''){echo number_format((float)$baccarat_commission_val[0],2);}else{echo '0.00';}}?>" title="<?php if(isset($bac_com[0])){echo $bac_com[0];}else{echo '0.00';}?>" id="txtBacarratCommision"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if($baccarat_commission_val[0]!=''){echo number_format((float)$baccarat_commission_val[0],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBaccaratFrom"><?php if($commission_baccarat[0]!=''){echo number_format((float)$commission_baccarat[0],2);}else{ echo '0.00';}?></label>% - <?php if(isset($bac_com[0])){echo number_format((float)$bac_com[0],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteSharing')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($roullete_sharing_val[0]!=''){echo number_format((float)$roullete_sharing_val[0],2);}else{echo '0.00';}}}?>" title="<?php if(isset($rou_shr[0])){echo $rou_shr[0];}else{echo '0.00';}?>" id="txtRouletteSharing"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($roullete_sharing_val[0]!=''){echo number_format((float)$roullete_sharing_val[0],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareRouletteFrom"><?php if($share_roulette[0]!=''){echo number_format((float)$share_roulette[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_shr[0])){echo number_format((float)$rou_shr[0],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteCommision')" type="text" value="<?php if(isset($_GET['e'])){if($roullete_commission_val[0]!=''){echo number_format((float)$roullete_commission_val[0],2);}else{echo '0.00';}}?>" title="<?php if(isset($rou_com[0])){echo $rou_com[0];}else{echo '0.00';}?>" id="txtRouletteCommision"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if($roullete_commission_val[0]!=''){echo number_format((float)$roullete_commission_val[0],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionRouletteFrom"><?php if($commission_roulette[0]!=''){echo number_format((float)$commission_roulette[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_com[0])){echo number_format((float)$rou_com[0],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Dragon Tiger -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerSharing')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($dragontiger_sharing_val[0]!=''){echo number_format((float)$dragontiger_sharing_val[0],2);}else{echo '0.00';}}}?>" title="<?php if(isset($dra_shr[0])){echo $dra_shr[0];}else{echo '0.00';}?>" id="txtDragonTigerSharing"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($dragontiger_sharing_val[0]!=''){echo number_format((float)$dragontiger_sharing_val[0],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareDragonTigerFrom"><?php if($share_dragon_tiger[0]!=''){echo number_format((float)$share_dragon_tiger[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_shr[0])){echo number_format((float)$dra_shr[0],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerCommision')" type="text" value="<?php if(isset($_GET['e'])){if($dragontiger_commission_val[0]!=''){echo number_format((float)$dragontiger_commission_val[0],2);}else{echo '0.00';}}?>" title="<?php if(isset($dra_com[0])){echo $dra_com[0];}else{echo '0.00';}?>" id="txtDragonTigerCommision"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if($dragontiger_commission_val[0]!=''){echo number_format((float)$dragontiger_commission_val[0],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionDragonTigerFrom"><?php if($commission_dragon_tiger[0]!=''){echo number_format((float)$commission_dragon_tiger[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_com[0])){echo number_format((float)$dra_com[0],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Blackjack -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.blackjack');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsSharing')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($bkj_sharing_val[0]!=''){echo number_format((float)$bkj_sharing_val[0],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bkj_shr[0])){echo $bkj_shr[0];}else{echo '0.00';}?>" id="txtSlotsSharing"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($bkj_sharing_val[0]!=''){echo number_format((float)$bkj_sharing_val[0],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBlackjackFrom"><?php if($share_blackjack[0]!=''){echo number_format((float)$share_blackjack[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_shr[0])){echo number_format((float)$bkj_shr[0],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsCommision')" type="text" value="<?php if(isset($_GET['e'])){if($bkj_commission_val[0]!=''){echo number_format((float)$bkj_commission_val[0],2);}else{echo '0.00';}}?>" title="<?php if(isset($bkj_com[0])){echo $bkj_com[0];}else{echo '0.00';}?>" id="txtSlotsCommision"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if($bkj_commission_val[0]!=''){echo number_format((float)$bkj_commission_val[0],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBlackjackFrom"><?php if($commission_blackjack[0]!=''){echo number_format((float)$commission_blackjack[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_com[0])){echo number_format((float)$bkj_com[0],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- American Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteSharing')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($americanroulette_sharing_val[0]!=''){echo number_format((float)$americanroulette_sharing_val[0],2);}else{echo '0.00';}}}?>" title="<?php if(isset($amr_shr[0])){echo $amr_shr[0];}else{echo '0.00';}?>" id="txtAmericanRouletteSharing"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($americanroulette_sharing_val[0]!=''){echo number_format((float)$americanroulette_sharing_val[0],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareAmericanRouletteFrom"><?php if($share_american_roulette[0]!=''){echo number_format((float)$share_american_roulette[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_shr[0])){echo number_format((float)$amr_shr[0],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteCommision')" type="text" value="<?php if(isset($_GET['e'])){if($americanroulette_commission_val[0]!=''){echo number_format((float)$americanroulette_commission_val[0],2);}else{echo '0.00';}}?>" title="<?php if(isset($amr_com[0])){echo $amr_com[0];}else{echo '0.00';}?>" id="txtAmericanRouletteCommision"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if($americanroulette_commission_val[0]!=''){echo number_format((float)$americanroulette_commission_val[0],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionAmericanRouletteFrom"><?php if($commission_american_roulette[0]!=''){echo number_format((float)$commission_american_roulette[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_com[0])){echo number_format((float)$amr_com[0],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Slots -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.slots');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotSharing')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($slt_sharing_val[0]!=''){echo number_format((float)$slt_sharing_val[0],2);}else{echo '0.00';}}}?>" title="<?php if(isset($slt_shr[0])){echo $slt_shr[0];}else{echo '0.00';}?>" id="txtSlotSharing"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if($slt_sharing_val[0]!=''){echo number_format((float)$slt_sharing_val[0],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareSlotsFrom"><?php if($share_slots[0]!=''){echo number_format((float)$share_slots[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_shr[0])){echo number_format((float)$slt_shr[0],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotCommision')" type="text" value="<?php if(isset($_GET['e'])){if($slt_commission_val[0]!=''){echo number_format((float)$slt_commission_val[0],2);}else{echo '0.00';}}?>" title="<?php if(isset($slt_com[0])){echo $slt_com[0];}else{echo '0.00';}?>" id="txtSlotCommision"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if($slt_commission_val[0]!=''){echo number_format((float)$slt_commission_val[0],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionSlotsFrom"><?php if($commission_slots[0]!=''){echo number_format((float)$commission_slots[0],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_com[0])){echo number_format((float)$slt_com[0],2);}else{echo '0.00';}?>%)</td>
			</tr>

		<!-- Start: HATIENVEGAS999 -->

			<tr style="display: none;">
				<td class="casinolabel" style="padding-top: 10px;"><input type="checkbox" id="chkHtv999" onclick="hideRowsHatien(this.id);" <?php if($s_htv999_checked=='' || $s_htv999_checked==1){if($s_htv999_select!='' && $s_htv999_select==0){}else{echo '';}}?> <?php if($s_htv999_select!='' && $s_htv999_select==0){echo 'disabled';}?>><label for="chkHtv999">HATIENVEGAS999</label></td>
				<td class="left" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>" ><b><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></b></td>
				<td class="right" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input type="text" id="txtHatienSharingForAll" onKeyPress="return checkIt(event,'txtHatienSharingForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplyHatienSharing" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: hatienApplySharing(document.getElementById('txtHatienSharingForAll').value);"></td>
				<td class="left" style="padding-top: 10px;"><b><?php echo Yii::t('agent','agent.newsubcompany.commission');?></b></td>
				<td class="right" style="padding-top: 10px;"><input type="text" id="txtHatienCommissionForAll" onKeyPress="return checkIt(event,'txtHatienCommissionForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplyHatienCommission" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: hatienApplyCommission(document.getElementById('txtHatienCommissionForAll').value);"></td>
			</tr>
			
			<tr style="display: none;"><!-- Baccarat -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.baccarat');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox"  onKeyPress="return checkIt(event,'txtBacarratSharing_htv')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[1]) && $baccarat_sharing_val[1]!=''){ echo number_format((float)$baccarat_sharing_val[1],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bac_shr[1])){echo $bac_shr[1];}else{echo '0.00';}?>" id="txtBacarratSharing_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[1]) && $baccarat_sharing_val[1]!=''){ echo number_format((float)$baccarat_sharing_val[1],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBaccaratFrom_htv"><?php if(isset($share_baccarat[1]) && $share_baccarat[1]!=''){echo number_format((float)$share_baccarat[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($bac_shr[1])){echo number_format((float)$bac_shr[1],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtBacarratCommision_htv')" type="text" value="<?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[1]) && $baccarat_commission_val[1]!=''){echo number_format((float)$baccarat_commission_val[1],2);}else{echo '0.00';}}?>" title="<?php if(isset($bac_com[1])){echo $bac_com[1];}else{echo '0.00';}?>" id="txtBacarratCommision_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[1]) && $baccarat_commission_val[1]!=''){echo number_format((float)$baccarat_commission_val[1],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBaccaratFrom_htv"><?php if(isset($commission_baccarat[1]) && $commission_baccarat[1]!=''){echo number_format((float)$commission_baccarat[1],2);}else{ echo '0.00';}?></label>% - <?php if(isset($bac_com[1])){echo number_format((float)$bac_com[1],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteSharing_htv')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[1]) && $roullete_sharing_val[1]!=''){echo number_format((float)$roullete_sharing_val[1],2);}else{echo '0.00';}}}?>" title="<?php if(isset($rou_shr[1])){echo $rou_shr[1];}else{echo '0.00';}?>" id="txtRouletteSharing_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[1]) && $roullete_sharing_val[1]!=''){echo number_format((float)$roullete_sharing_val[1],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareRouletteFrom_htv"><?php if(isset($share_roulette[1]) && $share_roulette[1]!=''){echo number_format((float)$share_roulette[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_shr[1])){echo number_format((float)$rou_shr[1],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteCommision_htv')" type="text" value="<?php if(isset($_GET['e'])){if(isset($roullete_commission_val[1]) && $roullete_commission_val[1]!=''){echo number_format((float)$roullete_commission_val[1],2);}else{echo '0.00';}}?>" title="<?php if(isset($rou_com[1])){echo $rou_com[1];}else{echo '0.00';}?>" id="txtRouletteCommision_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($roullete_commission_val[1]) && $roullete_commission_val[1]!=''){echo number_format((float)$roullete_commission_val[1],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionRouletteFrom_htv"><?php if(isset($commission_roulette[1]) && $commission_roulette[1]!=''){echo number_format((float)$commission_roulette[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_com[1])){echo number_format((float)$rou_com[1],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Dragon Tiger -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerSharing_htv')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[1]) && $dragontiger_sharing_val[1]!=''){echo number_format((float)$dragontiger_sharing_val[1],2);}else{echo '0.00';}}}?>" title="<?php if(isset($dra_shr[1])){echo $dra_shr[1];}else{echo '0.00';}?>" id="txtDragonTigerSharing_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[1]) && $dragontiger_sharing_val[1]!=''){echo number_format((float)$dragontiger_sharing_val[1],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareDragonTigerFrom_htv"><?php if(isset($share_dragon_tiger[1]) && $share_dragon_tiger[1]!=''){echo number_format((float)$share_dragon_tiger[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_shr[1])){echo number_format((float)$dra_shr[1],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerCommision_htv')" type="text" value="<?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[1]) && $dragontiger_commission_val[1]!=''){echo number_format((float)$dragontiger_commission_val[1],2);}else{echo '0.00';}}?>" title="<?php if(isset($dra_com[1])){echo $dra_com[1];}else{echo '0.00';}?>" id="txtDragonTigerCommision_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[1]) && $dragontiger_commission_val[1]!=''){echo number_format((float)$dragontiger_commission_val[1],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionDragonTigerFrom_htv"><?php if(isset($commission_dragon_tiger[1]) && $commission_dragon_tiger[1]!=''){echo number_format((float)$commission_dragon_tiger[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_com[1])){echo number_format((float)$dra_com[1],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Blackjack -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.blackjack');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsSharing_htv')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[1]) && $bkj_sharing_val[1]!=''){echo number_format((float)$bkj_sharing_val[1],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bkj_shr[1])){echo $bkj_shr[1];}else{echo '0.00';}?>" id="txtSlotsSharing_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[1]) && $bkj_sharing_val[1]!=''){echo number_format((float)$bkj_sharing_val[1],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBlackjackFrom_htv"><?php if(isset($share_blackjack[1]) && $share_blackjack[1]!=''){echo number_format((float)$share_blackjack[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_shr[1])){echo number_format((float)$bkj_shr[1],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsCommision_htv')" type="text" value="<?php if(isset($_GET['e'])){if(isset($bkj_commission_val[1]) && $bkj_commission_val[1]!=''){echo number_format((float)$bkj_commission_val[1],2);}else{echo '0.00';}}?>" title="<?php if(isset($bkj_com[1])){echo $bkj_com[1];}else{echo '0.00';}?>" id="txtSlotsCommision_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($bkj_commission_val[1]) && $bkj_commission_val[1]!=''){echo number_format((float)$bkj_commission_val[1],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBlackjackFrom_htv"><?php if(isset($commission_blackjack[1]) && $commission_blackjack[1]!=''){echo number_format((float)$commission_blackjack[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_com[1])){echo number_format((float)$bkj_com[1],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- American Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteSharing_htv')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[1]) && $americanroulette_sharing_val[1]!=''){echo number_format((float)$americanroulette_sharing_val[1],2);}else{echo '0.00';}}}?>" title="<?php if(isset($amr_shr[1])){echo $amr_shr[1];}else{echo '0.00';}?>" id="txtAmericanRouletteSharing_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[1]) && $americanroulette_sharing_val[1]!=''){echo number_format((float)$americanroulette_sharing_val[1],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareAmericanRouletteFrom_htv"><?php if(isset($share_american_roulette[1]) && $share_american_roulette[1]!=''){echo number_format((float)$share_american_roulette[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_shr[1])){echo number_format((float)$amr_shr[1],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteCommision_htv')" type="text" value="<?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[1]) && $americanroulette_commission_val[1]!=''){echo number_format((float)$americanroulette_commission_val[1],2);}else{echo '0.00';}}?>" title="<?php if(isset($amr_com[1])){echo $amr_com[1];}else{echo '0.00';}?>" id="txtAmericanRouletteCommision_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[1]) && $americanroulette_commission_val[1]!=''){echo number_format((float)$americanroulette_commission_val[1],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionAmericanRouletteFrom_htv"><?php if(isset($commission_american_roulette[1]) && $commission_american_roulette[1]!=''){echo number_format((float)$commission_american_roulette[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_com[1])){echo number_format((float)$amr_com[1],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Slots -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.slots');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotSharing_htv')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[1]) && $slt_sharing_val[1]!=''){echo number_format((float)$slt_sharing_val[1],2);}else{echo '0.00';}}}?>" title="<?php if(isset($slt_shr[1])){echo $slt_shr[1];}else{echo '0.00';}?>" id="txtSlotSharing_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[1]) && $slt_sharing_val[1]!=''){echo number_format((float)$slt_sharing_val[1],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareSlotsFrom_htv"><?php if(isset($share_slots[1]) && $share_slots[1]!=''){echo number_format((float)$share_slots[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_shr[1])){echo number_format((float)$slt_shr[1],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotCommision_htv')" type="text" value="<?php if(isset($_GET['e'])){if(isset($slt_commission_val[1]) && $slt_commission_val[1]!=''){echo number_format((float)$slt_commission_val[1],2);}else{echo '0.00';}}?>" title="<?php if(isset($slt_com[1])){echo $slt_com[1];}else{echo '0.00';}?>" id="txtSlotCommision_htv"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($slt_commission_val[1]) && $slt_commission_val[1]!=''){echo number_format((float)$slt_commission_val[1],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionSlotsFrom_htv"><?php if(isset($commission_slots[1]) && $commission_slots[1]!=''){echo number_format((float)$commission_slots[1],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_com[1])){echo number_format((float)$slt_com[1],2);}else{echo '0.00';}?>%)</td>
			</tr>
		<!-- End: HATIENVEGAS999 -->	
		
		<!-- Start: SAVANVEGAS999 -->	
			<tr>
				<td class="casinolabel" style="padding-top: 10px;"><input type="checkbox" id="chkSavanVegas999" onclick="hideRowsSavan(this.id);" <?php if($s_savan999_checked=='' || $s_savan999_checked==1){if($s_savan999_select!='' && $s_savan999_select==0){}else{echo 'checked';}}?> <?php if($s_savan999_select!='' && $s_savan999_select==0){echo 'disabled';}?>><label for="chkSavanVegas999">SAVANVEGAS999</label></td>
				<td class="left" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><b><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></b></td>
				<td class="right" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input type="text" id="txtSavanSharingForAll" onKeyPress="return checkIt(event,'txtSavanSharingForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplySavanSharing" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: savanApplySharing(document.getElementById('txtSavanSharingForAll').value);"></td>
				<td class="left" style="padding-top: 10px;"><b><?php echo Yii::t('agent','agent.newsubcompany.commission');?></b></td>
				<td class="right" style="padding-top: 10px;"><input type="text" id="txtSavanCommissionForAll" onKeyPress="return checkIt(event,'txtSavanCommissionForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplySavanCommission" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: savanApplyCommission(document.getElementById('txtSavanCommissionForAll').value);"></td>
			</tr>
			
			<tr style="display: none;"><!-- Baccarat -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.baccarat');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox"  onKeyPress="return checkIt(event,'txtBacarratSharing_savan')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[2]) && $baccarat_sharing_val[2]!=''){ echo number_format((float)$baccarat_sharing_val[2],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bac_shr[2])){echo $bac_shr[2];}else{echo '0.00';}?>" id="txtBacarratSharing_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[2]) && $baccarat_sharing_val[2]!=''){ echo number_format((float)$baccarat_sharing_val[2],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBaccaratFrom_savan"><?php if(isset($share_baccarat[2]) && $share_baccarat[2]!=''){echo number_format((float)$share_baccarat[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($bac_shr[2])){echo number_format((float)$bac_shr[2],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtBacarratCommision_savan')" type="text" value="<?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[2]) && $baccarat_commission_val[2]!=''){echo number_format((float)$baccarat_commission_val[2],2);}else{echo '0.00';}}?>" title="<?php if(isset($bac_com[2])){echo $bac_com[2];}else{echo '0.00';}?>" id="txtBacarratCommision_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[2]) && $baccarat_commission_val[2]!=''){echo number_format((float)$baccarat_commission_val[2],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBaccaratFrom_savan"><?php if(isset($commission_baccarat[2]) && $commission_baccarat[2]!=''){echo number_format((float)$commission_baccarat[2],2);}else{ echo '0.00';}?></label>% - <?php if(isset($bac_com[2])){echo number_format((float)$bac_com[2],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteSharing_savan')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[2]) && $roullete_sharing_val[2]!=''){echo number_format((float)$roullete_sharing_val[2],2);}else{echo '0.00';}}}?>" title="<?php if(isset($rou_shr[2])){echo $rou_shr[2];}else{echo '0.00';}?>" id="txtRouletteSharing_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[2]) && $roullete_sharing_val[2]!=''){echo number_format((float)$roullete_sharing_val[2],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareRouletteFrom_savan"><?php if(isset($share_roulette[2]) && $share_roulette[2]!=''){echo number_format((float)$share_roulette[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_shr[2])){echo number_format((float)$rou_shr[2],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteCommision_savan')" type="text" value="<?php if(isset($_GET['e'])){if(isset($roullete_commission_val[2]) && $roullete_commission_val[2]!=''){echo number_format((float)$roullete_commission_val[2],2);}else{echo '0.00';}}?>" title="<?php if(isset($rou_com[2])){echo $rou_com[2];}else{echo '0.00';}?>" id="txtRouletteCommision_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($roullete_commission_val[2]) && $roullete_commission_val[2]!=''){echo number_format((float)$roullete_commission_val[2],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionRouletteFrom_savan"><?php if(isset($commission_roulette[2]) && $commission_roulette[2]!=''){echo number_format((float)$commission_roulette[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_com[2])){echo number_format((float)$rou_com[2],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Dragon Tiger -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerSharing_savan')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[2]) && $dragontiger_sharing_val[2]!=''){echo number_format((float)$dragontiger_sharing_val[2],2);}else{echo '0.00';}}}?>" title="<?php if(isset($dra_shr[2])){echo $dra_shr[2];}else{echo '0.00';}?>" id="txtDragonTigerSharing_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[2]) && $dragontiger_sharing_val[2]!=''){echo number_format((float)$dragontiger_sharing_val[2],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareDragonTigerFrom_savan"><?php if(isset($share_dragon_tiger[2]) && $share_dragon_tiger[2]!=''){echo number_format((float)$share_dragon_tiger[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_shr[2])){echo number_format((float)$dra_shr[2],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerCommision_savan')" type="text" value="<?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[2]) && $dragontiger_commission_val[2]!=''){echo number_format((float)$dragontiger_commission_val[2],2);}else{echo '0.00';}}?>" title="<?php if(isset($dra_com[2])){echo $dra_com[2];}else{echo '0.00';}?>" id="txtDragonTigerCommision_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[2]) && $dragontiger_commission_val[2]!=''){echo number_format((float)$dragontiger_commission_val[2],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionDragonTigerFrom_savan"><?php if(isset($commission_dragon_tiger[2]) && $commission_dragon_tiger[2]!=''){echo number_format((float)$commission_dragon_tiger[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_com[2])){echo number_format((float)$dra_com[2],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Blackjack -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.blackjack');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsSharing_savan')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[2]) && $bkj_sharing_val[2]!=''){echo number_format((float)$bkj_sharing_val[2],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bkj_shr[2])){echo $bkj_shr[2];}else{echo '0.00';}?>" id="txtSlotsSharing_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[2]) && $bkj_sharing_val[2]!=''){echo number_format((float)$bkj_sharing_val[2],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBlackjackFrom_savan"><?php if(isset($share_blackjack[2]) && $share_blackjack[2]!=''){echo number_format((float)$share_blackjack[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_shr[2])){echo number_format((float)$bkj_shr[2],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsCommision_savan')" type="text" value="<?php if(isset($_GET['e'])){if(isset($bkj_commission_val[2]) && $bkj_commission_val[2]!=''){echo number_format((float)$bkj_commission_val[2],2);}else{echo '0.00';}}?>" title="<?php if(isset($bkj_com[2])){echo $bkj_com[2];}else{echo '0.00';}?>" id="txtSlotsCommision_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($bkj_commission_val[2]) && $bkj_commission_val[2]!=''){echo number_format((float)$bkj_commission_val[2],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBlackjackFrom_savan"><?php if(isset($commission_blackjack[2]) && $commission_blackjack[2]!=''){echo number_format((float)$commission_blackjack[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_com[2])){echo number_format((float)$bkj_com[2],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- American Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteSharing_savan')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[2]) && $americanroulette_sharing_val[2]!=''){echo number_format((float)$americanroulette_sharing_val[2],2);}else{echo '0.00';}}}?>" title="<?php if(isset($amr_shr[2])){echo $amr_shr[2];}else{echo '0.00';}?>" id="txtAmericanRouletteSharing_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[2]) && $americanroulette_sharing_val[2]!=''){echo number_format((float)$americanroulette_sharing_val[2],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareAmericanRouletteFrom_savan"><?php if(isset($share_american_roulette[2]) && $share_american_roulette[2]!=''){echo number_format((float)$share_american_roulette[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_shr[2])){echo number_format((float)$amr_shr[2],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteCommision_savan')" type="text" value="<?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[2]) && $americanroulette_commission_val[2]!=''){echo number_format((float)$americanroulette_commission_val[2],2);}else{echo '0.00';}}?>" title="<?php if(isset($amr_com[2])){echo $amr_com[2];}else{echo '0.00';}?>" id="txtAmericanRouletteCommision_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[2]) && $americanroulette_commission_val[2]!=''){echo number_format((float)$americanroulette_commission_val[2],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionAmericanRouletteFrom_savan"><?php if(isset($commission_american_roulette[2]) && $commission_american_roulette[2]!=''){echo number_format((float)$commission_american_roulette[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_com[2])){echo number_format((float)$amr_com[2],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Slots -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.slots');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotSharing_savan')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[2]) && $slt_sharing_val[2]!=''){echo number_format((float)$slt_sharing_val[2],2);}else{echo '0.00';}}}?>" title="<?php if(isset($slt_shr[2])){echo number_format((float)$slt_shr[2],2);}else{echo '0.00';}?>" id="txtSlotSharing_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[2]) && $slt_sharing_val[2]!=''){echo number_format((float)$slt_sharing_val[2],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareSlotsFrom_savan"><?php if(isset($share_slots[2]) && $share_slots[2]!=''){echo number_format((float)$share_slots[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_shr[2])){echo number_format((float)$slt_shr[2],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotCommision_savan')" type="text" value="<?php if(isset($_GET['e'])){if(isset($slt_commission_val[2]) && $slt_commission_val[2]!=''){echo number_format((float)$slt_commission_val[2],2);}else{echo '0.00';}}?>" title="<?php if(isset($slt_com[2])){echo $slt_com[2];}else{echo '0.00';}?>" id="txtSlotCommision_savan"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($slt_commission_val[2]) && $slt_commission_val[2]!=''){echo number_format((float)$slt_commission_val[2],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionSlotsFrom_savan"><?php if(isset($commission_slots[2]) && $commission_slots[2]!=''){echo number_format((float)$commission_slots[2],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_com[2])){echo number_format((float)$slt_com[2],2);}else{echo '0.00';}?>%)</td>
			</tr>
		<!-- End: SAVANVEGAS999 -->
		
		<!-- Start: VIRTUAVEGAS999 -->	
			<tr style='display: none;'>
				<td class="casinolabel" style="padding-top: 10px;"><input type="checkbox" id="chkVirtuaVegas999" onclick="hideRowsVirtua(this.id);" <?php if($s_virtua999_checked=='' || $s_virtua999_checked==1){if($s_virtua999_select!='' && $s_virtua999_select==0){}else{echo 'checked';}}?> <?php if($s_virtua999_select!='' && $s_virtua999_select==0){echo 'disabled';}?>><label for="chkVirtuaVegas999">VIRTUAVEGAS999</label></td>
				<td class="left" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><b><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></b></td>
				<td class="right" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input type="text" id="txtVirtuaSharingForAll" onKeyPress="return checkIt(event,'txtVirtuaSharingForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplyVirtuaSharing" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: virtuaApplySharing(document.getElementById('txtVirtuaSharingForAll').value);"></td>
				<td class="left" style="padding-top: 10px;"><b><?php echo Yii::t('agent','agent.newsubcompany.commission');?></b></td>
				<td class="right" style="padding-top: 10px;"><input type="text" id="txtVirtuaCommissionForAll" onKeyPress="return checkIt(event,'txtVirtuaCommissionForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplyVirtuaCommission" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: virtuaApplyCommission(document.getElementById('txtVirtuaCommissionForAll').value);"></td>
			</tr>
			
			<tr style="display: none;"><!-- Baccarat -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.baccarat');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox"  onKeyPress="return checkIt(event,'txtBacarratSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[3]) && $baccarat_sharing_val[3]!=''){ echo number_format((float)$baccarat_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bac_shr[3])){echo $bac_shr[3];}else{echo '0.00';}?>" id="txtBacarratSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[3]) && $baccarat_sharing_val[3]!=''){ echo number_format((float)$baccarat_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBaccaratFrom_virtua"><?php if(isset($share_baccarat[3]) && $share_baccarat[3]!=''){echo number_format((float)$share_baccarat[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($bac_shr[3])){echo number_format((float)$bac_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtBacarratCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[3]) && $baccarat_commission_val[3]!=''){echo number_format((float)$baccarat_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($bac_com[3])){echo $bac_com[3];}else{echo '0.00';}?>" id="txtBacarratCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[3]) && $baccarat_commission_val[3]!=''){echo number_format((float)$baccarat_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBaccaratFrom_virtua"><?php if(isset($commission_baccarat[3]) && $commission_baccarat[3]!=''){echo number_format((float)$commission_baccarat[3],2);}else{ echo '0.00';}?></label>% - <?php if(isset($bac_com[3])){echo number_format((float)$bac_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[3]) && $roullete_sharing_val[3]!=''){echo number_format((float)$roullete_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($rou_shr[3])){echo $rou_shr[3];}else{echo '0.00';}?>" id="txtRouletteSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[3]) && $roullete_sharing_val[3]!=''){echo number_format((float)$roullete_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareRouletteFrom_virtua"><?php if(isset($share_roulette[3]) && $share_roulette[3]!=''){echo number_format((float)$share_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_shr[3])){echo number_format((float)$rou_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($roullete_commission_val[3]) && $roullete_commission_val[3]!=''){echo number_format((float)$roullete_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($rou_com[3])){echo $rou_com[3];}else{echo '0.00';}?>" id="txtRouletteCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($roullete_commission_val[3]) && $roullete_commission_val[3]!=''){echo number_format((float)$roullete_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionRouletteFrom_virtua"><?php if(isset($commission_roulette[3]) && $commission_roulette[3]!=''){echo number_format((float)$commission_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_com[3])){echo number_format((float)$rou_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Dragon Tiger -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[3]) && $dragontiger_sharing_val[3]!=''){echo number_format((float)$dragontiger_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($dra_shr[3])){echo $dra_shr[3];}else{echo '0.00';}?>" id="txtDragonTigerSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[3]) && $dragontiger_sharing_val[3]!=''){echo number_format((float)$dragontiger_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareDragonTigerFrom_virtua"><?php if(isset($share_dragon_tiger[3]) && $share_dragon_tiger[3]!=''){echo number_format((float)$share_dragon_tiger[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_shr[3])){echo number_format((float)$dra_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[3]) && $dragontiger_commission_val[3]!=''){echo number_format((float)$dragontiger_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($dra_com[3])){echo $dra_com[3];}else{echo '0.00';}?>" id="txtDragonTigerCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[3]) && $dragontiger_commission_val[3]!=''){echo number_format((float)$dragontiger_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionDragonTigerFrom_virtua"><?php if(isset($commission_dragon_tiger[3]) && $commission_dragon_tiger[3]!=''){echo number_format((float)$commission_dragon_tiger[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_com[3])){echo number_format((float)$dra_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Blackjack -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.blackjack');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[3]) && $bkj_sharing_val[3]!=''){echo number_format((float)$bkj_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bkj_shr[3])){echo $bkj_shr[3];}else{echo '0.00';}?>" id="txtSlotsSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[3]) && $bkj_sharing_val[3]!=''){echo number_format((float)$bkj_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBlackjackFrom_virtua"><?php if(isset($share_blackjack[3]) && $share_blackjack[3]!=''){echo number_format((float)$share_blackjack[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_shr[3])){echo number_format((float)$bkj_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($bkj_commission_val[3]) && $bkj_commission_val[3]!=''){echo number_format((float)$bkj_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($bkj_com[3])){echo $bkj_com[3];}else{echo '0.00';}?>" id="txtSlotsCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($bkj_commission_val[3]) && $bkj_commission_val[3]!=''){echo number_format((float)$bkj_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBlackjackFrom_virtua"><?php if(isset($commission_blackjack[3]) && $commission_blackjack[3]!=''){echo number_format((float)$commission_blackjack[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_com[3])){echo number_format((float)$bkj_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- American Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[3]) && $americanroulette_sharing_val[3]!=''){echo number_format((float)$americanroulette_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($amr_shr[3])){echo $amr_shr[3];}else{echo '0.00';}?>" id="txtAmericanRouletteSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[3]) && $americanroulette_sharing_val[3]!=''){echo number_format((float)$americanroulette_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareAmericanRouletteFrom_virtua"><?php if(isset($share_american_roulette[3]) && $share_american_roulette[3]!=''){echo number_format((float)$share_american_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_shr[3])){echo number_format((float)$amr_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[3]) && $americanroulette_commission_val[3]!=''){echo number_format((float)$americanroulette_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($amr_com[3])){echo $amr_com[3];}else{echo '0.00';}?>" id="txtAmericanRouletteCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[3]) && $americanroulette_commission_val[3]!=''){echo number_format((float)$americanroulette_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionAmericanRouletteFrom_virtua"><?php if(isset($commission_american_roulette[3]) && $commission_american_roulette[3]!=''){echo number_format((float)$commission_american_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_com[3])){echo number_format((float)$amr_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Slots -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.slots');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[3]) && $slt_sharing_val[3]!=''){echo number_format((float)$slt_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($slt_shr[3])){echo number_format((float)$slt_shr[3],2);}else{echo '0.00';}?>" id="txtSlotSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[3]) && $slt_sharing_val[3]!=''){echo number_format((float)$slt_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareSlotsFrom_virtua"><?php if(isset($share_slots[3]) && $share_slots[3]!=''){echo number_format((float)$share_slots[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_shr[3])){echo number_format((float)$slt_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($slt_commission_val[3]) && $slt_commission_val[3]!=''){echo number_format((float)$slt_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($slt_com[3])){echo $slt_com[3];}else{echo '0.00';}?>" id="txtSlotCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($slt_commission_val[3]) && $slt_commission_val[3]!=''){echo number_format((float)$slt_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionSlotsFrom_virtua"><?php if(isset($commission_slots[3]) && $commission_slots[3]!=''){echo number_format((float)$commission_slots[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_com[3])){echo number_format((float)$slt_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
		<!-- End: VIRTUAVEGAS999 -->
		
		<!-- Start: SPORTSBOOK999 -->	
			<tr style='display: none;'>
				<td class="casinolabel" style="padding-top: 10px;"><input type="checkbox" id="chkSportsBook999" onclick="hideRowsSportsBook(this.id);" <?php if($s_sportsbook999_checked=='' || $s_sportsbook999_checked==1){if($s_sportsbook999_select!='' && $s_sportsbook999_select==0){}else{echo 'checked';}}?> <?php if($s_sportsbook999_select!='' && $s_sportsbook999_select==0){echo 'disabled';}?>><label for="chkSportsBook999">SPORTSBOOK999</label></td>
				<td class="left" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><b><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></b></td>
				<td class="right" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input type="text" id="txtSportsBookSharingForAll" onKeyPress="return checkIt(event,'txtSportsBookSharingForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplySportsBookSharing" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: sportsBookApplySharing(document.getElementById('txtSportsBookSharingForAll').value);"></td>
				<td class="left" style="padding-top: 10px;"><b><?php echo Yii::t('agent','agent.newsubcompany.commission');?></b></td>
				<td class="right" style="padding-top: 10px;"><input type="text" id="txtSportsBookCommissionForAll" onKeyPress="return checkIt(event,'txtSportsBookCommissionForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplySportsBookCommission" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: sportsBookApplyCommission(document.getElementById('txtSportsBookCommissionForAll').value);"></td>
			</tr>
			
			<tr style="display: none;"><!-- Baccarat -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.baccarat');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox"  onKeyPress="return checkIt(event,'txtBacarratSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[3]) && $baccarat_sharing_val[3]!=''){ echo number_format((float)$baccarat_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bac_shr[3])){echo $bac_shr[3];}else{echo '0.00';}?>" id="txtBacarratSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[3]) && $baccarat_sharing_val[3]!=''){ echo number_format((float)$baccarat_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBaccaratFrom_virtua"><?php if(isset($share_baccarat[3]) && $share_baccarat[3]!=''){echo number_format((float)$share_baccarat[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($bac_shr[3])){echo number_format((float)$bac_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtBacarratCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[3]) && $baccarat_commission_val[3]!=''){echo number_format((float)$baccarat_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($bac_com[3])){echo $bac_com[3];}else{echo '0.00';}?>" id="txtBacarratCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[3]) && $baccarat_commission_val[3]!=''){echo number_format((float)$baccarat_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBaccaratFrom_virtua"><?php if(isset($commission_baccarat[3]) && $commission_baccarat[3]!=''){echo number_format((float)$commission_baccarat[3],2);}else{ echo '0.00';}?></label>% - <?php if(isset($bac_com[3])){echo number_format((float)$bac_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[3]) && $roullete_sharing_val[3]!=''){echo number_format((float)$roullete_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($rou_shr[3])){echo $rou_shr[3];}else{echo '0.00';}?>" id="txtRouletteSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[3]) && $roullete_sharing_val[3]!=''){echo number_format((float)$roullete_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareRouletteFrom_virtua"><?php if(isset($share_roulette[3]) && $share_roulette[3]!=''){echo number_format((float)$share_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_shr[3])){echo number_format((float)$rou_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($roullete_commission_val[3]) && $roullete_commission_val[3]!=''){echo number_format((float)$roullete_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($rou_com[3])){echo $rou_com[3];}else{echo '0.00';}?>" id="txtRouletteCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($roullete_commission_val[3]) && $roullete_commission_val[3]!=''){echo number_format((float)$roullete_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionRouletteFrom_virtua"><?php if(isset($commission_roulette[3]) && $commission_roulette[3]!=''){echo number_format((float)$commission_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_com[3])){echo number_format((float)$rou_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Dragon Tiger -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[3]) && $dragontiger_sharing_val[3]!=''){echo number_format((float)$dragontiger_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($dra_shr[3])){echo $dra_shr[3];}else{echo '0.00';}?>" id="txtDragonTigerSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[3]) && $dragontiger_sharing_val[3]!=''){echo number_format((float)$dragontiger_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareDragonTigerFrom_virtua"><?php if(isset($share_dragon_tiger[3]) && $share_dragon_tiger[3]!=''){echo number_format((float)$share_dragon_tiger[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_shr[3])){echo number_format((float)$dra_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[3]) && $dragontiger_commission_val[3]!=''){echo number_format((float)$dragontiger_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($dra_com[3])){echo $dra_com[3];}else{echo '0.00';}?>" id="txtDragonTigerCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[3]) && $dragontiger_commission_val[3]!=''){echo number_format((float)$dragontiger_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionDragonTigerFrom_virtua"><?php if(isset($commission_dragon_tiger[3]) && $commission_dragon_tiger[3]!=''){echo number_format((float)$commission_dragon_tiger[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_com[3])){echo number_format((float)$dra_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Blackjack -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.blackjack');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[3]) && $bkj_sharing_val[3]!=''){echo number_format((float)$bkj_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bkj_shr[3])){echo $bkj_shr[3];}else{echo '0.00';}?>" id="txtSlotsSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[3]) && $bkj_sharing_val[3]!=''){echo number_format((float)$bkj_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBlackjackFrom_virtua"><?php if(isset($share_blackjack[3]) && $share_blackjack[3]!=''){echo number_format((float)$share_blackjack[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_shr[3])){echo number_format((float)$bkj_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($bkj_commission_val[3]) && $bkj_commission_val[3]!=''){echo number_format((float)$bkj_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($bkj_com[3])){echo $bkj_com[3];}else{echo '0.00';}?>" id="txtSlotsCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($bkj_commission_val[3]) && $bkj_commission_val[3]!=''){echo number_format((float)$bkj_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBlackjackFrom_virtua"><?php if(isset($commission_blackjack[3]) && $commission_blackjack[3]!=''){echo number_format((float)$commission_blackjack[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_com[3])){echo number_format((float)$bkj_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- American Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[3]) && $americanroulette_sharing_val[3]!=''){echo number_format((float)$americanroulette_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($amr_shr[3])){echo $amr_shr[3];}else{echo '0.00';}?>" id="txtAmericanRouletteSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[3]) && $americanroulette_sharing_val[3]!=''){echo number_format((float)$americanroulette_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareAmericanRouletteFrom_virtua"><?php if(isset($share_american_roulette[3]) && $share_american_roulette[3]!=''){echo number_format((float)$share_american_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_shr[3])){echo number_format((float)$amr_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[3]) && $americanroulette_commission_val[3]!=''){echo number_format((float)$americanroulette_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($amr_com[3])){echo $amr_com[3];}else{echo '0.00';}?>" id="txtAmericanRouletteCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[3]) && $americanroulette_commission_val[3]!=''){echo number_format((float)$americanroulette_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionAmericanRouletteFrom_virtua"><?php if(isset($commission_american_roulette[3]) && $commission_american_roulette[3]!=''){echo number_format((float)$commission_american_roulette[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_com[3])){echo number_format((float)$amr_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Slots -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.slots');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotSharing_virtua')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[3]) && $slt_sharing_val[3]!=''){echo number_format((float)$slt_sharing_val[3],2);}else{echo '0.00';}}}?>" title="<?php if(isset($slt_shr[3])){echo number_format((float)$slt_shr[3],2);}else{echo '0.00';}?>" id="txtSlotSharing_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[3]) && $slt_sharing_val[3]!=''){echo number_format((float)$slt_sharing_val[3],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareSlotsFrom_virtua"><?php if(isset($share_slots[3]) && $share_slots[3]!=''){echo number_format((float)$share_slots[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_shr[3])){echo number_format((float)$slt_shr[3],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotCommision_virtua')" type="text" value="<?php if(isset($_GET['e'])){if(isset($slt_commission_val[3]) && $slt_commission_val[3]!=''){echo number_format((float)$slt_commission_val[3],2);}else{echo '0.00';}}?>" title="<?php if(isset($slt_com[3])){echo $slt_com[3];}else{echo '0.00';}?>" id="txtSlotCommision_virtua"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($slt_commission_val[3]) && $slt_commission_val[3]!=''){echo number_format((float)$slt_commission_val[3],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionSlotsFrom_virtua"><?php if(isset($commission_slots[3]) && $commission_slots[3]!=''){echo number_format((float)$commission_slots[3],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_com[3])){echo number_format((float)$slt_com[3],2);}else{echo '0.00';}?>%)</td>
			</tr>
		<!-- End: SPORTSBOOK999 -->
		<!-- Start: SLOTSVEGAS999 -->	
			<tr>
				<td class="casinolabel" style="padding-top: 10px;"><input type="checkbox" id="chkSlotsVegas999" onclick="hideRowsSlotsVegas(this.id);" <?php if($s_slotsvegas_checked=='' || $s_slotsvegas_checked==1){if($s_slotsvegas999_select!='' && $s_slotsvegas999_select==0){}else{echo 'checked';}}?> <?php if($s_slotsvegas999_select!='' && $s_slotsvegas999_select==0){echo 'disabled';}?>><label for="chkSlotsVegas999">SLOTSVEGAS999</label></td>
				<td class="left" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><b><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></b></td>
				<td class="right" style="padding-top: 10px; <?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input type="text" id="txtSlotsVegasSharingForAll" onKeyPress="return checkIt(event,'txtSlotsVegasSharingForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplySlotsVegasSharing" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: slotsVegasApplySharing(document.getElementById('txtSlotsVegasSharingForAll').value);"></td>
				<td class="left" style="padding-top: 10px;"><b><?php echo Yii::t('agent','agent.newsubcompany.commission');?></b></td>
				<td class="right" style="padding-top: 10px;"><input type="text" id="txtSlotsVegasCommissionForAll" onKeyPress="return checkIt(event,'txtSlotsVegasCommissionForAll');" class="txt_sharing_commission" value="0"> <b>%</b> <input type="button" id="btnApplySlotsVegasCommission" class="btn red" value="<?php echo Yii::t('agent','agent.newsubcompany.apply_all');?>" onclick="javascript: slotsVegasApplyCommission(document.getElementById('txtSlotsVegasCommissionForAll').value);"></td>
			</tr>
			
			<tr style="display: none;"><!-- Baccarat -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.baccarat');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox"  onKeyPress="return checkIt(event,'txtBacarratSharing_slots')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[4]) && $baccarat_sharing_val[4]!=''){ echo number_format((float)$baccarat_sharing_val[4],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bac_shr[4])){echo $bac_shr[4];}else{echo '0.00';}?>" id="txtBacarratSharing_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($baccarat_sharing_val[4]) && $baccarat_sharing_val[4]!=''){ echo number_format((float)$baccarat_sharing_val[4],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBaccaratFrom_slots"><?php if(isset($share_baccarat[4]) && $share_baccarat[4]!=''){echo number_format((float)$share_baccarat[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($bac_shr[4])){echo number_format((float)$bac_shr[4],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtBacarratCommision_slots')" type="text" value="<?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[4]) && $baccarat_commission_val[4]!=''){echo number_format((float)$baccarat_commission_val[4],2);}else{echo '0.00';}}?>" title="<?php if(isset($bac_com[4])){echo $bac_com[4];}else{echo '0.00';}?>" id="txtBacarratCommision_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($baccarat_commission_val[4]) && $baccarat_commission_val[4]!=''){echo number_format((float)$baccarat_commission_val[4],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBaccaratFrom_slots"><?php if(isset($commission_baccarat[4]) && $commission_baccarat[4]!=''){echo number_format((float)$commission_baccarat[4],2);}else{ echo '0.00';}?></label>% - <?php if(isset($bac_com[4])){echo number_format((float)$bac_com[4],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.european_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteSharing_slots')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[4]) && $roullete_sharing_val[4]!=''){echo number_format((float)$roullete_sharing_val[4],2);}else{echo '0.00';}}}?>" title="<?php if(isset($rou_shr[4])){echo $rou_shr[4];}else{echo '0.00';}?>" id="txtRouletteSharing_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($roullete_sharing_val[4]) && $roullete_sharing_val[4]!=''){echo number_format((float)$roullete_sharing_val[4],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareRouletteFrom_slots"><?php if(isset($share_roulette[4]) && $share_roulette[4]!=''){echo number_format((float)$share_roulette[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_shr[4])){echo number_format((float)$rou_shr[4],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtRouletteCommision_slots')" type="text" value="<?php if(isset($_GET['e'])){if(isset($roullete_commission_val[4]) && $roullete_commission_val[4]!=''){echo number_format((float)$roullete_commission_val[4],2);}else{echo '0.00';}}?>" title="<?php if(isset($rou_com[4])){echo $rou_com[4];}else{echo '0.00';}?>" id="txtRouletteCommision_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($roullete_commission_val[4]) && $roullete_commission_val[4]!=''){echo number_format((float)$roullete_commission_val[4],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionRouletteFrom_slots"><?php if(isset($commission_roulette[4]) && $commission_roulette[4]!=''){echo number_format((float)$commission_roulette[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($rou_com[4])){echo number_format((float)$rou_com[4],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Dragon Tiger -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.dragon_tiger');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerSharing_slots')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[4]) && $dragontiger_sharing_val[4]!=''){echo number_format((float)$dragontiger_sharing_val[4],2);}else{echo '0.00';}}}?>" title="<?php if(isset($dra_shr[4])){echo $dra_shr[4];}else{echo '0.00';}?>" id="txtDragonTigerSharing_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($dragontiger_sharing_val[4]) && $dragontiger_sharing_val[4]!=''){echo number_format((float)$dragontiger_sharing_val[4],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareDragonTigerFrom_slots"><?php if(isset($share_dragon_tiger[4]) && $share_dragon_tiger[4]!=''){echo number_format((float)$share_dragon_tiger[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_shr[4])){echo number_format((float)$dra_shr[4],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtDragonTigerCommision_slots')" type="text" value="<?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[4]) && $dragontiger_commission_val[4]!=''){echo number_format((float)$dragontiger_commission_val[4],2);}else{echo '0.00';}}?>" title="<?php if(isset($dra_com[4])){echo $dra_com[3];}else{echo '0.00';}?>" id="txtDragonTigerCommision_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($dragontiger_commission_val[4]) && $dragontiger_commission_val[4]!=''){echo number_format((float)$dragontiger_commission_val[4],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionDragonTigerFrom_slots"><?php if(isset($commission_dragon_tiger[4]) && $commission_dragon_tiger[4]!=''){echo number_format((float)$commission_dragon_tiger[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($dra_com[4])){echo number_format((float)$dra_com[4],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Blackjack -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.blackjack');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsSharing_slots')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[4]) && $bkj_sharing_val[4]!=''){echo number_format((float)$bkj_sharing_val[4],2);}else{echo '0.00';}}}?>" title="<?php if(isset($bkj_shr[4])){echo $bkj_shr[4];}else{echo '0.00';}?>" id="txtSlotsSharing_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($bkj_sharing_val[4]) && $bkj_sharing_val[4]!=''){echo number_format((float)$bkj_sharing_val[4],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareBlackjackFrom_virtua"><?php if(isset($share_blackjack[4]) && $share_blackjack[4]!=''){echo number_format((float)$share_blackjack[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_shr[4])){echo number_format((float)$bkj_shr[4],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotsCommision_slots')" type="text" value="<?php if(isset($_GET['e'])){if(isset($bkj_commission_val[4]) && $bkj_commission_val[4]!=''){echo number_format((float)$bkj_commission_val[4],2);}else{echo '0.00';}}?>" title="<?php if(isset($bkj_com[4])){echo $bkj_com[4];}else{echo '0.00';}?>" id="txtSlotsCommision_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($bkj_commission_val[4]) && $bkj_commission_val[4]!=''){echo number_format((float)$bkj_commission_val[4],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionBlackjackFrom_slots"><?php if(isset($commission_blackjack[4]) && $commission_blackjack[4]!=''){echo number_format((float)$commission_blackjack[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($bkj_com[4])){echo number_format((float)$bkj_com[4],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- American Roulette -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.american_roulette');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteSharing_slots')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[4]) && $americanroulette_sharing_val[4]!=''){echo number_format((float)$americanroulette_sharing_val[4],2);}else{echo '0.00';}}}?>" title="<?php if(isset($amr_shr[4])){echo $amr_shr[4];}else{echo '0.00';}?>" id="txtAmericanRouletteSharing_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($americanroulette_sharing_val[4]) && $americanroulette_sharing_val[4]!=''){echo number_format((float)$americanroulette_sharing_val[4],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareAmericanRouletteFrom_slots"><?php if(isset($share_american_roulette[4]) && $share_american_roulette[4]!=''){echo number_format((float)$share_american_roulette[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_shr[4])){echo number_format((float)$amr_shr[4],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtAmericanRouletteCommision_slots')" type="text" value="<?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[4]) && $americanroulette_commission_val[4]!=''){echo number_format((float)$americanroulette_commission_val[4],2);}else{echo '0.00';}}?>" title="<?php if(isset($amr_com[4])){echo $amr_com[4];}else{echo '0.00';}?>" id="txtAmericanRouletteCommision_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($americanroulette_commission_val[4]) && $americanroulette_commission_val[4]!=''){echo number_format((float)$americanroulette_commission_val[4],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionAmericanRouletteFrom_slots"><?php if(isset($commission_american_roulette[4]) && $commission_american_roulette[4]!=''){echo number_format((float)$commission_american_roulette[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($amr_com[4])){echo number_format((float)$amr_com[4],2);}else{echo '0.00';}?>%)</td>
			</tr>
			<tr style="display: none;"><!-- Slots -->
				<td ><b><?php echo Yii::t('agent','agent.newsubcompany.slots');?></b></td>
				<td class="left" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><?php echo Yii::t('agent','agent.newsubcompany.sharing');?></td><td class="right" style="<?php if(isset($_GET['type'])){if($_GET['type']=='MEM'){echo 'display:none;';}}else{if(Yii::app()->session['level']=='AGT'){echo 'display:none;';}}?>"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotSharing_slots')" type="text" value="<?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[4]) && $slt_sharing_val[4]!=''){echo number_format((float)$slt_sharing_val[4],2);}else{echo '0.00';}}}?>" title="<?php if(isset($slt_shr[4])){echo number_format((float)$slt_shr[4],2);}else{echo '0.00';}?>" id="txtSlotSharing_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if ($_GET['type']!='MEM'){if(isset($slt_sharing_val[4]) && $slt_sharing_val[4]!=''){echo number_format((float)$slt_sharing_val[4],2) . '%,';}else{echo '0.00%,';}}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblShareSlotsFrom_slots"><?php if(isset($share_slots[4]) && $share_slots[4]!=''){echo number_format((float)$share_slots[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_shr[4])){echo number_format((float)$slt_shr[4],2);}else{echo '0.00';}?>%)</td>
				<td class="left"><?php echo Yii::t('agent','agent.newsubcompany.commission');?></td><td class="right"><input class="txtbox" onKeyPress="return checkIt(event,'txtSlotCommision_slots')" type="text" value="<?php if(isset($_GET['e'])){if(isset($slt_commission_val[4]) && $slt_commission_val[4]!=''){echo number_format((float)$slt_commission_val[4],2);}else{echo '0.00';}}?>" title="<?php if(isset($slt_com[4])){echo $slt_com[4];}else{echo '0.00';}?>" id="txtSlotCommision_slots"> % <font color="red">*</font> <?php if(isset($_GET['e'])){if(isset($slt_commission_val[4]) && $slt_commission_val[4]!=''){echo number_format((float)$slt_commission_val[4],2) . '%,';}else{echo '0.00%,';}}?> <?php echo Yii::t('agent','agent.newsubcompany.limit');?>(<label id="lblCommissionSlotsFrom_slots"><?php if(isset($commission_slots[4]) && $commission_slots[4]!=''){echo number_format((float)$commission_slots[4],2);}else{echo '0.00';}?></label>% - <?php if(isset($slt_com[4])){echo number_format((float)$slt_com[4],2);}else{echo '0.00';}?>%)</td>
			</tr>
		<!-- End: SLOTSVEGAS999 -->
			
<!-- End: Additional rows for commission and sharing each casinos -->			
		</table>
		<div id="dv_limits_msg" style="display:none"><b>Note:</b> The below limits are applicable only for Hatienvegas999 and Savanvegas999.</div>
		<div id="qry_baccarat"></div>
		<div id="qry_roullete"></div>
		<div id="qry_dragon_tiger"></div>
		<div id="qry_blackjack"></div>
		<div id="qry_american_roulette"></div>
		<?php 
			$user_authorization='';
			if(!isset($_GET['e'])){
				if(!Yii::app()->user->checkAccess('agent.addNewSubCompany')){
					if(Yii::app()->session['account_type']!='agent'){
						$user_authorization ='disabled';
					}
				}
			}else{
				if(!Yii::app()->user->checkAccess('agent.writeSubCompanySetting')){
					if(Yii::app()->session['account_type']!='agent'){
						$user_authorization ='disabled';
					}
				}
			}
		?>
		<div id="tblfooter"><button class="btn red" <?php echo $user_authorization;?> value="" onclick="javascript: cashierConfirm();" id="btnConfirm"><?php echo Yii::t('agent','agent.newsubcompany.confirm');?> <i class="icon-ok-sign"></i></button> <button class="btn red" value="" onclick="javascript: reSet();"/><?php echo Yii::t('agent','agent.newsubcompany.reset');?> <i class="icon-refresh"></i></button> <button class="btn red" value="" onclick="javascript: window.location='<?php echo $link;?>';"><?php echo $buttonTitle;?> <i class="icon-circle-arrow-left"></i></button></div>
		<input type="text" id="txtErrMessage" readonly="true"/>
	</div>
	<?php //dialogNewSubCompany
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id'=>'dialogNewSubCompany',
			    // additional javascript options for the dialog plugin
			    'options'=>array(
			        'title'=>'New Sub Company',
			        'autoOpen'=>false,
		    		'width'=>'auto',
		    		'height'=>'auto',
			    	'modal'=> true,
			    	'resizable'=> false,
			    	'closeOnEscape'=>false,
			    	'open'=>'js: function(event, ui) {$(".ui-dialog-titlebar-close", this.parentNode).hide();}',
			    ),
			));
	    echo '<div id="vwDialogNewSubCompany"></div>';
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
	<?php //dialogSeniorMasterAgent
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id'=>'dialogSeniorMasterAgent',
			    // additional javascript options for the dialog plugin
			    'options'=>array(
			        'title'=>'Senior Master',
			        'autoOpen'=>false,
		    		'width'=>'auto',
		    		'height'=>'auto',
			    	'modal'=> true,
			    	'resizable'=> false,
			    	'closeOnEscape'=>false,
			    	'open'=>'js: function(event, ui) {$(".ui-dialog-titlebar-close", this.parentNode).hide();}',
			    ),
			));
	    echo '<div id="vwDialogSeniorMasterAgent"></div>';
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
	<?php //dialogMasterAgent
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id'=>'dialogMasterAgent',
			    // additional javascript options for the dialog plugin
			    'options'=>array(
			        'title'=>'Master Agent',
			        'autoOpen'=>false,
		    		'width'=>'auto',
		    		'height'=>'auto',
			    	'modal'=> true,
			    	'resizable'=> false,
			    	'closeOnEscape'=>false,
			    	'open'=>'js: function(event, ui) {$(".ui-dialog-titlebar-close", this.parentNode).hide();}',
			    ),
			));
	    echo '<div id="vwDialogMasterAgent"></div>';
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
	<?php //dialogAgent
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id'=>'dialogAgent',
			    // additional javascript options for the dialog plugin
			    'options'=>array(
			        'title'=>'Agent',
			        'autoOpen'=>false,
		    		'width'=>'auto',
		    		'height'=>'auto',
			    	'modal'=> true,
			    	'resizable'=> false,
			    	'closeOnEscape'=>false,
			    	'open'=>'js: function(event, ui) {$(".ui-dialog-titlebar-close", this.parentNode).hide();}',
			    ),
			));
	    echo '<div id="vwDialogAgent"></div>';
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
	<?php //dialogMember
		$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id'=>'dialogMember',
			    // additional javascript options for the dialog plugin
			    'options'=>array(
			        'title'=>'Member',
			        'autoOpen'=>false,
		    		'width'=>'auto',
		    		'height'=>'auto',
			    	'modal'=> true,
			    	'resizable'=> false,
			    	'closeOnEscape'=>false,
			    	'open'=>'js: function(event, ui) {$(".ui-dialog-titlebar-close", this.parentNode).hide();}',
			    ),
			));
	    echo '<div id="vwDialogMember"></div>';
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
	<script>
	var $createForceLogoutDialog='';
	$(document).ready(function() {
		$createForceLogoutDialog = $('<div></div>')
			.html( '<div>'
			 +'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
			 +'<tr><td class="row"><img width=100px src="<?php echo Yii::app()->request->baseUrl ?>/images/process_warning.png" /></td><td class="row1"><b><font color="red"><?php echo Yii::t('agent','agent.subcompanylist.newsubcompanylogout');?></font></b></td></tr>'
			 +'<tr><td class="row"></td><td class="row1"><input id="accountDepositID" name="accountDepositID" class="depConfirmInput"  readonly="true" hidden="true"/></b></td></tr>'
			 +'<tr><td class="row"></td><td class="row1"><input style="width:120px;height:30px" id="btnForceLogout" type="button" value="Force Logout" onclick="javascript:logoutPlayer();" />&nbsp;&nbsp;<input style="width:120px;height:30px" id="btnCancelLogout" type="button" value="Cancel" onclick="javascript:$createForceLogoutDialog.dialog(\'close\');" /></td></tr>'
			 +'</table><img id="imgLogout" style="cursor:pointer; width:470px;height:20px" src="<?php echo $this->module->assetsUrl; ?>/images/logout-loader.gif" ></div>'
				)
			.dialog({
				autoOpen: false,
				closeOnEscape: false,
				width: 500,
				heigth:100,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.warning');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 $('#list2').trigger("reloadGrid");
          		}
			});
	});
	var $createWaitWithdrawProcessDialog='';
	$(document).ready(function() {
		$createWaitWithdrawProcessDialog = $('<div></div>')
			.html( '<div><table" >'
			 +'<font color="red">' + '<?php echo Yii::t('agent','agent.subcompanylist.transfer_casino_lobby');?>' + '</font>'
			 +'</br></br>'
			 +'</table><img id="imgLogout1" style="cursor:pointer; width:400px;height:20px" src="<?php echo $this->module->assetsUrl; ?>/images/logout-loader.gif" ></div>'
				)
			.dialog({
				autoOpen: false,
				closeOnEscape: false,
				width: 420,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.transfer_casino_lobby_title');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 //$('#list2').trigger("reloadGrid");
          		}
			});
	});
	function logoutPlayer()
	{
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferProcess',
			type: 'POST',
			data: {'task': 'logout', 'AccountID' : document.getElementById('txtPreAccountID').value},
			context: '',
			success: function(data){
				document.getElementById("btnForceLogout").disabled = true;
				document.getElementById("btnCancelLogout").disabled = true;
				$(".ui-dialog-titlebar-close").show();
				$createForceLogoutDialog.dialog('close');
				document.getElementById("btnForceLogout").disabled = false;
				document.getElementById("btnCancelLogout").disabled = false;
				
	    	}
		});
	}

	//FUNCTION FOR CHECKING IF THE PLAYER IS STILL ONLINE ON A LOBBY
	var isProcessing = false;
	
	function checkPlayerOnlineStat(){
		if(isProcessing){
		       return;
		}
		isProcessing = true;
		
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/IsStockOnTheLobby',
			type: 'POST',
			data: {'task': 'checkPlayerOnlineStat', 'AccountID' : document.getElementById('txtPreAccountID').value},
			context: '',
			async:false,
			success: function(data){
				isProcessing = false;
				var rec=data.split("#");

						if(rec[2]=='w'){
							$createWaitWithdrawProcessDialog.dialog('open');
							$(".ui-dialog-titlebar-close").hide();
							var check =setInterval(function(){
								jQuery.ajax({
									url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany/IsStockOnTheLobby',
									type: 'POST',
									data: {'AccountID' : document.getElementById('txtPreAccountID').value},
									context: '',
									async:false,
									success: function(data){
										isProcessing = false;
										if (data==0){
											$(".ui-dialog-titlebar-close").show();
											$createWaitWithdrawProcessDialog.dialog('close');
											clearInterval(check);
											$('#list2').trigger("reloadGrid"); 
											return false;
										}else if(data=='h'){
											$(".ui-dialog-titlebar-close").show();
											$createWaitWithdrawProcessDialog.dialog('close');
											alert('Cannot connect to HTV withdrawal API Server.Please try again!');
											clearInterval(check);
												return false;
										}else if(data=='s'){
											$(".ui-dialog-titlebar-close").show();
											$createWaitWithdrawProcessDialog.dialog('close');
											alert('Cannot connect to Savan withdrawal API Server.Please try again!');
											clearInterval(check);
											return false;
										}
							    	}
								});
								},2000);
								
						}
				
				
	    	}
		});
	}
	</script>
</div>

</body>
</html>
