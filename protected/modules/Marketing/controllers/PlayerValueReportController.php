<?php

class PlayerValueReportController extends MyController
{
	public function actionPlayerValueList()
	{
		$cp = new PlayerValue();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$cp->getCountPlayerValueList();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
		
		$startIndex = $limit*$page - $limit;
		if ($startIndex<0){
			$startIndex=0;
		}
		$user_records=$cp->getPlayerValueList($orderField, $sortType, $startIndex, $limit);
		
		$filedNames = array("account_id","player_value","value_redeem","available_value","player_points","points_redeem","available_points","redeem_count","operation");
		$urlAS= Yii::app()->request->baseUrl;
// 		if(Yii::app()->user->checkAccess('systemSetting.writeCurrencySetting')){
// 			$htmValue='<a class=\'update-currency\' href=\'#\' onclick=\'javascript: getCurrencyName(this); $updatePromoItemDialog.dialog(\"open\");document.getElementById(\"e_cs_errMessage\").value=\"\"\' id=\'currency\'>Edit</a>';
// 		}else{
			$htmValue='  ';
// 		}
		
		echo JsonUtil::jsonEncodeSystemSetting($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,9);
	}
	
	
	public function actionPlayerValueListDetails()
	{
		$cp = new PlayerValue();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$cp->getCountPlayerValueListDetails();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 1;
		}
		if ($page > $total_pages) $page=$total_pages;
	
		$startIndex = $limit*$page - $limit;
		$user_records=$cp->getPlayerValueListDetails($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("redeem_date","item_name","quantity","total_value","total_points");
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::jsonEncodeSystemSetting($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,8);
	}
	
	public function actionIndex() {
		if(Yii::app()->user->checkAccess('marketing.readPlayerValueReport'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionGetPromoItems(){
		$cp=new PlayerValue();
		$result=$cp->getItemName();
		$data=$result->readAll();
		
		echo json_encode($data);
	}
	
	public function actionRedeemItem(){
		
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		
		$postPromoItem=new TablePromoItem;
		$postRedeem= new TableRedeem;
		
		$post=TablePromoItem::model()->find(array(
				'select'=>'equevalent_value,equevalent_points,item_name',
				'condition'=>'id=:id',
				'params'=>array(':id'=>$_POST['item_id']),
		));
		
		$value= $post['equevalent_value'];
		$points= $post['equevalent_points'];
		$itemName=$post['item_name'];
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT (IFNULL(SUM(tppv.player_value),0) - IFNULL(tpr.total_value,0)) AS available_value,(IFNULL(SUM(tppv.points_earned),0)-IFNULL(tpr.total_points,0)) AS available_points
											   FROM tbl_promo_player_value AS tppv LEFT JOIN (SELECT account_id,SUM(total_value) AS total_value,SUM(total_points) AS total_points,COUNT(account_id) AS redeem_count FROM tbl_promo_redeem GROUP BY account_id )AS tpr ON tppv.account_id = tpr.account_id where tppv.account_id='".$_POST['account_id']."' AND tppv.date_time BETWEEN '".$_POST['dateFrom']."' AND '".$_POST['dateTo']."'  GROUP BY tppv.account_id");
		$rows = $command->queryRow();

		$available_value=$rows['available_value'];
		$available_points=$rows['available_points'];

		$total_value=$value*$_POST['qty'];
		$total_points=$points*$_POST['qty'];
		if ($_POST['redeem_type']=='v'){
			if ($available_value < $total_value){
				exit($_POST['account_id']." don\'t have enough value to redeem this item.");
			}
		}
		if ($_POST['redeem_type']=='p'){
			if ($available_points < $total_points){
				exit($_POST['account_id']." don\'t have enough points to redeem this item.");
			}
		}
		if(Yii::app()->user->checkAccess('marketing.writePlayerValueReport'))
		{
			$postRedeem->account_id=$_POST['account_id'];
			$postRedeem->item_id=$_POST['item_id'];
			$postRedeem->quantity=$_POST['qty'];
			$postRedeem->redeem_date=date('Y-m-d H:i:s');
			if ($_POST['redeem_type']=='v'){
				$postRedeem->total_value=$total_value;
				$postRedeem->total_points=0;
				//$postRedeem->available_value=$available_value-$total_value;
				//$postRedeem->available_points=$available_points;
			}else{
				$postRedeem->total_value=0;
				$postRedeem->total_points=$total_points;
				//$postRedeem->available_value=$available_value;
				//Redeem->available_points=$available_points-$total_points;
			}
			$postRedeem->save();
		
			$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> redeem promo item <label style=\"color:red\">'.$itemName.'</label> for player <label style=\"color:red\">'.$_POST['account_id'].'</label></b>';
		
				
			$logs = new MarketingLogs();
			$logs->saveLog($AccountID, $level,$_POST['account_id'], 'Cash Player', 29, $log_details);
				
			echo 'Redeem transaction complete.';
		
		}else{
			echo "You don\'t have permission to do redeem.";
		}
	}
}