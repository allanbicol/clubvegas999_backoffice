<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentwinloss.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		var readVirtuaSlotWinloss="<?php if(Yii::app()->user->checkAccess('agent.readVirtuaWinLoss') || User::getUserType() === Constants::ROLE_AGENT){echo '1';}?>";
		var readHatienSlotWinloss="<?php if(Yii::app()->user->checkAccess('agent.readHTVSlotWinLoss') || User::getUserType() === Constants::ROLE_AGENT){echo '1';}?>";
		var readSavanSlotWinloss="<?php if(Yii::app()->user->checkAccess('agent.readSavanSlotWinLoss') || User::getUserType() === Constants::ROLE_AGENT){echo '1';}?>";
		
		/********START: HTV999 WinLoss*******************************************************/
		function d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_htv999_total1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstTotal_htv1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_total1").appendChild(divTag);
	    	var grid=jQuery("#d_lstTotal_htv1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTV999Total&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    	var col_group_header='';
	        if(accountID.length==0){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}else if(accountID.length==8){
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
					  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
					  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
	    		],
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            rownumbers: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<font color='#ab0e10'><label id='lblHTVTotalWinLossID1'></label></font> HATIENVEGAS999 TOTAL <select id='currencyTypeConvertHTV' onchange='javascript: htvTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            footerrow:true,
	    	});
	    	$("#d_lstTotal_htv1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	
			if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[6].name);
			}
		}
		function d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_htv999_result1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstHTV1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_result1").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrHTV1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_result1").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstHTV1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTVSummaryWinLoss&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;

	        var col_agent='';
	        var col_group_header='';
	        if(accountID.length==0){
	        	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
		    	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}
	        var fltBetCount=0;
	        var fltValidBet=0;
	        var strCurrency='';
	        function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }

	        function htv_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
	    		    		col_agent,
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.company');?>',
	    		    		'exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: htv_xrate,summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				
				    //Lower level
				    $(".account_id_htv").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
					    	d_htvSubAgent1(account_id_val);
			    			document.getElementById('d_lblAgentID_htv1').innerHTML=account_id_val;
			    			//document.getElementById('qry_costavegas_total').innerHTML='';
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='d_lblAgentID_htv1'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='d_lblDateFrom_htv1'></label> - <label id='d_lblDateTo_htv1'></label> <input type='button' id='d_lblDateTo_htv1' value='<<' onclick='backToParent_htv(document.getElementById(\"d_lblAgentID_htv1\").innerHTML);'> <input type='button' id='btnDetails_htv' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='d_showDetails_htv1(document.getElementById(\"d_lblAgentID_htv1\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: htvWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrHTV1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstHTV1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	$('#d_lstHTV1').jqGrid('navGrid', '#d_pgrHTV1', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstHTV1").jqGrid('navButtonAdd','#d_pgrHTV1',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_htv999_total1');
	    			var table2= document.getElementById('d_qry_htv999_result1');
	    			d_exportWinLossToExcel('HTV999_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
			}
		}
		/**
		 * @todo export to excel
		 * @author leokarl
		 * @since 2013-01-24
		 */
		function d_exportWinLossToExcel1(filename, html_table){
			var game = $('#d_cmbGame_slot')[0].value;
			var currency = $('#d_cmbCurrency_slot')[0].value;
			testChecked=0;
			if (document.getElementById("d_chkTestCurrency_slot").checked==true){
				 testChecked=1;
			}
			var dateFrom	=	$('#d_dtFrom_slot')[0].value + ' ' + $('#d_cmbTime1_slot')[0].value + ':00:00';
			var dateTo	=	$('#d_dtTo_slot')[0].value + ' ' + $('#d_cmbTime2_slot')[0].value + ':59:59';
			var accountId = $('#d_txtAccountID_slot')[0].value;
			var filenameSplit=filename.split("_");
			
			if (document.getElementById('d_chkVIRTUA_slot').checked==true && document.getElementById('d_chkHTV_slot').checked!=true && document.getElementById('d_chkSAVAN_slot').checked!=true){
				document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
			}else if (document.getElementById('d_chkVIRTUA_slot').checked!=true && document.getElementById('d_chkHTV_slot').checked==true && document.getElementById('d_chkSAVAN_slot').checked!=true){
				document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
			}else if (document.getElementById('d_chkVIRTUA_slot').checked!=true && document.getElementById('d_chkHTV_slot').checked!=true && document.getElementById('d_chkSAVAN_slot').checked==true){
				document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
			}else if (document.getElementById('d_chkVIRTUA_slot').checked==true && document.getElementById('d_chkHTV_slot').checked==true && document.getElementById('d_chkSAVAN_slot').checked!=true){
				if(filenameSplit[0]=='VIRTUAVEGAS999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}else if (document.getElementById('d_chkVIRTUA_slot').checked==true && document.getElementById('d_chkHTV_slot').checked!=true && document.getElementById('d_chkSAVAN_slot').checked==true){
				if(filenameSplit[0]=='SAVANVEGAS999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}else if (document.getElementById('d_chkVIRTUA_slot').checked!=true && document.getElementById('d_chkHTV_slot').checked==true && document.getElementById('d_chkSAVAN_slot').checked==true){
				if(filenameSplit[0]=='HTV999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}else if (document.getElementById('d_chkVIRTUA_slot').checked==true && document.getElementById('d_chkHTV_slot').checked==true && document.getElementById('d_chkSAVAN_slot').checked==true){
				if(filenameSplit[0]=='VIRTUAVEGAS999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else if (filenameSplit[0]=='HTV999'){
					document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}else{
					document.forms[1].txtParams.value=$('.ui-paging-info')[2].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				}
			}
			
			document.forms[1].csvBuffer.value = ""; // clear buffer
			document.forms[1].csvBuffer.value = html_table;
		    document.forms[1].method='POST';
		    document.forms[1].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
		    document.forms[1].target='_top';
		    document.forms[1].submit();
		}

		function d_exportAllWinLossToExcel1(filename,url){
		    $.ajax({	
				 url:url, 
				 async:true,
				 success: function(result) {
						var data =result.split("<BREAK>");
				
						var game = $('#d_cmbGame_slot')[0].value;
						var currency = $('#d_cmbCurrency_slot')[0].value;
						testChecked=0;
						if (document.getElementById("d_chkTestCurrency_slot").checked==true){
							 testChecked=1;
						}
						var dateFrom	=	$('#d_dtFrom_slot')[0].value + ' ' + $('#d_cmbTime1_slot')[0].value + ':00:00';
						var dateTo	=	$('#d_dtTo_slot')[0].value + ' ' + $('#d_cmbTime2_slot')[0].value + ':59:59';
						var accountId = $('#d_txtAccountID_slot')[0].value;

						document.forms[1].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
						
						document.forms[1].csvBuffer.value = ""; // clear buffer
						document.forms[1].csvBuffer.value = data[2]+''+data[0];
					    document.forms[1].method = 'POST';
					    document.forms[1].action = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
					    document.forms[1].target = '_top';
					    document.forms[1].submit();
				 },
				 error: function(XMLHttpRequest, textStatus, errorThrown) { 
			 }});
		}

		
		function d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_htv999_result1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstHTV1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_result1").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrHTV1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_result1").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstHTV1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTVSummaryMemberWinLoss&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    	function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	    	function htv_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: htv_xrate,summaryTpl:'<label class="htv_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    $(".account_id_htv").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    //start: hide parent grids
						    document.getElementById('d_qry_htv999_result1').style.display='none';
						    document.getElementById('d_qry_htv999_result_details1').style.display='none';
						    //end: hide parent grids
					    	var dtfrom=document.getElementById('d_lblDateFrom_htv1').innerHTML;
		        			var dtto=document.getElementById('d_lblDateTo_htv1').innerHTML;
		        			tableMemberWinLoss_htv999_details(account_id_val,d_dtFrom_slot,d_dtTo_slot,document.getElementById('d_cmbGame_slot').name);
		        			document.getElementById('lblBettingDetailAgentID_htv').innerHTML=account_id_val;
		        			document.getElementById('lblDDateFrom_htv').innerHTML=dtfrom;
		        			document.getElementById('lblDDateTo_htv').innerHTML=dtto;
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
	    			$('#d_lstHTV1').jqGrid('navGrid', '#d_pgrHTV1', {edit: false, add: false, del:false, search: false});
	    	    	$("#d_lstHTV1").jqGrid('navButtonAdd','#d_pgrHTV1',{
	    	            caption:"Export to Excel", 
	    	            buttonicon:"ui-icon-calculator", 
	    	            onClickButton: function(){
	    	            	var table1= document.getElementById('d_qry_htv999_total1');
	    	    			var table2= document.getElementById('d_qry_htv999_result1');
	    	    			d_exportWinLossToExcel1('HTV999_SLOT_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
	    	           }, 
	    	            position:"last"
	    	        });   
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='d_lblAgentID_htv1'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='d_lblDateFrom_htv1'></label> - <label id='d_lblDateTo_htv1'></label> <input type='button' value='<<' id='d_lblDateTo_htv1' onclick='backToParent_htv(document.getElementById(\"d_lblAgentID_htv1\").innerHTML);'> <input type='button' id='btnDetails_htv' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='d_showDetails_htv1(document.getElementById(\"d_lblAgentID_htv1\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: htvMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrHTV1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstHTV1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
				  ]
			});
		}
		//Details
		
		function d_tableHTVWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_htv999_result_details1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstHTV2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_result_details1").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrHTV2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_result_details1").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstHTV2");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTVWinLossDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;

	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.company');?>'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_htv',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_bonus', index: 'mem_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_bonus', index: 'ma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_bonus', index: 'sma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_bonus', index: 'sc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    
				    //Lower level
				    $(".detail_account_id_htv").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    if(account_id_val.length!=10){
				    			d_showDetails_htv1(account_id_val);
				    			document.getElementById('lblDetailAgentID_htv').innerHTML=account_id_val;
						    }else{
							    //start: hide parent grids
							    document.getElementById('d_qry_htv999_result1').style.display='none';
							    document.getElementById('d_qry_htv999_result_details1').style.display='none';
							    //end: hide parent grids
						    	var dtfrom=document.getElementById('lblDetailDateFrom_htv').innerHTML;
			        			var dtto=document.getElementById('lblDetailDateTo_htv').innerHTML;
			        			d_tableMemberWinLoss_htv999_details1(account_id_val,d_dtFrom_slot,d_dtTo_slot,document.getElementById('d_cmbGame_slot').name);
			        			document.getElementById('lblBettingDetailAgentID_htv').innerHTML=account_id_val;
			        			document.getElementById('lblDDateFrom_htv').innerHTML=dtfrom;
			        			document.getElementById('lblDDateTo_htv').innerHTML=dtto;
							}
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	        			var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblDetailAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_winloss_details');?> <label id='lblDetailDateFrom_htv'></label> - <label id='lblDetailDateTo_htv'></label> <input type='button' id='btnDetailBack_htv' value='<<' onclick='backToParentDetail_htv1(document.getElementById(\"lblDetailAgentID_htv\").innerHTML);'>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrHTV2',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstHTV2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
					{startColumnName: 'ma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.master_agent');?></label>'},
					{startColumnName: 'sma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?></label>'},
					{startColumnName: 'sc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.sub_company');?></label>'},
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.main_company');?></label>'},
				  ]
			});
	    	$('#d_lstHTV2').jqGrid('navGrid', '#d_pgrHTV2', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstHTV2").jqGrid('navButtonAdd','#d_pgrHTV2',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_htv999_result_details1');
	    			d_exportWinLossToExcel1('HTV999_SLOT_WIN_LOSS_DETAILS_', table1.outerHTML);
	           }, 
	            position:"last"
	        });   
		}
		function d_tableMemberWinLoss_htv999_details1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency){
			document.getElementById('d_qry_htv999_betting_details1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_list5'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_betting_details1").appendChild(divTag);

		    var div2Tag = document.createElement("Div"); 
		    div2Tag.id = 'd_pager5'; 
		    div2Tag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_htv999_betting_details1").appendChild(div2Tag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#d_list5");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTV999WinLossDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency, 
					datatype: 'json',
				    mtype: 'POST',
				    height: 'auto',
				    colNames: ["<?php echo Yii::t('agent','agent.agentwinloss.number');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>", "<?php echo Yii::t('agent','agent.agentwinloss.currency');?>","<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>", "<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>", "<?php echo Yii::t('agent','agent.agentwinloss.commission');?>", "<?php echo Yii::t('agent','agent.agentwinloss.tips');?>","<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>","<?php echo Yii::t('agent','agent.agentwinloss.total');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.casino');?>"],
				    colModel: [
					  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
					  //{name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
					  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : '<?php echo Yii::t('agent','agent.agentwinloss.total');?>'},
				      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
				      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
				      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
				      {name: 'bet_amount', index: 'bet_amount', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      //{name: 'amount_wagers', index: 'amount_wagers', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
				      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
				      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
				      {name: 'bonus', index: 'bonus', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
				      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
				      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
				    ],
				    loadComplete: function() {
					    var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	//if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
					    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
					    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
					    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
					    	if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
					    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
					    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
						}
					  	//color for summary footer
				        //$(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
				        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
				        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
				        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
				        $(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
				        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
				        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

				        $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    	d_showHTVBettingFooter1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency);
				    },
				    loadtext:"",
	                sortable: false,
				    sortname: 'row',
				    sortorder: 'ASC',
				    pager: '#d_pager5',
				    rowNum: 25,	
				    rowList: [20, 50, 100, 200, 99999],
				    caption: '<b><label id="lblBettingDetailAgentID_htv"></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_betting_details_title');?></b> <label id="lblDDateFrom_htv"></label> - <label id="lblDDateTo_htv"></label> <input type="button" value="<<" onclick="hideBettingDetails_htv();">',
				    hidegrid: false,
				    viewrecords: true,
				    grouping: true,
				   	groupingView : {
				   		groupField : ['currency_name'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				});
				$('#d_list5').jqGrid('navGrid', '#d_pager5', {edit: false, add: false, del:false, search: false, refresh: false});
				$("#d_list5").jqGrid('setGroupHeaders', {
					  useColSpanStyle: true, 
					  groupHeaders:[
						{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					  ]
				});
				$("#d_list5").jqGrid('navButtonAdd','#d_pager5',{
		            caption:"Export to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var table1= document.getElementById('d_qry_htv999_betting_details1');
		    			d_exportWinLossToExcel1('HTV999_SLOT_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
		           }, 
		            position:"last"
		        });   
				
			});
		}
		/********END: HTV999 WinLoss*******************************************************/
		/********START: SAVAN VEGAS WinLoss*******************************************************/
		function d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_savan_vegas_total1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstTotal_savan1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_total1").appendChild(divTag);
	    	var grid=jQuery("#d_lstTotal_savan1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANTotal&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    	 var col_group_header='';
		        if(accountID.length==0){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
			    }else if(accountID.length==2){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
				}else if(accountID.length==4){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
				}else if(accountID.length==6){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
				}else if(accountID.length==8){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
				}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
					  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
					  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
	    		      {name: 'company', index: 'company', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
	    		],
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            rownumbers: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<font color='#ab0e10'><label id='lblSAVANTotalWinLossID11'></label></font> SAVANVEGAS999 TOTAL <select id='currencyTypeConvertSAVAN' onchange='javascript: d_savanTotalConversion1(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            footerrow:true,
	    	});
	    	$("#d_lstTotal_savan1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
			
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[6].name);
			}
		}
		function d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_savan_vegas_result1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstSAVAN1_slot'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_result1").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrSAVAN1_slot'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_result1").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstSAVAN1_slot");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANSummaryWinLoss&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;

	        var col_agent='';
	        var col_group_header='';
	        if(accountID.length==0){
	        	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
		    	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}
	        var fltBetCount=0;
	        var fltValidBet=0;
	        var strCurrency='';
	        function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function savan_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',col_agent,'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_savan_slot',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				
				    //Lower level
				    $(".account_id_savan_slot").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
					    	d_savanSubAgent1(account_id_val);
			    			document.getElementById('lblAgentID_savan1').innerHTML=account_id_val;
			    			//document.getElementById('qry_costavegas_total').innerHTML='';
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
	    			
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_savan1'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_savan1'></label> - <label id='lblDateTo_savan1'></label> <input type='button' id='btnBack_savan_slot' value='<<' onclick='d_backToParent_savan1(document.getElementById(\"lblAgentID_savan1\").innerHTML);'> <input type='button' id='btnDetails_savan' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='d_showDetails_savan1(document.getElementById(\"lblAgentID_savan1\").innerHTML);'> <select id='currencyTypeConvertSAVANWinLoss' onchange='javascript: d_savanWinLossConversion1(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrSAVAN1_slot',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstSAVAN1_slot").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	$('#d_lstSAVAN1_slot').jqGrid('navGrid', '#d_pgrSAVAN1_slot', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstSAVAN1_slot").jqGrid('navButtonAdd','#d_pgrSAVAN1_slot',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_savan_vegas_total1');
	    			var table2= document.getElementById('d_qry_savan_vegas_result1');
	    			d_exportWinLossToExcel1('SAVANVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
			}
		}

		function d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_savan_vegas_result1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstSAVAN1_slot'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_result1").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrSAVAN1_slot'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_result1").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstSAVAN1_slot");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANSummaryMemberWinLoss&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    	function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function savan_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_savan_slot',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".savan_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    $(".account_id_savan_slot").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    //start: hide parent grids
						    document.getElementById('d_qry_savan_vegas_result1').style.display='none';
						    document.getElementById('d_qry_savan_vegas_result_details1').style.display='none';
						    //end: hide parent grids
					    	var dtfrom=document.getElementById('lblDetailDateFrom_savan_slot').innerHTML;
		        			var dtto=document.getElementById('lblDetailDateTo_savan_slot').innerHTML;
		        			d_tableMemberWinLoss_savan_vegas_details1(account_id_val,d_dtFrom_slot,d_dtTo_slot,document.getElementById('d_cmbGame_slot').value);
		        			document.getElementById('d_lblBettingDetailAgentID_savan').innerHTML=account_id_val;
		        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
		        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_savan1'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='lblDateFrom_savan1'></label> - <label id='lblDateTo_savan1'></label> <input type='button' value='<<' id='btnBack_savan_slot' onclick='d_backToParent_savan1(document.getElementById(\"lblAgentID_savan1\").innerHTML);'> <input type='button' id='btnDetails_savan' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='d_showDetails_savan1(document.getElementById(\"lblAgentID_savan1\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: d_savanMemberWinLossConversion1(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrSAVAN1_slot',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstSAVAN1_slot").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
				  ]
			});
	    	$('#d_lstSAVAN1_slot').jqGrid('navGrid', '#d_pgrSAVAN1_slot', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstSAVAN1_slot").jqGrid('navButtonAdd','#d_pgrSAVAN1_slot',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_savan_vegas_total1');
	    			var table2= document.getElementById('d_qry_savan_vegas_result1');
	    			d_exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
		}
		//Details
		
		function d_tableSAVANWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_savan_vegas_result_details1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstSAVAN2_slot'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_result_details1").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrSAVAN2_slot'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_result_details1").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstSAVAN2_slot");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANWinLossDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;

	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>',
	    		    	'<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
						'<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.company');?>'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_savan_slot',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_bonus', index: 'mem_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_bonus', index: 'agt_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_bonus', index: 'ma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_bonus', index: 'sma_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_bonus', index: 'sc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    
				    //Lower level
				    $(".detail_account_id_savan_slot").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    if(account_id_val.length!=10){
				    			d_showDetails_savan1(account_id_val);
				    			document.getElementById('lblDetailAgentID_savan_slot').innerHTML=account_id_val;
						    }else{
							    //start: hide parent grids
							    document.getElementById('d_qry_savan_vegas_result1').style.display='none';
							    document.getElementById('d_qry_savan_vegas_result_details1').style.display='none';
							    //end: hide parent grids
						    	var dtfrom=document.getElementById('lblDetailDateFrom_savan_slot').innerHTML;
			        			var dtto=document.getElementById('lblDetailDateTo_savan_slot').innerHTML;
			        			d_tableMemberWinLoss_savan_vegas_details1(account_id_val,d_dtFrom_slot,d_dtTo_slot,document.getElementById('d_cmbGame_slot').value);
			        			document.getElementById('d_lblBettingDetailAgentID_savan').innerHTML=account_id_val;
			        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
			        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
							}
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	        			var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblDetailAgentID_savan_slot'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_winloss_details');?> <label id='lblDetailDateFrom_savan_slot'></label> - <label id='lblDetailDateTo_savan_slot'></label> <input type='button' id='btnDetailBack_savan_slot' value='<<' onclick='d_backToParentDetail_savan(document.getElementById(\"lblDetailAgentID_savan_slot\").innerHTML);'>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrSAVAN2_slot',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstSAVAN2_slot").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
					{startColumnName: 'ma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.master_agent');?></label>'},
					{startColumnName: 'sma_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?></label>'},
					{startColumnName: 'sc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.sub_company');?></label>'},
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.main_company');?></label>'},
				  ]
			});
	    	$('#d_lstSAVAN2_slot').jqGrid('navGrid', '#d_pgrSAVAN2_slot', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstSAVAN2_slot").jqGrid('navButtonAdd','#d_pgrSAVAN2_slot',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_savan_vegas_result_details1');
	    			d_exportWinLossToExcel1('SAVANVEGAS999_SLOT_WIN_LOSS_DETAILS_',table1.outerHTML);
	           }, 
	            position:"last"
	        });
	    	$("#d_lstSAVAN2_slot").jqGrid('navButtonAdd','#d_pgrSAVAN2_slot',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSAVANWinLossDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    			d_exportAllWinLossToExcel1('SAVANVEGAS999_SLOT_WIN_LOSS_DETAILS_',url);
	           }, 
	            position:"last"
	        });
		}
		function d_tableMemberWinLoss_savan_vegas_details1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency){
			document.getElementById('d_qry_savan_vegas_betting_details1').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_list6_slot'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_betting_details1").appendChild(divTag);

		    var div2Tag = document.createElement("Div"); 
		    div2Tag.id = 'd_pager_savan_slot'; 
		    div2Tag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_savan_vegas_betting_details1").appendChild(div2Tag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#d_list6_slot");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANWinLossBettingDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency, 
					datatype: 'json',
				    mtype: 'POST',
				    height: 'auto',
				    colNames: ["<?php echo Yii::t('agent','agent.agentwinloss.number');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>", "<?php echo Yii::t('agent','agent.agentwinloss.currency');?>","<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>", "<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>", "<?php echo Yii::t('agent','agent.agentwinloss.commission');?>", "<?php echo Yii::t('agent','agent.agentwinloss.tips');?>","<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>","<?php echo Yii::t('agent','agent.agentwinloss.total');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.casino');?>"],
				    colModel: [
					  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
					  //{name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
					  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : '<?php echo Yii::t('agent','agent.agentwinloss.total');?>'},
				      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
				      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
				      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
				      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      //{name: 'amount_wagers', index: 'amount_wagers', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
				      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
				      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
				      {name: 'bonus', index: 'bonus', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
				      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
				      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
				    ],
				    loadComplete: function() {
					    var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	//if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
					    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
					    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
					    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
					    	if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
					    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
					    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
						}
					  	//color for summary footer
				       // $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
				        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
				        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
				        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
				        $(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
				        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
				        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

				        $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    	d_showSAVANBettingFooter1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency);
				    },
				    loadtext:"",
	                sortable: false,
				    sortname: 'no',
				    sortorder: 'ASC',
				    pager: '#d_pager_savan_slot',
				    rowNum: 25,	
				    rowList: [20, 50, 100, 200, 99999],
				    caption: '<b><label id="d_lblBettingDetailAgentID_savan"></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_betting_details_title');?></b> <label id="lblDDateFrom_savan"></label> - <label id="lblDDateTo_savan"></label> <input type="button" value="<<" onclick="d_hideBettingDetails_savan1();">',
				    hidegrid: false,
				    viewrecords: true,
				    grouping: true,
				   	groupingView : {
				   		groupField : ['currency_name'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				});
				$('#d_list6_slot').jqGrid('navGrid', '#d_pager_savan_slot', {edit: false, add: false, del:false, search: false, refresh: false});
				$("#d_list6_slot").jqGrid('setGroupHeaders', {
					  useColSpanStyle: true, 
					  groupHeaders:[
						{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					  ]
				});
				$("#d_list6_slot").jqGrid('navButtonAdd','#d_pager_savan_slot',{
		            caption:"Export Current Page to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var table1= document.getElementById('d_qry_savan_vegas_betting_details1');
		    			d_exportWinLossToExcel1('SAVANVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
		           }, 
		            position:"last"
		        });   
		        
				$("#d_list6_slot").jqGrid('navButtonAdd','#d_pager_savan_slot',{
		            caption:"Export All Pages to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllSAVANWinLossBettingDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency;
		    			d_exportAllWinLossToExcel1('SAVANVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_', url);
		           }, 
		            position:"last"
		        });   				
			});
		}
		/********END: SAVANVEGAS WinLoss*******************************************************/
		/********START: VIRTUAVEGAS WinLoss*******************************************************/
		function d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_virtua_vegas_total').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstTotal_virtua'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_total").appendChild(divTag);
	    	var grid=jQuery("#d_lstTotal_virtua");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUATotal&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    	 var col_group_header='';
		        if(accountID.length==0){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
			    }else if(accountID.length==2){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
				}else if(accountID.length==4){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
				}else if(accountID.length==6){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
				}else if(accountID.length==8){
		        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
				}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
					  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
					  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
	    		      {name: 'company', index: 'company', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
	    		],
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            rownumbers: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<font color='#ab0e10'><label id='lblVIRTUATotalWinLossID'></label></font> VIRTUAVEGAS999 TOTAL <select id='currencyTypeConvertVIRTUA' onchange='javascript: d_virtuaTotalConversion1(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            footerrow:true,
	    	});
	    	$("#d_lstTotal_virtua").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[6].name);
			}
		}
		function d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_virtua_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstVIRTUA1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrVIRTUA1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstVIRTUA1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUASummaryWinLoss&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;

	        var col_agent='';
	        var col_group_header='';
	        if(accountID.length==0){
	        	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		    }else if(accountID.length==2){
		    	col_agent='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
			}else if(accountID.length==4){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
			}else if(accountID.length==6){
				col_agent='<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
	        	col_group_header='<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
			}
	        var fltBetCount=0;
	        var fltValidBet=0;
	        var strCurrency='';
	        function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function virtua_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>','<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>',col_agent,'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_virtua',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: virtua_xrate,summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".htv_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				
				    //Lower level
				    $(".account_id_virtua").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
					    	d_virtuaSubAgent1(account_id_val);
			    			document.getElementById('lblAgentID_virtua').innerHTML=account_id_val;
			    			//document.getElementById('qry_costavegas_total').innerHTML='';
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
	    			
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_virtua'></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_title');?> <label id='lblDateFrom_virtua'></label> - <label id='lblDateTo_virtua'></label> <input type='button' id='btnBack_virtua' value='<<' onclick='d_backToParent_virtua1(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <input type='button' id='btnDetails_virtua' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='d_showDetails_virtua1(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <select id='currencyTypeConvertVIRTUAWinLoss' onchange='javascript: virtuaWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrVIRTUA1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstVIRTUA1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">'+col_group_header+'</label>'},
				  ]
			});
	    	$('#d_lstVIRTUA1').jqGrid('navGrid', '#d_pgrVIRTUA1', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstVIRTUA1").jqGrid('navButtonAdd','#d_pgrVIRTUA1',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_virtua_vegas_total');
	    			var table2= document.getElementById('d_qry_virtua_vegas_result');
	    			d_exportWinLossToExcel1('VIRTUAVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
	    	if(accountID==''){
				grid.jqGrid('showCol', grid.getGridParam("colModel")[12].name);
			}
		}

		function d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_virtua_vegas_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstVIRTUA1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrVIRTUA1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_result").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstVIRTUA1");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUASummaryMemberWinLoss&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    	function myAvg(val, name, record)
	        {
		        if (strCurrency==record['currency_name']){
		        	fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}else{
					fltBetCount=0;
			        fltValidBet=0;
			        fltBetCount+=parseFloat(record['bet_count']);
		        	fltValidBet+=parseFloat(record['valid_bet']);
				}
	        	strCurrency=record['currency_name'];
	        	return parseFloat(fltValidBet/fltBetCount);
	        }
	        function virtua_xrate(val,name,record){
				return record['exchange_rate'];
			}
	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>','<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.balance');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>',
	    		    		'<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.company');?>','exchange_rate'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'account_id_virtua',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'bonus', index: 'bonus', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: virtua_xrate,summaryTpl:'<label class="virtua_sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".virtua_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    $(".account_id_virtua").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    //start: hide parent grids
						    document.getElementById('d_qry_virtua_vegas_result').style.display='none';
						    document.getElementById('d_qry_virtua_vegas_result_details').style.display='none';
						    //end: hide parent grids
					    	var dtfrom=document.getElementById('lblDateFrom_virtua').innerHTML;
		        			var dtto=document.getElementById('lblDateTo_virtua').innerHTML;
		        			d_tableMemberWinLoss_virtua_vegas_details1(account_id_val,d_dtFrom_slot,d_dtTo_slot,document.getElementById('d_cmbGame_slot').value);
		        			document.getElementById('lblBettingDetailAgentID_virtua').innerHTML=account_id_val;
		        			document.getElementById('lblDDateFrom_virtua').innerHTML=dtfrom;
		        			document.getElementById('lblDDateTo_virtua').innerHTML=dtto;
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	            		var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
	    			
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblAgentID_virtua'></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_title');?> <label id='lblDateFrom_virtua'></label> - <label id='lblDateTo_virtua'></label> <input type='button' value='<<' id='btnBack_virtua' onclick='d_backToParent_virtua1(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <input type='button' id='btnDetails_virtua' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='d_showDetails_virtua1(document.getElementById(\"lblAgentID_virtua\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: d_virtuaMemberWinLossConversion1(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrVIRTUA1',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstVIRTUA1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
				  ]
			});
	    	$('#d_lstVIRTUA1').jqGrid('navGrid', '#d_pgrVIRTUA1', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstVIRTUA1").jqGrid('navButtonAdd','#d_pgrVIRTUA1',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_virtua_vegas_total');
	    			var table2= document.getElementById('d_qry_virtua_vegas_result');
	    			d_exportWinLossToExcel1('VIRTUAVEGAS999_SLOT_WIN_LOSS_',table1.outerHTML + ' ' + table2.outerHTML);
	           }, 
	            position:"last"
	        });   
		}
		//Details
		
		function d_tableVIRTUAWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency){
			document.getElementById('d_qry_virtua_vegas_result_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_lstVIRTUA2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_result_details").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'd_pgrVIRTUA2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_result_details").appendChild(divTag1);
		    
	    	var grid=jQuery("#d_lstVIRTUA2");
	    	var grid_url='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUAWinLossDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;

	    	grid.jqGrid({ 
	    		url: grid_url, 
				datatype: 'json',
				mtype: 'POST',
				height: 'auto',
	    		colNames: ['<?php echo Yii::t('agent','agent.agentwinloss.number');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>','<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.currency');?>','<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>','<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>',
    		    		'<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>','<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>','<?php echo Yii::t('agent','agent.agentwinloss.balance');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>','<?php echo Yii::t('agent','agent.agentwinloss.commission');?>','<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>','<?php echo Yii::t('agent','agent.agentwinloss.total');?>',
						'<?php echo Yii::t('agent','agent.agentwinloss.company');?>'],
	    		colModel: [
					  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
	    			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_virtua',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
	    		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
	    		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
	    		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
	    		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_bonus', index: 'mem_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'ma_bonus', index: 'ma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'sma_bonus', index: 'sma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      //{name: 'sc_bonus', index: 'sc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_bonus', index: 'mc_bonus', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
	    		],
	    		loadComplete: function() {
		    		//footer summary font color
				    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
				    
				    //Lower level
				    $(".detail_account_id_virtua").click(function() {
				    	var account_id_val=this.innerHTML;
				    	if(account_id_val!='&nbsp;'){
						    if(account_id_val.length!=10){
				    			d_showDetails_virtua1(account_id_val);
				    			document.getElementById('lblDetailAgentID_virtua').innerHTML=account_id_val;
						    }else{
							    //start: hide parent grids
							    document.getElementById('d_qry_virtua_vegas_result').style.display='none';
							    document.getElementById('d_qry_virtua_vegas_result_details').style.display='none';
							    //end: hide parent grids
						    	var dtfrom=document.getElementById('lblDetailDateFrom_virtua').innerHTML;
			        			var dtto=document.getElementById('lblDetailDateTo_virtua').innerHTML;
			        			d_tableMemberWinLoss_virtua_vegas_details1(account_id_val,d_dtFrom_slot,d_dtTo_slot,document.getElementById('d_cmbGame_slot').value);
			        			document.getElementById('lblBettingDetailAgentID_virtua').innerHTML=account_id_val;
			        			document.getElementById('lblDDateFrom_virtua').innerHTML=dtfrom;
			        			document.getElementById('lblDDateTo_virtua').innerHTML=dtto;
							}
				    	}
		    		});
				    $('.sharing_commission').mouseover(function(e) {
	        			//alert(this.name);
	        			var x = e.pageX - this.offsetLeft;
        				var y = e.pageY - this.offsetTop;
	            		showDiv_slot('d_dvSharingCommission_slot',this,x,y);
	            		
	            		jQuery.ajax({
	            			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SharingAndCommission&agent_id=' + this.name,
	            			method: 'POST',
	            			data: {},
	            			context: '',
	            			success: function(data){
	            				document.getElementById('d_dvSharingCommission_slot').innerHTML='';
	            				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML=data;
	            				}else{
	            					document.getElementById('d_dvSharingCommission_slot').innerHTML='Please set-up sharing/commission!';
	                			}
	            	    	}
	            		});
	        		});
	    			$('.sharing_commission').mouseout(function(e) {
	    				hideDiv_slot('d_dvSharingCommission_slot');
	    			});
					    
	        	},   	
	    		treeGridModel:'adjacency',
	            height:'auto',
	            hidegrid: false,
	            treeGrid: false,
	            ExpandColumn:'desc',
	            ExpandColClick: true,
	            ExpandColumn: 'account_id',
	            caption:"<label id='lblDetailAgentID_virtua'></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_winloss_details');?> <label id='lblDetailDateFrom_virtua'></label> - <label id='lblDetailDateTo_virtua'></label> <input type='button' id='btnDetailBack_virtua' value='<<' onclick='d_backToParentDetail_virtua1(document.getElementById(\"lblDetailAgentID_virtua\").innerHTML);'>",
	            viewrecords: true,
	            grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	pager: '#d_pgrVIRTUA2',
			   	rowNum: 25,	
			    rowList: [20, 50, 100, 200, 99999],
	    	});   
	    	$("#d_lstVIRTUA2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'mem_win_loss', numberOfColumns: 5, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.agent');?></label>'},
					{startColumnName: 'ma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.master_agent');?></label>'},
					{startColumnName: 'sma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?></label>'},
					{startColumnName: 'sc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.sub_company');?></label>'},
					{startColumnName: 'mc_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.main_company');?></label>'},
				  ]
			});
	    	$('#d_lstVIRTUA2').jqGrid('navGrid', '#d_pgrVIRTUA2', {edit: false, add: false, del:false, search: false});
	    	$("#d_lstVIRTUA2").jqGrid('navButtonAdd','#d_pgrVIRTUA2',{
	            caption:"Export Current Page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var table1= document.getElementById('d_qry_virtua_vegas_result_details');
	    			d_exportWinLossToExcel1('VIRTUAVEGAS999_SLOT_WIN_LOSS_DETAILS_',table1.outerHTML);
	           }, 
	            position:"last"
	        });  
	    	$("#d_lstVIRTUA2").jqGrid('navButtonAdd','#d_pgrVIRTUA2',{
	            caption:"Export All Pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){
	            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllVIRTUAWinLossDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('d_txtAccountID_slot').value;
	    			d_exportAllWinLossToExcel1('VIRTUAVEGAS999_SLOT_WIN_LOSS_DETAILS_',url);
	           }, 
	            position:"last"
	        }); 
		}
		function d_tableMemberWinLoss_virtua_vegas_details1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency){
			document.getElementById('d_qry_virtua_vegas_betting_details').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'd_list6_slot1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_betting_details").appendChild(divTag);

		    var div2Tag = document.createElement("Div"); 
		    div2Tag.id = 'd_pager_virtua_slot'; 
		    div2Tag.style.margin = "0px auto"; 
		    document.getElementById("d_qry_virtua_vegas_betting_details").appendChild(div2Tag);
		    
		    $(document).ready(function() {
				var grid=jQuery("#d_list6_slot1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUAWinLossBettingDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency, 
					datatype: 'json',
				    mtype: 'POST',
				    height: 'auto',
				    colNames: ["<?php echo Yii::t('agent','agent.agentwinloss.number');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>","<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>", "<?php echo Yii::t('agent','agent.agentwinloss.currency');?>","<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>","<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>", "<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>", "<?php echo Yii::t('agent','agent.agentwinloss.commission');?>", "<?php echo Yii::t('agent','agent.agentwinloss.tips');?>","<?php echo Yii::t('agent','agent.agentslotwinloss.jackpot');?>","<?php echo Yii::t('agent','agent.agentwinloss.total');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.balance');?>", "<?php echo Yii::t('agent','agent.agentwinloss.casino');?>"],
				    colModel: [
					  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
					  //{name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
					  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : '<?php echo Yii::t('agent','agent.agentwinloss.total');?>'},
				      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
				      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
				      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
				      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      //{name: 'amount_wagers', index: 'amount_wagers', width: 90,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
				      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cwinlos">{0}</label>'},
				      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
				      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
				      {name: 'bonus', index: 'bonus', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
				      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
				      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}, hidden: true},
				      {name: 'na_balance', index: 'balance', width: 100,sortable: false},
				      {name: 'casino_code', index: 'casino_code', width: 90,sortable: false,},
				    ],
				    loadComplete: function() {
					    var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	//if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
					    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
					    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
					    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
					    	if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
					    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
					    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
						}
					  	//color for summary footer
				       // $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
				        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
				        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
				        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
				        $(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
				        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
				        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

				        $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    	d_showVIRTUABettingFooter1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency);
				    },
				    loadtext:"",
	                sortable: false,
				    sortname: 'no',
				    sortorder: 'ASC',
				    pager: '#d_pager_virtua_slot',
				    rowNum: 25,	
				    rowList: [20, 50, 100, 200, 99999],
				    caption: '<b><label id="lblBettingDetailAgentID_virtua"></label> <?php echo Yii::t('agent','agent.agentwinloss.virtua_betting_details_title');?></b> <label id="lblDDateFrom_virtua"></label> - <label id="lblDDateTo_virtua"></label> <input type="button" value="<<" onclick="d_hideBettingDetails_virtua1();">',
				    hidegrid: false,
				    viewrecords: true,
				    grouping: true,
				   	groupingView : {
				   		groupField : ['currency_name'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				});
				$('#d_list6_slot1').jqGrid('navGrid', '#d_pager_virtua_slot', {edit: false, add: false, del:false, search: false, refresh: false});
				$("#d_list6_slot1").jqGrid('setGroupHeaders', {
					  useColSpanStyle: true, 
					  groupHeaders:[
						{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black"><?php echo Yii::t('agent','agent.agentwinloss.player');?></label>'},
					  ]
				});
				$('#d_list6_slot1').jqGrid('navGrid', '#d_pager_virtua_slot', {edit: false, add: false, del:false, search: false});
		    	$("#d_list6_slot1").jqGrid('navButtonAdd','#d_pager_virtua_slot',{
		            caption:"Export Current Page to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var table1= document.getElementById('d_qry_virtua_vegas_betting_details');
		    			d_exportWinLossToExcel1('VIRTUAVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_',table1.outerHTML);
		           }, 
		            position:"last"
		        });  
		    	$("#d_list6_slot1").jqGrid('navButtonAdd','#d_pager_virtua_slot',{
		            caption:"Export All Pages to Excel", 
		            buttonicon:"ui-icon-calculator", 
		            onClickButton: function(){
		            	var url= '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/ExportAllVIRTUAWinLossBettingDetails&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency;
		    			d_exportAllWinLossToExcel1('VIRTUAVEGAS999_SLOT_WIN_LOSS_BETTING_DETAILS_',url);
		           }, 
		            position:"last"
		        });  
				
			});
		}
		/********END: VIRTUAVEGAS WinLoss*******************************************************/
	</script>
	<script>
	//Javascript functions
	function checkIt(evt,id) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    var strText=document.getElementById(id).value;
	    var countStr=(strText.split(".").length - 1);
	    if (charCode==46){
		    //allow only one period
	    	if(countStr > 0){
		    	return false;
		    }
		}
	    if (charCode > 31 && ((charCode < 48 && charCode!=46) || charCode > 57)) {
	        status = "This field accepts numbers only.";
	        return false;
	    }
	    status = "";
	    return true;
	}
	//start sharing and commission
	
	function onMouseOutDiv(id){
		document.getElementById('id').addEventListener('mouseout',onMouseOut,true);
	}
	function onMouseOut(event) {
	    //this is the original element the event handler was assigned to
	        e = event.toElement || event.relatedTarget;
	        if (e.parentNode == this || e == this) {
	           return;
	        }
	        hideDiv_slot("d_dvSharingCommission_slot");
	    // handle mouse event here!
	}
	$(document).ready(function() { 
		var div1Tag = document.createElement("div"); 
	    div1Tag.id = "d_dvSharingCommission_slot"; 
	    div1Tag.setAttribute("align", "center"); 
	    div1Tag.setAttribute("onmouseout","javascript: document.getElementById('d_dvSharingCommission_slot').addEventListener('mouseout',onMouseOut,true);");
	    div1Tag.style.margin = "0px auto"; 
	    div1Tag.className = "dynamicDiv"; 
	    document.body.appendChild(div1Tag);
	    hideDiv_slot('d_dvSharingCommission_slot');
	}); 

	function hideDiv_slot(divID) { 
		if (document.getElementById) { 
		 	document.getElementById(divID).style.visibility = 'hidden'; 
		} 
	}
		
	function showDiv_slot(divID,objXY,x,y) { 
		//get link position
		for (var lx=0, ly=0;
		objXY != null;
        lx += objXY.offsetLeft, ly += objXY.offsetTop, objXY = objXY.offsetParent);
        
        //set div new position
		document.getElementById(divID).style.left= (x-65) +"px";
		document.getElementById(divID).style.top= (y+15) +"px";
		document.getElementById(divID).style.visibility = 'visible'; 
	}
	//end sharing and commission
	jQuery(document).ready(function() {
	    jQuery("#d_dtFrom_slot").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	        //dateFormat: "dd-mm-yy"
	    });
	
	    jQuery("#d_dtTo_slot").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});
	
	function d_htvSubAgent1(accountID)
	{
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + " " + document.getElementById('d_cmbTime1_slot').value + ":00:00";
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + " " + document.getElementById('d_cmbTime2_slot').value + ":59:59";
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
			if(accountID.length!=8){
				d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}else{
				d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					}else{
						d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					}
				}else{
					d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
		}
		if(accountID==''){
			document.getElementById('d_lblDateTo_htv1').style.display='none';
		}else{
			document.getElementById('d_lblDateTo_htv1').style.display='inline';
		}
		document.getElementById('d_lblDateFrom_htv1').innerHTML=d_dtFrom_slot;
		document.getElementById('d_lblDateTo_htv1').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(document.getElementById('d_lblAgentID_htv1').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('d_lblAgentID_htv1').innerHTML=account_id;
					//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				}
			}
		}
	}
	function d_savanSubAgent1(accountID)
	{
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + " " + document.getElementById('d_cmbTime1_slot').value + ":00:00";
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + " " + document.getElementById('d_cmbTime2_slot').value + ":59:59";
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
			if(accountID.length!=8){
				d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}else{
				d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					}else{
						d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					}
				}else{
					d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
		}
		if(accountID==''){
			document.getElementById('btnBack_savan_slot').style.display='none';
		}else{
			document.getElementById('btnBack_savan_slot').style.display='inline';
		}
		document.getElementById('lblDateFrom_savan1').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_savan1').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(document.getElementById('lblAgentID_savan1').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('lblAgentID_savan1').innerHTML=account_id;
					//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				}
			}
		}
	}
	function d_virtuaSubAgent1(accountID)
	{
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + " " + document.getElementById('d_cmbTime1_slot').value + ":00:00";
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + " " + document.getElementById('d_cmbTime2_slot').value + ":59:59";
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
			if(accountID.length!=8){
				d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}else{
				d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					}else{
						d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					}
				}else{
					d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
			}
		}
		if(accountID==''){
			document.getElementById('btnBack_virtua').style.display='none';
		}else{
			document.getElementById('btnBack_virtua').style.display='inline';
		}
		document.getElementById('lblDateFrom_virtua').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_virtua').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(document.getElementById('lblAgentID_virtua').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
				}
			}
		}
	}
	function d_submitComplete1(accountID)
	{
		if(readVirtuaSlotWinloss!=1){
			document.getElementById('dv_virtuavegas').style.display='none';
			document.getElementById('d_chkVIRTUA_slot').style.display='none';
			document.getElementById('d_lblchkVIRTUA_slot').style.display='none';
		}
		if(readHatienSlotWinloss!=1){
			document.getElementById('dv_hatienvegas').style.display='none';
			document.getElementById('d_chkHTV_slot').style.display='none';
			document.getElementById('d_lblchkHTV_slot').style.display='none';
		}
		if(readSavanSlotWinloss!=1){
			document.getElementById('dv_savanvegas1').style.display='none';
			document.getElementById('d_chkSAVAN_slot').style.display='none';
			document.getElementById('d_lblchkSAVAN_slot').style.display='none';
		}
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + " " + document.getElementById('d_cmbTime1_slot').value + ":00:00";
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + " " + document.getElementById('d_cmbTime2_slot').value + ":59:59";
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(accountID==''){
				if(document.getElementById('d_chkHTV_slot').checked==true){
					document.getElementById('dv_hatienvegas1').style.display='block';
					d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_hatienvegas1').style.display='none';
				}
				if(document.getElementById('d_chkSAVAN_slot').checked==true){
					//document.getElementById('dv_savanvegas1').style.display='block';
					//d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
					//d_tableSAVANWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
					d_showDetails_savan1('');
				}else{
					document.getElementById('dv_savanvegas1').style.display='none';
				}
				if(document.getElementById('d_chkVIRTUA_slot').checked==true){
					document.getElementById('dv_virtuavegas').style.display='block';
					d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_virtuavegas').style.display='none';
				}
			}
			if(accountID.length!=8){
				if(document.getElementById('d_chkHTV_slot').checked==true){
					document.getElementById('dv_hatienvegas1').style.display='block';
					d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_hatienvegas1').style.display='none';
				}
				if(document.getElementById('d_chkSAVAN_slot').checked==true){
					document.getElementById('dv_savanvegas1').style.display='block';
					d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_savanvegas1').style.display='none';
				}
				if(document.getElementById('d_chkVIRTUA_slot').checked==true){
					document.getElementById('dv_virtuavegas').style.display='block';
					d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_virtuavegas').style.display='none';
				}
				
			}
		}else{
			if(accountID.length!=8){
				if(accountID==''){
					if(user_level!='AGT'){
						if(document.getElementById('d_chkHTV_slot').checked==true){
							document.getElementById('dv_hatienvegas1').style.display='block';
							d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
							d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_hatienvegas1').style.display='none';
						}
						if(document.getElementById('d_chkSAVAN_slot').checked==true){
							document.getElementById('dv_savanvegas1').style.display='block';
							d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
							d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_savanvegas1').style.display='none';
						}
						if(document.getElementById('d_chkVIRTUA_slot').checked==true){
							document.getElementById('dv_virtuavegas').style.display='block';
							d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
							d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_virtuavegas').style.display='none';
						}
					}else{
						if(document.getElementById('d_chkHTV_slot').checked==true){
							document.getElementById('dv_hatienvegas1').style.display='block';
							d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
							d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_hatienvegas1').style.display='none';
						}
						if(document.getElementById('d_chkSAVAN_slot').checked==true){
							document.getElementById('dv_savanvegas1').style.display='block';
							d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
							d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_savanvegas1').style.display='none';
						}
						if(document.getElementById('d_chkVIRTUA_slot').checked==true){
							document.getElementById('dv_virtuavegas').style.display='block';
							d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
							d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
						}else{
							document.getElementById('dv_virtuavegas').style.display='none';
						}
					}
				}else{
					if(document.getElementById('d_chkHTV_slot').checked==true){
						document.getElementById('dv_hatienvegas1').style.display='block';
						d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
					}else{
						document.getElementById('dv_hatienvegas1').style.display='none';
					}
					if(document.getElementById('d_chkSAVAN_slot').checked==true){
						document.getElementById('dv_savanvegas1').style.display='block';
						d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
					}else{
						document.getElementById('dv_savanvegas1').style.display='none';
					}
					if(document.getElementById('d_chkVIRTUA_slot').checked==true){
						document.getElementById('dv_virtuavegas').style.display='block';
						d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
					}else{
						document.getElementById('dv_virtuavegas').style.display='none';
					}
				}
			}else{
				if(document.getElementById('d_chkHTV_slot').checked==true){
					document.getElementById('dv_hatienvegas1').style.display='block';
					d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_hatienvegas1').style.display='none';
				}
				if(document.getElementById('d_chkSAVAN_slot').checked==true){
					document.getElementById('dv_savanvegas1').style.display='block';
					d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_savanvegas1').style.display='none';
				}
				if(document.getElementById('d_chkVIRTUA_slot').checked==true){
					document.getElementById('dv_virtuavegas').style.display='block';
					d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
				}else{
					document.getElementById('dv_virtuavegas').style.display='none';
				}
			}
		}
		if(accountID==''){
			document.getElementById('d_lblDateTo_htv1').style.display='none';
			document.getElementById('btnBack_savan_slot').style.display='none';
			document.getElementById('btnBack_virtua').style.display='none';
		}else{
			document.getElementById('d_lblDateTo_htv1').style.display='inline';
			document.getElementById('btnBack_savan_slot').style.display='inline';
			document.getElementById('btnBack_virtua').style.display='inline';
		}
		document.getElementById('d_lblDateFrom_htv1').innerHTML=d_dtFrom_slot;
		document.getElementById('d_lblDateTo_htv1').innerHTML=d_dtTo_slot;
		document.getElementById('lblDateFrom_savan1').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_savan1').innerHTML=d_dtTo_slot;
		document.getElementById('lblDateFrom_virtua').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_virtua').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(document.getElementById('d_lblAgentID_htv1').innerHTML==''){
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('d_lblAgentID_htv1').innerHTML=account_id;
					document.getElementById('lblAgentID_savan1').innerHTML=account_id;
					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					document.getElementById('lblHTVTotalWinLossID1').innerHTML=account_id;
					document.getElementById('lblSAVANTotalWinLossID11').innerHTML=account_id;
					document.getElementById('lblVIRTUATotalWinLossID').innerHTML=account_id;
				}
			}
		}
		d_hideBettingDetails_htv1();
		d_hideBettingDetails_savan1();
		d_hideBettingDetails_virtua1();
		if(readVirtuaSlotWinloss!=1){
			document.getElementById('dv_virtuavegas1').style.display='none';
			document.getElementById('d_chkVIRTUA_slot').checked==false;
			document.getElementById('d_chkVIRTUA_slot').style.display='none';
			document.getElementById('d_lblchkVIRTUA_slot').style.display='none';
		}
		if(readHatienSlotWinloss!=1){
			document.getElementById('dv_hatienvegas1').style.display='none';
			document.getElementById('d_chkHTV_slot').checked==false;
			document.getElementById('d_chkHTV_slot').style.display='none';
			document.getElementById('d_lblchkHTV_slot').style.display='none';
		}
		if(readSavanSlotWinloss!=1){
			document.getElementById('dv_savanvegas1').style.display='none';
			document.getElementById('d_chkSAVAN_slot').checked==false;
			document.getElementById('d_chkSAVAN_slot').style.display='none';
			document.getElementById('d_lblchkSAVAN_slot').style.display='none';
		}
	}
	function d_btnToDay1()
	{
		if(readVirtuaSlotWinloss!=1){
			document.getElementById('dv_virtuavegas1').style.display='none';
			document.getElementById('d_chkVIRTUA_slot').style.display='none';
			document.getElementById('d_lblchkVIRTUA_slot').style.display='none';
		}
		if(readHatienSlotWinloss!=1){
			document.getElementById('dv_hatienvegas1').style.display='none';
			document.getElementById('d_chkHTV_slot').style.display='none';
			document.getElementById('d_lblchkHTV_slot').style.display='none';
		}
		if(readSavanSlotWinloss!=1){
			document.getElementById('dv_savanvegas1').style.display='none';
			document.getElementById('d_chkSAVAN_slot').style.display='none';
			document.getElementById('d_lblchkSAVAN_slot').style.display='none';
		}
		var dtTo_slotday='<?php echo date("Y-m-d");?>';
		document.getElementById('d_dtFrom_slot').value=dtTo_slotday;
		document.getElementById('d_dtTo_slot').value=dtTo_slotday;
		document.getElementById('d_cmbTime1_slot').value='00';
		document.getElementById('d_cmbTime2_slot').value='23';
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + " " + document.getElementById('d_cmbTime1_slot').value + ":00:00";
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + " " + document.getElementById('d_cmbTime2_slot').value + ":59:59";
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}

		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(document.getElementById('d_chkHTV_slot').checked==true){
				document.getElementById('dv_hatienvegas1').style.display='block';
				d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
			}else{
				document.getElementById('dv_hatienvegas1').style.display='none';
			}
			if(document.getElementById('d_chkSAVAN_slot').checked==true){
				document.getElementById('dv_savanvegas1').style.display='block';
				//d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				//d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				d_showDetails_savan1('');
			}else{
				document.getElementById('dv_savanvegas1').style.display='none';
			}
			if(document.getElementById('d_chkVIRTUA_slot').checked==true){
				document.getElementById('dv_virtuavegas').style.display='block';
				//d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				//d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				d_showDetails_virtua1('');
			}else{
				document.getElementById('dv_virtuavegas').style.display='none';
			}
		}else{
			if(document.getElementById('d_chkHTV_slot').checked==true){
				document.getElementById('dv_hatienvegas1').style.display='block';
				d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
			}else{
				document.getElementById('dv_hatienvegas1').style.display='none';
			}
			if(document.getElementById('d_chkSAVAN_slot').checked==true){
				document.getElementById('dv_savanvegas1').style.display='block';
				//d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				d_showDetails_savan1('');
			}else{
				document.getElementById('dv_savanvegas1').style.display='none';
			}
			if(document.getElementById('d_chkVIRTUA_slot').checked==true){
				document.getElementById('dv_virtuavegas').style.display='block';
				//d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				d_showDetails_virtua1('');
			}else{
				document.getElementById('dv_virtuavegas').style.display='none';
			}
			if(user_level!='AGT'){
				if(document.getElementById('d_chkHTV_slot').checked==true){
					document.getElementById('dv_hatienvegas1').style.display='block';
					d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_hatienvegas1').style.display='none';
				}
				if(document.getElementById('d_chkSAVAN_slot').checked==true){
					document.getElementById('dv_savanvegas1').style.display='block';
					//d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_savan1('');
				}else{
					document.getElementById('dv_savanvegas1').style.display='none';
				}
				if(document.getElementById('d_chkVIRTUA_slot').checked==true){
					document.getElementById('dv_virtuavegas').style.display='block';
					//d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_virtua1('');
				}else{
					document.getElementById('dv_virtuavegas').style.display='none';
				}
			}else{
				if(document.getElementById('d_chkHTV_slot').checked==true){
					document.getElementById('dv_hatienvegas1').style.display='block';
					d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_hatienvegas1').style.display='none';
				}
				if(document.getElementById('d_chkSAVAN_slot').checked==true){
					document.getElementById('dv_savanvegas1').style.display='block';
					//d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_savan1('');
				}else{
					document.getElementById('dv_savanvegas1').style.display='none';
				}
				if(document.getElementById('d_chkVIRTUA_slot').checked==true){
					document.getElementById('dv_virtuavegas').style.display='block';
					//d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_virtua1('');
				}else{
					document.getElementById('dv_virtuavegas').style.display='none';
				}
			}
		}
		document.getElementById('d_lblDateFrom_htv1').innerHTML=d_dtFrom_slot;
		document.getElementById('d_lblDateTo_htv1').innerHTML=d_dtTo_slot;
		document.getElementById('lblDateFrom_savan1').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_savan1').innerHTML=d_dtTo_slot;
		document.getElementById('lblDateFrom_virtua').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_virtua').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(document.getElementById('d_lblAgentID_htv1').innerHTML==''){
			document.getElementById('d_lblDateTo_htv1').style.display='none';
			document.getElementById('btnBack_savan_slot').style.display='none';
			document.getElementById('btnBack_virtua').style.display='none';
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('d_lblAgentID_htv1').innerHTML=account_id;
					document.getElementById('lblAgentID_savan1').innerHTML=account_id;
					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					document.getElementById('lblHTVTotalWinLossID1').innerHTML=account_id;
					document.getElementById('lblSAVANTotalWinLossID11').innerHTML=account_id;
					document.getElementById('lblVIRTUATotalWinLossID').innerHTML=account_id;
				}
			}
		}
		d_hideBettingDetails_htv1();
		d_hideBettingDetails_savan1();
		d_hideBettingDetails_virtua1();
		if(readVirtuaSlotWinloss!=1){
			document.getElementById('dv_virtuavegas').style.display='none';
			document.getElementById('d_chkVIRTUA_slot').checked==false;
			document.getElementById('d_chkVIRTUA_slot').style.display='none';
			document.getElementById('d_lblchkVIRTUA_slot').style.display='none';
		}
		if(readHatienSlotWinloss!=1){
			document.getElementById('dv_hatienvegas1').style.display='none';
			document.getElementById('d_chkHTV_slot').checked==false;
			document.getElementById('d_chkHTV_slot').style.display='none';
			document.getElementById('d_lblchkHTV_slot').style.display='none';
		}
		if(readSavanSlotWinloss!=1){
			document.getElementById('dv_savanvegas1').style.display='none';
			document.getElementById('d_chkSAVAN_slot').checked==false;
			document.getElementById('d_chkSAVAN_slot').style.display='none';
			document.getElementById('d_lblchkSAVAN_slot').style.display='none';
		}
	}
	function d_btnYesterday1()
	{
		if(readVirtuaSlotWinloss!=1){
			document.getElementById('dv_virtuavegas').style.display='none';
			document.getElementById('d_chkVIRTUA_slot').style.display='none';
			document.getElementById('d_lblchkVIRTUA_slot').style.display='none';
		}
		if(readHatienSlotWinloss!=1){
			document.getElementById('dv_hatienvegas1').style.display='none';
			document.getElementById('d_chkHTV_slot').style.display='none';
			document.getElementById('d_lblchkHTV_slot').style.display='none';
		}
		if(readSavanSlotWinloss!=1){
			document.getElementById('dv_savanvegas1').style.display='none';
			document.getElementById('d_chkSAVAN_slot').style.display='none';
			document.getElementById('d_lblchkSAVAN_slot').style.display='none';
		}
		var dtYesterday='<?php echo date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d"))));?>';
		document.getElementById('d_dtFrom_slot').value=dtYesterday;
		document.getElementById('d_dtTo_slot').value=dtYesterday;
		document.getElementById('d_cmbTime1_slot').value='00';
		document.getElementById('d_cmbTime2_slot').value='23';
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + " " + document.getElementById('d_cmbTime1_slot').value + ":00:00";
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + " " + document.getElementById('d_cmbTime2_slot').value + ":59:59";
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id='<?php echo Yii::app()->session['account_id'];?>';
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
			if(document.getElementById('d_chkHTV_slot').checked==true){
				document.getElementById('dv_hatienvegas1').style.display='block';
				d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
			}else{
				document.getElementById('dv_hatienvegas1').style.display='none';
			}
			if(document.getElementById('d_chkSAVAN_slot').checked==true){
				document.getElementById('dv_savanvegas1').style.display='block';
				//d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				//d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				d_showDetails_savan1('');
			}else{
				document.getElementById('dv_savanvegas1').style.display='none';
			}
			if(document.getElementById('d_chkVIRTUA_slot').checked==true){
				document.getElementById('dv_virtuavegas').style.display='block';
				//d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				//d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,'',testCurrency);
				d_showDetails_virtua1('');
			}else{
				document.getElementById('dv_virtuavegas').style.display='none';
			}
		}else{
			if(document.getElementById('d_chkHTV_slot').checked==true){
				document.getElementById('dv_hatienvegas1').style.display='block';
				d_tableHTVWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
			}else{
				document.getElementById('dv_hatienvegas1').style.display='none';
			}
			if(document.getElementById('d_chkSAVAN_slot').checked==true){
				document.getElementById('dv_savanvegas1').style.display='block';
				//d_tableSAVANWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				d_showDetails_savan1('');
			}else{
				document.getElementById('dv_savanvegas1').style.display='none';
			}
			if(document.getElementById('d_chkVIRTUA_slot').checked==true){
				document.getElementById('dv_virtuavegas').style.display='block';
				//d_tableVIRTUAWinLossTotal1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				d_showDetails_savan1('');d_showDetails_virtua1('');
			}else{
				document.getElementById('dv_virtuavegas').style.display='none';
			}
			if(user_level!='AGT'){
				if(document.getElementById('d_chkHTV_slot').checked==true){
					document.getElementById('dv_hatienvegas1').style.display='block';
					d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_hatienvegas1').style.display='none';
				}
				if(document.getElementById('d_chkSAVAN_slot').checked==true){
					document.getElementById('dv_savanvegas1').style.display='block';
					//d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_savan1('');
				}else{
					document.getElementById('dv_savanvegas1').style.display='none';
				}
				if(document.getElementById('d_chkVIRTUA_slot').checked==true){
					document.getElementById('dv_virtuavegas').style.display='block';
					//d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_virtua1('');
				}else{
					document.getElementById('dv_virtuavegas').style.display='none';
				}
			}else{
				if(document.getElementById('d_chkHTV_slot').checked==true){
					document.getElementById('dv_hatienvegas1').style.display='block';
					d_tableHTVMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
				}else{
					document.getElementById('dv_hatienvegas1').style.display='none';
				}
				if(document.getElementById('d_chkSAVAN_slot').checked==true){
					document.getElementById('dv_savanvegas1').style.display='block';
					//d_tableSAVANMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_savan1('');
				}else{
					document.getElementById('dv_savanvegas1').style.display='none';
				}
				if(document.getElementById('d_chkVIRTUA_slot').checked==true){
					document.getElementById('dv_virtuavegas').style.display='block';
					//d_tableVIRTUAMemberWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
					d_showDetails_virtua1('');
				}else{
					document.getElementById('dv_virtuavegas').style.display='none';
				}
			}
		}
		
		document.getElementById('d_lblDateFrom_htv1').innerHTML=d_dtFrom_slot;
		document.getElementById('d_lblDateTo_htv1').innerHTML=d_dtTo_slot;
		document.getElementById('lblDateFrom_savan1').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_savan1').innerHTML=d_dtTo_slot;
		document.getElementById('lblDateFrom_virtua').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_virtua').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(document.getElementById('d_lblAgentID_htv1').innerHTML==''){
			document.getElementById('d_lblDateTo_htv1').style.display='none';
			document.getElementById('btnBack_savan_slot').style.display='none';
			document.getElementById('btnBack_virtua').style.display='none';
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('d_lblAgentID_htv1').innerHTML=account_id;
					document.getElementById('lblAgentID_savan1').innerHTML=account_id;
					document.getElementById('lblAgentID_virtua').innerHTML=account_id;
					document.getElementById('lblHTVTotalWinLossID1').innerHTML=account_id;
					document.getElementById('lblSAVANTotalWinLossID11').innerHTML=account_id;
					document.getElementById('lblVIRTUATotalWinLossID').innerHTML=account_id;
				}
			}
		}
		d_hideBettingDetails_htv1();
		d_hideBettingDetails_savan1();
		d_hideBettingDetails_virtua1();
		if(readVirtuaSlotWinloss!=1){
			document.getElementById('dv_virtuavegas').style.display='none';
			document.getElementById('d_chkVIRTUA_slot').checked==false;
			document.getElementById('d_chkVIRTUA_slot').style.display='none';
			document.getElementById('d_lblchkVIRTUA_slot').style.display='none';
		}
		if(readHatienSlotWinloss!=1){
			document.getElementById('dv_hatienvegas1').style.display='none';
			document.getElementById('d_chkHTV_slot').checked==false;
			document.getElementById('d_chkHTV_slot').style.display='none';
			document.getElementById('d_lblchkHTV_slot').style.display='none';
		}
		if(readSavanSlotWinloss!=1){
			document.getElementById('dv_savanvegas1').style.display='none';
			document.getElementById('d_chkSAVAN_slot').checked==false;
			document.getElementById('d_chkSAVAN_slot').style.display='none';
			document.getElementById('d_lblchkSAVAN_slot').style.display='none';
		}
	}
	
	function d_backToParent_htv1(accountID){
		var d_dtFrom_slot=document.getElementById('d_lblDateFrom_htv1').innerHTML;
		var d_dtTo_slot=document.getElementById('d_lblDateTo_htv1').innerHTML;
		var strCurrency= document.getElementById('d_cmbCurrency_slot').name;
		var intGame=document.getElementById('d_cmbGame_slot').name;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		d_tableHTVWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
		
		document.getElementById('d_lblDateFrom_htv1').innerHTML=d_dtFrom_slot;
		document.getElementById('d_lblDateTo_htv1').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(accountID.length!=0){
			document.getElementById('d_lblAgentID_htv1').innerHTML=account_id;
		}
		if(document.getElementById('d_lblAgentID_htv1').innerHTML==''){
			document.getElementById('d_lblDateTo_htv1').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('d_lblDateTo_htv1').style.display='none';
				}
			}
		}
	}
	function d_backToParent_savan1(accountID){
		var d_dtFrom_slot=document.getElementById('lblDateFrom_savan1').innerHTML;
		var d_dtTo_slot=document.getElementById('lblDateTo_savan1').innerHTML;
		var strCurrency= document.getElementById('d_cmbCurrency_slot').name;
		var intGame=document.getElementById('d_cmbGame_slot').name;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		d_tableSAVANWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
		
		document.getElementById('lblDateFrom_savan1').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_savan1').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(accountID.length!=0){
			document.getElementById('lblAgentID_savan1').innerHTML=account_id;
		}
		if(document.getElementById('lblAgentID_savan1').innerHTML==''){
			document.getElementById('btnBack_savan_slot').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnBack_savan_slot').style.display='none';
				}
			}
		}
	}
	function d_backToParent_virtua1(accountID){
		var d_dtFrom_slot=document.getElementById('lblDateFrom_virtua').innerHTML;
		var d_dtTo_slot=document.getElementById('lblDateTo_virtua').innerHTML;
		var strCurrency= document.getElementById('d_cmbCurrency_slot').name;
		var intGame=document.getElementById('d_cmbGame_slot').name;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		d_tableVIRTUAWinLoss1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
		
		document.getElementById('lblDateFrom_virtua').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDateTo_virtua').innerHTML=d_dtTo_slot;
		document.getElementById('d_cmbGame_slot').name=intGame;
		document.getElementById('d_cmbCurrency_slot').name=strCurrency;
		if(accountID.length!=0){
			document.getElementById('lblAgentID_virtua').innerHTML=account_id;
		}
		if(document.getElementById('lblAgentID_virtua').innerHTML==''){
			document.getElementById('btnBack_virtua').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnBack_virtua').style.display='none';
				}
			}
		}
	}
	function d_showDetails_htv1(account_id){
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var d_dtFrom_slot=document.getElementById('d_lblDateFrom_htv1').innerHTML;
		var d_dtTo_slot=document.getElementById('d_lblDateTo_htv1').innerHTML;
		var strCurrency= document.getElementById('d_cmbCurrency_slot').name;
		var intGame=document.getElementById('d_cmbGame_slot').name;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		if(account_id!=''){
			accountID=account_id;
		}else{
			if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
				accountID='';
			}else{
				accountID='<?php echo Yii::app()->session['account_id'];?>';
			}
		}

		jQuery("#d_lstHTV1").trigger("reloadGrid");//reload hatien winloss
		d_tableHTVWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
		jQuery("#d_lstTotal_htv1").trigger("reloadGrid");//reload hatien winloss total
		
		
		if(accountID.length!=0){
			var myGrid = $('#d_lstHTV2');
			document.getElementById('lblDetailAgentID_htv').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_htv').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDetailDateTo_htv').innerHTML=d_dtTo_slot;
		//hide back button
		if(document.getElementById('lblDetailAgentID_htv').innerHTML==''){
			document.getElementById('btnDetailBack_htv').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==accountID){
					document.getElementById('btnDetailBack_htv').style.display='none';
				}
			}
		}
	}
	function d_showDetails_savan1(account_id){

		var user_level='<?php echo Yii::app()->session['level'];?>';
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + ' ' + document.getElementById('d_cmbTime1_slot').value +':00:00';
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + ' ' + document.getElementById('d_cmbTime2_slot').value +':59:59';
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		if(account_id!=''){
			accountID=account_id;
		}else{
			if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
				accountID='';
			}else{
				accountID='<?php echo Yii::app()->session['account_id'];?>';
			}
		}

		jQuery("#d_lstSAVAN1_slot").trigger("reloadGrid");//reload savan winloss
		d_tableSAVANWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
		jQuery("#d_lstTotal_savan1").trigger("reloadGrid");//reload savan winloss total
		
		if(accountID.length!=0){
			var myGrid = $('#d_lstSAVAN2_slot');
			document.getElementById('lblDetailAgentID_savan_slot').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_savan_slot').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDetailDateTo_savan_slot').innerHTML=d_dtTo_slot;
		//hide back button
		if(document.getElementById('lblDetailAgentID_savan_slot').innerHTML==''){
			document.getElementById('btnDetailBack_savan_slot').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==accountID){
					document.getElementById('btnDetailBack_savan_slot').style.display='none';
				}
			}
		}
	}
	function d_showDetails_virtua1(account_id){
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var d_dtFrom_slot=document.getElementById('d_dtFrom_slot').value + ' ' + document.getElementById('d_cmbTime1_slot').value +':00:00';
		var d_dtTo_slot=document.getElementById('d_dtTo_slot').value + ' ' + document.getElementById('d_cmbTime2_slot').value +':59:59';
		var strCurrency= document.getElementById('d_cmbCurrency_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		if(account_id!=''){
			accountID=account_id;
		}else{
			if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
				accountID='';
			}else{
				accountID='<?php echo Yii::app()->session['account_id'];?>';
			}
		}

		jQuery("#d_lstVIRTUA1").trigger("reloadGrid");//reload virtua winloss
		d_tableVIRTUAWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,accountID,testCurrency);
		jQuery("#d_lstTotal_virtua").trigger("reloadGrid");//reload virtua winloss total
		
		if(accountID.length!=0){
			var myGrid = $('#d_lstVIRTUA2');
			document.getElementById('lblDetailAgentID_virtua').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}
		}
		document.getElementById('lblDetailDateFrom_virtua').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDetailDateTo_virtua').innerHTML=d_dtTo_slot;
		//hide back button
		if(document.getElementById('lblDetailAgentID_virtua').innerHTML==''){
			document.getElementById('btnDetailBack_virtua').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==accountID){
					document.getElementById('btnDetailBack_virtua').style.display='none';
				}
			}
		}
	}
	function d_backToParentDetail_htv(accountID){
		var d_dtFrom_slot=document.getElementById('lblDetailDateFrom_htv').innerHTML;
		var d_dtTo_slot=document.getElementById('lblDetailDateTo_htv').innerHTML;
		var strCurrency= document.getElementById('d_cmbGame_slot').name;
		var intGame=document.getElementById('d_cmbGame_slot').name;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		d_tableHTVWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
		if(account_id.length!=0){
			var myGrid = $('#d_lstHTV2');
			document.getElementById('lblDetailAgentID_htv').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_htv').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDetailDateTo_htv').innerHTML=d_dtTo_slot;
		if(accountID.length!=0){
			document.getElementById('lblDetailAgentID_htv').innerHTML=account_id;
		}
		if(document.getElementById('lblDetailAgentID_htv').innerHTML==''){
			document.getElementById('btnDetailBack_htv').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnDetailBack_htv').style.display='none';
				}
			}
		}
	}
	function d_backToParentDetail_savan(accountID){
		var d_dtFrom_slot=document.getElementById('lblDetailDateFrom_savan_slot').innerHTML;
		var d_dtTo_slot=document.getElementById('lblDetailDateTo_savan_slot').innerHTML;
		var strCurrency= document.getElementById('d_cmbGame_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		d_tableSAVANWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
		if(account_id.length!=0){
			var myGrid = $('#d_lstSAVAN2_slot');
			document.getElementById('lblDetailAgentID_savan_slot').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[30].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[31].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[32].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[33].name);
			}
		}
		document.getElementById('lblDetailDateFrom_savan_slot').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDetailDateTo_savan_slot').innerHTML=d_dtTo_slot;
		if(accountID.length!=0){
			document.getElementById('lblDetailAgentID_savan_slot').innerHTML=account_id;
		}
		if(document.getElementById('lblDetailAgentID_savan_slot').innerHTML==''){
			document.getElementById('btnDetailBack_savan_slot').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnDetailBack_savan_slot').style.display='none';
				}
			}
		}
	}
	function d_backToParentDetail_virtua1(accountID){
		var d_dtFrom_slot=document.getElementById('lblDetailDateFrom_virtua').innerHTML;
		var d_dtTo_slot=document.getElementById('lblDetailDateTo_virtua').innerHTML;
		var strCurrency= document.getElementById('d_cmbGame_slot').value;
		var intGame=document.getElementById('d_cmbGame_slot').value;
		var testCurrency=0;
		if(document.getElementById('d_chkTestCurrency_slot').checked==true){
			testCurrency=1;
		}else{
			testCurrency=0;
		}
		
		var user_level='<?php echo Yii::app()->session['level'];?>';
		var account_id=accountID.substring(0,accountID.length-2);
		d_tableVIRTUAWinLossDetails1(d_dtFrom_slot,d_dtTo_slot,strCurrency,intGame,account_id,testCurrency);
		if(account_id.length!=0){
			var myGrid = $('#d_lstVIRTUA2');
			document.getElementById('lblDetailAgentID_virtua').innerHTML=accountID;
			if(accountID.length==2){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==4){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==6){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}else if(accountID.length==8){
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[28].name);
				myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[29].name);
			}
		}
		document.getElementById('lblDetailDateFrom_virtua').innerHTML=d_dtFrom_slot;
		document.getElementById('lblDetailDateTo_virtua').innerHTML=d_dtTo_slot;
		if(accountID.length!=0){
			document.getElementById('lblDetailAgentID_virtua').innerHTML=account_id;
		}
		if(document.getElementById('lblDetailAgentID_virtua').innerHTML==''){
			document.getElementById('btnDetailBack_virtua').style.display='none';
		}else{
			if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
				if('<?php echo Yii::app()->session['account_id'];?>'==account_id){
					document.getElementById('btnDetailBack_virtua').style.display='none';
				}
			}
		}
	}
	function d_hideBettingDetails_htv1(){
		//start: show parent grids
	    document.getElementById('d_qry_htv999_result1').style.display='block';
	    document.getElementById('d_qry_htv999_result_details1').style.display='block';
	    //end: show parent grids
	    //start: clear betting details
	    document.getElementById('d_qry_htv999_betting_details1').innerHTML='';
	    document.getElementById('d_qry_htv999_betting_details_summary_footer1').style.display='none';
	    //end: clear betting details
	}
	function d_hideBettingDetails_savan1(){
		//start: show parent grids
	    document.getElementById('d_qry_savan_vegas_result1').style.display='block';
	    document.getElementById('d_qry_savan_vegas_result_details1').style.display='block';
	    //end: show parent grids
	    //start: clear betting details
	    document.getElementById('d_qry_savan_vegas_betting_details1').innerHTML='';
	    document.getElementById('d_qry_savan_vegas_betting_details_summary_footer1').style.display='none';
	    //end: clear betting details
	}
	function d_hideBettingDetails_virtua1(){
		//start: show parent grids
	    document.getElementById('d_qry_virtua_vegas_result').style.display='block';
	    document.getElementById('d_qry_virtua_vegas_result_details').style.display='block';
	    //end: show parent grids
	    //start: clear betting details
	    document.getElementById('d_qry_virtua_vegas_betting_details').innerHTML='';
	    document.getElementById('d_qry_virtua_vegas_betting_details_summary_footer').style.display='none';
	    //end: clear betting details
	}
	function d_clearContainer1(){
		//document.getElementById('d_qry_htv999_result_details1').innerHTML='';
		//document.getElementById('d_qry_savan_vegas_result_details1').innerHTML='';
		//document.getElementById('d_qry_virtua_vegas_result_details').innerHTML='';
	}
	function d_htvTotalConversion1(currency){
		var grid = jQuery("#d_lstTotal_htv1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstTotal_htv1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var company=grid.getCell(x, 'company');
		    	var total=grid.getCell(x, 'total');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 1, fltRate[0]);
		    	grid.jqGrid('setCell', x, 2, total_stake_convert);
		    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 4, win_loss_convert);
		    	grid.jqGrid('setCell', x, 5, commission_convert);
		    	grid.jqGrid('setCell', x, 6, bonus_convert);
		    	grid.jqGrid('setCell', x, 7, total_convert);
		    	grid.jqGrid('setCell', x, 8, company_convert);
		    	grid.jqGrid('setCell', x, 9, fltRate[1]);
		    	
		     	x++;
		    }

		    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
		    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
		    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
		    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
		    var bonus_sum=grid.jqGrid('getCol', 'bonus', false, 'sum');
		    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
		    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
	        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,bonus: bonus_sum,total: total_sum,company: company_sum});
		}else{
			grid.trigger("reloadGrid");
			grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',bonus:'',total: '',company: ''});
		}
	}
	function d_htvWinLossConversion1(currency){
		var grid = jQuery("#d_lstHTV1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstHTV1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	//var bet_count=grid.getCell(x, 'bet_count');
		    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var total=grid.getCell(x, 'total');
		    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
		    	var mc_comm=grid.getCell(x, 'mc_comm');
		    	var mc_bonus=grid.getCell(x, 'mc_bonus');
		    	var mc_total=grid.getCell(x, 'mc_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_bonus_convert=((parseFloat(mc_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, total_convert);
		    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
		    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
		    	grid.jqGrid('setCell', x, 12, mc_bonus_convert);
		    	grid.jqGrid('setCell', x, 13, mc_total_convert);
		    	grid.jqGrid('setCell', x, 14, company_convert);
		    	grid.jqGrid('setCell', x, 15, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("htv_sum_data");
		    var i=0;
		    var xrate=9;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=10;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
		
		
	}
	function d_htvMemberWinLossConversion1(currency){
		var grid = jQuery("#d_lstHTV1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstHTV1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	
		    	var average_bet=grid.getCell(x, 'average_bet');
  		    	var total_stake=grid.getCell(x, 'total_stake');
  		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var balance=grid.getCell(x, 'balance');
		    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
		    	var agt_comm=grid.getCell(x, 'agt_comm');
		    	var agt_bonus=grid.getCell(x, 'agt_bonus');
		    	var agt_total=grid.getCell(x, 'agt_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_bonus_convert=((parseFloat(agt_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, win_loss_convert);
		    	grid.jqGrid('setCell', x, 10, commission_convert);
		    	grid.jqGrid('setCell', x, 11, bonus_convert);
		    	grid.jqGrid('setCell', x, 12, total_convert);
		    	grid.jqGrid('setCell', x, 13, balance_convert);
		    	grid.jqGrid('setCell', x, 14, agt_win_loss_convert);
		    	grid.jqGrid('setCell', x, 15, agt_comm_convert);
		    	grid.jqGrid('setCell', x, 16, agt_bonus_convert);
		    	grid.jqGrid('setCell', x, 17, agt_total_convert);
		    	grid.jqGrid('setCell', x, 18, company_convert);
		    	grid.jqGrid('setCell', x, 19, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("htv_sum_data");
		    var i=0;
		    var xrate=12;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=13;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
	}
	function d_savanTotalConversion1(currency){
		var grid = jQuery("#d_lstTotal_savan1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstTotal_savan1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var company=grid.getCell(x, 'company');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 1, fltRate[0]);
		    	grid.jqGrid('setCell', x, 2, total_stake_convert);
		    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 4, win_loss_convert);
		    	grid.jqGrid('setCell', x, 5, commission_convert);
		    	grid.jqGrid('setCell', x, 6, bonus_convert);
		    	grid.jqGrid('setCell', x, 7, total_convert);
		    	grid.jqGrid('setCell', x, 8, company_convert);
		    	grid.jqGrid('setCell', x, 9, fltRate[1]);
		    	
		     	x++;
		    }

		    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
		    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
		    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
		    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
		    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
		    var bonus_sum=grid.jqGrid('getCol', 'bonus', false, 'sum');
		    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
	        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum, company: company_sum, bonus: bonus_sum});
		}else{
			grid.trigger("reloadGrid");
			grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',bonus: '',total: '',company: ''});
		}
	}
	function d_savanWinLossConversion1(currency){
		var grid = jQuery("#d_lstSAVAN1_slot");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstSAVAN1_slot").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
  		      
		    	//var bet_count=grid.getCell(x, 'bet_count');
		    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var total=grid.getCell(x, 'total');
		    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
		    	var mc_comm=grid.getCell(x, 'mc_comm');
		    	var mc_bonus=grid.getCell(x, 'mc_bonus');
		    	var mc_total=grid.getCell(x, 'mc_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_bonus_convert=((parseFloat(mc_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, total_convert);
		    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
		    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
		    	grid.jqGrid('setCell', x, 12, mc_bonus_convert);
		    	grid.jqGrid('setCell', x, 13, mc_total_convert);
		    	grid.jqGrid('setCell', x, 14, company_convert);
		    	grid.jqGrid('setCell', x, 15, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("savan_sum_data");
		    var i=0;
		    var xrate=9;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=10;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
		
		
	}
	function d_savanMemberWinLossConversion1(currency){
		var grid = jQuery("#d_lstSAVAN1_slot");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstSAVAN1_slot").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	
		    	var average_bet=grid.getCell(x, 'average_bet');
  		    	var total_stake=grid.getCell(x, 'total_stake');
  		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var balance=grid.getCell(x, 'balance');
		    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
		    	var agt_comm=grid.getCell(x, 'agt_comm');
		    	var agt_bonus=grid.getCell(x, 'agt_bonus');
		    	var agt_total=grid.getCell(x, 'agt_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_bonus_convert=((parseFloat(agt_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, win_loss_convert);
		    	grid.jqGrid('setCell', x, 10, commission_convert);
		    	grid.jqGrid('setCell', x, 11, bonus_convert);
		    	grid.jqGrid('setCell', x, 12, total_convert);
		    	grid.jqGrid('setCell', x, 13, balance_convert);
		    	grid.jqGrid('setCell', x, 14, agt_win_loss_convert);
		    	grid.jqGrid('setCell', x, 15, agt_comm_convert);
		    	grid.jqGrid('setCell', x, 16, agt_bonus_convert);
		    	grid.jqGrid('setCell', x, 17, agt_total_convert);
		    	grid.jqGrid('setCell', x, 18, company_convert);
		    	grid.jqGrid('setCell', x, 19, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("savan_sum_data");
		    var i=0;
		    var xrate=12;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=13;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
	}

	//---------------------
	function d_virtuaTotalConversion1(currency){
		var grid = jQuery("#d_lstTotal_virtua");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstTotal_virtua").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var company=grid.getCell(x, 'company');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 1, fltRate[0]);
		    	grid.jqGrid('setCell', x, 2, total_stake_convert);
		    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 4, win_loss_convert);
		    	grid.jqGrid('setCell', x, 5, commission_convert);
		    	grid.jqGrid('setCell', x, 6, bonus_convert);
		    	grid.jqGrid('setCell', x, 7, total_convert);
		    	grid.jqGrid('setCell', x, 8, company_convert);
		    	grid.jqGrid('setCell', x, 9, fltRate[1]);
		    	
		     	x++;
		    }

		    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
		    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
		    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
		    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
		    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
		    var bonus_sum=grid.jqGrid('getCol', 'bonus', false, 'sum');
		    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
	        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum, company: company_sum, bonus: bonus_sum});
		}else{
			grid.trigger("reloadGrid");
			grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',bonus: '',total: '',company: ''});
		}
	}
	function d_virtuaWinLossConversion1(currency){
		var grid = jQuery("#d_lstVIRTUA1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstVIRTUA1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
  		      
		    	//var bet_count=grid.getCell(x, 'bet_count');
		    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var total=grid.getCell(x, 'total');
		    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
		    	var mc_comm=grid.getCell(x, 'mc_comm');
		    	var mc_bonus=grid.getCell(x, 'mc_bonus');
		    	var mc_total=grid.getCell(x, 'mc_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_bonus_convert=((parseFloat(mc_bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, total_convert);
		    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
		    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
		    	grid.jqGrid('setCell', x, 12, mc_bonus_convert);
		    	grid.jqGrid('setCell', x, 13, mc_total_convert);
		    	grid.jqGrid('setCell', x, 14, company_convert);
		    	grid.jqGrid('setCell', x, 15, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("virtua_sum_data");
		    var i=0;
		    var xrate=9;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=10;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
		
		
	}
	function d_virtuaMemberWinLossConversion1(currency){
		var grid = jQuery("#d_lstVIRTUA1");
		if(currency!='--'){
			var fltRate=currency.split('-');
			var noRow=$("#d_lstVIRTUA1").getGridParam("reccount");
		    var x=1;
		    while (x<=noRow)
		    {
		    	
		    	var average_bet=grid.getCell(x, 'average_bet');
  		    	var total_stake=grid.getCell(x, 'total_stake');
  		    	var valid_bet=grid.getCell(x, 'valid_bet');
		    	var win_loss=grid.getCell(x, 'win_loss');
		    	var commission=grid.getCell(x, 'commission');
		    	var bonus=grid.getCell(x, 'bonus');
		    	var total=grid.getCell(x, 'total');
		    	var balance=grid.getCell(x, 'balance');
		    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
		    	var agt_comm=grid.getCell(x, 'agt_comm');
		    	var agt_total=grid.getCell(x, 'agt_total');
		    	var company=grid.getCell(x, 'company');
		    	var exchange_rate=grid.getCell(x, 'exchange_rate');
	
		    	
		    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var bonus_convert=((parseFloat(bonus)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
		    	
		    	grid.jqGrid('setCell', x, 3, fltRate[0]);
		    	grid.jqGrid('setCell', x, 6, average_bet_convert);
		    	grid.jqGrid('setCell', x, 7, total_stake_convert);
		    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
		    	grid.jqGrid('setCell', x, 9, win_loss_convert);
		    	grid.jqGrid('setCell', x, 10, commission_convert);
		    	grid.jqGrid('setCell', x, 11, bonus_convert);
		    	grid.jqGrid('setCell', x, 12, total_convert);
		    	grid.jqGrid('setCell', x, 13, balance_convert);
		    	grid.jqGrid('setCell', x, 14, agt_win_loss_convert);
		    	grid.jqGrid('setCell', x, 15, agt_comm_convert);
		    	grid.jqGrid('setCell', x, 16, agt_total_convert);
		    	grid.jqGrid('setCell', x, 17, company_convert);
		    	grid.jqGrid('setCell', x, 18, fltRate[1]);
		     	x++;
		    }//costa_exchange_rate
		    //var labels = document.getElementsByTagName("label");
		    var labels = document.getElementsByClassName("virtua_sum_data");
		    var i=0;
		    var xrate=12;
			for (i=0; i < labels.length; i++) {
				if(i>xrate){
					xrate+=13;
				}
				labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
			}
		}else{
			grid.trigger("reloadGrid");
		}
	}
	</script>
	<script>
	function d_showHTVBettingFooter1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency){
		document.getElementById('d_lblHTVBettingSummaryPlayerID').innerHTML=strAccountID;
		document.getElementById('d_qry_htv999_betting_details_summary_footer1').style.display='block';
		//Current Page
		var grid = jQuery("#d_list5");
		var noRow=$("#d_list5").getGridParam("reccount");
	    var x=1;
	    var bet_amount=0;
    	var win_loss=0;
    	var commission=0;
    	var bonus=0;
    	var total=0;
    	var balance=0;
	    while (x<=noRow)
	    {
	    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
	    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
	    	commission+=parseFloat(grid.getCell(x, 'commission'));
	    	bonus+=parseFloat(grid.getCell(x, 'bonus'));
	    	total+=parseFloat(grid.getCell(x, 'total'));
	    	if(x==1){
	    		balance+=parseFloat(grid.getCell(x, 'balance'));
	    	}
	     	x++;
	    }
	    document.getElementById('lblCPNoOfBet_htv').innerHTML=x-1;
	    document.getElementById('lblCPBetAmount_htv').innerHTML=parseFloat(bet_amount).toFixed(2);
	    document.getElementById('lblCPWinLoss_htv').innerHTML=parseFloat(win_loss).toFixed(2);
	    document.getElementById('lblCPCommission_htv').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('lblCPBonus_htv').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('lblCPTotal_htv').innerHTML=parseFloat(total).toFixed(2);
	    document.getElementById('lblCPBalance_htv').innerHTML=parseFloat(balance).toFixed(2);
	    if(parseFloat(bet_amount)<0){document.getElementById('lblCPBetAmount_htv').style.color='red';}else{document.getElementById('lblCPBetAmount_htv').style.color='black';}
	    if(parseFloat(win_loss)<0){document.getElementById('lblCPWinLoss_htv').style.color='red';}else{document.getElementById('lblCPWinLoss_htv').style.color='black';}
	    if(parseFloat(commission)<0){document.getElementById('lblCPCommission_htv').style.color='red';}else{document.getElementById('lblCPCommission_htv').style.color='black';}
	    if(parseFloat(bonus)<0){document.getElementById('lblCPBonus_htv').style.color='red';}else{document.getElementById('lblCPBonus_htv').style.color='black';}
	    if(parseFloat(total)<0){document.getElementById('lblCPTotal_htv').style.color='red';}else{document.getElementById('lblCPTotal_htv').style.color='black';}
	    if(parseFloat(balance)<0){document.getElementById('lblCPBalance_htv').style.color='red';}else{document.getElementById('lblCPBalance_htv').style.color='black';}
		//All Pages
		
	    jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/HTV999WinLossDetailsSummaryFooter&d_dtFrom_slot=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&d_dtTo_slot=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				var fltTotal = data.split(';');
				document.getElementById('lblAPNoOfBet_htv').innerHTML=fltTotal[0];
			    document.getElementById('lblAPBetAmount_htv').innerHTML=fltTotal[1];
			    document.getElementById('lblAPWinLoss_htv').innerHTML=fltTotal[2];
			    document.getElementById('lblAPCommission_htv').innerHTML=fltTotal[3];
			    document.getElementById('lblAPBonus_htv').innerHTML=fltTotal[4];
			    document.getElementById('lblAPTotal_htv').innerHTML=fltTotal[5];
			    document.getElementById('lblAPBalance_htv').innerHTML=fltTotal[6];
			    if(parseFloat(fltTotal[1])<0){document.getElementById('lblAPBetAmount_htv').style.color='red';}else{document.getElementById('lblAPBetAmount_htv').style.color='black';}
			    if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPWinLoss_htv').style.color='red';}else{document.getElementById('lblAPWinLoss_htv').style.color='black';}
			    if(parseFloat(fltTotal[3])<0){document.getElementById('lblAPCommission_htv').style.color='red';}else{document.getElementById('lblAPCommission_htv').style.color='black';}
			    if(parseFloat(fltTotal[4])<0){document.getElementById('lblAPBonus_htv').style.color='red';}else{document.getElementById('lblAPBonus_htv').style.color='black';}
			    if(parseFloat(fltTotal[5])<0){document.getElementById('lblAPTotal_htv').style.color='red';}else{document.getElementById('lblAPTotal_htv').style.color='black';}
			    if(parseFloat(fltTotal[6])<0){document.getElementById('lblAPBalance_htv').style.color='red';}else{document.getElementById('lblAPBalance_htv').style.color='black';}
	    	}
		});
	}
	function d_showSAVANBettingFooter1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency){
		document.getElementById('d_lblSAVANBettingSummaryPlayerID1').innerHTML=strAccountID;
		document.getElementById('d_qry_savan_vegas_betting_details_summary_footer1').style.display='block';
		//Current Page
		var grid = jQuery("#d_list6_slot");
		var noRow=$("#d_list6_slot").getGridParam("reccount");
	    var x=1;
	    var bet_amount=0;
    	var win_loss=0;
    	var commission=0;
    	var bonus=0;
    	var total=0;
    	var balance=0;
	    while (x<=noRow)
	    {
	    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
	    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
	    	commission+=parseFloat(grid.getCell(x, 'commission'));
	    	bonus+=parseFloat(grid.getCell(x, 'bonus'));
	    	total+=parseFloat(grid.getCell(x, 'total'));
	    	if(x==1){
	    		balance+=parseFloat(grid.getCell(x, 'balance'));
	    	}
	     	x++;
	    }
	    document.getElementById('d_lblCPNoOfBet_savan_slot').innerHTML=x-1;
	    document.getElementById('d_lblCPBetAmount_savan_slot').innerHTML=parseFloat(bet_amount).toFixed(2);
	    document.getElementById('d_lblCPWinLoss_savan_slot').innerHTML=parseFloat(win_loss).toFixed(2);
	    document.getElementById('d_lblCPCommission_savan_slot').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('d_lblCPBonus_savan_slot').innerHTML=parseFloat(bonus).toFixed(2);
	    document.getElementById('d_lblCPTotal_savan_slot').innerHTML=parseFloat(total).toFixed(2);
	    document.getElementById('d_lblCPBalance_savan_slot').innerHTML=parseFloat(balance).toFixed(2);
	    if(parseFloat(bet_amount)<0){document.getElementById('d_lblCPBetAmount_savan_slot').style.color='red';}else{document.getElementById('d_lblCPBetAmount_savan_slot').style.color='black';}
	    //if(parseFloat(amount_wagers)<0){document.getElementById('lblCPValidBet_savan').style.color='red';}else{document.getElementById('lblCPValidBet_savan').style.color='black';}
	    if(parseFloat(win_loss)<0){document.getElementById('d_lblCPWinLoss_savan_slot').style.color='red';}else{document.getElementById('d_lblCPWinLoss_savan_slot').style.color='black';}
	    if(parseFloat(commission)<0){document.getElementById('d_lblCPCommission_savan_slot').style.color='red';}else{document.getElementById('d_lblCPCommission_savan_slot').style.color='black';}
	    if(parseFloat(bonus)<0){document.getElementById('d_lblCPBonus_savan_slot').style.color='red';}else{document.getElementById('d_lblCPBonus_savan_slot').style.color='black';}
	    if(parseFloat(total)<0){document.getElementById('d_lblCPTotal_savan_slot').style.color='red';}else{document.getElementById('d_lblCPTotal_savan_slot').style.color='black';}
	    if(parseFloat(balance)<0){document.getElementById('d_lblCPBalance_savan_slot').style.color='red';}else{document.getElementById('d_lblCPBalance_savan_slot').style.color='black';}
		//All Pages
		
	    jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/SAVANWinLossBettingDetailsSummaryFooter&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				var fltTotal = data.split(';');
				document.getElementById('d_lblAPNoOfBet_savan_slot').innerHTML=fltTotal[0];
			    document.getElementById('d_lblAPBetAmount_savan_slot').innerHTML=fltTotal[1];
			    document.getElementById('d_lblAPWinLoss_savan_slot').innerHTML=fltTotal[2];
			    document.getElementById('d_lblAPCommission_savan_slot').innerHTML=fltTotal[3];
			    document.getElementById('d_lblAPBonus_savan_slot').innerHTML=fltTotal[4];
			    document.getElementById('d_lblAPTotal_savan_slot').innerHTML=fltTotal[5];
			    document.getElementById('d_lblAPBalance_savan_slot').innerHTML=fltTotal[6];
			    if(parseFloat(fltTotal[1])<0){document.getElementById('d_lblAPBetAmount_savan_slot').style.color='red';}else{document.getElementById('d_lblAPBetAmount_savan_slot').style.color='black';}
			    //if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPValidBet_savan').style.color='red';}else{document.getElementById('lblAPValidBet_savan').style.color='black';}
			    if(parseFloat(fltTotal[2])<0){document.getElementById('d_lblAPWinLoss_savan_slot').style.color='red';}else{document.getElementById('d_lblAPWinLoss_savan_slot').style.color='black';}
			    if(parseFloat(fltTotal[3])<0){document.getElementById('d_lblAPCommission_savan_slot').style.color='red';}else{document.getElementById('d_lblAPCommission_savan_slot').style.color='black';}
			    if(parseFloat(fltTotal[4])<0){document.getElementById('d_lblAPBonus_savan_slot').style.color='red';}else{document.getElementById('d_lblAPBonus_savan_slot').style.color='black';}
			    if(parseFloat(fltTotal[5])<0){document.getElementById('d_lblAPTotal_savan_slot').style.color='red';}else{document.getElementById('d_lblAPTotal_savan_slot').style.color='black';}
			    if(parseFloat(fltTotal[6])<0){document.getElementById('d_lblAPBalance_savan_slot').style.color='red';}else{document.getElementById('d_lblAPBalance_savan_slot').style.color='black';}
	    	}
		});
	}
	function d_showVIRTUABettingFooter1(strAccountID,d_dtFrom_slot,d_dtTo_slot,intGame,testCurrency){
		document.getElementById('d_lblVIRTUABettingSummaryPlayerID').innerHTML=strAccountID;
		document.getElementById('d_qry_virtua_vegas_betting_details_summary_footer').style.display='block';
		//Current Page
		var grid = jQuery("#d_list6_slot1");
		var noRow=$("#d_list6_slot1").getGridParam("reccount");
	    var x=1;
	    var bet_amount=0;
    	var win_loss=0;
    	var commission=0;
    	var bonus=0;
    	var total=0;
    	var balance=0;
	    while (x<=noRow)
	    {
	    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
	    	win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
	    	commission+=parseFloat(grid.getCell(x, 'commission'));
	    	bonus+=parseFloat(grid.getCell(x, 'bonus'));
	    	total+=parseFloat(grid.getCell(x, 'total'));
	    	if(x==1){
	    		balance+=parseFloat(grid.getCell(x, 'balance'));
	    	}
	     	x++;
	    }
	    document.getElementById('d_lblCPNoOfBet_virtua').innerHTML=x-1;
	    document.getElementById('d_lblCPBetAmount_virtua').innerHTML=parseFloat(bet_amount).toFixed(2);
	    document.getElementById('d_lblCPWinLoss_virtua').innerHTML=parseFloat(win_loss).toFixed(2);
	    document.getElementById('d_lblCPCommission_virtua').innerHTML=parseFloat(commission).toFixed(2);
	    document.getElementById('d_lblCPBonus_virtua').innerHTML=parseFloat(bonus).toFixed(2);
	    document.getElementById('d_lblCPTotal_virtua').innerHTML=parseFloat(total).toFixed(2);
	    //document.getElementById('lblCPBalance_virtua').innerHTML=parseFloat(balance).toFixed(2);
	    document.getElementById('d_lblCPBalance_virtua').innerHTML='N/A';
	    //document.getElementById('lblAPBalance_virtua').innerHTML=parseFloat(balance).toFixed(2);
	    if(parseFloat(bet_amount)<0){document.getElementById('d_lblCPBetAmount_virtua').style.color='red';}else{document.getElementById('d_lblCPBetAmount_virtua').style.color='black';}
	    //if(parseFloat(amount_wagers)<0){document.getElementById('lblCPValidBet_savan').style.color='red';}else{document.getElementById('lblCPValidBet_savan').style.color='black';}
	    if(parseFloat(win_loss)<0){document.getElementById('d_lblCPWinLoss_virtua').style.color='red';}else{document.getElementById('d_lblCPWinLoss_virtua').style.color='black';}
	    if(parseFloat(commission)<0){document.getElementById('d_lblCPCommission_virtua').style.color='red';}else{document.getElementById('d_lblCPCommission_virtua').style.color='black';}
	    if(parseFloat(bonus)<0){document.getElementById('d_lblCPBonus_virtua').style.color='red';}else{document.getElementById('d_lblCPBonus_virtua').style.color='black';}
	    if(parseFloat(total)<0){document.getElementById('d_lblCPTotal_virtua').style.color='red';}else{document.getElementById('d_lblCPTotal_virtua').style.color='black';}
	    //if(parseFloat(balance)<0){document.getElementById('lblCPBalance_virtua').style.color='red';}else{document.getElementById('lblCPBalance_virtua').style.color='black';}
	    //if(parseFloat(balance)<0){document.getElementById('lblAPBalance_virtua').style.color='red';}else{document.getElementById('lblAPBalance_virtua').style.color='black';}
		//All Pages
		
	    jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSlotWinLoss/VIRTUAWinLossBettingDetailsSummaryFooter&dtFrom=' + d_dtFrom_slot.replace(" ", "_").split('-').join('/') + '&dtTo=' + d_dtTo_slot.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				var fltTotal = data.split(';');
				document.getElementById('d_lblAPNoOfBet_virtua').innerHTML=fltTotal[0];
			    document.getElementById('d_lblAPBetAmount_virtua').innerHTML=fltTotal[1];
			    document.getElementById('d_lblAPWinLoss_virtua').innerHTML=fltTotal[2];
			    document.getElementById('d_lblAPCommission_virtua').innerHTML=fltTotal[3];
			    document.getElementById('d_lblAPBonus_virtua').innerHTML=fltTotal[4];
			    document.getElementById('d_lblAPTotal_virtua').innerHTML=fltTotal[5];
			    document.getElementById('d_lblAPBalance_virtua').innerHTML= (fltTotal[6] != '') ? fltTotal[6] : '0.00';
			    if(parseFloat(fltTotal[1])<0){document.getElementById('d_lblAPBetAmount_virtua').style.color='red';}else{document.getElementById('d_lblAPBetAmount_virtua').style.color='black';}
			    //if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPValidBet_savan').style.color='red';}else{document.getElementById('lblAPValidBet_savan').style.color='black';}
			    if(parseFloat(fltTotal[2])<0){document.getElementById('d_lblAPWinLoss_virtua').style.color='red';}else{document.getElementById('d_lblAPWinLoss_virtua').style.color='black';}
			    if(parseFloat(fltTotal[3])<0){document.getElementById('d_lblAPCommission_virtua').style.color='red';}else{document.getElementById('d_lblAPCommission_virtua').style.color='black';}
			    if(parseFloat(fltTotal[4])<0){document.getElementById('d_lblAPBonus_virtua').style.color='red';}else{document.getElementById('d_lblAPBonus_virtua').style.color='black';}
			    if(parseFloat(fltTotal[5])<0){document.getElementById('d_lblAPTotal_virtua').style.color='red';}else{document.getElementById('d_lblAPTotal_virtua').style.color='black';}
			    if(parseFloat(fltTotal[6])<0){document.getElementById('d_lblAPBalance_virtua').style.color='red';}else{document.getElementById('d_lblAPBalance_virtua').style.color='black';}
	    	}
		});
	}
	</script>
</head>
<body>
	<div id="parameter_area">
		<div class="header"><font color="#f0e62b"><?php echo Yii::app()->session['account_id'];?></font> <?php echo Yii::t('agent','agent.agentwinloss.agent_slot_winloss');?></div>
		<div class="body">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.game');?></td>
			<td class="right">
				<select name="d_cmbGame_slot" id="d_cmbGame_slot" class="cmb">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. strtoupper($row['game_name']) . '</option>';
					}
					?>
				</select>
			</td></tr>
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.currency');?></td>
			<td class="right">
				<select name="d_cmbCurrency_slot" id="d_cmbCurrency_slot" class="cmb" onchange="if(this.value!=''){document.getElementById('d_chkTestCurrency_slot').style.display='none';document.getElementById('lblTestCurrency1').style.display='none';}else{document.getElementById('d_chkTestCurrency_slot').style.display='inline';document.getElementById('lblTestCurrency1').style.display='inline';}">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableCurrency::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['currency_name'] . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>
				<input type="checkbox" id="d_chkTestCurrency_slot" checked/><label id="lblTestCurrency1" for="d_chkTestCurrency_slot"><?php echo Yii::t('agent','agent.agentwinloss.except_test_currency');?></label>
			</td></tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.from');?></td>
				<td class="right">
					<input type="text" id="d_dtFrom_slot" value="<?php echo date("Y-m-d");?>">
					<select id="d_cmbTime1_slot">
					<?php 
						for($i=0;$i<=23;$i++){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					?>
					</select> :00:00
				</td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.to');?></td>
				<td class="right">
					<input type="text" id="d_dtTo_slot" value="<?php echo date("Y-m-d");?>">
					<select id="d_cmbTime2_slot">
					<?php 
						for($i=0;$i<=23;$i++){
							if($i<23){
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}else{
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}
						}
					?> 
					</select> :59:59
				</td>
			</tr>
			<tr>
				<td class="left">Account ID</td>
				<td class="right"><input type="text" id="d_txtAccountID_slot"></td>
			</tr>
			<tr>
				<td class="left">Casino</td>
				<?php
				$read_virtua = (Yii::app()->user->checkAccess('agent.readVirtuaWinLoss')==1 || User::getUserType() === Constants::ROLE_AGENT) ? 'style="display:inline"' : 'style="display:none"';  
				$read_hatien = (Yii::app()->user->checkAccess('agent.readHTVSlotWinLoss')==1 || User::getUserType() === Constants::ROLE_AGENT) ? 'style="display:inline"' : 'style="display:none"';
				$read_savan = (Yii::app()->user->checkAccess('agent.readSavanSlotWinLoss')==1 || User::getUserType() === Constants::ROLE_AGENT) ? 'style="display:inline"' : 'style="display:none"';
				?>
				<td class="right">
					<?php echo '<input type="hidden" id="d_lblAgentID_htv1"><input type="hidden" id="d_lblDateFrom_htv1"><input type="hidden" id="d_lblDateTo_htv1"><input type="hidden" id="d_lblDateTo_htv1">';?>
					<input type="checkbox" id="d_chkHTV_slot" <?php echo $read_hatien;?> disabled><label <?php echo $read_hatien;?> id="d_lblchkHTV_slot" for="d_chkHTV_slot">Hatienvegas999</label> 
					<input type="checkbox" id="d_chkSAVAN_slot" checked <?php echo $read_savan;?>><label <?php echo $read_savan;?> id="d_lblchkSAVAN_slot" for="d_chkSAVAN_slot">Savanvegas999</label> 
					<input type="checkbox" id="d_chkVIRTUA_slot" checked <?php echo $read_virtua;?>><label <?php echo $read_virtua;?> id="d_lblchkVIRTUA_slot" for="d_chkVIRTUA_slot">Virtuavegas999</label>
				</td>
			</tr>
			</table>
			<div class="footer"><input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.submit');?>" onclick="javascript: d_showDetails_savan1('');d_showDetails_virtua1('');"> <input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.yesterday');?>" onclick="javascript: d_btnYesterday1(); d_clearContainer1();"> <input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.today');?>" onclick="javascript: d_btnToDay1(); d_clearContainer1();"></div>
		</div>
	</div>
	<div id="agent_winloss_body">
		<div id="dv_hatienvegas1">
	    	<div id="d_qry_htv999_total1"></div>
	    	<div id="d_qry_htv999_result1"></div>
	    	<div id="d_qry_htv999_result_details1"></div>
	    	<div id="d_qry_htv999_betting_details1"></div>
	    	<div id="d_qry_htv999_betting_details_summary_footer1" style="display: none;">
	    		<div id="betting_footer_header"><label id="d_lblHTVBettingSummaryPlayerID"></label> HTV BETTING HISTORY SUMMARY</div>
	    		<table id="betting_footer" cellpadding="5" cellspacing="0">
	    		<tr>
	    			<td class="theader">Type</td>
	    			<td class="theader">No. of Bet</td>
	    			<td class="theader">Bet Amount</td>
	    			<td class="theader">Win/Loss</td>
	    			<td class="theader">Commission</td>
	    			<td class="theader">Jackpot</td>
	    			<td class="theader">Total</td>
	    			<td class="theader">Balance</td>
	    		</tr>
	    		<tr>
	    			<td>Current Page</td>
	    			<td><label id="lblCPNoOfBet_htv">0</label></td>
	    			<td><label id="lblCPBetAmount_htv">0</label></td>
	    			<td><label id="lblCPWinLoss_htv">0</label></td>
	    			<td><label id="lblCPCommission_htv">0</label></td>
	    			<td><label id="lblCPBonus_htv">0</label></td>
	    			<td><label id="lblCPTotal_htv">0</label></td>
	    			<td><label id="lblCPBalance_htv">0</label></td>
	    		</tr>
	    		<tr>
	    			<td>All Page</td>
	    			<td><label id="lblAPNoOfBet_htv">0</label></td>
	    			<td><label id="lblAPBetAmount_htv">0</label></td>
	    			<td><label id="lblAPWinLoss_htv">0</label></td>
	    			<td><label id="lblAPCommission_htv">0</label></td>
	    			<td><label id="lblAPBonus_htv">0</label></td>
	    			<td><label id="lblAPTotal_htv">0</label></td>
	    			<td><label id="lblAPBalance_htv">0</label></td>
	    		</tr>
	    		</table>
	    	</div>
	    </div>
	    <div id="dv_savanvegas1">
	    	<div id="d_qry_savan_vegas_total1"></div>
	    	<div id="d_qry_savan_vegas_result1"></div>
	    	<div id="d_qry_savan_vegas_result_details1"></div>
	    	<div id="d_qry_savan_vegas_betting_details1"></div>
		    <div id="d_qry_savan_vegas_betting_details_summary_footer1" style="display: none;">
	    		<div id="betting_footer_header"><label id="d_lblSAVANBettingSummaryPlayerID1"></label> SAVANVEGAS BETTING HISTORY SUMMARY</div>
	    		<table id="betting_footer" cellpadding="5" cellspacing="0">
	    		<tr>
	    			<td class="theader">Type</td>
	    			<td class="theader">No. of Bet</td>
	    			<td class="theader">Bet Amount</td>
	    			<td class="theader">Win/Loss</td>
	    			<td class="theader">Commission</td>
	    			<td class="theader">Jackpot</td>
	    			<td class="theader">Total</td>
	    			<td class="theader">Balance</td>
	    		</tr>
	    		<tr>
	    			<td>Current Page</td>
	    			<td><label id="d_lblCPNoOfBet_savan_slot">0</label></td>
	    			<td><label id="d_lblCPBetAmount_savan_slot">0</label></td>
	    			<td><label id="d_lblCPWinLoss_savan_slot">0</label></td>
	    			<td><label id="d_lblCPCommission_savan_slot">0</label></td>
	    			<td><label id="d_lblCPBonus_savan_slot">0</label></td>
	    			<td><label id="d_lblCPTotal_savan_slot">0</label></td>
	    			<td><label id="d_lblCPBalance_savan_slot">0</label></td>
	    		</tr>
	    		<tr>
	    			<td>All Page</td>
	    			<td><label id="d_lblAPNoOfBet_savan_slot">0</label></td>
	    			<td><label id="d_lblAPBetAmount_savan_slot">0</label></td>
	    			<td><label id="d_lblAPWinLoss_savan_slot">0</label></td>
	    			<td><label id="d_lblAPCommission_savan_slot">0</label></td>
	    			<td><label id="d_lblAPBonus_savan_slot">0</label></td>
	    			<td><label id="d_lblAPTotal_savan_slot">0</label></td>
	    			<td><label id="d_lblAPBalance_savan_slot">0</label></td>
	    		</tr>
	    		</table>
	    	</div>
	    </div>
	    <br/>
	    <div id="dv_virtuavegas">
	    	<div id="d_qry_virtua_vegas_total"></div>
	    	<div id="d_qry_virtua_vegas_result"></div>
	    	<div id="d_qry_virtua_vegas_result_details"></div>
	    	<div id="d_qry_virtua_vegas_betting_details"></div>
		    <div id="d_qry_virtua_vegas_betting_details_summary_footer" style="display: none;">
	    		<div id="betting_footer_header"><label id="d_lblVIRTUABettingSummaryPlayerID"></label> VIRTUAVEGAS BETTING HISTORY SUMMARY</div>
	    		<table id="betting_footer" cellpadding="5" cellspacing="0">
	    		<tr>
	    			<td class="theader">Type</td>
	    			<td class="theader">No. of Bet</td>
	    			<td class="theader">Bet Amount</td>
	    			<td class="theader">Win/Loss</td>
	    			<td class="theader">Commission</td>
	    			<td class="theader">Jackpot</td>
	    			<td class="theader">Total</td>
	    			<td class="theader">Balance</td>
	    		</tr>
	    		<tr>
	    			<td>Current Page</td>
	    			<td><label id="d_lblCPNoOfBet_virtua">0</label></td>
	    			<td><label id="d_lblCPBetAmount_virtua">0</label></td>
	    			<td><label id="d_lblCPWinLoss_virtua">0</label></td>
	    			<td><label id="d_lblCPCommission_virtua">0</label></td>
	    			<td><label id="d_lblCPBonus_virtua">0</label></td>
	    			<td><label id="d_lblCPTotal_virtua">0</label></td>
	    			<td><label id="d_lblCPBalance_virtua">0</label></td>
	    		</tr>
	    		<tr>
	    			<td>All Page</td>
	    			<td><label id="d_lblAPNoOfBet_virtua">0</label></td>
	    			<td><label id="d_lblAPBetAmount_virtua">0</label></td>
	    			<td><label id="d_lblAPWinLoss_virtua">0</label></td>
	    			<td><label id="d_lblAPCommission_virtua">0</label></td>
	    			<td><label id="d_lblAPBonus_virtua">0</label></td>
	    			<td><label id="d_lblAPTotal_virtua">0</label></td>
	    			<td><label id="d_lblAPBalance_virtua">0</label></td> 
	    		</tr>
	    		</table>
	    	</div>
	    </div>
	</div>
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
	</form>
</body>
</html>