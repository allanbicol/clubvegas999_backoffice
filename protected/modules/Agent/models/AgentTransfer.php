<?php
/**
 * @todo AgentTransfer Model
 * @copyright CE
 * @author Allan
 * @since 2012-06-14
 */
class AgentTransfer
{
	public function makeAgentTransfer()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		
		if ($_POST['task']=='getUpperAgentRecord')
		{
			if (Yii::app()->session['account_id']!=null){
				if ($_POST['level']=='A'){
					$id=substr($_POST['AccountID'], 0,6);
				}elseif($_POST['level']=='AM'){
					$id=substr($_POST['AccountID'], 0,4);
				}elseif($_POST['level']=='SM'){
					$id=substr($_POST['AccountID'], 0,2);
				}elseif($_POST['level']=='SC'){
					$command= $connection->createCommand("SELECT company_id from tbl_main_company");
					$rd=$command->queryRow();
					$id=$rd['company_id'];
				}
				if ($_POST['level']=='SC'){
					$command= $connection->createCommand("SELECT balance,(balance - credit) as winloss,account_id,currency_id from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
					$rd=$command->queryRow();
					$currencyA=$rd['currency_id'];
					$winloss=$rd['winloss'];
					
					$commandC = $connection->createCommand("SELECT currency_id,balance,credit from tbl_main_company  WHERE company_id='".$id."'");
					$rdC=$commandC->queryRow();
					$balanceA=$rdC['balance'];
					$creditA=$rdC['credit'];
					$currencyUA=$rdC['currency_id'];
							
					//get exchange rate for the agent
					$commandCurrency1=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyA . "'");
					$rdCurrencyA=$commandCurrency1->queryRow();
					$rateA=$rdCurrencyA['exchange_rate'];
					//get exchange rate for the upper agent
					$commandCurrency2=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyUA . "'");
					$rdCurrencyUA=$commandCurrency2->queryRow();
					$rateUA=$rdCurrencyUA['exchange_rate'];
					
					$convertedBalance=($balanceA/$rateUA)*$rateA;
					$convertedCredit=($creditA/$rateUA)*$rateA;
					
					$command= $connection->createCommand("SELECT market_type from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
					$rd=$command->queryRow();
					if ($rd['market_type']==1){
						echo round($convertedBalance,2)."#".round($winloss,2)."#ClubVegas999#".Yii::t('agent','agent.subcompanylist.upperbalance');
					}else{
						echo round($convertedCredit,2)."#".round($winloss,2)."#ClubVegas999#".Yii::t('agent','agent.subcompanylist.uppercredit');
					}
				}else{
	
					$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
					$command1= $connection->createCommand("SELECT (balance - credit) as winloss,market_type from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
					$rd=$command->queryRow();
					$rd1=$command1->queryRow();
					if ($rd1['market_type']==1){
						echo $rd['balance'] . "#" . $rd1['winloss']."#".$id."#".Yii::t('agent','agent.subcompanylist.upperbalance');
					}else{
						echo $rd['credit'] . "#" . $rd1['winloss']."#".$id."#".Yii::t('agent','agent.subcompanylist.uppercredit');
					}
				}
			}else{
				echo 'die#die#die';
				//header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
			}
		}
		
		if ($_POST['task']=='AgentDepositConfirm')
		{
			$commandGetMarketType= $connection->createCommand("SELECT market_type from tbl_agent WHERE account_id='" . $_POST['AccountID'] . "'");
			$rdMarketType=$commandGetMarketType->queryRow();
			if($rdMarketType['market_type']==1){
				AgentTransfer::depositCreditMarket();
			}else{
				AgentTransfer::depositCashMarket();
			}
		}
		
		if ($_POST['task']=='AgentWithdrawConfirm')
		{
			$commandGetMarketType= $connection->createCommand("SELECT market_type from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rdMarketType=$commandGetMarketType->queryRow();
			if($rdMarketType['market_type']==1){
				AgentTransfer::withdrawCreditMarket();
			}else{
				AgentTransfer::withdrawCashMarket();
			}
		}
		
		if($_POST['task']=='checkbalance')
		{

			$command= $connection->createCommand("SELECT (balance - credit) as winloss,account_id,currency_id from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd=$command->queryRow();
			$winloss=$rd['winloss'];
		
			echo $rd['winloss']."#".$rd['account_id'];
			
		}
	}
	
	function depositCreditMarket(){
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		
		if ($_POST['level']=='A'){
			$id=substr($_POST['AccountID'], 0,6);
			$label='Agent';
			$labelU='Master Agent';
		}elseif($_POST['level']=='AM'){
			$id=substr($_POST['AccountID'], 0,4);
			$label='Master Agent';
			$labelU='Senior Master';
		}elseif($_POST['level']=='SM'){
			$id=substr($_POST['AccountID'], 0,2);
			$label='Senior Master';
			$labelU='Sub Company';
		}elseif($_POST['level']=='SC'){
			$commandC= $connection->createCommand("SELECT company_id from tbl_main_company");
			$rd=$commandC->queryRow();
			$id=$rd['company_id'];
			$label='Sub Company';
			$labelU='Main Company';
		}
			
			
		if ($_POST['level']=='SC'){
		
			$command = $connection->createCommand("SELECT balance,credit,currency_id from tbl_main_company  WHERE company_id='" . $id . "'");
			$rd=$command->queryRow();
			$currencyUA=$rd['currency_id'];
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
		
			$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd1=$command1->queryRow();
			$agentBalance= $rd1['balance'];
			$agentCredit= $rd1['credit'];
			$currencyA= $rd1['currency_id'];
		
			//get exchange rate for the agent
			$commandCurrency1=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyA . "'");
			$rdCurrencyA=$commandCurrency1->queryRow();
			$rateA=$rdCurrencyA['exchange_rate'];
			//get exchange rate for the upper agent
			$commandCurrency2=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyUA . "'");
			$rdCurrencyUA=$commandCurrency2->queryRow();
			$rateUA=$rdCurrencyUA['exchange_rate'];
			$amount=$_POST['amountDeposit'];
			$convertedAmount=($amount/$rateA)*$rateUA;
		
			$newUpperAgentBalance=$upperAgentBalance - $convertedAmount;
			$newAgentBalance= $agentBalance + $amount;
		
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
			$winloss=(($rd2['winloss'])*-1);
			if  ($_POST['amountDeposit'] > $winloss){
				echo  Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
				
			if  ($_POST['amountDeposit'] > $upperAgentBalance){
				echo Yii::t('agent','agent.subcompanylist.deposithigher');
				exit;
			}
			//update upper agent  balance
			TableMainCompany::model()->updateAll(array('balance'=>$newUpperAgentBalance),'company_id="'.$id.'"');
		
			//update agent  balance
			TableAgent::model()->updateAll(array('balance'=>$newAgentBalance),'account_id="'.$_POST['AccountID'].'"');
		}else{
			//update upper agent balance
			$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
			$rd=$command->queryRow();
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
			$newUpperAgentBalance=$upperAgentBalance - $_POST['amountDeposit'];
		
		
			//update agent  balance
			$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd1=$command1->queryRow();
			$agentBalance= $rd1['balance'];
			$agentCredit= $rd1['credit'];
			$currencyA= $rd1['currency_id'];
			$newAgentBalance= $agentBalance + $_POST['amountDeposit'];
		
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
				
			$winloss=(($rd2['winloss'])*-1);
			if  ($_POST['amountDeposit'] > $winloss){
				echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
				
			if  ($_POST['amountDeposit'] > $upperAgentBalance){
				echo Yii::t('agent','agent.subcompanylist.deposithigher');
				exit;
			}
		
			TableAgent::model()->updateAll(array('balance'=>$newUpperAgentBalance),'account_id="'.$id.'"');
			TableAgent::model()->updateAll(array('balance'=>$newAgentBalance),'account_id="'.$_POST['AccountID'].'"');
		}
			
		//Save to Agent and upper agent transaction history
		//$postDepositUA =new AgentSaveTransactionHistory;
		$postDepositA=new AgentSaveTransactionHistory;
		$postLog=new TableLog;
		$postLog1=new TableLog;
			
		$account_id=Yii::app()->session['account_id'];
			
		$transNum= TransactionNumber::generateTransactionNumber("MD");
			
		$postDepositA->transaction_number=$transNum."A";
		$postDepositA->agent_account_id=$_POST['AccountID'];
		$postDepositA->currency_id=$currencyA;
		$postDepositA->trans_type_id=2;
		$postDepositA->amount= $_POST['amountDeposit'];
		$postDepositA->market_type= 'Credit Market';
		$postDepositA->balance_before=$agentBalance;
		$postDepositA->balance_after=$newAgentBalance;
		$postDepositA->credit_before=$agentCredit;
		$postDepositA->credit_after=$agentCredit;
		$postDepositA->upper_agent_balance_before=$upperAgentBalance;
		$postDepositA->upper_agent_balance_after=$newUpperAgentBalance;
		$postDepositA->upper_agent_credit_before=$upperAgentCredit;
		$postDepositA->upper_agent_credit_after=$upperAgentCredit;
		$postDepositA->transaction_date=$dateTime;
		$postDepositA->operator_id=$account_id;
		
		$postLog->operated_by=$account_id;
		$postLog->operated_by_level=$level;
		$postLog->operated=$_POST['AccountID'];
		$postLog->operated_level=$label;
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=3;
		$postLog->log_details='<b>'.$label.' <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Deposit:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label></b>';
			
		$postLog1->operated_by=$account_id;
		$postLog1->operated_by_level=$level;
		$postLog1->operated=$id;
		$postLog1->operated_level=$labelU;
		$postLog1->operation_time=$dateTime;
		$postLog1->log_type_id=3;
		$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Send:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label> to '.$_POST['AccountID'].'</b>';
			
		//$postDepositUA->save();
		$postDepositA->save();
		$postLog->save();
		$postLog1->save();
			
		echo Yii::t('agent','agent.subcompanylist.depositcomplete');
	}
	
	function withdrawCreditMarket()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		
		if ($_POST['level']=='A'){
			$id=substr($_POST['AccountID'], 0,6);
			$label='Agent';
			$labelU='Master Agent';
		}elseif($_POST['level']=='AM'){
			$id=substr($_POST['AccountID'], 0,4);
			$label='Master Agent';
			$labelU='Senior Master';
		}elseif($_POST['level']=='SM'){
			$id=substr($_POST['AccountID'], 0,2);
			$label='Senior Master';
			$labelU='Sub Company';
		}elseif($_POST['level']=='SC'){
			$command= $connection->createCommand("SELECT company_id from tbl_main_company");
			$rd=$command->queryRow();
			$id=$rd['company_id'];
			$label='Sub Company';
			$labelU='Main Company';
		}
		
		$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
		$rd1=$command1->queryRow();
			
		//update agent  balance
		$agentBalance= $rd1['balance'];
		$agentCredit= $rd1['credit'];
		$currencyA= $rd1['currency_id'];
		$newAgentBalance= $agentBalance - $_POST['amountWithdraw'];
			
			
		//update upper agent  balance
		if ($_POST['level']=='SC'){
			$command = $connection->createCommand("SELECT balance,credit,currency_id from tbl_main_company  WHERE company_id='" . $id . "'");
			$rd=$command->queryRow();
			$currencyUA=$rd['currency_id'];
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
			//get exchange rate for the agent
			$commandCurrency1=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyA . "'");
			$rdCurrencyA=$commandCurrency1->queryRow();
			$rateA=$rdCurrencyA['exchange_rate'];
			//get exchange rate for the upper agent
			$commandCurrency2=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyUA . "'");
			$rdCurrencyUA=$commandCurrency2->queryRow();
			$rateUA=$rdCurrencyUA['exchange_rate'];
				
			$amount=$_POST['amountWithdraw'];
			$convertedAmount=($amount/$rateA)*$rateUA;
				
			$newUpperAgentBalance=$upperAgentBalance + $convertedAmount;
				
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
		
			if  ($_POST['amountWithdraw'] > $rd2['winloss']){
				echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
				
			TableMainCompany::model()->updateAll(array('balance'=>$newUpperAgentBalance),'company_id="'.$id.'"');
		}else{
			$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
			$rd=$command->queryRow();
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
			$newUpperAgentBalance=$upperAgentBalance + $_POST['amountWithdraw'];
		
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
				
			if  ($_POST['amountWithdraw'] > $rd2['winloss']){
				echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
		
			TableAgent::model()->updateAll(array('balance'=>$newUpperAgentBalance),'account_id="'.$id.'"');
		}
			
		
		TableAgent::model()->updateAll(array('balance'=>$newAgentBalance),'account_id="'.$_POST['AccountID'].'"');
		//Save to Agent and upper agent transaction history
		//$postDepositUA =new AgentSaveTransactionHistory;
		$postDepositA=new AgentSaveTransactionHistory;
		$postLog=new TableLog;
		$postLog1=new TableLog;
			
		$account_id=Yii::app()->session['account_id'];
			
		$transNum= TransactionNumber::generateTransactionNumber("MW");
			
		$postDepositA->transaction_number=$transNum."A";
		$postDepositA->agent_account_id=$_POST['AccountID'];
		$postDepositA->currency_id=$currencyA;
		$postDepositA->trans_type_id=4;
		$postDepositA->amount= $_POST['amountWithdraw'];
		$postDepositA->market_type= 'Credit Market';
		$postDepositA->balance_before=$agentBalance;
		$postDepositA->balance_after=$newAgentBalance;
		$postDepositA->credit_before=$agentCredit;
		$postDepositA->credit_after=$agentCredit;
		$postDepositA->upper_agent_balance_before=$upperAgentBalance;
		$postDepositA->upper_agent_balance_after=$newUpperAgentBalance;
		$postDepositA->upper_agent_credit_before=$upperAgentCredit;
		$postDepositA->upper_agent_credit_after=$upperAgentCredit;
		$postDepositA->transaction_date=$dateTime;
		$postDepositA->operator_id=$account_id;
		
		$postLog->operated_by=$account_id;
		$postLog->operated_by_level=$level;
		$postLog->operated=$_POST['AccountID'];
		$postLog->operated_level=$label;
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=2;
		$postLog->log_details='<b>Credit Market : '.$label.' <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Withdraw:<label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label></b>';
			
		$postLog1->operated_by=$account_id;
		$postLog1->operated_by_level=$level;
		$postLog1->operated=$id;
		$postLog1->operated_level=$labelU;
		$postLog1->operation_time=$dateTime;
		$postLog1->log_type_id=2;
		$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Recieved <label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label> from Credit Market '.$_POST['AccountID'].'</b>';
			
		//$postDepositUA->save();
		$postDepositA->save();
		$postLog->save();
		$postLog1->save();
			
		echo Yii::t('agent','agent.subcompanylist.withdrawcomplete');
	}
	
	
	function depositCashMarket(){
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
	
		if ($_POST['level']=='A'){
			$id=substr($_POST['AccountID'], 0,6);
			$label='Agent';
			$labelU='Master Agent';
		}elseif($_POST['level']=='AM'){
			$id=substr($_POST['AccountID'], 0,4);
			$label='Master Agent';
			$labelU='Senior Master';
		}elseif($_POST['level']=='SM'){
			$id=substr($_POST['AccountID'], 0,2);
			$label='Senior Master';
			$labelU='Sub Company';
		}elseif($_POST['level']=='SC'){
			$commandC= $connection->createCommand("SELECT company_id from tbl_main_company");
			$rd=$commandC->queryRow();
			$id=$rd['company_id'];
			$label='Sub Company';
			$labelU='Main Company';
		}
			
			
		if ($_POST['level']=='SC'){
	
			$command = $connection->createCommand("SELECT balance,credit,currency_id from tbl_main_company  WHERE company_id='" . $id . "'");
			$rd=$command->queryRow();
			$currencyUA=$rd['currency_id'];
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
	
			$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd1=$command1->queryRow();
			$agentBalance= $rd1['balance'];
			$agentCredit= $rd1['credit'];
			$currencyA= $rd1['currency_id'];
	
			//get exchange rate for the agent
			$commandCurrency1=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyA . "'");
			$rdCurrencyA=$commandCurrency1->queryRow();
			$rateA=$rdCurrencyA['exchange_rate'];
			//get exchange rate for the upper agent
			$commandCurrency2=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyUA . "'");
			$rdCurrencyUA=$commandCurrency2->queryRow();
			$rateUA=$rdCurrencyUA['exchange_rate'];
			$amount=$_POST['amountDeposit'];
			$convertedAmount=($amount/$rateA)*$rateUA;
	
			$newUpperAgentCredit=$upperAgentCredit + $convertedAmount;
			$newAgentCredit= $agentCredit - $amount;
	
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
			$winloss=(($rd2['winloss'])*-1);
			if  ($_POST['amountDeposit'] > $winloss){
				echo  Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
	
			if  ($_POST['amountDeposit'] > $upperAgentCredit){
				echo Yii::t('agent','agent.subcompanylist.deposithigher');
				exit;
			}
			//update upper agent  balance
			TableMainCompany::model()->updateAll(array('credit'=>$newUpperAgentCredit),'company_id="'.$id.'"');
	
			//update agent  balance
			TableAgent::model()->updateAll(array('credit'=>$newAgentCredit),'account_id="'.$_POST['AccountID'].'"');
		}else{
			//update upper agent balance
			$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
			$rd=$command->queryRow();
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
			$newUpperAgentCredit=$upperAgentCredit + $_POST['amountDeposit'];
	
	
			//update agent  balance
			$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd1=$command1->queryRow();
			$agentBalance= $rd1['balance'];
			$agentCredit= $rd1['credit'];
			$currencyA= $rd1['currency_id'];
			$newAgentCredit= $agentCredit - $_POST['amountDeposit'];
	
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
	
			$winloss=(($rd2['winloss'])*-1);
			if  ($_POST['amountDeposit'] > $winloss){
				echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
	
			if  ($_POST['amountDeposit'] > $upperAgentCredit){
				echo Yii::t('agent','agent.subcompanylist.deposithigher');
				exit;
			}
	
			TableAgent::model()->updateAll(array('credit'=>$newUpperAgentCredit),'account_id="'.$id.'"');
			TableAgent::model()->updateAll(array('credit'=>$newAgentCredit),'account_id="'.$_POST['AccountID'].'"');
		}
			
		//Save to Agent and upper agent transaction history
		//$postDepositUA =new AgentSaveTransactionHistory;
		$postDepositA=new AgentSaveTransactionHistory;
		$postLog=new TableLog;
		$postLog1=new TableLog;
			
		$account_id=Yii::app()->session['account_id'];
			
		$transNum= TransactionNumber::generateTransactionNumber("MD");
			
		$postDepositA->transaction_number=$transNum."A";
		$postDepositA->agent_account_id=$_POST['AccountID'];
		$postDepositA->currency_id=$currencyA;
		$postDepositA->trans_type_id=2;
		$postDepositA->amount= $_POST['amountDeposit'];
		$postDepositA->market_type= 'Cash Market';
		$postDepositA->balance_before=$agentBalance;
		$postDepositA->balance_after=$agentBalance;
		$postDepositA->credit_before=$agentCredit;
		$postDepositA->credit_after=$newAgentCredit;
		$postDepositA->upper_agent_balance_before=$upperAgentBalance;
		$postDepositA->upper_agent_balance_after=$upperAgentBalance;
		$postDepositA->upper_agent_credit_before=$upperAgentCredit;
		$postDepositA->upper_agent_credit_after=$newUpperAgentCredit;
		$postDepositA->transaction_date=$dateTime;
		$postDepositA->operator_id=$account_id;
	
		$postLog->operated_by=$account_id;
		$postLog->operated_by_level=$level;
		$postLog->operated=$_POST['AccountID'];
		$postLog->operated_level=$label;
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=3;
		$postLog->log_details='<b>Cash Market : '.$label.' <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Deposit:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label></b>';
			
		$postLog1->operated_by=$account_id;
		$postLog1->operated_by_level=$level;
		$postLog1->operated=$id;
		$postLog1->operated_level=$labelU;
		$postLog1->operation_time=$dateTime;
		$postLog1->log_type_id=3;
		$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Send:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label> to Cash Market '.$_POST['AccountID'].'</b>';
			
		//$postDepositUA->save();
		$postDepositA->save();
		$postLog->save();
		$postLog1->save();
			
		echo Yii::t('agent','agent.subcompanylist.depositcomplete');
	}
	
	function withdrawCashMarket()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
	
		if ($_POST['level']=='A'){
			$id=substr($_POST['AccountID'], 0,6);
			$label='Agent';
			$labelU='Master Agent';
		}elseif($_POST['level']=='AM'){
			$id=substr($_POST['AccountID'], 0,4);
			$label='Master Agent';
			$labelU='Senior Master';
		}elseif($_POST['level']=='SM'){
			$id=substr($_POST['AccountID'], 0,2);
			$label='Senior Master';
			$labelU='Sub Company';
		}elseif($_POST['level']=='SC'){
			$command= $connection->createCommand("SELECT company_id from tbl_main_company");
			$rd=$command->queryRow();
			$id=$rd['company_id'];
			$label='Sub Company';
			$labelU='Main Company';
		}
	
		$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
		$rd1=$command1->queryRow();
			
		//update agent  balance
		$agentBalance= $rd1['balance'];
		$agentCredit= $rd1['credit'];
		$currencyA= $rd1['currency_id'];
		$newAgentCredit= $agentCredit+ $_POST['amountWithdraw'];
			
			
		//update upper agent  balance
		if ($_POST['level']=='SC'){
			$command = $connection->createCommand("SELECT balance,credit,currency_id from tbl_main_company  WHERE company_id='" . $id . "'");
			$rd=$command->queryRow();
			$currencyUA=$rd['currency_id'];
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
			//get exchange rate for the agent
			$commandCurrency1=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyA . "'");
			$rdCurrencyA=$commandCurrency1->queryRow();
			$rateA=$rdCurrencyA['exchange_rate'];
			//get exchange rate for the upper agent
			$commandCurrency2=$connection->createCommand("SELECT exchange_rate from tbl_currency  WHERE id='" . $currencyUA . "'");
			$rdCurrencyUA=$commandCurrency2->queryRow();
			$rateUA=$rdCurrencyUA['exchange_rate'];
	
			$amount=$_POST['amountWithdraw'];
			$convertedAmount=($amount/$rateA)*$rateUA;
	
			$newUpperAgentCredit=$upperAgentCredit - $convertedAmount;
	
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
	
			if  ($_POST['amountWithdraw'] > $rd2['winloss']){
				echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
	
			TableMainCompany::model()->updateAll(array('credit'=>$newUpperAgentCredit),'company_id="'.$id.'"');
		}else{
			$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
			$rd=$command->queryRow();
			$upperAgentBalance=$rd['balance'];
			$upperAgentCredit=$rd['credit'];
			$newUpperAgentCredit=$upperAgentCredit - $_POST['amountWithdraw'];
	
			$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent  WHERE account_id='" . $_POST['AccountID'] . "'");
			$rd2=$command2->queryRow();
	
			if  ($_POST['amountWithdraw'] > $rd2['winloss']){
				echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
				exit;
			}
	
			TableAgent::model()->updateAll(array('credit'=>$newUpperAgentCredit),'account_id="'.$id.'"');
		}
			
	
		TableAgent::model()->updateAll(array('credit'=>$newAgentCredit),'account_id="'.$_POST['AccountID'].'"');
		//Save to Agent and upper agent transaction history
		//$postDepositUA =new AgentSaveTransactionHistory;
		$postDepositA=new AgentSaveTransactionHistory;
		$postLog=new TableLog;
		$postLog1=new TableLog;
			
		$account_id=Yii::app()->session['account_id'];
			
		$transNum= TransactionNumber::generateTransactionNumber("MW");
			
		$postDepositA->transaction_number=$transNum."A";
		$postDepositA->agent_account_id=$_POST['AccountID'];
		$postDepositA->currency_id=$currencyA;
		$postDepositA->trans_type_id=4;
		$postDepositA->amount= $_POST['amountWithdraw'];
		$postDepositA->market_type= 'Cash Market';
		$postDepositA->balance_before=$agentBalance;
		$postDepositA->balance_after=$agentBalance;
		$postDepositA->credit_before=$agentCredit;
		$postDepositA->credit_after=$newAgentCredit;
		$postDepositA->upper_agent_balance_before=$upperAgentBalance;
		$postDepositA->upper_agent_balance_after=$upperAgentBalance;
		$postDepositA->upper_agent_credit_before=$upperAgentCredit;
		$postDepositA->upper_agent_credit_after=$newUpperAgentCredit;
		$postDepositA->transaction_date=$dateTime;
		$postDepositA->operator_id=$account_id;
	
		$postLog->operated_by=$account_id;
		$postLog->operated_by_level=$level;
		$postLog->operated=$_POST['AccountID'];
		$postLog->operated_level=$label;
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=2;
		$postLog->log_details='<b>Cash Market : '.$label.' <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Withdraw:<label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label></b>';
			
		$postLog1->operated_by=$account_id;
		$postLog1->operated_by_level=$level;
		$postLog1->operated=$id;
		$postLog1->operated_level=$labelU;
		$postLog1->operation_time=$dateTime;
		$postLog1->log_type_id=2;
		$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Recieved <label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label> from Cash Market'.$_POST['AccountID'].'</b>';
			
		//$postDepositUA->save();
		$postDepositA->save();
		$postLog->save();
		$postLog1->save();
			
		echo Yii::t('agent','agent.subcompanylist.withdrawcomplete');
	}
	
}