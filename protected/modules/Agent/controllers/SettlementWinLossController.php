<?php
/**
 * @todo AgentWinLossController
* @copyright CE
* @author Leo karl
* @since 2012-05-28
*/
class SettlementWinLossController extends MyController
{
	/**
	 * @todo Export Grid data to excel
	 * @author leokarl
	 * @since 2013-01-24
	 */
	public function actionExportToExcel(){
		if(isset($_POST['csvBuffer']) && isset($_GET['filename'])){
			$model = new ExportToExcel();
			$model->exportToExcelFromGrid($_GET['filename'] . date('Y-m-d'), $_POST['csvBuffer']);
		}
	}
	/**
	 * @todo actionCostaVegasTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionCostaVegasTotal()
	{
		$bl=new AgentWinLoss();
		$records=0;
		// data input validation ...
		if(!isset($_GET['accountID'])){
			exit('data_not_set');
		}

		if(strlen($_GET['accountID']) > 10 || $bl->isSpecialCharPresent($_GET['accountID'])){
			exit('invalid_account_id');
		}

		if(strlen($_GET['specificID']) > 10 || $bl->isSpecialCharPresent($_GET['specificID'])){
			exit('invalid_account_id');
		}
		// ... data input validation



		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getCostaVegasCSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getCostaVegasSMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getCostaVegasMATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getCostaVegasAGTTotal();
		}else{
			$player_records=$bl->getCostaVegasTotal();
		}
		$filedNames = array("row","currency_name","valid_bet","win_loss","commission","tips","total","vig_win_loss","vig_tips","vig_total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);

	}

	/**
	 * @todo actionCostaVegasSummaryWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionCostaVegasSummaryWinLoss()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getCostaVegasWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getCostaVegasWinLoss($orderField, $sortType, $startIndex, $limit);
		$filedNames = array("row","account_id","account_name","currency_name","valid_bet","average_bet","total","mc_win_loss","mc_comm","tips","mc_total","vig_win_loss","vig_tips","vig_total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionCostaVegasSummaryMemberWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionCostaVegasSummaryMemberWinLoss()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getCostaVegasWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getCostaVegasWinLoss($orderField, $sortType, $startIndex, $limit);
		$filedNames = array("row","account_id","account_name","currency_name","valid_bet","average_bet","win_loss","commission","tips","total","balance","agt_win_loss","agt_comm","agt_tips","agt_total","pl_total","exchange_rate");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionCostaVegasWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionCostaVegasWinLossDetails()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getCostaVegasWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getCostaVegasWinLossDetails($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","account_id","account_name","currency_name","valid_bet","average_bet","mem_win_loss",
				"mem_comm","tips","mem_total","current_balance","agt_win_loss","agt_comm","agt_tips","agt_total","ma_win_loss",
				"ma_comm","ma_tips","ma_total","sma_win_loss","sma_comm","sma_tips","sma_total","sc_win_loss","sc_comm","sc_tips","sc_total",
				"mc_win_loss","mc_comm","mc_tips","mc_total","vig_win_loss","vig_tips","vig_total","pl_total");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionCostaVegasMemberWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionCostaVegasMemberWinLossDetails()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getCostaVegasMemberWinLossDetailsCount();
		$data=$result->readAll();

		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getCostaVegasMemberWinLossDetails($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","id","bet_date","account_id","account_name","currency_name","game_type","amount_wagers","win_loss","commission","amount_tips","total","na_balance","casino_code");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	/**
	 * @todo actionCostaVegasMemberWinLossDetailsSummaryFooter
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionCostaVegasMemberWinLossDetailsSummaryFooter()
	{
		$bl=new AgentWinLoss();
		$bl->getCostaVegasMemberWinLossDetailsSummaryFooter();
	}
	/**
	 * @todo actionCostaVegasWinLossResultDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-15
	 */
	public function actionCostaVegasWinLossResultDetails(){
		$bl=new AgentWinLoss();
		$bl->getCostaVegasWinLossResultDetails();
	}





	/**
	 * @todo actionHTV999Total
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTV999Total()
	{
		$bl=new AgentWinLoss();
		$records=0;
		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getHTV999CSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getHTV999SMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getHTV999MATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getHTV999AGTTotal();
		}else{
			$player_records=$bl->getHTV999Total();
		}
		$filedNames = array("row","currency_name","total_stake","valid_bet","win_loss","commission","total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);
	}

	/**
	 * @todo actionHTVSummaryWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTVSummaryWinLoss()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getHTVWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getHTVWinLoss($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","total","mc_win_loss","mc_comm","mc_total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionHTVSummaryMemberWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTVSummaryMemberWinLoss()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getHTVWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getHTVWinLoss($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","win_loss","commission","total","current_balance","agt_win_loss","agt_comm","agt_total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionHTVWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTVWinLossDetails()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getHTVWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getHTVWinLossDetails($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","mem_win_loss",
				"mem_comm","mem_total","current_balance","agt_win_loss","agt_comm","agt_total","ma_win_loss",
				"ma_comm","ma_total","sma_win_loss","sma_comm","sma_total","sc_win_loss","sc_comm","sc_total",
				"mc_win_loss","mc_comm","mc_total","pl_total");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionHTV999WinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTV999WinLossDetails()
	{
		$bl=new AgentWinLoss();
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getHTVMemberWinLossDetailsCount();
		$data=$result->readAll();

		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getHTVMemberWinLossDetails($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","id","table_shoe_game","bet_date","account_id","account_name","currency_name","game_type","bet_amount","amount_wagers","result","win_loss","commission","amount_tips","balance_before","balance","total","casino_code");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}

	/**
	 * @todo actionHTV999WinLossDetailsSummaryFooter
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionHTV999WinLossDetailsSummaryFooter()
	{
		$bl=new AgentWinLoss();
		$bl->getHTVMemberWinLossDetailsSummaryFooter();
	}
	/**
	 * @todo actionHatienVegasWinLossResultDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-15
	 */
	public function actionHatienVegasWinLossResultDetails()
	{
		$bl=new AgentWinLoss();
		$bl->getHatienVegasWinLossResultDetails();
	}




	/**
	 * @todo actionSAVANTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSAVANTotal()
	{
		$bl=new AgentWinLoss();
		$records=0;
		if(Yii::app()->session['level']=='SC'){
			$player_records=$bl->getSAVANCSTotal();
		}else if(Yii::app()->session['level']=='SMA'){
			$player_records=$bl->getSAVANSMATotal();
		}else if(Yii::app()->session['level']=='MA'){
			$player_records=$bl->getSAVANMATotal();
		}else if(Yii::app()->session['level']=='AGT'){
			$player_records=$bl->getSAVANAGTTotal();
		}else{
			$player_records=$bl->getSAVANTotal();
		}
		$filedNames = array("row","currency_name","total_stake","valid_bet","win_loss","commission","total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);
	}

	/**
	 * @todo actionSAVANSummaryWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSAVANSummaryWinLoss()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getSAVANWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getSAVANWinLoss($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","total","mc_win_loss","mc_comm","mc_total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionSAVANSummaryMemberWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSAVANSummaryMemberWinLoss()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getSAVANWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getSAVANWinLoss($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","win_loss","commission","total","current_balance","agt_win_loss","agt_comm","agt_total","pl_total","exchange_rate");

		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionSAVANWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSAVANWinLossDetails()
	{
		$bl=new AgentWinLoss();

		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getSAVANWinLossCount();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getSAVANWinLossDetails($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","account_id","account_name","currency_name","bet_count","average_bet","total_stake","valid_bet","mem_win_loss",
				"mem_comm","mem_total","current_balance","agt_win_loss","agt_comm","agt_total","ma_win_loss",
				"ma_comm","ma_total","sma_win_loss","sma_comm","sma_total","sc_win_loss","sc_comm","sc_total",
				"mc_win_loss","mc_comm","mc_total","pl_total");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='<a style=\"color: blue; text-decoration: none;\" name=\"id1\" id=\"id1\" class=\"sharing_commission\" href=\"#\">View</a>';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}

	/**
	 * @todo actionSAVANWinLossBettingDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSAVANWinLossBettingDetails()
	{
		$bl=new AgentWinLoss();
		$page = (isset($_POST['page'])) ? $_POST['page'] : 0;
		$limit = (isset($_POST['rows'])) ? $_POST['rows'] : 0;
		$orderField = (isset($_POST['sidx'])) ? $_POST['sidx'] : 'currency_name asc';
		$sortType = (isset($_POST['sord'])) ? $_POST['sord'] : 'asc';

		$result=$bl->getSAVANMemberWinLossDetailsCount();
		$data=$result->readAll();

		$records=$data[0]['COUNT(0)'];

		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;

		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}

		$player_records=$bl->getSAVANMemberWinLossDetails($orderField, $sortType, $startIndex, $limit);

		$filedNames = array("row","id","table_shoe_game","bet_date","account_id","account_name","currency_name","game_type","bet_amount","amount_wagers","result","win_loss","commission","amount_tips","balance_before","balance","total","ip_address");
		$urlAS= Yii::app()->request->baseUrl;
			
		$htmValue='';
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}

	/**
	 * @todo actionSAVANWinLossBettingDetailsSummaryFooter
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSAVANWinLossBettingDetailsSummaryFooter()
	{
		$bl=new AgentWinLoss();
		$bl->getSAVANMemberWinLossDetailsSummaryFooter();
	}
	/**
	 * @todo actionSavanVegasWinLossResultDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-15
	 */
	public function actionSavanVegasWinLossResultDetails()
	{
		$bl=new AgentWinLoss();
		$bl->getSavanVegasWinLossResultDetails();
	}

	public function actionSavanVegasResultDetails(){
		$bl=new AgentWinLoss();
		echo $bl->getSavanVegasResultDetails();
	}

	/**
	 * @todo actionSharingAndCommission
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionSharingAndCommission()
	{
		$bl=new AgentSharingAndCommission();
		$bl->getSharingAndCommission();
	}

	/**
	 * @todo actionIndex
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readWinLoss') || User::getUserType() === Constants::ROLE_AGENT)
		{
			$this->render("index");
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}