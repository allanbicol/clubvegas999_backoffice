<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/createuser.css"/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo $this->module->assetsUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl;?>/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl;?>/js/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl;?>/js/jquery.jqGrid.min.js"></script>

<script type="text/javascript">
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_user_account').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i><li>System Setting <i class='icon-angle-right'></i></li><li>Users <i class='icon-angle-right'></i></li><li><a href='#'>Create</a></li>");
	jQuery(document).ready(function() {
		// customize mask
		// jQuery.mask.definitions['~']= '[ 1234567890-]';

        var myUsernameEditting = 'myUsernameEditting';
        var ipAddressParam = 'ipAddressParam';
        var gridId = {single: '1', range: '2'};
        var gridSingleURL = '<?php echo Yii::app()->request->baseUrl . '/index.php?r=SystemSetting/User/GetSingleIPList'; ?>';
        var gridSingleEditURL = '<?php echo Yii::app()->request->baseUrl . '/index.php?r=SystemSetting/User/UpdateSingleIPList'; ?>';
        var gridRangeURL = '<?php echo Yii::app()->request->baseUrl . '/index.php?r=SystemSetting/User/GetRangeIPList'; ?>';
        var gridRangeEditURL = '<?php echo Yii::app()->request->baseUrl . '/index.php?r=SystemSetting/User/UpdateRangeIPList'; ?>';
        var currentUser = '<?php echo $model->account_id; ?>';
		/////////////////// singleGrid ////////////////
        var lastSel, gridSingle = jQuery("#singleIPList"),
        onclickSubmitLocal = function(options, postdata) {
            var grid_p = gridSingle[0].p,
                idname = grid_p.prmNames.id,
                grid_id = gridSingle[0].id,
                id_in_postdata = grid_id+"_id",
                rowid = postdata[id_in_postdata],
                addMode = rowid === "_empty",
                // update
                myUsername = myUsernameEditting,
                ipAddressParam = 'ipAddressParam',
                oldValueOfSortColumn;

            // postdata has row id property with another name. we fix it:
            if (addMode) {
                // generate new id
                var new_id = grid_p.records + 1;
                while (jQuery("#"+new_id).length !== 0) {
                    new_id++;
                }
                postdata[idname] = String(new_id);
                postdata[ipAddressParam] = gridId.single;
                postdata[myUsername] = currentUser;
            } else if (typeof(postdata[idname]) === "undefined") {
                // set id property only if the property not exist
                postdata[idname] = rowid;
                postdata[ipAddressParam] = gridId.single;
                postdata[myUsername] = currentUser;
            }
			// remove space
            if (postdata['ipaddr']) {
				postdata['ipaddr'] = postdata['ipaddr'].replace(/\s/g, '');
            }
 
            delete postdata[id_in_postdata];

            // prepare postdata for tree grid
            if(grid_p.treeGrid === true) {
                if(addMode) {
                    var tr_par_id = grid_p.treeGridModel === 'adjacency' ? grid_p.treeReader.parent_id_field : 'parent_id';
                    postdata[tr_par_id] = grid_p.selrow;
                }

                jQuery.each(grid_p.treeReader, function (i){
                    if(postdata.hasOwnProperty(this)) {
                        delete postdata[this];
                    }
                });
            }

            // decode data if there encoded with autoencode
            if(grid_p.autoencode) {
                jQuery.each(postdata,function(n,v){
                    postdata[n] = jQuery.jgrid.htmlDecode(v); // TODO: some columns could be skipped
                });
            }

            // save old value from the sorted column
            oldValueOfSortColumn = grid_p.sortname === "" ? undefined: gridSingle.jqGrid('getCell',rowid,grid_p.sortname);

            // save the data in the grid
            if (grid_p.treeGrid === true) {
                if (addMode) {
                    gridSingle.jqGrid("addChildNode",rowid,grid_p.selrow,postdata);
                } else {
                    gridSingle.jqGrid("setTreeRow",rowid,postdata);
                }
            } else {
                if (addMode) {
                    gridSingle.jqGrid("addRowData", rowid,postdata,options.addedrow);
                } else {
                    gridSingle.jqGrid("setRowData",rowid,postdata);
                }
            }

            if ((addMode && options.closeAfterAdd) || (!addMode && options.closeAfterEdit)) {
                // close the edit/add dialog
                jQuery.jgrid.hideModal("#editmod"+grid_id,
                                  {gb:"#gbox_"+grid_id,jqm:options.jqModal,onClose:options.onClose});
            }

            if (postdata[grid_p.sortname] !== oldValueOfSortColumn) {
                // if the data are changed in the column by which are currently sorted
                // we need resort the grid
                setTimeout(function() {
                    gridSingle.trigger("reloadGrid", [{current:true}]);
                },100);
            }

            // !!! the most important step: skip ajax request to the server
            this.processing = true;
            return {};
        },
        editSettings = {
            //recreateForm:true,
            jqModal: false,
            reloadAfterSubmit: false,
            closeOnEscape: true,
            savekey: [true,13],
            closeAfterEdit: true,
            onclickSubmit: onclickSubmitLocal
        },
        addSettings = {
            //recreateForm:true,
            jqModal: false,
            reloadAfterSubmit: false,
            savekey: [true,13],
            closeOnEscape: true,
            closeAfterAdd: true,
            onclickSubmit: onclickSubmitLocal
        },
        delSettings = {
            // because I use "local" data I don't want to send the changes to the server
            // so I use "processing:true" setting and delete the row manually in onclickSubmit
            onclickSubmit: function(options, rowid) {

                var grid_id = jQuery.jgrid.jqID(gridSingle[0].id),
                    grid_p = gridSingle[0].p,
                    newPage = gridSingle[0].p.page;
				// edited
                options.delData = {
                	ipaddr: gridSingle.jqGrid ('getCell', rowid, 'ipaddr'),
                	myUsernameEditting: currentUser,
                    ipAddressParam: gridId.single
                };

				// reset the value of processing option which could be modified
				options.processing = false;
				
                // delete the row
                gridSingle.delRowData(rowid);
                jQuery.jgrid.hideModal("#delmod"+grid_id,
                                  {gb:"#gbox_"+grid_id,jqm:options.jqModal,onClose:options.onClose});

                if (grid_p.lastpage > 1) {// on the multipage grid reload the grid
                    if (grid_p.reccount === 0 && newPage === grid_p.lastpage) {
                        // if after deliting there are no rows on the current page
                        // which is the last page of the grid
                        newPage--; // go to the previous page
                    }
                    // reload grid to make the row from the next page visable.
                    gridSingle.trigger("reloadGrid", [{page:newPage}]);
                }

                return true;
            },
            processing:true
        };

    gridSingle.jqGrid({
        url: gridSingleURL, 
		datatype: 'json',
		postData: {myUsernameEditting: currentUser, ipAddressParam: gridId.single},
		mtype: 'GET',
        colNames:['IP Adress', 'username'],
        colModel:[
            {name: 'ipaddr', index: 'ipaddr', width: 70, editable: true, align: 'left', 
                editoptions: {
					dataInit: function (elem) {
						// jQuery(elem).addClass("ip ip-enabled");alert(jQuery(elem).attr("class"));
                    	// jQuery(elem).ipaddress();
                    	//jQuery(elem).addClass("ip ip-enabled");
                    	//jQuery(elem).css("height", "20");
                    	//jQuery(elem).css("width", "200");
                    	//jQuery(elem).css("display", "");
                    	// jQuery(elem).mask("999.999.999.999");
                    	//jQuery(elem).mask("~~9.~~9.~~9.~~9");
                   	}
				}
			},
			{name: 'username', index: 'username', hidden: true}
        ],
        //rowNum:10,
        gridview: true,
        //rownumbers: true,
        autoencode: true,
        ignoreCase: true,
        //sortname: 'invdate',
        sortorder: 'asc',
        caption: 'IP Address',
        height: '70',
        width: '230',
        editurl: gridSingleEditURL,
        pager: '#singleIPPage',
        //rowList: [],        // disable page size dropdown
        pgbuttons: false,     // disable page control like next, back button
        pgtext: null,         // disable pager text like 'Page 0 of 10'
        //viewrecords: false,
        ondblClickRow: function(rowid, ri, ci) {
            //var p = gridSingle[0].p;
            //if (p.selrow !== rowid) {
            //    // prevent the row from be unselected on double-click
            //    // the implementation is for "multiselect:false" which we use,
            //    // but one can easy modify the code for "multiselect:true"
            //    gridSingle.jqGrid('setSelection', rowid);
            //}
            //gridSingle.jqGrid('editGridRow', rowid, editSettings);
        },
        onSelectRow: function(id) {
            if (id && id !== lastSel) {
                // cancel editing of the previous selected row if it was in editing state.
                // jqGrid hold intern savedRow array inside of jqGrid object,
                // so it is safe to call restoreRow method with any id parameter
                // if jqGrid not in editing state
                if (typeof lastSel !== "undefined") {
                    gridSingle.jqGrid('restoreRow', lastSel);
                }
                
                lastSel = id;
            }
        }
    }).jqGrid('navGrid','#singleIPPage',{edit: false, del: true, add: true, refresh: true}, editSettings, addSettings, delSettings);

		/////////////////// singleGrid ////////////////
		/////////////////// rangeGrid ////////////////
        var lastSelRange, gridRange = jQuery("#rangeIPList"),
        onclickSubmitLocal = function(options, postdata) {
            var grid_p = gridRange[0].p,
                idname = grid_p.prmNames.id,
                grid_id = gridRange[0].id,
                id_in_postdata = grid_id+"_id",
                rowid = postdata[id_in_postdata],
                addMode = rowid === "_empty",
                // update
                myUsername = myUsernameEditting,
                ipAddressParam = 'ipAddressParam',
                oldValueOfSortColumn;

            // postdata has row id property with another name. we fix it:
            if (addMode) {
                // generate new id
                var new_id = grid_p.records + 1;
                while (jQuery("#"+new_id).length !== 0) {
                    new_id++;
                }
                postdata[idname] = String(new_id);
                postdata[ipAddressParam] = gridId.range;
                postdata[myUsername] = currentUser;
            } else if (typeof(postdata[idname]) === "undefined") {
                // set id property only if the property not exist
                postdata[idname] = rowid;
                postdata[ipAddressParam] = gridId.range;
                postdata[myUsername] = currentUser;
            }
			// remove space
            if (postdata['ipaddr']) {
            	postdata['ipaddr'] = postdata['ipaddr'].replace(/\s/g, '');
            }

            delete postdata[id_in_postdata];

            // prepare postdata for tree grid
            if(grid_p.treeGrid === true) {
                if(addMode) {
                    var tr_par_id = grid_p.treeGridModel === 'adjacency' ? grid_p.treeReader.parent_id_field : 'parent_id';
                    postdata[tr_par_id] = grid_p.selrow;
                }

                jQuery.each(grid_p.treeReader, function (i){
                    if(postdata.hasOwnProperty(this)) {
                        delete postdata[this];
                    }
                });
            }

            // decode data if there encoded with autoencode
            if(grid_p.autoencode) {
                jQuery.each(postdata,function(n,v){
                    postdata[n] = jQuery.jgrid.htmlDecode(v); // TODO: some columns could be skipped
                });
            }

            // save old value from the sorted column
            oldValueOfSortColumn = grid_p.sortname === "" ? undefined: gridSingle.jqGrid('getCell',rowid,grid_p.sortname);

            // save the data in the grid
            if (grid_p.treeGrid === true) {
                if (addMode) {
                	gridRange.jqGrid("addChildNode",rowid,grid_p.selrow,postdata);
                } else {
                	gridRange.jqGrid("setTreeRow",rowid,postdata);
                }
            } else {
                if (addMode) {
                	gridRange.jqGrid("addRowData", rowid,postdata,options.addedrow);
                } else {
                	gridRange.jqGrid("setRowData",rowid,postdata);
                }
            }

            if ((addMode && options.closeAfterAdd) || (!addMode && options.closeAfterEdit)) {
                // close the edit/add dialog
                jQuery.jgrid.hideModal("#editmod"+grid_id,
                                  {gb:"#gbox_"+grid_id,jqm:options.jqModal,onClose:options.onClose});
            }

            if (postdata[grid_p.sortname] !== oldValueOfSortColumn) {
                // if the data are changed in the column by which are currently sorted
                // we need resort the grid
                setTimeout(function() {
                	gridRange.trigger("reloadGrid", [{current:true}]);
                },100);
            }

            // !!! the most important step: skip ajax request to the server
            this.processing = true;
            return {};
        },
        editSettings = {
            //recreateForm:true,
            jqModal: false,
            reloadAfterSubmit: false,
            closeOnEscape: true,
            savekey: [true,13],
            closeAfterEdit: true,
            onclickSubmit: onclickSubmitLocal
        },
        addSettings = {
            //recreateForm:true,
            jqModal: false,
            reloadAfterSubmit: false,
            savekey: [true,13],
            closeOnEscape: true,
            closeAfterAdd: true,
            onclickSubmit: onclickSubmitLocal
        },
        delSettings = {
            // because I use "local" data I don't want to send the changes to the server
            // so I use "processing:true" setting and delete the row manually in onclickSubmit
            onclickSubmit: function(options, rowid) {

                var grid_id = jQuery.jgrid.jqID(gridRange[0].id),
                    grid_p = gridRange[0].p,
                    newPage = gridRange[0].p.page;
				// edited
                options.delData = {
                	ipaddr: gridRange.jqGrid ('getCell', rowid, 'ipaddr'),
                	ipaddr2: gridRange.jqGrid ('getCell', rowid, 'ipaddr2'),
                	myUsernameEditting: currentUser,
                    ipAddressParam: gridId.range
                };

				// reset the value of processing option which could be modified
				options.processing = false;
				
                // delete the row
                gridRange.delRowData(rowid);
                jQuery.jgrid.hideModal("#delmod"+grid_id,
                                  {gb:"#gbox_"+grid_id,jqm:options.jqModal,onClose:options.onClose});

                if (grid_p.lastpage > 1) {// on the multipage grid reload the grid
                    if (grid_p.reccount === 0 && newPage === grid_p.lastpage) {
                        // if after deliting there are no rows on the current page
                        // which is the last page of the grid
                        newPage--; // go to the previous page
                    }
                    // reload grid to make the row from the next page visable.
                    gridRange.trigger("reloadGrid", [{page:newPage}]);
                }

                return true;
            },
            processing:true
        };

	gridRange.jqGrid({
        url: gridRangeURL, 
		datatype: 'json',
		postData: {myUsernameEditting: currentUser, ipAddressParam: gridId.range},
		mtype: 'GET',
        colNames:['Start Range', 'End Range', 'username'],
        colModel:[
            {name: 'ipaddr', index: 'ipaddr', width: 120, editable: true, align: 'left', 
                editoptions: {
					dataInit: function (elem) { 
                    	// jQuery(elem).ipaddress();
                    	jQuery(elem).width(200);
                    	// jQuery(elem).mask("~~~.~~9.~~9.~~9-~~9.~~9.~~9.~~9");
                   	}
				}
			},
			{name: 'ipaddr2', index: 'ipaddr2', width: 120, editable: true, align: 'left', 
                editoptions: {
					dataInit: function (elem) { 
                    	// jQuery(elem).ipaddress();
                    	jQuery(elem).width(200);
                    	// jQuery(elem).mask("~~~.~~9.~~9.~~9-~~9.~~9.~~9.~~9");
                   	}
				}
			},
			{name: 'username', index: 'username', hidden: true}
        ],
        //rowNum:10,
        gridview: true,
        //rownumbers: true,
        autoencode: true,
        ignoreCase: true,
        //sortname: 'invdate',
        sortorder: 'asc',
        caption: 'IP Range Address',
        height: '70',
        width: '230',
        editurl: gridRangeEditURL,
        pager: '#rangeIPPage',
        //rowList: [],        // disable page size dropdown
        pgbuttons: false,     // disable page control like next, back button
        pgtext: null,         // disable pager text like 'Page 0 of 10'
        //viewrecords: false,
        ondblClickRow: function(rowid, ri, ci) {
            //var p = gridRange[0].p;
            //if (p.selrow !== rowid) {
            //    // prevent the row from be unselected on double-click
            //    // the implementation is for "multiselect:false" which we use,
            //    // but one can easy modify the code for "multiselect:true"
            //    gridRange.jqGrid('setSelection', rowid);
            //}
            //gridRange.jqGrid('editGridRow', rowid, editSettings);
        },
        onSelectRow: function(id) {
            if (id && id !== lastSelRange) {
                // cancel editing of the previous selected row if it was in editing state.
                // jqGrid hold intern savedRow array inside of jqGrid object,
                // so it is safe to call restoreRow method with any id parameter
                // if jqGrid not in editing state
                if (typeof lastSelRange !== "undefined") {
                	gridRange.jqGrid('restoreRow', lastSelRange);
                }
                
                lastSelRange = id;
            }
        }
    }).jqGrid('navGrid','#rangeIPPage',{edit: false, del: true, add: true, refresh: true},editSettings,addSettings,delSettings,
			{
				multipleSearch: true,
				overlay: false,
				onClose: function(form) {}
        	}
    	);
    	
		/////////////////// rangeGrid ////////////////
    	jQuery('#UserAccount_black_list').change(function() {
			if (jQuery('#UserAccount_black_list').is(':checked')) {
				gridSingle.jqGrid('setGridState','visible');
				gridRange.jqGrid('setGridState','visible');
		    } else {
				gridSingle.jqGrid('setGridState','hidden');
				gridRange.jqGrid('setGridState','hidden');
		    }
		});

    	//jQuery('.ip').ipaddress({cidr:true});
	});

</script>
<div id="body_wrapper">
<div id='title_header1'>
<?php $action=$model->getIsNewRecord() ? 'Create User Account' : 'Update User Account';
	echo "<b>".$action."</b>";
?>
</div>
<div class="form">
<div id="table_body">
<table>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	//'enableClientValidation'=>true,
)); ?>
    
    <?php
    	// store account id when perform an update 
    	echo $form->hiddenField($model,'account_id',array('size'=>45,'maxlength'=>45)); 
    	// echo $form->errorSummary($model);
    ?>
    <div class="row">
    	<tr>
    		<td width="250px">
        	<?php echo $form->label($model,'Account ID'); ?>
        	</td>
        	<td>
        		<?php 
        		if ($model->getIsNewRecord())
        			echo $form->textField($model,'account_id');
        		else
        			echo $form->textField($model,'account_id',array('disabled'=>'disabled')); 
        		?>
        	</td>
        	<td class="errorText">
				<?php echo $form->error($model,'account_id'); ?>
			</td>
        </tr>
    </div>
    <div class="row">
    	<tr>
    		<td>
        		<?php echo $form->label($model,'Account Name'); ?>
        	</td>
        	<td>
        		<?php echo $form->textField($model,'account_name'); ?>
        	</td>
        </tr>
    </div>
    
	<div class="row">
		<tr>
			<td>
				<?php echo $form->label($model,'Password'); ?>
			</td>
			<td>
				<?php echo $form->passwordField($model,'password'); ?>
			</td>
			<td class="errorText">
				<?php echo $form->error($model,'password'); ?>
			</td>
		</tr>
	</div>
	<div class="row">
		<tr>
			<td>
				<?php echo $form->label($model,'Confirmed Password'); ?>
			</td>
			<td>
				<?php 
				if ($model->getIsNewRecord())
					echo $form->passwordField($model,'confirmedPassword');
				else
					echo $form->passwordField($model,'confirmedPassword',array('value'=>$model->password));
				?>
			</td>
			<td class="errorText">
				<?php echo $form->error($model,'confirmedPassword'); ?>
			</td>
		</tr>
	</div>
	<div class="row">
		<tr>
			<td>
				<?php echo $form->label($model,'Enable'); ?>
			</td>
			<td>
				<?php 
				if ($model->getIsNewRecord())
					echo $form->checkBox($model,'is_enable',array('checked'=>true));
				else
					echo $form->checkBox($model,'is_enable',array('checked'=>$model->is_enable)); 
				?>
			</td>
		</tr>
	</div>
	<div class="row">
		<tr>
			<td>
				<?php echo $form->label($model,'Role'); ?>
			</td>
			<td>
				<?php 
					if ($model->getIsNewRecord())
						echo $form->dropDownList($model,'roleType',UserAccount::model()->getRoleList());
					else
					{
						$userSelectedRole=UserAccount::model()->getUserRoleName($_GET['Account_ID']);
						echo $form->dropDownList($model,'roleType',UserAccount::model()->getRoleList(),
						array('options'=>array($userSelectedRole=>array('selected'=>'selected'))));
					}
								
					/*
					echo "&nbsp;";
					$imghtml=CHtml::image('images/edit.png');
// 					echo CHtml::ajaxLink
// 								(
// 									$imghtml, 
// 									CController::createUrl('loadRoleDialog'),
// 									array('update' => '#dialog-ajax')
// 								);
					echo CHtml::ajaxLink
								(
									$imghtml,
									CController::createUrl('loadRoleDialog'),
									array(
											'onclick'=>	'$("#dialog-ajax").dialog("open"); return true;',
											'update' => '#dialog-ajax'
										 )
								);
					*/
					
				?>
			</td>
		</tr>
	</div>
	<?php
		// check action name
		if (isset(Yii::app()->controller->action->id) && Yii::app()->controller->action->id == 'update') {
	?>
	<div class="row">
		<tr>
			<td>
				<?php echo $form->label($model, 'Allowed IP'); ?>
			</td>
			<td>
				<?php
					if ($model->getIsNewRecord()) {
						echo $form->checkBox($model,'black_list', array('checked' => 1, 'unchecked' => 0, 'uncheckedValue' => 'N'));
					} else {
						echo $form->checkBox($model,'black_list', array('checked' => $model->black_list));
					} 
				?>
			</td>
		</tr>		
	</div>
	<div class="row">
		<tr>
			<td>
				<?php echo $form->label($model, 'Enter IP'); ?>
			</td>
			<td>
				<?php
				//echo $form->textField($model, 'addSigleIPText', array('class' => 'ip', 'id'=>'addSingleIP', 'value' => '0.0.0.0'));
				//echo CHtml::button('Add', array('id'=>'addSingleIPB'));
				?>
				<table id="singleIPList"><tr><td></td></tr></table>
					<div id="singleIPPage"></div>
			</td>
		</tr>
	</div>

	<div class="row">
		<tr>
			<td>
				<?php echo $form->label($model, 'Range IP Address'); ?>
			</td>
			<td>
				<table id="rangeIPList"><tr><td></td></tr></table>
				<div id="rangeIPPage"></div>
			</td>
			<td class="errorText">
				<?php echo $form->error($model,'white_list_ip_range_address'); ?>
			</td>
		</tr>
	</div>
	<?php
		}
	?>
	<div class="row buttons">
		<tr>
			<td></td>
			<td>
				<?php
				if(Yii::app()->user->checkAccess('systemSetting.writeUserAccount')){
					echo CHtml::submitButton('Save',array('name'=>'create','class'=>'btn red'));
				}
				?>
				<?php // echo CHtml::button('Cancel', array('name'=>'User')); ?>
				<?php echo CHtml::button('Cancel', array('onclick'=>'history.go(-1);','class'=>'btn red'));?>
			</td>
		</tr>
	</div>

<?php $this->endWidget(); ?>
</table>
</div>
</div></div><!-- form -->
