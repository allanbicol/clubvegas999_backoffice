<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/button.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agenttransactionhistory.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		//active menu color
		document.getElementById('agentHeader').className="start active";
		document.getElementById('agentHeader').className="start active";
		document.getElementById('mnu_transaction_hist').className="active";
		$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Agent Transaction History</a></li>");
	</script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		    jQuery("#dtFrom").datepicker({
		        changeMonth: true,
		        changeYear: true,
		        dateFormat: "yy-mm-dd"
		        //dateFormat: "dd-mm-yy"
		    });
		
		    jQuery("#dtTo").datepicker({
		        changeMonth: true,
		        changeYear: true,
		        dateFormat: "yy-mm-dd"
		    });
		});
		// tables for Fund transaction-------------------------------------------------------------------------------
		function tableTransactionHistory(currency_id,dtfrom,dtto,testChecked,accountID) { 
			document.getElementById('qry_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentTransactionHistory/AgentTransactionHistory&currency_id='+ currency_id +'&dtfrom='+ dtfrom + '&dtto=' + dtto +'&test='+testChecked+'&accountID='+accountID, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['<?php echo Yii::t('agent','agent.transHistory.id');?>','<?php echo Yii::t('agent','agent.transHistory.currency');?>','<?php echo Yii::t('agent','agent.transHistory.currency');?>','<?php echo Yii::t('agent','agent.transHistory.noTrans');?>','<?php echo Yii::t('agent','agent.transHistory.totalCredit');?>'],
				    colModel: [
				      {name: 'agent_account_id', index: 'agent_account_id', width: 100, sortable: false,},
				      {name: 'sdf', index: 'sdf', width: 90, sortable: false,hidden:true,},
				      {name: 'number', index: 'number', width: 70, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 70, align:"right", sortable: false},
				      {name: 'total_amnt', index: 'total_amnt', width: 70,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}}
				    ],
				    loadtext:"",
				    loadComplete: function() {
				       
				        var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	grid.jqGrid('setCell',i,"agent_account_id","",{'text-decoration':'underline'}); 
					    	grid.jqGrid('setCell',i,"agent_account_id","",{'color':'#663300'}); 
					    	grid.jqGrid('setCell',i,"total_amnt","",{background:'#f7f7c8'});
						}   
					  //grid stripe
					    $("tr.jqgrow:odd").css("background", "#DDDDDC");
					    $("tr.jqgrow:even").css("background", "#ffffff");
				    },
					width: 650,
					pager: '#pager1',
                    rownumbers: true,
                    rowNum: 25,	
            	    rowList: [25, 50, 100,200,500,99999],
                    sortable: false,
				    sortname: 'agent_account_id',
				    sortorder: 'ASC',
				    caption: '<b><?php echo Yii::app()->session['account_id'];?> <?php echo Yii::t('agent','agent.transHistory.credit');?></b> <input type="text" name="" id="txtDate" readonly="true">',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
			});
		} 
		
		// END tables for Fund transaction-------------------------------------------------------------------------------
		
		//Tables for Available credit transaction------------------------------------------------------------------------
		function tableAvailableCreditTransactionHistory(currency_id,dtfrom,dtto,testChecked,accountID) { 
			document.getElementById('qry_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentTransactionHistory/AvailableCreditAgentTransactionHistory&currency_id='+ currency_id +'&dtfrom='+ dtfrom + '&dtto=' + dtto+'&test='+testChecked+'&accountID='+accountID, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['<?php echo Yii::t('agent','agent.transHistory.id');?>','Currency','<?php echo Yii::t('agent','agent.transHistory.currency');?>','<?php echo Yii::t('agent','agent.transHistory.noTrans');?>','<?php echo Yii::t('agent','agent.transHistory.totalTransferred');?>'],
				    colModel: [
				      {name: 'agent_account_id', index: 'agent_account_id', width: 100, sortable: false,},
				      {name: 'sdf', index: 'sdf', width: 90, sortable: false,hidden:true,},
				      {name: 'number', index: 'number', width: 70, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 70, align:"right", sortable: false},
				      {name: 'total_amnt', index: 'total_amnt', width: 70,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}}
				    ],
				    loadtext:"",
				    loadComplete: function() {
					       
				        var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	grid.jqGrid('setCell',i,"agent_account_id","",{'text-decoration':'underline'}); 
					    	grid.jqGrid('setCell',i,"agent_account_id","",{'color':'#663300'}); 
					    	grid.jqGrid('setCell',i,"total_amnt","",{background:'#f7f7c8'});
						}   
				    },
					width: 650,
					pager: '#pager1',
                    rownumbers: true,
                    rowNum: 25,	
            	    rowList: [25, 50, 75],
                    sortable: false,
				    sortname: 'agent_account_id',
				    sortorder: 'ASC',
				    caption: '<b><?php echo Yii::app()->session['account_id'];?> <?php echo Yii::t('agent','agent.transHistory.availableCredit');?></b> <input type="text" name="" id="txtDate" readonly="true">',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false});
			});
		} 
		//End Tables available credit transaction----------------------------------------------------------------------------
		
		//Tables for Bonus transaction------------------------------------------------------------------------
		function tableBonusTransactionHistory(currency_id,dtfrom,dtto,testChecked) { 
			document.getElementById('qry_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentTransactionHistory/BonusAgentTransactionHistory&currency_id='+ currency_id +'&dtfrom='+ dtfrom + '&dtto=' + dtto+'&test='+testChecked, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['<?php echo Yii::t('agent','agent.transHistory.id');?>','Currency','<?php echo Yii::t('agent','agent.transHistory.currency');?>','<?php echo Yii::t('agent','agent.transHistory.noTrans');?>','<?php echo Yii::t('agent','agent.transHistory.totalCredit');?>'],
				    colModel: [
				      {name: 'agent_account_id', index: 'agent_account_id', width: 100, sortable: false,},
				      {name: 'sdf', index: 'sdf', width: 90, sortable: false,hidden:true,},
				      {name: 'number', index: 'number', width: 70, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 70, align:"right", sortable: false},
				      {name: 'total_amnt', index: 'total_amnt', width: 70,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}}
				    ],
				    loadtext:"",
				    loadComplete: function() {
					       
				        var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	grid.jqGrid('setCell',i,"agent_account_id","",{'text-decoration':'underline'}); 
					    	grid.jqGrid('setCell',i,"agent_account_id","",{'color':'#663300'}); 
					    	grid.jqGrid('setCell',i,"total_amnt","",{background:'#f7f7c8'});
						}   
				    },
					width: 650,
					pager: '#pager1',
                    rownumbers: true,
                    rowNum: 25,	
            	    rowList: [25, 50, 75],
                    sortable: false,
				    sortname: 'agent_account_id',
				    sortorder: 'ASC',
				    caption: '<b><?php echo Yii::app()->session['account_id'];?> <?php echo Yii::t('agent','agent.transHistory.bonus');?></b> <input type="text" name="" id="txtDate" readonly="true">',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false});
			});
		} 
		//End Tables bonus transaction----------------------------------------------------------------------------
		
		function tableTransactionHistoryDetails(account_id,currency_id,dtfrom,dtto,type) { 
			document.getElementById('qry_result').innerHTML='';
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list1'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager1'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_result").appendChild(divTag1);
		    
		    $(document).ready(function() {
				var grid=jQuery("#list1");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentTransactionHistory/AgentTransactionHistoryDetails&currency_id='+ currency_id +'&dtfrom='+ dtfrom + '&dtto=' + dtto + '&account_id=' + account_id +'&type='+type, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['Transaction Number','<?php echo Yii::t('agent','agent.transHistory.id');?>','<?php echo Yii::t('agent','agent.transHistory.currency');?>','<?php echo Yii::t('agent','agent.transHistory.type');?>','<?php echo Yii::t('agent','agent.transHistory.amount');?>','Market Type','Credit Before','Credit After','<?php echo Yii::t('agent','agent.transHistory.bBefore');?>','<?php echo Yii::t('agent','agent.transHistory.bAfter');?>','<?php echo Yii::t('agent','agent.transHistory.transDate');?>','<?php echo Yii::t('agent','agent.transHistory.operator');?>'],
				    colModel: [
				      {name: 'transaction_number', index: 'transaction_number', width:130, sortable: false},
				      {name: 'agent_account_id', index: 'agent_account_id', width: 120, sortable: false},
				      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
				      {name: 'trans_type_id', index: 'trans_type_id', width: 70, sortable: false,formatter:TypeFormatter},
				      {name: 'amount', index: 'amount', width: 80, sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'market_type', index: 'market_type', width: 100, sortable: false},
				      {name: 'credit_before', index: 'credit_before', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'credit_after', index: 'credit_after', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'balance_before', index: 'balance_before', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'balance_after', index: 'balance_after', width: 100,sortable: false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
				      {name: 'transaction_date', index: 'transaction_date', width: 130,sortable: false},
				      {name: 'operator_id', index: 'operator_id', width: 80,sortable: false}
				    ],
				    loadtext:"",
				    loadComplete: function() {
				        var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	grid.jqGrid('setCell',i,"operator_id","",{background:'#f7f7c8'});
					    	grid.jqGrid('setCell',i,"transaction_date","",{background:'#f7f7c8'});
					    	grid.jqGrid('setCell',i,"agent_account_id","",{color:'#663300'});
					    	if(myrow.amount < 0){
					    		grid.jqGrid('setCell',i,"amount","",{color:'#800000'});	
						    }else{
						    	grid.jqGrid('setCell',i,"amount","",{color:'#002952'});
							}

							//if (document.getElementById('btnId').value=="1"){
							//	$("#list1").hideCol("balance_before");
							//	$("#list1").hideCol("balance_after");
							//}else if (document.getElementById('btnId').value=="2"){
							//	$("#list1").hideCol("credit_before");
							//	$("#list1").hideCol("credit_after");
							//}
						} 
				    },
					width: 'auto',
					rowNum: 999999999,
                    rownumbers: true,
                    pager: '#pager1',
                    sortable: true,
				    sortname: 'transaction_date',
				    sortorder: 'DESC',
				    caption: '<b><?php echo Yii::app()->session['account_id'];?> <?php echo Yii::t('agent','agent.transHistory.transDetails');?></b> <input type="text" name="" id="txtDDate" readonly="true"> <input class="btn red" type="button" value="<<" onclick="javascript: return_to_main();">',
				    hidegrid: false,
				    viewrecords: true
				});
				$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});

				function TypeFormatter(cellvalue, options, rowObject) {
			        $("cellvalue").val(cellvalue);
					if (cellvalue==7){
						return '<label>Add</label>';
					}else if (cellvalue==8){
						return '<label">Remove</label>';
					}else if (cellvalue==2){
						return '<label">Recieve</label>';
					}else if (cellvalue==4){
						return '<label">Send</label>';
					}else if (cellvalue==6){
						return '<label">Bonus</label>';
					}
				}; 
			});
		} 
		
		function FundButton(){
			<?php
				if (Yii::app()->session['account_id']!=null){
			?>
					document.getElementById("chkTest").checked=true;
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					var accountID= document.getElementById('txtAccountId').value;
					document.getElementById('btnId').value="1";
					var dtfrom=(document.getElementById('dtFrom').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime1').value + ":00:00";
					var dtto=(document.getElementById('dtTo').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime2').value + ":59:59";
					tableTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
					document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
					document.getElementById('btnSubmit').name=document.getElementById('cmbCurrency').value;
		<?php }else{
					header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
				}
		?>
		}
		function AvailableCreditButton(){
			<?php
					if (Yii::app()->session['account_id']!=null){
				?>
						document.getElementById("chkTest").checked=true;
						testChecked=0;
						if (document.getElementById("chkTest").checked==true){
							 testChecked=1;
						}
						var accountID= document.getElementById('txtAccountId').value;
						document.getElementById('btnId').value="2";
						var dtfrom=(document.getElementById('dtFrom').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime1').value + ":00:00";
						var dtto=(document.getElementById('dtTo').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime2').value + ":59:59";
						tableAvailableCreditTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
						document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
						document.getElementById('btnSubmit').name=document.getElementById('cmbCurrency').value;
			<?php }else{
						header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
				  }
			?>
		}
		function BonusButton(){
			<?php
					if (Yii::app()->session['account_id']!=null){
				?>
					document.getElementById("chkTest").checked=true;
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					document.getElementById('btnId').value="3";
					var dtfrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
					var dtto=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
					tableBonusTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked);
					document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
					document.getElementById('btnSubmit').name=document.getElementById('cmbCurrency').value;
			<?php }else{
					header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
				}
			?>
		}
		
		function submitComplete()
		{
			<?php
					if (Yii::app()->session['account_id']!=null){
				?>
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					var accountID= document.getElementById('txtAccountId').value;
					var dtfrom=(document.getElementById('dtFrom').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime1').value + ":00:00";
					var dtto=(document.getElementById('dtTo').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime2').value + ":59:59";

					if (document.getElementById('btnId').value==1){
						tableTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
					}else if (document.getElementById('btnId').value==2){
						tableAvailableCreditTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
					}
					document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
					document.getElementById('btnSubmit').name=document.getElementById('cmbCurrency').value;
			<?php }else{
						header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
					}
				?>
		}
		function btnToDay()
		{
			<?php
				if (Yii::app()->session['account_id']!=null){
				?>
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					
					var dtToday='<?php echo date("Y-m-d");?>';
					document.getElementById('dtFrom').value=dtToday;
					document.getElementById('dtTo').value=dtToday;
					document.getElementById('cmbTime1').value='00';
					document.getElementById('cmbTime2').value='23';
					var accountID= document.getElementById('txtAccountId').value;
					var dtfrom=(document.getElementById('dtFrom').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime1').value + ":00:00";
					var dtto=(document.getElementById('dtTo').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime2').value + ":59:59";
					if (document.getElementById('btnId').value==1){
						tableTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
					}else if (document.getElementById('btnId').value==2){
						tableAvailableCreditTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
					}
					document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
					document.getElementById('btnSubmit').name=document.getElementById('cmbCurrency').value;
		<?php }else{
				header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
			}
		?>
		}
		function btnYesterday()
		{
		<?php
				if (Yii::app()->session['account_id']!=null){
			?>
				testChecked=0;
				if (document.getElementById("chkTest").checked==true){
					 testChecked=1;
				}
				
				var dtYesterday='<?php echo date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d"))));?>';
				document.getElementById('dtFrom').value=dtYesterday;
				document.getElementById('dtTo').value=dtYesterday;
				document.getElementById('cmbTime1').value='00';
				document.getElementById('cmbTime2').value='23';
				var accountID= document.getElementById('txtAccountId').value;
				var dtfrom=(document.getElementById('dtFrom').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime1').value + ":00:00";
				var dtto=(document.getElementById('dtTo').value).replace("-","/").replace("-","/") + "_" + document.getElementById('cmbTime2').value + ":59:59";
				if (document.getElementById('btnId').value==1){
					tableTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
				}else if (document.getElementById('btnId').value==2){
					tableAvailableCreditTransactionHistory(document.getElementById('cmbCurrency').value,dtfrom,dtto,testChecked,accountID);
				}
				document.getElementById('txtDate').value=dtfrom + " >> " + dtto;
				document.getElementById('btnSubmit').name=document.getElementById('cmbCurrency').value;
		<?php }else{
				header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
			}
		?>
		}
		function extract_details(obj)
		{
			<?php
				if (Yii::app()->session['account_id']!=null){
				?>
				var str_date=document.getElementById('txtDate').value.split(" >> ");
				if (document.getElementById('btnId').value==1){
					tableTransactionHistoryDetails(obj.title,obj.name,str_date[0],str_date[1],1);
				}else if (document.getElementById('btnId').value==2){
					tableTransactionHistoryDetails(obj.title,obj.name,str_date[0],str_date[1],2);
				}else if (document.getElementById('btnId').value==3){
					tableTransactionHistoryDetails(obj.title,obj.name,str_date[0],str_date[1],3);
				}
				document.getElementById('txtDDate').value=str_date[0] + " >> " + str_date[1];
		<?php }else{
				header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
			}
		?>
		}
		function return_to_main()
		{
		<?php
			if (Yii::app()->session['account_id']!=null){
			?>
				testChecked=0;
				if (document.getElementById("chkTest").checked==true){
					 testChecked=1;
				}
				var accountID= document.getElementById('txtAccountId').value;
				var str_date=document.getElementById('txtDDate').value.split(" >> ");
				if (document.getElementById('btnId').value==1){
					tableTransactionHistory(document.getElementById('btnSubmit').name,str_date[0],str_date[1],testChecked,accountID);
				}else if (document.getElementById('btnId').value==2){
					tableAvailableCreditTransactionHistory(document.getElementById('btnSubmit').name,str_date[0],str_date[1],testChecked,accountID);
				}else if (document.getElementById('btnId').value==3){
					tableBonusTransactionHistory(document.getElementById('btnSubmit').name,str_date[0],str_date[1],testChecked,accountID);
				}
				document.getElementById('txtDate').value=str_date[0] + " >> " + str_date[1];
		<?php }else{
				header( 'Location: '. Yii::app()->request->baseUrl.'/index.php?r=Login') ;
			}
		?>
		}

		function changeCurrency(){
			var lblExcept = document.getElementById('lblExcept');
			var chkTest = document.getElementById('chkTest');
			if (document.getElementById('cmbCurrency').value != 0){
				chkTest.style.visibility = 'hidden';
				lblExcept.style.visibility = 'hidden';
				chkTest.checked=false;
			 }else{
				chkTest.style.visibility = 'visible';
				lblExcept.style.visibility = 'visible';
		     }
		}
	</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript: FundButton();">
<div id="body_wraper">
	<div id="transaction_parameter_box">
		<div class="header"><?php echo Yii::t('agent','agent.transHistory.id');?> <font color="#D84A38"><?php echo Yii::app()->session['account_id'];?></font>: <?php echo Yii::t('agent','agent.transHistory.trans');?></div>
		<div class="body">
		<input class="btn red" type="button" value="<?php echo Yii::t('agent','agent.transHistory.fund');?>" onclick="javascript:FundButton();">
		<input class="btn red" type="button" value="<?php echo Yii::t('agent','agent.transHistory.availableCredit');?>" onclick="javascript:AvailableCreditButton();"> 
		<!--  <input class="gray" type="button" value="<?php echo Yii::t('agent','agent.transHistory.bonus');?>" onclick="javascript:BonusButton();">-->
		<input  id="btnId" type="hidden">
		<br/><br/>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?php echo Yii::t('agent','agent.transHistory.id');?></td>
				<td><input type="text" id="txtAccountId" style="width: 100px"></td>
			</tr>
			<tr><td class="left"><?php echo Yii::t('agent','agent.transHistory.currency');?> </td>
			<td class="right">
				<Select name="cmbCurrency" id="cmbCurrency" onChange="javascript:changeCurrency();">
					<option value="0">All</option>
					<?php 
						$dataReader = TableCurrency::model()->findAll();
						foreach ($dataReader as $row){
								echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
						}
					?>
				</Select>&nbsp;&nbsp;&nbsp;
				<input type="checkbox" id="chkTest" checked="checked" ><label id="lblExcept"><?php echo Yii::t('agent','agent.transHistory.except');?></label>
			</td></tr>
			<tr><td class="left"><?php echo Yii::t('agent','agent.transHistory.from');?></td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtFrom" name="dtFrom" class="dtText">
				<select id="cmbTime1">
				<?php 
					for($i=0;$i<=23;$i++){
						echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
					}
				?>
				</select> :00:00
			</td></tr>
			<tr><td class="left">
				<?php echo Yii::t('agent','agent.transHistory.to');?>
			</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtTo" class="dtText">
				<select id="cmbTime2">
				<?php 
					for($i=0;$i<=23;$i++){
						if($i<23){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}else{
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					}
				?> 
				</select> :59:59
			</td>
			</tr>
		</table>
		
		
		</div>
		<div class="row">
		</div>
		<center><input class="btn red" id="btnSubmit" name="" type="button" value="<?php echo Yii::t('agent','agent.transHistory.submit');?>" onclick="javascript: submitComplete();"> <input class="btn red" type="button" value="<?php echo Yii::t('agent','agent.transHistory.yesterday');?>" onclick="javascript: btnYesterday()"> <input class="btn red" type="button" value="<?php echo Yii::t('agent','agent.transHistory.today');?>" onclick="javascript: btnToDay();"></center>
		<br/>
	</div>
	<div id="qry_result">
	<div id="pager1"></div>
	</div>
</div>
</body>
</html>