<?php
class PlayerLoginAudit
{
	public function getPlayerLoginAudit($orderField, $sortType, $startIndex , $limit)
	{
		if ($_GET['accountId']!=""){
			$accountID="and account_id LIKE "."'".$_GET['accountId']."%'";
		}else{ $accountID='';}
		if ($_GET['session']!="All"){
			$session="and player_type="."'".$_GET['session']."'";
		}else{ $session=""; }
		if ($_GET['status']!="All"){
			$status="and onlive="."'".$_GET['status']."'";
		}else{ $status="";}
		if ($_GET['browser']!="All"){
			$browser="and browser LIKE "."'".$_GET['browser']."%'";
		}else{ $browser="";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select account_id,player_type,onlive,session,ip_address,isp,browser as browser_name,CONCAT(url_protocol,'://',url_domain,':',url_port) as site from tbl_player_login_session_history where session between '".$_GET['datefrom']."' and '".$_GET['dateto']."' $accountID $session $status $browser  ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	

	}
	
	public function getPlayerLoginAuditSummary($orderField, $sortType, $startIndex , $limit)
	{
		if ($_GET['accountId']!=""){
			$accountID="and account_id LIKE "."'".$_GET['accountId']."%'";
		}else{ $accountID='';}
		if ($_GET['session']!="All"){
			$session="and player_type="."'".$_GET['session']."'";
		}else{ $session=""; }
		if ($_GET['status']!="All"){
			$status="and onlive="."'".$_GET['status']."'";
		}else{ $status="";}
		if ($_GET['browser']!="All"){
			$browser="and browser LIKE "."'".$_GET['browser']."%'";
		}else{ $browser="";
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT '' as blank,CONCAT(url_protocol,'://',url_domain,':',url_port) AS url_domain,COUNT(account_id) as login_count FROM tbl_player_login_session_history WHERE onlive=1 AND url_domain !='' and session between '".$_GET['datefrom']."' and '".$_GET['dateto']."' $accountID $session $status $browser GROUP BY CONCAT(url_protocol,'://',url_domain,':',url_port)  ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayerLoginAuditExport()
	{
		if ($_GET['accountId']!=""){
			$accountID="and account_id LIKE "."'".$_GET['accountId']."%'";
		}else{ $accountID='';}
		if ($_GET['session']!="All"){
			$session="and player_type="."'".$_GET['session']."'";
		}else{ $session=""; }
		if ($_GET['status']!="All"){
			$status="and onlive="."'".$_GET['status']."'";
		}else{ $status="";}
		if ($_GET['browser']!="All"){
			$browser="and browser LIKE "."'".$_GET['browser']."%'";
		}else{ $browser="";
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select account_id,player_type,IF(player_type ='c','Cash Player','Agent Player') as player_type,IF(onlive='1','Login','Logout') as onlive,session,ip_address,isp,browser as browser_name,CONCAT(url_protocol,'://',url_domain,':',url_port) as site from tbl_player_login_session_history where session between '".$_GET['datefrom']."' and '".$_GET['dateto']."' $accountID $session $status $browser ");
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getPlayerLoginAuditSummaryExport()
	{
		if ($_GET['accountId']!=""){
			$accountID="and account_id LIKE "."'".$_GET['accountId']."%'";
		}else{ $accountID='';}
		if ($_GET['session']!="All"){
			$session="and player_type="."'".$_GET['session']."'";
		}else{ $session=""; }
		if ($_GET['status']!="All"){
			$status="and onlive="."'".$_GET['status']."'";
		}else{ $status="";}
		if ($_GET['browser']!="All"){
			$browser="and browser LIKE "."'".$_GET['browser']."%'";
		}else{ $browser="";
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT '' as blank,CONCAT(url_protocol,'://',url_domain,':',url_port) AS url_domain,COUNT(account_id) as login_count FROM tbl_player_login_session_history WHERE onlive=1 AND url_domain !='' and session between '".$_GET['datefrom']."' and '".$_GET['dateto']."' $accountID $session $status $browser GROUP BY CONCAT(url_protocol,'://',url_domain,':',url_port)");
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountPlayerLoginAudit()
	{
		if ($_GET['accountId']!=""){
			$accountID="and account_id LIKE "."'".$_GET['accountId']."%'";
		}else{ $accountID='';}
		if ($_GET['session']!="All"){
			$session="and player_type="."'".$_GET['session']."'";
		}else{ $session=""; }
		if ($_GET['status']!="All"){
			$status="and onlive="."'".$_GET['status']."'";
		}else{ $status="";}
		if ($_GET['browser']!="All"){
			$browser="and browser LIKE "."'".$_GET['browser']."%'";
		}else{ $browser="";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from tbl_player_login_session_history where session between '".$_GET['datefrom']."' and '".$_GET['dateto']."' $accountID $session $status $browser ");
		$rows = $command->query();
		return $rows;
	
	}
}