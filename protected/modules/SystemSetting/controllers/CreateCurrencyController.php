<?php
/**
 * @todo CreateUserController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CreateCurrencyController extends MyController
{
	public function actionCreateCurrency()
	{
		if(Yii::app()->user->checkAccess('systemSetting.writeCurrencySetting'))
		{
			$cu=new Currency();
			$cu->addNewCurrency();
		}
	}
	public function actionIndex()
	{
		$this->render("index");
	}
}
