<?php

class PromoItemController extends MyController
{
	
public function actionPromoItemList()
	{
		$cp = new PromoItem();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$cp->getCountPromoItemList();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
		
		$startIndex = $limit*$page - $limit;
		$user_records=$cp->getPromoItemList($orderField, $sortType, $startIndex, $limit);
		
		$filedNames = array("id","item_name","equevalent_value","equevalent_points","stock","available","available");
		$urlAS= Yii::app()->request->baseUrl;
		if(Yii::app()->user->checkAccess('systemSetting.writeCurrencySetting')){
			$htmValue='<a class=\'btn mini red\' href=\'#\' onclick=\'javascript: getCurrencyName(this); $updatePromoItemDialog.dialog(\"open\");document.getElementById(\"e_cs_errMessage\").value=\"\"\' id=\'currency\'>Edit <i class=\"icon-edit\"></i></a>';
		}else{
			$htmValue='  '; 
		}
	
		echo JsonUtil::jsonEncodeSystemSetting($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,6);
	}
	public function actionIndex()
	{
		
			
		if(Yii::app()->user->checkAccess('marketing.readPromoItem'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		
	}
	
	public function actionPromoItemDetails()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select id,item_name,equevalent_value,equevalent_points,stock from tbl_promo_items where id='".$_POST['id']."'");
		$rows = $command->queryRow();
		echo json_encode($rows);
	}
	
	public function actionUpdateAvailablePromoItem() {
		//START: $_POST['is_checked'] validation
		if(Yii::app()->user->checkAccess('marketing.writePromoItem'))
		{
			if($_POST['is_checked']!='true' && $_POST['is_checked']!='false'){
				exit('Invalid white_list value!');
			}
				
			if(trim($_POST['id'])==''){
				exit('Invalid account_id value!');
			}

			$AccountID = Yii::app()->session['account_id'];
			$level=Yii::app()->session['level_name'];
			$dateTime = date('Y-m-d H:i:s');
			$log_details='';

			
			if($_POST['is_checked']=='true'){
				$is_checked=1;
				$msg='SC '. $_POST['item_name'] . ' is now available.';
				$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> added updated promo item <label style=\"color:red\">'.$_POST['item_name'].'</label> from <label style=\"color:red\">unavailable</label> to <label style=\"color:green\">available</label></b>';
			}else{
				$is_checked=0;
				$msg='SC '. $_POST['item_name'] . ' is now unavailable.';
				$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> added updated promo item <label style=\"color:red\">'.$_POST['item_name'].'</label> from <label style=\"color:green\">available</label> to <label style=\"color:red\">unavailable</label></b>';
			}
			TablePromoItem::model()->updateAll(array('available'=>$is_checked),'id = "'.$_POST['id'].'"');
				
			$logs = new MarketingLogs();
			$logs->saveLog($AccountID, $level,$_POST['item_name'], 'Marketing', 27, $log_details);
			
		}else{
			$msg='You dont have permission to do this operation.';
		}
		echo $msg;
	}
	
	public function actionUpdatePromoItem() {
		//START: $_POST['is_checked'] validation
		if(Yii::app()->user->checkAccess('marketing.writePromoItem'))
		{
	
			if(trim($_POST['id'])==''){
				exit('Invalid account_id value!');
			}
	
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT id,item_name,equevalent_value,equevalent_points,stock from tbl_promo_items where id='".$_POST['id']."'");
			$rd=$command->queryRow();
			$item_name=$rd['item_name'];
			$equevalent_value=$rd['equevalent_value'];
			$equevalent_points=$rd['equevalent_points'];
			$stock=$rd['stock'];
				
			$AccountID = Yii::app()->session['account_id'];
			$level=Yii::app()->session['level_name'];
			$dateTime = date('Y-m-d H:i:s');
			$log_details='';
	
			TablePromoItem::model()->updateAll(array('item_name'=>$_POST['itemName'],'equevalent_value'=>$_POST['equevalentValue'],'equevalent_points'=>$_POST['equevalentPoints'],'stock'=>$_POST['stock']),'id = "'.$_POST['id'].'"');
	
	
		if($item_name!=$_POST['itemName']){
			$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> change item name  from <label style=\"color:red\">'.$item_name.'</label> to <label style=\"color:green\">'.$_POST['itemName'].'</label></b>';
		}
		if($equevalent_value!=$_POST['equevalentValue']){
			$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> change equevalent value  from <label style=\"color:red\">'.$equevalent_value.'</label> to <label style=\"color:red\">'.$_POST['equevalentValue'].'</label></b>';
		}
		if($equevalent_points!=$_POST['equevalentPoints']){
			$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> change equevalent points  from <label style=\"color:red\">'.$equevalent_points.'</label> to <label style=\"color:red\">'.$_POST['equevalentPoints'].'</label></b>';
		}
		if($stock!=$_POST['stock']){
			$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> change stock from <label style=\"color:red\">'.$stock.'</label> to <label style=\"color:red\">'.$_POST['stock'].'</label></b>';
		}
		
		$logs = new MarketingLogs();
		$logs->saveLog($AccountID, $level,$_POST['itemName'], 'Marketing', 27, $log_details);
			
		$msg='suc';		
		}else{
			$msg='You dont have permission to do this operation.';
		}
		echo $msg;
	}
	
	public function actionSavePromoItem() {
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		
		$postPromoItem=new TablePromoItem;
		$dateTime = date('Y-m-d H:i:s');
		if(Yii::app()->user->checkAccess('marketing.writePromoItem'))
		{
			$postPromoItem->item_name=$_POST['itemName'];
			$postPromoItem->equevalent_value=$_POST['equevalentValue'];
			$postPromoItem->equevalent_points=$_POST['equevalentPoints'];
			$postPromoItem->stock=$_POST['stock'];
			$postPromoItem->available=1;
			$postPromoItem->save();
		
			$log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> added new promo item <label style=\"color:red\">'.$_POST['itemName'].'</label> to item list with the equevalent value of <label style=\"color:red\">'.$_POST['equevalentValue'].'</label> and equevalent points of <label style=\"color:red\">'.$_POST['equevalentPoints'].'</label></b>';

			
			$logs = new MarketingLogs();
			$logs->saveLog($AccountID, $level,$_POST['itemName'], 'Marketing', 27, $log_details);
			
			echo 'suc';
		
		}else{
			echo 'err';
		}
		
	}
	
	
	
	
}