<?php

/*
 * This file is part of the Predis package.
 *
 * (c) Daniele Alessandri <suppakilla@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
defined('_CV999_') or die;

require __DIR__.'/autoload.php';

$single_server = array(
    'host'     => '192.168.4.43',
    'port'     => 6278,
    'password' => 'r3d15@C3',
    'database' => 7,

);

$multiple_servers = array(
    array(
       'host'     => '192.168.4.43',
       'port'     => 6278,
       'password' => 'r3d15@C3',
       'database' => 8,
       'alias'    => 'first',

    ),
    array(
       'host'     => '192.168.4.43',
       'port'     => 6278,
       'password' => 'r3d15@C3',
       'database' => 9,
       'alias'    => 'second',

    ),
);

?>
