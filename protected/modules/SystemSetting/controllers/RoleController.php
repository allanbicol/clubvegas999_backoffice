<?php

class RoleController extends MyController
{
	public function actionRoleList()
	{
		$model = new Role();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$model->getRecordCount();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
	
		$startIndex = $limit*$page - $limit;
		$user_records=$model->getRoleList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("user_name","user_role");
		$htmValue='';
		echo JsonUtil::jsonEncode($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readRoleAndPermission'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	/**
	 * @todo update user role page
	 * @author leokarl
	 * @since 2013-01-07
	 */
	public function actionFormUpdate(){
		if(Yii::app()->user->checkAccess('systemSetting.readRoleAndPermission') || Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission'))
		{
			$model = new Role();
			$this->render("update",array('role_name'=>$model->getDataAsRoleName($_GET['role_id']),
					'home_page_url_id'=>$model->getHomePageUrlId($_GET['role_id']),
					'home_page_list'=>$model->getHomePageList(),
					'agent'=>$model->getRolesForUpdate($_GET['role_id'], 'agent'),
					'cashplayer'=>$model->getRolesForUpdate($_GET['role_id'], 'cashplayer'),
					'banking'=>$model->getRolesForUpdate($_GET['role_id'], 'banking'),
					'gamesetting'=>$model->getRolesForUpdate($_GET['role_id'], 'gameSetting'),
					'systemmonitor'=>$model->getRolesForUpdate($_GET['role_id'], 'systemMonitor'),
					'systemsetting'=>$model->getRolesForUpdate($_GET['role_id'], 'systemSetting'),
					'securitylog'=>$model->getRolesForUpdate($_GET['role_id'], 'securityLog'),
					'marketingcustomreport'=>$model->getRolesForUpdate($_GET['role_id'], 'marketing')
					));
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
	/**
	 * @todo create user role page
	 * @author leokarl
	 * @since 2013-01-09
	 */
	public function actionFormCreate(){
		if(Yii::app()->user->checkAccess('systemSetting.readRoleAndPermission') || Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission'))
		{
			$model = new Role();
			$this->render("create",array('agent'=>$model->getRolesForCreate('agent'),
					'home_page_list'=>$model->getHomePageList(),
					'cashplayer'=>$model->getRolesForCreate('cashplayer'),
					'banking'=>$model->getRolesForCreate('banking'),
					'gamesetting'=>$model->getRolesForCreate('gameSetting'),
					'systemmonitor'=>$model->getRolesForCreate('systemMonitor'),
					'systemsetting'=>$model->getRolesForCreate('systemSetting'),
					'securitylog'=>$model->getRolesForCreate('securityLog'),
					'marketingcustomreport'=>$model->getRolesForCreate('marketing')
					));
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
	/**
	 * @todo update user role
	 * @author leokarl
	 * @since 2013-01-08
	 */
	public function actionUpdateRole(){
		$model = new Role();
		
		// data validation
		if(!isset(Yii::app()->session['account_id'])){
			exit('session_expired');
		}
		if(!Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission')){
			exit('no_permission');
		}
		if(!isset($_POST['role_id']) || !isset($_POST['role_id_before']) || !isset($_POST['agent_roles']) || !isset($_POST['cashplayer_roles']) || !isset($_POST['banking_roles']) ||
		   !isset($_POST['game_setting_roles']) || !isset($_POST['system_monitor_roles']) || !isset($_POST['system_setting_roles'])){
			exit('data_param_not_set');
		}
		if(!$model->isRoleNameExist($_POST['role_id_before'])){
			exit('role_name_not_exist');
		}
		if(trim($_POST['role_id']) == ''){
			exit('role_id_blank');
		}
		if(trim($_POST['role_name']) == ''){
			exit('role_name_blank');
		}
		if($model->isSpecialCharPresentIncludeWhiteSpace($_POST['role_id'])){
			exit('special_char_present');
		}
		if($model->isSpecialCharPresent($_POST['role_name'])){
			exit('special_char_present_in_role_name');
		}
		if(trim($_POST['role_id']) != trim($_POST['role_id_before'])){
			if($model->isRoleNameExist($_POST['role_id'])){
				exit('role_name_already_exist');
			}
		}
		if(trim($_POST['home_page_url_id']) == ''){
			exit('blank_home_page_url');
		}
		if(!$model->isPermissionsValid($_POST['agent_roles'], $_POST['cashplayer_roles'], $_POST['banking_roles'], 
				$_POST['game_setting_roles'], $_POST['system_monitor_roles'], $_POST['system_setting_roles'])){
			exit('invalid_permission');	
		}
		// update
		$model->updateRole();
		
	}
	
	/**
	 * @todo create user role
	 * @author leokarl
	 * @since 2013-01-08
	 */
	public function actionCreateRole(){
		$model = new Role();
		
		// data validation
		if(!isset(Yii::app()->session['account_id'])){
			exit('session_expired');
		}
		if(!Yii::app()->user->checkAccess('systemSetting.writeRoleAndPermission')){
			exit('no_permission');
		}
		if(!isset($_POST['role_id']) || !isset($_POST['agent_roles']) || !isset($_POST['cashplayer_roles']) || !isset($_POST['banking_roles']) ||
				!isset($_POST['game_setting_roles']) || !isset($_POST['system_monitor_roles']) || !isset($_POST['system_setting_roles'])){
			exit('data_param_not_set');
		}
		if(trim($_POST['role_id']) == ''){
			exit('role_id_blank');
		}
		if($model->isRoleNameExist($_POST['role_id'])){
			exit('role_name_already_exist');
		}
		if($model->isSpecialCharPresentIncludeWhiteSpace($_POST['role_id'])){
			exit('special_char_present');
		}
		if($model->isSpecialCharPresent($_POST['role_name'])){
			exit('special_char_present_in_role_name');
		}
		if(trim($_POST['role_name']) == ''){
			exit('role_name_blank');
		}
		if(!$model->isPermissionsValid($_POST['agent_roles'], $_POST['cashplayer_roles'], $_POST['banking_roles'],
				$_POST['game_setting_roles'], $_POST['system_monitor_roles'], $_POST['system_setting_roles'])){
			exit('invalid_permission');
		}
		if(trim($_POST['home_page_url_id']) == ''){
			exit('blank_home_page_url');
		}
		// create
		$model->createRole();
	}
	
}