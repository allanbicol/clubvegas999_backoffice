<?php
/**
 * @todo UpdateUser Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class UpdateUser
{
 
    /**
     * @todo changeUserStatus
     * @copyright CE
     * @author Leo karl
     * @since 2012-04-24
     */
	public function changeUserStatus()
	{
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		
		if ($_GET['status']=='Active'){
			$intStatID='1';
		}
		elseif($_GET['status']=='Closed'){
			$intStatID='2';
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT is_enable from tbl_user  WHERE account_id='" . $_GET['AccountID'] . "'");
		$rd=$command->queryRow();
		$statusVal=$rd['is_enable'];
		//$levelID=$rd['level_id'];
		
		$command1 = $connection->createCommand("SELECT level_name from vwGetUserList  WHERE account_id='" . $_GET['AccountID'] . "' ");
		$rd1=$command1->queryRow();
		$levelname=$rd1['level_name'];
			
		if ($statusVal==1){
			$status='Active';
		}else{$status='Closed';}
			
		User::model()->updateAll(array('is_enable'=>$intStatID),'account_id="'.$_GET['AccountID'].'"');
		
		$postLog=new TableLog();
		$postLog->operated_by=$AccountID;
		$postLog->operated_by_level=$level;
		$postLog->operated=$_GET['AccountID'];
		$postLog->operated_level=$levelname;
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=8;
		$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE status of <label style=\"color:green\">'.$levelname.'</label> <label style=\"color:#7A5C00\">'.$_GET['AccountID'].'</label> from <label style=\"color:red\">'.$status.'</label> to <label style=\"color:red\">'.$_GET['status'].'</label></b>';
		$postLog->save();
	}
}