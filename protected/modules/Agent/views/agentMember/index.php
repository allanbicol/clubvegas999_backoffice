<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17 27/themes/base/jquery-ui.css"/> -->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/statpopup.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
function addNewSubCompany() 
{
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentNewSubCompany&type=MEM&account_id="+document.getElementById('txtAccountID').value;
}
function backToAgent() 
{
	<?php if (Yii::app()->session['level']!='AGT') {?>
		window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/Agent&type=MA&account_id="+document.getElementById('txtAccountID').value.substring(0,6);
	<?php }?>
}
</script>
<script type="text/javascript">
	//active menu color
	document.getElementById('agentHeader').className="start active";
	document.getElementById('mnu_agent_list_info').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Agent Member</a></li>");
</script>
<script type="text/javascript">
var isIE = document.all?true:false;
var cRowNo=0;
if (!isIE) document.captureEvents(Event.CLICK);
document.onmousemove = getMousePosition;
function getMousePosition(e) {
  var _x;
  var _y;
  if (!isIE) {
    _x = e.pageX;
    _y = e.pageY;
  }
  if (isIE) {
    _x = event.clientX + document.body.scrollLeft;
    _y = event.clientY + document.body.scrollTop;
  }

posX=_x;
posY=_y;

  return true;
}
var div1Tag = document.createElement("div"); 
function createDiv() { 
    div1Tag.id = "dvStatus"; 
    div1Tag.setAttribute("align", "center"); 
    div1Tag.setAttribute("onmouseout","javascript: document.getElementById('dvStatus').addEventListener('mouseout',onMouseOut,true);")
    div1Tag.style.margin = "0px auto"; 
    div1Tag.className = "dynamicDiv"; 
    document.body.appendChild(div1Tag);
} 

function hideDiv(divID) { 
	<?php if (Yii::app()->session['level']=='AGT') {?>
		$('#btnBack').hide();
	<?php }?>
	 if (document.getElementById) { 
	 document.getElementById(divID).style.visibility = 'hidden'; 
	 } 
	} 
function showDiv(divID,x,y) { 
	 var X = 0; 
	 var Y = 0; 
	 
	 if (document.getElementById) { 
	 
	   var relativeX = x;
	   var relativeY = y;
	 document.getElementById(divID).style.left= relativeX+"px";
	 document.getElementById(divID).style.top= relativeY+"px";
	 document.getElementById(divID).style.visibility = 'visible'; 
	 } 
	}

function getPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    } 
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX + 
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY + 
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    alert(cursor);
    return cursor;
}
function onMouseOutDiv(id){
	document.getElementById('id').addEventListener('mouseout',onMouseOut,true);
}
function onMouseOut(event) {
    //this is the original element the event handler was assigned to
        e = event.toElement || event.relatedTarget;
        if (e.parentNode == this || e == this) {
           return;
        }
        hideDiv("dvStatus");
    // handle mouse event here!
}
function expandContract(id,e)
{
   if (!e) var e = window.event;

   var divObj = document.getElementById(id)

   var eventSource = (window.event) ? e.srcElement : e.target;
   if (e.type == "mouseout" && eventSource.nodeName != "DIV")
       return;

   // prevent event bubbling
   var relTarg = e.relatedTarget || e.toElement || e.fromElement;

   try
   {
       while (relTarg && relTarg != divObj)
           relTarg = relTarg.parentNode;

       if (relTarg == divObj)
           return;
//hideDiv();
       divObj.style.display = (divObj.style.display == "block") ? "none" : "block";
   }
   catch(e)
   {
   }
}
//============================================================================================
var cRowNo=0;
	function tableTransHistoryTodayTotal(id,name,currency,status,accountID) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list2'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultTotal").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager2'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultTotal").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#list2");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentMember/AgentMember&account_id='+accountID+'&id='+id+'&name='+name+'&currency='+currency+'&stat='+status, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','<?php echo Yii::t('agent','agent.subcompanylist.accountid');?>', '<?php echo Yii::t('agent','agent.subcompanylist.accountname');?>','<?php echo Yii::t('agent','agent.subcompanylist.currency');?>','<?php echo Yii::t('agent','agent.subcompanylist.status');?>','<?php echo Yii::t('agent','agent.subcompanylist.opening');?>','<?php echo Yii::t('agent','agent.subcompanylist.balancedue');?>','<?php echo Yii::t('agent','agent.subcompanylist.availablecredit');?>','<?php echo Yii::t('agent','agent.subcompanylist.balance');?>','<?php echo Yii::t('agent','agent.subcompanylist.operations');?>'],
			    colModel: [
					{name:'id',index: 'id',hidden:true},
					{name: 'account_id', index: 'account_id', width: 100, search:true,title:false},
					{name: 'account_name', index: 'account_name', width: 100,title:false},
					{name: 'currency_name', index: 'currency_name',align:"center", width: 60,title:false},
					{name: 'status', index: 'status', width: 60,align:"center",title:false},
					{name: 'opening_acc_date', index: 'opening_acc_date', width: 130,title:false},
					{name: 'balance_to_from', index: 'balance_to_from', width: 80, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'credit', index: 'credit', width: 95, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'balance', index: 'balance', width: 100,title:false, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'operation', index: 'operation', width: 450, align:"center",title:false,sortable:false},
					//{name: 'online_status', index: 'online_status', width: 110,title:false,formatter:onliveFormatter,hidden:true},
					//{name: 'login_ip_address', index: 'login_ip_address', width: 100,title:false,hidden:true},
					
			    ],
			    ondblClickRow: function (rowid, iCol, cellcontent, e) {
				    //alert ("sdfgsdgsdgsdgsdg");
			    	var myrow = grid.jqGrid('getRowData', rowid);
				    if (colNo==5){
					    if (myrow.status == 'Closed')
						{
					    	div1Tag.innerHTML = '<input type="checkbox" name="chkStat" value="Active" class="status-popup" id="stat_active"/><label for="stat_active">Active</label>';
						}
					    else if(myrow.status=='Active')
					    {
					    	div1Tag.innerHTML = '<input type="checkbox" name="chkStat" value="Closed" class="status-popup" id="stat_close"/><label for="stat_close">Closed</label>';
						}
				    	jQuery('.status-popup').click(function(event) {
				    		currenctStatusValue = jQuery(this).val();
				    		jQuery.ajax({
					    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerStatus/AgentPlayerStatusProcess',
					    		method: 'POST',
					    		data: {'task': 'changeStatus', 'AccountID': myrow.id, 'status': jQuery(this).val()},
					    		context: jQuery(this).val(),
					    		success: function(data) {
					    			if(currenctStatusValue == 'Active'){
				    					grid.jqGrid('setCell', rowid, colNo, currenctStatusValue);
							    		grid.jqGrid('setCell',rowid,"status","",{color:'green'});	
								 	}else if (currenctStatusValue == 'Closed') {
								    	grid.jqGrid('setCell', rowid, colNo, currenctStatusValue);
							    		grid.jqGrid('setCell',rowid,"status","",{color:'#999999'});	
								 	}
							     	grid.jqGrid('setCell',i,"status","",{'font-weight': 'bold'});	
					    			 hideDiv("dvStatus");
						    	}
					    	});
				    	});
			    		showDiv('dvStatus',posX-5,posY-5);
				    }
		        },
		        onCellSelect : function (rowid,iCol,cellcontent,e) {
		        	var myrow = grid.jqGrid('getRowData', rowid);
					rValue = myrow.account_id;
					colNo=iCol;
			    },  
			    loadtext:"",
			    loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	if(myrow.status == 'Active'){
				    		grid.jqGrid('setCell',i,"status","",{color:'green'});	
					    }else if (myrow.status == 'Closed') {
				    		grid.jqGrid('setCell',i,"status","",{color:'#999999'});	
					    }
				    	grid.jqGrid('setCell',i,"status","",{'font-weight': 'bold'});	    
					}   
				  //grid stripe
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    $("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rownumbers:true,
			    rowNum: 30,	
			    rowList: [30, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'account_id,status',
			    sortorder: 'ASC',
			    cellEdit: true,
			    hidegrid:false,
		        cellsubmit : 'remote',
			    caption: ' <label style="color:#D84A38"><?php echo strtoupper(Yii::app()->session['account_id']);?></label> <?php echo Yii::t('agent','agent.subcompanylist.playerlist');?> &nbsp;&nbsp;&nbsp;&nbsp;<button style=" <?php if(!Yii::app()->user->checkAccess('agent.addNewSubCompany') && Yii::app()->session['account_type'] !='agent'){echo 'display:none';}?>" onclick="javascript: addNewSubCompany();" class="btn red" ><?php echo Yii::t('agent','agent.subcompanylist.add');?> <i class="icon-plus"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn red" id="btnBack" onclick="javascript:backToAgent();"><?php echo Yii::t('agent','agent.subcompanylist.back');?> <i class="icon-circle-arrow-left"></i></button>',
			    viewrecords: true,
			});
			$('#list2').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false,refresh:false});
			function onliveFormatter(cellvalue, options, rowObject) {
		        $("cellvalue").val(cellvalue);
		        if (cellvalue=="online"){
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/online.png" />'+"  "+'<label style="color:green">'+ cellvalue+'</label>';
		        }else{
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/offline.gif" />'+"  "+'<label style="color:#A7A7A6">'+ cellvalue+'</label>';
			    }
			};
			function statusFormatter(cellvalue, options, rowObject) {
		        $("cellvalue").val(cellvalue);
		        if (cellvalue=="Active"){
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/active.png" /><label style="color:green">'+ cellvalue+'</label>';
		        }else{
		        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/inactive.png" /><label style="color:#A7A7A6">'+ cellvalue+'</label>';
			    }
			};	
		});
	} 
</script>

<script type="text/javascript">
function runKeyPress(e) {
    if (e.keyCode == 13) {
       chkBtn();
    }
}

function chkBtn()
{
		checkBeforeLoad();
}

function checkBeforeLoad(){
	if (document.getElementById('txtAccountId').value=="" && document.getElementById('txtAccountName').value=="")
	{
		var currency=document.getElementById('txtCurrency').value;
		var status=document.getElementById('txtStatus').value;
		loadTransTable("All","All",currency,status);
	}
	else if (document.getElementById('txtAccountId').value=="" && document.getElementById('txtAccountName').value!="")
	{
		var accountName=document.getElementById('txtAccountName').value;
		var currency=document.getElementById('txtCurrency').value;
		var status=document.getElementById('txtStatus').value;
		loadTransTable("All",accountName,currency,status);
	}
	else if (document.getElementById('txtAccountId').value!="" && document.getElementById('txtAccountName').value=="")
	{
		var accountId=document.getElementById('txtAccountId').value;
		var currency=document.getElementById('txtCurrency').value;
		var status=document.getElementById('txtStatus').value;
		
		loadTransTable(accountId,"All",currency,status);
	}else
	{
		var accountId=document.getElementById('txtAccountId').value;
		var accountName=document.getElementById('txtAccountName').value;
		var currency=document.getElementById('txtCurrency').value;
		var status=document.getElementById('txtStatus').value;
		loadTransTable(accountId,accountName,currency,status);
	}
}

	function loadTransTable(accountId,accountName,currency,status){
		var accountID=document.getElementById('txtAccountID').value;
	    document.getElementById('qry_resultTotal').innerHTML='';
	    tableTransHistoryTodayTotal(accountId,accountName,currency,status,accountID);
	}
	function getAccountIDforCashier(aElement) {
		 var accountIDvar = aElement.href.split("&");
		 var accountID=accountIDvar[1].split("=");
		
		    document.getElementById('accountID').value=accountID[1];
		    $createCashierDialog.dialog('open');

	 }
	 function chkPermission()
	 {
		 var userType="<?php echo yii::app()->session['level'] ?>";
		 if (userType==3){
			alert('You have no permission in this operation.');
		}
	 }
	//SET BONUS DIALOG------------------------------------------------------- 
	var $createCashierDialog='';
	$(document).ready(function() {
		$createCashierDialog = $('<div></div>')
			.html('<body><div style="font-size: 9pt; position: relative;left: 0px; top:1px; background-color: #807D7D; width: 522px;height:100px;border-radius: 2px; ">'
			+'<div style="position: relative; background-color: #CFCCCC; width: 522px;height:96px;top:2px">'
			+'<div style="position: relative; width: 510px;height:30px;top: 5px; left:10px">'
			+'<form method="post" action="" >'
			+'<table>'
			+'<tr><td style="width: 100px"><b><?php echo Yii::t('agent','agent.subcompanylist.accountid');?></b></td><td><input style=" background-color:transparent; border: 0px" type="text" id="accountID" name="accountID" readonly></input></td></tr>'
			+'<tr><td style="width: 100px"><b><?php echo Yii::t('agent','agent.subcompanylist.amount');?></b></td><td><input type="text" id="amount" name="amount"></input><label style="color:red">*</label><label> ( <?php echo Yii::t('agent','agent.subcompanylist.minus');?>)</label></td></tr>'
			+'<tr><td></td><td><input id="submitBonus" type="button" style="width: 100px; height: 27px" value="<?php echo Yii::t('agent','agent.subcompanylist.confirm');?>" onclick="javascript:validateBonus();"></input></td></tr>'
			+'</table>'
			+'<div align="center"></div>'
			+'</form>'
			+'</div></body>'
				)
			.dialog({
				autoOpen: false,
				width: 540,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.setbonus');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 $('#list2').trigger("reloadGrid");
          		}
			});
	});

	function confirmBonus()
	{

		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerCashier/AgentPlayerCashier',
    		type: 'POST',
    		async:false,
    		data: {'accountID': document.getElementById('accountID').value, 
	    		   'amount' :document.getElementById('amount').value
	    		   },
    		context: '',
    		success: function(data) {
    			 $createCashierDialog.dialog('close');
    			 alert('<?php echo Yii::t('agent','agent.subcompanylist.bonusprocess');?>!');
    			 $('#list2').trigger("reloadGrid");
	    	}
    	});
	}
	function validateBonus()
	{
		var pattern = /^[-\0-9]+(.[0-9]{1,2})?$/; 
		var text = document.getElementById('amount').value;
		
	    if (text.match(pattern)==null) 
	    {
			alert('<?php echo Yii::t('agent','agent.subcompanylist.invalidvalue');?>.');
			return false;
	    }
		else
		{
			confirmBonus();
			return true;
		}
		
	    
	}

	//TRANSFER ALL PROCESS================================================================================
	function transferAllProcess() {
		var transferAll=confirm("Are you sure you want to do transfer all process?");
		if (transferAll==true){
			jQuery.ajax({
				url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferAllProcess',
				type: 'POST',
				data: {'upperAccountId' : document.getElementById('lblUpperAccountId').innerHTML},
				context: '',
				async:false,
				success: function(data){
					//alert(data);
					$('#divTranferResult').show();
					document.getElementById('ulTransferResult').innerHTML=data;
					$("#list2").trigger("reloadGrid");
		    	}
			});
		}
	}

	//TRANSFER DEPOSIT DIALOG------------------------------------------------------- 
	function getAccountIDforTransfer(aElement) {
		 var accountIDvar = aElement.href.split("&");
		 var accountID=accountIDvar[1].split("=");
		 document.getElementById('accountDepositID').value=accountID[1];
		 upperAgentRecord();
	 }
	function upperAgentRecord(){
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferProcess',
			type: 'POST',
			data: {'task': 'getUpperAgentRecord', 'AccountID' : document.getElementById('accountDepositID').value},
			context: '',
			async:false,
			success: function(data){
				var rec=data.split("#");
				var toDeposit=(rec[1]*-1);
				var toWithdraw=rec[1];
				//for deposit
				document.getElementById('upperAccount').value=rec[2];
				document.getElementById('upperBalance').value=parseFloat(rec[0]).toFixed(2);
				document.getElementById('winloss').value=parseFloat(rec[1]).toFixed(2);
				document.getElementById('toDeposit').value=parseFloat(toDeposit).toFixed(2);
				document.getElementById('tdUpperCreditBalance').innerHTML=rec[3];
				//for withdraw
				document.getElementById('upperAccount1').value=rec[2];
				document.getElementById('upperBalance1').value=parseFloat(rec[0]).toFixed(2);
				document.getElementById('winloss1').value=parseFloat(rec[1]).toFixed(2);
				document.getElementById('toWithdraw').value=parseFloat(toWithdraw).toFixed(2);
				document.getElementById('tdUpperCreditBalance1').innerHTML=rec[3];
	    	}
		});
	}

	function agentPlayerDepositConfirm(){
		var pattern = /^[0-9]+(.[0-9]{1,2})?$/; 
		var isAmount=document.getElementById('toDeposit').value;
		var tryAmount=document.getElementById('amountToDeposit').value;
		var upperBalance =document.getElementById('upperBalance').value;

		if (parseFloat(tryAmount) <= parseFloat(isAmount) && tryAmount.match(pattern)!=null && parseFloat(tryAmount) <= parseFloat(upperBalance)){
			jQuery.ajax({
				url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferProcess',
				type: 'POST',
				data: {'task': 'AgentPlayerDepositConfirm', 'AccountID' : document.getElementById('accountDepositIDIn').value,'amountDeposit' : document.getElementById('amountToDeposit').value},
				context: '',
				async:false,
				success: function(data){
					$createDepositDialog.dialog('close');
					alert(data);
					$("#list2").trigger("reloadGrid");
			    }
			});

		}else if (tryAmount.match(pattern)!=null && parseFloat(tryAmount) > parseFloat(upperBalance)){
			alert("<?php echo Yii::t('agent','agent.subcompanylist.deposithigher');?>.");

		}else if (parseFloat(tryAmount) > parseFloat(isAmount) && tryAmount.match(pattern)!=null && parseFloat(tryAmount) <= parseFloat(upperBalance)){
			alert("<?php echo Yii::t('agent','agent.subcompanylist.depositamounthigher');?>.");

		}else{
			alert('<?php echo Yii::t('agent','agent.subcompanylist.invalidvalue');?>.');

		}
	}
	//FUNCTION FOR CHECKING IF THE PLAYER IS STILL ONLINE ON A LOBBY
	var isProcessing = false;
	
	function checkPlayerOnlineStat(){
		if(isProcessing){
		       return;
		}
		isProcessing = true;
		
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferProcess',
			type: 'POST',
			data: {'task': 'checkPlayerOnlineStat', 'AccountID' : document.getElementById('accountDepositID').value},
			context: '',
			async:false,
			success: function(data){
				isProcessing = false;
				var rec=data.split("#");
				document.getElementById('accountDepositIDIn').value=rec[2];
				document.getElementById('accountWithdrawIDIn').value=rec[2];
				document.getElementById('accountDepositID0').value=rec[2];
				if (rec[2]!='die'){
					if (parseFloat(rec[0])==0 && parseFloat(rec[1]) < 0 ){
						//this is for deposit
						$createDepositDialog.dialog('open');
						return false;
					}
					else if (parseFloat(rec[0])==0 && parseFloat(rec[1])== 0){
						//this is a dialog if the balance is zero
						$createInvalidDialog.dialog('open');
						return false;
					}
					else if (parseFloat(rec[0])==0 && parseFloat(rec[1])> 0){
						//this is for withdraw 
						$createWithdrawDialog.dialog('open');
						return false;
					}
					else {
						if(rec[2]=='w'){
							$createWaitWithdrawProcessDialog.dialog('open');
							$(".ui-dialog-titlebar-close").hide();
							var check =setInterval(function(){
								jQuery.ajax({
									url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerLobbyWithdrawProcess',
									type: 'POST',
									data: {'AccountID' : document.getElementById('accountDepositID').value},
									context: '',
									async:false,
									success: function(data){
										isProcessing = false;
										if (data==0){
											$(".ui-dialog-titlebar-close").show();
											$createWaitWithdrawProcessDialog.dialog('close');
											clearInterval(check);
											$('#list2').trigger("reloadGrid"); 
											return false;
										}else if(data=='h'){
											$(".ui-dialog-titlebar-close").show();
											$createWaitWithdrawProcessDialog.dialog('close');
											alert('Cannot connect to HTV withdrawal API Server.Please try again!');
											clearInterval(check);
											return false;
										}else if(data=='s'){
											$(".ui-dialog-titlebar-close").show();
											$createWaitWithdrawProcessDialog.dialog('close');
											alert('Cannot connect to Savan withdrawal API Server.Please try again!');
											clearInterval(check);
											return false;
										}
							    	}
								});
								},2000);
								
						}else{
							//this is for force logout
							$createForceLogoutDialog.dialog('open');
							$('#imgLogout').hide();
							$(".ui-dialog-titlebar-close").hide();
						}
					}
				}else{
					window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login";
				}
	    	}
		});
	}
	var $createDepositDialog='';
	$(document).ready(function() {
		$createDepositDialog = $('<div></div>')
			.html('<body><div style="font-size: 9pt; position: relative;left: 0px; top:1px; background-color: #807D7D; width: 472px;height:180px;border-radius: 2px; ">'
			+'<div style="position: relative; background-color: #CFCCCC; width: 472px;height:175px;top:2px">'
			+'<div style="position: relative; width: 450px;height:25px;top: 5px; left:10px">'
			+'<div><b><?php echo Yii::t('agent','agent.subcompanylist.accountid');?>: <input style=" background-color:transparent; border: 0px" type="text" id="accountDepositIDIn" name="accountDepositIDIn" readonly></input></b></div>'
			+'<form method="post" action="" >'
			+'<table style="border-collapse:collapse; width:450px">'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.upperaccount');?></td><td align="right"><input id="upperAccount" style="background-color:transparent; width:120px; border:0;text-align:right"  readonly></td></tr>'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td id="tdUpperCreditBalance" style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.upperbalance');?></td><td align="right"><input id="upperBalance" style="background-color:transparent; width:120px; border:0;text-align:right" readonly></td></tr>'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.winloss');?></td><td align="right"><input id="winloss" style="background-color:transparent; width:120px; border:0;text-align:right" readonly></td></tr>'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.enteramount');?></td><td align="right"><input style="width:100px" type="text" id="amountToDeposit" name="amountToDeposit"></input> <label style="color:red"><= <?php echo Yii::t('agent','agent.subcompanylist.thisamount');?></label><input id="toDeposit" style="background-color:transparent; width:110px; border:0;text-align:right" readonly></td></tr>'
			+'</table><br/>'
			+'<div align="center" style="padding-top:23px;"><input type="button" onclick="javescript: return agentPlayerDepositConfirm();" class="btn red" value="<?php echo Yii::t('agent','agent.subcompanylist.confirm');?>"></input></div>'
			+'</form>'
			+'</div></body>'
				)
			.dialog({
				autoOpen: false,
				width: 500,
				height:275,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.deposit');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 $('#list2').trigger("reloadGrid");
          		}
			});
	});
	var $createInvalidDialog='';
	$(document).ready(function() {
		$createInvalidDialog = $('<div></div>')
			.html('<body><div style="font-size: 9pt; position: relative;left: 0px; top:1px; background-color: #807D7D; width: 472px;height:80px;border-radius: 2px; ">'
			+'<div style="position: relative; background-color: #CFCCCC; width: 472px;height:75px;top:2px">'
			+'<div style="position: relative; width: 450px;height:25px;top: 5px; left:10px">'
			+'<div><b><?php echo Yii::t('agent','agent.subcompanylist.accountid');?>: <input style=" background-color:transparent; border: 0px" type="text" id="accountDepositID0" name="accountDepositID0" readonly></input></b></div>'
			+'<div><?php echo Yii::t('agent','agent.subcompanylist.winloss_zero');?>.</div>'
			+'</div></body>'
				)
			.dialog({
				autoOpen: false,
				width: 500,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.transfer');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 $('#list2').trigger("reloadGrid");
          		}
			});
	});
	var $createForceLogoutDialog='';
	$(document).ready(function() {
		$createForceLogoutDialog = $('<div></div>')
			.html( '<div>'
			 +'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
			 +'<tr><td class="row"><img width=100px src="<?php echo Yii::app()->request->baseUrl ?>/images/process_warning.png" /></td><td class="row1"><b><font color="red"><?php echo Yii::t('agent','agent.subcompanylist.mustlogout');?></font></b></td></tr>'
			 +'<tr><td class="row"></td><td class="row1"><input id="accountDepositID" name="accountDepositID" class="depConfirmInput"  readonly="true" hidden="true"/></b></td></tr>'
			 +'<tr><td class="row"></td><td class="row1"><input class="btn red" id="btnForceLogout" type="button" value="Force Logout" onclick="javascript:logoutPlayer();" />&nbsp;&nbsp;<input sclass="btn red" id="btnCancelLogout" type="button" value="Cancel" onclick="javascript:$createForceLogoutDialog.dialog(\'close\');" /></td></tr>'
			 +'</table><img id="imgLogout" style="cursor:pointer; width:470px;height:20px" src="<?php echo $this->module->assetsUrl; ?>/images/logout-loader.gif" ></div>'
				)
			.dialog({
				autoOpen: false,
				closeOnEscape: false,
				width: 500,
				heigth:120,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.warning');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 $('#list2').trigger("reloadGrid");
          		}
			});
	});

	var $createWaitWithdrawProcessDialog='';
	$(document).ready(function() {
		$createWaitWithdrawProcessDialog = $('<div></div>')
			.html( '<div id="processWithdraw">'
			 +'<font color="red">' + '<?php echo Yii::t('agent','agent.subcompanylist.transfer_casino_lobby');?>' + '</font>'
			 +'</br></br>'
			 +'</table><img id="imgLogout1" style="cursor:pointer; width:400px;height:20px" src="<?php echo $this->module->assetsUrl; ?>/images/logout-loader.gif" ></div>'
				)
			.dialog({
				autoOpen: false,
				closeOnEscape: false,
				width: 420,
				height:50,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.transfer_casino_lobby_title');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 $('#list2').trigger("reloadGrid");
          		}
			});
	});

	function disableAnchor(obj, disable){
		//alert(obj);
		if(disable){
		var href = obj.getAttribute("href");
		if(href && href != "" && href != null){
		obj.setAttribute('href_bak', href);
		}
		obj.removeAttribute('href');
		obj.style.color="gray";
		}
		else{
		obj.setAttribute('href', obj.attributes
		['href_bak'].nodeValue);
		obj.style.color="blue";
		}
	}
	
	//Logout function
	function logoutPlayer()
	{
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferProcess',
			type: 'POST',
			data: {'task': 'logout', 'AccountID' : document.getElementById('accountDepositID').value},
			context: '',
			success: function(data){
				document.getElementById("btnForceLogout").disabled = true;
				document.getElementById("btnCancelLogout").disabled = true;
				$(".ui-dialog-titlebar-close").show();
				$createForceLogoutDialog.dialog('close');
				 $('#list2').trigger("reloadGrid");
				document.getElementById("btnForceLogout").disabled = false;
				document.getElementById("btnCancelLogout").disabled = false;
				
	    	}
		});
	}
	function startCounting(){
		var check=null;
		var check =setInterval(function(){
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferProcess',
			type: 'POST',
			data: {'task': 'checkIfStillProcessing', 'AccountID' : document.getElementById('accountDepositID').value},
			context: '',
			success: function(data){
				if (data==0){
					$(".ui-dialog-titlebar-close").show();
					$createForceLogoutDialog.dialog('close');
					document.getElementById("btnForceLogout").disabled = false;
					document.getElementById("btnCancelLogout").disabled = false;
					clearInterval(check);
					 $('#list2').trigger("reloadGrid");
				}
	    	}
		});
		},2000);
	}
	//-----------------------------------------------------------------------
	
	//TRANSFER WITHDRAWAL DIALOG---------------------------------------------
	var $createWithdrawDialog='';
	$(document).ready(function() {
		$createWithdrawDialog = $('<div></div>')
			.html('<body><div style="font-size: 9pt; position: relative;left: 0px; top:1px; background-color: #807D7D; width: 472px;height:180px;border-radius: 2px; ">'
			+'<div style="position: relative; background-color: #CFCCCC; width: 472px;height:175px;top:2px">'
			+'<div style="position: relative; width: 450px;height:25px;top: 5px; left:10px">'
			+'<div><b><?php echo Yii::t('agent','agent.subcompanylist.accountid');?>: <input style=" background-color:transparent; border: 0px" type="text" id="accountWithdrawIDIn" name="accountWithdrawIDIn" readonly></input></b></div>'
			+'<form method="post" action="" >'
			+'<table style="border-collapse:collapse; width:450px">'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.upperaccount');?></td><td align="right"><input id="upperAccount1" style="background-color:transparent; width:120px; border:0;text-align:right"  readonly></td></tr>'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td id="tdUpperCreditBalance1" style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.upperbalance');?></td><td align="right"><input id="upperBalance1" style="background-color:transparent; width:120px; border:0;text-align:right" readonly></td></tr>'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.winloss');?></td><td align="right"><input id="winloss1" style="background-color:transparent; width:120px; border:0;text-align:right" readonly></td></tr>'
			+'<tr style="border:1px solid #A3A3A3; height:25px"><td style="width: 120px"><?php echo Yii::t('agent','agent.subcompanylist.enteramount');?></td><td align="right"><input style="width:100px" type="text" id="amountToWithdraw" name="amountToWithdraw"></input> <label style="color:red"><= <?php echo Yii::t('agent','agent.subcompanylist.thisamount');?></label><input id="toWithdraw" style="background-color:transparent; width:110px; border:0;text-align:right" readonly></td></tr>'
			+'</table><br/>'
			+'<div align="center" style="padding-top:23px;"><input type="button" onclick="javescript:agentPlayerWithdrawConfirm();" class="btn red" value="<?php echo Yii::t('agent','agent.subcompanylist.confirm');?>"></input></div>'
			+'</form>'
			+'</div></body>'
				)
			.dialog({
				autoOpen: false,
				width: 500,
				height:275,
				title: '<?php echo Yii::t('agent','agent.subcompanylist.withdraw');?>',
				resizable: false,
				modal: true,
				cache:false,
				close: function () {
               	 $('#list2').trigger("reloadGrid");
          		}
			});
	});
	function agentPlayerWithdrawConfirm(){
		var pattern = /^[0-9]+(.[0-9]{1,2})?$/; 
		var isAmount1=document.getElementById('toWithdraw').value;
		var tryAmount1=document.getElementById('amountToWithdraw').value;

		
		if (parseFloat(tryAmount1) <= parseFloat(isAmount1) && tryAmount1.match(pattern)!=null){

			jQuery.ajax({
				url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentPlayerTransfer/AgentPlayerTransferProcess',
				type: 'POST',
				data: {'task': 'AgentPlayerWithdrawConfirm', 'AccountID' : document.getElementById('accountWithdrawIDIn').value,'amountWithdraw' : document.getElementById('amountToWithdraw').value},
				context: '',
				async:false,
				success: function(data){
					$createWithdrawDialog.dialog('close');
					alert(data);
					$("#list2").trigger("reloadGrid");
			    }
			});

		}else if (parseFloat(tryAmount1) > parseFloat(isAmount1) && tryAmount1.match(pattern)!=null){
			alert('<?php echo Yii::t('agent','agent.subcompanylist.withdrawhigher');?>.');

		}else{
			alert('<?php echo Yii::t('agent','agent.subcompanylist.invalidvalue');?>.');

		}
	}
	//-----------------------------------------------------------------------
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
.ui-jqgrid tr.jqgrow td {
        word-wrap: break-word; /* IE 5.5+ and CSS3 */
        white-space: pre-wrap; /* CSS3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        overflow: hidden;
        height: auto;
        vertical-align: middle;
        padding-top: 3px;
        padding-bottom: 3px
    }
.ui-jqgrid-sortable { 
	height: 33px!important; 
	white-space: normal!important; 
	vertical-align: text-middle;
}
</style>
</head>
<body onload="javascript: chkBtn();createDiv();hideDiv('dvStatus')">


<div style="position: relative;left: 5px; top:3px">
	<!-- <div style="background-color: #9C9A9A; border-radius: 2px; width:935px; height: 130px; position: relative;"> -->
		<div style="background-color: #333333; border-radius: 2px; width: 919px; height: 20px;  position: relative; top: 2px; left: 3px;padding:5px;"><label id="lblUpperAccountId" style="color:#D84A38;font-weight: bold;"><?php echo $_GET['account_id'];?></label> <font color="white"><?php echo Yii::t('agent','agent.subcompanylist.info');?></font></div>
		<div id="agent_info" style="background-color: #807D7D;color:#2B2A2A; border-radius: 2px;width: 929px; height: 23px;  position: relative; margin-top: 2px;margin-bottom:1px; left: 3px">
		<!-- <div style="background-color: #807D7D;color:#2B2A2A; border-radius: 2px;width: 929px; height: 20px;  position: relative; top: 10px; left: 3px"> --><?php echo Yii::t('agent','agent.subcompanylist.level');?>: Agent |
		<?php 
		$cp=new AgentMember();
		$getTotalCredit=$cp->getAgentMemberTotalCredit();
		//this is for the total company credit
		foreach ($getTotalCredit as $row){echo Yii::t('agent','agent.subcompanylist.availablecredit'),': '.$row['credit'].' | '.Yii::t('agent','agent.subcompanylist.currency').': '.$row['currency_name'].' | '.Yii::t('agent','agent.subcompanylist.status').': '.$row['status'].' | <a class="btn mini red" href="index.php?r=Agent/AgentSubCompanyLogAll">'.Yii::t('agent','agent.subcompanylist.log').' <i class="icon-reorder"></i></a> ';}
		?>
		</div>
		<!-- New sharing and commission structure by leokarl
			<table style="background-color: #CFCCCC; width: 99.9%">
				<tr style="background-color: #807D7D; color:#2B2A2A;"><td style="width: 150px;"></td><td style="width: 120px; color: white">Baccarat</td><td style="width: 120px; color: white">European Roulette</td><td style="width: 120px; color: white">American Roulette</td><td  style="width: 120px; color: white">Dragon Tiger</td><td style="width: 120px; color: white">Blackjack</td><td style="width: 120px; color: white">Slots</td></tr>
				<?php 
					$cp=new AgentMember();
					$getSharing=$cp->getAgentSharingAndCommission();
					echo '<tr style="color: #440808"><td>'.Yii::t('agent','agent.parameter.sharing').'</td>';
					foreach ($getSharing as $row){echo '<td>'.$row['share_baccarat'].'%</td><td>'.$row['share_roulette'].'%</td><td>'.$row['share_american_roulette'].'%</td><td>'.$row['share_dragon_tiger'].'%</td><td>'.$row['share_blackjack'].'%</td><td>'.$row['share_slots'].'%</td>';}	
					echo '</tr>';
					$cp=new AgentMember();
					$getCommission=$cp->getAgentSharingAndCommission();
					echo '<tr style="color: #440808"><td>'.Yii::t('agent','agent.parameter.commission').'</td>';
					foreach ($getCommission as $row){echo '<td>'.$row['commission_baccarat'].'%</td><td>'.$row['commission_roulette'].'%</td><td>'.$row['commission_american_roulette'].'%</td><td>'.$row['commission_dragon_tiger'].'%</td><td>'.$row['commission_blackjack'].'%</td><td>'.$row['commission_slots'].'%</td>';}
					echo '</tr>';
					?>
			</table>
		-->
		<?php 
		 $cp=new AgentMember();
		 $getSharingCommission=$cp->getAgentSharingAndCommission();
		 foreach ($getSharingCommission as $row){
		 	$share_baccarat=split('#',$row['share_baccarat']);
		 	$share_roulette=split('#',$row['share_roulette']);
		 	$share_american_roulette=split('#',$row['share_american_roulette']);
		 	$share_dragon_tiger=split('#',$row['share_dragon_tiger']);
		 	$share_blackjack=split('#',$row['share_blackjack']);
		 	$share_slots=split('#',$row['share_slots']);
		 	$commission_baccarat=split('#',$row['commission_baccarat']);
		 	$commission_roulette=split('#',$row['commission_roulette']);
		 	$commission_american_roulette=split('#',$row['commission_american_roulette']);
		 	$commission_dragon_tiger=split('#',$row['commission_dragon_tiger']);
		 	$commission_blackjack=split('#',$row['commission_blackjack']);
		 	$commission_slots=split('#',$row['commission_slots']);
		 }
		 ?>
		 
		 	<table id="tblAgentSeniorMaster" style="background-color: #CFCCCC; width: 929px" cellpadding="0" cellspacing="0">
			 	<tr>
			 		<th class="agentsharingcommission"><div class="agentsharingcommissionheader">Games</div></th>
			 		<th class="agentsharingcommission" colspan="2"><center><div class="agentsharingcommissionheader">Costavegas999</div></center></th>
			 		<th class="agentsharingcommission" colspan="2"><center><div class="agentsharingcommissionheader">Hatienvegas999</div></center></th>
			 		<th class="agentsharingcommission" colspan="2"><center><div class="agentsharingcommissionheader">Savanvegas999</div></center></th>
			 		<th class="agentsharingcommission" colspan="2"><center><div class="agentsharingcommissionheader">Virtuavegas999</div></center></th>
			 	</tr>
			 	<tr>
			 		<td class="agentsharingcommission"></td>
			 			<td class="agentsharingcommission"><i><b>Sharing</b></i></td><td class="agentsharingcommission"><i><b>Commission</b></i></td>
			 			<td class="agentsharingcommission"><i><b>Sharing</b></i></td><td class="agentsharingcommission"><i><b>Commission</b></i></td>
			 			<td class="agentsharingcommission"><i><b>Sharing</b></i></td><td class="agentsharingcommission"><i><b>Commission</b></i></td>
			 			<td class="agentsharingcommission"><i><b>Sharing</b></i></td><td class="agentsharingcommission"><i><b>Commission</b></i></td>
			 	</tr>
			 	<tr>
			 		<td class="agentsharingcommission">Baccarat</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_baccarat[0],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_baccarat[0],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_baccarat[1],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_baccarat[1],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_baccarat[2],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_baccarat[2],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_baccarat[3],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_baccarat[3],2);?>%</td>
			 	</tr>
			 	<tr>
			 		<td class="agentsharingcommission">Roulette</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_roulette[0],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_roulette[0],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_roulette[1],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_roulette[1],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_roulette[2],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_roulette[2],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_roulette[3],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_roulette[3],2);?>%</td>
			 	</tr>
			 	<tr>
			 		<td class="agentsharingcommission">Blackjack</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_blackjack[0],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_blackjack[0],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_blackjack[1],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_blackjack[1],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_blackjack[2],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_blackjack[2],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_blackjack[3],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_blackjack[3],2);?>%</td>
			 	</tr>
			 	<tr>
			 		<td class="agentsharingcommission">Dragon Tiger</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_dragon_tiger[0],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_dragon_tiger[0],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_dragon_tiger[1],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_dragon_tiger[1],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_dragon_tiger[2],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_dragon_tiger[2],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_dragon_tiger[3],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_dragon_tiger[3],2);?>%</td>
			 	</tr>
			 	<tr>
			 		<td class="agentsharingcommission">American Roulette</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_american_roulette[0],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_american_roulette[0],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_american_roulette[1],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_american_roulette[1],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_american_roulette[2],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_american_roulette[2],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_american_roulette[3],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_american_roulette[3],2);?>%</td>
			 	</tr>
			 	<tr>
			 		<td class="agentsharingcommission">Slots</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_slots[0],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_slots[0],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_slots[1],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_slots[1],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_slots[2],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_slots[2],2);?>%</td>
			 			<td class="agentsharingcommission"><?php echo number_format($share_slots[3],2);?>%</td><td class="agentsharingcommission"><?php echo number_format($commission_slots[3],2);?>%</td>
			 	</tr>
		 	</table>
	<br/>
	<div style="width: 1119px; height: 35px;background-color: #333333;border-radius: 2px; position: relative; color:#FFFFFF; ">
		<table>
			<tr>
				<td style="padding-left: 10px; padding-right: 10px;"><?php echo Yii::t('agent','agent.subcompanylist.accountid');?>:</td><td><input style="width: 90px" type="text" id="txtAccountId" onkeypress="return runKeyPress(event)"></input></td>
				<td style="padding-left: 10px; padding-right: 10px;"><?php echo Yii::t('agent','agent.subcompanylist.accountname');?>:</td><td><input style="width: 90px" type="text" id="txtAccountName" onkeypress="return runKeyPress(event)"></input></td>
				<td style="padding-left: 10px; padding-right: 10px;"><?php echo Yii::t('agent','agent.subcompanylist.currency');?>:</td>
				<td style="padding-left: 10px; padding-right: 10px;"><select id="txtCurrency" onkeypress="return runKeyPress(event)">
						<option value="All"><?php echo Yii::t('agent','agent.subcompanylist.all');?></option>
							<?php 
								$dataReader = AgentCurrencyType::model()->findAll();
								foreach ($dataReader as $row){
									echo '<option value="' . strtoupper($row['currency_name']) . '">'. strtoupper($row['currency_name']) . '</option>';
								}
							?>
					</select>
				</td>
				<td style="padding-left: 10px; padding-right: 10px;"><?php echo Yii::t('agent','agent.subcompanylist.status');?>:</td>
				<td style="padding-left: 10px; padding-right: 10px;"><select id="txtStatus" onkeypress="return runKeyPress(event)">
						<option value="All"><?php echo Yii::t('agent','agent.subcompanylist.all');?></option>
						<option value="Active"><?php echo Yii::t('agent','agent.subcompanylist.active');?></option>
						<option value="Close"><?php echo Yii::t('agent','agent.subcompanylist.close');?></option>
					</select>
				</td>
				<td style="padding-left: 10px; padding-right: 10px;"><button class="btn red" id="btnSearch" onclick="javascript: chkBtn();"><?php echo Yii::t('agent','agent.subcompanylist.search');?> <i class="icon-search"></i></button></td>
			</tr>
		</table>
	</div>
	<br/>
	<div id="divTranferResult" style="width: 935px; display:none;">
		<div id="tranferResultTitle"><a href="javascript:;" onclick="javascript: $('#divTranferResult').hide();"><img style="vertical-align: middle;" src="<?php echo $this->module->assetsUrl;?>/images/hide_icon.jpg"></a><div>TRANSFER RESULT:</div></div>
		<div id="ulTransferResult">
		</div>
	</div>
	<div id="qry_resultTotal">
	</div>
	<div id="pager1"></div>
	<input type="hidden" id="txtAccountID" value="<?php echo $_GET['account_id'];?>">
</div>

</body>
</html>