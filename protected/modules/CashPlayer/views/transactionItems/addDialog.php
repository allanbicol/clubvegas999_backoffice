<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td class="left">Transaction Item</td>
	<td class="right"><?php echo CHtml::TextField('txtTransactionItem','',array('id'=>'txtTransactionItem')); ?></td>
</tr>
<tr>
	<td class="left">Type</td>
	<td class="right">
		<?php echo CHtml::dropDownList('cmbTransactionType','0', 
				Array('6'=>'Bonus','2'=>'Deposit','4'=>'Withdraw'));?>
	</td>
</tr>
<tr>
	<td class="left">Bank Transaction</td>
	<td class="right">
		<?php echo CHtml::checkBox('chkBankTransaction', false, 
				array('id' => 'chkBankTransaction'));?>
	</td>
</tr>
		
</table>
<br/>
<hr>
<?php
	// submit button
	echo CHtml::button('Submit', array('id'=>'btnSubmit', 
			'onclick'=>'addTransactionItem($("#txtTransactionItem").val(),$("#cmbTransactionType").val(),document.getElementById("chkBankTransaction"));','class'=>'btn red'));
	// cancel button
	echo CHtml::button('Cancel', array('id'=>'btnCancel', 'onclick'=>'$("#add_transaction_item_dialog").dialog("close");','class'=>'btn red'));
?>