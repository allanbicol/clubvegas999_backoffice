<?php
/**
 * @todo
 * @copyright	CE
 * @author	Kimny MOUK
 * @since	2012-04-24
 */

class EmailSettingController extends MyController
{
	
	public function actionSignupEmailList()
	{
		$cp = new ProcessEmailSetting();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$cp->getCountSignUpEmailList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
	
		$startIndex = $limit*$page - $limit;
		$user_records=$cp->getSignUpEmailList($orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("id","email_desc","id");
		$urlAS= Yii::app()->request->baseUrl;
// 		if(Yii::app()->user->checkAccess('systemSetting.writeCurrencySetting')){
// 			$htmValue='<a class=\'update-currency\' href=\'#\' onclick=\'javascript: alertmsg();\'>Update</a>';
// 		}else{
 			$htmValue='  ';
// 		}
	
		echo JsonUtil::jsonJqgridSignUpEmailList($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,3);
	}
	
	public function actionGetEmailSetting()
	{
		$em = new ProcessEmailSetting();
		$em->getEmailSetting();
	}
	public function actionSaveEmailSetting()
	{
		if(Yii::app()->user->checkAccess('systemSetting.writeEmailSetting'))
		{
			$em = new ProcessEmailSetting();
			$em->saveEmailSetting();
		}
	}
	public function actionSaveEmailSetting1()
	{
		if(Yii::app()->user->checkAccess('systemSetting.writeEmailSetting'))
		{
			$em = new ProcessEmailSetting();
			$em->saveEmailSettingNew();
		}
	}
	
	public function actionSetSignupSetting()
	{
		Yii::app()->session['signupEmail']=$_POST['desc'];
		//echo Yii::app()->session['signupEmail'];
		
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT lang_id,email_detail FROM tbl_email_setting_new WHERE email_type=1 AND email_desc='".Yii::app()->session['signupEmail']."' ORDER BY lang_id");
		$rows = $command->query();
		$rows1=$rows->readAll();
		$res='';
		foreach ($rows1 as $row){
			$res.= $row['email_detail']."||";
		}
		echo Yii::app()->session['signupEmail'].'||'.$res;
	}
	public function actionIndex()
	{
// 		$email = new EmailSetting;
		
// 		$emailLanaguage = isset($_POST['emailChoseLanguage'])? $_POST['emailChoseLanguage'] : 1; 
// 		$records = $email->getEmailSetting($emailLanaguage, 1);
		
// 		$this->render("index", array("emailSetting" => $records));
		if(Yii::app()->user->checkAccess('systemSetting.readEmailSetting'))
		{
			if (!isset(Yii::app()->session['signupEmail'])){
				Yii::app()->session['signupEmail']='Email Verification';
			}
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
}