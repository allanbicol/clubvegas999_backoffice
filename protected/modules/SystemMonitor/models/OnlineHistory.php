<?php
/**
 * @todo Online History Model
 * @copyright CE
 * @author Leo karl
 * @since 5/16/2012
 *
 */

Class OnlineHistory
{
	public function getOnlineHistory($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command=$connection->createCommand("
				SELECT a.account_id,
			    (case when a.player_type='c' then 'Cash Player' when a.player_type='a' then 'Agent Player' end) as account_type,
			    (Select count(*) from tbl_casino_login_history where account_id=a.account_id and casino_id=3 and session>='". $_GET['dtfrom'] ."' and session<='". $_GET['dtto'] ."') as clubvegas,
			    (Select count(*) from tbl_casino_login_history where account_id=a.account_id and casino_id=1 and session>='". $_GET['dtfrom'] ."' and session<='". $_GET['dtto'] ."') as htv,
			    (Select count(*) from tbl_casino_login_history where account_id=a.account_id and casino_id=2 and session>='". $_GET['dtfrom'] ."' and session<='". $_GET['dtto'] ."') as savan
			    FROM tbl_casino_login_history a 
			    WHERE a.session>='". $_GET['dtfrom'] ."' and a.session<='". $_GET['dtto'] ."'
			    GROUP BY a.account_id
			    ORDER BY $orderField $sortType LIMIT $startIndex,$limit;
				");
		$rows = $command->query();
		return $rows;
	}
	public function countOnlineHistory()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command=$connection->createCommand("Select count(account_id) as reccount from (SELECT a.account_id FROM tbl_casino_login_history a WHERE a.session>='". $_GET['dtfrom'] ."' and a.session<='". $_GET['dtto'] ."' GROUP BY account_id) t");
		$rows = $command->query();
		return $rows;
	}
	
	public function getOnlineHistoryDetails($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command=$connection->createCommand("SELECT * FROM vwGetOnlineHistoryPlayerTransaction WHERE account_id='" . $_GET['account_id'] . "' and login_date>='". $_GET['dtfrom'] ."' and login_date<='". $_GET['dtto'] ."' ORDER BY $orderField $sortType LIMIT $startIndex,$limit");
		$rows = $command->query();
		return $rows;
	}
	public function countOnlineHistoryDetails()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command=$connection->createCommand("SELECT COUNT(0) FROM vwGetOnlineHistoryPlayerTransaction WHERE account_id='" . $_GET['account_id'] . "' and login_date>='". $_GET['dtfrom'] ."' and login_date<='". $_GET['dtto'] ."'");
		$rows = $command->query();
		return $rows;
	}
}