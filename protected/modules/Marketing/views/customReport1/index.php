
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>


<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl;?>/css/customreport.css" /> 
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/jquery.yiigridview.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/customreport.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/customreport.js"></script>
<script type="text/javascript">
var addFilterURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1/SaveFilter';
var loadFilterURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1/LoadSavedFilter';
var deleteFilterURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1/DeleteSavedFilter';
var customReportUrl='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1';
var exportCustomReportURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/CustomReport1/CostumReportCurrentPageExportExcel';
var sessionSelectedItem ='<?php echo yii::app()->session['selected_columns']; ?>';
var sessionUnselectedItem ='<?php echo yii::app()->session['unselected_columns']; ?>';

var pager ='<?php echo yii::app()->session['pager']; ?>';
var dateFrom ='<?php echo yii::app()->session['dateFrom']; ?>';
var dateTo ='<?php echo yii::app()->session['dateTo']; ?>';
var accountID ='<?php echo yii::app()->session['accountID']; ?>';
var currency ='<?php echo yii::app()->session['currency']; ?>';
var operator ='<?php echo yii::app()->session['operator']; ?>';
var winloss ='<?php echo yii::app()->session['winloss']; ?>';
var country ='<?php echo yii::app()->session['country']; ?>';
var email ='<?php echo yii::app()->session['email']; ?>';
var phone ='<?php echo yii::app()->session['phone']; ?>';
var depositOpt ='<?php echo yii::app()->session['deposit']; ?>';
var playerType='<?php echo yii::app()->session['playertype']; ?>';

document.getElementById('marketingHeader').className="start active";
document.getElementById('mnu_custom_report1').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Marketing <i class='icon-angle-right'></i></li><li><a href='#'>Custom Report</a></li>");

</script>

<b>Custom Report</b>
<br><br>


<form id="params" action="<?php echo Yii::app()->request->baseUrl . '/index.php?r=Marketing/CustomReport1';?>" method="post">

<div id="crheader">
<div id="crfilter">
	<div class="title">Filter</div>
	<div class="body">
		<div class="grid_params">
			<table border="0">
			<tr>
				<td class="left">Opening Acc. From</td>
				<td class="right"><input type="text" class="txt" name="txtDateFrom" id="txtDateFrom" value="<?php $from=date("Y-m-d"); echo $from." 00:00:00";?>"></td>
			</tr>
			<tr>
				<td class="left">Opening Acc. To</td>
				<td class="right"><input type="text" class="txt" name="txtDateTo" id="txtDateTo" value="<?php echo date("Y-m-d")." 23:59:59";?>"></td>
			</tr>
			<tr>
				<td class="left">Account ID</td>
				<td class="right"><input type="text" class="txt" name="txtAccountID" id="txtAccountID"></td>
			</tr>
			<tr>
				<td class="left">Player Type</td>
				<td class="right"><select class="txt" name="txtPlayerType" id="txtPlayerType">
					<option value="All">All</option>
					<option value="Aff">Affiliate</option>
					<option value="Cash">Cash Player</option>
				</select></td>
			</tr>
			<tr>
				<td class="left">Currency</td>
				<td class="right"><select class="txt" name="txtCurrency" id="txtCurrency">
					<option value="All">ALL</option>
  					<?php 
					$dataReader = TableCurrency::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . strtoupper($row['currency_name']) . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select></td>
				<!--  <td class="right"><input type="text" class="txt" name="txtCurrency" id="txtCurrency"></td>-->
			</tr>
			<tr>
				<td class="left">Win/Loss</td>
				<td class="right">
				<select name="txtOperator" id="txtOperator">
					<option value="0"></option>
					<option value="1">=</option>
					<option value="2"><=</option>
					<option value="3">>=</option>
				</select>
				<input style="width:100px;" type="text" class="txt" name="txtWinLoss" id="txtWinLoss"></td>
			</tr>
			<tr>
				<td class="left">Country</td>
				<td class="right"><select  style="width:155px;" name="txtCountry" id="txtCountry">
  					<option value="All">ALL</option>
		  				<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='country';  
		  				$criteria->condition='country<>:country_name';
		  				$criteria->group = 'country';
		  				$criteria->params=array(':country_name'=>'');
						$dataReader = TableCashPlayer::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<option value="' . $row['country'] . '">'. strtoupper($row['country']) . '</option>';
						}
						?>
				</select></td>
				<!--  <td class="right"><input type="text" class="txt" name="txtCountry" id="txtCountry"></td>-->
			</tr>
			<tr>
				<td class="left">Email</td>
				<td class="right"><input type="text" class="txt" name="txtEmail" id="txtEmail"></td>
			</tr>
			<tr>
				<td class="left">Phone</td>
				<td class="right"><input type="text" class="txt" name="txtPhone" id="txtPhone"></td>
			</tr>
			<tr>
				<td class="left">Deposit</td>
				<td class="right"><input type="checkbox" class="txt" name="chkDeposit" id="chkDeposit"></td>
			</tr>
			<tr>
				<td class="left">Rows per page</td>
				<td class="right">
				<?php echo CHtml::dropDownList('pageSize',$pageSize,array(25=>25,50=>50,100=>100,200=>200,500=>500,99999=>99999));?>
				</td>
			</tr>
			</table>
		</div>
		
	</div>
</div>
<div id="crgridcolumns">
	<div class="title">Grid Columns</div>
	<div class="body">
			<table border="0" cellpadding="7">
			<tr><td><b>Available</b></td><td><b>Selected</b></td></tr>
			<tr>
				<td>
					<ul id="sortable1" class="connectedSortable">
					<li class="ui-state-default" value="opening_acc_date" id="Opening Account Date">Opening Account Date</li>
					<li class="ui-state-default" value="last_date_played" id="Last Date Played">Last Date Played</li>
					<li class="ui-state-default" value="account_id" id="Account ID">Account ID</li>
					<li class="ui-state-default" value="account_name" id="Account Name">Account Name</li>
					<li class="ui-state-default" value="win_loss" id="Win/Loss">Win/Loss</li>
					<li class="ui-state-default" value="country" id="Country">Country</li>
					<li class="ui-state-default" value="email" id="Email">Email</li>
					<li class="ui-state-default" value="phone_number" id="Phone">Phone</li>
					<li class="ui-state-default" value="currency_name" id="Currency">Currency</li>
					<li class="ui-state-default" value="first_deposit_date" id="First Deposit Date">First Deposit Date</li>
					</ul>
				</td>
				<td>
					<ul id="sortable2" class="connectedSortable">
					<!-- <li class="ui-state-highlight">Item 1</li> -->
					</ul>
				</td>
			</tr>
			</table>
		
	</div>
</div>
<div id="crsavedfilters">
	<div class="title">Saved Filters</div>
	<div class="body">
	<div class="subtitle">Filters</div>
	<ul class="sortable">
	<?php 
		$criteria=new CDbCriteria;
		$criteria->select='id,filter_name';
		$criteria->condition='account_id=:id';
		$criteria->params=array(':id'=>yii::app()->session['account_id']);
		$filters=TableCustomReportFilterSetting::model()->findAll($criteria);
		foreach($filters as $row){
			echo '<li class="ui-state-default" onclick="loadSavedFilters(this.id);" value="'.$row['id'].'" id="'.$row['filter_name'].'">'.$row['filter_name'].'</li>';
		}
		?>	
	</ul>
	<ul id="sortable-delete" class="sortable" "><img style="vertical-align: middle;" width="20px" src="<?php echo $this->module->assetsUrl;?>/images/bin.png"> Drag here to delete filter!</ul>
	</div>
</div>
</div>
<input type="hidden" name="columns" id="columns">
<input type="hidden" name="column_names" id="column_names">
<br/>
<!-- buttons -->
<div id="buttons_wrapper">
	<input class="btn red" type="button" value="Apply" id="btnApply" onclick="btnApplyFunction();">
	<input class="btn red" type="button" value="Clear" onclick="clearFilters();">
	<input class="btn red" type="button" onclick="saveFilter();" value="Save">
	<img id="loader" src="<?php echo Yii::app()->request->baseUrl;?>/images/loader.gif">
</div>
</form>

<h3><div id="grid_report_title"></div></h3>
<!--grid report-->
<div id="grid_report_wrapper">
<?php 

	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider' => $arrayDataProvider,
		'columns' => $columns,
				
	));
	

?>
<!--end grid report-->
</div>
<div id="gridFooter">
<input class="btn red" type="button" value="Export Current Page" id="btnExportCurrentPage" onclick="exportCurrentToExcel();">
<input class="btn red" type="button" value="Export All Pages" id="btnExportAllPages" onclick="exportAllToExcel();">
</div>
<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
</form>
