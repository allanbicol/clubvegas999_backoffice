<?php
/**
 * @todo bind AuthItem CActiveRecord
 * @copyright CE
 * @author Leo karl
 * @since 2013-01-08
 */
class TableAuthItem extends CActiveRecord //CV99ActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'AuthItem';
	}
}