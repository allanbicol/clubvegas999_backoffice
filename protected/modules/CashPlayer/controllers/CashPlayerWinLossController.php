<?php
class CashPlayerWinLossController extends MyController
{
	//for Costa
	public function actionCashPlayerWinLoss()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerWinLoss();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(*)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerWinLoss($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("account_id","account_id","account_name","last_bet_date","currency_name","amount_wager","avg_bet","win_los","commission","amount_tips","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerWinLossCostaTotal()
	{

		$cpHfund = new CashPlayerWinLoss;
		$player_records = $cpHfund->getPlayerWinLossCostaTotal();
		//$test = $records->readAll();
		
		$filedNames = array("currency_name","amount_wager","win_los","commission","amount_tips","total_win_los","vig_winloss","vig_tips","vig_total","p_l","rate","amount_wager","win_los","commission","amount_tips","total_win_los","vig_winloss","vig_tips","vig_total","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), 0, 0, 0,$filedNames,$htmvalue,6);
		
	}
	
	//for HTV 
	public function actionCashPlayerWinLossHTV()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerWinLossHTV();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(*)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerWinLossHTV($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_id","account_name","last_bet_date","currency","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","bonus","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerWinLossHTVTotal()
	{

		$cpHfund = new CashPlayerWinLoss;
		$player_records = $cpHfund->getPlayerWinLossHTVTotal();

		$filedNames = array("currency","bet_count","total_stake","amount_wager","win_los","commission","bonus","total_win_los","p_l","rate","total_stake","amount_wager","win_los","commission","bonus","total_win_los","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
	
	//For savan vegas
	public function actionCashPlayerWinLossSAVAN()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerWinLossSAVAN();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(*)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerWinLossSAVAN($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_id","account_name","last_bet_date","currency","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","bonus","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerWinLossSAVANTotal()
	{

		$cpHfund = new CashPlayerWinLoss;
		$player_records = $cpHfund->getPlayerWinLossSAVANTotal();

		$filedNames = array("currency","bet_count","total_stake","amount_wager","win_los","commission","bonus","total_win_los","p_l","rate","total_stake","amount_wager","win_los","commission","bonus","total_win_los","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
	
	//For savan vegas version 2
	public function actionCashPlayerWinLossSAVANv2()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerWinLossSAVANv2();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(*)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerWinLossSAVANv2($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_id","account_name","last_bet_date","currency","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","amount_tips","bonus","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerWinLossSAVANTotalv2()
	{
	
		$cpHfund = new CashPlayerWinLoss;
		$player_records = $cpHfund->getPlayerWinLossSAVANTotalv2();
	
		$filedNames = array("currency","bet_count","total_stake","amount_wager","win_los","commission","amount_tips","bonus","total_win_los","p_l","rate","total_stake","amount_wager","win_los","commission","amount_tips","bonus","total_win_los","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionIndex()
	{
		//exit("you're in index");
		if(Yii::app()->user->checkAccess('cashPlayer.readWinLoss'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionCostaVegasWinLossExcel(){
		$file="CashPlayerCostaVegas_WinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($params[2] != ''){
			$command = $connection->createCommand("SELECT table_name from tbl_table_management  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$table=$rd['table_name'];
		}else{ $table ='ALL'; }
		
		if ($params[5] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[5] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';}
		
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Winloss for Costavegas</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> ='.$params[1].'\n<b>Table Id</b> = '.$table.'\n<b>Game Id</b> = '.$params[3].'\n<b>Shoes Id</b> ='.$params[4].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[6].'\n<b>Test Currency</b> = '.$params[7].'\n<b>Date From</b> = '.$params[8].'\n<b>Date To</b> = '.$params[9];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionHTVWinLossExcel(){
		$file="CashPlayerHTV_WinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($params[2] != ''){
			$command = $connection->createCommand("SELECT table_name from tbl_table_management  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$table=$rd['table_name'];
		}else{ $table ='ALL';
		}
		
		if ($params[5] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[5] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';
		}
		
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Winloss for HTV</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> ='.$params[1].'\n<b>Table Id</b> = '.$table.'\n<b>Game Id</b> = '.$params[3].'\n<b>Shoes Id</b> ='.$params[4].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[6].'\n<b>Test Currency</b> = '.$params[7].'\n<b>Date From</b> = '.$params[8].'\n<b>Date To</b> = '.$params[9];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionSavanWinLossExcel(){
		$file="CashPlayerSavan_WinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($params[2] != ''){
			$command = $connection->createCommand("SELECT table_name from tbl_table_management  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$table=$rd['table_name'];
		}else{ $table ='ALL';
		}
		
		if ($params[5] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[5] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';
		}
		
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Winloss for Savanvegas</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> ='.$params[1].'\n<b>Table Id</b> = '.$table.'\n<b>Game Id</b> = '.$params[3].'\n<b>Shoes Id</b> ='.$params[4].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[6].'\n<b>Test Currency</b> = '.$params[7].'\n<b>Date From</b> = '.$params[8].'\n<b>Date To</b> = '.$params[9];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	
	public function actionSavanWinLossExcelv2(){
		$file="CashPlayerSavan_WinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($params[2] != ''){
			$command = $connection->createCommand("SELECT table_name from tbl_table_management  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$table=$rd['table_name'];
		}else{ $table ='ALL';
		}
	
		if ($params[5] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[5] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';
		}
	
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Winloss for Savanvegas</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> ='.$params[1].'\n<b>Table Id</b> = '.$table.'\n<b>Game Id</b> = '.$params[3].'\n<b>Shoes Id</b> ='.$params[4].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[6].'\n<b>Test Currency</b> = '.$params[7].'\n<b>Date From</b> = '.$params[8].'\n<b>Date To</b> = '.$params[9];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	
	public function actionCostaVegasWinLossExcelAll()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$result=$cpHfund->getCountPlayerWinLoss();
		$data=$result->readAll();
		$records=$data[0]['COUNT(*)'];

		$player_records = $cpHfund->getExportPlayerWinLoss();

		$fieldNames = array("account_id","account_name","last_bet_date","currency_name","amount_wager","avg_bet","win_los","commission","amount_tips","total_win_los","current_balance","p_l");
		$headerNames=array("Account Id","Account Name","Last Bet Date","Currency","Valid Stake","Average Bet","Winloss","Commission","Tips","Total","Balance","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerWinLossCostaTotal();

		$fieldNames = array("currency_name","amount_wager","win_los","commission","amount_tips","total_win_los","vig_winloss","vig_tips","vig_total","p_l");
		$headerNames=array("Currency","Player Valid Stake","Player Winloss","Player Commission","Player Tips","Player Total","VIG Winloss","VIG Tips","VIG Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
	
	public function actionHTVWinLossExcelAll()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$result=$cpHfund->getCountPlayerWinLossHTV();
		$data=$result->readAll();
		$records=$data[0]['COUNT(*)'];
	
		$player_records = $cpHfund->getExportPlayerWinLossHTV();

		$fieldNames = array("account_id","account_name","last_bet_date","currency","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","bonus","total_win_los","current_balance","p_l");
		$headerNames=array("Account Id","Account Name","Last Bet Date","Currency","Bet Count","Total Stake","Valid Stake","Average Bet","Winloss","Commission","Bonus","Total","Balance","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerWinLossHTVTotal();
		
		$fieldNames = array("currency","bet_count","total_stake","amount_wager","win_los","commission","bonus","total_win_los","p_l");
		$headerNames=array("Currency","Bet Count","Total Stake","Valid Stake","Winloss","Commission","Bonus","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
	public function actionSavanWinLossExcelAll()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$result=$cpHfund->getCountPlayerWinLossSAVAN();
		$data=$result->readAll();
		$records=$data[0]['COUNT(*)'];
	
		$player_records = $cpHfund->getExportPlayerWinLossSAVAN();
	
		$fieldNames = array("account_id","account_name","last_bet_date","currency","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","bonus","total_win_los","current_balance","p_l");
		$headerNames=array("Account Id","Account Name","Last Bet Date","Currency","Bet Count","Total Stake","Valid Stake","Average Bet","Winloss","Commission","Bonus","Total","Balance","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerWinLossSAVANTotal();
	
		$fieldNames = array("currency","bet_count","total_stake","amount_wager","win_los","commission","bonus","total_win_los","p_l");
		$headerNames=array("Currency","Bet Count","Total Stake","Valid Stake","Winloss","Commission","Bonus","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
	
	public function actionSavanWinLossExcelAllv2()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLoss;
		$result=$cpHfund->getCountPlayerWinLossSAVAN();
		$data=$result->readAll();
		$records=$data[0]['COUNT(*)'];
	
		$player_records = $cpHfund->getExportPlayerWinLossSAVANv2();
	
		$fieldNames = array("account_id","account_name","last_bet_date","currency","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","bonus","total_win_los","current_balance","p_l");
		$headerNames=array("Account Id","Account Name","Last Bet Date","Currency","Bet Count","Total Stake","Valid Stake","Average Bet","Winloss","Commission","Bonus","Total","Balance","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerWinLossSAVANTotalv2();
	
		$fieldNames = array("currency","bet_count","total_stake","amount_wager","win_los","commission","bonus","total_win_los","p_l");
		$headerNames=array("Currency","Bet Count","Total Stake","Valid Stake","Winloss","Commission","Bonus","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
}