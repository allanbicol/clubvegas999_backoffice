<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl;?>/css/banking.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/payment.js"></script>
<script type="text/javascript">
	var urlPaymentList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/PaymentList';
	var urlLoadFormDepositPending='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/LoadFormDepositPending';
	var urlLoadFormDeposit='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/LoadFormDeposit';
	var urlLoadFormWithdraw='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/LoadFormWithdraw';
	var urlCheckPlayerStat='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/CheckPlayerStat';
	var dtYesterday='<?php echo date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d"))));?>';
	var urlForceLogOutDialog='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/LoadForceLogOutDialog';
	var assetURL = '<?php echo $this->module->assetsUrl; ?>';
	var baseURL = '<?php echo Yii::app()->request->baseUrl;?>';
	var dtToday='<?php echo date("Y-m-d");?>';
	var lblTransactions = '<?php echo Yii::t('banking','banking.payment.transactions');?>';
	var lblTransactionID = '<?php echo Yii::t('banking','banking.payment.transaction_id');?>';
	var lblDateTime = '<?php echo Yii::t('banking','banking.payment.date_time');?>';
	var lblTransactionType = '<?php echo Yii::t('banking','banking.payment.transaction_type');?>';
	var lblTransactionVia = '<?php echo Yii::t('banking','banking.payment.transaction_via');?>';
	var lblAccountID = '<?php echo Yii::t('banking','banking.payment.account_id_small');?>';
	var lblFullname = '<?php echo Yii::t('banking','banking.payment.fullname');?>';
	var lblCurrency = '<?php echo Yii::t('banking','banking.payment.currency_small');?>';
	var lblAmount = '<?php echo Yii::t('banking','banking.payment.amount');?>';
	var lblStatus = '<?php echo Yii::t('banking','banking.payment.status');?>';
	var lblManualWithdraw = '<?php echo Yii::t('banking','banking.payment.manual_withdraw');?>';
	var lblFrontEndWithdraw = '<?php echo Yii::t('banking','banking.payment.frontend_withdraw');?>';
</script>
<style type="text/css">
.ui-widget-overlay{
z-index: 10010!important;
}
</style>
</head>
<body onload="javascript: qryTransactions();">
<div class="">
	<div id="search_cont">
		<div class="title"><?php echo Yii::t('banking','banking.payment.search_withdraw_request');?></div>
		<div class="body">
			<div style="display:none"><!-- hide these options -->
				Transaction Types:
		        <input type="radio" id="radAll" name="cage_action" checked="checked" value="" onclick="javascript: qryTransactions();"/><label for="radAll">All</label>
		        <input type="radio" id="radDeposit" name="cage_action" value="AND transaction_type='Deposit'" onclick="javascript: qryTransactions();"/><label for="radDeposit">Deposit</label>
		        <input type="radio" id="radWithdraw" name="cage_action" value="AND transaction_type='Withdraw'" onclick="javascript: qryTransactions();"/><label for="radWithdraw">Withdraw</label>
	        </div>
	        <input type="checkbox" id="chkExceptCancel" style="margin-left: 10px;" onchange="javascript: qryTransactions();" checked><label for="chkExceptCancel"><?php echo Yii::t('banking','banking.payment.except_cancelled_transactions');?></label>
	        <br/><br/>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="left" width="40%"><?php echo Yii::t('banking','banking.payment.from');?></td>
				<td class="right"><input name="dtFrom" id="dtFrom" type="text" maxlength="12" class="textbox1" value="<?php echo isset($_POST['datefrom'])? $_POST['datefrom'] : date('Y-m-d', time());?>"/></td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('banking','banking.payment.to');?></td>
				<td class="right"><input name="dtTo" id="dtTo" type="text" maxlength="12" class="textbox" value="<?php echo isset($_POST['dateto'])? $_POST['dateto'] : date('Y-m-d', time());?>" /></td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('banking','banking.payment.account_id');?></td>
				<td class="right"><input type="text" id="txtAccountID"/></td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('banking','banking.payment.email');?></td>
				<td class="right"><input type="text" id="txtEmail"/></td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('banking','banking.payment.currency');?></td>
				<td class="right">
					<Select name="cmbCurrency" id="cmbCurrency" onchange="javascript: showExceptTestCurrency(this);">
						<option value="">All</option>
						<?php 
							$dataReader = TableCurrency::model()->findAll();
							foreach ($dataReader as $row){
								echo '<option value=\' AND currency_name="' . strtoupper($row['currency_name'])  . '"\'>'. strtoupper($row['currency_name']) . '</option>';
							}
						?>
					</Select>
					<input type="checkbox" id="chkExceptTestCurrency" onchange="javascript: qryTransactions();" checked><label id="lblExceptTestCurrency" for="chkExceptTestCurrency"><?php echo Yii::t('banking','banking.payment.except_test_currency');?></label>
				</td>
			</tr>
			</table>
			<br/>
			<div align="center">
			<input class="btn red" type="button" value="<?php echo Yii::t('banking','banking.payment.search');?>" onclick="javascript: qryTransactions();"/>
			<input class="btn red" type="button" value="<?php echo Yii::t('banking','banking.payment.yesterday');?>" onclick="javascript: qryTransactionsYesterday();"/>
			<input class="btn red" type="button" value="<?php echo Yii::t('banking','banking.payment.today');?>" onclick="javascript: qryTransactionsToday();"/>
			</div>
		</div>
	
	    <input type='hidden' id='h_condition' value="" />
		<input type='hidden' id='h_bindParams' value="" />	
		<table id="sg1"></table>
		<div id="psg1" style="padding-top: 10px;"></div>
		<div id="total_amount"></div>
	</div>
</div>

<div id="qry_result"></div>

<?php 
	//Create Dialog
//if(Yii::app()->session['level']!='8' && Yii::app()->session['level']!='7'){
if(Yii::app()->user->checkAccess('banking.readPayment')){
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		    'id'=>'withdraw-dialog',
		    // additional javascript options for the dialog plugin
		    'options'=>array(
		        'title'=>'Withdrawal',
		        'autoOpen'=>false,
	    		'width'=>'auto',
	    		'height'=>'auto',
		    	'modal'=> false,
		    	'resizable'=> false,
		    ),
		));
	
	// Initialize dialog content
    echo '<div id="viewWithdraw"></div>';
	
    // End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'deposit-dialog',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Deposit',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> false,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewDeposit"></div>';
	
	
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'forcelogout-dialog',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Warning',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> false,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewForceLogOut"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
}
?>
</body>
</html>