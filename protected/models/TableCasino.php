<?php
/**
 * @todo bind tbl_casino to TableCasino CActiveRecord
 * @copyright CE
 * @author leokarl
 * @since 2012-11-26
 */
class TableCasino extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_casino';
	}
}