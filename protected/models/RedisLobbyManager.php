<?php
Yii::import('application.extensions.redis.*');
require_once('libs/Predis/Commands/ICommand.php');
require_once('libs/Predis/Commands/Command.php');
require_once('libs/Predis/Commands/IPrefixable.php');
require_once('libs/Predis/Commands/PrefixableCommand.php');
require_once('libs/Predis/Commands/KeyExists.php');
require_once('libs/Predis/Commands/KeyKeys.php');
require_once('libs/Predis/Commands/StringGet.php');
require_once('libs/Predis/Commands/ConnectionSelect.php');
require_once('libs/Predis/Commands/ConnectionAuth.php');
require_once('libs/Predis/Helpers.php');
require_once('libs/Predis/Commands/HashGetMultiple.php');
require_once('libs/Predis/Client.php');
require_once('libs/Predis/IConnectionFactory.php');
require_once('libs/Predis/IConnectionParameters.php');
require_once('libs/Predis/ConnectionParameters.php');
require_once('libs/Predis/Network/IConnection.php');
require_once('libs/Predis/Network/IConnectionSingle.php');
require_once('libs/Predis/Network/ConnectionBase.php');
require_once('libs/Predis/Network/StreamConnection.php');
require_once('libs/Predis/Options/IClientOptions.php');
require_once('libs/Predis/Options/IOption.php');
require_once('libs/Predis/Options/Option.php');
require_once('libs/Predis/Options/ClientConnectionFactory.php');
require_once('libs/Predis/Options/ClientCluster.php');
require_once('libs/Predis/Options/ClientReplication.php');
require_once('libs/Predis/Commands/Processors/IProcessingSupport.php');
require_once('libs/Predis/Profiles/IServerProfile.php');
require_once('libs/Predis/Profiles/ServerProfile.php');
require_once('libs/Predis/Profiles/ServerVersion24.php');
require_once('libs/Predis/ConnectionFactory.php');
require_once('libs/Predis/Options/ClientPrefix.php');
require_once('libs/Predis/Options/ClientProfile.php');
require_once('libs/Predis/Options/ClientOptions.php');

require_once('classes/ICV999RedisStaff.php');
require_once('classes/ICV999RedisCore.php');
require_once('classes/CV999RedisCore.php');
require_once('classes/CV999RedisMain.php');
require_once('classes/CV999RedisLobbies.php');
require_once('classes/CV999OnlinePlayersYii.php');
require_once('libs/Predis/Commands/KeyDelete.php');
class RedisLobbyManager extends CV999RedisLobbies
{
	private $depositWithrawal	= 1; // one is unsuccessful
	private $htvCode = 1;
	private $svCode = 2;

	
	/**
	 * 
	 * @param array $agentPlayers
	 */
	public function forceLogoutByAgentPlayer($agentPlayers)
	{
		$this->forceLogoutByPlayers('a', $agentPlayers);
	}
	
	/**
	 * 
	 * @param array $cashPlayer
	 */
	public function forceLogoutByCashPlayer($cashPlayer)
	{
		$this->forceLogoutByPlayers('c', $cashPlayer);
	}

	/**
	 * @param string $playerType as 'a' and 'c'
	 * @param array $playerNames
	 */
	public function forceLogoutByPlayers($playerType, $playerNames)
	{

		// force all casino
		foreach ($playerNames as $playerName) {
			// remove redis from server
			$this->deletePlayer($playerType, $playerName, '*');
		}
	}

	/**
	 * 
	 * @param string $accountID
	 * @param integer $casinoID
	 */
	public function isStuckedBalanceOnLobby($accountID, $casinoID = null) {
		// 
		$hasCasinoParam		= '@po_has_casino';
		$casinoIdsParam		= '@po_casino_ids';
		$playerTypeParam	= '@po_player_type';
		$currencyParam		= '@po_currency';
		$keyCodeParam		= '@po_key_code';
		$isSuccessfulParam	= '@po_is_successful';

		// 
		$command = Yii::app()->db->createCommand('CALL spBEGetTransOnWalletToJSONByPlayer(:pi_account_id, :pi_deposit_withrawal, ' . $hasCasinoParam . ', ' . $casinoIdsParam . ', ' . $playerTypeParam . ', ' . $currencyParam . ', ' . $keyCodeParam . ', ' . $isSuccessfulParam . ')');
		$command->execute(array('pi_account_id' => $accountID, 'pi_deposit_withrawal' => $this->depositWithrawal));
		$result = Yii::app()->db->createCommand('SELECT ' . $hasCasinoParam . ', ' . $casinoIdsParam . ', ' . $playerTypeParam . ', ' . $currencyParam . ', ' . $keyCodeParam . ', ' . $isSuccessfulParam . ';')->queryAll();

		// validate
		if (isset($result) && isset($result[0][$hasCasinoParam]) && $result[0][$hasCasinoParam] > 0) {
			// 
			$jsonCasinoID = json_decode($result[0][$casinoIdsParam]);
			if ($casinoID != null) {
                $redisLobby = new RedisLobbyManager();
				// 
				foreach ($jsonCasinoID as $casino) {	
					// 
					if (!$redisLobby->isExisting($result[0][$playerTypeParam], $accountID, $casino->{"casinoID"})) {
							// 
						if ($casino->{"casinoID"} == $casinoID) {
							return TRUE;
						} else if ($casino->{"casinoID"} == $casinoID) {
							return TRUE;
						} else {
							return FALSE;
						}
					}
				}
				return FALSE;
			} else {
				if (is_array($jsonCasinoID) && count($jsonCasinoID) > 0) {
					return TRUE;
				}
				return FALSE;
			}
		} else {
			return FALSE;
		}

	}
	
	/**
	 * @todo call withdrawl API 
	 * @author Kimny MOUk
	 * @since 2012-12-08
	 */
	public function ProcessWithdrawalRequest($accountID)
	{
		$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='". $accountID ."' and deposit_withdrawal<>0");
		$rd=$command->queryRow();
		if ($rd['deposit_withdrawal'] == 1)
		{
			if ($rd['casino_id'] == 1 || $rd['casino_id'] == 2)
			{
				//$checkBalanceIfLock = new RedisLobbyManager();
				//$result=$checkBalanceIfLock->withdrawFromCasinoLobby($_POST['accountID']);
				$result= $this->withdrawFromCasinoLobby($accountID);
				//if there's a problem in HTV and SAVAN
				if ($result['errorCode'] == 1 && $result['casino'] === 'HTV') // error
				{
					//to display message that there is an error during lobby withdraw ih HTV
					echo 'h';
				}
				else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN') // error
				{
					//to display message that there is an error during lobby withdraw ih SAVAN
					echo 's';
				}
			}
			else if ($rd['casino_id'] == 4)
			{
				$result= $this->withdrawFromCasinoLobby($accountID);
				if ($result['errorCode'] == 1 && $result['casino'] === 'VIRTUALCASINO') // error
				{
					//to display message that there is an error during lobby withdraw ih SAVAN
					echo 'vc';
				}
			}
            else if ($rd['casino_id'] == 5)
            {
                $result= $this->withdrawFromCasinoLobby($accountID);
                if ($result['errorCode'] == 1 && $result['casino'] === 'SPORTBOOK') // error
                {
                    //to display message that there is an error during lobby withdraw ih SAVAN
                    echo 'sb';
                }
            }
			else if ($rd['casino_id'] == 6)
			{
				$result= $this->withdrawFromCasinoLobby($accountID);
				// SAVANSLOT = slot vegas
				if ($result['errorCode'] == 1 && $result['casino'] === 'SLOTVEGAS') // error
				{
					//to display message that there is an error during lobby withdraw ih SAVAN
					echo 'svs';
				}
			}
			else // balance stuck on Costavegas & VirtuaVegas
			{
				echo '1';
			}
		}
		else
		{
			echo 0;
		}
	}

	/**
	 * @todo to widthdraw balance & bonus from Casno Lobby when balance and bonus jame in Casino lobby
	 * @author sitthykun
	 * @param string $accountID
	 * @param string $casinoID
	 */
	
	private $effAPI; 
	
	public function withdrawFromCasinoLobby($accountID)
	{
	
	    $this->effAPI = new Constants();

		$hasCasinoParam		= '@po_has_casino';
		$casinoIdsParam		= '@po_casino_ids';
		$playerTypeParam	= '@po_player_type';
		$currencyParam		= '@po_currency';
		$keyCodeParam		= '@po_key_code';
		$isSuccessfulParam	= '@po_is_successful';
		$depositWithrawal	= 1; // one is unsuccessful
		$htv				= 1;
		$sv					= 2;
		$vc					= 4;
		$sb					= 5;
		$svs				= 6; 
	
		$command = Yii::app()->db->createCommand('CALL spBEGetTransOnWalletToJSONByPlayer(:pi_account_id, :pi_deposit_withrawal, ' . $hasCasinoParam . ', ' . $casinoIdsParam . ', ' . $playerTypeParam . ', ' . $currencyParam . ', ' . $keyCodeParam . ', ' . $isSuccessfulParam . ')');
		$command->execute(array('pi_account_id' => $accountID, 'pi_deposit_withrawal' => $this->depositWithrawal));
		$result = Yii::app()->db->createCommand('SELECT ' . $hasCasinoParam . ', ' . $casinoIdsParam . ', ' . $playerTypeParam . ', ' . $currencyParam . ', ' . $keyCodeParam . ', ' . $isSuccessfulParam . ';')->queryAll();
	
		// validatehttp://192.168.20.213:8082/
		if (isset($result) && isset($result[0][$hasCasinoParam]) && $result[0][$hasCasinoParam] > 0)
		{
			//
			$jsonCasinoID = json_decode($result[0][$casinoIdsParam]);
            $redisLobby = new RedisLobbyManager();
				
			foreach ($jsonCasinoID as $casino) {
	
				//
				if (!$redisLobby->isExisting($result[0][$playerTypeParam], $accountID, $casino->{"casinoID"})) {
					$mode = md5($result[0][$currencyParam] . $result[0][$keyCodeParam] . strtoupper($accountID));
					$code = strtoupper($accountID);
						
					// htv
					if ($casino->{"casinoID"} == $htv) {
	
						$session1 = curl_init($this->effAPI->effeciaAPIAddress . 'efficia-withdrawal-api-htv/Withdraw75648439341?code=' . $code . '&mode=' . $mode . '&type=' . $result[0][$playerTypeParam] . '&rd=' . 'be' . '&casinoID=' . $casino->{"casinoID"} . '&webuser=RoCher21@clubvegas&webpassword=@clubvegas!'); // $data['language']
						// Don't return HTTP headers. Do return the contents of the call
						curl_setopt($session1, CURLOPT_RETURNTRANSFER, TRUE);
						// get otp from api
						$rSession1 = curl_exec($session1);
						curl_close($session1);
						if ($rSession1 !== 'ok') {
							//exit('session='.$session1);
							return array('errorCode'=>1,'casino'=>'HTV');
						}
							
						// params
							
						// params
						$statusParam = '@po_status';
						$isSuccessfulParam = '@po_is_successful';
							
						$command = Yii::app()->db->createCommand('CALL spBEGetTransStatusOnWalletByPlayerAndCasinoID(:pi_account_id,:pi_casino_id, :pi_deposit_withrawal, ' . $statusParam . ', ' . $isSuccessfulParam . ')');
						$command->execute(array('pi_account_id' => $accountID, 'pi_casino_id' => $casino->{"casinoID"}, 'pi_deposit_withrawal' => $depositWithrawal));
						$subResult = Yii::app()->db->createCommand('SELECT ' . $statusParam . ', ' . $isSuccessfulParam . ';')->queryAll();
							
						// equal zero means it has done
						if (isset($subResult) && isset($subResult[0][$statusParam]) && $subResult[0][$statusParam] == 0) {
							// [@po_status] => 0 [@po_is_successful]
						}
	
					} else if ($casino->{"casinoID"} == $sv) { // savan
                        $paramsRequest = 'line=0' .
                            '&isIP=1' .
                            '&domain=' . $_SERVER['SERVER_NAME'] .
                            '&countryCode=' . (isset($_SERVER['GEOIP_COUNTRY_CODE']) ? $_SERVER['GEOIP_COUNTRY_CODE']: '') .
                            '&code=' . $code .
                            '&mode=' . $mode .
                            '&siteID=cv999_2' .
                            '&type=' .  $result[0][$playerTypeParam] .
                            '&rd=' . 'be' .
                            '&casinoID=' . $casino->{"casinoID"} .
                            '&webuser=RoCher21@clubvegas' .
                            '&webpassword=@clubvegas!' .
                            '&language=' . 'EN';
                        // curl efficia session login
                        $session2 = curl_init($this->effAPI->effeciaAPIAddress . 'efficia-withdrawal-api-sv2/Withdraw8763647531?' . $paramsRequest);

						// $session2 = curl_init($this->effAPI->effeciaAPIAddress . 'efficia-withdrawal-api-sv2/Withdraw8763647531?line=0&code=' . $code . '&mode=' . $mode . '&siteID=cv999_2' . '&type=' . $result[0][$playerTypeParam] . '&rd=' . 'be'  . '&casinoID=' . $casino->{"casinoID"} . '&webuser=RoCher21@clubvegas&webpassword=@clubvegas!&language=EN');  // $data['language']
						
	
						// Don't return HTTP headers. Do return the contents of the call
						curl_setopt($session2, CURLOPT_RETURNTRANSFER, TRUE);
						// get otp from api
						$rSession2 = curl_exec($session2);
						curl_close($session2);
	
						if ($rSession2 !== 'ok') {
							return array('errorCode'=>1,'casino'=>'SAVAN');
						}
	
						// params
						$statusParam = '@po_status';
						$isSuccessfulParam = '@po_is_successful';
	
						$command = Yii::app()->db->createCommand('CALL spBEGetTransStatusOnWalletByPlayerAndCasinoID(:pi_account_id,:pi_casino_id, :pi_deposit_withrawal, ' . $statusParam . ', ' . $isSuccessfulParam . ')');
						$command->execute(array('pi_account_id' => $accountID, 'pi_casino_id' => $casino->{"casinoID"}, 'pi_deposit_withrawal' => $depositWithrawal));
						$subResult = Yii::app()->db->createCommand('SELECT ' . $statusParam . ', ' . $isSuccessfulParam . ';')->queryAll();
	
						// equal zero means it has done
						if (isset($subResult) && isset($subResult[0][$statusParam]) && $subResult[0][$statusParam] == 0) {
							// [@po_status] => 0 [@po_is_successful]
						}
	
					} else if ($casino->{"casinoID"} == $vc) { // virtual casino

						// mode for virtual casino
						$mode = substr(md5($result[0][$currencyParam] . $result[0][$keyCodeParam] . strtoupper($accountID)), 0, 10);

						$session2 = curl_init(Constants::VIRTUALCASINO_API_ADDRESS . 'cdn-withdrawal-api-vc/Withdraw623838474585?code=' . $code . '&mode=' . $mode . '&type=' . $result[0][$playerTypeParam] . '&rd=' . 'be'  . '&casinoID=' . $casino->{"casinoID"} . '&webuser=RoCher21@clubvegas&webpassword=@clubvegas!');  // $data['language']
					
						// Don't return HTTP headers. Do return the contents of the call
						curl_setopt($session2, CURLOPT_RETURNTRANSFER, TRUE);
						// get otp from api
						$rSession2 = curl_exec($session2);
						curl_close($session2);

						if ($rSession2 !== 'ok') {
							return array('errorCode' => 1, 'casino' => 'VIRTUALCASINO');
						}

						// params
						$statusParam = '@po_status';
						$isSuccessfulParam = '@po_is_successful';

						$command = Yii::app()->db->createCommand('CALL spBEGetTransStatusOnWalletByPlayerAndCasinoID(:pi_account_id,:pi_casino_id, :pi_deposit_withrawal, ' . $statusParam . ', ' . $isSuccessfulParam . ')');
						$command->execute(array('pi_account_id' => $accountID, 'pi_casino_id' => $casino->{"casinoID"}, 'pi_deposit_withrawal' => $depositWithrawal));
						$subResult = Yii::app()->db->createCommand('SELECT ' . $statusParam . ', ' . $isSuccessfulParam . ';')->queryAll();
					
						// equal zero means it has done
						if (isset($subResult) && isset($subResult[0][$statusParam]) && $subResult[0][$statusParam] == 0) {
							// [@po_status] => 0 [@po_is_successful]
						}
                    } else if ($casino->{"casinoID"} == $sb) { // sportbook

                        // mode for sportbook
                        $mode = substr(md5($result[0][$currencyParam] . $result[0][$keyCodeParam] . strtoupper($accountID)), 0, 10);

                        $session2 = curl_init(Constants::SPORTBOOK_API_ADDRESS . 'cdn-withdrawal-api-sb/Withdraw53735278256?code=' . $code . '&mode=' . $mode . '&type=' . $result[0][$playerTypeParam] . '&rd=' . 'be'  . '&casinoID=' . $casino->{"casinoID"} . '&webuser=RoCher21@clubvegas&webpassword=@clubvegas!');  // $data['language']

                        // Don't return HTTP headers. Do return the contents of the call
                        curl_setopt($session2, CURLOPT_RETURNTRANSFER, TRUE);
                        // get otp from api
                        $rSession2 = curl_exec($session2);
                        curl_close($session2);

                        if ($rSession2 !== 'ok') {
                            return array('errorCode' => 1, 'casino' => 'SPORTBOOK');
                        }

                        // params
                        $statusParam = '@po_status';
                        $isSuccessfulParam = '@po_is_successful';

                        $command = Yii::app()->db->createCommand('CALL spBEGetTransStatusOnWalletByPlayerAndCasinoID(:pi_account_id,:pi_casino_id, :pi_deposit_withrawal, ' . $statusParam . ', ' . $isSuccessfulParam . ')');
                        $command->execute(array('pi_account_id' => $accountID, 'pi_casino_id' => $casino->{"casinoID"}, 'pi_deposit_withrawal' => $depositWithrawal));
                        $subResult = Yii::app()->db->createCommand('SELECT ' . $statusParam . ', ' . $isSuccessfulParam . ';')->queryAll();

                        // equal zero means it has done
                        if (isset($subResult) && isset($subResult[0][$statusParam]) && $subResult[0][$statusParam] == 0) {
                            // [@po_status] => 0 [@po_is_successful]
                        }

					} else if ($casino->{"casinoID"} == $svs) { // slot vegas = savan slot

						// mode for virtual casino
						$mode = substr(md5($result[0][$currencyParam] . $result[0][$keyCodeParam] . strtoupper($accountID)), 0, 20);
 
						$session2 = curl_init(Constants::SLOTVEGAS_API_ADDRESS . 'efficia-withdrawal-slots-sv/Execute?code=' . $code . '&mode=' . $mode . '&type=' . $result[0][$playerTypeParam] . '&rd=' . 'be'  . '&casinoID=' . $casino->{"casinoID"} . '&webuser=RoCher21@clubvegas&webpassword=@clubvegas!&language=en');  // $data['language']
					
						// Don't return HTTP headers. Do return the contents of the call
						curl_setopt($session2, CURLOPT_RETURNTRANSFER, TRUE);
						// get otp from api
						$rSession2 = curl_exec($session2);
						curl_close($session2);

						if ($rSession2 !== 'ok') {
							return array('errorCode' => 1, 'casino' => 'SLOTVEGAS');
						}

						// params
						$statusParam = '@po_status';
						$isSuccessfulParam = '@po_is_successful';

						$command = Yii::app()->db->createCommand('CALL spBEGetTransStatusOnWalletByPlayerAndCasinoID(:pi_account_id,:pi_casino_id, :pi_deposit_withrawal, ' . $statusParam . ', ' . $isSuccessfulParam . ')');
						$command->execute(array('pi_account_id' => $accountID, 'pi_casino_id' => $casino->{"casinoID"}, 'pi_deposit_withrawal' => $depositWithrawal));
						$subResult = Yii::app()->db->createCommand('SELECT ' . $statusParam . ', ' . $isSuccessfulParam . ';')->queryAll();
					
						// equal zero means it has done
						if (isset($subResult) && isset($subResult[0][$statusParam]) && $subResult[0][$statusParam] == 0) {
							// [@po_status] => 0 [@po_is_successful]
						}
					
					}
					
				}
			}
		}
	}
}


?>