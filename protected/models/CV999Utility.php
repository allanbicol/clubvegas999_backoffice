<?php
/**
 * @todo 
 * @copyright	CE
 * @author	Kimny MOUK
 * @since	2012-04-23
 */

class CV999Utility
{ 

	/**
	 * @todo encrypt data from array object to string
	 * @author chamnan
	 * @return string
	 * @param array $data
	 * @param string $key
	 */
	public static function generateOneWayPassword($data, $key = 'CV999') {
		//$secured_data= strrev(md5($data . $key . strrev($data)));
		//$secured_data= md5($data);
		return md5($data);
	}
	/**
	 * @todo encrypt data from array object to string
	 * @author leokarl
	 * @return string
	 */
	public static function generateKeyCode() {
		$key = microtime(TRUE);
		return md5($key);
	}
	
	/**
	 * @todo decrypt data from string to array
	 * @author sitthykun
	 * @return array
	 * @param string $data
	 * @param string $key
	 */
	public static function getDecrypt($string, $key = null) {
		$key = 'IOMCDEPOPEYECODETHENUTD';
	
		return CV999Utility::getStringToArrayCrypto(rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode(str_replace(' ', '+', $string)), MCRYPT_MODE_CBC, md5(md5($key))), "\0"));
	}
	
	/**
	 * @todo encrypt data from array object to string
	 * @author sitthykun
	 * @return string
	 * @param array $data
	 * @param string $key
	 */
	public static function getEncrypt($array, $key = null) {
		$key = 'IOMCDEPOPEYECODETHENUTD';
	
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), CV999Utility::getArrayToStringCrypto($array), MCRYPT_MODE_CBC, md5(md5($key))));
	}
	
	public static function getArrayToStringCrypto($arr) {
		$dilimeterKey = '###';
		$dilimeterValue = '|||';
		$string = '';
	
		foreach ($arr as $key => $value) {
			$string .= $key . $dilimeterKey . $value . $dilimeterValue;
		}
	
		return $string;
	}
	
	public static function getStringToArrayCrypto($string) {
		$dilimeterKey = '###';
		$dilimeterValue = '|||';
	
		$arr =  array();
		// $arr[0] = 'offset1';	// prevent error
	
		$arrayTemp = explode($dilimeterValue, $string);
	
		// remove last element
		unset($arrayTemp[count($arrayTemp) -1 ]);
	
		foreach ($arrayTemp as $val) {
			if (strlen($val) > 1) {
				list($key, $value) = explode($dilimeterKey, $val);
	
				// not
				if ($key != '' && $value != '') {
					$temp = array($key => $value);
					if (isset($temp)) {
						$arr = array_merge((array)$arr, (array)$temp);
						//$arr = array_merge((array)$arr, array($key => $value));
					}
				}
			}
		}
	
		return $arr;
	}

	
	public static function encrypt($string){
		$key='BETEAM123';
		
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
	}
	public static function decrypt($string){
		$key='BETEAM123';
		return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	}
	
	
	// Encryption Function
	function encryption($plain_text)
	{
		$secret_key=Constants::KEY_LOGIN;
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(md5($secret_key)), $plain_text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB),MCRYPT_RAND))));
	}
	
	// Decryption Function
	function decryption($plain_text)
	{
		$secret_key=Constants::KEY_LOGIN;
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(md5($secret_key)), base64_decode($plain_text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
	}
// 	public static function getBrowser() {
			
// 		$user_agent = $_SERVER['HTTP_USER_AGENT'];
	
// 		$browsername        =   "Unknown Browser";
	
// 		$browser_array  =   array(
// 				'/msie/i'       =>  'Internet Explorer',
// 				'/firefox/i'    =>  'Mozilla Firefox',
// 				'/safari/i'     =>  'Safari',
// 				'/chrome/i'     =>  'Chrome',
// 				'/opera/i'      =>  'Opera',
// 				'/netscape/i'   =>  'Netscape',
// 				'/maxthon/i'    =>  'Maxthon',
// 				'/konqueror/i'  =>  'Konqueror',
// 				'/mobile/i'     =>  'Handheld Browser'
// 		);
	
// 		foreach ($browser_array as $regex => $value) {
	
// 			if (preg_match($regex, $user_agent)) {
// 				$browsername    =   $value;
// 			}
	
// 		}
	
// 		return $browsername;
	
// 	}
	
	public static function getBrowser() {
		$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
	
	
		$browser_array  =   array(
				'/msie/i'       =>  'Internet Explorer',
				'/firefox/i'    =>  'Mozilla Firefox',
				'/safari/i'     =>  'Safari',
				'/chrome/i'     =>  'Chrome',
				'/opera/i'      =>  'Opera',
				'/netscape/i'   =>  'Netscape',
				'/maxthon/i'    =>  'Maxthon',
				'/konqueror/i'  =>  'Konqueror',
				'/mobile/i'     =>  'Handheld Browser'
		);
		$name	=	'';
		foreach ($browser_array as $regex => $value) {

			if (preg_match($regex, $userAgent)) {
				$name    =   $value;
			}

		}
				
		$browsers = array("firefox", "msie", "opera", "chrome", "safari",
				"mozilla", "seamonkey","konqueror", "netscape",
				"gecko", "navigator", "mosaic", "lynx", "amaya",
				"omniweb", "avant", "camino", "flock", "aol","mobile");
		// What version?
		foreach($browsers as $browser)
		{
			if (preg_match("#($browser)[/ ]?([0-9.]*)#", $userAgent, $matches)) {
				$version = $matches[2];
				break;
			}
// 			else {
// 				$version = 'unknown';
// 				break;
// 			}
			
		}
		// Running on what platform?
		if (preg_match('/linux/', $userAgent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/macintosh|mac os x/', $userAgent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/', $userAgent)) {
			$platform = 'windows';
		}
		else {
			$platform = 'unrecognized';
		}
	
		return array(
				'name'      => $name,
				'version'   => $version,
				'platform'  => $platform,
				'userAgent' => $userAgent
		);
	}
	
	function formatMoney($number, $fractional=false) {
		if ($fractional) {
			$number = sprintf('%.2f', $number);
		}
		while (true) {
			$replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
			if ($replaced != $number) {
				$number = $replaced;
			} else {
				break;
			}
		}
		return $number;
	}

    public static function prefix_encode($aff_id, $aff_code, $aff_name, $banner_id, $media_id) {
        $packed =  implode('#', array($aff_id,$aff_code,$aff_name,$banner_id,$media_id));
        $init_vector_size=mcrypt_get_iv_size(MCRYPT_BLOWFISH,MCRYPT_MODE_ECB);
        $init_vector=mcrypt_create_iv($init_vector_size,MCRYPT_RAND);
        $encrypted=mcrypt_encrypt(MCRYPT_BLOWFISH,Constants::CE_ENCRYPTION_KEY,$packed,MCRYPT_MODE_ECB,$init_vector);
        return strtr(base64_encode($encrypted),'+/=','-_,');
    }

    public static function prefix_decode($encrypted) {
        $init_vector_size=mcrypt_get_iv_size(MCRYPT_BLOWFISH,MCRYPT_MODE_ECB);
        $init_vector=mcrypt_create_iv($init_vector_size,MCRYPT_RAND);
        $decrypted=trim(mcrypt_decrypt(MCRYPT_BLOWFISH,Constants::CE_ENCRYPTION_KEY,base64_decode(strtr($encrypted,'-_,','+/=')),MCRYPT_MODE_ECB,$init_vector));
        if (!preg_match('/^.+#./s', $decrypted, $matches)){
            return "ERROR: unknown prefix format";
        }

        $keys   = array('aff_id','aff_code','aff_name','banner_id','media_id');
        $values = explode('#', $decrypted);
        $list = array_combine($keys, $values);

        return $list;
    }

}