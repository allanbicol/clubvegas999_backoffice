<?php
/**
 * @todo CreateUserController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-26
 */
class UpdateCurrencyController extends MyController
{
	public function actionUpdateCurrency()
	{
		if(Yii::app()->user->checkAccess('systemSetting.writeCurrencySetting'))
		{
			$cu=new Currency();
			$cu->updateCurrency();
		}
			
	}
	
	public function actionIndex()
	{
		$this->render("index");
	}
}
