<?php
class GetTableLimit
{
	public function getTableBacarratLimit()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
				
			}
			if(trim($sqlAdd)!=''){
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT * FROM vwBacarratTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd." order by sort_id");
				$rows = $command->query();
				return $rows;
			}else{
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT * FROM vwBacarratTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
				$rows = $command->query();
				return $rows;
			}
		}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwBacarratTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
			$rows = $command->query();
			return $rows;
		}
			
	}
	public function getCountBacarratLimitRecord()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT COUNT(0) FROM vwBacarratTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd);
				$rows = $command->query();
				return $rows;
			}else{
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT COUNT(0) FROM vwBacarratTableLimit WHERE currency_id=". $_GET['currency_id']);
				$rows = $command->query();
				return $rows;
			}
		}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT COUNT(0) FROM vwBacarratTableLimit WHERE currency_id=". $_GET['currency_id']);
			$rows = $command->query();
			return $rows;
		}
	}
	
	public function getTableDragonTigerLimit()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT * FROM vwDragontigerTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd." order by sort_id");
				$rows = $command->query();
				return $rows;
			}else{
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT * FROM vwDragontigerTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
				$rows = $command->query();
				return $rows;
			}
		}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwDragontigerTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
			$rows = $command->query();
			return $rows;
		}
	}
	public function getCountDragonTigerRecord()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
						$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT COUNT(0) FROM vwDragontigerTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd);
				$rows = $command->query();
				return $rows;
			}else{
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT COUNT(0) FROM vwDragontigerTableLimit WHERE currency_id=". $_GET['currency_id']);
				$rows = $command->query();
				return $rows;
			}
		}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT COUNT(0) FROM vwDragontigerTableLimit WHERE currency_id=". $_GET['currency_id']);
			$rows = $command->query();
			return $rows;
		}
	}
	
	public function getTableRouletteLimit()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT * FROM vwEuropeanRouletteTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd ." order by sort_id");
				$rows = $command->query();
				return $rows;
			}else{
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT * FROM vwEuropeanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
				$rows = $command->query();
				return $rows;
			}
		}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwEuropeanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
			$rows = $command->query();
			return $rows;
		}
	}
	public function getCountRouletteRecord()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT COUNT(0) FROM vwEuropeanRouletteTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd);
				$rows = $command->query();
				return $rows;
			}else{
				$connection = Yii::app()->db_cv999_fd_master;
				$command = $connection->createCommand("SELECT COUNT(0) FROM vwEuropeanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']);
				$rows = $command->query();
				return $rows;
			}
		}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT COUNT(0) FROM vwEuropeanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']);
			$rows = $command->query();
			return $rows;
			}
	}
	public function getTableBlackjackLimit()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwBlackjackTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd." order by sort_id");
					$rows = $command->query();
					return $rows;
			}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwBlackjackTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
			$rows = $command->query();
			return $rows;
			}
		}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwBlackjackTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
					$rows = $command->query();
			return $rows;
		}
	}
	public function getCountTableBlackjackLimit()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT COUNT(0) FROM vwBlackjackTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd);
			$rows = $command->query();
			return $rows;
			}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT COUNT(0) FROM vwBlackjackTableLimit WHERE currency_id=". $_GET['currency_id']);
			$rows = $command->query();
			return $rows;
			}
			}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT COUNT(0) FROM vwBlackjackTableLimit WHERE currency_id=". $_GET['currency_id']);
					$rows = $command->query();
					return $rows;
			}
	}
	public function getTableAmericanRouletteLimit()
	{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
			$limit_ids=explode(',',$_GET['limit_ids']);
			for($i=0;$i<=count($limit_ids)-2;$i++)
			{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
			}
			if(trim($sqlAdd)!=''){
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwAmericanRouletteTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd." order by sort_id");
					$rows = $command->query();
					return $rows;
			}else{
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwAmericanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
			$rows = $command->query();
			return $rows;
			}
		}else{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM vwAmericanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']." order by sort_id");
				$rows = $command->query();
		return $rows;
		}
		}
		
		public function getCountAmericanRouletteRecord()
		{
		$sqlAdd='';
		if(isset($_GET['limit_ids'])){
		$limit_ids=explode(',',$_GET['limit_ids']);
		for($i=0;$i<=count($limit_ids)-2;$i++)
		{
				if(is_numeric($limit_ids[$i])){
					if($i>0){
						if($sqlAdd!=''){
							$sqlAdd.= ' OR id=' . $limit_ids[$i];
						}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
						}
					}else{
							$sqlAdd.= ' id=' . $limit_ids[$i];
					}
				}
		}
		if(trim($sqlAdd)!=''){
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwAmericanRouletteTableLimit WHERE currency_id=". $_GET['currency_id'] . " AND " . $sqlAdd);
		$rows = $command->query();
		return $rows;
		}else{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwAmericanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']);
		$rows = $command->query();
		return $rows;
		}
		}else{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwAmericanRouletteTableLimit WHERE currency_id=". $_GET['currency_id']);
		$rows = $command->query();
		return $rows;
		}
		}
}