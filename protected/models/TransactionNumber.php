<?php
/**
 * @todo	Create transaction number
 * @copyright	CE
 * @author 	Kimny MOUK
 * @since	2012-04-17
 *
 */
class TransactionNumber
{
	const PREFIX_AUTOMATIC_DEPOSIT = "AD";
	const PREFIX_MANUAL_DEPOSIT = "MD";
	const PREFIX_MANUAL_WITHDRAW = "MW";
	/** Added by Allan 04-17-2012*/
	const PREFIX_BUNOS="BU";
	const PREFIX_DEDUCTION="DE";
	const PREFIX_ADD_CREDIT="AC";
	const PREFIX_REMOVE_CREDIT="RC";
	/** Added by leokarl 12-19-2012**/
	const PREFIX_CHANGE_COMMISSION_SHARING="CS";
	
	
	
	/**
	 * @todo	Generate Transaction Number
	 * @author	Kimny MOUK
	 * @param string $transactionType
	 * @return string
	 */
	public static function generateTransactionNumber($transactionType)
	{
		if($transactionType == self::PREFIX_AUTOMATIC_DEPOSIT)
			$tn = self::PREFIX_AUTOMATIC_DEPOSIT . microtime(TRUE);
		else if($transactionType == self::PREFIX_MANUAL_DEPOSIT)
			$tn = self::PREFIX_MANUAL_DEPOSIT . microtime(TRUE);
		else if($transactionType == self::PREFIX_MANUAL_WITHDRAW)
			$tn = self::PREFIX_MANUAL_WITHDRAW . microtime(TRUE);
		/** Added by Allan 04-17-2012*/
		else if($transactionType == self::PREFIX_BUNOS)
			$tn = self::PREFIX_BUNOS . microtime(TRUE);
		else if($transactionType == self::PREFIX_ADD_CREDIT)
			$tn = self::PREFIX_ADD_CREDIT . microtime(TRUE);
		else if($transactionType == self::PREFIX_REMOVE_CREDIT)
			$tn = self::PREFIX_REMOVE_CREDIT . microtime(TRUE);
		else if($transactionType == self::PREFIX_DEDUCTION)
			$tn = self::PREFIX_DEDUCTION . microtime(TRUE);
		else if($transactionType == self::PREFIX_CHANGE_COMMISSION_SHARING)
			$tn = self::PREFIX_CHANGE_COMMISSION_SHARING . microtime(TRUE);
		return str_replace(".","",$tn);
	}
}
?>