<?php
class LobbyCodeModel
{

	/**
	 * 
	 * @param integer $id
	 */
	public static function getCodeById($id)
	{
		$temp = $this->getCodes();
		return isset($temp[$id]) ? $temp[$id] : null;
	} 

	/**
	 * 
	 */
	public static function getCodes()
	{
		$codes = array(
			0 => 'cv999_0',
			1 => 'cv999_1',
			2 => 'cv999_2',
			3 => 'cv999_3',
			4 => 'cv999_4'
		);
		return $codes;
	}
}

