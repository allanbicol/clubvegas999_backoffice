<?php

	
class CashPlayerTransHistoryBonusController extends MyController
{
	public function actionCashPlayerTransHistoryBonus()
	{
		//echo $_GET['AccountID'];
		$cpHbonus = new CashPlayerTransHistoryBonus;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHbonus->getCountTransactionHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHbonus->getPlayerTransHistoryBonus($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("transNumber","transTypeName","currencyName","Amount","deposit_via","balance_before","balance_after","submitTime","cashierID");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
		
			
	}

	
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerTransHistory')){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}

	}

	

}