<?php
/**
 * @todo		Languages Model
 * @copyright	CE
 * @author		Sitthykun LY
 * @since	 	2013-06-26
 * 
 */
class Languages 
{
	const EnglishCode = 'en';
	const ThaiCode	= 'th';
	const VietCode	= 'vn';
	const ChineseCode = 'zh';
	const HongKongCode= 'hk';

	/**
	 * 
	 * @param string $langCode en
	 */
	public static function getLanguageByLanguageCode($langCode)
	{
		$outputParam = '@po_successful';
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetLanguageByLanguageCode('" . $langCode . "', ". $outputParam . ")");
		//$command->execute();

		//$command = $connection->createCommand("SELECT " . $outputParam . ";");
		return $command->queryRow();
	}
 
	/**
	 * 
	 * @return Ambigous <multitype:, mixed, unknown>
	 */
    public static function getLanguages()
    {
        $connection = Yii::app()->db_cv999_fd_master;
        $command = $connection->createCommand("CALL spBeGetLanguages()");

        return $command->queryAll();
    }

    /**
     * 
     */
    public static function getLanguageCodeAsJSON()
    {
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("CALL spBeGetLanguageCodes()");

    	$temp = $command->queryAll();
    	return json_encode($temp, true);
    }

    /**
     * 
     */
	public static function getLanguageCodeConcatString()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetLanguageCodes()");
		
		$temp = $command->queryAll();
		$str = '';
		// 
		foreach ($temp as $t)
		{
			if (strlen($str) > 2)
			{
				$str .= ',';
			}
			$str .= $t;
		}
		// return 'en,vn,th'
		return $str;
	}
}

?>
