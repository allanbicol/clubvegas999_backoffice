<?php
/**
 * @todo UserStatusController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-024
 */
class UserStatusController extends MyController
{
	public function actionUpdateUserStatus()
	{
		$uu=new UpdateUser();
		$uu->changeUserStatus();
	}
}