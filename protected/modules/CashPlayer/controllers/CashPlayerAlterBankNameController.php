<?php

class CashPlayerAlterBankNamecontroller extends MyController
{
	public function actionAlterBankName()
	{
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT player_id from tbl_cash_player_trans_history  where trans_number='".$_POST['transNumber']."'");
		$rd=$command->queryRow();
	
		$postLog=new TableLog;
		
		$dateTime = date('Y-m-d H:i:s');
		if(Yii::app()->user->checkAccess('cashPlayer.writeTransHistorySummary'))
		{
			if ($_POST['oldBankName'] != $_POST['newBankName']){
				TableCashPlayerTransactionHistory::model()->updateAll(array(
						'transaction_item'=>$_POST['newBankName']),
						'trans_number="'.$_POST['transNumber'].'"');
				
				//Table Log
				$command1 = $connection->createCommand("SELECT transaction_item from tbl_transaction_item  where id='".$_POST['newBankName']."'");
				$rd1=$command1->queryRow();
				
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$rd['player_id'];
				$postLog->operated_level='Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=16;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> alter transaction via from:<label style=\"color:red\">'.$_POST['oldBankName'].'</label> to: <label style=\"color:red\">'.$rd1['transaction_item'].'</label> in transaction no '.$_POST['transNumber'].'</b>';
				$postLog->save();
				
				echo 'suc';
			}else{
				echo 'not';	
			}
		}else{
			echo 'err';
		}
	}
	
	
}