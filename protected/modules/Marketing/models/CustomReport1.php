<?php
/**
 * @todo custom report model
 * @author leokarl
 * @since 2013-04-13
 */
class CustomReport1
{
	public function getList($accountID,$playertype,$currency,$country,$email,$phone,$deposit,$dateFrom,$dateTo,$operator,$winloss,$selected){
		$connection = Yii::app()->db_cv999_fd_master;
		

			if ($accountID==""){
				$andAccountId='';
			}else{
				$andAccountId="and account_id="."'".htmlEntities($accountID,ENT_QUOTES)."'";
			}
			
			$andPlayerType='';
			if ($playertype=="All"){
				$andPlayerType='';
			}elseif ($playertype=="Aff"){
				$andPlayerType="and afftoken <>''";
			}elseif ($playertype=="Cash"){
				$andPlayerType="and afftoken =''";
			}
			

			if ($currency=="All"){
				$andCurrency='';
			}else{
				$andCurrency="and currency_name="."'".htmlEntities($currency,ENT_QUOTES)."'";
			}

		
			if ($country=="All"){
				$andCountry='';
			}else{
				$andCountry="and country="."'".htmlEntities($country,ENT_QUOTES)."'";
			}
	

			if ($email==""){
				$andEmail='';
			}else{
				$andEmail="and email="."'".htmlEntities($email,ENT_QUOTES)."'";
			}

			if ($phone==""){
				$andPhone='';
			}else{
				$andPhone="and phone_number="."'".htmlEntities($phone,ENT_QUOTES)."'";
			}
		

			if ($deposit==1){
				$andDeposit="and with_deposit <>0";
			}else{
				$andDeposit="and with_deposit=0 ";
			}

			$andDate="opening_acc_date between "."'".$dateFrom."' and "."'".$dateTo."'";
			$winlossDate1 = " and bet_time > '".$dateFrom."'"; 
			$winlossDate2 = " and bet_date > '".$dateFrom."'";
			
			if ($operator<>0 && $winloss<>""){
				if ($operator==1) {
					$andWinloss="and win_loss = "."".htmlEntities($winloss,ENT_QUOTES)."";
				}
				if ($operator==2) {
					$andWinloss="and win_loss <= "."".htmlEntities($winloss,ENT_QUOTES)."";
				}
				if ($operator==3) {
					$andWinloss="and win_loss >= "."".htmlEntities($winloss,ENT_QUOTES)."";
				}
		
			}else{
				$andWinloss='';
			}
		
			// The "i" after the pattern delimiter indicates a case-insensitive search
// 			if (preg_match("/first_deposit_date/", $selected)) {
// 				$joinFirstDepositDate="UNION 
// 					(SELECT CH.player_id,tcp.account_name,tcp.country,tcp.email,tcp.phone_number,tbl_currency.currency_name,
// 					0 AS win_los,'' AS last_date_played,0 AS with_deposit,'' AS opening_acc_date,MIN(CH.finish_time) AS first_deposit_date
// 					FROM tbl_cash_player_trans_history AS CH LEFT JOIN tbl_cash_player tcp ON CH.player_id=tcp.account_id LEFT JOIN tbl_currency ON CH.currency_id=tbl_currency.id WHERE (CH.cash_player_trans_type_id < 3) GROUP BY CH.player_id)";
// 			} else {
// 				$joinFirstDepositDate='';
// 			}
			
			if (preg_match("/win_loss/", $selected) || preg_match("/last_date_played/", $selected)) {
				$joinWinLoss="UNION 
					(SELECT CH.account_id,tcp.account_name,'' as afftoken,tcp.country,tcp.email,tcp.phone_number,currency_name,
					SUM(amount_settlements - amount_wagers) AS win_los,MAX(bet_date) AS last_date_played,0 AS with_deposit,'' AS opening_acc_date,'' AS first_deposit_date
					FROM tbl_costavegas_casino_history AS CH INNER JOIN tbl_cash_player tcp ON CH.account_id=tcp.account_id WHERE account_type='c'  $winlossDate2  GROUP BY currency_name,account_id )
					
					UNION
				
					(SELECT CH.account_id,tcp.account_name,'' as afftoken,tcp.country,tcp.email,tcp.phone_number,currency,
					SUM(win_loss) AS win_los,MAX(bet_time) AS last_date_played,0 AS with_deposit,'' AS opening_acc_date,'' AS first_deposit_date
					FROM tbl_htv_casino_history AS CH INNER JOIN tbl_cash_player tcp ON CH.account_id=tcp.account_id WHERE account_type='c'  $winlossDate1  GROUP BY currency,account_id) 
					
					UNION
					
					(SELECT CH.account_id,tcp.account_name,'' as afftoken,tcp.country,tcp.email,tcp.phone_number,currency,
					SUM(win_loss) AS win_los,MAX(bet_time) AS last_date_played,0 AS with_deposit,'' AS opening_acc_date,'' AS first_deposit_date
					FROM tbl_savan_casino_history AS CH INNER JOIN tbl_cash_player tcp ON CH.account_id=tcp.account_id WHERE account_type='c' $winlossDate1  GROUP BY currency,account_id)
					
					UNION
					
					(SELECT CH.account_id,tcp.account_name,'' as afftoken,tcp.country,tcp.email,tcp.phone_number,currency_name,
					SUM(amount_settlements - amount_wagers) AS win_los,MAX(bet_date) AS last_date_played,0 AS with_deposit,'' AS opening_acc_date,'' AS first_deposit_date
					FROM tbl_virtuavegas_casino_history AS CH INNER JOIN tbl_cash_player tcp ON CH.account_id=tcp.account_id WHERE account_type='c' $winlossDate2  GROUP BY currency_name,account_id)
					
					UNION
					
					(SELECT CH.account_id,tcp.account_name,'' as afftoken,tcp.country,tcp.email,tcp.phone_number,currency,
					SUM(win_loss) AS win_loss,MAX(bet_time) AS last_date_played,0 AS with_deposit,'' AS opening_acc_date,'' AS first_deposit_date
					FROM tbl_htv_bet_slot_record_casino_history AS CH INNER JOIN tbl_cash_player tcp ON CH.account_id=tcp.account_id WHERE account_type=1 $winlossDate1  GROUP BY currency,account_id)
					
					UNION
					
					(SELECT CH.account_id,tcp.account_name,'' as afftoken,tcp.country,tcp.email,tcp.phone_number,currency,
					SUM(win_loss) AS win_los,MAX(bet_time) AS last_date_played,0 AS with_deposit,'' AS opening_acc_date,'' AS first_deposit_date
					FROM tbl_savan_bet_slot_record_casino_history AS CH INNER JOIN tbl_cash_player tcp ON CH.account_id=tcp.account_id WHERE account_type=1 $winlossDate1   GROUP BY currency,account_id)
					";
			} else {
				$joinWinLoss='';
			}
			
		
		$command = $connection->createCommand("SELECT id,account_id,account_name,country,email,phone_number,currency_name,win_loss,last_date_played,with_deposit,opening_acc_date,first_deposit_date  FROM 
					(SELECT '' AS id,wc.account_id,wc.account_name,wc.afftoken,wc.country,wc.email,wc.phone_number,wc.currency_name,ROUND(SUM(wc.win_los),2) AS win_loss,MAX(wc.last_date_played) AS last_date_played,SUM(wc.with_deposit) AS with_deposit,MAX(opening_acc_date) AS opening_acc_date,MAX(first_deposit_date) as first_deposit_date
					FROM (
					
					(SELECT account_id,account_name,afftoken,country,email,phone_number,tbl_currency.currency_name,
					0 AS win_los,'' AS last_date_played,0 AS with_deposit,opening_acc_date,'' AS first_deposit_date
					FROM tbl_cash_player INNER JOIN tbl_currency ON tbl_cash_player.currency_id=tbl_currency.id)
				
					$joinWinLoss
					
					UNION 
					(SELECT CH.player_id AS account_id,tcp.account_name,'' as afftoken,tcp.country,tcp.email,tcp.phone_number,tbl_currency.currency_name,
					0 AS win_los,'' AS last_date_played,COUNT(CH.player_id) AS with_deposit,'' AS opening_acc_date,MIN(CH.finish_time) AS first_deposit_date
					FROM tbl_cash_player_trans_history AS CH LEFT JOIN tbl_cash_player tcp ON CH.player_id=tcp.account_id LEFT JOIN tbl_currency ON tcp.currency_id=tbl_currency.id WHERE  cash_player_trans_type_id <3  GROUP BY player_id)	
					
					) 
					AS wc GROUP BY wc.account_id ) as customReport WHERE $andDate $andPlayerType $andDeposit $andAccountId $andCurrency $andCountry $andEmail $andPhone $andWinloss order by opening_acc_date asc
				");
		$rows = $command->queryAll();
		return $rows;
	}
 
	
}