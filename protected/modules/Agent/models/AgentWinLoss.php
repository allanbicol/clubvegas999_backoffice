<?php
/**
 * @todo AgentWinLoss Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-06-01
 */
class AgentWinLoss
{
	/**
     * @todo getCostaVegasWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
	public function getCostaVegasWinLoss($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$vigSharing=0;
		if($_GET['vigSharing']==''){
			$vigSharing=0;
		}else{
			$vigSharing=$_GET['vigSharing'];
		}
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
				$strQueryWhereForBalance='';
			}else{
				$strQueryWhere="";
				$strQueryWhereForBalance='';
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT 
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency_name,
					sum(a.amount_wagers) as valid_bet,
					avg(a.amount_wagers) as average_bet,
					sum(a.amount_tips) as tips,
					
					(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
					((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
					(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
					(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
					)*-1 as total,
					
					((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					sum(a.amount_tips) as mc_tips,
					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
					
					(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
				    sum(a.amount_tips) as vig_tips,
					((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
					
					(
					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
					((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
					) as pl_total
					
					FROM tbl_costavegas_casino_history a 
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)	
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r
				order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
					#SUB COMPANY WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
					FROM  (
					    SELECT 
					    SUBSTRING(a.account_id,1,4) as account_id,
					    j.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    sum(a.amount_tips) as tips,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
						)*-1 as total,
					
					    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss, #sc_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm, #sc_comm,
						sum(a.amount_tips) as mc_tips,
					    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total, #sc_total
						
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
					    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY SUBSTRING(a.account_id,1,4)
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
					#SENIOR MASTER WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
					FROM  (
					    SELECT 
					    SUBSTRING(a.account_id,1,6) as account_id,
					    j.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    sum(a.amount_tips) as tips,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
						)*-1 as total,
					
					    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss, #sma_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm, #sma_comm,
						sum(a.amount_tips) as mc_tips,
					    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total, #sma_total
					
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
					    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY SUBSTRING(a.account_id,1,6)
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
					#MASTER WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
					FROM  (
					    SELECT 
					    SUBSTRING(a.account_id,1,8) as account_id,
					    j.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    sum(a.amount_tips) as tips,
					
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))  
						)*-1 as total,
					
					    ((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
						sum(a.amount_tips) as mc_tips,
					    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total, #ma_total
					
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					    
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
						(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
					    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY SUBSTRING(a.account_id,1,8)
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
					#AGENT WINLOSS
					SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
					FROM  (
					    SELECT 
					    a.account_id as account_id,
					    b.account_name,
					    a.currency_name,
					    sum(a.amount_wagers) as valid_bet,
					    avg(a.amount_wagers) as average_bet,
					    (sum(a.amount_tips))*-1 as tips,
					
					    #sum(a.amount_settlements - a.amount_wagers) as win_loss,
						sum(a.amount_settlements - a.amount_wagers) as win_loss,
					    sum(a.commission) as commission,
					    ((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as total,
					    (SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id 
					    AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as balance,
					
					    (sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,#agt_win_loss,
					    ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm, #agt_comm,
						sum(a.amount_tips) as agt_tips,
					    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total, #agt_total
					
						0 as vig_win_loss,
						0 as vig_tips,
						0 as vig_total,
					
					    #(((sum(a.amount_settlements - a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)))*-1) + (sum((a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)) * (-(CASE WHEN i.game_commission IS NULL THEN 0 ELSE i.game_commission END)/100)))) as pl_total
						(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) 
						)*-1 as pl_total
					
					    FROM tbl_costavegas_casino_history a 
						LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					    GROUP BY a.account_id
					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasWinLossCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasWinLossCount()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
				$strQueryWhereForBalance='';
			}else{
				$strQueryWhere="";
				$strQueryWhereForBalance='';
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
				    SELECT  SUBSTRING(a.account_id,1,2) as account_id,a.currency_name
				    FROM tbl_costavegas_casino_history a 
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,2)
				)t		
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT 
				    SUBSTRING(a.account_id,1,4) as account_id,
				    j.account_name
				    FROM tbl_costavegas_casino_history a 
				    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,4)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT 
				    SUBSTRING(a.account_id,1,6) as account_id,
				    j.account_name
				    FROM tbl_costavegas_casino_history a 
				    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,6)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT 
				    SUBSTRING(a.account_id,1,8) as account_id,
				    j.account_name
				    
				    FROM tbl_costavegas_casino_history a 
				    LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY SUBSTRING(a.account_id,1,8)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
				SELECT 
				    a.account_id as account_id,
				    b.account_name
				    FROM tbl_costavegas_casino_history a 
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
				    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				    GROUP BY a.account_id
				)t
			");
		}
		$rows = $command->query();
		return $rows;
	}
	
	
	/**
	 * @todo getCostaVegasWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasWinLossDetails($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		$vigSharing=0;
		if($_GET['vigSharing']==''){
			$vigSharing=0;
		}else{
			$vigSharing=$_GET['vigSharing'];
		}
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
				$strQueryWhereForBalance='';
			}else{
				$strQueryWhere="";
				$strQueryWhereForBalance='';
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
		}
		//Agent ID
		$strAccountIDWhere='';
		$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
		
		if(trim($_GET['accountID'])==''){
			$strAccountIDWhere='';
		}else{
			if($intAccountIDLength!=10){
				$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
			}else{
				$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
			}
		}
		$pl_total='';
		if(trim($_GET['accountID'])==''){
			$pl_total="(
					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
					((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
					) as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==2){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
			)*-1 as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==4){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
			)*-1 as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==6){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
			)*-1 as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==8){
			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) 
			)*-1 as pl_total";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT 
				a.bet_date,
				substring(a.account_id,1,$intAccountIDLength) as account_id,
				j.account_name,
				a.game_id,
				a.currency_name,
				sum(a.amount_wagers) as valid_bet,
				avg(a.amount_wagers) as average_bet,
				(sum(a.amount_tips))*-1 as tips,
				sum(a.amount_settlements - a.amount_wagers) as mem_win_loss,
				sum(a.commission) as mem_comm,
				((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as mem_total,
				(SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
				                
				(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
			    sum(a.amount_tips) as agt_tips,
			    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
			    
			    ((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
			    sum(a.amount_tips) as ma_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
			    
			    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
			    sum(a.amount_tips) as sma_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
			    
			    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
			    sum(a.amount_tips) as sc_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
			
			    ((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
			    (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
			    sum(a.amount_tips) as mc_tips,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
			    
				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
			    sum(a.amount_tips) as vig_tips,
				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
				
			    ". $pl_total ."
					
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY substring(a.account_id,1,$intAccountIDLength)
			)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit	
		");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasMemberWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
		if($_GET['intGame']!==''){
			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
		}else{
			$addWhere="";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetCostaVegasMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc, t.id desc  LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasMemberWinLossDetailsSummaryFooter
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMemberWinLossDetailsSummaryFooter()
	{
		if($_GET['intGame']!==''){
			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
		}else{
			$addWhere="";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row,
			count(t.win_loss) as bet_count, 
			sum(t.amount_wagers) as amount_wagers, 
			sum(t.win_loss) as win_loss,
			sum(t.commission) as commission,
			sum(t.amount_tips) as amount_tips,
			sum(t.total) as total,
			(Select t.balance from vwGetCostaVegasMemberWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance 
			FROM vwGetCostaVegasMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc");
		$rows = $command->query();
		
		foreach($rows as $row){
			echo $row['bet_count'] . ';';
			echo $row['amount_wagers'] . ';';
			echo $row['win_loss'] . ';';
			echo $row['commission'] . ';';
			echo $row['amount_tips'] . ';';
			echo $row['total'] . ';';
			echo $row['balance'];
		}
	}
	
	/**
	 * @todo getCostaVegasMemberWinLossDetailsCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMemberWinLossDetailsCount()
	{
		if($_GET['intGame']!==''){
			$addWhere="AND game_id='" . $_GET['intGame'] . "'";
		}else{
			$addWhere="";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetCostaVegasMemberWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' ". $addWhere);
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasTotal()
	{
		$vigSharing=0;
		if($_GET['vigSharing']==''){
			$vigSharing=0;
		}else{
			$vigSharing=$_GET['vigSharing'];
		}
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row,
				t.currency_name,t.exchange_rate,sum(t.valid_bet) as valid_bet,sum(t.tips) as tips,
				sum(t.win_loss) as win_loss,sum(t.commission) as commission,sum(t.total) as total,
				sum(t.vig_win_loss) as vig_win_loss,sum(t.vig_tips) as vig_tips,sum(t.vig_total) as vig_total,
				sum(t.pl_total) as pl_total
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,2) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
	
				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as win_loss, #mc_win_loss,
				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission, #mc_comm,
				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as total, #mc_total
				
				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
			    sum(a.amount_tips) as vig_tips,
				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
				
				(
				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
				) as pl_total
				
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY SUBSTRING(a.account_id,1,2)
			)t, (SELECT @rownum := 0) r
			GROUP BY t.currency_name
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasCSTotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,2) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
				
				((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss, #sc_win_loss,
				((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission, #sc_comm,
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as total, #sc_total
				
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) 
				)*-1 as pl_total
					
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasSMATotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasSMATotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,4) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
							
				((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as total, #sma_total,
    
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
				)*-1 as pl_total
	
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasMATotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasMATotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,6) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
				
				((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
			    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as total, #ma_total,
    
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			    
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
				)*-1 as pl_total
	
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCostaVegasAGTTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getCostaVegasAGTTotal()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST'";
			}else{
				$strQueryWhere="";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
			}
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
				SELECT
				a.bet_date,
				SUBSTRING(a.account_id,1,8) as account_id,
				j.account_name,
				a.currency_name,
				cu.exchange_rate,
				sum(a.amount_wagers) as valid_bet,
				sum(a.amount_tips) as tips,
				
				(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
			    ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
			    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as total, #agt_total,
    
				0 as vig_win_loss,
				0 as vig_tips,
				0 as vig_total,
			
				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) 
				)*-1 as pl_total
	
				FROM tbl_costavegas_casino_history a 
				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
				LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%'  AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
				GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
				");
		$rows = $command->query();
		return $rows;
	}
	/**
	 * @todo getCostaVegasWinLossResultDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-15
	 */
	public function getCostaVegasWinLossResultDetails(){
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM tbl_costavegas_casino_history WHERE id=" . $_POST['winlossid']);
	}

	
	
	
	/**
	 * @todo getHTVWinLoss
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getHTVWinLoss($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
	
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as total,
					
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
					
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total
					
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
					)*-1 as total,
					
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
					 
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
				$command = $connection->createCommand("
				#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
					)*-1 as total,
						
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
				");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as total,
					
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,8)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
					
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(b.bonus)  as bonus,
					((sum(a.win_loss) + sum(a.commission))) as total,
					(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
					
					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as pl_total
					    
					FROM tbl_htv_casino_history a
	    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getHTVWinLossCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getHTVWinLossCount()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (SELECT
					SUBSTRING(a.account_id,1,2) as account_id
					FROM tbl_htv_casino_history a
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id
					
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
			  	)t
			");
    	}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
    				SELECT  COUNT(0)
    				FROM  (
					    SELECT
						SUBSTRING(a.account_id,1,8) as account_id
						    
						FROM tbl_htv_casino_history a
						LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
						WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
						GROUP BY SUBSTRING(a.account_id,1,8)
					)t
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
	    		SELECT  COUNT(0)
	    		FROM  (
					SELECT
					a.account_id as account_id
					FROM tbl_htv_casino_history a
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
				)t
			");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999Total
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999Total()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as win_loss,
			    (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
			    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as total,
    			
    			(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    			");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999CSTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999CSTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    
    			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
    			((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
    			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as total, #sc_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
				)*-1 as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999SMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999SMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,4) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
    			((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
    			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as total, #sma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
				)*-1 as pl_total
    				
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999MATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999MATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,6) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    
    			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
    			((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as total, #ma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
				)*-1 as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999AGTTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999AGTTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,8) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.available_bet) as valid_bet,
    			
    			(sum(a.win_loss * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
    			((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
    			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as total, #agt_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
				)*-1 as pl_total
    
    			FROM tbl_htv_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVWinLossDetails($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
   	 	//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}
    	//Agent ID
    	$strAccountIDWhere='';
    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    
    	if(trim($_GET['accountID'])==''){
    		$strAccountIDWhere='';
    	}else{
    		if($intAccountIDLength!=10){
    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    		}else{
    			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    		}
    	}
    	$pl_total='';
    	if(trim($_GET['accountID'])==''){
    		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    		)*-1 as pl_total";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    			SELECT  @rownum := @rownum + 1 row, t.*
    			FROM  (
	    			SELECT
	    			substring(a.account_id,1,$intAccountIDLength) as account_id,
	    			j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.available_bet) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.available_bet) as valid_bet,
					sum(0) as tips,
	    			sum(a.win_loss) as mem_win_loss,
					sum(a.commission) as mem_comm,
					sum(b.bonus)  as mem_bonus,
					((sum(a.win_loss) + sum(a.commission))) as mem_total,
					(SELECT balance FROM vwGetHTVMemberWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
	    
	    			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
				   
				    ((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
				    ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
				    (((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.available_bet * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
				   
				    ((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
				    ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
				    (((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.available_bet * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
				    
				    ((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
				    ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.available_bet * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.available_bet * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
				    
				    ((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
				    (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
				    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.available_bet * ((100 - a.sc_share)/100)) * (-a.sc_commission/100)))) as mc_total,
				        
				    ".$pl_total."
				        
	    			FROM tbl_htv_casino_history a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
	    			GROUP BY substring(a.account_id,1,$intAccountIDLength)
	    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetailsCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetailsCount()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetHTVMemberWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetHTVMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc,t.id desc LIMIT $startIndex , $limit");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetailsSummaryFooter
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetailsSummaryFooter()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, 
    			count(t.win_loss) as bet_count,
    			sum(t.bet_amount) as bet_amount,
				sum(t.amount_wagers) as amount_wagers, 
				sum(t.win_loss) as win_loss,
				sum(t.commission) as commission,
				sum(t.total) as total,
				(Select t.balance from vwGetHTVMemberWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
    			FROM vwGetHTVMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
    	$rows = $command->query();
    foreach($rows as $row){
			echo $row['bet_count'] . ';';
			echo $row['bet_amount'] . ';';
			echo $row['amount_wagers'] . ';';
			echo $row['win_loss'] . ';';
			echo $row['commission'] . ';';
			echo $row['total'] . ';';
			echo $row['balance'];
		}
    }
    /**
     * @todo getHTVMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHatienVegasWinLossResultDetails()
    {
		$row = TableHtvCasinoHistory::model()->find(array('select'=>'game_type,currency,bet_result_detail,bet_detail,bet_result','condition'=>'id=:id',
					'params'=>array(':id'=>$_GET['winlossid']),));
    	
		echo $this->getWinLossBettingDetail($row['game_type'], $row['bet_result_detail'], $row['bet_detail'],$row['bet_result'], $row['currency']);
		
    }
    
    
    
    

    
    
    /**
     * @todo getSAVANWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLoss($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    			$strQueryWhere="";
    		}
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}
    		$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as total,
					
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as mc_total,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as pl_total
					
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
				#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
					)*-1 as total,
					
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
					 
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					- sum(a.tips))*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$command = $connection->createCommand("
				#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
					)*-1 as total,
						
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
					- sum(a.tips))*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
				");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as total,
					
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
					- sum(a.tips))*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,8)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					(sum(a.tips))*-1 as tips,
					
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(b.bonus)  as bonus,
					((sum(a.win_loss) + sum(a.commission))) as total,
					(SELECT balance FROM vwGetSAVANVEGASMemberWinLossDetailsV2 WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
					
					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				sum(a.tips) as agt_tips,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					- sum(a.tips))*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
	    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
	    }
	    $rows = $command->query();
	    return $rows;
    }
    
    /**
     * @todo getSAVANWinLossCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLossCount()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    			$strQueryWhere="";
    		}
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}
    		$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (SELECT
    				SUBSTRING(a.account_id,1,2) as account_id
    				FROM tbl_savan_casino_history_v2 a
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,2)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,4) as account_id						
    				FROM tbl_savan_casino_history_v2 a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,4)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,6) as account_id
    				FROM tbl_savan_casino_history_v2 a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,6)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,8) as account_id
    				FROM tbl_savan_casino_history_v2 a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,8)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    				SELECT
    				a.account_id as account_id
    				FROM tbl_savan_casino_history_v2 a
	    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY a.account_id
    			)t
    		");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.stake_amount) as total_stake,
    			sum(a.valid_stake) as valid_bet,
    			sum(a.tips) as tips,
    			
    			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as win_loss,
			    (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
			    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as total,
    			
    			(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as pl_total
    
    			FROM tbl_savan_casino_history_v2 a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    			");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getSAVANCSTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
    public function getSAVANCSTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.stake_amount) as total_stake,
    			sum(a.valid_stake) as valid_bet,
    			sum(a.tips) as tips,
    
    			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
    			((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
    			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + sum(a.tips)) as total, #sc_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
				- sum(a.tips))*-1 as pl_total
    
    			FROM tbl_savan_casino_history_v2 a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANSMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANSMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    		$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
	    }else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		    if($_GET['testCurrency']==1){
			    $strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
	    	}else{
	    		$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
	    	}
	    }else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
	    	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
	    }
	    $connection = Yii::app()->db_cv999_fd_master;
	    $command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,4) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.stake_amount) as total_stake,
    			sum(a.valid_stake) as valid_bet,
	    		sum(a.tips) as tips,
    			
    			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
    			((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
    			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + sum(a.tips)) as total, #sma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
	    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
	    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
				- sum(a.tips))*-1 as pl_total
    				
    			FROM tbl_savan_casino_history_v2 a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,6) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.stake_amount) as total_stake,
    			sum(a.valid_stake) as valid_bet,
    			sum(a.tips) as tips,
    
    			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
    			((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + sum(a.tips)) as total, #ma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
				- sum(a.tips))*-1 as pl_total
    
    			FROM tbl_savan_casino_history_v2 a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANAGTTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANAGTTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    			$strQueryWhere="";
    		}
	    }else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
	    	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
	    }else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		    if($_GET['testCurrency']==1){
		    	$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
		    }else{
		    	$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
		    }
	    }else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
	    	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
	    }
	    $connection = Yii::app()->db_cv999_fd_master;
	    $command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,8) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.stake_amount) as total_stake,
    			sum(a.valid_stake) as valid_bet,
	    		sum(a.tips) as tips,
    			
    			(sum(a.win_loss * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
    			((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
    			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + sum(a.tips)) as total, #agt_total,
    			
    			(((sum(a.win_loss) + sum(a.commission))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
				- sum(a.tips))*-1 as pl_total
    
    			FROM tbl_savan_casino_history_v2 a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLossDetails($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
   	 	//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
		}
    	//Agent ID
    	$strAccountIDWhere='';
    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    
    	if(trim($_GET['accountID'])==''){
    		$strAccountIDWhere='';
    	}else{
    		if($intAccountIDLength!=10){
    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    		}else{
    			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    		}
    	}
    	$pl_total='';
    	if(trim($_GET['accountID'])==''){
    		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		+ sum(a.tips))*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    		+ sum(a.tips))*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    		+ sum(a.tips))*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    		+ sum(a.tips))*-1 as pl_total";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    			SELECT  @rownum := @rownum + 1 row, t.*
    			FROM  (
	    			SELECT
	    			substring(a.account_id,1,$intAccountIDLength) as account_id,
	    			j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					(sum(a.tips))*-1 as tips,
	    			sum(a.win_loss) as mem_win_loss,
					sum(a.commission) as mem_comm,
					sum(b.bonus)  as mem_bonus,
					((sum(a.win_loss) + sum(a.commission))) as mem_total,
					(SELECT balance FROM vwGetSAVANVEGASMemberWinLossDetailsV2 WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
	    
	    			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				sum(a.tips) as agt_tips,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))+ sum(a.tips)) as agt_total,
				   
				    ((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
				    ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
    				sum(a.tips) as ma_tips,
				    (((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))+ sum(a.tips)) as ma_total,
				   
				    ((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
				    ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
				    sum(a.tips) as sma_tips,
    				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))+ sum(a.tips)) as sma_total,
				    
				    ((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
				    ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
    				sum(a.tips) as sc_tips,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))+ sum(a.tips)) as sc_total,
				    
				    ((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
				    (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.tips) as mc_tips,
				    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as mc_total,
				        
				    ".$pl_total."
				        
	    			FROM tbl_savan_casino_history_v2 a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
	    			GROUP BY substring(a.account_id,1,$intAccountIDLength)
	    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetailsCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetailsCount()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetSAVANVEGASMemberWinLossDetailsV2 WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
       	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSAVANVEGASMemberWinLossDetailsV2 t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc,t.id desc LIMIT $startIndex , $limit");
       	$rows = $command->query();
       	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetailsSummaryFooter
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetailsSummaryFooter()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row,
    			count(t.win_loss) as bet_count,
    			sum(t.bet_amount) as bet_amount,
    			sum(t.amount_wagers) as amount_wagers,
    			sum(t.win_loss) as win_loss,
    			sum(t.commission) as commission,
    			sum(t.amount_tips) as amount_tips,
    			sum(t.total) as total,
    			(Select t.balance from vwGetSAVANVEGASMemberWinLossDetailsV2 t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
    			FROM vwGetSAVANVEGASMemberWinLossDetailsV2 t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
    	$rows = $command->query();
    	foreach($rows as $row){
    		echo $row['bet_count'] . ';';
    		echo $row['bet_amount'] . ';';
    		echo $row['amount_wagers'] . ';';
    		echo $row['win_loss'] . ';';
    		echo $row['commission'] . ';';
    		echo $row['amount_tips'] . ';';
    		echo $row['total'] . ';';
    		echo $row['balance'];
    	}
    }
    
    /**
     * @todo getSavanVegasWinLossResultDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-11-17
     */
	public function getSavanVegasResultDetails(){
		$row = TableSavanCasinoHistoryV2::model()->find(array('select'=>'result_imgname','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
		 
		return 'https://pic01.clubvegas999.com:8443/uploadmanager/app/downLoadService/getResource?'.$row['result_imgname'];
	}
	
	public function getSavanVegasWinLossResultCards()
	{
	$row = TableSavanCasinoHistoryV2::model()->find(array('select'=>'game_type_id,banker_result,live_member_report_details','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
		
		$opt1='';$opt2='';
		if ($row['game_type_id']==1){
			$opt1='PLAYER';$opt2='BANKER';
			$index1 = 0;
			$index2 = 1;
			
		}
		if ($row['game_type_id']==4){
			$opt1='DRAGON';$opt2='TIGER';
			$index1 = 0;
			$index2 = 1;
		}
		
		if ($row['game_type_id']!=2){
			$result = explode("~", $row['banker_result']);
			$spl = json_decode($result[1]);
			//echo count($spl[0]);
			echo '<div style="float:left;padding-right:20px;"><div style="background-color:#929292;color:#FFF;text-align:center;margin-bottom:5px"><b>'.$opt1.'</b></div>';
			$margin=0;
			$deg = count($spl[1])>1 ? -5: 0;

			for ($i=0; $i<= count($spl[$index1])-1; $i++){
				//-webkit-transform: rotate('.$deg.'deg);-moz-transform: rotate('.$deg.'deg);
				echo '<img style="margin-left:'.$margin.'px;"  src="'.Yii::app()->request->baseUrl.'/images/cards/'.$spl[$index1][$i].'.png">';
				$margin=-59;
				$deg +=5;
			}
			
			echo '</div><div style="float:right"><div style="background-color:#929292;color:#FFF;text-align:center;margin-bottom:5px"><b>'.$opt2.'</b></div>';
			
			$margin1=0;
			$deg = count($spl[0])>1 ? -5: 0;
			for ($i=0; $i<= count($spl[$index2])-1; $i++){
				//-webkit-transform: rotate('.$deg.'deg);-moz-transform: rotate('.$deg.'deg);
				echo '<img style="margin-left:'.$margin1.'px;"  src="'.Yii::app()->request->baseUrl.'/images/cards/'.$spl[$index2][$i].'.png">';
				$margin1=-59;
				$deg +=5;
			}
			echo '</div>';
			//print_r($spl);
		}
	}
	public function getSavanVegasWinLossResultDetails()
	{
		$row = TableSavanCasinoHistoryV2::model()->find(array('select'=>'game_type_id,currency,banker_result,live_member_report_details','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
	
		echo $this->getWinLossBettingDetail($row['game_type_id'], $row['banker_result'], $row['live_member_report_details'], $row['currency']);
		
	}
    
    /**
     * @todo apply amount incentive if currency is incentive
     * @author leokarl
     * @since 2013-02-23
     */
    public function amountIncentive($currency, $amount){
    	$row = TableCurrency::model()->find(array('select'=>'incentive,incentive_amount','condition'=>'currency_name=:currency',
    			'params'=>array(':currency'=>$currency),));
    	
    	return ($row['incentive'] == 1) ? number_format($amount * $row['incentive_amount'],2,".",",") : number_format($amount,2,".",",");
    }
    
    /**
     * @todo return winloss betting details in html table format
     * @author leokarl
     * @since 2012-12-12
     * @param int $game_type
     * @param array $bet_result_detail_array
     */
    public function getWinLossBettingDetail($game_type, $banker_result, $live_member_report_details, $currency){
    	
    	 
    	$table_data = '<table border="0" cellspacing="2" cellpadding="0" width="100%">'.
				'<tr><th class="c_winlossresultheader">Type</th><th class="c_winlossresultheader">Bet Amount</th><th class="c_winlossresultheader">Win/Loss</th></tr>';
    	$winloss_result='';
		if($game_type == 1){ // baccarat

			$result=json_decode($live_member_report_details, true);

			foreach ($result as $item) {
				// copy item to grouped
				$grouped[$item['betType']][] = $item;
			}
				
			foreach($grouped as $key => $value){
				$groupResult = $grouped[$key];
				$type='';
				$winloss = 0;
				$betAmount = 0;
				$winlossTotal = 0;
				for ($i = 0; $i <= count($groupResult)-1; $i++){
					if ($key==$groupResult[$i]['betType']){
						$winloss=$groupResult[$i]['winLossAmount'] - $groupResult[$i]['betAmount'];
						$betAmount+= $groupResult[$i]['betAmount'];
						$winlossTotal+= $winloss;
					}
				}
			
				switch ($key) {
					case 'BC_BANKER':
    					$type = '<font color="red">BANKER</font>';
    					break;
    				case 'BC_PLAYER':
    					$type = '<font color="#000c75">PLAYER</font>';
    					break;
    				case 'BC_TIE':
    					$type = '<font color="black">TIE</font>';
    					break;
    				case 'BC_SMALL':
    					$type = '<font color="blue">SMALL</font>';
    					break;
    				case 'BC_BIG':
    					$type = '<font color="green">BIG</font>';
    					break;
    				case 'BC_BANKER_PAIR':
    					$type = '<font color="red">BANKER PAIR</font>';
    					break;
    				case 'BC_PLAYER_PAIR':
    					$type = '<font color="#000c75">PLAYER PAIR</font>';
    					break;
    				case 'TIPS':
    					$type = '<font color="black">TIPS</font>';
    					break;
				}
				if ($winlossTotal<0){
					$winloss_result='<font color="red" >' . money_format('%i',$winlossTotal) . '</font>';
				}
				if ($winlossTotal>0){
					$winloss_result='<font color="#000c75">' . money_format('%i',$winlossTotal) . '</font>';
				}
				$table_data .= '<tr><td class="c_bet_type">' .$type. '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $betAmount) . '</td><td class="c_win_loss">' . $winloss_result . '</td></tr>';
			}
 
		}else if($game_type == 4){ // dragon tiger

			$result=json_decode($live_member_report_details, true);
			foreach ($result as $item) {
				// copy item to grouped
				$grouped[$item['betType']][] = $item;
			}
			
			foreach($grouped as $key => $value){
				$groupResult = $grouped[$key];
				$type='';
				$winloss = 0;
				$betAmount = 0;
				$winlossTotal = 0;
				for ($i = 0; $i <= count($groupResult)-1; $i++){
					if ($key==$groupResult[$i]['betType']){
						$winloss=$groupResult[$i]['winLossAmount'] - $groupResult[$i]['betAmount'];
						$betAmount+= $groupResult[$i]['betAmount'];
						$winlossTotal+= $winloss;
					}
				}
				
				switch ($key) {
					case 'DT_DRAGON':
						$type = '<font color="red">DRAGON</font>';
						break;
					case 'DT_TIGER':
						$type = '<font color="#000c75">TIGER</font>';
						break;
					case 'DT_TIE':
						$type = '<font color="black">TIE</font>';
						break;
					case 'TIPS':
						$type = '<font color="black">TIPS</font>';
						break;
				}
				if ($winlossTotal<0){
					$winloss_result='<font color="red" >' . money_format('%i',$winlossTotal) . '</font>';
				}
				if ($winlossTotal>0){
					$winloss_result='<font color="#000c75">' . money_format('%i',$winlossTotal) . '</font>';
				}
				$table_data .= '<tr><td class="c_bet_type">' .$type. '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $betAmount) . '</td><td class="c_win_loss">' . $winloss_result . '</td></tr>';
			}

		}else if($game_type == 2){ // roulette
			
			$result=json_decode($live_member_report_details, true);
			foreach ($result as $item) {
				// copy item to grouped
				$grouped[$item['betType']][] = $item;
			}
				
			foreach($grouped as $key => $value){
				$groupResult = $grouped[$key];
				$type='';
				$winloss = 0;
				$betAmount = 0;
				$winlossTotal = 0;
				for ($i = 0; $i <= count($groupResult)-1; $i++){
					if ($key==$groupResult[$i]['betType']){
						$winloss=$groupResult[$i]['winLossAmount'] - $groupResult[$i]['betAmount'];
						$betAmount+= $groupResult[$i]['betAmount'];
						$winlossTotal+= $winloss;
					}
				}
				$betType = str_replace("RL_","",$key);
				switch ($betType) {
					case 'RED':
						$type = '<font color="red">' . $betType . '</font>';
						break;
					case 'BLACK':
						$type = '<font color="black">' . $betType . '</font>';
						break;
					case 'ODD':
						$type = '<font color="#000c75">' . $betType . '</font>';
						break;
					case 'EVEN':
						$type = '<font color="#000c75">' . $betType . '</font>';
						break;
					case 'SMALL':
						$type = '<font color="green">' . $betType . '</font>';
						break;
					case 'BIG':
						$type = '<font color="blue">' . $betType . '</font>';
						break;
					default:
						$type = '<font color="black">' . $betType . '</font>';
				}
				if ($winlossTotal<0){
					$winloss_result='<font color="red" >' . money_format('%i',$winlossTotal) . '</font>';
				}
				if ($winlossTotal>0){
					$winloss_result='<font color="#000c75">' . money_format('%i',$winlossTotal) . '</font>';
				}
				$table_data .= '<tr><td class="c_bet_type">' .$type. '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $betAmount) . '</td><td class="c_win_loss">' . $winloss_result . '</td></tr>';
			}
			
		}
		 
		$table_data .= '</table>';
		return  $table_data;
    }
    
    /**
     * @todo extract raw bet_type and return formatted bet_type
     * @author leokarl
     * @since 2012-12-13
     * @param string $raw_type
     */
    public function getRouletteTypeFromBetResultDetail($row_number){
    	/*
    	 four number(00010203)bet^
	     line bet 1 ^ Line bet 2 ^ line bet 3 ^ Dozen 1 ^ Dozen 2 ^ Dozen 3
		 ^red^black^odd^even^small^big^
    	 */
    	//^0^0^0^0^0^0^0^210^210^0^100^0^0^0^0^0^
    	//straight bet,	split bet,	triple bet,	conner bet,	line bet,	three number,	four number,	
    	//row1,	row2,	row3,	column1,	column2,	column3,	red,	black,	odd,	even,	small,	big
    	
    	switch ($row_number -1)
    	{
    		case 0:
    			return 'Corner Bet'; break;
    		case 1:
    			return 'Line Bet'; break;
    		case 2:
    			return 'Three Number'; break;
    		case 3:
    			return 'Four Number'; break;
    		case 4:
    			return 'Row One'; break;
    		case 5:
    			return 'Row Two'; break;
    		case 6:
    			return 'Row Three'; break;
    		case 7:
    			return 'Column One'; break;
    		case 8:
    			return 'Column Two'; break;
    		case 9:
    			return 'Column Three'; break;
    		case 10:
    			return 'Red'; break;
    		case 11:
    			return 'Black'; break;
    		case 12:
    			return 'Odd'; break;
    		case 13:
    			return 'Even'; break;
    		case 14:
    			return 'Small'; break;
    		case 15:
    			return 'Big'; break;
    		default:
    			return "Undefined";
    	}
    }
    
    /**
     * @todo extract raw bet_type and return formatted number bet_type
     * @author leokarl
     * @since 2012-12-13
     * @param string $raw_type
     */
    public function getRouletteNumberTypeFromBetResultDetail($raw_type){
    	// 0^0^112233 = 11&22&33
    	$extract_type = split("\^",$raw_type);
    	$type = $extract_type[count($extract_type)-1];
    	$separator_number = 2;
    	
    	$loop_until = (strpos($raw_type,'^') === false) ? count($extract_type)-1 : count($extract_type)-2;
    	
    	for($i = 0; $i <= (strlen($type)/2)/2; $i++){
    		$type = substr_replace($type,'&',$separator_number,0);
    		$separator_number += 3;
    	}
    	$type = (substr_compare($type,"&",-1,1) == 0) ? substr($type,0,strlen($type) -1) : $type;
    	$type = str_replace('&', '<font color="#c0d2fa">&</font>', $type);
    	
    	return (strlen($raw_type) <= 2) ? $raw_type : $type;
    }
    
    /**
     * @todo return winloss from bet_result_detail
     * @author leokarl
     * @since 2012-12-13
     * @param array $bet_result_detail_array
     * @param int $row_count
     */
    public function getRouletteWinLossFromBetResultDetail($bet_result_detail_array, $row_count){
    	$bet_detail = split("\|",$bet_result_detail_array);
    	$row_detail = split("#",$bet_detail[$row_count]);
    	//$winloss = (count($row_detail) == 4) ? $row_detail[3] : $row_detail[2];
    	
    	// return winloss
    	return $row_detail[count($row_detail)-1];
    }
    public function getRouletteWinLossFromBetResultDetail1($bet_result_detail_array, $row_count, $line_number){
    	$bet_detail = split("\|",$bet_result_detail_array);
    	//$row_detail = split("#",$bet_detail[$row_count]);
    	//$winloss = (count($row_detail) == 4) ? $row_detail[3] : $row_detail[2];
    	$last = '';
    	for($i=$row_count; $i < count($bet_detail); $i++){
    		$last .= $bet_detail[$i];
    	}
    	$row_detail = split("\^",$last);
    	$final = split("#",$row_detail[$line_number]);
    	
    	// return winloss
    	//return $row_detail[count($row_detail)-1];
    	return $final[count($final) -1];
    }
    
    /**
     * @todo get baccarat bet type
     * @author leokarl
     * @since 2012-12-12
     * @param int $bet_detail_number
     */
    public function getBaccaratBetTypeByBetDetailNumber($bet_detail_number){
    	
    	switch ($bet_detail_number)
    	{
    		case 0:
    			return '<font color="red">Banker</font>'; break;
    		case 1:
    			return '<font color="blue">Player</font>'; break;
    		case 2:
    			return '<font color="green">Tie</font>'; break;
    		case 3:
    			return '<font color="red">Banker pair</font>'; break;
    		case 4:
    			return '<font color="blue">Player pair</font>'; break;
    		case 5:
    			return '<font color="red">Big</font>'; break;
    		case 6:
    			return '<font color="blue">Small</font>'; break;
    		default:
    			return "Undefined";
    	}
    }
    
    
    /**
     * @todo get dragon tiger bet type
     * @author leokarl
     * @since 2012-12-12
     * @param int $bet_detail_number
     */
    public function getDragonTigerBetTypeByBetDetailNumber($bet_detail_number){
    	if($bet_detail_number == 0){
    		return '<font color="red">Dragon</font>';
    	}
    	elseif($bet_detail_number == 1){
    		return '<font color="blue">Tiger</font>';
    	}
    	elseif($bet_detail_number == 2){
    		return '<font color="green">Tie</font>';
    	}
    }
    
    /**
     * @todo disallow special characters
     * @author leokarl
     * @since 2012-12-08
     * @param string $mystring
     */
    public function isSpecialCharPresent($mystring){
    	if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
    			&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
    			&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false && strpos($mystring, '_') === false
    			&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '-') === false && strpos($mystring, '`') === false
    			&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
    			&& strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
    			&& strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){
    
    		return FALSE; // special character not present
    	}else{
    		return TRUE; // special character present
    	}
    }
    
    //==========================================EXPORT ALL TO EXCEL==================================================
    
    public function getExportCostaVegasWinLoss()
    {

    	$vigSharing=0;
    	if($_GET['vigSharing']==''){
    		$vigSharing=0;
    	}else{
    		$vigSharing=$_GET['vigSharing'];
    	}
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    			$strQueryWhereForBalance='';
    		}else{
    			$strQueryWhere="";
    			$strQueryWhereForBalance='';
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
    		}
    		$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
    		$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
    				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    				FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,2) as account_id,
    				j.account_name,
    				a.currency_name,
    				sum(a.amount_wagers) as valid_bet,
    				avg(a.amount_wagers) as average_bet,
    				sum(a.amount_tips) as tips,
    					
    				(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as total,
    					
    				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
    				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.amount_tips) as mc_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
    					
    				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
    				sum(a.amount_tips) as vig_tips,
    				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
    					
    				(
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
    				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
    		) as pl_total
    					
    				FROM tbl_costavegas_casino_history a
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,2)
    		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r
    				order by t.currency_name asc,t.account_id asc 
    				");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    	$command = $connection->createCommand("
    			#SUB COMPANY WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,4) as account_id,
    			j.account_name,
    			a.currency_name,
    			sum(a.amount_wagers) as valid_bet,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_tips) as tips,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    	)*-1 as total,
    				
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss, #sc_win_loss,
    			((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm, #sc_comm,
    			sum(a.amount_tips) as mc_tips,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total, #sc_total
    
    			0 as vig_win_loss,
    			0 as vig_tips,
    			0 as vig_total,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    	)*-1 as pl_total
    				
    			FROM tbl_costavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,4)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    			");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    	$command = $connection->createCommand("
    	#SENIOR MASTER WINLOSS
    	SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    	FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,6) as account_id,
    			j.account_name,
    			a.currency_name,
    			sum(a.amount_wagers) as valid_bet,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_tips) as tips,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    			)*-1 as total,
    				
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss, #sma_win_loss,
    			((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm, #sma_comm,
    			sum(a.amount_tips) as mc_tips,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total, #sma_total
    				
    			0 as vig_win_loss,
    			0 as vig_tips,
    			0 as vig_total,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    			)*-1 as pl_total
    				
    			FROM tbl_costavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,6)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    			");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    	$command = $connection->createCommand("
    	#MASTER WINLOSS
    	SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    	FROM  (
    	SELECT
    	SUBSTRING(a.account_id,1,8) as account_id,
    	j.account_name,
    	a.currency_name,
    	sum(a.amount_wagers) as valid_bet,
    	avg(a.amount_wagers) as average_bet,
    	sum(a.amount_tips) as tips,
    		
    	(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    	)*-1 as total,
    				
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
    			((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
    			sum(a.amount_tips) as mc_tips,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total, #ma_total
    				
    			0 as vig_win_loss,
    			0 as vig_tips,
    			0 as vig_total,
    				
    			(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    	)*-1 as pl_total
    				
    			FROM tbl_costavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,8)
    	)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    			");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    			$command = $connection->createCommand("
    			#AGENT WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
    			FROM  (
    			SELECT
    			a.account_id as account_id,
    			b.account_name,
    			a.currency_name,
    			sum(a.amount_wagers) as valid_bet,
    			avg(a.amount_wagers) as average_bet,
    			(sum(a.amount_tips))*-1 as tips,
    				
    			#sum(a.amount_settlements - a.amount_wagers) as win_loss,
    			sum(a.amount_settlements - a.amount_wagers) as win_loss,
    			sum(a.commission) as commission,
    			((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as total,
    					(SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id
    					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as balance,
    						
    					(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,#agt_win_loss,
    					((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm, #agt_comm,
    					sum(a.amount_tips) as agt_tips,
    					((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total, #agt_total
    								
    							0 as vig_win_loss,
    							0 as vig_tips,
    							0 as vig_total,
    								
    							#(((sum(a.amount_settlements - a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)))*-1) + (sum((a.amount_wagers * ((100 - (CASE WHEN i.game_sharing IS NULL THEN 0 ELSE i.game_sharing END))/100)) * (-(CASE WHEN i.game_commission IS NULL THEN 0 ELSE i.game_commission END)/100)))) as pl_total
    							(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    							((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    					)*-1 as pl_total
    								
    							FROM tbl_costavegas_casino_history a
    							LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    							WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    							GROUP BY a.account_id
    					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    							");
    	}
    							$rows = $command->query();
    							return $rows;
    	}
    	
    	public function getExportCostaVegasWinLossDetails()
    	{
 
    		$vigSharing=0;
    		if($_GET['vigSharing']==''){
    			$vigSharing=0;
    		}else{
    			$vigSharing=$_GET['vigSharing'];
    		}
    		//additional parameter
    		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    			if($_GET['testCurrency']==1){
    				$strQueryWhere=" AND a.currency_name!='TEST'";
    				$strQueryWhereForBalance='';
    			}else{
    				$strQueryWhere="";
    				$strQueryWhereForBalance='';
    			}
    		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    			$strQueryWhereForBalance='';
    		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    			if($_GET['testCurrency']==1){
    				$strQueryWhere=" AND a.currency_name!='TEST' AND a.game_id='" . $_GET['intGame'] . "'";
    			}else{
    				$strQueryWhere=" AND a.game_id='" . $_GET['intGame'] . "'";
    			}
    			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "' AND a.game_id='" . $_GET['intGame'] . "'";
    			$strQueryWhereForBalance=" AND game_id='" . $_GET['intGame'] . "'";
    		}
    		//Agent ID
    		$strAccountIDWhere='';
    		$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    	
    		if(trim($_GET['accountID'])==''){
    			$strAccountIDWhere='';
    		}else{
    			if($intAccountIDLength!=10){
    				$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    			}else{
    				$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    			}
    		}
    		$pl_total='';
    		if(trim($_GET['accountID'])==''){
    			$pl_total="(
    			(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) - #as mc_total,
    			((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) #as vig_total,
    			) as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==2){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    			)*-1 as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==4){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    			)*-1 as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==6){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    			)*-1 as pl_total";
    		}elseif(strlen(trim($_GET['accountID']))==8){
    			$pl_total="(((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    			)*-1 as pl_total";
    		}
    		$connection = Yii::app()->db_cv999_fd_master;
    		$command = $connection->createCommand("
    				SELECT  @rownum := @rownum + 1 row, t.*
    				FROM  (
    				SELECT
    				a.bet_date,
    				substring(a.account_id,1,$intAccountIDLength) as account_id,
    				j.account_name,
    				a.game_id,
    				a.currency_name,
    				sum(a.amount_wagers) as valid_bet,
    				avg(a.amount_wagers) as average_bet,
    				(sum(a.amount_tips))*-1 as tips,
    				sum(a.amount_settlements - a.amount_wagers) as mem_win_loss,
    				sum(a.commission) as mem_comm,
    				((sum(a.amount_settlements - a.amount_wagers) + sum(a.commission))) as mem_total,
    				(SELECT balance FROM vwGetCostaVegasMemberWinLossDetails WHERE account_id=b.account_id
    				AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
    	
    				(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
    				((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				sum(a.amount_tips) as agt_tips,
    				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
    				 
    				((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
    				((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
    				sum(a.amount_tips) as ma_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
    				 
    				((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
    				((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
    				sum(a.amount_tips) as sma_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
    				 
    				((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
    				((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
    				sum(a.amount_tips) as sc_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
    					
    				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
    				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.amount_tips) as mc_tips,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.amount_tips)) as mc_total,
    				 
    				(CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) as vig_win_loss,
    				sum(a.amount_tips) as vig_tips,
    				((CASE WHEN sum(a.amount_settlements - a.amount_wagers) < 0 THEN (sum(a.amount_settlements - a.amount_wagers)*-(". $vigSharing ."/100)) ELSE 0 END) + sum(a.amount_tips)) as vig_total,
    	
    				". $pl_total ."
    					
    				FROM tbl_costavegas_casino_history a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY substring(a.account_id,1,$intAccountIDLength)
    		)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    		");
    		$rows = $command->query();
    		return $rows;
    	}
    	
    	public function getExportCostaVegasMemberWinLossDetails()
    	{
    		if($_GET['intGame']!==''){
    			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    		}else{
    			$addWhere="";
    		}
    		$connection = Yii::app()->db_cv999_fd_master;
    		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetCostaVegasMemberWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc, t.id desc ");
    		$rows = $command->query();
    		return $rows;
    	}
    	
    	public function getExportSAVANWinLoss()
    	{
    		//additional parameter
    		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
    		}else{
    			$strQueryWhere="";
    		}
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}else{
    			$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
    		}
    		$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
    		$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as total,
					
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as mc_total,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as pl_total
					
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
			");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
				#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) 
					)*-1 as total,
					
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
					 
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
			");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$command = $connection->createCommand("
				#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) 
					)*-1 as total,
						
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
				");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					sum(a.tips) as tips,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as total,
					
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
    				sum(a.tips) as mc_tips,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,8)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
			");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*,cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					(sum(a.tips))*-1 as tips,
					
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(b.bonus)  as bonus,
					((sum(a.win_loss) + sum(a.commission))) as total,
					(SELECT balance FROM vwGetSAVANVEGASMemberWinLossDetailsV2 WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
					
					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				sum(a.tips) as agt_tips,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
					
					(((sum(a.win_loss) + sum(a.commission))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
					)*-1 as pl_total
					    
					FROM tbl_savan_casino_history_v2 a
	    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id 
			");
	    }
	    $rows = $command->query();
	    return $rows;
     }
     
     public function getExportSAVANWinLossDetails()
     {
     	//additional parameter
     	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.currency!='FUN'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST' AND a.game_type_id='" . $_GET['intGame'] . "'";
			}else{
				$strQueryWhere=" AND a.game_type_id='" . $_GET['intGame'] . "'";
			}
			$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "' AND a.game_type_id='" . $_GET['intGame'] . "'";
			$strQueryWhereForBalance=" AND game_type_id='" . $_GET['intGame'] . "'";
		}
    	//Agent ID
    	$strAccountIDWhere='';
    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    
    	if(trim($_GET['accountID'])==''){
    		$strAccountIDWhere='';
    	}else{
    		if($intAccountIDLength!=10){
    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    		}else{
    			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    		}
    	}
    	$pl_total='';
    	if(trim($_GET['accountID'])==''){
    		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    		)*-1 as pl_total";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    			SELECT  @rownum := @rownum + 1 row, t.*
    			FROM  (
	    			SELECT
	    			substring(a.account_id,1,$intAccountIDLength) as account_id,
	    			j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.valid_stake) as average_bet,
					sum(a.stake_amount) as total_stake,
					sum(a.valid_stake) as valid_bet,
					(sum(a.tips))*-1 as tips,
	    			sum(a.win_loss) as mem_win_loss,
					sum(a.commission) as mem_comm,
					sum(b.bonus)  as mem_bonus,
					((sum(a.win_loss) + sum(a.commission))) as mem_total,
					(SELECT balance FROM vwGetSAVANVEGASMemberWinLossDetailsV2 WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
	    
	    			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				sum(a.tips) as agt_tips,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
				   
				    ((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
				    ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
    				sum(a.tips) as ma_tips,
				    (((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.valid_stake * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
				   
				    ((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
				    ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
				    sum(a.tips) as sma_tips,
    				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.valid_stake * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
				    
				    ((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
				    ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
    				sum(a.tips) as sc_tips,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.valid_stake * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.valid_stake * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
				    
				    ((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
				    (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				sum(a.tips) as mc_tips,
				    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.valid_stake * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + sum(a.tips)) as mc_total,
				        
				    ".$pl_total."
				        
	    			FROM tbl_savan_casino_history_v2 a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id 
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.end_time>='" . $_GET['dtFrom'] . "' AND a.end_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
	    			GROUP BY substring(a.account_id,1,$intAccountIDLength)
	    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
    	");
    	$rows = $command->query();
    	return $rows;
     }
     
     public function getExportSAVANMemberWinLossDetails()
     {
     	if($_GET['intGame']!==''){
     		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
     	}else{
     		$addWhere="";
     	}
     	$connection = Yii::app()->db_cv999_fd_master;
     	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSAVANVEGASMemberWinLossDetailsV2 t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc,t.id desc ");
     	$rows = $command->query();
     	return $rows;
     }
}