<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/deposit.css" />

<script>
$(document).ready(function() {
	$("#ddlOtherList").css("display","none"); // hide ddlOtherList onload
});

isProcessingUnlock = false;
function cancelButton(account_id){
	
	if(isProcessingUnlock){
	       return;
	}
	jQuery.ajax({
		url: baseUrl+'/index.php?r=CashPlayer/CashPlayerList/UnlockPlayer',
		type: 'POST',
		data: {'accountID' :account_id},
		context: '',
		async: false,
		success: function(data){
			isProcessingUnlock = false;
		}	
	});
	  
}

$(".ui-dialog-titlebar-close").click(function() {
	cancelButton("<?php echo $_GET['account_id']; ?>");
});

</script>
<table class="deposit">
	<tr>
		<td class="left">Account ID</td>
		<td class="right"><b><?php echo $_GET['account_id'];?></b></td>
	</tr>
	<tr>
		<td class="left">Currency</td>
		<td class="right"><?php echo $currency;?></td>
	</tr>
	<tr>
		<td class="left">Amount</td>
		<td class="right">
			<?php echo CHtml::textField('txtDepositAmount','',array('id'=>'txtDepositAmount'));?> 
		</td>
	</tr>
	<tr>
		<td class="left">Deposit Type</td>
		<td class="right">
			<?php echo CHtml::radioButton('radDepositType',true,array('value'=>'1','id'=>'bank','onchange'=>'changeDepositType(this.value);'));?>
			<?php echo CHtml::label('Banks', 'bank');?>
			<?php echo CHtml::radioButton('radDepositType',false,array('value'=>'0','id'=>'other','onchange'=>'changeDepositType(this.value);'));?>
			<?php echo CHtml::label('Others', 'other');?>
		</td>
	</tr>
	<tr>
		<td class="left">Transaction Item</td>
		<td class="right">
			<?php echo CHtml::dropDownList('ddlBankList', '',$bankList, array('empty' => '(Select bank)'));?>
			<?php echo CHtml::dropDownList('ddlOtherList', '',$otherList, array('empty' => '(Select list)'));?>	
		</td>
	</tr>
</table>
<hr/>
<div style="text-align: center;">
<?php echo CHtml::button('Confirm',array('id'=>'btnConfirm', 'class'=>'btn red',
		'onclick'=>'makeDeposit("'. $_GET['account_id'] .'", $("#txtDepositAmount").val(), $("input:radio[name=radDepositType]:checked").val(),
		getSelectedDepositType());'));?>

<?php 
echo  CHtml::button('Cancel',array('id'=>'btnCancel', 'class'=>'btn red', 'onclick'=>'$("#deposit-dialog").dialog("close"); cancelButton("'.$_GET['account_id'].'");'));
?>

</div>