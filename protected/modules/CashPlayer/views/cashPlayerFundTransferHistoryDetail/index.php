<!--@TODO: VIEWS FOR CASH PLAYER TRANSACTION HISTORY FOR FUND DETAIL
    @AUTHOR: Allan
    @SINCE: 05082012
-->
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>


<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

<script type="text/javascript">
document.getElementById('cashplayerHeader').className="start active";
document.getElementById('mnu_fundTrans').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Fund Transfer History</a></li>");

function goBack()
{
	var dateFrom = (document.getElementById('datefrom').value).split("-");
	
	var monthF = dateFrom[1];
	var dayF = dateFrom[2];
	var yearF = dateFrom[0];
	var dateF=(yearF + "/" + monthF + "/" + dayF);
	var dateTo =(document.getElementById('dateto').value).split("-");
	var monthT = dateTo[1];
	var dayT = dateTo[2];
	var yearT = dateTo[0];
	if (dayT.length==1){
		dayT="0"+dayT;
		}
	if (dayF.length==1){
		dayF="0"+dayF;
		}
	if (monthT.length==1){
		monthT="0"+monthT;
		}
	if (monthF.length==1){
		monthF="0"+monthF;
		}
	var dateT= (yearT + "/" + monthT + "/" +dayT);
	var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
	var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerFundTransferHistory&dateFrom="+enc(dateSubmitFrom)+"&dateTo="+enc(dateSubmitTo)+"&id="+enc(document.getElementById('s_id').value);
}
</script>

<script type="text/javascript"> 

	var cRowNo=0;
	function tableTransHistoryToday(method_id,dateFrom,dateTo,accountID,optCasino) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerFundTransferHistoryDetail/CashPlayerFundTransferHistoryDetail&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID+'&optCasino='+optCasino, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Transaction Date', 'Currency','Transaction Type','Balance','Bonus','Casino Balance','Casino'],
			    colModel: [
					{name: 'transaction_date', index: 'transaction_date', width: 150, search:true,title:false},
					{name: 'currency_name', index: 'currency_name',align:'center', width: 80,title:false},
					{name: 'transaction_type', index: 'transaction_type', width: 120,title:false},
					{name: 'balance', index: 'balance', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'bonus', index: 'bonus', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'casino_balance', index: 'casino_balance', width: 120, align:"right",title:false},
					{name: 'casino_name', index: 'casino_name', width: 120,title:false},
					
			    ],
			   	loadtext:"",
			    loadComplete: function() {
					var grid=jQuery("#list1");
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"balance","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#cfe5fa'});
					}   
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rowNum: 20,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'transaction_date',
			    sortorder: 'DESC',
			    caption: ' <label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> FUND TRANSFER HISTORY - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
		});

	    function DepositViaFormatter(cellvalue, options, rowObject) {
			return cellvalue;
		};
	} 
				
	
</script>

<script type="text/javascript">

function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
         
function decryptparam()
{
	var strF = document.getElementById('f').value;
	var strT= document.getElementById('t').value;
	var encodeF = enc(strF);
	var encodeT = enc(strT);
	
	var dateF= encodeF.split("_");	
	var dateFrom=dateF[0];
	var dateT= encodeT.split("_");	
	var dateTo=dateT[0];
	var dateFSplit=dateFrom.split("/");
	var dateTSplit=dateTo.split("/");

	month1=dateFSplit[1].toString();
	day1=dateFSplit[2].toString();
	if (month1.length==1){
		month1='0'+month1;
	}
	if (day1.length==1){
		day1='0'+day1;
	}
	month2=dateTSplit[1].toString();
	day2=dateTSplit[2].toString();
	
	if (month2.length==1){
		month2='0'+month2;
	}
	if (day2.length==1){
		day2='0'+day2;
	}
	document.getElementById("datefrom").value=dateFSplit[0]+"-"+month1+"-"+day1;
	document.getElementById("dateto").value=dateTSplit[0]+"-"+month2+"-"+day2;

}
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

	function loadTransTable(selected_method,dateFrom,dateTo,accountID,optCasino){
		if (selected_method=='ALL'){
			//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID,optCasino);
 	
		}
	    else
	    {
	    	//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID,optCasino);
	    
		}
	}

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			var accountID=document.getElementById('txtAccountID').value;
			var optCasino=document.getElementById('optCAsino').value;
			loadTransTable("ALL",datefrom,dateto,accountID,optCasino);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			var optCasino=document.getElementById('optCasino').value;
			loadTransTable(methodtype,datefrom,dateto,accountID,optCasino);
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			var optCasino=document.getElementById('optCasino').value;
			loadTransTable(methodtype,datefrom,dateto,accountID,optCasino);
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			//document.getElementById('datefrom').value=(yearF+"-"+month1+"-"+day1);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			//document.getElementById('dateto').value=(yearT+"-"+month2+"-"+day2);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			var optCasino=document.getElementById('optCasino').value;
			loadTransTable(methodtype,dateSubmitFrom,dateSubmitTo,accountID,optCasino);
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript:decryptparam(); btnclick('Submit');">
<div id="parameter_area" style="width: 500px">
	<div class="header" >Cash Player Fund Transfer History</div>
	<form action="">
		<br/>
		<input type="text" id="txtAccountID" value="<?php echo $_GET['account_id'];?>" style="display: none;" >
		<table>
		<tr><td style="padding-left: 5px; width: 30%;">METHOD:</td>
			<td><select id="MethodType">
				<option value="ALL">All</option>
  				<option value="Deposit">Deposit</option>
  				<option value="Withdraw">Withdraw</option>
			</select>

		</td></tr>
		<tr><td style="padding-left: 5px;">CASINO:</td>
			<td><select id="optCasino">
			<option value="ALL">All</option>
			<?php 
  				$criteria=new CDbCriteria;
  				$criteria->select='casino_code,casino_name';
				$dataReader = TableCasino::model()->findAll($criteria);
				foreach ($dataReader as $row){
					echo '<option value="' . $row['casino_code'] . '">'. $row['casino_name'] . '</option>';
				}
			?>
			</select>

		</td></tr>
		<tr>
			<td style="padding-left: 5px;">FROM:</td>
			<td><input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />
				<select id="cbHourfrom">
					<?php 
						$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
						foreach ($hHour as  $value){
						echo '<option>'.$value.'</option>';
						}
					?></select>
				<input value=":00:00" style="border: 0px; background-color: transparent;width: 50px;" disabled>
			</td>
		</tr>
		
		<tr>
		<td style="padding-left: 5px;">TO:</td>
		<td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; background-color: transparent;width: 50px;" disabled></td></tr>
			<tr><td><div align="center">
			</td></tr>
		</table>
		</br>
		<div align="center" style="height: 35px; v-align: middle;">
		<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
		<input onclick="javascript:goBack();" type="button" value="Back" class="btn red">
		</div>
		</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		month=month.toString();
		day=day.toString();
		
		if (month.length==1){
			month='0'+month;
		}
		if (day.length==1){
			day='0'+day;
		}
		document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
		document.getElementById("dateto").value=(year + "-" + month + "-" + day);
		</script>
</div>		

	<div id="qry_result">
	</div>
	<div id="pager1"></div>
	<br/>
	<!--
	<div id="qry_resultTotal"></div>
	<div id="qry_resultTotal1"></div>
	<br/>-->
	<div><input id="f" value="<?php echo $_GET['f'];?>" hidden=true><input id="t" value="<?php echo $_GET['t'];?>" hidden=true><input id="s_id" value="<?php echo $_GET['s_id'];?>" hidden=true></div>
	
	
	<?php 
	// withdraw Dialog----------------------------------------------------------------
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'alter-dialog',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Update Trans Item',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> true,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewAlterItem"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
</body>
</html>