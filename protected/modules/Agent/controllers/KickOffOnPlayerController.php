<?php
class KickOffOnPlayerController extends MyController
{

	/**
	 * @todo actionIndex
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-28
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readWinLoss') || User::getUserType() === Constants::ROLE_AGENT)
		{
			$this->render("tab");
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}