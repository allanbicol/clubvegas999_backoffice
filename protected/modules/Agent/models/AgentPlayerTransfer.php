<?php
/**
 * @todo AgentPlayerTransfer Model
 * @copyright CE
 * @author Allan
 * @since 2012-06-13
 */
class AgentPlayerTransfer
{
	private $onlinePlayer;
	public function makeAgentPlayerTransfer()
	{
			$connection = Yii::app()->db_cv999_fd_master;
			$cashierAccountID = Yii::app()->session['account_id'];
			$level=Yii::app()->session['level_name'];
			$dateTime = date('Y-m-d H:i:s');
			if (isset($_POST['task'])==''){
				exit;
			}else{
				if ($_POST['task']=='getUpperAgentRecord')
				{
					$id=substr($_POST['AccountID'], 0,8);
					$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
					$command1= $connection->createCommand("SELECT (balance - credit) as winloss,market_type from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
					$rd=$command->queryRow();
					$rd1=$command1->queryRow();
					if ($rd1['market_type']==1){
						echo $rd['balance'] . "#" . $rd1['winloss']."#".$id."#".Yii::t('agent','agent.subcompanylist.upperbalance');
					}else{
						echo $rd['credit'] . "#" . $rd1['winloss']."#".$id."#".Yii::t('agent','agent.subcompanylist.uppercredit');
					}
				}
				
				if ($_POST['task']=='AgentPlayerDepositConfirm')
				{
					$commandGetMarketType= $connection->createCommand("SELECT market_type from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
					$rdMarketType=$commandGetMarketType->queryRow();
					if($rdMarketType['market_type']==1){
						AgentPlayerTransfer::depositCreditMarket();
					}else{
						AgentPlayerTransfer::depositCashMarket();
					}

				}
				
				if ($_POST['task']=='AgentPlayerWithdrawConfirm')
				{
					$commandGetMarketType= $connection->createCommand("SELECT market_type from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
					$rdMarketType=$commandGetMarketType->queryRow();
					if($rdMarketType['market_type']==1){
						AgentPlayerTransfer::withdrawCreditMarket();
					}else{
						AgentPlayerTransfer::withdrawCashMarket();
					}
					
				}
				
				if($_POST['task']=='checkPlayerOnlineStat')
				{
					//To check id the player is online in a lobby
					$this->onlinePlayer=new RedisManager();
					$checkLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['AccountID']);//($_POST['AccountID']);
					
					$command= $connection->createCommand("SELECT (balance - credit) as winloss,account_id from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
					$rd=$command->queryRow();
					//if the player is not online in a lobby the $checkLobby==0
					if ($checkLobby!=0){
						echo $checkLobby."#".$rd['winloss']."#".$rd['account_id'];
					}else{
						//Check if the Player Balance is Stocked on a lobby
						$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["AccountID"]."' and deposit_withdrawal<>0");
						$rd=$command->queryRow();
						if ($rd['deposit_withdrawal'] == 1){
								//to Display waiting for withdraw process in a lobby
								echo "w#w#w";

						}else{ //if there is no balance stocked on a lobby
							$commandBalance= $connection->createCommand("SELECT (balance - credit) as winloss,account_id from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
							$rdBalance=$commandBalance->queryRow();
							//to display the withdraw dialog
							echo "0#".$rdBalance['winloss']."#".$rdBalance['account_id'];
						}
					}

				}
				
				if ($_POST['task']=='logout'){
					//Logout Process
					$this->onlinePlayer=new RedisManager();
					
					$checkLobby=$this->onlinePlayer->getCasinoIDByPlayer($_POST['AccountID']);
						
					$this->onlinePlayer->forceLogoutOnLobbyByPlayer($_POST['AccountID']);
				}
				if ($_POST['task']=='checkIfStillProcessing'){
					$command = Yii::app()->db->createCommand("SELECT deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["AccountID"]."' and deposit_withdrawal<>0");
					$rd=$command->queryRow();

					if ($rd['deposit_withdrawal'] == 1) {
						echo 1;
					}else{
						echo 0;
					}
				}
			}
			
	}
	
	function depositCreditMarket()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		$id=substr($_POST['AccountID'], 0,8);
			
		$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
		$rd1=$command1->queryRow();
		//update agent player balance
		$balance= $rd1['balance'];
		$credit= $rd1['credit'];
		$currencyAP= $rd1['currency_id'];
		$newBalance= $balance + $_POST['amountDeposit'];
		
		//update upper agent  balance
		$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
		$rd=$command->queryRow();
		$agentBalance=$rd['balance'];
		$agentCredit=$rd['credit'];
		$newAgentBalance=$agentBalance - $_POST['amountDeposit'];
		
		$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
		$rd2=$command2->queryRow();
		
		$winloss=(($rd2['winloss'])*-1);
		if  ($_POST['amountDeposit'] > $winloss){
			echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
			exit;
		}
		
		if  ($_POST['amountDeposit'] > $agentBalance){
			echo Yii::t('agent','agent.subcompanylist.deposithigher');
			exit;
		}
		
		TableAgentPlayer::model()->updateAll(array('balance'=>$newBalance),'account_id="'.$_POST['AccountID'].'"');
		TableAgent::model()->updateAll(array('balance'=>$newAgentBalance),'account_id="'.$id.'"');
		
		//Save to Agent and agent player transaction history
		$postDepositAP =new AgentPlayerSaveTransactionHistory;
		//$postDepositA=new AgentSaveTransactionHistory;
		$postLog=new TableLog;
		$postLog1=new TableLog;
		
		$account_id=Yii::app()->session['account_id'];
		
		$transNum= TransactionNumber::generateTransactionNumber("MD");
		
		$postDepositAP->transaction_number=$transNum;
		$postDepositAP->agent_account_id=$_POST['AccountID'];
		$postDepositAP->currency_id=$currencyAP;
		$postDepositAP->trans_type_id=2;
		$postDepositAP->amount= $_POST['amountDeposit'];
		$postDepositAP->market_type= 'Credit Market';
		$postDepositAP->balance_before=$balance;
		$postDepositAP->balance_after=$newBalance;
		$postDepositAP->credit_before=$credit;
		$postDepositAP->credit_after=$credit;
		$postDepositAP->upper_agent_balance_before=$agentBalance;
		$postDepositAP->upper_agent_balance_after=$newAgentBalance;
		$postDepositAP->upper_agent_credit_before=$agentCredit;
		$postDepositAP->upper_agent_credit_after=$agentCredit;
		$postDepositAP->transaction_date=$dateTime;
		$postDepositAP->operator_id=$account_id;
			
		$postLog->operated_by=$account_id;
		$postLog->operated_by_level=$level;
		$postLog->operated=$_POST['AccountID'];
		$postLog->operated_level='Agent Player';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=3;
		$postLog->log_details='<b>Credit Market : Agent Player <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Deposit:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label></b>';
		
		$postLog1->operated_by=$account_id;
		$postLog1->operated_by_level=$level;
		$postLog1->operated=$id;
		$postLog1->operated_level='Agent';
		$postLog1->operation_time=$dateTime;
		$postLog1->log_type_id=3;
		$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Send:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label> to Credit Market: '.$_POST['AccountID'].'</b>';
		
		//$postDepositA->save();
		$postDepositAP->save();
		$postLog->save();
		$postLog1->save();
		//echo $id;
		echo Yii::t('agent','agent.subcompanylist.depositcomplete');
	}
	
	function withdrawCreditMarket()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		$id=substr($_POST['AccountID'], 0,8);
		
		$command = $connection->createCommand("CALL spCheckOnliveStatusGamesByPlayer('" . $_POST["AccountID"] . "',@po_result);");
		$command->execute();
		$command = $connection->createCommand("SELECT @po_result as poResult");
		$rd=$command->queryRow();
		//Check if Player is online in the Lobby
		$this->onlinePlayer=new RedisManager();
		$checkLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['AccountID']);
			
		if ($checkLobby==0)//if not online
		{
			//Check Player Balance is Stock in a Lobby
			$checkBalanceIfLock = new RedisLobbyManager();
			$result = $checkBalanceIfLock->withdrawFromCasinoLobby($_POST['AccountID']);
		
			if ($result['errorCode'] != 1) // no error
			{
		
				// process transfer withdraw ....
				$id=substr($_POST['AccountID'], 0,8);
		
				$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
				$rd1=$command1->queryRow();
					
				//update agent player balance
				$balance= $rd1['balance'];
				$credit= $rd1['credit'];
				$currencyAP= $rd1['currency_id'];
				$newBalance= $balance - $_POST['amountWithdraw'];
					
				//update upper agent  balance
				$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
				$rd=$command->queryRow();
				$agentBalance=$rd['balance'];
				$agentCredit=$rd['balance'];
				$newAgentBalance=$agentBalance + $_POST['amountWithdraw'];
					
				$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
				$rd2=$command2->queryRow();
					
				if  ($_POST['amountWithdraw'] > $rd2['winloss']){
					echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
					exit;
				}
					
				TableAgentPlayer::model()->updateAll(array('balance'=>$newBalance),'account_id="'.$_POST['AccountID'].'"');
				TableAgent::model()->updateAll(array('balance'=>$newAgentBalance),'account_id="'.$id.'"');
					
				//Save to Agent and agent player transaction history
				$postDepositAP =new AgentPlayerSaveTransactionHistory;
				//$postDepositA=new AgentSaveTransactionHistory;
				$postLog=new TableLog;
				$postLog1=new TableLog;
					
				$account_id=Yii::app()->session['account_id'];
		
				$transNum= TransactionNumber::generateTransactionNumber("MW");
				//write transaction history
				$postDepositAP->transaction_number=$transNum;
				$postDepositAP->agent_account_id=$_POST['AccountID'];
				$postDepositAP->currency_id=$currencyAP;
				$postDepositAP->trans_type_id=4;
				$postDepositAP->amount= $_POST['amountWithdraw'];
				$postDepositAP->market_type= 'Credit Market';
				$postDepositAP->balance_before=$balance;
				$postDepositAP->balance_after=$newBalance;
				$postDepositAP->credit_before=$credit;
				$postDepositAP->credit_after=$credit;
				$postDepositAP->upper_agent_balance_before=$agentBalance;
				$postDepositAP->upper_agent_balance_after=$newAgentBalance;
				$postDepositAP->upper_agent_credit_before=$agentCredit;
				$postDepositAP->upper_agent_credit_after=$agentCredit;
				$postDepositAP->transaction_date=$dateTime;
				$postDepositAP->operator_id=$account_id;
				//write log for the agent player
				$postLog->operated_by=$account_id;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_POST['AccountID'];
				$postLog->operated_level='Agent Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=2;
				$postLog->log_details='<b>Credit Market : Agent Player <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Withdraw:<label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label></b>';
				//write log for the agent
				$postLog1->operated_by=$account_id;
				$postLog1->operated_by_level=$level;
				$postLog1->operated=$id;
				$postLog1->operated_level='Agent';
				$postLog1->operation_time=$dateTime;
				$postLog1->log_type_id=2;
				$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Recieved:<label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label> from Credit Market: '.$_POST['AccountID'].'</b>';
					
				//$postDepositA->save();
				$postDepositAP->save();
				$postLog->save();
				$postLog1->save();
				echo Yii::t('agent','agent.subcompanylist.withdrawcomplete');
			}
			//if there's an error during lobby withdraw
			else if ($result['errorCode'] == 1 && $result['casino'] === 'HTV') // error
			{
				echo 'Cannot withdraw from HTV please try again later.';
			}
			else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN') // error
			{
				echo 'Cannot withdraw from SAVAN please try again later.';
			}
		}
		else //if the player cannot process withdraw
		{
			echo 'Agent Player not successfully logout in the lobby. Please try again!';
		}
	}
	
	function depositCashMarket()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		$id=substr($_POST['AccountID'], 0,8);
			
		$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
		$rd1=$command1->queryRow();
		//update agent player balance
		$balance= $rd1['balance'];
		$credit= $rd1['credit'];
		$currencyAP= $rd1['currency_id'];
		$newCredit= $credit - $_POST['amountDeposit'];
	
		//update upper agent  balance
		$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
		$rd=$command->queryRow();
		$agentBalance=$rd['balance'];
		$agentCredit=$rd['credit'];
		$newAgentCredit=$agentCredit + $_POST['amountDeposit'];
	
		$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
		$rd2=$command2->queryRow();
	
		$winloss=(($rd2['winloss'])*-1);
		if  ($_POST['amountDeposit'] > $winloss){
			echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
			exit;
		}
	
		if  ($_POST['amountDeposit'] > $agentCredit){
			echo Yii::t('agent','agent.subcompanylist.deposithigher');
			exit;
		}
	
		TableAgentPlayer::model()->updateAll(array('credit'=>$newCredit),'account_id="'.$_POST['AccountID'].'"');
		TableAgent::model()->updateAll(array('credit'=>$newAgentCredit),'account_id="'.$id.'"');
	
		//Save to Agent and agent player transaction history
		$postDepositAP =new AgentPlayerSaveTransactionHistory;
		//$postDepositA=new AgentSaveTransactionHistory;
		$postLog=new TableLog;
		$postLog1=new TableLog;
	
		$account_id=Yii::app()->session['account_id'];
	
		$transNum= TransactionNumber::generateTransactionNumber("MD");
	
		$postDepositAP->transaction_number=$transNum;
		$postDepositAP->agent_account_id=$_POST['AccountID'];
		$postDepositAP->currency_id=$currencyAP;
		$postDepositAP->trans_type_id=2;
		$postDepositAP->amount= $_POST['amountDeposit'];
		$postDepositAP->market_type= 'Cash Market';
		$postDepositAP->balance_before=$balance;
		$postDepositAP->balance_after=$balance;
		$postDepositAP->credit_before=$credit;
		$postDepositAP->credit_after=$newCredit;
		$postDepositAP->upper_agent_balance_before=$agentBalance;
		$postDepositAP->upper_agent_balance_after=$agentBalance;
		$postDepositAP->upper_agent_credit_before=$agentCredit;
		$postDepositAP->upper_agent_credit_after=$newAgentCredit;
		$postDepositAP->transaction_date=$dateTime;
		$postDepositAP->operator_id=$account_id;
			
		$postLog->operated_by=$account_id;
		$postLog->operated_by_level=$level;
		$postLog->operated=$_POST['AccountID'];
		$postLog->operated_level='Agent Player';
		$postLog->operation_time=$dateTime;
		$postLog->log_type_id=3;
		$postLog->log_details='<b>Cash Market : Agent Player <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Deposit:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label></b>';
	
		$postLog1->operated_by=$account_id;
		$postLog1->operated_by_level=$level;
		$postLog1->operated=$id;
		$postLog1->operated_level='Agent';
		$postLog1->operation_time=$dateTime;
		$postLog1->log_type_id=3;
		$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Send:<label style=\"color:red\">'.number_format($_POST['amountDeposit'],2,'.',',').'</label> to Cash Market '.$_POST['AccountID'].'</b>';
	
		//$postDepositA->save();
		$postDepositAP->save();
		$postLog->save();
		$postLog1->save();
		//echo $id;
		echo Yii::t('agent','agent.subcompanylist.depositcomplete');
	}
	
	function withdrawCashMarket()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$cashierAccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		$id=substr($_POST['AccountID'], 0,8);
	
		$command = $connection->createCommand("CALL spCheckOnliveStatusGamesByPlayer('" . $_POST["AccountID"] . "',@po_result);");
		$command->execute();
		$command = $connection->createCommand("SELECT @po_result as poResult");
		$rd=$command->queryRow();
		//Check if Player is online in the Lobby
		$this->onlinePlayer=new RedisManager();
		$checkLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['AccountID']);
			
		if ($checkLobby==0)//if not online
		{
			//Check Player Balance is Stock in a Lobby
			$checkBalanceIfLock = new RedisLobbyManager();
			$result = $checkBalanceIfLock->withdrawFromCasinoLobby($_POST['AccountID']);
	
			if ($result['errorCode'] != 1) // no error
			{
	
				// process transfer withdraw ....
				$id=substr($_POST['AccountID'], 0,8);
	
				$command1= $connection->createCommand("SELECT balance,currency_id,credit from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
				$rd1=$command1->queryRow();
					
				//update agent player balance
				$balance= $rd1['balance'];
				$credit= $rd1['credit'];
				$currencyAP= $rd1['currency_id'];
				$newCredit= $credit + $_POST['amountWithdraw'];
					
				//update upper agent  balance
				$command = $connection->createCommand("SELECT balance,credit from tbl_agent  WHERE account_id='" . $id . "'");
				$rd=$command->queryRow();
				$agentBalance=$rd['balance'];
				$agentCredit=$rd['credit'];
				$newAgentCredit=$agentCredit - $_POST['amountWithdraw'];
					
				$command2= $connection->createCommand("SELECT (balance - credit) as winloss from tbl_agent_player  WHERE account_id='" . $_POST['AccountID'] . "'");
				$rd2=$command2->queryRow();
					
				if  ($_POST['amountWithdraw'] > $rd2['winloss']){
					echo Yii::t('agent','agent.subcompanylist.depositamounthigher');
					exit;
				}
					
				TableAgentPlayer::model()->updateAll(array('credit'=>$newCredit),'account_id="'.$_POST['AccountID'].'"');
				TableAgent::model()->updateAll(array('credit'=>$newAgentCredit),'account_id="'.$id.'"');
					
				//Save to Agent and agent player transaction history
				$postDepositAP =new AgentPlayerSaveTransactionHistory;
				//$postDepositA=new AgentSaveTransactionHistory;
				$postLog=new TableLog;
				$postLog1=new TableLog;
					
				$account_id=Yii::app()->session['account_id'];
	
				$transNum= TransactionNumber::generateTransactionNumber("MW");
				//write transaction history
				$postDepositAP->transaction_number=$transNum;
				$postDepositAP->agent_account_id=$_POST['AccountID'];
				$postDepositAP->currency_id=$currencyAP;
				$postDepositAP->trans_type_id=4;
				$postDepositAP->amount= $_POST['amountWithdraw'];
				$postDepositAP->market_type= 'Cash Market';
				$postDepositAP->balance_before=$balance;
				$postDepositAP->balance_after=$balance;
				$postDepositAP->credit_before=$credit;
				$postDepositAP->credit_after=$newCredit;
				$postDepositAP->upper_agent_balance_before=$agentBalance;
				$postDepositAP->upper_agent_balance_after=$agentBalance;
				$postDepositAP->upper_agent_credit_before=$agentCredit;
				$postDepositAP->upper_agent_credit_after=$newAgentCredit;
				$postDepositAP->transaction_date=$dateTime;
				$postDepositAP->operator_id=$account_id;
				//write log for the agent player
				$postLog->operated_by=$account_id;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_POST['AccountID'];
				$postLog->operated_level='Agent Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=2;
				$postLog->log_details='<b>Cash Market : Agent Player <label style=\"color:#7A5C00\">'.$_POST['AccountID'].'</label> Withdraw:<label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label></b>';
				//write log for the agent
				$postLog1->operated_by=$account_id;
				$postLog1->operated_by_level=$level;
				$postLog1->operated=$id;
				$postLog1->operated_level='Agent';
				$postLog1->operation_time=$dateTime;
				$postLog1->log_type_id=2;
				$postLog1->log_details='<b><label style=\"color:#7A5C00\">'.$id.'</label> Recieved:<label style=\"color:red\">'.number_format($_POST['amountWithdraw'],2,'.',',').'</label> from Cash Market '.$_POST['AccountID'].'</b>';
					
				//$postDepositA->save();
				$postDepositAP->save();
				$postLog->save();
				$postLog1->save();
				echo Yii::t('agent','agent.subcompanylist.withdrawcomplete');
			}
			//if there's an error during lobby withdraw
			else if ($result['errorCode'] == 1 && $result['casino'] === 'HTV') // error
			{
				echo 'Cannot withdraw from HTV please try again later.';
			}
			else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN') // error
			{
				echo 'Cannot withdraw from SAVAN please try again later.';
			}
		}
		else //if the player cannot process withdraw
		{
			echo 'Agent Player not successfully logout in the lobby. Please try again!';
		}
		
	}
	
	public function makeProcessWithdrawalRequest()
	{
		$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["AccountID"]."' and deposit_withdrawal<>0");
		$rd=$command->queryRow();
		if ($rd['deposit_withdrawal'] == 1){
			if ($rd['casino_id']==1 || $rd['casino_id']==2){
				$checkBalanceIfLock = new RedisLobbyManager();
			 	$result=$checkBalanceIfLock->withdrawFromCasinoLobby($_POST['AccountID']);
			 	//if there's a problem in HTV and SAVAN
			 	if ($result['errorCode'] == 1 && $result['casino'] === 'HTV') // error
			 	{
			 		//to display message that there is an error during lobby withdraw ih HTV
			 		echo 'h';
			 	}
			 	else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN') // error
			 	{
			 		//to display message that there is an error during lobby withdraw ih SAVAN
			 		echo 's';
			 	}
			}
			else // balance stock on Costavegas & VirtuaVegas
			{
				echo '1';
			}
		}
			
			
	}
	
	public function getUpperAgentBalance($upperAccountId)
	{
		$connection = Yii::app()->db_cv999_fd_master;
	
		$command = $connection->createCommand("Select balance from tbl_agent WHERE account_id='".$upperAccountId."'" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getAgentMemberCreditAndBalance($upperAccountId)
	{
		$connection = Yii::app()->db_cv999_fd_master;
	
		$command = $connection->createCommand("Select account_id,credit,balance,status_id from tbl_agent_player WHERE agent_account_id='".$upperAccountId."' ORDER BY balance-credit DESC" );
		$rows = $command->query();
		return $rows;
	}
	
	public function makeTransferAll()
	{
		$amount_to_withdraw	=	0;
		$amount_to_deposit	=	0;
		$balanceDueToForm=0;
		$upperAccountId=$_POST['upperAccountId'];
		
		$cp=new AgentPlayerTransfer();
		$getCreditAndBalance=$cp->getAgentMemberCreditAndBalance($upperAccountId);

		foreach ($getCreditAndBalance as $row){
			if ($row['balance']-$row['credit'] > 0){
				$amount_to_withdraw	= $amount_to_withdraw	+	($row['balance']-$row['credit']);
			}else{
				$amount_to_deposit	= $amount_to_deposit	+	($row['balance']-$row['credit']);
			}
			
		}
		
		$getUpperAgentBalance=$cp->getUpperAgentBalance($upperAccountId);
		foreach ($getUpperAgentBalance as $row){
			$upperAgentBalance=$row['balance'];
		}
		$totalAmount	=	$amount_to_withdraw	+	$upperAgentBalance;
		
// 		if (($amount_to_deposit*-1)		>	$totalAmount){
			
// 			echo 'insufficient balance';
			
// 		}else{
			
			//DO TRANSFER OPERATION
			$apt=new AgentPlayerTransfer();
			$tranferBalance=$apt->getAgentMemberCreditAndBalance($upperAccountId);

			foreach ($tranferBalance as $row){
				
				$this->onlinePlayer=new RedisManager();
				$checkPlayerLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($row['account_id']);
					
				$balanceDueToForm	=	$row['balance']-$row['credit'];
				if ($checkPlayerLobby==0){
					if ($balanceDueToForm > 0){
						
						//get upper agent balance
						$getUpperAgentBalance_w=$apt->getUpperAgentBalance($upperAccountId);
						foreach ($getUpperAgentBalance_w as $row_upper){
							$upperAgentBalance_w=$row_upper['balance'];
						}
						
						$updatedBalance=$upperAgentBalance_w + $balanceDueToForm;
						//update upper agent balance
						TableAgent::model()->updateAll(array('balance'=>$upperAgentBalance_w + $balanceDueToForm),'account_id="'.$upperAccountId.'"');
						//update agent balance
						TableAgentPlayer::model()->updateAll(array('balance'=>$row['balance'] - $balanceDueToForm),'account_id="'.$row['account_id'].'"');
						echo '<div>Withdraw balance from <label style="color:blue;">'.$row['account_id'].'</label>: '.$balanceDueToForm.' <label style="color:green;">Succcessful...</label></div>';
						echo '<div>Upper Agent <label style="color:blue;">'.$upperAccountId.'</label> new balance : '.$updatedBalance.'</div>';
					
					}elseif ($balanceDueToForm < 0){
						
						//get upper agent balance
						$getUpperAgentBalance_d=$apt->getUpperAgentBalance($upperAccountId);
						foreach ($getUpperAgentBalance_d as $row_upper){
							$upperAgentBalance_d=$row_upper['balance'];
						}
						
						$updatedBalance=$upperAgentBalance_d + $balanceDueToForm;
						
						if ($upperAgentBalance_d >= $balanceDueToForm*-1){
						//update upper agent balance
						TableAgent::model()->updateAll(array('balance'=>$upperAgentBalance_d + $balanceDueToForm),'account_id="'.$upperAccountId.'"');
						//update agent balance
						TableAgentPlayer::model()->updateAll(array('balance'=>$row['balance'] - $balanceDueToForm),'account_id="'.$row['account_id'].'"');
						
						echo '<div>Deposit balance to <label style="color:blue;">'.$row['account_id'].'</label>: '.$balanceDueToForm.' <label style="color:green;">Succcessful...</label></div>';
						echo '<div>Upper Agent <label style="color:blue;">'.$upperAccountId.'</label> new balance : '.$updatedBalance.'</div>';
						
						}else{
							echo '<div><label style="color:red">Other players faild to transfer due to insufficient balance.</label></div>';
							//exit();
						}
					}
					
				}
			}
			echo '<div><label style="color:green;">Transfer process completed.</label></div>';
// 		}
	}
}