<?php

class CashPlayerWinLossV1
{

	public function getPlayerWinLoss($orderField, $sortType, $startIndex, $limit)
	{
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and CH.account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and game_id='".$_GET['game']."' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND ''".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and currency_name='".$_GET['currencyId']."' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."' and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and currency_name='".$_GET['currencyId']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountPlayerWinLoss()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_costavegas_casino_history as CH where CH.account_type='c' ".$andAccountId." and  CH.bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by CH.currency_name,CH.account_id ) as counter"  );
			}else{
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_costavegas_casino_history as CH where CH.account_type='c' ".$andAccountId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and  CH.bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency_name,CH.account_id ) as counter"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_costavegas_casino_history as CH where CH.account_type='c' ".$andAccountId." and CH.game_id='".$_GET['game']."' and CH.bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency_name,CH.account_id ) as counter"   );
			}else{
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_costavegas_casino_history as CH where CH.account_type='c' ".$andAccountId." and CH.game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and CH.bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency_name,CH.account_id ) as counter"   );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_costavegas_casino_history as CH where CH.account_type='c' ".$andAccountId." and CH.currency_name='".$_GET['currencyId']."' and CH.bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency_name,CH.account_id ) as counter"   );
		}
		else{
			$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_costavegas_casino_history as CH where CH.account_type='c' ".$andAccountId." and  CH.currency_name='".$_GET['currencyId']."' and CH.game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency_name,CH.account_id ) as counter"   );
		}
		$rows = $command->query();
		return $rows;
	}
	
	
	public function getPlayerWinLossCostaTotal()
	{
	$connection = Yii::app()->db_cv999_fd_master;
	if ($_GET['accountId']=="All"){
		$andAccountId='';
	}else{
		$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
	}
	
	if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT TS.currency_name as currency_name,TS.rate as rate,SUM(TS.amount_wager) as amount_wager,SUM(TS.win_los) as win_los,SUM(TS.commission) as commission,concat('-',SUM(TS.amount_tips)) as amount_tips,SUM(TS.total_win_los) as total_win_los, 
						IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2)) AS vig_winloss,SUM(TS.amount_tips) AS vig_tips,(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)) AS vig_total,IF(SUM(TS.total_win_los)>0,(SUM(TS.total_win_los)-(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1,(SUM(TS.total_win_los)+(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1) AS p_l  FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency_name) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,sum(amount_wagers) as amount_wager,SUM(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,sum(amount_tips) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,account_id) as TS group by TS.currency_name"  );
			}else{
				$command = $connection->createCommand("SELECT TS.currency_name as currency_name,TS.rate as rate,SUM(TS.amount_wager) as amount_wager,SUM(TS.win_los) as win_los,SUM(TS.commission) as commission,concat('-',SUM(TS.amount_tips)) as amount_tips,SUM(TS.total_win_los) as total_win_los,
						IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2)) AS vig_winloss,SUM(TS.amount_tips) AS vig_tips,(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)) AS vig_total,IF(SUM(TS.total_win_los)>0,(SUM(TS.total_win_los)-(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1,(SUM(TS.total_win_los)+(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1) AS p_l  FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency_name) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,sum(amount_wagers) as amount_wager,SUM(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,sum(amount_tips) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,account_id) as TS group by TS.currency_name"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT TS.currency_name as currency_name,TS.rate as rate,SUM(TS.amount_wager) as amount_wager,SUM(TS.win_los) as win_los,SUM(TS.commission) as commission,concat('-',SUM(TS.amount_tips)) as amount_tips,SUM(TS.total_win_los) as total_win_los,
						IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2)) AS vig_winloss,SUM(TS.amount_tips) AS vig_tips,(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)) AS vig_total,IF(SUM(TS.total_win_los)>0,(SUM(TS.total_win_los)-(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1,(SUM(TS.total_win_los)+(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1) AS p_l  FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency_name) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,sum(amount_wagers) as amount_wager,SUM(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,sum(amount_tips) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,account_id) as TS group by TS.currency_name"  );
			}else{
				$command = $connection->createCommand("SELECT TS.currency_name as currency_name,TS.rate as rate,SUM(TS.amount_wager) as amount_wager,SUM(TS.win_los) as win_los,SUM(TS.commission) as commission,concat('-',SUM(TS.amount_tips)) as amount_tips,SUM(TS.total_win_los) as total_win_los,
						IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2)) AS vig_winloss,SUM(TS.amount_tips) AS vig_tips,(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)) AS vig_total,IF(SUM(TS.total_win_los)>0,(SUM(TS.total_win_los)-(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1,(SUM(TS.total_win_los)+(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1) AS p_l  FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency_name) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,sum(amount_wagers) as amount_wager,SUM(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,sum(amount_tips) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,account_id) as TS group by TS.currency_name"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("SELECT TS.currency_name as currency_name,TS.rate as rate,SUM(TS.amount_wager) as amount_wager,SUM(TS.win_los) as win_los,SUM(TS.commission) as commission,concat('-',SUM(TS.amount_tips)) as amount_tips,SUM(TS.total_win_los) as total_win_los,
					IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2)) AS vig_winloss,SUM(TS.amount_tips) AS vig_tips,(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)) AS vig_total,IF(SUM(TS.total_win_los)>0,(SUM(TS.total_win_los)-(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1,(SUM(TS.total_win_los)+(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1) AS p_l  FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency_name) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,sum(amount_wagers) as amount_wager,SUM(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,sum(amount_tips) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,account_id) as TS group by TS.currency_name"  );
		}
		else{
			$command = $connection->createCommand("SELECT TS.currency_name as currency_name,TS.rate as rate,SUM(TS.amount_wager) as amount_wager,SUM(TS.win_los) as win_los,SUM(TS.commission) as commission,concat('-',SUM(TS.amount_tips)) as amount_tips,SUM(TS.total_win_los) as total_win_los,
					IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2)) AS vig_winloss,SUM(TS.amount_tips) AS vig_tips,(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)) AS vig_total,IF(SUM(TS.total_win_los)>0,(SUM(TS.total_win_los)-(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1,(SUM(TS.total_win_los)+(IF(SUM(TS.win_los)>0,0,ROUND(SUM(TS.win_los)*-('".$_GET['vig']."'/100),2))+SUM(TS.amount_tips)))*-1) AS p_l  FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency_name) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,sum(amount_wagers) as amount_wager,SUM(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,sum(amount_tips) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."' and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency_name,account_id) as TS group by TS.currency_name"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	
	public function getPlayerWinLossHTV($orderField, $sortType, $startIndex, $limit)
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId=''; }
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}else{
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}else{
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountPlayerWinLossHTV()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_htv_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}else{
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_htv_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_htv_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.game_type='".$_GET['game']."' and CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}else{
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_htv_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_htv_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.currency='".$_GET['currencyId']."' and CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_htv_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.currency='".$_GET['currencyId']."' and CH.game_type='".$_GET['game']."' and CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayerWinLossHTVTotal()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id) AS TT GROUP BY TT.currency"  );
			}else{
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id) AS TT GROUP BY TT.currency"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id) AS TT GROUP BY TT.currency"  );
			}else{
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id) AS TT GROUP BY TT.currency"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id) AS TT GROUP BY TT.currency"  );
		}
		else{
			$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id) AS TT GROUP BY TT.currency"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	// for savan vegas
	public function getPlayerWinLossSAVAN($orderField, $sortType, $startIndex, $limit)
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and CH.account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountPlayerWinLossSAVAN()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_savan_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}else{
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_savan_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_savan_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.game_type='".$_GET['game']."' and CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}else{
				$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_savan_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_savan_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.currency='".$_GET['currencyId']."' and CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(*) FROM (Select COUNT(0) from tbl_savan_casino_history as CH where CH.account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and CH.currency='".$_GET['currencyId']."' and CH.game_type='".$_GET['game']."' and CH.bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by CH.currency,CH.account_id ) as counter"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayerWinLossSAVANTotal()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id ) AS TT GROUP BY TT.currency"  );
			}else{
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id ) AS TT GROUP BY TT.currency"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id ) AS TT GROUP BY TT.currency"  );
			}else{
				$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id ) AS TT GROUP BY TT.currency"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id ) AS TT GROUP BY TT.currency"  );
		}
		else{
			$command = $connection->createCommand("SELECT TT.account_id,TT.rate,TT.account_name,SUM(TT.bonus) AS bonus,TT.currency,SUM(TT.bet_count) AS bet_count,SUM(TT.total_stake) AS total_stake,SUM(TT.amount_wager) AS amount_wager,SUM(TT.win_los) AS win_los,SUM(TT.commission) AS commission,SUM(TT.amount_tips) AS amount_tips,SUM(TT.total_win_los) AS total_win_los,SUM(TT.p_l) AS p_l FROM (Select account_id,(SELECT TC.exchange_rate FROM tbl_currency AS TC WHERE TC.currency_name=CH.currency) AS rate,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,0 AS bonus,currency,count(bet_amount) as bet_count,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by account_id ) AS TT GROUP BY TT.currency"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	
	public function getExportPlayerWinLoss()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and CH.account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
	
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id "  );
					
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id "  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and game_id='".$_GET['game']."' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND ''".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id "  );
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id "  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and currency_name='".$_GET['currencyId']."' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id "  );
		}
		else{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) as avg_bet,sum(amount_wagers) as amount_wager,sum(amount_settlements - amount_wagers) as win_los,sum(commission) as commission,concat('-',sum(amount_tips)) as amount_tips,IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) as total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 as p_l from tbl_costavegas_casino_history as CH where account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."' and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  group by currency_name,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_date FROM tbl_costavegas_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,currency_name,AVG(amount_wagers) AS avg_bet,SUM(amount_wagers) AS amount_wager,SUM(amount_settlements - amount_wagers) AS win_los,SUM(CH.commission) AS commission,CONCAT('-', SUM(amount_tips)) AS amount_tips,IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips)) AS total_win_los,(SELECT casino_balance FROM `tbl_costavegas_casino_history` AS CH1 WHERE CH1.id = MAX(CH.id)) AS current_balance,( IF(SUM(amount_settlements - amount_wagers) > 0,(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) + SUM(amount_tips),(SUM(amount_settlements - amount_wagers) + SUM(CH.commission)) - SUM(amount_tips))) * - 1 AS p_l FROM tbl_costavegas_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type = 'c' and currency_name='".$_GET['currencyId']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." AND bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency_name,CH.account_id "  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getExportPlayerWinLossHTV()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
	
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
	
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id "  );
			}else{
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id "  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id "  );
			}else{
				$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id "  );
			}
	
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id "  );
		}
		else{
			$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_htv_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_htv_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_htv_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id "  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getExportPlayerWinLossSAVAN()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and CH.account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
	
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
	
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id "  );
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id "  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id "  );
			}else{
				//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
				$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id "  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id "  );
		}
		else{
			//$command = $connection->createCommand("Select account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,count(bet_amount) as bet_count,AVG(available_bet) as avg_bet,sum(bet_amount) as total_stake,sum(available_bet) as amount_wager,sum(win_loss) as win_los,sum(commission) as commission,sum(0) as amount_tips,(sum(win_loss)+sum(commission)+0) as total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((sum(win_loss)+sum(commission)+0))*-1 as p_l from tbl_savan_casino_history as CH where account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' group by currency,account_id order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT CH.account_id,CP.account_name,(SELECT bet_time FROM tbl_savan_casino_history AS CH1 WHERE CH1.id = MAX(CH.id)) AS last_bet_date,0 AS bonus,currency,COUNT(bet_amount) AS bet_count,AVG(available_bet) AS avg_bet,SUM(bet_amount) AS total_stake,SUM(available_bet) AS amount_wager,SUM(win_loss) AS win_los,SUM(CH.commission) AS commission,SUM(0) AS amount_tips,(SUM(win_loss)+SUM(CH.commission)+0) AS total_win_los,(SELECT balance_after FROM `tbl_savan_casino_history` AS CH1 WHERE CH1.id=MAX(CH.id)) AS current_balance,((SUM(win_loss)+SUM(CH.commission)+0))*-1 AS p_l FROM tbl_savan_casino_history AS CH  LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE account_type='c' ".$andAccountId." ".$andTableId." ".$andGameId." ".$andShoesId." and currency='".$_GET['currencyId']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' AND bet_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY currency,account_id "  );
		}
		$rows = $command->query();
		return $rows;
	}
	
}