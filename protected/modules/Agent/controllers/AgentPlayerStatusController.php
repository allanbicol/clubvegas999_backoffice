<?php
/**
 * @todo AgentPlayerStatusController
 * @copyright CE
 * @author Allan 
 * @since 2012-05-11
 */
class AgentPlayerStatusController extends MyController
{
	public function actionAgentPlayerStatusProcess()
	{
		if (Yii::app()->user->checkAccess('agent.writeAgentStatus') || User::getUserType() === Constants::ROLE_AGENT)
		{
			$cps = new AgentPlayerStatus();
			$cps->changeAgentPlayerStatus();
		}
	}
}