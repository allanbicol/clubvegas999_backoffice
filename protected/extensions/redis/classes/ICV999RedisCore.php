<?php
/**
 * @TODO store all variables CV999
 * @copyright Creative Entertainment Ltd.
 * @author sitthykun
 * @since 2012-04-04
 */



interface ICV999RedisCore {
	// declare abstract
	public function equalTo($value);
	public function getCoreKey();
	public function isExisting($playerTypeID = NULL, $playerName = NULL, $casinoID = NULL);
	// public function isOnLobby($playerTypeID = NULL, $playerName = NULL);
	public function length();
}

?>