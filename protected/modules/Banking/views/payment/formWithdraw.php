
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-timepicker-addon.js"></script>
	<table class="bank-withdraw">
		<thead>
			<tr>
				<td style="width: 250px;">
					
				</td>
				<td style="width: 300px;">
					
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<label><?php echo Yii::t('banking','banking.payment.transaction_id');?>: <b><?php echo $withdraw[0]['transaction_number'];?></b></label>
				</td>
				<td>
					<label><?php echo Yii::t('banking','banking.payment.date');?>: <?php echo $withdraw[0]['transaction_date'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label><?php echo Yii::t('banking','banking.payment.account_id_small');?>:</label>			
				</td>
				<td>
					<label><b><?php echo $withdraw[0]['cash_player_account_id'];?></b></label>
				</td>
			</tr>
			<?php if(substr($withdraw[0]['transaction_number'], 0, 2) != 'MW'){?>
			<tr>
				<td>
					<label><?php echo Yii::t('banking','banking.payment.player_balance');?>:</label>		
				</td>
				<td>
					<label id="lblBalance"><?php echo $balance[0]['balance'];?></label>
				</td>
			</tr>
			
			<tr>
				<td>
					<label><?php echo Yii::t('banking','banking.payment.player_bonus');?>:</label>
				</td>
				<td>
					<label><?php echo $balance[0]['bonus'];?></label>
				</td>
			</tr>
			<?php }?>
			<tr>
				<td>
					<label><?php echo Yii::t('banking','banking.payment.currency_small');?>:</label>
				</td>
				<td>
					<label><?php echo $balance[0]['currency_name'];?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label><?php echo Yii::t('banking','banking.payment.request_amount');?>:</label>
				</td>
				<td>
					<label id="lblRequestAmount"><?php echo $withdraw[0]['request_amount'];?></label>
				</td>
			</tr>
			
			<?php if(substr($withdraw[0]['transaction_number'], 0, 2) != 'MW'){?>
				<tr>
					<td>
						<label><?php echo Yii::t('banking','banking.payment.request_currency');?>:</label>
					</td>
					<td>
						<label><?php echo $withdraw[0]['currency_name'];?></label>
					</td>
				</tr>
			<?php }?>
				
				<tr>
					<td>
						<label><?php echo Yii::t('banking','banking.payment.transaction_via');?>:</label>
					</td>
					<td>
					<?php 
						if($withdraw[0]['transaction_status_id']!=6){
							echo CHtml::dropDownList('ddlBankList', $transaction_item ,$bankList, array('empty' => '(Select bank)'));
						}else{
							echo $transaction_item_name;
						}
					?>
					</td>
				</tr>
			<?php if(substr($withdraw[0]['transaction_number'], 0, 2) != 'MW'){?>
					<?php if($withdraw[0]['transaction_via']==1){?>
					<tr id="trBankAccountNumber">
						<td>
							<label><?php echo Yii::t('banking','banking.payment.bank_account_number');?>:</label>
						</td>
						<td>
							<label id="lblBankAccountNumber"><?php echo $withdraw[0]['bank_account_number'];?></label>
						</td>
					</tr>
					<tr id="trBankAccountName">
						<td>
							<label><?php echo Yii::t('banking','banking.payment.bank_account_name');?>:</label>
						</td>
						<td>
							<label id="lblBankAccountName"><?php echo $withdraw[0]['bank_account_name'];?></label>
						</td>
					</tr>
					<tr id="trBankName">
						<td>
							<label><?php echo Yii::t('banking','banking.payment.bank_name');?>:</label>
						</td>
						<td>
							<label id="lblBankName"><?php echo $withdraw[0]['bank_name'];?></label>
						</td>
					</tr>
					<tr id="trBankBranch">
						<td>
							<label><?php echo Yii::t('banking','banking.payment.bank_branch');?>:</label>
						</td>
						<td>
							<label id="lblBankBranch"><?php echo $withdraw[0]['bank_branch'];?></label>
						</td>
					</tr>
					
					<?php }else{?>
					<tr id="trEmail">
						<td>
							<label><?php echo Yii::t('banking','banking.payment.email_small');?>:</label>
						</td>
						<td>
							<label id="lblEmail"><?php echo $withdraw[0]['email'];?></label>
						</td>
					</tr>
					<?php }?>
				<?php }?>
			<tr>
				<td><?php echo Yii::t('banking','banking.payment.process_date');?>:</td>
				<td>
					<?php
					//$date = ($withdraw[0]['transaction_status_id']==6) ? $finish_datetime : date('Y-m-d H:m:s', time());
					$date = ($withdraw[0]['transaction_status_id']==6) ? $finish_datetime : '';
					$s_disabled = ($withdraw[0]['transaction_status_id']==6) ? true : false;
					$disabled = ($withdraw[0]['transaction_status_id']==6) ? 'disabled' : '';
					echo CHtml::textField('dtProcessDate', $date,
							array('id'=>'dtProcessDate','maxlength'=>12, 'disabled'=>$disabled));?>
					<font color="red">*</font></br>
					<?php 
					if($withdraw[0]['transaction_status_id']!=6){
					?>
					<span><font color="red"><?php echo Yii::t('banking','banking.payment.process_date_note');?></font></span>
					<?php }?>
				</td>
			</tr>
			<tr>
				<td><?php echo Yii::t('banking','banking.payment.process');?>:</td>
				<td>
					<select name="cmbProcess" id="cmbProcess" <?php if($withdraw[0]['transaction_status_id']!=6){if(!Yii::app()->user->checkAccess('banking.writePayment')){echo 'disabled';}}else{echo 'disabled';}?>>
						<option value="1"><?php echo Yii::t('banking','banking.payment.accept');?></option>
						<option value="7" <?php if($withdraw[0]['transaction_status_id']==7){echo 'selected';}?>><?php echo Yii::t('banking','banking.payment.deny');?></option>
					</select>
				</td>
			</tr>

			<tr>
				<td align="center" colspan="2" style="padding-top: 15px;">					
						<input class="btn red" type="submit" value="<?php echo Yii::t('banking','banking.payment.confirm');?>" name="btnConfirm" id="btnConfirm" <?php if(Yii::app()->user->checkAccess('banking.writePayment')){if($withdraw[0]['transaction_status_id']==6){echo 'disabled';}}else{echo 'disabled';}?> onClick="confirmWithdraw();" >					
				</td>
			</tr>
		</tbody>
	</table>
	
<script type="text/javascript">
	$(document).ready(function(){
		$('#ui-dialog-title-withdraw-dialog').html(getWithdrawTypeByTransNumber('<?php echo $withdraw[0]['transaction_number'];?>'));

//		 jQuery("#dtProcessDate").datepicker({
//		        changeMonth: true,
//		        changeYear: true,
//		        dateFormat: "yy-mm-dd"
//		 });
	
		$('#dtProcessDate').datetimepicker({
			controlType: 'select',
			dateFormat: "yy-mm-dd",
			timeFormat: 'HH:mm:ss'
		});
		 
	});
	/**
	 * @todo confirm withdraw
	 * @author leokarl
	 * @since 2012-12-27
	 */
	function confirmWithdraw()
	{
		if('<?php echo Yii::app()->user->checkAccess('banking.writePayment');?>' == 1)
		{
			if($('#cmbProcess').val() == '1'){
				// select bank for frontend withdraw
				if($('#ddlBankList').val() == ''){
					alert('Warning: Please select bank!');
					$('#ddlBankList').focus();
					return false;
				}
				
				// withdrawal confirmation
				if(confirm('Do you want to accept withdrawal?')){
					if('<?php echo substr($withdraw[0]['transaction_number'], 0, 2)?>' != 'MW'){ // for front-end withdraw
						//exit if withdraw amount is greater that balance - bonus
						if('<?php echo $withdraw_status?>' == 'in_process'){
							if(parseFloat('<?php echo $balance[0]['balance'];?>') < parseFloat('<?php echo $withdraw[0]['request_amount'];?>')){
								
								if(confirm("The player doesn't have enough balance to proceed the withdraw request. Click OK to deny transaction.")){
									//save to database
									saveWithdraw(7);
								}
							}else{
								//save to database
								saveWithdraw(6);
							}
						}else{
							saveWithdraw(6);
						}
						
					}else{ // for manual withdraw
						//save to database
						saveWithdraw(6);
					}
				}
			}else if($('#cmbProcess').val() == '7'){
				if(confirm('Do you want to cancel transaction?')){
					//save to database
					saveWithdraw(7);
				}
			}else{
				alert('Warning: Invalid withdraw process!');
				return false;
			}
		}else{
			alert('Warning: You don\'t have permission to confirm withdraw!');
			return false;
		}
		
	}

	/**
	 * @todo save withdraw
	 * @author leokarl
	 * @since 2012-12-27
	 * @param transaction_status_id; 6 = complete or 7 = cancel
	 */
	function saveWithdraw(transaction_status_id){
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/ConfirmWithdraw',
			type: 'POST',
			async: false,
    		data: {'transaction_number': '<?php echo $withdraw[0]['transaction_number'];?>',
        		'account_id': '<?php echo $withdraw[0]['cash_player_account_id'];?>',
        		'transaction_status_id': transaction_status_id,
        		'transaction_item':$('#ddlBankList').val(),
        		'transation_time': $('#dtProcessDate').val(),
        		},
    		success: function(msg) {
	    		if(msg == 'account_inactive'){
		    		alert('Warning: Account is inactive!');
		    		return false;
	    		}else if(msg == 'required_data_not_set'){
		    		alert('Warning: Required data not set!');
		    		return false;
	    		}else if(msg == 'invalid_currency_id'){
		    		alert('Warning: Invalid currency id!');
		    		return false;
	    		}else if(msg == 'no_permission'){
		    		alert('Warning: You don\'t have permission to confirm withdraw!');
		    		return false;
	    		}else if(msg == 'invalid_request_amount'){
		    		alert('Warning: Invalid request amount!');
		    		return false;
	    		}else if(msg == 'transaction_number_not_exist_in_cashplayer_withdraw'){
		    		alert('Warning: Transaction number does not exist in cash player withdraw!');
		    		return false;
	    		}else if(msg == 'invalid_transaction_status_id'){
		    		alert('Warning: Invalid transaction status id!');
		    		return false;
	    		}else if(msg == 'transaction_already_done'){
		    		alert('Warning: Transaction is already completed!');
		    		return false;
	    		}else if(msg == 'transaction_already_cancelled'){
		    		alert('Warning: Transaction is already cancelled!');
		    		return false;
    			}else if(msg =='force_logout_warning'){

	    			// force logout warning message
	    			$('#viewForceLogOut').load('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/LoadForceLogOutDialog&player_id=<?php echo $withdraw[0]['cash_player_account_id'];?>' ,
			    			function(response, status, xhr) {		    	
			    				if (status == "success") {	
				    				
				    				 // @todo process force logout
				    				 // 1. display force logout dialog
				    				 // 2. display count down timer
				    				 		    		  									
					  				$("#forcelogout-dialog").dialog("open"); return false;
					  				
					  			}else{
				  					$('#viewForceLogOut').load('<p style="color:red;">Something wrong during request!!!</p>');
				  					$("#forcelogout-dialog").dialog("open"); return false;
		    		  			}
				  			}
						);
		    	}else if(msg == 'disconnected_to_htv'){
					alert("Can't connet to HatienVegas999. Please try again!");
					return false;
			    }else if(msg == 'disconnected_to_savan'){
					alert("Can't connet to SavanVegas999. Please try again!");
					return false;
			    }else if(msg == 'withdraw_cancelled'){
				    alert('Withdraw cancelled!');
				    $("#withdraw-dialog").dialog("close");
				    qryTransactions(); // reload grid
			    }else if(msg == 'withdraw_pending'){
				    alert('Withdraw pending!');
				    $("#withdraw-dialog").dialog("close");
				    qryTransactions(); // reload grid
				}else if(msg == 'withdraw_done'){
				    alert('Withdraw complete!');
				    $("#withdraw-dialog").dialog("close");
				    qryTransactions(); // reload grid
				}else if(msg == 'select_bank'){
					alert('Warning: Please select bank!');
					$('#ddlBankList').focus();
					return false;
				}else if(msg == 'invalid_bank_id'){
					alert('Warning: Invalid bank id!');
					$('#ddlBankList').focus();
					return false;
				}else if(msg == 'trans_number_exist'){
					alert('Error: Transaction number (FW) is already exist in the transaction history!');
					return false;
				}else if(msg == 'process_date_required'){
					alert('Warning: Process Date is required!');
					$('#dtProcessDate').focus();
					return false;
				}else if(msg == 'invalid_time_format'){
					alert('Warning: Invalid datetime format!');
					$('#dtProcessDate').focus();
					return false;
				}else if(msg == 'cant_set_finish_time_before_transaction_date'){
					alert('Warning: You can\'t set process date before transaction date!');
					$('#dtProcessDate').focus();
					return false;
				}else if(msg == 'cant_set_process_date_as_tomorrow_date'){
					alert('Warning: You can\'t set process date ahead to current date!');
					$('#dtProcessDate').focus();
					return false;
				}
		    	
    			
	    	}
    	});
	}
	</script>
