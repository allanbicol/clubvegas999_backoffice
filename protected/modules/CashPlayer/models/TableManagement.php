<?php
/**
 * @todo bind tbl_table_management to TableManagement CActiveRecord
 * @copyright CE
 * @author Allan
 * @since 2013-01-05
 */
class TableManagement extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_table_management';
	}
}