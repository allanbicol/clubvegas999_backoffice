<?php
class GetPoolLimit
{
	public function getPoolLimitList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM vwGetPoolLimitList WHERE currency_id=". $_GET['currency_id'] ."");
		$rows = $command->query();
		return $rows;		
	}
	public function getCountPoolLimitRecord()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetPoolLimitList WHERE currency_id=". $_GET['currency_id'] ."");
		$rows = $command->query();
		return $rows;
	}
	public function getPoolLimitGameList($currency_id)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM vwGetPoolLimitNewGame WHERE currency_id!=" . $currency_id . "");
		$rows = $command->query();
		return $rows;
	}
}