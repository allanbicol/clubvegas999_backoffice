<html>
<head>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/onlineagentplayermanagement.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/settlement.css"/>

<script type="text/javascript">
var urlJsAssets='<?php echo $this->module->assetsUrl; ?>';
var session_level='<?php echo yii::app()->session['level'];?>';
var account_id='<?php echo yii::app()->session['account_id'];?>';
var dtToday='<?php echo date("Y-m-d");?>';
var listURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement/GetOnlineAgentPlayer';
var playerStatuslistURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/Settlement/PlayerStatusList';
var urlLogOutPlayer='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement/ForcePlayerLogOut';
var urlGetCasinoNameFromRedisByPlayerID='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineAgentPlayerManagement/GetCasinoNameFromRedisByPlayerID';
var writeOnlineUsers = '<?php echo (Yii::app()->user->checkAccess('systemMonitor.writeOnlineUsers'))?>';
var userType='<?php echo User::getUserType()?>';
var kickOffURL = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/Settlement/KickOff';
// json
var kickOffJson = {"1": "Change all accounts to IN-ACTIVE", "2": "Change all accounts to ACTIVE"};

/*
 * @todo level column formatter
 * @author leokarl
 * @date 2012-12-08
 */
function levelFormatter(cellvalue, options, rowObject){
	if(cellvalue=='a'){
		return 'Agent Player';
	}else if(cellvalue=='c'){
		return 'Cash Player';
	}
}
/*
 * @todo operation column formatter
 * @author leokarl
 * @date 2012-12-08
 */
function operationFormatter(cellvalue, options, rowObject) {
	// initialize
	var account_id =  (rowObject[0] === undefined) ? rowObject.account_id : rowObject[0];
	var level = (rowObject[1] === undefined) ? rowObject.level : rowObject[1];
    if (account_id!=""){
    	if(writeOnlineUsers == 1 || userType == 'agent'){
    		return '<center><input class="btn mini red" type="button" value="Logout" style="width:50px" onClick="javacript: forcePlayerLogOut(\'' + account_id + '\',\'' + level + '\');"></center>';
    	}else{
    		return '<center><input class="btn mini red" type="button" value="Logout" disabled="disabled" style="width:50px"></center>';
    	}
    }else{
        return '';
    }
}
/*
 * @todo casino column formatter
 * @author leokarl
 * @date 2012-12-08
 */
function casinoFormatter(cellvalue, options, rowObject) {
    if (cellvalue==0){
    	return 'ClubVegas999';
    }else if(cellvalue==1){
    	return 'HatienVegas999';
    }else if(cellvalue==2){
    	return 'SavanVegas999';
    }else if(cellvalue==3){
    	return 'CostaVegas999';
    }else if(cellvalue==4){
    	return 'VirtuaVegas999';
    }
}

function playerStatusList()
{
	document.getElementById('qry_reslut_player_status').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_reslut_player_status").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_reslut_player_status").appendChild(divTag1);

	if(session_level=='SC' || session_level=='SMA' || session_level=='MA' || session_level=='AGT'){
		//account_id='<?php //echo yii::app()->session['account_id'];?>';
	}else{
		account_id='';
	}
	var grid=jQuery("#list2");
	grid.jqGrid({ 
		url: playerStatuslistURL,
		datatype: 'json',
	    mtype: 'GET',
	    colNames: ['Account ID', 'Account Name','Status','In-Active Time','Active Time'],
	    colModel: [
	      {name: 'account_id', index: 'account_id', width: 150,title:false,sorttype:"string",},
          {name: 'account_name', index: 'level', width: 150,title:false,sorttype:"string",},
          {name: 'status', index: 'casino_name', width: 150,title:false,sorttype:"string",},
          {name: 'kick_off_time', index: 'casino_login_date', width: 150,title:false,sorttype:"string",},
          {name: 'kick_on_time', index: 'ip_address', width: 150,title:false,sorttype:"string",}, 
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
    	},
	    rowNum: 100,
	    rownumbers:true,
	    rowList: [20, 50, 100,200,500,99999],
	    pager: '#pager2',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    caption: 'De-activate/Activate Player List',
	    hidegrid: false,
	    height: '100%',
	    loadonce: true, // to enable sorting on client side 
        sortable: true, //to enable sorting 
        viewrecords: true
	});
}

function tableOnlineUsers() { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);

	if(session_level=='SC' || session_level=='SMA' || session_level=='MA' || session_level=='AGT'){
		//account_id='<?php //echo yii::app()->session['account_id'];?>';
	}else{
		account_id='';
	}
	var grid=jQuery("#list1");
	grid.jqGrid({ 
		url: listURL,
		datatype: 'json',
	    mtype: 'POST',
	    colNames: ['Account ID', 'Level','CV999 Login Date','Casino','Casino Login Date','IP Address','Operation'],
	    colModel: [
	      {name: 'account_id', index: 'account_id', width: 100,title:false,sorttype:"string",},
          {name: 'level', index: 'level', width: 150,title:false,formatter: levelFormatter,sorttype:"string",},
          {name: 'cv999_login_date', index: 'cv999_login_date', width: 150,title:false,sorttype:"string",},
          {name: 'casino_name', index: 'casino_name', width: 100,title:false,formatter: casinoFormatter,sorttype:"string",},
          {name: 'casino_login_date', index: 'casino_login_date', hidden: true, width: 150,title:false,sorttype:"string",},
          {name: 'ip_address', index: 'ip_address', width: 250,title:false,sorttype:"string",}, 
          {name: 'operation', index: 'operation', hidden: true, width: 60,title:false,formatter:operationFormatter,sortable: false}, 
	    ],
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
    	},
	    rowNum: 100,
	    rownumbers:true,
	    rowList: [20, 50, 100,200,500,99999],
	    pager: '#pager1',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    caption: 'Online Player List',
	    hidegrid: false,
	    height: '100%',
	    loadonce: true, // to enable sorting on client side 
        sortable: true, //to enable sorting 
        viewrecords: true
	});
}
/*
 * @todo force logout player
 * @author leokarl
 * @date 2012-12-08
 */

function logout(player_id,player_level){
	jQuery.ajax({
		url: urlLogOutPlayer,
		type: 'POST',
		data: {'player_id': player_id,'player_level':player_level},
		context: '',
		success: function(msg) {
			alert(msg);
    		$createForceLogoutConfirmation.dialog('close');
    		tableOnlineUsers();//refresh list
    	}
	});
}

/**
 * 
 */
function kickOffByAgent() {
	jQuery.getJSON(kickOffURL, function(data, status, xhr) {
		jQuery("#setKickOffbtn").attr("value", kickOffJson[data.value]);
		jQuery("#lastKickOnTime").text(data.activeTime);
		jQuery("#lastKickOffTime").text(data.deactiveTime);
		tableOnlineUsers();
		playerStatusList();
		// jQuery.each(data, function(key, val) {});
	});
}

jQuery(document).ready(function() {
	<?php
		/**
		 * KickOff
		 */
	
		$agentKickOff = new AgentKickOff();
		$ako = $agentKickOff->getKickOffStatus(Yii::app()->session['account_id']);
		echo 'jQuery("#setKickOffbtn").attr("value", kickOffJson[' . $ako['@status'] . ']);';
	?>

	jQuery("#setKickOffbtn").click(function(event) {
		kickOffByAgent();
	});

	jQuery("#refreshPlayerOnlineTableKickOffbtn").click(function(event) {
		tableOnlineUsers();
	});
});

function Load(){
	var level='<?php echo Yii::app()->session['level']; ?>';
	if (level=='SC'){
		LoadSeniorMaster();
	}else if (level=='SMA'){
		LoadMaster();
	}else if (level=='MA'){
		LoadAgent();
	}else if (level=='AGT'){
		LoadAgentPlayer();
	}
}
</script>
</head>
<body onload="javascript: tableOnlineUsers();playerStatusList();Load();">
<?php
	/**
	 * KickOff
	 */
	$refreshImage = Yii::app()->request->baseUrl . "/images/refresh.png";

	if (Yii::app()->session['level'] === 'AGT')
	{	
		echo '<section class="settlement-top-container">
		<article class="settlement-top-title">
			<aside class="settlement-top-title-button"><a onclick="tableOnlineUsers();" href="#"><img title="Refresh Online Player List table" alt="Refresh" src="'. $refreshImage .'"/></a></aside>
		</article>
		<article class="settlement-top-list">
			<ul>
				<li><input type="button" id="setKickOffbtn" /></li>';
	
				$agentKickOff = new AgentKickOff();
				$ako = $agentKickOff->getKickOffStatus(Yii::app()->session['account_id']);

				// @status, @kickOffTime, @kickOnTime;
		echo '
				<li><label>Last Active:</label><span id="lastKickOnTime">' . $ako['@kickOnTime'] . '</span></li>
				<li><label>Last In-active:</label><span id="lastKickOffTime">' . $ako['@kickOffTime'] . '</span></li>
		';

		echo '</ul>
		</article>
    </section>';
	}
	else
	{
		echo '<a id="refreshPlayerOnlineTableKickOffbtn" href="#"><img style="width:45px;height:45px;" title="Refresh Online Player List table" alt="Refresh" src="'. $refreshImage .'"/></a>';
	}
?>    

    <section class="settlement-middle-container">         			 
		<div id="qry_result"></div>
		<br/><br/>
		<div id="qry_reslut_player_status"></div>
	</section>
</body>
</html>