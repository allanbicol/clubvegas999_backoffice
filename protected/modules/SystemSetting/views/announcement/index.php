<link rel="stylesheet" type=	"text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/role.css" />
<script type="text/javascript">
	var baseUrl="<?php echo Yii::app()->request->baseUrl;?>";
	$(document).ready(function() {
		announcementList('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Announcement/AnnouncementList',
				'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Announcement/Create'
				);
	});
	

	//active menu color
	document.getElementById('systemsettingHeader').className="start active";
	document.getElementById('mnu_system_setting_announcement').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i><li>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Anouncement</a></li>");

	function announcementList(listURL,createAnnouncementURL)
	{
		var grid=jQuery("#listlog");
		grid.jqGrid({ 
			url: listURL, 
			datatype: 'json',
		    mtype: 'GET',
		    height: 'auto',
		    colNames: ['id','Content English', 'content_status','Operation'],
		    colModel: [
			  {name: 'id', index: 'id', width: 10,title:false,hidden:true},	    
		      {name: 'contentEN', index: 'Announcement Message', width: 1200, search:true},
		      {name: 'content_status', index: 'Status', formatter:imageFormatter, width: 150, search:true,align:"center"},
		      {name: 'operation', index: 'operation', width: 150,title:false,align:"center"},
		    ],
		    loadComplete: function(){
		    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
			},
			width: 1400,
		    rowNum: 25,
		    rownumbers: true,
		    rowList: [25,50, 75, 100],
		    pager: '#pager2',
		    sortname: 'content_status',
		    sortorder: 'ASC',
		    hidegrid: false,
		    height: 'auto',
		    caption: 'Announcement Management <input class="btn red" type="submit" value="Add" onclick="window.location=\''+ createAnnouncementURL + '\';"/>',
		    viewrecords: true
		});
		
		
		

	}

	function imageFormatter(cellvalue, options, rowObject) {
        $("cellvalue").val(cellvalue);
        if (cellvalue=="published"){
        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/online.png" />'+"  "+'<label style="color:green">'+ cellvalue+'</label>';
        }else{
        	return '<img src="<?php echo Yii::app()->request->baseUrl;?>/images/offline.gif" />'+"  "+'<label style="color:#A7A7A6">'+ cellvalue+'</label>';
	    }
	}; 

	function openDeleteDialog(aElement)
	{
		var urlDeleteAnnouncement = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Announcement/Delete';
		var accountIDvar = aElement.href.split("&");
		var accountID=accountIDvar[1].split("=");
		var id=accountID[1].split(",");
		
		var conf=confirm("Are you sure you want to delete this annountment ");
		if (conf==true){
			jQuery.ajax({
		   		url: urlDeleteAnnouncement,
		   		type: 'POST',
		   		datatype:'json',
		   		data: {'id':id.toString()},
		   		context: '',
		   		success: function(data) {
			    		alert(data);
			    		$('#listlog').trigger("reloadGrid");
			    	}
		   	});
		}
	}
	
	 
</script>
</head>
<body>
	<table id="listlog" ><tr><td/></tr></table>
	<div id="pager2"></div>
</body>
</html>