<html>
<head>

	<script type="text/javascript">
	//The instanceReady event is fired, when an instance of CKEditor has finished
	//its initialization.
	var editor;
	CKEDITOR.on( 'instanceReady', function( ev ) {
		// Show this sample buttons.
		document.getElementById( 'eButtons' ).style.display = 'block';
	});
	editor.setReadOnly('True');
	</script>
</head>


<body>

	<?php 
	$language='';
	$dataReader = TableLanguage::model()->findAll();
	foreach ($dataReader as $row){
		$language=$language.",".$row['language'];
	}
	$langValue=split(",",$language);

	$enTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.profilePassword.enSetting',null,TRUE);
	$thTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.profilePassword.thSetting',null,TRUE);
	$vnTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.profilePassword.vnSetting',null,TRUE);
	$cnTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.profilePassword.cnSetting',null,TRUE);
	$hkTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.profilePassword.hkSetting',null,TRUE);
	$this->widget('zii.widgets.jui.CJuiTabs', array(
		'tabs' => array(
				'English'=>array('content'=>$enTab, 'id'=>'tabSignupEN1'),
				'Thai'=>array('content'=>$thTab, 'id'=>'tabSignupTH1'),
				'Vietnamese'=>array('content'=>$vnTab, 'id'=>'tabSignupVN1'),
				'Traditional Chinese'=>array('content'=>$cnTab, 'id'=>'tabSignupCN1'),
				'Simplified chinese'=>array('content'=>$hkTab, 'id'=>'tabSignupHK1'),
		),
		// additional javascript options for the tabs plugin
		'options' => array(
				'collapsible' => false,
		),
		// set id for this widgets
		'id'=>'ProfileChangePasswordTab',
		'htmlOptions'=>array(
				'style'=>'width:1000px;'
		),
	));
?>
</body>      
</html>

