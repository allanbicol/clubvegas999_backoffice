
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/playervalue.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/systemsetting.css" />
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/playervalue.js"></script>

<script type="text/javascript">
var playerValueListURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PlayerValueReport/PlayerValueList';
var playerValueListDetailsURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PlayerValueReport/PlayerValueListDetails';
var itemListURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PlayerValueReport/GetPromoItems';
var redeemURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Marketing/PlayerValueReport/RedeemItem';

</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

</script>
<style>
.ui-widget-overlay{
	z-index: 10010!important;
}
</style>
</head>
<body onload="javascript: displayPlayerValue();">
<div id="parameter_area" style="width: 500px">
	<div class="header" >PLAYER VALUE REPORT</div>
	<form action="">
	<br/>
		<table style="background-color:transparent;  width: 500px;">
			<tr><td style="padding-left: 5px;">PROMO FROM:</td>
				<td><input style="width: 130px"  type="text" id="datefrom" name="datefrom" value="" />
					<select id="cbHourfrom">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
									echo '<option>'.$value.'</option>';
								}
						?></select>	
						<input value=":00:00" style="border: 0px; background-color: transparent; width: 60px" disabled></td></tr>
			<tr><td style="padding-left: 5px;">PROMO TO:</td>
				<td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
					<select id="cbHourto">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
								foreach ($hHour as  $value){
									echo '<option selected="true">'.$value.'</option>';
								}
						?></select>
						<input value=":59:59" style="border: 0px; background-color: transparent; width: 60px" disabled></td></tr>
			</table><br/>
			<div align="center">
			<input onclick="javascript: displayPlayerValue();" type="button" value="Submit" class="btn red">
			</div>
	</form>	
</div>

<br/>
<div id="qry_resultPlayerValue"></div>
<br/>
<div id="qry_result_details">
</div>
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		month=month.toString();
		day=day.toString();
		
		if (month.length==1){
			month='0'+month;
		}
		if (day.length==1){
			day='0'+day;
		}
		document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
		document.getElementById("dateto").value=(year + "-" + month + "-" + day);
		</script>

	
	<?php 
	// withdraw Dialog----------------------------------------------------------------
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'alter-dialog',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Update Trans Item',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> true,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewAlterItem"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
</body>
</html>