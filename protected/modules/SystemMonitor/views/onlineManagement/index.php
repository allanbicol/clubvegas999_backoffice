<html>
<head>

	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/onlinemanagement.css" />
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/onlinemanagement.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/systemmonitor.js"></script>
	<script type="text/javascript">
		var urlJsAssets='<?php echo $this->module->assetsUrl; ?>';	
		var urlOnlineUserList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement/OnlineUserList';
		var urlLockUserList='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement/LockUserList';
		var urlLogOutUser='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement/LogoutUSer';
		var urlGetCasinoNameFromRedisByPlayerID='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement/GetCasinoNameFromRedisByPlayerID';
		var urlWaitIfBalanceStuckedOnLobby='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement/CheckBalanceStuckedOnTheLobby';
		var urlWithdrawFromCasinoLobby='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement/WithdrawFromCasinoLobby';
		var urlBalanceTransferStatus='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineManagement/BalanceTransferStatus';
		var writeOnlineUsers = '<?php echo (Yii::app()->user->checkAccess('systemMonitor.writeOnlineUsers'))?>';
		var userType='<?php echo User::getUserType()?>';
	</script>
</head>
<body onload="javascript: onTheLoad();">
<div id="body_wrapper">
	<div id="body_header">Online Management</div>
	<div id="qry_parameter" style="display: none;">
		Level:
		<Select id="cmbLevel" onchange="javascript: tableOnlineUsers(this.value)" style="font-size: 9pt;">
			<option value="All">All</option>
			<option value="SC">Sub Company</option>
			<option value="SMA">Senior Master Agent</option>
			<option value="MA">Master Agent</option>
			<option value="AGT">Agent</option>
			<option value="player">Player</option>
			<option value="user">User</option>
		</Select> 
		
		<!--  <input type="radio" id="radYesterday" name="radButton" checked onchange="javascript: tableOnlineUsers(document.getElementById('cmbLevel').value)"><label for="radYesterday" style="font-size: 9pt;">Yesterday</label>
		<input type="radio" id="radToday" name="radButton" onchange="javascript: tableOnlineUsers(document.getElementById('cmbLevel').value)"><label for="radToday" style="font-size: 9pt;">Today</label>
		<input type="radio" id="radPlaying" name="radButton" onchange="javascript: tableOnlineUsers(document.getElementById('cmbLevel').value)"><label for="radPlaying" style="font-size: 9pt;">Played</label>
		-->
	</div>
	<div id="qry_result">
	</div>
	</br></br>
	<!-- <div><b>List of Player Locked in Casino Lobby</b></div> 
	<div id="qry_result1">-->
	</div>
</div>
</body>
</html>