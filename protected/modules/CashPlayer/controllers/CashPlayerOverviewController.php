<?php
/**
 * @todo CashPlayerOverviewController
 * @copyright CE
 * @author Allan Bicol
 * @since 2012-07-19
 */
class CashPlayerOverviewController extends MyController
{
	public function actionCashPlayerOverviewDetails()
	{
		$bl = new CashPlayerOverview;
		$player_records = $bl->getCashPlayerOverview();
		
		$records=0;
		$filedNames = array("currency","no_players","balance");
		$htmValue="";
		echo JsonUtil::jsonAgentTransHist($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,0);
	}
	
	
	public function actionCashPlayerOverviewConvertion()
	{
		$bl=new CashPlayerOverview();
		$bl->convertAmount();
	}
	
	public function actionPlayersBalance()
	{
		$playersBalance=new CashPlayerOverview();
		$playersBalance->getPlayersBalance();
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerOverview'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
}