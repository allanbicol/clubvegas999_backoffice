<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/setbonus.css" />
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/setbonus.js"></script>
<script type="text/javascript">

//isProcessingUnlock = false;
function cancelButton(account_id){
	
	//if(isProcessingUnlock){
	//       return;
	//}
	jQuery.ajax({
		url: baseUrl+'/index.php?r=CashPlayer/CashPlayerList/UnlockPlayer',
		type: 'POST',
		data: {'accountID' :account_id},
		context: '',
		async: false,
		success: function(data){
			//isProcessingUnlock = false;
		}	
	});
	  
}

$(".ui-dialog-titlebar-close").click(function() {
	cancelButton("<?php echo $_GET['account_id']; ?>");
});

</script>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td class="left">Account ID</td>
	<td class="right">
		<label id="lblAccountId"><?php echo $_GET['account_id'];?></label>
	</td>
</tr>
<tr>
	<td class="left">Currency</td>
	<td class="right">
		<label id="lblCurrency"><?php echo $currency;?></label>
	</td>
</tr>
<tr>
	<td class="left">Amount</td>
	<td class="right">
		<?php echo CHtml::textField('txtSetBonusAmount','',array('id'=>'txtSetBonusAmount'));?> 
		<font color="red">*</font> ("-" Remove)
	</td>
</tr>
<tr>
	<td class="left">Bonus Type</td>
	<td class="right">
		<?php echo CHtml::dropDownList('cmbBonusType', '', 
              $list, array('empty' => '(Select type)'));
		?>
	</td>
</tr>
</table>
<hr/>
<div style="text-align: center;">
<?php
	// submit button 
	echo CHtml::button('Confirm', array('id'=>'btnSubmitBonus', 'class'=>'btn red',
			'onclick'=>'confirmBonus("' . $_GET['account_id'] .'", $("#txtSetBonusAmount").val(), $("#cmbBonusType").val());'));
	echo '&nbsp;&nbsp;';

	// cancel button
	echo CHtml::button('Cancel', array('id'=>'btnCancelBonus','class'=>'btn red',
			'onclick'=>'$("#set_bonus_dialog").dialog("close"); cancelButton("'.$_GET['account_id'].'");'));
?>
</div>
