<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/setbonus.css" />
<script>
// initialize account id
$(document).ready(function(){
	$('#txtAccountId').val($('#accountID').val());
	$('#lblAccountId').html($('#accountID').val());	
});
</script>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td class="left">Account ID</td>
	<td class="right">
		<label id="lblAccountId">wer</label>
		<?php echo CHtml::textField('txtAccountId','',array('id'=>'txtAccountId', 'hidden'=>'hidden'));?></td>
</tr>
<tr>
	<td class="left">Amount</td>
	<td class="right">
		<?php echo CHtml::textField('txtSetBonusAmount','',array('id'=>'txtSetBonusAmount'));?> 
		<font color="red">*</font> ("-" Remove)
	</td>
</tr>
<tr>
	<td class="left">Bonus Type</td>
	<td class="right">
		<?php echo CHtml::dropDownList('cmbBonusType', '', 
              $list, array('empty' => '(Select type)'));
		?>
	</td>
</tr>
</table>
<hr/>

<?php
	// submit button 
	echo CHtml::button('SUBMIT', array('id'=>'btnSubmitBonus', 
			'onclick'=>'confirmBonus($("#txtAccountId").val(), $("#txtSetBonusAmount").val(), $("#cmbBonusType").val());'));

	// cancel button
	echo CHtml::button('CANCEL', array('id'=>'btnCancelBonus',
			'onclick'=>'$("#set_bonus_dialog").dialog("close");'));
?>

