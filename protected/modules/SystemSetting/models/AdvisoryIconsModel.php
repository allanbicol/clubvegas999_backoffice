<?php
/**
 * @todo ProcessEmailSetting Model
 * @copyright CE
 * @author Sitthykun
 * @since 2013-07-05
 */
class AdvisoryIconsModel
{
	/**
	 * 
	 * @param string
	 */
	public function getList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spGetAdvisoryIconList()");

		return $command->queryAll();
	}
}

?>