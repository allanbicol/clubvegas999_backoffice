<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agenttransactionhistory.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/userlistlog.js"></script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>

<body onload="javascript: submitComplete('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSubCompanyLog/AgentSubCompanyLog');">
<div id="body_wraper">
	<div id="transaction_parameter_box1">
		<div class="header">Account ID <font color="#D84A38"><?php echo $_GET['Account_ID'];?></font>: LOGS</div>
		<div class="body">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left">Type</td>
			<td class="right">
				<Select name="cmbLogType" id="cmbLogType">
					<option value="0">All</option>
					<?php 
						$dataReader = TableLogType::model()->findAll();
						foreach ($dataReader as $row){
							//if ($row['id']<19){
								echo '<option value="' . $row['id'] . '">'. strtoupper($row['log_type_name']) . '</option>';
							//}
					}
					?>
				</Select>
			</td></tr>
			<tr><td class="left"><?php echo $_GET['Account_ID'];?></td>
			<td class="right">
				<Select name="cmbOperation" id="cmbOperation">
					<option value="0">Operating</option>
					<option value="1">Operated</option>
				</Select>
			</td></tr>
			<tr><td class="left">FROM</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtFrom" name="dtFrom" class="dtText">
				<select id="cmbTime1">
				<?php 
					for($i=0;$i<=23;$i++){
						echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
					}
				?>
				</select> :00:00
			</td></tr>
			<tr><td class="left">
				TO
			</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtTo" class="dtText">
				<select id="cmbTime2">
				<?php 
					for($i=0;$i<=23;$i++){
						if($i<23){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}else{
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					}
				?> 
				</select> :59:59
			</td>
			</tr>
		</table>
		
		
		</div>
		<div class="row">
		</div>
		<center><input class="btn red" id="btnSubmit" name="" type="button" value="Submit" onclick="javascript: submitComplete('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentSubCompanyLog/AgentSubCompanyLog');"> <input class="btn red" type="button" value="Back" onclick="javascript: history.go(-1);"></center>
		<br/>
	</div>
	<div id="qry_result">
	<div id="pager1"></div>
	</div>
	<input id="txtAccountID" name="txtAccountID" value="<?php echo $_GET['Account_ID'];?>" hidden>
</div>
</body>
</html>