<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $isenable
 * @property integer $level_id
 * @property string $position
 *
 * The followings are the available model relations:
 * @property TblLevel $level
 */
class UserAccount extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public $dilimeter = '#';
	public $confirmedPassword;
	public $roleType;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_enable', 'numerical', 'integerOnly'=>true),
			array('account_id, account_name, password', 'length', 'max'=>45),
			array('black_list', 'numerical', 'integerOnly'=>true),
			array('white_list_ip_address, white_list_ip_range_address', 'length', 'max'=>500),
			array('white_list_ip_range_address, white_list_ip_range_address', 'length', 'max'=>500),
			array('password, confirmedPassword', 'length', 'min'=>6),
			array('account_id,password,confirmedPassword,roleType','required'),
			// to set the default value
			array('opening_date_time', 'default', 'value' => new CDbExpression('NOW()'), 'setOnEmpty' => true),
			array('password', 'compare', 'compareAttribute'=>'confirmedPassword'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('id, account_id, account_name, password, is_enable, opening_date_time, max_deposit, max_withdraw', 'safe', 'on'=>'search'),
			array('id, account_id, account_name, password, is_enable, opening_date_time, max_deposit, max_withdraw, black_list, white_list_ip_address, white_list_ip_range_address', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
//	public function relations()
//	{
//		// NOTE: you may need to adjust the relation name and the related
//		// class name for the relations automatically generated below.
//		return array(
//			'level' => array(self::BELONGS_TO, 'TblLevel', 'level_id'),
//		);
//	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'AccountID',
			'password' => 'Password',
			'isenable' => 'Isenable',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id,true);
		$criteria->compare('account_name',$this->account_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('is_enable',$this->is_enable);
		$criteria->compare('opening_date_time',$this->opening_date_time,true);
		$criteria->compare('max_deposit',$this->max_deposit,true);
		$criteria->compare('max_withdraw',$this->max_withdraw,true);

		$criteria->compare('white_list_ip_address', $this->white_list_ip_address, true);
		$criteria->compare('white_list_ip_range_address', $this->white_list_ip_range_address, true);
		$criteria->compare('black_list', $this->black_list, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getRoleList()
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT name,data from AuthItem  WHERE type=2");
		$rows = $command->queryAll();
		return CHtml::listData($rows,'name', 'data');
	
	}

	/**
	 * 
	 * @return Ambigous <NULL, unknown, multitype:unknown Ambigous <unknown, NULL> , mixed, multitype:, multitype:unknown >
	 */
	private function comparePassword($postAccountID, $postPassword) {
		$currentPassword = $this->find(
								array(
										'select' => 'password',
										'condition' => 'account_id=:postAccountID',
										'params' => array(':postAccountID' => $postAccountID)
								)	
							);		

		return  isset($currentPassword['password']) && $currentPassword['password'] == $postPassword ? TRUE : FALSE;
	}
	
	private function compareStatus($postAccountID, $postStatus) {
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		$connection = Yii::app()->db_cv999_fd_master;
		
		$currentStatus = $this->find(
				array(
						'select' => 'is_enable',
						'condition' => 'account_id=:postAccountID',
						'params' => array(':postAccountID' => $postAccountID)
				)
		);
		
		if(isset($currentStatus['is_enable']) && $currentStatus['is_enable'] != $postStatus ){
			
			if ($currentStatus['is_enable']==1){
				$statusOld='Active';
			}else{$statusOld='Closed';}
			
			if ($postStatus==1){
				$statusNew='Active';
			}else{$statusNew='Closed';}
			
			$command1 = $connection->createCommand("SELECT level_name from vwGetUserList  WHERE account_id='" . $postAccountID . "' ");
			$rd1=$command1->queryRow();
			$levelname=$rd1['level_name'];
			
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated=$postAccountID;
			$postLog->operated_level=$levelname;
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=8;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE status of <label style=\"color:green\">'.$levelname.'</label> <label style=\"color:#7A5C00\">'.$postAccountID.'</label> from <label style=\"color:red\">'.$statusOld.'</label> to <label style=\"color:red\">'.$statusNew.'</label></b>';
			$postLog->save();
		}
	}
	
	/**
	 * @return actions to perform before saving ie: hash password
	 */
	public function beforeSave()
	{
		$this->compareStatus($this->account_id, $this->is_enable);
		if (!$this->comparePassword($this->account_id, $this->password)) {
			$this->password = md5($this->password);
		}

		return true;
	}
	
	/**
	 * @todo getUserList
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-04-06
	 *
	 */
	public function getUserList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("Select * from vwGetUserList ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCountUserRecord
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-04-06
	 *
	 */
	public function getCountUserRecord()
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetUserList");
		$rows = $command->query();
		return $rows;
	}
	
	public function getUserRoleName($accountID)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT itemname FROM AuthAssignment WHERE userid=:userid");
		$command->bindParam(":userid",$accountID,PDO::PARAM_STR);
		$row = $command->queryScalar();
		return $row;
	}

	/**
	 *
	 * @param string $accountID
	 */
	public function getBlackListByUser($accountID) {
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT black_list FROM tbl_user WHERE account_id=:account_id");
		$command->bindParam(":account_id", $accountID, PDO::PARAM_INT);
		$row = $command->queryScalar();
		return $row;
	}

	/**
	 * 
	 * @param string $accountID
	 */
	public function getIPAddressByUser($accountID) {
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT white_list_ip_address FROM tbl_user WHERE account_id=:account_id");
		$command->bindParam(":account_id", $accountID, PDO::PARAM_STR);
		$row = $command->queryScalar();
		return $row;
	}

	/**
	 * 
	 * @param string $accountID
	 */
	public function getIPRangeAddressByUser($accountID) {
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT white_list_ip_range_address FROM tbl_user WHERE account_id=:account_id");
		$command->bindParam(":account_id", $accountID, PDO::PARAM_STR);
		$row = $command->queryScalar();
		return $row;
	}

	/**
	 *
	 * @param string $accountID
	 * @param string $value
	 */
	public function addIPAddressByUser($accountID, $value) {
		$command = Yii::app()->db->createCommand('CALL spBEAddIPByUser (:pi_account_id, :pi_value, :pi_dilimeter, @po_is_successful)');
		$command->execute(array('pi_account_id' => $accountID, 'pi_value' => $value, 'pi_dilimeter' => $this->dilimeter));
		//$subResult = Yii::app()->db->createCommand('SELECT ' . $isSuccessfulParam . ';')->queryAll();
		return 'successful';
	}

	/**
	 * 
	 * @param string $accountID
	 * @param string $value
	 */
	public function deleteIPAddressByUser($accountID, $value) {
		$command = Yii::app()->db->createCommand('CALL spBEDeleteIPByUser (:pi_account_id, :pi_value, :pi_dilimeter, @po_is_successful)');
		$command->execute(array('pi_account_id' => $accountID, 'pi_value' => $value, 'pi_dilimeter' => $this->dilimeter));
		return 'successful';
	}

	/**
	 *
	 * @param string $accountID
	 * @param string $value
	 */
	public function addIPRangeAddressByUser($accountID, $value) {

		$command = Yii::app()->db->createCommand('CALL spBEAddIPRangeByUser (:pi_account_id, :pi_value, :pi_dilimeter, @po_is_successful)');
		$command->execute(array('pi_account_id' => $accountID, 'pi_value' => $value, 'pi_dilimeter' => $this->dilimeter));
		//$subResult = Yii::app()->db->createCommand('SELECT ' . $isSuccessfulParam . ';')->queryAll();
		return 'successful';
	}
	
	/**
	 *
	 * @param string $accountID
	 * @param string $value
	 */
	public function deleteIPRangeAddressByUser($accountID, $value) {
		$command = Yii::app()->db->createCommand('CALL spBEDeleteIPRangeByUser (:pi_account_id, :pi_value, :pi_dilimeter, @po_is_successful)');
		$command->execute(array('pi_account_id' => $accountID, 'pi_value' => $value, 'pi_dilimeter' => $this->dilimeter));
		return 'successful';
	}
}