
/**
 * @todo deposit type onchange
 * @author leokarl
 * @since 2012-12-24
 * @param data; 1 = bank, 0 = others
 */
function changeDepositType(data){
	if(data == 1){
		// display bank list only
		$("#ddlBankList").css("display","inline");
		$("#ddlOtherList").css("display","none");
	}else{
		// display other list only
		$("#ddlBankList").css("display","none");
		$("#ddlOtherList").css("display","inline");
	}
}

 /**
  * @todo get selected deposit type from list
  * @author leokarl
  * @since 2012-12-24
  */
function getSelectedDepositType(){
	return ($("input:radio[name=radDepositType]:checked").val() == 1) ? $("#ddlBankList").val() : $("#ddlOtherList").val();
}

var isProcessingDeposit = false;

function processDeposit(aElement){
	if(isProcessingDeposit){
	       return;
	}
	isProcessingDeposit = true;
	var AcountID = aElement.id.split("=");
	jQuery.ajax({
		url: baseUrl + '/index.php?r=CashPlayer/CashPlayerDeposit/CheckPlayerOnlineStatus',
		type: 'POST',
		//data: {'accountID' : $('#txtAcountID').val()},
		data: {'accountID' : AcountID[1]},
		context: '',
		async:false,
		success: function(data){
			isProcessingDeposit = false;
			if(data == 'no_permission'){
				alert('Warning: You have no permission to set cash player bonus!');
				return false;
			}
			if (data!='die'){
				if (data == 0){
					// continue to deposit: open deposit dialog
					$('#viewDeposit').load(baseUrl + '/index.php?r=CashPlayer/CashPlayerList/LoadFormDeposit&account_id='+ AcountID[1],
						function(response, status, xhr) {		    	
			  				if (status == "success") {			    		  									
				  				$("#deposit-dialog").dialog("open"); return false;
				  			}else{
			  					$('#viewDeposit').load('<p style="color:red;">Something wrong during request!!!</p>');
			  					$("#deposit-dialog").dialog("open"); return false;
				  			}
			  			}
					);
					return false;
					
				}else{
					
					if(data=='w'){
						$createWaitWithdrawProcessDialog.dialog('open');
						$(".ui-dialog-titlebar-close").hide();
						var check =setInterval(function(){
							jQuery.ajax({
								url: baseUrl + '/index.php?r=CashPlayer/CashPlayerDeposit/ProcessWithdrawalRequest',
								type: 'POST',
								data: {'accountID' : AcountID[1]},
								context: '',
								async:false,
								success: function(data){
									isProcessingDeposit = false;
									if (data==0){ // withdrawal successfully
										$('#list2').trigger("reloadGrid");// reload grid
										$(".ui-dialog-titlebar-close").show();
										$createWaitWithdrawProcessDialog.dialog('close');
										
										$('#viewDeposit').load(baseUrl + '/index.php?r=CashPlayer/CashPlayerList/LoadFormDeposit&account_id='+ AcountID[1],
											function(response, status, xhr) {		    	
								  				if (status == "success") {			    		  									
									  				$("#deposit-dialog").dialog("open"); return false;
									  			}else{
								  					$('#viewDeposit').load('<p style="color:red;">Something wrong during request!!!</p>');
								  					$("#deposit-dialog").dialog("open"); return false;
									  			}
								  			}
										);
											
										clearInterval(check);
										return false;
									}
									else if(data=='h'){
										$createWaitWithdrawProcessDialog.dialog('close');
										alert('Cannot connect to HTV withdrawal API Server.Please try again!.');
										clearInterval(check);
										return false;
									}else if(data=='s'){
										$createWaitWithdrawProcessDialog.dialog('close');
										alert('Cannot connect to Savan withdrawal API Server.Please try again!.');
										clearInterval(check);
										return false;
									}else if(data=='vc'){
										$createWaitWithdrawProcessDialog.dialog('close');
										alert('Cannot connect to Virtual Casino withdrawal API Server.Please try again!.');
										clearInterval(check);
										return false;
									}else if(data=='svs'){
										$createWaitWithdrawProcessDialog.dialog('close');
										alert('Cannot connect to SlotVegas withdrawal API Server.Please try again!.');
										clearInterval(check);
										return false;
									}
									
						    	}
							});
							},2000);
							
					
					}
					else{
						// force logout player on the lobby
						forceLogOutDialog(AcountID[1]);
//						$createForceLogoutDialog.dialog('open');
//						$('#imgLogout').hide();
//						document.getElementById("accountIDLogout").value=AcountID[1];
//						document.getElementById("casinoId").value = data;
//						$(".ui-dialog-titlebar-close").hide();
//						document.getElementById("submitBonus").disabled = false;
//						return false;
					}
				}


			}else{

				// return to login page
				window.location= baseUrl + "/index.php?r=Login";
			}
    	}
	});
}

function makeDeposit(account_id, amount, type, trans_item){
	$('#btnConfirm').attr('disabled','disabled');
	jQuery.ajax({
		url: baseUrl + '/index.php?r=CashPlayer/CashPlayerDeposit/CashPlayerDepositProcess',
		type: 'POST',
		async: false,
		//data: {'task': 'playerDeposit', 'AccountID': document.getElementById('txtAcountID').value, 'dblAmount' : document.getElementById('txtAmount').value, 'strCashierPass' : document.getElementById('txtCashierPassword').value},
		data: {'account_id': account_id, 'amount':amount, 'type':type, 'trans_item':trans_item},
		success: function(msg) {
			if(msg == 'no_permission'){
				alert('Warning: You have no permission to make deposit!');
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'account_id_not_set'){
				alert('Warning: Account ID not set!');
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'invalid_account_id'){
				alert('Warning: Invalid Account ID!');
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'invalid_deposit_amount'){
				alert('Warning: Invalid deposit amount!');
				$('#txtDepositAmount').focus();
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'deposit_amount_not_set'){
				alert('Warning: Deposit amount not set!');
				$('#txtDepositAmount').focus();
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'deposit_type_not_set'){
				alert('Warning: Deposit type not set!');
				$('#bank').focus();
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'invalid_deposit_type'){
				alert('Warning: Invalid deposit type');
				$('#bank').focus();
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'transaction_item_not_set'){
				alert('Warning: Transaction item not set!');
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'invalid_trans_item'){
				alert('Warning: Invalid transaction item!');
				if(type == 1){
					$('#ddlBankList').focus();
				}else{
					$('#ddlOtherList').focus();
				}
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'select_trans_item'){
				alert('Please select transaction item');
				if(type == 1){
					$('#ddlBankList').focus();
				}else{
					$('#ddlOtherList').focus();
				}
				$('#btnConfirm').removeAttr('disabled');
				return false;
			}else if(msg == 'player_is_on_lobby'){
				alert('Player is on the lobby!');
				$("#deposit-dialog").dialog("close");
				$('#list2').trigger("reloadGrid");		
			}else if(msg == 'deposit_done'){
				$("#deposit-dialog").dialog("close");
				alert('Deposit complete!');
				$('#list2').trigger("reloadGrid");	
				$('#btnConfirm').removeAttr('disabled');
				//window.location= baseUrl + "/index.php?r=CashPlayer/CashPlayerList";
			}
			
    	}
	});
}