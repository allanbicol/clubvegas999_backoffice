//active menu color
document.getElementById('systemsettingHeader').className="start active";
document.getElementById('mnu_user_account').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>System Setting <i class='icon-angle-right'></i></li><li><a href='#'>Users</a></li>");
var isIE = document.all?true:false;
var colNo=0;
if (!isIE) document.captureEvents(Event.CLICK);
document.onmousemove = getMousePosition;
function getMousePosition(e) {
  var _x;
  var _y;
  if (!isIE) {
    _x = e.pageX;
    _y = e.pageY;
  }
  if (isIE) {
    _x = event.clientX + document.body.scrollLeft;
    _y = event.clientY + document.body.scrollTop;
  }

posX=_x;
posY=_y;

  return true;
}
function getPosition(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    } 
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX + 
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY + 
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    alert(cursor);
    return cursor;
}
function onMouseOutDiv(id){
	document.getElementById('id').addEventListener('mouseout',onMouseOut,true);
}
function onMouseOut(event) {
    //this is the original element the event handler was assigned to
        e = event.toElement || event.relatedTarget;
        if (e.parentNode == this || e == this) {
           return;
        }
        hideDiv("dvStatus");
    // handle mouse event here!
}
var divTag4 = document.createElement("div"); 
function createDiv() { 
    divTag4.id = "dvStatus"; 
    divTag4.setAttribute("align", "center"); 
    divTag4.setAttribute("onmouseout","javascript: document.getElementById('dvStatus').addEventListener('mouseout',onMouseOut,true);")
    divTag4.style.margin = "0px auto"; 
    divTag4.className = "dynamicDiv"; 
    //divTag.innerHTML = ''; 
    document.body.appendChild(divTag4);
} 
function hideDiv(divID) { 
	if (document.getElementById) { 
	 	document.getElementById(divID).style.visibility = 'hidden'; 
	} 
} 
function showDiv(divID,x,y) { 
	 var X = 0; 
	 var Y = 0; 
	 
	 if (document.getElementById) { 
	 
	 var relativeX = x;
	 var relativeY = y;
	 document.getElementById(divID).style.left= relativeX+"px";
	 document.getElementById(divID).style.top= relativeY+"px";
	 document.getElementById(divID).style.visibility = 'visible'; 
	 } 
}
function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}
function userList(listURL,actionURL,captionURL)
{
	var grid=jQuery("#listlog");
	grid.jqGrid({ 
		url: listURL, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['ID', 'Account Name','Status','level','Opening Date/Time','Max. Deposit','Max. Withdrawal','Operation','Online Status','Last Login IP'],
	    colModel: [
	      {name: 'account_id', index: 'account_id', width: 50, search:true},
	      {name: 'account_name', index: 'account_name', width: 90},
	      {name: 'account_status', index: 'account_status', width: 50},
	      {name: 'level_name', index: 'level_name', width: 70},
	      {name: 'opening_date_time', index: 'opening_date_time', width: 65},
	      {name: 'max_deposit', index: 'max_deposit', width: 50,hidden:true },
	      {name: 'max_withdraw', index: 'max_withdraw', width: 50,hidden:true },
	      {name: 'operations', index: 'operations', width: 55, sortable: false},
	      {name: 'online_status', index: 'online_status', width: 110, hidden: true},
	      {name: 'last_login_ip_address', index: 'last_login_ip_address', width: 130,hidden: true}
	    ],
	    ondblClickRow: function (rowid, iCol, cellcontent, e) {
	    	var myrow = grid.jqGrid('getRowData', rowid);
	    	
		    if (colNo==3){
			    if (myrow.account_status == 'Closed')
				{
			    	divTag4.innerHTML = '<input type="checkbox" name="chkStat" value="Active" class="status-popup" id="stat_active"/> <label for="stat_active">Active</label>';
				}
			    else if(myrow.account_status=='Active')
			    {
			    	divTag4.innerHTML = '<input type="checkbox" name="chkStat" value="Closed" class="status-popup" id="stat_close"/> <label for="stat_close">Closed</label>';
				}
		    	jQuery('.status-popup').click(function(event) {
		    		currenctStatusValue = jQuery(this).val();
		    		
		    		jQuery.ajax({
			    		url: actionURL,
			    		method: 'POST',
			    		data: {'task': 'changeStatus', 'AccountID': myrow.account_id, 'status': jQuery(this).val()},
			    		context: jQuery(this).val(),
			    		success: function(data) {
			    			 grid.jqGrid('setCell', rowid, colNo, currenctStatusValue);
			    			 hideDiv("dvStatus");
				    	}
			    	});
		    	});
	    		showDiv('dvStatus',posX-5,posY-5);
		    }
        },
        onCellSelect : function (rowid,iCol,cellcontent,e) {
        	var myrow = grid.jqGrid('getRowData', rowid);
			rValue = myrow.account_id;
			colNo=iCol;
			
	    },
	    loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");

		    $('.setting').click(function(){
		    	setCookie("page",$('.ui-pg-input').val()); //current page number
			});
		},
		width: 1100,
	    rowNum: 50,
	    rownumbers: true,
	    rowList: [25,50, 75, 100],
	    pager: '#pager2',
	    sortname: 'account_id',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: '<input type="submit" class="btn red" value="Add" onclick="window.location=\''+ captionURL + '\';"/> User Account',
	    viewrecords: true
	});
	
	$('#listlog').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search: false, refresh: false});
}