<?php
class AdvisoryPublishedCategories
{

	/**
	 * @return string
	 * @param string $agent
	 */
	public function getAllStatus() {
		$outputParam = "@successful";
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetAdvisoryPublishStatusCategories(0, {$outputParam})");

		//$command = $connection->createCommand("SELECT " . $outputParam . ";");
		return $command->queryAll();
	}

	/**
	 * @return string
	 * @param string $agent
	 */
	public function getStatusById($id) {
		$outputParam = "@successful";
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetAdvisoryPublishStatusCategories({$id}, {$outputParam})");

		//$command = $connection->createCommand("SELECT @status, @successful;");
		return $command->queryRow(); 
	}

}