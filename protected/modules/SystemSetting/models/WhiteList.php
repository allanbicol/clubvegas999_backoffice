<?php
/**
* @todo WhiteList Model
* @copyright CE
* @author Leo karl
* @since 2012-11-02
*/

class WhiteList
{
 
    /**
     * @todo getSubCompanyList
     * @copyright CE
     * @author Leo karl
     * @since 2012-11-02
     */
	public function getSubCompanyList($orderField, $sortType, $startIndex , $limit)
	{
		//initialize
		$account_id = (isset($_GET['account_id'])) ? $_GET['account_id'] : "";
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT a.account_id,a.account_name,b.currency_name,c.status_name as status,a.opening_acc_date,a.white_list FROM tbl_agent a 
			LEFT JOIN tbl_currency b ON b.id=a.currency_id 
			LEFT JOIN tbl_status c ON c.id=a.agent_status_id 
			WHERE a.account_id like '%" . $account_id . "%' 
			AND a.agent_type='SC' ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo countSubCompanyList
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-02
	 */
	public function countSubCompanyList()
	{
		//initialize
		$account_id = (isset($_GET['account_id'])) ? $_GET['account_id'] : "";
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_agent WHERE agent_type='SC' AND account_id like '%" . $account_id . "%' ");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCashPlayerList
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-02
	 */
	public function getCashPlayerList($orderField, $sortType, $startIndex , $limit)
	{
		//initialize
		$account_id = (isset($_GET['account_id'])) ? $_GET['account_id'] : "";
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT a.account_id,a.account_name,b.currency_name,c.status_name as status,a.opening_acc_date,a.white_list FROM tbl_cash_player a 
			LEFT JOIN tbl_currency b ON b.id=a.currency_id 
			LEFT JOIN tbl_status c ON c.id=a.status_id
			WHERE a.account_id like '%" . $account_id . "%' 
			ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo countCashPlayerList
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-02
	 */
	public function countCashPlayerList()
	{
		//initialize
		$account_id = (isset($_GET['account_id'])) ? $_GET['account_id'] : "";
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_cash_player WHERE account_id like '%" . $account_id . "%' ");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo saveSubCompanyWhiteList
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-02
	 */
	public function saveSubCompanyWhiteList(){
		//START: $_POST['is_checked'] validation
		if($_POST['is_checked']!='true' && $_POST['is_checked']!='false'){
			exit('Invalid white_list value!');
		}
		//END: $_POST['is_checked'] validation
		//START: $_POST['account_id'] validation
		if(trim($_POST['account_id'])==''){
			exit('Invalid account_id value!');
		}
		//END: $_POST['account_id'] validation
		
		$log_details='';
		
		if($_POST['is_checked']=='true'){
			$is_checked=1;
			$msg='SC '. $_POST['account_id'] . ' and his/her sub agents and players were successfully added to white list.';
			$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> added <label style=\"color:green\">Sub Company</label> <label style=\"color:red\">'.$_POST['account_id'].'</label> to WhiteList.</b>';
		}else{
			$is_checked=0;
			$msg='SC '. $_POST['account_id'] . ' and his/her sub agents and players were successfully removed from the white list.';
			$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> removed <label style=\"color:green\">Sub Company</label> <label style=\"color:red\">'.$_POST['account_id'].'</label> from WhiteList.</b>';
		}
		TableAgent::model()->updateAll(array('white_list'=>$is_checked),'account_id like "'.$_POST['account_id'].'%"');
		TableAgentPlayer::model()->updateAll(array('white_list'=>$is_checked),'account_id like "'.$_POST['account_id'].'%"');
		
		// save logs
		$logs = new SystemSetting();
		$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'], $_POST['account_id'], 'Sub Company', 17, $log_details);
		
		echo $msg;
	}
	
	/**
	 * @todo saveCashPlayerWhiteList
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-11-02
	 */
	public function saveCashPlayerWhiteList(){
		//START: $_POST['is_checked'] validation
		if($_POST['is_checked']!='true' && $_POST['is_checked']!='false'){
			exit('Invalid white_list value!');
		}
		//END: $_POST['is_checked'] validation
	
		if($_POST['is_checked']=='true'){
			$is_checked=1;
			$msg='Cash player '. $_POST['account_id'] . ' was successfully added to white list.';
			$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> added <label style=\"color:green\">Cash Player</label> <label style=\"color:red\">'.$_POST['account_id'].'</label> to WhiteList.</b>';
		}else{
			$is_checked=0;
			$msg='Cash player '. $_POST['account_id'] . ' was successfully removed from the white list.';
			$log_details='<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> removed <label style=\"color:green\">Cash Player</label> <label style=\"color:red\">'.$_POST['account_id'].'</label> from WhiteList.</b>';
		}
		TableCashPlayer::model()->updateAll(array('white_list'=>$is_checked),'account_id="'.$_POST['account_id'].'"');
		
		// save logs
		$logs = new SystemSetting();
		$logs->saveLog(Yii::app()->session['account_id'], Yii::app()->session['level_name'], $_POST['account_id'], 'Cash Player', 17, $log_details);
		
		echo $msg;
	}
	
	
	
}