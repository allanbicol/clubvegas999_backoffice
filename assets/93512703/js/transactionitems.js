// active menu color
document.getElementById('mnu_transaction_items').style.color="#5A0000";
document.getElementById('mnu_transaction_items').style.fontWeight="bold";

/**
 * @todo perform scripts onload
 * @author leokarl
 * @since 2012-12-20
 */
$(document).ready(function(){
	tableTransactionItems(); // jqgrid
});

/**
 * @todo jqgrid transaction items
 * @author leokarl
 * @since 2012-12-20
 */
function tableTransactionItems(){
	document.getElementById('jqgrid_transaction_items').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list_transaction_items'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("jqgrid_transaction_items").appendChild(divTag);
    
    var div2Tag = document.createElement("Div"); 
    div2Tag.id = 'pager'; 
    div2Tag.style.margin = "0px auto"; 
    document.getElementById("jqgrid_transaction_items").appendChild(div2Tag);
    
	var grid = jQuery("#list_transaction_items");
        
	grid.jqGrid({ 
		url: urlTransactionItems, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: ['ID', 'Transaction Item', 'Type', 'Bank Transaction'],
		colModel: [
			  {name: 'id', index: 'id', width: 25,sortable: false, hidden: true},
			  {name: 'transaction_item', index: 'transaction_item', width: 150,sortable: true,},
			  {name: 'item_type', index: 'item_type', width: 80,sortable: true},
		      {name: 'transaction_bank', index: 'transaction_bank', width: 80,sortable: true, align:'center', 
				  editable: true, edittype: "checkbox",editoptions: { value:"1:0"},formatter: "checkbox",
				  formatoptions: {disabled : (permissionWrite) ? false : true}}, // permissionWrite = boolean
		],
		loadComplete: function(){
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    	
	    	var getColumnIndexByName = function(grid, columnName) {
	            var cm = grid.jqGrid('getGridParam', 'colModel'), i, l;
	            for (i = 1, l = cm.length; i < l; i += 1) {
	                if (cm[i].name === columnName) {
	                    return i; // return the index
	                }
	            }
	            return -1;
	        };
	    	
	    	var iCol = getColumnIndexByName ($(this), 'transaction_bank'), rows = this.rows, i,
	        c = rows.length;
	    	
		    for (i = 1; i < c; i += 1) {
		        $(rows[i].cells[iCol]).change(function (e) {
		        	// initialize
		            var id = $(e.target).closest('tr')[0].id, isChecked = $(e.target).is(':checked');
			    	var item_id = grid.jqGrid('getCell', id, 'id');
			    	var transaction_item = grid.jqGrid('getCell', id, 'transaction_item');			    	
		            var confirmMsg= (isChecked==true) ? "Set " + transaction_item + " to Bank transaction?" : "Unset " + transaction_item + " Bank transaction?";
			    	var conf = confirm(confirmMsg);
			    	
			    	// confirmation box
			        if(conf == true){
			        	// update function
			        	updateItemBankTransaction(item_id, isChecked); 
			        }else{
			        	// reload grid
			        	$('#list_transaction_items').trigger( 'reloadGrid' ); 
				    }
		        });
		    }
		},
		sortname: 'id',
	    sortorder: 'ASC',
	    rowNum: 50,	
	    rowList: [20, 50, 100, 200, 99999],
        height: 'auto',
        width: 500,
        hidegrid: false,
        rownumbers: true,
        caption: 'Transaction Items <button class="btn red" value="ADD" onclick="openAddTransactionItem();">ADD <i class="icon-plus"></i></button>',
        viewrecords: true,
	    pager: '#pager',
    	
	});
	
	// jqgrid footer
	$('#list_transaction_items').jqGrid('navGrid', '#pager', {edit: false, add: false, del:false, refresh: false, search: false});
	
	// add button
	$('.ui-pg-div').click(function(){
		openAddTransactionItem();
	});
	
}

/**
 * @todo open add transaction item dialog
 * @author leokarl
 * @since 2012-12-20
 */
function openAddTransactionItem(){
	$('#viewAddTransactionItem').load(urlAddTransactionItemDialog,
		function(response, status, xhr) {		    	
			if (status == "success") {			    		  									
  				$("#add_transaction_item_dialog").dialog("open"); 
  				return false;
  			}else{
				$('#viewAddTransactionItem').load('<p style="color:red;">Something wrong during request!!!</p>');
				$("#add_transaction_item_dialog").dialog("open"); 
				return false;
  			}
		}
	);
}

/**
 * @todo add transaction item
 * @author leokarl
 * @since 2012-12-20
 * @param item
 * @param type
 * @param bank
 */
function addTransactionItem(item, type, bank){
	// initialize
	var s_bank_checked = (bank.checked) ? 1 : 0;

	jQuery.ajax({
		url: urlAddTransactionItem,
		type: 'POST',
		data: {'item':item, 'type':type, 's_bank_checked':s_bank_checked},
		context: '',
		success: function(msg){
			if(msg == 'item_not_set'){
				
				alert('Transaction Item not set!');
				$('#txtTransactionItem').focus();
				
			}else if(msg == 'type_not_set'){
				
				alert('Transaction Type not set!');
				$('#cmbTransactionType').focus();
				
			}else if(msg == 'bank_not_set'){
				
				alert('Transaction Type not set!');
				$('#chkBankTransaction').focus();
				
			}else if(msg == 'special_char'){
				
				alert('Special character(s) not allowed!');
				$('#txtTransactionItem').focus();
				
			}else{
				// adding transaction item succeeded
				alert(msg); // message
				$("#add_transaction_item_dialog").dialog("close"); // close dialog
				$('#list_transaction_items').trigger("reloadGrid"); // reload grid
			}
    	}
	});
}


/**
 * @todo update item bank transaction
 * @author leokarl
 * @since 2012-12-20
 * @param item_id
 * @param isChecked
 */
function updateItemBankTransaction(item_id, isChecked){
	jQuery.ajax({
		url: urlUpdateItemBankTransaction,
		type: 'POST',
		data: {'item_id':item_id ,'bank_transaction':isChecked},
		context: '',
		success: function(data){
			alert(data);
			$('#list_transaction_items').trigger( 'reloadGrid' ); // reload grid
    	}
});
}

