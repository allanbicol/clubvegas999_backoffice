<?php
/**
 * @todo AgentSeniorMAsterLogController
 * @copyright CE
 * @author allan
 * @since 2012-06-18
 */
class AgentSeniorMasterLogController extends MyController
{
	public function actionAgentSeniorMasterLog()
	{

		$cpHfund = new AgentSeniorMasterLog();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountAgentSeniorMasterLog();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentSeniorMasterLog();
		//$test = $records->readAll();
		
		$filedNames = array("operated_by","operated_by_level","operated","operated_level","operation_time","log_details");
		$urlAS= Yii::app()->request->baseUrl;

		$htmvalue= '';
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,9);
	
			
	}
	
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readLog') || User::getUserType() === Constants::ROLE_AGENT){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	
	}
}
	