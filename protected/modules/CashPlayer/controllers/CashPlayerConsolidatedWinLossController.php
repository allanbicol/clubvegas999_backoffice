<?php
class CashPlayerConsolidatedWinLossController extends MyController
{

	public function actionCashPlayerConsolidatedWinLoss()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerConsolidatedWinLoss;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerConsolidatedWinLoss();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerConsolidatedWinLoss($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("","account_id","currency_name","total_stake","amount_wager","avg_bet","win_los","commission","bonus_total","bonus","bonus_used","amount_tips","total_win_los","p_l","deposit","bonus_total","withdraw");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerConsolidatedWinLossTotal()
	{

		$cpHfund = new CashPlayerConsolidatedWinLoss;
		$player_records = $cpHfund->getPlayerConsolidatedWinLossTotal();

		$filedNames = array("currency_name","player_count","total_stake","amount_wager","win_los","commission","bonus","amount_tips","total_win_los","p_l","avg_player_value","rate","total_stake","amount_wager","win_los","commission","bonus","amount_tips","total_win_los","p_l","avg_player_value");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readConsolidatedWinLoss'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionConsolidatedExcel(){
		$file="CashPlayerConsolidated_WinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		try{
			echo $buffer;
		}catch(Exception $e){
	
		}
	}
	
	public function actionExportAllCashPlayerConsolidatedWinLoss()
	{
		$cpHfund = new CashPlayerConsolidatedWinLoss;
		$result=$cpHfund->getCountPlayerConsolidatedWinLoss();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records = $cpHfund->getExportPlayerConsolidatedWinLoss();
	
		$fieldNames = array("account_id","currency_name","total_stake","amount_wager","avg_bet","win_los","commission","bonus_total","bonus","bonus_used","amount_tips","total_win_los","p_l","deposit","withdraw");
		$headerNames=array("Account Id","Currency","Total Stake","Valid Stake","Average Bet","Winloss","Commission","Assigned Bonus","Available Bonus","Used Bonus","Tips","Total","P/L","Deposit","Withdraw");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerConsolidatedWinLossTotal();
		
		$fieldNames = array("currency_name","total_stake","amount_wager","win_los","commission","bonus","amount_tips","total_win_los","p_l");
		$headerNames=array("Currency Name","Total Stake","Valid Stake","Winloss","Commission","Bonus Used","Tips","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
}