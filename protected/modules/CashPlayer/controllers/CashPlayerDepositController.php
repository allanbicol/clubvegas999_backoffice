<?php
/**
 * @todo CashPlayerDepositController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerDepositController extends MyController
{
	public function actionProcessWithdrawalRequest()
	{
		$withdrawal = new RedisLobbyManager();
		$withdrawal->ProcessWithdrawalRequest($_POST['accountID']);
	
	}
	/**
	 * @todo process cash player deposit
	 * @author leokarl
	 * @since 2012-12-24
	 */
	public function actionCashPlayerDepositProcess()
	{
		// initialzie
		$model = new CashPlayerDeposit();
		//$redis = new RedisManager();
		// data validation
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerDeposit'))
		{
			if(!isset($_POST['account_id'])){
				exit('account_id_not_set');
			}
			if(!$model->isAccountIdExist($_POST['account_id'])){
				exit('invalid_account_id');
			}
			if(!isset($_POST['amount'])){
				exit('deposit_amount_not_set');
			}
			if($_POST['amount']<=0){
				exit('invalid_deposit_amount');
			}
			if($model->isSpecialCharPresent($_POST['amount']) || !is_numeric($_POST['amount'])){
				exit('invalid_deposit_amount');
			}
			if(!isset($_POST['type'])){
				exit('deposit_type_not_set');
			}
			if(!$model->isDepositTypeValid($_POST['type'])){
				exit('invalid_deposit_type');
			}
			if(!isset($_POST['trans_item'])){
				exit('transaction_item_not_set');
			}
			if(trim($_POST['trans_item']) == ''){
				exit('select_trans_item');
			}
			if(!$model->isTransactionItemValid($_POST['trans_item'], $_POST['type'])){
				exit('invalid_trans_item');
			}
			
//			if($redis->isExistingOnLobbyByPlayer($_POST['account_id']) != 0){
//				exit('player_is_on_lobby');
//			}
			// continue saving
			$model->saveCashPlayerDeposit($_POST['account_id'], $_POST['amount'], $_POST['trans_item']);
		}
		else
		{
			exit('no_permission');
		}
	}
	
	/**
	 * @todo check player's online status
	 * @author leokarl
	 * @since 2012-12-21
	 */
	public function actionCheckPlayerOnlineStatus(){
		if(!isset(Yii::app()->session['account_id'])){
			exit('die');
		}
	
		if(!Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerDeposit')){
			exit('no_permission');
		}
		// initialize
		//$redis = new RedisManager();
		$checkLobby = 0;//$redis->isExistingOnLobbyByPlayer($_POST['accountID']);
		//$casinoId = $redis->getCasinoIDByPlayer($_POST['accountID']);

		if ($checkLobby!=0){
			echo $casinoId;
		}
		else
		{
			$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer
					WHERE account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
			$rd = $command->queryRow();
			if ($rd['deposit_withdrawal'] == 1)
			{
				// display wating message.
				echo 'w';
			}else {
				
				//lock the player before display dialog
				TableCashPlayer::model()->updateAll(array(
				'kick_off'=>3),
				'account_id="'.$_POST["accountID"].'"');
				//display setbonus dialog
				echo '0';
			}
		}
	
	}
}
