<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="language" content="en" />
	<title>CodeEric | Login</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/select2/select2_metro.css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/pages/login.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/loginCheckAttempt.js"></script>
</head>
<style>
#lang ul {
	display:table-row;
}
#lang ul li
{
   display: table-cell;
   list-style-type: none;
   margin: 10px;
   vertical-align: middle;
   padding-left: 5px;
}
body{
		background-image:url(<?php echo Yii::app()->request->baseUrl; ?>/images/header-pattern.jpg)!important;
}
</style>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login"  onload="loadCheck();" >
<script type="text/javascript">
	var checkLocked='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/CheckLocked';
	var addLockedUser='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/AddLockUser';
	var removeLockedUser='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/RemoveLockUser';
	var countAttempt='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/CountAttempt';
	var getDuration='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/GetDuration';
	var attempt_count='<?php echo yii::app()->session['no_attempt']; ?>';
	var user_that_attempt='<?php echo yii::app()->session['user_that_attempt']; ?>'
</script>

<!-- BEGIN LOGO -->
<div class="logo">
<!--	<h3>CodeEric</h3>-->
</div>

	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<div class="form-vertical login-form">
		<div class="form">
			
			<center>
			<form id="loginform" name="loginform" method="post" action="">
		    <input type="hidden" value="en" name="chooselan" id="chooselan">
		    <div id="lang">
		        <ul id="lang_lanjian">
		          <li><a onclick="document.getElementById('chooselan').value='en';document.getElementById('loginform').submit();" href="#"><img  title="English" alt="English" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/en.png"></a></li>
		          <li><a onclick="document.getElementById('chooselan').value='th';document.getElementById('loginform').submit();" href="#"><img  title="à¹„à¸—à¸¢" alt="à¹„à¸—à¸¢" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/th.png"></a></li>	          
		          <li><a onclick="document.getElementById('chooselan').value='vi';document.getElementById('loginform').submit();" href="#"><img title="Tiáº¿ng Viá»‡t" alt="Tiáº¿ng Viá»‡t" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/vn.png"></a></li>
		     	  <li><a onclick="document.getElementById('chooselan').value='zh_cn';document.getElementById('loginform').submit();" href="#"><img  title="ä¸­æ–‡ç®€ä½“" alt="ä¸­æ–‡ç®€ä½“" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/cn.png"></a></li>	          
		          <li><a onclick="document.getElementById('chooselan').value='zh_hk';document.getElementById('loginform').submit();" href="#"><img  title="ä¸­æ–‡ç¹�ä½“" alt="ä¸­æ–‡ç¹�ä½“" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/hk.png"></a></li>
		          <li><a onclick="document.getElementById('chooselan').value='kr';document.getElementById('loginform').submit();" href="#"><img  title="ä¸­æ–‡ç¹�ä½“" alt="ä¸­æ–‡ç¹�ä½“" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/kr.png"></a></li>
		     	</ul>
		    </div>
			</form>
			<br/><br/>
			</center>
			<?php 
			$lang = isset($_POST['chooselan'])? $_POST['chooselan'] : "en";
			Yii::app()->session['chooselanaguage'] = $lang;
			Yii::app()->language=$lang;
			
			$form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>Yii::app()->baseUrl .'/index.php?r=Login&lang='. $lang,
				'enableClientValidation'=>true,
				'clientOptions'=>array('validateOnSubmit'=>true,),
			)); ?>
			<p class="note"></p>
			<div id="locked_error"></div>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,Yii::t('login','LBL_USER_NAME')); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<?php echo $form->textField($model,'username',array('onkeydown'=>'if (event.keyCode == 13){return validation();}','class'=>'m-wrap placeholder-no-fix','placeholder'=>Yii::t('login','LBL_USER_NAME') )); ?>
						<!-- <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/> -->
					</div>
					<?php echo $form->error($model,'username',array('class'=>'alert alert-error')); ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,Yii::t('login','LBL_PASSWORD')); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<!--<input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>  -->
						<?php echo $form->passwordField($model,'password',array('onkeydown'=>'if (event.keyCode == 13){return validation();}','class'=>'m-wrap placeholder-no-fix','placeholder'=>Yii::t('login','LBL_PASSWORD') )); ?>
					</div>
					<?php echo $form->error($model,'password',array('class'=>'alert alert-error')); ?>
				</div>
			</div>
			<?php if(CCaptcha::checkRequirements()): ?>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,Yii::t('login','LBL_CODE_CAPTCHA')); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-key"></i>
						<!--  <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Code" name="password"/>-->
						<?php echo $form->textField($model,'verifyCode',array('onkeydown'=>'if (event.keyCode == 13){return validation();}','class'=>'m-wrap small','placeholder'=>Yii::t('login','LBL_CODE_CAPTCHA') )); ?>
						<?php $this->widget('CCaptcha', array('buttonLabel'=>'','clickableImage'=>true)); ?>
					</div>
					<?php echo $form->error($model,'verifyCode',array('class'=>'alert alert-error')); ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="form-actions">
				<?php //echo CHtml::button(Yii::t('login','LBL_LOGIN_BUTTON'),array('onclick'=>'return validation();','class'=>'btn red pull-right')); ?>
	       		<button type="button" class="btn red pull-right" onclick="return validation();" >
				<?php echo Yii::t('login','LBL_LOGIN_BUTTON')?> <i class="m-icon-swapright m-icon-white"></i>
				</button> 
			</div>
			<!--  <div style="position: absolute; width: 130px;height: 130px; margin-left: -115px;margin-top: -70px;"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/gaming.png"></div>-->
		</div>
		<?php $this->endWidget(); ?>
		</div>
		<!-- END LOGIN FORM -->        
		
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright" style="width: 300px;">
		Copyright <?php echo date('Y'); ?>. CodeEric IT Solution All rights reserved.
		<br/>
		Version 1.0.0
	</div>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->  
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-validation/dist_/jquery.validate.min.js" type="text/javascript"></script>	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/select2/select2.min.js"></script>     
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/app.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/login.js" type="text/javascript"></script> 
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script>
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();
		
		 
		
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>