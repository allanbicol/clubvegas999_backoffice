<?php

/**
 * This is the model class for table "tbl_announcement".
 *
 * The followings are the available columns in table 'tbl_announcement':
 * @property integer $id
 * @property string $contentEN
 * @property string $contentTH
 * @property string $contentVN
 * @property string $contentCN
 * @property string $contentTW
 * @property integer$status
 */
class Announcement extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Announcement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_announcement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		
		/* Do not add id field in the require rule otherwise you can't save it */
		/* array('id, contentEN, contentTH, contentVN, contentCN, contentTW, content_status', 'required') */
		
		return array(
			array('contentEN, contentTH, contentVN, contentCN, contentHK, content_status', 'required'),
			array('content_status', 'numerical','integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('contentEN, contentTH, contentVN, contentCN, contentHK, content_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contentEN' => 'Content En',
			'contentTH' => 'Content Th',
			'contentVN' => 'Content Vn',
			'contentCN' => 'Content Cn',
			'contentHK' => 'Content Hk',
			'contentStatus' 	=> 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contentEN',$this->contentEN,true);
		$criteria->compare('contentTH',$this->contentTH,true);
		$criteria->compare('contentVN',$this->contentVN,true);
		$criteria->compare('contentCN',$this->contentCN,true);
		$criteria->compare('contentHK',$this->contentHK,true);
		$criteria->compare('contentStatus',$this->contentStatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getAnnouncementList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT id,contentEN, IF ((`content_status` = 1), 'published', 'unpublish') AS `content_status`, '' as operation FROM tbl_announcement ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	public function getRecordCount()
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_announcement");
		$rows = $command->query();
		return $rows;
	}
	
	public function getStatusName($announcementID)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT content_status AS `contentStatus` FROM tbl_announcement WHERE id=:id");
		$command->bindParam(":id",$announcementID,PDO::PARAM_STR);
		$row = $command->queryScalar();
		return $row;
	}
	
	public function unpublishAll()
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("UPDATE tbl_announcement SET content_status=0");
		$command->execute();
	}
	
	public function updateAllStatus($announcementID)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("UPDATE tbl_announcement SET content_status=0 WHERE id <>:id");
		$command->bindParam(":id",$announcementID,PDO::PARAM_INT);
		$command->execute();
	}
}