//active menu color
document.getElementById('agentHeader').className="start active";
document.getElementById('agentHeader').className="start active";
document.getElementById('mnu_agent_winloss_v1').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Agent Winloss Version 1</a></li>");

/**
 * @todo export to excel
 * @author leokarl
 * @since 2013-01-24
 */
function exportWinLossToExcel(filename, html_table){
	var game = $('#cmbGame')[0].value;
	var currency = $('#cmbCurrency')[0].value;
	testChecked=0;
	if (document.getElementById("chkTestCurrency").checked==true){
		 testChecked=1;
	}
	var dateFrom	=	$('#dtFrom')[0].value + ' ' + $('#cmbTime1')[0].value + ':00:00';
	var dateTo	=	$('#dtTo')[0].value + ' ' + $('#cmbTime2')[0].value + ':59:59';
	var accountId = $('#txtAccountID')[0].value;
	var filenameSplit=filename.split("_");
	
	if (document.getElementById('chkVIG').checked==true && document.getElementById('chkHTV').checked!=true && document.getElementById('chkSAVAN').checked!=true){
		document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
	}else if (document.getElementById('chkVIG').checked!=true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked!=true){
		document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
	}else if (document.getElementById('chkVIG').checked!=true && document.getElementById('chkHTV').checked!=true && document.getElementById('chkSAVAN').checked==true){
		document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
	}else if (document.getElementById('chkVIG').checked==true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked!=true){
		if(filenameSplit[0]=='COSTAVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}else if (document.getElementById('chkVIG').checked==true && document.getElementById('chkHTV').checked!=true && document.getElementById('chkSAVAN').checked==true){
		if(filenameSplit[0]=='COSTAVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}else if (document.getElementById('chkVIG').checked!=true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked==true){
		if(filenameSplit[0]=='HATIENVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}else if (document.getElementById('chkVIG').checked==true && document.getElementById('chkHTV').checked==true && document.getElementById('chkSAVAN').checked==true){
		if(filenameSplit[0]=='COSTAVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else if (filenameSplit[0]=='HATIENVEGAS999'){
			document.forms[1].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}else{
			document.forms[1].txtParams.value=$('.ui-paging-info')[2].innerHTML+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
		}
	}
	
	document.forms[1].csvBuffer.value = ""; // clear buffer
	document.forms[1].csvBuffer.value = html_table;
    document.forms[1].method = 'POST';
    document.forms[1].action = baseURL + '/index.php?r=Agent/AgentWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
    document.forms[1].target = '_top';
    document.forms[1].submit();
}

function exportAllWinLossToExcel(filename,url){
    $.ajax({	
		 url:url, 
		 async:true,
		 success: function(result) {
				var data =result.split("<BREAK>");
		
				var game = $('#cmbGame')[0].value;
				var currency = $('#cmbCurrency')[0].value;
				testChecked=0;
				if (document.getElementById("chkTestCurrency").checked==true){
					 testChecked=1;
				}
				var dateFrom	=	$('#dtFrom')[0].value + ' ' + $('#cmbTime1')[0].value + ':00:00';
				var dateTo	=	$('#dtTo')[0].value + ' ' + $('#cmbTime2')[0].value + ':59:59';
				var accountId = $('#txtAccountID')[0].value;

				document.forms[1].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+game+'#'+currency+'#'+testChecked+'#'+dateFrom+'#'+dateTo+'#'+accountId;
				
				document.forms[1].csvBuffer.value = ""; // clear buffer
				document.forms[1].csvBuffer.value = data[2]+''+data[0];
			    document.forms[1].method = 'POST';
			    document.forms[1].action = baseURL + '/index.php?r=Agent/AgentWinLoss/ExportToExcel&filename=' + filename;  // send it to server which will open this contents in excel file
			    document.forms[1].target = '_top';
			    document.forms[1].submit();
		 },
		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 }});
}
/**
 * @todo winloss column header
 * @author leokarl
 * @since 2012-12-15
 * @param string account_id 
 * @returns column header
 */
function getWinLossColumnHeaderByAccountId(account_id){
	if(account_id.length == 0){
    	return lblMainCompany;
    }else if(account_id.length == 2){
    	return lblSubCompany;
	}else if(account_id.length == 4){
		return lblSeniorMasterAgent;
	}else if(account_id.length == 6){
		return lblMasterAgent;
	}else if(account_id.length == 8){
		return lblAgent;
	}
}

/**
 * @todo winloss column header
 * @author leokarl
 * @since 2012-12-15
 * @param string account_id 
 * @returns agent column header
 */
function getAgentColumnHeaderByAccountId(account_id){
	if(account_id.length==0){
    	return lblSubCompanyTotal;
    }else if(account_id.length==2){
    	return lblSeniorMasterAgentTotal;
	}else if(account_id.length==4){
		return lblMasterAgentTotal;
	}else if(account_id.length==6){
		return lblAgentTotal;
	}
}


function checkIt(evt,id) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    var strText=document.getElementById(id).value;
    var countStr=(strText.split(".").length - 1);
    if (charCode==46){
	    //allow only one period
    	if(countStr > 0){
	    	return false;
	    }
	}
    if (charCode > 31 && ((charCode < 48 && charCode!=46) || charCode > 57)) {
        status = "This field accepts numbers only.";
        return false;
    }
    status = "";
    return true;
}

function onMouseOutDiv(id){
	document.getElementById('id').addEventListener('mouseout',onMouseOut,true);
}
function onMouseOut(event) {
    //this is the original element the event handler was assigned to
        e = event.toElement || event.relatedTarget;
        if (e.parentNode == this || e == this) {
           return;
        }
        hideDiv("dvSharingCommission");
    // handle mouse event here!
}
$(document).ready(function() { 
	var div1Tag = document.createElement("div"); 
    div1Tag.id = "dvSharingCommission"; 
    div1Tag.setAttribute("align", "center"); 
    div1Tag.setAttribute("onmouseout","javascript: document.getElementById('dvSharingCommission').addEventListener('mouseout',onMouseOut,true);");
    div1Tag.style.margin = "0px auto"; 
    div1Tag.className = "dynamicDiv"; 
    document.body.appendChild(div1Tag);
    hideDiv('dvSharingCommission');
}); 

function hideDiv(divID) { 
	if (document.getElementById) { 
	 	document.getElementById(divID).style.visibility = 'hidden'; 
	} 
}
	
function showDiv(divID,objXY,x,y) { 
	//get link position
	for (var lx=0, ly=0;
	objXY != null;
    lx += objXY.offsetLeft, ly += objXY.offsetTop, objXY = objXY.offsetParent);
    
    //set div new position
	document.getElementById(divID).style.left= (x-65) +"px";
	document.getElementById(divID).style.top= (y+30) +"px";
	document.getElementById(divID).style.visibility = 'visible'; 
}

jQuery(document).ready(function() {
    jQuery("#dtFrom").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });

    jQuery("#dtTo").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});

function hideBettingDetails_savan(){
	//start: show parent grids
    document.getElementById('qry_savan_vegas_result').style.display='block';
    document.getElementById('qry_savan_vegas_result_details').style.display='block';
    //end: show parent grids
    //start: clear betting details
    document.getElementById('qry_savan_vegas_betting_details').innerHTML='';
    document.getElementById('qry_savan_vegas_betting_details_summary_footer').style.display='none';
    //end: clear betting details
}
function clearContainer(){
	document.getElementById('qry_savan_vegas_result_details').innerHTML='';
}


function savanTotalConversion(currency){
	var grid = jQuery("#lstTotal_savan");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#lstTotal_savan").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	var total_stake=grid.getCell(x, 'total_stake');
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var win_loss=grid.getCell(x, 'win_loss');
	    	var commission=grid.getCell(x, 'commission');
	    	var company=grid.getCell(x, 'company');
	    	var total=grid.getCell(x, 'total');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 1, fltRate[0]);
	    	grid.jqGrid('setCell', x, 2, total_stake_convert);
	    	grid.jqGrid('setCell', x, 3, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 4, win_loss_convert);
	    	grid.jqGrid('setCell', x, 5, commission_convert);
	    	grid.jqGrid('setCell', x, 6, total_convert);
	    	grid.jqGrid('setCell', x, 7, company_convert);
	    	grid.jqGrid('setCell', x, 8, fltRate[1]);
	    	
	     	x++;
	    }

	    var total_stake_sum=grid.jqGrid('getCol', 'total_stake', false, 'sum');
	    var valid_bet_sum= grid.jqGrid('getCol', 'valid_bet', false, 'sum');
	    var win_loss_sum=grid.jqGrid('getCol', 'win_loss', false, 'sum');
	    var commission_sum=grid.jqGrid('getCol', 'commission', false, 'sum');
	    var company_sum=grid.jqGrid('getCol', 'company', false, 'sum');
	    var total_sum=grid.jqGrid('getCol', 'total', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake: total_stake_sum,valid_bet: valid_bet_sum,win_loss: win_loss_sum,commission: commission_sum,total: total_sum, company: company_sum});
	}else{
		grid.trigger("reloadGrid");
		grid.jqGrid('footerData','set', {total_stake: '',valid_bet: '',win_loss: '',commission: '',total: '',company: ''});
	}
}
function savanWinLossConversion(currency){
	var grid = jQuery("#lstSAVAN1");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#lstSAVAN1").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
		      
	    	//var bet_count=grid.getCell(x, 'bet_count');
	    	var average_bet=grid.getCell(x, 'average_bet');
	    	var total_stake=grid.getCell(x, 'total_stake');
	    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var total=grid.getCell(x, 'total');
	    	var mc_win_loss=grid.getCell(x, 'mc_win_loss');
	    	var mc_comm=grid.getCell(x, 'mc_comm');
	    	var mc_total=grid.getCell(x, 'mc_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	//var bet_count_convert=((parseFloat(bet_count)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_win_loss_convert=((parseFloat(mc_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_comm_convert=((parseFloat(mc_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var mc_total_convert=((parseFloat(mc_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	//grid.jqGrid('setCell', x, 5, bet_count_convert);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, total_stake_convert);
	    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 9, total_convert);
	    	grid.jqGrid('setCell', x, 10, mc_win_loss_convert);
	    	grid.jqGrid('setCell', x, 11, mc_comm_convert);
	    	grid.jqGrid('setCell', x, 12, mc_total_convert);
	    	grid.jqGrid('setCell', x, 13, company_convert);
	    	grid.jqGrid('setCell', x, 14, fltRate[1]);
	     	x++;
	    }//costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("savan_sum_data");
	    var i=0;
	    var xrate=8;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=9;
			}
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
	
	
}
function savanMemberWinLossConversion(currency){
	var grid = jQuery("#lstSAVAN1");
	if(currency!='--'){
		var fltRate=currency.split('-');
		var noRow=$("#lstSAVAN1").getGridParam("reccount");
	    var x=1;
	    while (x<=noRow)
	    {
	    	
	    	var average_bet=grid.getCell(x, 'average_bet');
		    	var total_stake=grid.getCell(x, 'total_stake');
		    	var valid_bet=grid.getCell(x, 'valid_bet');
	    	var win_loss=grid.getCell(x, 'win_loss');
	    	var commission=grid.getCell(x, 'commission');
	    	var total=grid.getCell(x, 'total');
	    	var balance=grid.getCell(x, 'balance');
	    	var agt_win_loss=grid.getCell(x, 'agt_win_loss');
	    	var agt_comm=grid.getCell(x, 'agt_comm');
	    	var agt_total=grid.getCell(x, 'agt_total');
	    	var company=grid.getCell(x, 'company');
	    	var exchange_rate=grid.getCell(x, 'exchange_rate');

	    	
	    	var average_bet_convert=((parseFloat(average_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_stake_convert=((parseFloat(total_stake)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var valid_bet_convert=((parseFloat(valid_bet)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var win_loss_convert=((parseFloat(win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var commission_convert=((parseFloat(commission)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var total_convert=((parseFloat(total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var balance_convert=((parseFloat(balance)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_win_loss_convert=((parseFloat(agt_win_loss)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_comm_convert=((parseFloat(agt_comm)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var agt_total_convert=((parseFloat(agt_total)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	var company_convert=((parseFloat(company)/parseFloat(exchange_rate)) * parseFloat(fltRate[1]));
	    	
	    	grid.jqGrid('setCell', x, 3, fltRate[0]);
	    	grid.jqGrid('setCell', x, 6, average_bet_convert);
	    	grid.jqGrid('setCell', x, 7, total_stake_convert);
	    	grid.jqGrid('setCell', x, 8, valid_bet_convert);
	    	grid.jqGrid('setCell', x, 9, win_loss_convert);
	    	grid.jqGrid('setCell', x, 10, commission_convert);
	    	grid.jqGrid('setCell', x, 11, total_convert);
	    	grid.jqGrid('setCell', x, 12, balance_convert);
	    	grid.jqGrid('setCell', x, 13, agt_win_loss_convert);
	    	grid.jqGrid('setCell', x, 14, agt_comm_convert);
	    	grid.jqGrid('setCell', x, 15, agt_total_convert);
	    	grid.jqGrid('setCell', x, 16, company_convert);
	    	grid.jqGrid('setCell', x, 17, fltRate[1]);
	     	x++;
	    }
	    //costa_exchange_rate
	    //var labels = document.getElementsByTagName("label");
	    var labels = document.getElementsByClassName("savan_sum_data");
	    var i=0;
	    var xrate=11;
		for (i=0; i < labels.length; i++) {
			if(i>xrate){
				xrate+=12;
			}
			labels[i].innerHTML=((parseFloat(labels[i].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))/parseFloat(labels[xrate].innerHTML.replace(',','').replace(',','').replace(',','').replace(',',''))) * parseFloat(fltRate[1])).toFixed(2);
		}
	}else{
		grid.trigger("reloadGrid");
	}
}
function copyVIGValue(val){
	
	//document.getElementById('txtVIGsharing').value=val;
}

function showSAVANBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency){
	document.getElementById('lblSAVANBettingSummaryPlayerID').innerHTML=strAccountID;
	document.getElementById('qry_savan_vegas_betting_details_summary_footer').style.display='block';
	//Current Page
	var grid = jQuery("#list6");
	var noRow=$("#list6").getGridParam("reccount");
    var x=1;
    var bet_amount=0;
    var amount_wagers=0;
	var win_loss=0;
	var commission=0;
	var total=0;
	var balance=0;
    while (x<=noRow)
    {
    	bet_amount+=parseFloat(grid.getCell(x, 'bet_amount'));
    	amount_wagers+=parseFloat(grid.getCell(x, 'amount_wagers'));
    	//win_loss+=parseFloat(grid.getCell(x, 'win_loss'));
    	win_loss+=parseFloat(grid.getCell(x, 'win_loss').substring(grid.getCell(x, 'win_loss').indexOf('>')+1,grid.getCell(x, 'win_loss').indexOf('</')));
    	commission+=parseFloat(grid.getCell(x, 'commission'));
    	total+=parseFloat(grid.getCell(x, 'total'));
    	if(x==1){
    		balance+=parseFloat(grid.getCell(x, 'balance'));
    	}
     	x++;
    }
    document.getElementById('lblCPNoOfBet_savan').innerHTML=x-1;
    document.getElementById('lblCPBetAmount_savan').innerHTML=parseFloat(bet_amount).toFixed(2);
    document.getElementById('lblCPValidBet_savan').innerHTML=parseFloat(amount_wagers).toFixed(2);
    document.getElementById('lblCPWinLoss_savan').innerHTML=parseFloat(win_loss).toFixed(2);
    document.getElementById('lblCPCommission_savan').innerHTML=parseFloat(commission).toFixed(2);
    document.getElementById('lblCPTotal_savan').innerHTML=parseFloat(total).toFixed(2);
    document.getElementById('lblCPBalance_savan').innerHTML=parseFloat(balance).toFixed(2);
    if(parseFloat(bet_amount)<0){document.getElementById('lblCPBetAmount_savan').style.color='red';}else{document.getElementById('lblCPBetAmount_savan').style.color='black';}
    if(parseFloat(amount_wagers)<0){document.getElementById('lblCPValidBet_savan').style.color='red';}else{document.getElementById('lblCPValidBet_savan').style.color='black';}
    if(parseFloat(win_loss)<0){document.getElementById('lblCPWinLoss_savan').style.color='red';}else{document.getElementById('lblCPWinLoss_savan').style.color='black';}
    if(parseFloat(commission)<0){document.getElementById('lblCPCommission_savan').style.color='red';}else{document.getElementById('lblCPCommission_savan').style.color='black';}
    if(parseFloat(total)<0){document.getElementById('lblCPTotal_savan').style.color='red';}else{document.getElementById('lblCPTotal_savan').style.color='black';}
    if(parseFloat(balance)<0){document.getElementById('lblCPBalance_savan').style.color='red';}else{document.getElementById('lblCPBalance_savan').style.color='black';}
	//All Pages
	
    jQuery.ajax({
		url: urlSAVANWinLossBettingDetailsSummaryFooter +'&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + '&testCurrency=' + testCurrency,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			var fltTotal = data.split(';');
			document.getElementById('lblAPNoOfBet_savan').innerHTML=fltTotal[0];
		    document.getElementById('lblAPBetAmount_savan').innerHTML=fltTotal[1];
		    document.getElementById('lblAPValidBet_savan').innerHTML=fltTotal[2];
		    document.getElementById('lblAPWinLoss_savan').innerHTML=fltTotal[3];
		    document.getElementById('lblAPCommission_savan').innerHTML=fltTotal[4];
		    document.getElementById('lblAPTotal_savan').innerHTML=fltTotal[5];
		    document.getElementById('lblAPBalance_savan').innerHTML=fltTotal[6];
		    if(parseFloat(fltTotal[1])<0){document.getElementById('lblAPBetAmount_savan').style.color='red';}else{document.getElementById('lblAPBetAmount_savan').style.color='black';}
		    if(parseFloat(fltTotal[2])<0){document.getElementById('lblAPValidBet_savan').style.color='red';}else{document.getElementById('lblAPValidBet_savan').style.color='black';}
		    if(parseFloat(fltTotal[3])<0){document.getElementById('lblAPWinLoss_savan').style.color='red';}else{document.getElementById('lblAPWinLoss_savan').style.color='black';}
		    if(parseFloat(fltTotal[4])<0){document.getElementById('lblAPCommission_savan').style.color='red';}else{document.getElementById('lblAPCommission_savan').style.color='black';}
		    if(parseFloat(fltTotal[5])<0){document.getElementById('lblAPTotal_savan').style.color='red';}else{document.getElementById('lblAPTotal_savan').style.color='black';}
		    if(parseFloat(fltTotal[6])<0){document.getElementById('lblAPBalance_savan').style.color='red';}else{document.getElementById('lblAPBalance_savan').style.color='black';}
    	}
	});
}

$(document).ready(function() { 
	var htmTag = document.createElement("div"); 
	htmTag.id = "dvWinLossResultDetails"; 
	htmTag.setAttribute("align", "center"); 
	htmTag.setAttribute("onmouseout","javascript: document.getElementById('dvWinLossResultDetails').addEventListener('mouseout',onMouseOutWinLossResultDetails,true);");
	htmTag.style.margin = "0px auto"; 
	htmTag.className = "dynamicDiv"; 
    document.body.appendChild(htmTag);
    hideDiv('dvWinLossResultDetails');
    
    var htmTag1 = document.createElement("div"); 
	htmTag1.id = "dvResultDetails"; 
	htmTag1.setAttribute("align", "center"); 
	htmTag1.setAttribute("onmouseout","javascript: document.getElementById('dvResultDetails').addEventListener('mouseout',onMouseOutResultDetails,true);");
	htmTag1.style.margin = "0px auto"; 
	htmTag1.className = "dynamicDiv"; 
    document.body.appendChild(htmTag1);
    hideDiv('dvResultDetails');
}); 

function onMouseOutWinLossResultDetails(event) {
    e = event.toElement || event.relatedTarget;
    if (e.parentNode == this || e == this) {
       return;
    }
    hideDiv("dvWinLossResultDetails");
}

function onMouseOutResultDetails(event) {
    e = event.toElement || event.relatedTarget;
    if (e.parentNode == this || e == this) {
       return;
    }
    hideDiv("dvResultDetails");
}

function winLossArrow_savan(cellvalue, options, rowObject) {
	var imgsrc=" <img class='savanwinlossarrow' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"dvWinLossResultDetails\");' src='" + urlWinLossArrow + "/images/winlossarrow.png'/>";
	if(parseFloat(cellvalue)<0){
		return "<font color='red'>" + cellvalue + "</font>" + imgsrc;
	}else{
		return "<font color='black'>" + cellvalue + "</font>"+ imgsrc;
	}
	
}

function resultArrow_savan(cellvalue, options, rowObject){
	var imgsrc=" <img class='savanresultarrow' alt='"+ rowObject[1] +"' onmouseout='hideDiv(\"dvResultDetails\");' src='" + urlWinLossArrow + "/images/winlossarrow.png'/>";
	/* Baccarat (3#1#1#1#^9#9#0#9#9#0#)
	 　　 1(Banker Win),2(Player Win),3(Tie Win)　 # 0(No Banker Pair),1(Banker pair) # 0(No Player Pair),1(Player Pair)　 # 1(Small),2(Big) #
	 　　 ^
	 	Player poker one # Player poker two #  Player poker three　# Banker poker one # Banker poker two #  Banker poker three
	*/
	if(rowObject[7]=='Baccarat'){
		var strData=(cellvalue.substring(0,cellvalue.indexOf('^')-1)).split('#');
		var strResult='';
		// 1(Banker Win),2(Player Win),3(Tie Win)
		if(strData[0]==1){
			strResult='<font color="red">Banker Win</font>';
		}else if(strData[0]==2){
			strResult='<font color="blue">Player Win</font>';
		}else if(strData[0]==3){
			strResult='<font color="green">Tie</font>';
		}
		// 0(No Banker Pair),1(Banker pair)
		if(strData[1]==1){
			strResult+=', <font color="red">B Pair</font>';
		}
		// 0(No Player Pair),1(Player Pair)
		if(strData[2]==1){
			strResult+=', <font color="blue">P Pair</font>';
		}
		// 1(Small),2(Big)
		if(strData[3]==1){
			strResult+=', <font color="blue">Small</font>';
		}else if(strData[3]==2){
			strResult+=', <font color="red">Big</font>';
		}
		return strResult + imgsrc;
	}
	/* Dragon Tiger (1#^25#36#)
		1(Dragon Win ),2(Tiger Win),3(Tie Win)　 #
		^　
		Dragon poker  # Tiger poker  #
	*/
	else if(rowObject[7]=='Dragontiger'){
		var strData=cellvalue.substring(0,cellvalue.indexOf('^')-1);
		var strResult='';
		if(strData[0]==1){
			strResult='<font color="red">Dragon Win</font>';
		}else if(strData[0]==2){
			strResult='<font color="blue">Tiger Win</font>';
		}else if(strData[0]==3){
			strResult='<font color="green">Tie</font>';
		}
		return strResult + imgsrc;
	}
	/* Roulette (2)
		2
	*/
	else if(rowObject[7]=='Roulette'){
		return cellvalue + imgsrc;
	}
}
function showWinLossResultDetails_savan(winLossId,obj,x,y){
	document.getElementById('dvWinLossResultDetails').innerHTML="<b>Loading...</b>";
	showDiv('dvWinLossResultDetails',obj,x,y);	
	jQuery.ajax({
		url: urlSAVANWinLossResultDetails + '&winlossid=' + winLossId,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			//document.getElementById('dvWinLossResultDetails').innerHTML=data;
			$("#winloss_bet_result_detail").html(data);
			$("#winloss_bet_result_detail").dialog("open"); return false;
		}
	});
	
}

function showResultDetails_savan(winLossId,obj,x,y){
	document.getElementById('dvResultDetails').innerHTML="<b>Loading image...</b>";
	showDiv('dvResultDetails',obj,x + 420,y + 240);	
	jQuery.ajax({
		url: baseURL + '/index.php?r=Agent/AgentWinLoss/SavanVegasResultDetails&winlossid=' + winLossId,
		method: 'POST',
		data: {},
		context: '',
		success: function(data){
			//document.getElementById('dvResultDetails').innerHTML="<img class='resultImage' src='" + data + "'/>";
			//showDiv('dvResultDetails',obj,x,y);
			$("#winloss_result_img").html("<img class='resultImage' src='" + data + "'/>");
			$("#winloss_result_img").dialog("open"); return false;
		}
	});
	
}

/**
 * @todo Savan winloss total
 * @author leokarl
 * @since 2012-12-17
 * @param dtFrom
 * @param dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('qry_savan_vegas_total').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'lstTotal_savan'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_total").appendChild(divTag);
	var grid=jQuery("#lstTotal_savan");
	var col_group_header = getWinLossColumnHeaderByAccountId(accountID);
        
	grid.jqGrid({ 
		url: urlSAVANTotal + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + '&accountID=' + accountID + 
			'&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [lblNumber, lblCurrency, lblTotalStake, lblValidStake, lblWinLoss, lblCommission, lblTotal, lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25,sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; }},
			  {name: 'currency_name', index: 'currency_name', width: 80,sortable: false,},
			  {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';}},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'commission', index: 'commission', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';}},
		      {name: 'total', index: 'total', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';}},
		      {name: 'company', index: 'company', width: 100,sortable: false, formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';}},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10,sortable: false,hidden: true},
		],
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        rownumbers: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: captionSAVANWinLossTotal,
        footerrow:true,
	});
	$("#lstTotal_savan").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + col_group_header + '</label>'},
		  ]
	});
}


/**
 * @todo savan winloss
 * @author leokarl
 * @since 2012-12-17
 * @param dtFrom
 * @param dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
	var currency=strCurrency;
	document.getElementById('qry_savan_vegas_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'lstSAVAN1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pgrSAVAN1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_result").appendChild(divTag1);
    
	var grid=jQuery("#lstSAVAN1");

    var col_agent = getAgentColumnHeaderByAccountId(accountID);
    var col_group_header = getWinLossColumnHeaderByAccountId(accountID);
    var fltBetCount=0;
    var fltValidBet=0;
    //var strCurrency='';
    function myAvg(val, name, record)
    {
        if (strCurrency==record['currency_name']){
        	fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}else{
			fltBetCount=0;
	        fltValidBet=0;
	        fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}
    	strCurrency=record['currency_name'];
    	return parseFloat(fltValidBet/fltBetCount);
    }
    function savan_xrate(val,name,record){
		return record['exchange_rate'];
	}
	grid.jqGrid({ 
		url: urlSAVANSummaryWinLoss + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [lblNumber, lblAccountId, lblAccountName, lblCurrency, lblSharingCommission, lblBetCount, lblAverageBet, lblTotalStake, lblValidStake,
		    		col_agent, lblWinLoss, lblCommission, lblTotal, lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 80,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      //{name: 'mc_bonus', index: 'mc_comm', width: 70,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #f7f7c8"' : 'style="color:red; background-color: #f7f7c8"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 10, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".savan_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		
		    //Lower level
		    $(".account_id_savan").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
			    	savanSubAgent(account_id_val);
	    			document.getElementById('lblAgentID_savan').innerHTML=account_id_val;
	    			//document.getElementById('qry_costavegas_total').innerHTML='';
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('dvSharingCommission');
			});
			    
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: captionSAVANWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#pgrSAVAN1',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#lstSAVAN1").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + col_group_header + '</label>'},
		  ]
	});
	$('#lstSAVAN1').jqGrid('navGrid', '#pgrSAVAN1', {edit: false, add: false, del:false, search: false});
	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
        caption:"Export Current PAge to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('qry_savan_vegas_total');
        	var table2= document.getElementById('qry_savan_vegas_result');
			exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
        caption:"Export All Pages to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var url=urlExportAllSAVANSummaryWinLoss + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + currency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
			exportAllWinLossToExcel('SAVANVEGAS999_WIN_LOSS_',url);
       }, 
        position:"last"
    });   
}

/**
 * @todo savan member winloss
 * @author leokarl
 * @since 2012-12-17
 * @param dtFrom
 * @param dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('qry_savan_vegas_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'lstSAVAN1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pgrSAVAN1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_result").appendChild(divTag1);
    
	var grid=jQuery("#lstSAVAN1");

	var fltBetCount=0;
    var fltValidBet=0;
    //var strCurrency='';
    function myAvg(val, name, record)
    {
        if (strCurrency==record['currency_name']){
        	fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}else{
			fltBetCount=0;
	        fltValidBet=0;
	        fltBetCount+=parseFloat(record['bet_count']);
        	fltValidBet+=parseFloat(record['valid_bet']);
		}
    	strCurrency=record['currency_name'];
    	return parseFloat(fltValidBet/fltBetCount);
    }
    function savan_xrate(val,name,record){
		return record['exchange_rate'];
	}
	grid.jqGrid({ 
		url: urlSAVANSummaryMemberWinLoss + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [lblNumber, lblAccountId, lblAccountName, lblCurrency, lblSharingCommission, lblBetCount, lblAverageBet, lblTotalStake,
		    		lblValidStake, lblWinLoss, lblCommission, lblTotal, lblBalance, lblWinLoss, lblCommission, lblTotal, lblCompany, 'exchange_rate'],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 100,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:myAvg,formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      //{name: 'bonus', index: 'bonus', width: 50,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'total', index: 'total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red;';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		      {name: 'exchange_rate', index: 'exchange_rate', width: 100, hidden: true,summaryType: savan_xrate,summaryTpl:'<label class="savan_sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
    		$(".savan_sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    $(".account_id_savan").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
				    //start: hide parent grids
				    document.getElementById('qry_savan_vegas_result').style.display='none';
				    document.getElementById('qry_savan_vegas_result_details').style.display='none';
				    //end: hide parent grids
			    	var dtfrom=document.getElementById('lblDateFrom_savan').innerHTML;
        			var dtto=document.getElementById('lblDateTo_savan').innerHTML;
        			tableMemberWinLoss_savan_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
        			document.getElementById('lblBettingDetailAgentID_savan').innerHTML=account_id_val;
        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
        		var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('dvSharingCommission');
			});
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: captionSAVANMemberWinLoss,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#pgrSAVAN1',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#lstSAVAN1").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + lblAgent + '</label>'},
		  ]
	});
	$('#lstSAVAN1').jqGrid('navGrid', '#pgrSAVAN1', {edit: false, add: false, del:false, search: false});
	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
        caption:"Export Current to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('qry_savan_vegas_total');
        	var table2= document.getElementById('qry_savan_vegas_result');
			exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_', table1.outerHTML + ' ' + table2.outerHTML);
       }, 
        position:"last"
    });   
	$("#lstSAVAN1").jqGrid('navButtonAdd','#pgrSAVAN1',{
        caption:"Export All Pages to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var url= urlExportAllSAVANSummaryMemberWinLoss + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
			exportAllWinLossToExcel('SAVANVEGAS999_WIN_LOSS_', url);
       }, 
        position:"last"
    });  
}

/**
 * @todo savan winloss details
 * @author leokarl
 * @since 2012-12-17
 * @param dtFrom
 * @param dtTo
 * @param strCurrency
 * @param intGame
 * @param accountID
 * @param testCurrency
 */
function tableSAVANWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency){
	document.getElementById('qry_savan_vegas_result_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'lstSAVAN2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_result_details").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pgrSAVAN2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_result_details").appendChild(divTag1);
    
	var grid=jQuery("#lstSAVAN2");

	grid.jqGrid({ 
		url: urlSAVANWinLossDetails + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value, 
		datatype: 'json',
		mtype: 'POST',
		height: 'auto',
		colNames: [lblNumber, lblAccountId, lblAccountName, lblCurrency, lblSharingCommission, lblBetCount, lblAverageBet, lblTotalStake,
	    		lblValidStake, lblWinLoss, lblCommission, lblTotal, lblBalance, lblWinLoss, lblCommission, lblTotal, lblWinLoss, 
	    		lblCommission, lblTotal, lblWinLoss, lblCommission, lblTotal, lblWinLoss, lblCommission, lblTotal, lblWinLoss, lblCommission,
	    		lblTotal, lblCompany],
		colModel: [
			  {name: 'row', index: 'row', width: 25, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'account_id', index: 'account_id',classes:'detail_account_id_savan',align: 'center', width: 100, sortable: false,cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:blue; text-decoration: underline; cusor:hand;cursor:pointer;"' : 'style="color:blue; text-decoration: underline;cusor:hand;cursor:pointer;"';}},
		      {name: 'account_name', index: 'account_name', width: 90, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60, sortable: false},
		      {name: 'sharing', index: 'sharing', width: 75, sortable: false, align: "center", title: false},
		      {name: 'bet_count', index: 'bet_count', width: 100,sortable: false,summaryType:'sum',summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'average_bet', index: 'average_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'total_stake', index: 'total_stake', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'valid_bet', index: 'valid_bet', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black"' : 'style="color:red"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_win_loss', index: 'mem_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_comm', index: 'mem_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'mem_bonus', index: 'mem_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mem_total', index: 'mem_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_win_loss', index: 'agt_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_comm', index: 'agt_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'agt_bonus', index: 'agt_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'agt_total', index: 'agt_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_win_loss', index: 'ma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_comm', index: 'ma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'ma_bonus', index: 'ma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'ma_total', index: 'ma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_win_loss', index: 'sma_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_comm', index: 'sma_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'sma_bonus', index: 'sma_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sma_total', index: 'sma_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_win_loss', index: 'sc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_comm', index: 'sc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'sc_bonus', index: 'sc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'sc_total', index: 'sc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_win_loss', index: 'mc_win_loss', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_comm', index: 'mc_comm', width: 100,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      //{name: 'mc_bonus', index: 'mc_bonus', width: 50,sortable: false,summaryType:'sum', formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;"' : 'style="color:red;"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'mc_total', index: 'mc_total', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black; background-color: #cfe5fa"' : 'style="color:red; background-color: #cfe5fa"';},summaryTpl:'<label class="sum_data">{0}</label>'},
		      {name: 'company', index: 'company', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},cellattr: function (rowId, tv, rawObject, cm, rdata) { return Number(tv.replace(',','').replace(',','').replace(',','').replace(',','')) >= 0 ? 'style="color:black;' : 'style="color:red; ';},summaryTpl:'<label class="sum_data">{0}</label>'},
		],
		loadComplete: function() {
    		//footer summary font color
		    $(".sum_data").each(function(i) {if(parseFloat(this.innerHTML.replace(",","").replace(",","").replace(",","")) < 0){this.style.color='#a31212';}else{this.style.color='black';};});
		    
		    //Lower level
		    $(".detail_account_id_savan").click(function() {
		    	var account_id_val=this.innerHTML;
		    	if(account_id_val!='&nbsp;'){
				    if(account_id_val.length!=10){
		    			showDetails_savan(account_id_val);
		    			document.getElementById('lblDetailAgentID_savan').innerHTML=account_id_val;
				    }else{
					    //start: hide parent grids
					    document.getElementById('qry_savan_vegas_result').style.display='none';
					    document.getElementById('qry_savan_vegas_result_details').style.display='none';
					    //end: hide parent grids
				    	var dtfrom=document.getElementById('lblDetailDateFrom_savan').innerHTML;
	        			var dtto=document.getElementById('lblDetailDateTo_savan').innerHTML;
	        			tableMemberWinLoss_savan_vegas_details(account_id_val,dtFrom,dtTo,document.getElementById('cmbGame').name);
	        			document.getElementById('lblBettingDetailAgentID_savan').innerHTML=account_id_val;
	        			document.getElementById('lblDDateFrom_savan').innerHTML=dtfrom;
	        			document.getElementById('lblDDateTo_savan').innerHTML=dtto;
					}
		    	}
    		});
		    $('.sharing_commission').mouseover(function(e) {
    			//alert(this.name);
    			var x = e.pageX - this.offsetLeft;
				var y = e.pageY - this.offsetTop;
        		showDiv('dvSharingCommission',this,x,y);
        		
        		jQuery.ajax({
        			url: urlAgentSharingAndCommission + '&agent_id=' + this.name,
        			method: 'POST',
        			data: {},
        			context: '',
        			success: function(data){
        				document.getElementById('dvSharingCommission').innerHTML='';
        				if (data!='<table border="0" cellpadding="0" cellspacing="0"></table>'){
        					document.getElementById('dvSharingCommission').innerHTML=data;
        				}else{
        					document.getElementById('dvSharingCommission').innerHTML='Please set-up sharing/commission!';
            			}
        	    	}
        		});
    		});
			$('.sharing_commission').mouseout(function(e) {
				hideDiv('dvSharingCommission');
			});
			    
    	},   	
		treeGridModel:'adjacency',
        height:'auto',
        hidegrid: false,
        treeGrid: true,
        ExpandColumn:'desc',
        ExpandColClick: true,
        ExpandColumn: 'account_id',
        caption: captionSAVANWinLossDetails,
        viewrecords: true,
        grouping: true,
	   	groupingView : {
	   		groupField : ['currency_name'],
	   		groupColumnShow : [true],
	   		groupText : ['<b>{0}</b>'],
	   		groupCollapse : false,
			groupOrder: ['asc'],
			groupSummary : [true],
			groupDataSorted : true
	   	},
	   	pager: '#pgrSAVAN2',
	   	rowNum: 50,	
	    rowList: [20, 50, 100, 200, 500, 99999],
	});   
	$("#lstSAVAN2").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'mem_win_loss', numberOfColumns: 4, titleText: '<label style="Color:black">' + lblPlayer + '</label>'},
			{startColumnName: 'agt_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + lblAgent + '</label>'},
			{startColumnName: 'ma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + lblMasterAgent + '</label>'},
			{startColumnName: 'sma_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + lblSeniorMasterAgent + '</label>'},
			{startColumnName: 'sc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + lblSubCompany + '</label>'},
			{startColumnName: 'mc_win_loss', numberOfColumns: 3, titleText: '<label style="Color:black">' + lblMainCompany + '</label>'},
		  ]
	});
	$('#lstSAVAN2').jqGrid('navGrid', '#pgrSAVAN2', {edit: false, add: false, del:false, search: false});
	$("#lstSAVAN2").jqGrid('navButtonAdd','#pgrSAVAN2',{
        caption:"Export Current Page to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var table1= document.getElementById('qry_savan_vegas_result_details');
			exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_DETAILS_', table1.outerHTML);
       }, 
        position:"last"
    });   
	$("#lstSAVAN2").jqGrid('navButtonAdd','#pgrSAVAN2',{
        caption:"Export All Pages to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){
        	var url= urlExportAllSAVANWinLossDetails + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/') + '&strCurrency=' + strCurrency + '&intGame=' + intGame + 
			'&accountID=' + accountID + '&testCurrency=' + testCurrency + '&specificID=' + document.getElementById('txtAccountID').value;
			exportAllWinLossToExcel('SAVANVEGAS999_WIN_LOSS_DETAILS_', url);
       }, 
        position:"last"
    });   
}

/**
 * @todo savan vegas member winloss
 * @author leokarl
 * @since 2012-12-17
 * @param strAccountID
 * @param dtFrom
 * @param dtTo
 * @param intGame
 * @param testCurrency
 */
function tableMemberWinLoss_savan_vegas_details(strAccountID,dtFrom,dtTo,intGame,testCurrency){
	document.getElementById('qry_savan_vegas_betting_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list6'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_betting_details").appendChild(divTag);

    var div2Tag = document.createElement("Div"); 
    div2Tag.id = 'pager6'; 
    div2Tag.style.margin = "0px auto"; 
    document.getElementById("qry_savan_vegas_betting_details").appendChild(div2Tag);
    
    $(document).ready(function() {
		var grid=jQuery("#list6");
		grid.jqGrid({ 
			url: urlSAVANWinLossBettingDetails + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + 
				'&testCurrency=' + testCurrency, 
			datatype: 'json',
		    mtype: 'POST',
		    height: 'auto',
		    colNames: [lblNumber, "id", lblTableShoeGame, lblBetTime, lblAccountId, lblAccountName, lblCurrency, lblGameType, lblBetAmount, lblValidBet, 
					    lblResult, lblWinLoss, lblCommission, lblTips, lblBalanceBefore, lblBalanceAfter, lblTotal, lblIpAddress],
		    colModel: [
			  {name: 'row', index: 'row', width: 30, sortable: false,align: 'center',cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="background-color:#EBEBEB"'; },},
			  {name: 'id', index: 'id', width: 90, sortable: false,hidden: true},
			  {name: 'table_shoe_game', index: 'table_shoe_game', width: 120, sortable: false},
			  {name: 'bet_date', index: 'bet_date', width: 150, sortable: false,summaryType:'count', summaryTpl : lblTotal},
		      {name: 'account_id', index: 'account_id', width: 90, sortable: false},
		      {name: 'account_name', index: 'account_name', width: 120, sortable: false},
		      {name: 'currency_name', index: 'currency_name', width: 60,sortable: false},
		      {name: 'game_type', index: 'game_type', width: 120,sortable: false},
		      {name: 'bet_amount', index: 'bet_amount', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
		      {name: 'amount_wagers', index: 'amount_wagers', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amntwagers">{0}</label>'},
		      {name: 'result', index: 'result', width: 150,sortable: false,formatter: resultArrow_savan},
		      {name: 'win_loss', index: 'win_loss', width: 100,sortable: false,summaryType:'sum',formatter: winLossArrow_savan,summaryTpl:'<label class="cwinlos">{0}</label>'},
		      {name: 'commission', index: 'commission', width: 100,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ccomm">{0}</label>'},
		      {name: 'amount_tips', index: 'amount_tips', width: 100,hidden: true ,sortable: false,summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amttips">{0}</label>'},
		      //{name: 'bonus', index: 'bonus', width: 90,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="cbonus">{0}</label>'},
		      {name: 'balance_before', index: 'balance_before', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'balance', index: 'balance', width: 100,sortable: false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
		      {name: 'total', index: 'total', width: 100,sortable: false, summaryType:'sum',formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="ctotal">{0}</label>'},
		      {name: 'ip_address', index: 'ip_address', width: 230,sortable: false,},
		    ],
		    loadComplete: function() {
			    var i=0; 
			    for(i=0;i<=grid.getGridParam("reccount");i++)
			    {
			    	var myrow = grid.jqGrid('getRowData', i);
			    	if(myrow.amount_wagers < 0){grid.jqGrid('setCell',i,"amount_wagers","",{color:'red'});}//amount_wagers
			    	if(myrow.win_loss < 0){grid.jqGrid('setCell',i,"win_loss","",{color:'red'});}//win_loss
			    	if(myrow.commission < 0){grid.jqGrid('setCell',i,"commission","",{color:'red'});}//commission
			    	if(myrow.amount_tips < 0){grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});}//amount_tips
			    	//if(myrow.bonus < 0){grid.jqGrid('setCell',i,"bonus","",{color:'red'});}//bonus
			    	if(myrow.total < 0){grid.jqGrid('setCell',i,"total","",{color:'red'});}//total
			    	if(myrow.balance < 0){grid.jqGrid('setCell',i,"balance","",{color:'red'});}//balance
				}
			  	//color for summary footer
		        $(".amntwagers").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//valbet
		        //$(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//winloss
		        $(".cwinlos").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';} this.innerHTML=this.innerHTML.substring(0,this.innerHTML.indexOf('<i'));});//winlos
		        $(".ccomm").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//commisn
		        $(".amttips").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vtips
		        //$(".cbonus").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//bonus
		        $(".ctotal").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//tot
		        $(".cbalance").each(function(i) {if (parseFloat(this.innerHTML.replace(",","")) < 0){this.style.color='#f25757';}});//vpl

		        $("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
		    	showSAVANBettingFooter(strAccountID,dtFrom,dtTo,intGame,testCurrency);

		    	$('.savanwinlossarrow').click(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		showWinLossResultDetails_savan(this.alt,this,x-190,y-100);
		    	});
		    	/*
		    	$('.savanresultarrow').mouseover(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		showResultDetails_savan(this.alt,this,x-420,y-240);
		    	});
		    	*/
		    	$('.savanresultarrow').click(function(e) {
		    		var x = e.pageX - this.offsetLeft;
		    		var y = e.pageY - this.offsetTop;
		    		showResultDetails_savan(this.alt,this,x-420,y-240);
		    	});
		    },
		    loadtext:"",
            sortable: false,
		    sortname: 'no',
		    sortorder: 'ASC',
		    pager: '#pager6',
		    rowNum: 50,	
		    rowList: [20, 50, 100, 200, 500, 99999],
		    caption: captionMemberWinLossSavanVegasDetails,
		    hidegrid: false,
		    viewrecords: true,
		    grouping: true,
		   	groupingView : {
		   		groupField : ['currency_name'],
		   		groupColumnShow : [true],
		   		groupText : ['<b>{0}</b>'],
		   		groupCollapse : false,
				groupOrder: ['asc'],
				groupSummary : [true],
				groupDataSorted : true
		   	},
		});
		$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false, refresh: false});
		$("#list6").jqGrid('setGroupHeaders', {
			  useColSpanStyle: true, 
			  groupHeaders:[
				{startColumnName: 'win_loss', numberOfColumns: 6, titleText: '<label style="Color:black">' + lblPlayer + '</label>'},
			  ]
		});
		$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false});
		$("#list6").jqGrid('navButtonAdd','#pager6',{
	        caption:"Export Current Page to Excel", 
	        buttonicon:"ui-icon-calculator", 
	        onClickButton: function(){
	        	var table1= document.getElementById('qry_savan_vegas_betting_details');
				exportWinLossToExcel('SAVANVEGAS999_WIN_LOSS_BETTING_DETAILS_', table1.outerHTML);
	       }, 
	        position:"last"
	    });   
		$("#list6").jqGrid('navButtonAdd','#pager6',{
	        caption:"Export All Pages to Excel", 
	        buttonicon:"ui-icon-calculator", 
	        onClickButton: function(){
	        	var url= urlExportAllSAVANWinLossBettingDetails + '&dtFrom=' + dtFrom.replace(" ", "_").split('-').join('/') + '&dtTo=' + dtTo.replace(" ", "_").split('-').join('/')  + '&strAccountID=' + strAccountID + '&intGame=' + intGame + 
				'&testCurrency=' + testCurrency;
				exportAllWinLossToExcel('SAVANVEGAS999_WIN_LOSS_BETTING_DETAILS_', url);
	       }, 
	        position:"last"
	    });  
	});
}

function savanSubAgent(accountID)
{
	var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('cmbCurrency').value;
	var intGame=document.getElementById('cmbGame').value;
	var user_level=sessionLevel;
	var account_id=sessionAccountId;
	var testCurrency=0;
	if(document.getElementById('chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		if(accountID==''){
			tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
		}
		if(accountID.length!=8){
			tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
		}else{
			tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}else{
		if(accountID.length!=8){
			if(accountID==''){
				if(user_level!='AGT'){
					tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}else{
					tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
			}
		}else{
			tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
		}
	}
	if(accountID==''){
		document.getElementById('btnBack_savan').style.display='none';
	}else{
		document.getElementById('btnBack_savan').style.display='inline';
	}
	document.getElementById('lblDateFrom_savan').innerHTML=dtFrom;
	document.getElementById('lblDateTo_savan').innerHTML=dtTo;
	document.getElementById('cmbGame').name=intGame;
	document.getElementById('cmbCurrency').name=strCurrency;
	if(document.getElementById('lblAgentID_savan').innerHTML==''){
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(sessionAccountId==account_id){
				document.getElementById('lblAgentID_savan').innerHTML=account_id;
				//document.getElementById('lblCostaTotalWinLossID').innerHTML=account_id;
			}
		}
	}
}
function submitComplete(accountID)
{
	var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('cmbCurrency').value;
	var intGame=document.getElementById('cmbGame').value;
	var user_level=sessionLevel;
	var account_id=sessionAccountId;
	var testCurrency=0;
	if(document.getElementById('chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		if(accountID==''){
			
			if(document.getElementById('chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if(readSavanvegasWinLoss == 1 || account_type == 'agent')
				{
					tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
		if(accountID.length!=8){
			
			if(document.getElementById('chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if(readSavanvegasWinLoss == 1 || account_type == 'agent')
				{
					tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
	}else{
		if(accountID.length!=8){
			if(accountID==''){
				if(user_level!='AGT'){
					
					if(document.getElementById('chkSAVAN').checked==true){
						document.getElementById('dv_savanvegas').style.display='block';
						if(readSavanvegasWinLoss == 1 || account_type == 'agent'){
							tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
							tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_savanvegas').style.display='none';
					}
				}else{
					
					if(document.getElementById('chkSAVAN').checked==true){
						document.getElementById('dv_savanvegas').style.display='block';
						if(readSavanvegasWinLoss == 1 || account_type == 'agent'){
							tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
							tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
						}
					}else{
						document.getElementById('dv_savanvegas').style.display='none';
					}
				}
			}else{
				
				if(document.getElementById('chkSAVAN').checked==true){
					document.getElementById('dv_savanvegas').style.display='block';
					if(readSavanvegasWinLoss == 1 || account_type == 'agent'){
					tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
					}
				}else{
					document.getElementById('dv_savanvegas').style.display='none';
				}
			}
		}else{
			
			if(document.getElementById('chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if(readSavanvegasWinLoss == 1 || account_type == 'agent'){
				tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
	}
	if(accountID==''){
		document.getElementById('btnBack_savan').style.display='none';
	}else{
		document.getElementById('btnBack_savan').style.display='inline';
	}
	document.getElementById('lblDateFrom_savan').innerHTML=dtFrom;
	document.getElementById('lblDateTo_savan').innerHTML=dtTo;
	document.getElementById('cmbGame').name=intGame;
	document.getElementById('cmbCurrency').name=strCurrency;
	
	hideBettingDetails_savan();
}

function btnToDay()
{
	
	document.getElementById('dtFrom').value=dtToday;
	document.getElementById('dtTo').value=dtToday;
	document.getElementById('cmbTime1').value='00';
	document.getElementById('cmbTime2').value='23';
	
	var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('cmbCurrency').value;
	var intGame=document.getElementById('cmbGame').value;
	var testCurrency = (document.getElementById('chkTestCurrency').checked == true) ? 1 : 0;
	var user_level=sessionLevel;
	var account_id=sessionAccountId;
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		
		if(document.getElementById('chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
				tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
				tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
	}else{
		
		if(document.getElementById('chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
				tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
		if(user_level!='AGT'){
			
			if(document.getElementById('chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
					tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}else{
			
			if(document.getElementById('chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
					tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
	}
	document.getElementById('lblDateFrom_savan').innerHTML=dtFrom;
	document.getElementById('lblDateTo_savan').innerHTML=dtTo;
	document.getElementById('cmbGame').name=intGame;
	document.getElementById('cmbCurrency').name=strCurrency;
	
	hideBettingDetails_savan();
}

function btnYesterday()
{
	
	document.getElementById('dtFrom').value=dtYesterday;
	document.getElementById('dtTo').value=dtYesterday;
	document.getElementById('cmbTime1').value='00';
	document.getElementById('cmbTime2').value='23';
	var dtFrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtTo=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
	var strCurrency= document.getElementById('cmbCurrency').value;
	var intGame=document.getElementById('cmbGame').value;
	var user_level=sessionLevel;
	var account_id=sessionAccountId;
	
	
	var testCurrency=0;
	if(document.getElementById('chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT'){
		
		if(document.getElementById('chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
				tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
				tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,'',testCurrency);
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
	}else{
		
		if(document.getElementById('chkSAVAN').checked==true){
			document.getElementById('dv_savanvegas').style.display='block';
			if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
				tableSAVANWinLossTotal(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
			}
		}else{
			document.getElementById('dv_savanvegas').style.display='none';
		}
		if(user_level!='AGT'){
			
			if(document.getElementById('chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
					tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}else{
			
			if(document.getElementById('chkSAVAN').checked==true){
				document.getElementById('dv_savanvegas').style.display='block';
				if (readSavanvegasWinLoss == 1 || account_type == 'agent'){
					tableSAVANMemberWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
				}
			}else{
				document.getElementById('dv_savanvegas').style.display='none';
			}
		}
	}

	document.getElementById('lblDateFrom_savan').innerHTML=dtFrom;
	document.getElementById('lblDateTo_savan').innerHTML=dtTo;
	document.getElementById('cmbGame').name=intGame;
	document.getElementById('cmbCurrency').name=strCurrency;
	
	hideBettingDetails_savan();
}

function backToParent_savan(accountID){
	var dtFrom=document.getElementById('lblDateFrom_savan').innerHTML;
	var dtTo=document.getElementById('lblDateTo_savan').innerHTML;
	var strCurrency= document.getElementById('cmbCurrency').name;
	var intGame=document.getElementById('cmbGame').name;
	var testCurrency=0;
	if(document.getElementById('chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	tableSAVANWinLoss(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
	
	document.getElementById('lblDateFrom_savan').innerHTML=dtFrom;
	document.getElementById('lblDateTo_savan').innerHTML=dtTo;
	document.getElementById('cmbGame').name=intGame;
	document.getElementById('cmbCurrency').name=strCurrency;
	if(accountID.length!=0){
		document.getElementById('lblAgentID_savan').innerHTML=account_id;
	}
	if(document.getElementById('lblAgentID_savan').innerHTML==''){
		document.getElementById('btnBack_savan').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(sessionAccountId==account_id){
				document.getElementById('btnBack_savan').style.display='none';
			}
		}
	}
}
function showDetails_savan(account_id){
	var user_level=sessionLevel;
	var dtFrom=document.getElementById('lblDateFrom_savan').innerHTML;
	var dtTo=document.getElementById('lblDateTo_savan').innerHTML;
	var strCurrency= document.getElementById('cmbCurrency').name;
	var intGame=document.getElementById('cmbGame').name;
	var testCurrency=0;
	if(document.getElementById('chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	if(account_id!=''){
		accountID=account_id;
	}else{
		if(user_level!='SC' && user_level!='SMA' && user_level!='MA' && user_level!='AGT' && user_level!='MEM'){
			accountID='';
		}else{
			accountID=sessionAccountId;
		}
	}

	jQuery("#lstSAVAN1").trigger("reloadGrid");//reload savan winloss
	tableSAVANWinLossDetails(dtFrom,dtTo,strCurrency,intGame,accountID,testCurrency);
	jQuery("#lstTotal_savan").trigger("reloadGrid");//reload savan total
	
	if(accountID.length!=0){
		var myGrid = $('#lstSAVAN2');
		document.getElementById('lblDetailAgentID_savan').innerHTML=accountID;
		if(accountID.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}else if(accountID.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[27].name);
		}
	}
	document.getElementById('lblDetailDateFrom_savan').innerHTML=dtFrom;
	document.getElementById('lblDetailDateTo_savan').innerHTML=dtTo;
	//hide back button
	if(document.getElementById('lblDetailAgentID_savan').innerHTML==''){
		document.getElementById('btnDetailBack_savan').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(sessionAccountId==accountID){
				document.getElementById('btnDetailBack_savan').style.display='none';
			}
		}
	}
}


function backToParentDetail_savan(accountID){
	var dtFrom=document.getElementById('lblDetailDateFrom_savan').innerHTML;
	var dtTo=document.getElementById('lblDetailDateTo_savan').innerHTML;
	var strCurrency= document.getElementById('cmbGame').name;
	var intGame=document.getElementById('cmbGame').name;
	var testCurrency=0;
	if(document.getElementById('chkTestCurrency').checked==true){
		testCurrency=1;
	}else{
		testCurrency=0;
	}
	
	var user_level=sessionLevel;
	var account_id=accountID.substring(0,accountID.length-2);
	tableSAVANWinLossDetails(dtFrom,dtTo,strCurrency,intGame,account_id,testCurrency);
	if(account_id.length!=0){
		var myGrid = $('#lstSAVAN2');
		document.getElementById('lblDetailAgentID_savan').innerHTML=accountID;
		if(account_id.length==2){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==4){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==6){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}else if(account_id.length==8){
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[15].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[16].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[17].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[18].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[19].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[20].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[21].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[22].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[23].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[24].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[25].name);
			myGrid.jqGrid('hideCol', myGrid.getGridParam("colModel")[26].name);
		}
	}
	document.getElementById('lblDetailDateFrom_savan').innerHTML=dtFrom;
	document.getElementById('lblDetailDateTo_savan').innerHTML=dtTo;
	if(accountID.length!=0){
		document.getElementById('lblDetailAgentID_savan').innerHTML=account_id;
	}
	if(document.getElementById('lblDetailAgentID_savan').innerHTML==''){
		document.getElementById('btnDetailBack_savan').style.display='none';
	}else{
		if(user_level=='SC' || user_level=='SMA' || user_level=='MA' || user_level=='AGT'){
			if(sessionAccountId==account_id){
				document.getElementById('btnDetailBack_savan').style.display='none';
			}
		}
	}
}

