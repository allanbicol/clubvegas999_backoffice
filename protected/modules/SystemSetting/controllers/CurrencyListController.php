<?php
/**
 * @todo CurrencyListController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-25
 */
class CurrencyListController extends MyController
{
	public function actionCurrencyList()
	{
		$cp = new Currency();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$cp->getCountCurrencyRecord();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page=$total_pages;
		
		$startIndex = $limit*$page - $limit;
		$user_records=$cp->getCurrencyList($orderField, $sortType, $startIndex, $limit);
		
		$filedNames = array("id","currency_name","exchange_rate","last_update","id");
		$urlAS= Yii::app()->request->baseUrl;
		if(Yii::app()->user->checkAccess('systemSetting.writeCurrencySetting')){
			$htmValue='<a class=\'btn mini red\' href=\'#\' onclick=\'javascript: getCurrencyName(this); $updateCurrencyDialog.dialog(\"open\");document.getElementById(\"csu_errMessage\").value=\"\"\' id=\'currency\'>Setting <i class=\"icon-cogs\"></i></a>';
		}else{
			$htmValue='  '; 
		}
	
		echo JsonUtil::jsonEncodeSystemSetting($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,4);
	}
	public function actionIndex()
	{
		
			
		if(Yii::app()->user->checkAccess('systemSetting.readCurrencySetting'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		
	}
}
