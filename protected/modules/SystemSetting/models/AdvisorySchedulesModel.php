<?php
/**
 * @todo ProcessEmailSetting Model
 * @copyright CE
 * @author Sitthykun
 * @since 2013-07-05
 */
class AdvisorySchedulesModel
{
	private $days = array("monday" => "monday", "tuesday" => "tuesday", "wednesday" => "wednesday", "thursday" => "thursday", "friday" => "friday", "saturday" => "saturday", "sunday" => "sunday");
	/**
	 * 
	 * @param string
	 */
	public function getList()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spGetAdvisoryScheduleCategories()");

		return $command->queryAll();
	}

	/**
	 * 
	 */
	public function getDaysOfWeek()
	{
		return $this->days;
	}
}

?>