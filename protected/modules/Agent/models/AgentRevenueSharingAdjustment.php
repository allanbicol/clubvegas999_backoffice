<?php

class AgentRevenueSharingAdjustment
{
	/**
	 * @todo generate new limit to.
	 * @author leokarl
	 * @since 2013-03-21
	 * @param unknown_type $revenue
	 * @param unknown_type $default
	 * @param unknown_type $revenue_limit
	 */
	public function getNewLimitTo($revenue, $default, $revenue_limit){
		return ($revenue != '') ? $revenue_limit : $default;
	}
	/**
	 * @todo check if agent already change sharing/commission within the current date. return true if no log yet.
	 * @author leokarl
	 * @since 2013-03-18
	 * @return boolean
	 */
	public function saveLimitValidation(){
		$log = TableLog::model()->count(array('condition'=>'operated_by=:operated_by AND operated=:operated_by AND (log_type_id = 6 OR log_type_id = 7) 
				AND DATE_FORMAT(operation_time,"%Y-%m-%d") = DATE_FORMAT(NOW(),"%Y-%m-%d")',
				'params'=>array(':operated_by' => Yii::app()->session['account_id']),));
		
		return ($log > 0) ? FALSE : TRUE;
	}
	
	/**
	 * @todo mix before and after sharing/commission.
	 * @author leokarl
	 * @since 2013-03-16
	 * @param string $before, 0#0#0#0
	 * @param string $after, 0#0#0#0
	 * @param array $casino_level
	 */
	public function generateSharingCommissionString($before, $after, $casino_level){
		list($b_vig,$b_htv,$b_sv,$b_vv) = explode("#",$before);
		list($a_vig,$a_htv,$a_sv,$a_vv) = explode("#",$after);
		
		$vig = ($casino_level['vig'] == 1) ? $a_vig : $b_vig;
		$htv = ($casino_level['htv'] == 1) ? $a_htv : $b_htv;
		$sv = ($casino_level['sv'] == 1) ? $a_sv : $b_sv;
		$vv = ($casino_level['vv'] == 1) ? $a_vv : $b_vv;
		
		return $vig . "#" . $htv . "#" . $sv . "#" . $vv;
	}
	
	/**
	 * @todo compare agent player's commission
	 * @author leokarl
	 * @since 2013-03-18
	 * @param string $before
	 * @param string $after
	 * @return boolean
	 */
	public function isSharingCommissionChange($before, $after, $casino_level){
		// ex. 0.00#90.00#30.00#0.00
		$change_count = 0;
		list($costa_before,$htv_before,$sv_before,$virtua_before) = explode("#", $before);
		list($costa_after,$htv_after,$sv_after,$virtua_after) = explode("#", $after);
	
		// compare costa commission
		if($casino_level['vig']== 1 && $costa_before <> $costa_after){
			$change_count +=1;
		}
	
		// compare hatien commission
		if($casino_level['htv']== 1 && $htv_before <> $htv_after){
			$change_count +=1;
		}
	
		// compare savan commission
		if($casino_level['sv']== 1 && $sv_before <> $sv_after){
			$change_count +=1;
		}
	
		// compare virtua commission
		if($casino_level['vv']== 1 && $virtua_before <> $virtua_after){
			$change_count +=1;
		}
	
		return ($change_count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo validate sharing/commission value versus limits
	 * @author leokarl
	 * @since 2013-03-15
	 * @param float $value
	 * @param float $limit_from
	 * @param float $limit_to
	 */
	public function sValVSlimitValid($value, $limit_from, $limit_to){
		$err_counter = 0;
		$err_counter += ($value < $limit_from) ? 1 : 0;
		$err_counter += ($value > $limit_to) ? 1 : 0;
		return ($err_counter > 0) ? FALSE : TRUE;
	}
	
	/**
	 * @todo get higher agent's downline revenue value
	 * @author leokarl
	 * @since 2013-03-29
	 */
	public function getRevenueValue($game, $type, $casino){
		/**
		 * @variable defination
		 * $game: 1=bac, 2=dra, 3=eur, 4=ame, 5=bla, 6=slots
		 * $type: 1=share, 2=commission
		 * $casino: 1=vig, 2=htv, 3=sv, 4=vv   
		 */
		$connection = Yii::app()->db_cv999_fd_master;
		
		$command = $connection->createCommand('
			SELECT SPLIT_STR(SPLIT_STR(SPLIT_STR(revenue_sharing_limit_to, "|",'. $game .'),"^",'. $type .'), "#",'. $casino .') AS val FROM tbl_agent WHERE agent_parent_id="'. Yii::app()->session['account_id'] .'"
			ORDER BY SPLIT_STR(SPLIT_STR(SPLIT_STR(revenue_sharing_limit_to, "|",'. $game .'),"^",'. $type .'), "#",'. $casino .') DESC LIMIT 1
		');
		$rows = $command->queryAll();
		return (isset($rows[0]['val'])) ? (float)$rows[0]['val'] : 0;
	}
	
	/**
	 * @todo get sharing and commission limit from
	 * @author leokarl
	 * @since 2013-03-15 
	 */
	public function getLimitFrom(){
		// limit from
		
		
		// baccarat
		if(strlen(Yii::app()->session['account_id']) < 8){
			
			/** @todo get share and commission limit_from from agent */
			
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_baccarat,1,locate("#",share_baccarat)-1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'substring(share_baccarat,1,locate("#",share_baccarat)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_share_baccarat_from = ($cs['share_baccarat'] > $this->getRevenueValue(1, 1, 1)) ? $cs['share_baccarat'] : $this->getRevenueValue(1, 1, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat)+1))-1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_share_baccarat_from = ($cs['share_baccarat'] > $this->getRevenueValue(1, 1, 2)) ? $cs['share_baccarat'] : $this->getRevenueValue(1, 1, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))-1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),1,locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_share_baccarat_from = ($cs['share_baccarat'] > $this->getRevenueValue(1, 1, 3)) ? $cs['share_baccarat'] : $this->getRevenueValue(1, 1, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))+1) as share_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1),locate("#",substring(share_baccarat,locate("#",share_baccarat,locate("#",share_baccarat)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_share_baccarat_from = ($cs['share_baccarat'] > $this->getRevenueValue(1, 1, 4)) ? $cs['share_baccarat'] : $this->getRevenueValue(1, 1, 4);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_comm_baccarat_from = ($cs['commission_baccarat'] > $this->getRevenueValue(1, 2, 1)) ? $cs['commission_baccarat'] : $this->getRevenueValue(1, 2, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1)  as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_comm_baccarat_from = ($cs['commission_baccarat'] > $this->getRevenueValue(1, 2, 2)) ? $cs['commission_baccarat'] : $this->getRevenueValue(1, 2, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1)  as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_comm_baccarat_from = ($cs['commission_baccarat'] > $this->getRevenueValue(1, 2, 3)) ? $cs['commission_baccarat'] : $this->getRevenueValue(1, 2, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) as commission_baccarat','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_comm_baccarat_from = ($cs['commission_baccarat'] > $this->getRevenueValue(1, 2, 4)) ? $cs['commission_baccarat'] : $this->getRevenueValue(1, 2, 4);
			
			//roulette limit from
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_roulette,1,locate("#",share_roulette)-1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_roulette,1,locate("#",share_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_share_roulette_from= ($cs['share_roulette'] > $this->getRevenueValue(3, 1, 1)) ? $cs['share_roulette'] : $this->getRevenueValue(3, 1, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette)+1))-1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_share_roulette_from= ($cs['share_roulette'] > $this->getRevenueValue(3, 1, 2)) ? $cs['share_roulette'] : $this->getRevenueValue(3, 1, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))-1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),1,locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_share_roulette_from= ($cs['share_roulette'] > $this->getRevenueValue(3, 1, 3)) ? $cs['share_roulette'] : $this->getRevenueValue(3, 1, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))+1) as share_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1),locate("#",substring(share_roulette,locate("#",share_roulette,locate("#",share_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_share_roulette_from= ($cs['share_roulette'] > $this->getRevenueValue(3, 1, 4)) ? $cs['share_roulette'] : $this->getRevenueValue(3, 1, 4);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_comm_roulette_from= ($cs['commission_roulette'] > $this->getRevenueValue(3, 2, 1)) ? $cs['commission_roulette'] : $this->getRevenueValue(3, 2, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_comm_roulette_from= ($cs['commission_roulette'] > $this->getRevenueValue(3, 2, 2)) ? $cs['commission_roulette'] : $this->getRevenueValue(3, 2, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_comm_roulette_from= ($cs['commission_roulette'] > $this->getRevenueValue(3, 2, 3)) ? $cs['commission_roulette'] : $this->getRevenueValue(3, 2, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) as commission_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_comm_roulette_from= ($cs['commission_roulette'] > $this->getRevenueValue(3, 2, 4)) ? $cs['commission_roulette'] : $this->getRevenueValue(3, 2, 4);
			
			//dragon tiger limit from
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_dragon_tiger,1,locate("#",share_dragon_tiger)-1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_dragon_tiger,1,locate("#",share_dragon_tiger)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_share_dragontiger_from= ($cs['share_dragon_tiger'] > $this->getRevenueValue(2, 1, 1)) ? $cs['share_dragon_tiger'] : $this->getRevenueValue(2, 1, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1))-1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_share_dragontiger_from= ($cs['share_dragon_tiger'] > $this->getRevenueValue(2, 1, 2)) ? $cs['share_dragon_tiger'] : $this->getRevenueValue(2, 1, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))-1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),1,locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_share_dragontiger_from= ($cs['share_dragon_tiger'] > $this->getRevenueValue(2, 1, 3)) ? $cs['share_dragon_tiger'] : $this->getRevenueValue(2, 1, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))+1) as share_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1),locate("#",substring(share_dragon_tiger,locate("#",share_dragon_tiger,locate("#",share_dragon_tiger)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_share_dragontiger_from= ($cs['share_dragon_tiger'] > $this->getRevenueValue(2, 1, 4)) ? $cs['share_dragon_tiger'] : $this->getRevenueValue(2, 1, 4);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_comm_dragontiger_from= ($cs['commission_dragon_tiger'] > $this->getRevenueValue(2, 2, 1)) ? $cs['commission_dragon_tiger'] : $this->getRevenueValue(2, 2, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_comm_dragontiger_from= ($cs['commission_dragon_tiger'] > $this->getRevenueValue(2, 2, 2)) ? $cs['commission_dragon_tiger'] : $this->getRevenueValue(2, 2, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_comm_dragontiger_from= ($cs['commission_dragon_tiger'] > $this->getRevenueValue(2, 2, 3)) ? $cs['commission_dragon_tiger'] : $this->getRevenueValue(2, 2, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))+1) as commission_dragon_tiger','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_comm_dragontiger_from= ($cs['commission_dragon_tiger'] > $this->getRevenueValue(2, 2, 4)) ? $cs['commission_dragon_tiger'] : $this->getRevenueValue(2, 2, 4);
				
			//blackjack limit from
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_blackjack,1,locate("#",share_blackjack)-1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_blackjack,1,locate("#",share_blackjack)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_share_blackjack_from= ($cs['share_blackjack'] > $this->getRevenueValue(5, 1, 1)) ? $cs['share_blackjack'] : $this->getRevenueValue(5, 1, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack)+1))-1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_share_blackjack_from= ($cs['share_blackjack'] > $this->getRevenueValue(5, 1, 2)) ? $cs['share_blackjack'] : $this->getRevenueValue(5, 1, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))-1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),1,locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_share_blackjack_from= ($cs['share_blackjack'] > $this->getRevenueValue(5, 1, 3)) ? $cs['share_blackjack'] : $this->getRevenueValue(5, 1, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))+1) as share_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1),locate("#",substring(share_blackjack,locate("#",share_blackjack,locate("#",share_blackjack)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_share_blackjack_from= ($cs['share_blackjack'] > $this->getRevenueValue(5, 1, 4)) ? $cs['share_blackjack'] : $this->getRevenueValue(5, 1, 4);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_comm_blackjack_from= ($cs['commission_blackjack'] > $this->getRevenueValue(5, 2, 1)) ? $cs['commission_blackjack'] : $this->getRevenueValue(5, 2, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_comm_blackjack_from= ($cs['commission_blackjack'] > $this->getRevenueValue(5, 2, 2)) ? $cs['commission_blackjack'] : $this->getRevenueValue(5, 2, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_comm_blackjack_from= ($cs['commission_blackjack'] > $this->getRevenueValue(5, 2, 3)) ? $cs['commission_blackjack'] : $this->getRevenueValue(5, 2, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) as commission_blackjack','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_comm_blackjack_from= ($cs['commission_blackjack'] > $this->getRevenueValue(5, 2, 4)) ? $cs['commission_blackjack'] : $this->getRevenueValue(5, 2, 4);
				
			//american roulette limit from (*)
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_american_roulette,1,locate("#",share_american_roulette)-1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_american_roulette,1,locate("#",share_american_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_share_americanroulette_from= ($cs['share_american_roulette'] > $this->getRevenueValue(4, 1, 1)) ? $cs['share_american_roulette'] : $this->getRevenueValue(4, 1, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette)+1),1,locate("#",substring(share_american_roulette,locate("#",share_american_roulette)+1))-1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette)+1),1,locate("#",substring(share_american_roulette,locate("#",share_american_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_share_americanroulette_from= ($cs['share_american_roulette'] > $this->getRevenueValue(4, 1, 2)) ? $cs['share_american_roulette'] : $this->getRevenueValue(4, 1, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1),1,locate("#",substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1))-1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1,length(share_american_roulette)) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_share_americanroulette_from= ($cs['share_american_roulette'] > $this->getRevenueValue(4, 1, 3)) ? $cs['share_american_roulette'] : $this->getRevenueValue(4, 1, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1),locate("#",substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1))+1) as share_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1),locate("#",substring(share_american_roulette,locate("#",share_american_roulette,locate("#",share_american_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_share_americanroulette_from= ($cs['share_american_roulette'] > $this->getRevenueValue(4, 1, 4)) ? $cs['share_american_roulette'] : $this->getRevenueValue(4, 1, 4);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_comm_americanroulette_from= ($cs['commission_american_roulette'] > $this->getRevenueValue(4, 2, 1)) ? $cs['commission_american_roulette'] : $this->getRevenueValue(4, 2, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_comm_americanroulette_from= ($cs['commission_american_roulette'] > $this->getRevenueValue(4, 2, 2)) ? $cs['commission_american_roulette'] : $this->getRevenueValue(4, 2, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_comm_americanroulette_from= ($cs['commission_american_roulette'] > $this->getRevenueValue(4, 2, 3)) ? $cs['commission_american_roulette'] : $this->getRevenueValue(4, 2, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) as commission_american_roulette','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_comm_americanroulette_from= ($cs['commission_american_roulette'] > $this->getRevenueValue(4, 2, 4)) ? $cs['commission_american_roulette'] : $this->getRevenueValue(4, 2, 4);
				
			//slots limit from
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(share_slots,1,locate("#",share_slots)-1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(share_slots,1,locate("#",share_slots)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_share_slots_from= ($cs['share_slots'] > $this->getRevenueValue(6, 1, 1)) ? $cs['share_slots'] : $this->getRevenueValue(6, 1, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_slots,locate("#",share_slots)+1),1,locate("#",substring(share_slots,locate("#",share_slots)+1))-1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_slots,locate("#",share_slots)+1),1,locate("#",substring(share_slots,locate("#",share_slots)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_share_slots_from= ($cs['share_slots'] > $this->getRevenueValue(6, 1, 2)) ? $cs['share_slots'] : $this->getRevenueValue(6, 1, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),1,locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))-1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),1,locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_share_slots_from= ($cs['share_slots'] > $this->getRevenueValue(6, 1, 3)) ? $cs['share_slots'] : $this->getRevenueValue(6, 1, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))+1) as share_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1),locate("#",substring(share_slots,locate("#",share_slots,locate("#",share_slots)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_share_slots_from= ($cs['share_slots'] > $this->getRevenueValue(6, 1, 4)) ? $cs['share_slots'] : $this->getRevenueValue(6, 1, 4);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vig_comm_slots_from= ($cs['commission_slots'] > $this->getRevenueValue(6, 2, 1)) ? $cs['commission_slots'] : $this->getRevenueValue(6, 2, 1);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$htv_comm_slots_from= ($cs['commission_slots'] > $this->getRevenueValue(6, 2, 2)) ? $cs['commission_slots'] : $this->getRevenueValue(6, 2, 2);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$sv_comm_slots_from= ($cs['commission_slots'] > $this->getRevenueValue(6, 2, 3)) ? $cs['commission_slots'] : $this->getRevenueValue(6, 2, 3);
			$cs=TableAgent::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) as commission_slots','condition'=>'agent_parent_id=:agent_parent_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_parent_id'=>Yii::app()->session['account_id']),));
			$vv_comm_slots_from= ($cs['commission_slots'] > $this->getRevenueValue(6, 2, 4)) ? $cs['commission_slots'] : $this->getRevenueValue(6, 2, 4);
			
		}else{
			
			/** @todo get share and commission limit_from from agent player */
			
			$vig_share_baccarat_from = 0;
			$htv_share_baccarat_from = 0;
			$sv_share_baccarat_from = 0;
			$vv_share_baccarat_from = 0;
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) as commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_baccarat,1,locate("#",commission_baccarat)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vig_comm_baccarat_from = $cs['commission_baccarat'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1)  as commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$htv_comm_baccarat_from = $cs['commission_baccarat'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1)  as commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),1,locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$sv_comm_baccarat_from = $cs['commission_baccarat'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) as commission_baccarat','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1),locate("#",substring(commission_baccarat,locate("#",commission_baccarat,locate("#",commission_baccarat)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vv_comm_baccarat_from = $cs['commission_baccarat'];
				
			//roulette limit from
			$vig_share_roulette_from = 0;
			$htv_share_roulette_from = 0;
			$sv_share_roulette_from = 0;
			$vv_share_roulette_from = 0;
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) as commission_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_roulette,1,locate("#",commission_roulette)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vig_comm_roulette_from=$cs['commission_roulette'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) as commission_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$htv_comm_roulette_from=$cs['commission_roulette'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) as commission_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),1,locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$sv_comm_roulette_from=$cs['commission_roulette'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) as commission_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1),locate("#",substring(commission_roulette,locate("#",commission_roulette,locate("#",commission_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vv_comm_roulette_from=$cs['commission_roulette'];
				
			//dragon tiger limit from
			$vig_share_dragontiger_from = 0;
			$htv_share_dragontiger_from = 0;
			$sv_share_dragontiger_from = 0;
			$vv_share_dragontiger_from = 0;
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) as commission_dragon_tiger','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_dragon_tiger,1,locate("#",commission_dragon_tiger)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vig_comm_dragontiger_from=$cs['commission_dragon_tiger'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) as commission_dragon_tiger','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$htv_comm_dragontiger_from=$cs['commission_dragon_tiger'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) as commission_dragon_tiger','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),1,locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$sv_comm_dragontiger_from=$cs['commission_dragon_tiger'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))+1) as commission_dragon_tiger','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1),locate("#",substring(commission_dragon_tiger,locate("#",commission_dragon_tiger,locate("#",commission_dragon_tiger)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vv_comm_dragontiger_from=$cs['commission_dragon_tiger'];
			
			//blackjack limit from
			$vig_share_blackjack_from = 0;
			$htv_share_blackjack_from = 0;
			$sv_share_blackjack_from = 0;
			$vv_share_blackjack_from = 0;
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) as commission_blackjack','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_blackjack,1,locate("#",commission_blackjack)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vig_comm_blackjack_from=$cs['commission_blackjack'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) as commission_blackjack','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$htv_comm_blackjack_from=$cs['commission_blackjack'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) as commission_blackjack','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),1,locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$sv_comm_blackjack_from=$cs['commission_blackjack'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) as commission_blackjack','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1),locate("#",substring(commission_blackjack,locate("#",commission_blackjack,locate("#",commission_blackjack)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vv_comm_blackjack_from=$cs['commission_blackjack'];
			
			//american roulette limit from (*)
			$vig_share_americanroulette_from = 0;
			$htv_share_americanroulette_from = 0;
			$sv_share_americanroulette_from = 0;
			$vv_share_americanroulette_from = 0;
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) as commission_american_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_american_roulette,1,locate("#",commission_american_roulette)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vig_comm_americanroulette_from=$cs['commission_american_roulette'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) as commission_american_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$htv_comm_americanroulette_from=$cs['commission_american_roulette'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) as commission_american_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),1,locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$sv_comm_americanroulette_from=$cs['commission_american_roulette'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) as commission_american_roulette','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1),locate("#",substring(commission_american_roulette,locate("#",commission_american_roulette,locate("#",commission_american_roulette)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vv_comm_americanroulette_from=$cs['commission_american_roulette'];
			
			//slots limit from
			$vig_share_slots_from = 0;
			$htv_share_slots_from = 0;
			$sv_share_slots_from = 0;
			$vv_share_slots_from = 0;
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) as commission_slots','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(commission_slots,1,locate("#",commission_slots)-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vig_comm_slots_from=$cs['commission_slots'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) as commission_slots','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$htv_comm_slots_from=$cs['commission_slots'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) as commission_slots','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),1,locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))-1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$sv_comm_slots_from=$cs['commission_slots'];
			$cs=TableAgentPlayer::model()->find(array('select'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) as commission_slots','condition'=>'agent_account_id=:agent_account_id','order'=>'SUBSTRING(substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1),locate("#",substring(commission_slots,locate("#",commission_slots,locate("#",commission_slots)+1)+1))+1) desc','limit'=>'1','params'=>array(':agent_account_id'=>Yii::app()->session['account_id']),));
			$vv_comm_slots_from=$cs['commission_slots'];
		}
		return array('htv_share_baccarat_from'=>$htv_share_baccarat_from,
				'sv_share_baccarat_from'=>$sv_share_baccarat_from,
				'vig_share_baccarat_from'=>$vig_share_baccarat_from,
				'vv_share_baccarat_from'=>$vv_share_baccarat_from,
				'htv_comm_baccarat_from'=>$htv_comm_baccarat_from,
				'sv_comm_baccarat_from'=>$sv_comm_baccarat_from,
				'vig_comm_baccarat_from'=>$vig_comm_baccarat_from,
				'vv_comm_baccarat_from'=>$vv_comm_baccarat_from,
				'htv_share_roulette_from'=>$htv_share_roulette_from,
				'sv_share_roulette_from'=>$sv_share_roulette_from,
				'vig_share_roulette_from'=>$vig_share_roulette_from,
				'vv_share_roulette_from'=>$vv_share_roulette_from,
				'htv_comm_roulette_from'=>$htv_comm_roulette_from,
				'sv_comm_roulette_from'=>$sv_comm_roulette_from,
				'vig_comm_roulette_from'=>$vig_comm_roulette_from,
				'vv_comm_roulette_from'=>$vv_comm_roulette_from,
				'htv_share_dragontiger_from'=>$htv_share_dragontiger_from,
				'sv_share_dragontiger_from'=>$sv_share_dragontiger_from,
				'vig_share_dragontiger_from'=>$vig_share_dragontiger_from,
				'vv_share_dragontiger_from'=>$vv_share_dragontiger_from,
				'htv_comm_dragontiger_from'=>$htv_comm_dragontiger_from,
				'sv_comm_dragontiger_from'=>$sv_comm_dragontiger_from,
				'vig_comm_dragontiger_from'=>$vig_comm_dragontiger_from,
				'vv_comm_dragontiger_from'=>$vv_comm_dragontiger_from,
				'htv_share_blackjack_from'=>$htv_share_blackjack_from,
				'sv_share_blackjack_from'=>$sv_share_blackjack_from,
				'vig_share_blackjack_from'=>$vig_share_blackjack_from,
				'vv_share_blackjack_from'=>$vv_share_blackjack_from,
				'htv_comm_blackjack_from'=>$htv_comm_blackjack_from,
				'sv_comm_blackjack_from'=>$sv_comm_blackjack_from,
				'vig_comm_blackjack_from'=>$vig_comm_blackjack_from,
				'vv_comm_blackjack_from'=>$vv_comm_blackjack_from,
				'htv_share_americanroulette_from'=>$htv_share_americanroulette_from,
				'sv_share_americanroulette_from'=>$sv_share_americanroulette_from,
				'vig_share_americanroulette_from'=>$vig_share_americanroulette_from,
				'vv_share_americanroulette_from'=>$vv_share_americanroulette_from,
				'htv_comm_americanroulette_from'=>$htv_comm_americanroulette_from,
				'sv_comm_americanroulette_from'=>$sv_comm_americanroulette_from,
				'vig_comm_americanroulette_from'=>$vig_comm_americanroulette_from,
				'vv_comm_americanroulette_from'=>$vv_comm_americanroulette_from,
				'htv_share_slots_from'=>$htv_share_slots_from,
				'sv_share_slots_from'=>$sv_share_slots_from,
				'vig_share_slots_from'=>$vig_share_slots_from,
				'vv_share_slots_from'=>$vv_share_slots_from,
				'htv_comm_slots_from'=>$htv_comm_slots_from,
				'sv_comm_slots_from'=>$sv_comm_slots_from,
				'vig_comm_slots_from'=>$vig_comm_slots_from,
				'vv_comm_slots_from'=>$vv_comm_slots_from);
	}
	
	public function getLimitTo(){
		// revenue sharing limit_to
		
		$info = TableAgent::model()->find(array('select'=>'revenue_sharing_limit_to','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
		// Baccarat|Dragon Tiger|European Roulette|American Roulette|Blackjack|Slots
		if(trim($info['revenue_sharing_limit_to']) != ''){
			list($baccarat,$dragon_tiger,$european_roulette,$american_roulette,$blackjack,$slots) = explode("|",$info['revenue_sharing_limit_to']);
			
			// baccarat
			list($sharing_baccarat,$commission_baccarat) = explode("^",$baccarat);
			list($vig_share_baccarat_to,$htv_share_baccarat_to,$sv_share_baccarat_to,$vv_share_baccarat_to) = explode("#",$sharing_baccarat);
			list($vig_comm_baccarat_to,$htv_comm_baccarat_to,$sv_comm_baccarat_to,$vv_comm_baccarat_to) = explode("#",$commission_baccarat);
			
			// dragon tiger
			list($sharing_dragon_tiger,$commission_dragon_tiger) = explode("^",$dragon_tiger);
			list($vig_share_dragon_tiger_to,$htv_share_dragon_tiger_to,$sv_share_dragon_tiger_to,$vv_share_dragon_tiger_to) = explode("#",$sharing_dragon_tiger);
			list($vig_comm_dragon_tiger_to,$htv_comm_dragon_tiger_to,$sv_comm_dragon_tiger_to,$vv_comm_dragon_tiger_to) = explode("#",$commission_dragon_tiger);
			
			// european roulette
			list($sharing_e_roulette,$commission_e_roulette) = explode("^",$european_roulette);
			list($vig_share_e_roulette_to,$htv_share_e_roulette_to,$sv_share_e_roulette_to,$vv_share_e_roulette_to) = explode("#",$sharing_e_roulette);
			list($vig_comm_e_roulette_to,$htv_comm_e_roulette_to,$sv_comm_e_roulette_to,$vv_comm_e_roulette_to) = explode("#",$commission_e_roulette);
			
			// american roulette
			list($sharing_a_roulette,$commission_a_roulette) = explode("^",$american_roulette);
			list($vig_share_a_roulette_to,$htv_share_a_roulette_to,$sv_share_a_roulette_to,$vv_share_a_roulette_to) = explode("#",$sharing_a_roulette);
			list($vig_comm_a_roulette_to,$htv_comm_a_roulette_to,$sv_comm_a_roulette_to,$vv_comm_a_roulette_to) = explode("#",$commission_a_roulette);
			
			// blackjack
			list($sharing_blackjack,$commission_blackjack) = explode("^",$blackjack);
			list($vig_share_blackjack_to,$htv_share_blackjack_to,$sv_share_blackjack_to,$vv_share_blackjack_to) = explode("#",$sharing_blackjack);
			list($vig_comm_blackjack_to,$htv_comm_blackjack_to,$sv_comm_blackjack_to,$vv_comm_blackjack_to) = explode("#",$commission_blackjack);
			
			// slots
			list($sharing_slots,$commission_slots) = explode("^",$slots);
			list($vig_share_slots_to,$htv_share_slots_to,$sv_share_slots_to,$vv_share_slots_to) = explode("#",$sharing_slots);
			list($vig_comm_slots_to,$htv_comm_slots_to,$sv_comm_slots_to,$vv_comm_slots_to) = explode("#",$commission_slots);
			
			return array('vig_share_baccarat_to'=>$vig_share_baccarat_to,
				'htv_share_baccarat_to'=>$htv_share_baccarat_to,
				'sv_share_baccarat_to'=>$sv_share_baccarat_to,
				'vv_share_baccarat_to'=>$vv_share_baccarat_to,
				'vig_comm_baccarat_to'=>$vig_comm_baccarat_to,
				'htv_comm_baccarat_to'=>$htv_comm_baccarat_to,
				'sv_comm_baccarat_to'=>$sv_comm_baccarat_to,
				'vv_comm_baccarat_to'=>$vv_comm_baccarat_to,
				
				'vig_share_dragon_tiger_to'=>$vig_share_dragon_tiger_to,
				'htv_share_dragon_tiger_to'=>$htv_share_dragon_tiger_to,
				'sv_share_dragon_tiger_to'=>$sv_share_dragon_tiger_to,
				'vv_share_dragon_tiger_to'=>$vv_share_dragon_tiger_to,
				'vig_comm_dragon_tiger_to'=>$vig_comm_dragon_tiger_to,
				'htv_comm_dragon_tiger_to'=>$htv_comm_dragon_tiger_to,
				'sv_comm_dragon_tiger_to'=>$sv_comm_dragon_tiger_to,
				'vv_comm_dragon_tiger_to'=>$vv_comm_dragon_tiger_to,
				
				'vig_share_e_roulette_to'=>$vig_share_e_roulette_to,
				'htv_share_e_roulette_to'=>$htv_share_e_roulette_to,
				'sv_share_e_roulette_to'=>$sv_share_e_roulette_to,
				'vv_share_e_roulette_to'=>$vv_share_e_roulette_to,
				'vig_comm_e_roulette_to'=>$vig_comm_e_roulette_to,
				'htv_comm_e_roulette_to'=>$htv_comm_e_roulette_to,
				'sv_comm_e_roulette_to'=>$sv_comm_e_roulette_to,
				'vv_comm_e_roulette_to'=>$vv_comm_e_roulette_to,
				
				'vig_share_a_roulette_to'=>$vig_share_a_roulette_to,
				'htv_share_a_roulette_to'=>$htv_share_a_roulette_to,
				'sv_share_a_roulette_to'=>$sv_share_a_roulette_to,
				'vv_share_a_roulette_to'=>$vv_share_a_roulette_to,
				'vig_comm_a_roulette_to'=>$vig_comm_a_roulette_to,
				'htv_comm_a_roulette_to'=>$htv_comm_a_roulette_to,
				'sv_comm_a_roulette_to'=>$sv_comm_a_roulette_to,
				'vv_comm_a_roulette_to'=>$vv_comm_a_roulette_to,
				
				'vig_share_blackjack_to'=>$vig_share_blackjack_to,
				'htv_share_blackjack_to'=>$htv_share_blackjack_to,
				'sv_share_blackjack_to'=>$sv_share_blackjack_to,
				'vv_share_blackjack_to'=>$vv_share_blackjack_to,
				'vv_comm_blackjack_to'=>$vv_comm_blackjack_to,
				'htv_comm_blackjack_to'=>$htv_comm_blackjack_to,
				'sv_comm_blackjack_to'=>$sv_comm_blackjack_to,
				'vig_comm_blackjack_to'=>$vig_comm_blackjack_to,
				
				'vig_share_slots_to'=>$vig_share_slots_to,
				'htv_share_slots_to'=>$htv_share_slots_to,
				'sv_share_slots_to'=>$sv_share_slots_to,
				'vv_share_slots_to'=>$vv_share_slots_to,
				'vig_comm_slots_to'=>$vig_comm_slots_to,
				'htv_comm_slots_to'=>$htv_comm_slots_to,
				'sv_comm_slots_to'=>$sv_comm_slots_to,
				'vv_comm_slots_to'=>$vv_comm_slots_to,
				);
		}
	}
	
	/**
	 * @todo get sharing and commission log details
	 * @author leokarl
	 * @since 2013-01-15
	 * @param string $baccarat
	 * @param string $roulette
	 * @param string $dragon_tiger
	 * @param string $blackjack
	 * @param string $american_roulette
	 * @param string $slots
	 */
	public function getSharingCommissionLogDetails($baccarat, $roulette, $dragon_tiger, $blackjack, $american_roulette, $slots, $casino_level){
		$log_details='';
	
		// Baccarat
		list($baccarat_before, $baccarat_after) = explode(":",$baccarat);
		list($costa_baccarat_before, $htv_baccarat_before, $sv_baccarat_before, $virtua_baccarat_before) = explode("#", $baccarat_before);
		list($costa_baccarat_after, $htv_baccarat_after, $sv_baccarat_after, $virtua_baccarat_after) = explode("#", $baccarat_after);
	
		//// CostaVegas999
		if($casino_level['vig']== 1 && $costa_baccarat_before <> $costa_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegass999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . number_format((float)$costa_baccarat_before,2) . '</font> to <font color=\"red\">' . number_format((float)$costa_baccarat_after,2) . '</font>';
		}
	
		//// HatienVegas999
		if($casino_level['htv']== 1 && $htv_baccarat_before <> $htv_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegass999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . number_format((float)$htv_baccarat_before,2) . '</font> to <font color=\"red\">' . number_format((float)$htv_baccarat_after,2) . '</font>';
		}
	
		//// SavanVegas999
		if($casino_level['sv']== 1 && $sv_baccarat_before <> $sv_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegass999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . number_format((float)$sv_baccarat_before,2) . '</font> to <font color=\"red\">' . number_format((float)$sv_baccarat_after,2) . '</font>';
		}
	
		//// VirtuaVegas999
		if($casino_level['vv']== 1 && $virtua_baccarat_before <> $virtua_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegass999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . number_format((float)$virtua_baccarat_before,2) . '</font> to <font color=\"red">' . number_format((float)$virtua_baccarat_after,2) . '</font>';
		}
	
		// Roulette
		list($roulette_before, $roulette_after) = explode(":",$roulette);
		list($costa_roulette_before, $htv_roulette_before, $sv_roulette_before, $virtua_roulette_before) = explode("#", $roulette_before);
		list($costa_roulette_after, $htv_roulette_after, $sv_roulette_after, $virtua_roulette_after) = explode("#", $roulette_after);
	
		//// CostaVegas999
		if($casino_level['vig']== 1 && $costa_roulette_before <> $costa_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegass999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$costa_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$costa_roulette_after,2) . '</font>';
		}
	
		//// HatienVegas999
		if($casino_level['htv']== 1 && $htv_roulette_before <> $htv_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegass999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$htv_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$htv_roulette_after,2) . '</font>';
		}
	
		//// SavanVegas999
		if($casino_level['sv']== 1 && $sv_roulette_before <> $sv_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegass999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$sv_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$sv_roulette_after,2) . '</font>';
		}
	
		//// VirtuaVegas999
		if($casino_level['vv']== 1 && $virtua_roulette_before <> $virtua_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegass999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$virtua_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$virtua_roulette_after,2) . '</font>';
		}
	
		// Dragon Tiger
		list($dragon_tiger_before, $dragon_tiger_after) = explode(":",$dragon_tiger);
		list($costa_dragon_tiger_before, $htv_dragon_tiger_before, $sv_dragon_tiger_before, $virtua_dragon_tiger_before) = explode("#", $dragon_tiger_before);
		list($costa_dragon_tiger_after, $htv_dragon_tiger_after, $sv_dragon_tiger_after, $virtua_dragon_tiger_after) = explode("#", $dragon_tiger_after);
	
		//// CostaVegas999
		if($casino_level['vig']== 1 && $costa_dragon_tiger_before <> $costa_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegass999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . number_format((float)$costa_dragon_tiger_before,2) . '</font> to <font color=\"red\">' . number_format((float)$costa_dragon_tiger_after,2) . '</font>';
		}
	
		//// HatienVegas999
		if($casino_level['htv']== 1 && $htv_dragon_tiger_before <> $htv_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegass999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . number_format((float)$htv_dragon_tiger_before,2) . '</font> to <font color=\"red\">' . number_format((float)$htv_dragon_tiger_after,2) . '</font>';
		}
	
		//// SavanVegas999
		if($casino_level['sv']== 1 && $sv_dragon_tiger_before <> $sv_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegass999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . number_format((float)$sv_dragon_tiger_before,2) . '</font> to <font color=\"red\">' . number_format((float)$sv_dragon_tiger_after,2) . '</font>';
		}
	
		//// VirtuaVegas999
		if($casino_level['vv']== 1 && $virtua_dragon_tiger_before <> $virtua_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegass999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . number_format((float)$virtua_dragon_tiger_before,2) . '</font> to <font color=\"red\">' . number_format((float)$virtua_dragon_tiger_after,2) . '</font>';
		}
	
		// Blackjak
		list($blackjack_before, $blackjack_after) = explode(":",$blackjack);
		list($costa_blackjack_before, $htv_blackjack_before, $sv_blackjack_before, $virtua_blackjack_before) = explode("#", $blackjack_before);
		list($costa_blackjack_after, $htv_blackjack_after, $sv_blackjack_after, $virtua_blackjack_after) = explode("#", $blackjack_after);
	
		//// CostaVegas999
		if($casino_level['vig']== 1 && $costa_blackjack_before <> $costa_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegass999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . number_format((float)$costa_blackjack_before,2) . '</font> to <font color=\"red\">' . number_format((float)$costa_blackjack_after,2) . '</font>';
		}
	
		//// HatienVegas999
		if($casino_level['htv']== 1 && $htv_blackjack_before <> $htv_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegass999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . number_format((float)$htv_blackjack_before,2) . '</font> to <font color=\"red\">' . number_format((float)$htv_blackjack_after,2) . '</font>';
		}
	
		//// SavanVegas999
		if($casino_level['sv']== 1 && $sv_blackjack_before <> $sv_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegass999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . number_format((float)$sv_blackjack_before,2) . '</font> to <font color=\"red\">' . number_format((float)$sv_blackjack_after,2) . '</font>';
		}
	
		//// VirtuaVegas999
		if($casino_level['vv']== 1 && $virtua_blackjack_before <> $virtua_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegass999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . number_format((float)$virtua_blackjack_before,2) . '</font> to <font color=\"red\">' . number_format((float)$virtua_blackjack_after,2) . '</font>';
		}
	
		// American Roulette
		list($american_roulette_before, $american_roulette_after) = explode(":",$american_roulette);
		list($costa_american_roulette_before, $htv_american_roulette_before, $sv_american_roulette_before, $virtua_american_roulette_before) = explode("#", $american_roulette_before);
		list($costa_american_roulette_after, $htv_american_roulette_after, $sv_american_roulette_after, $virtua_american_roulette_after) = explode("#", $american_roulette_after);
	
		//// CostaVegas999
		if($casino_level['vig']== 1 && $costa_american_roulette_before <> $costa_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegass999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$costa_american_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$costa_american_roulette_after,2) . '</font>';
		}
	
		//// HatienVegas999
		if($casino_level['htv']== 1 && $htv_american_roulette_before <> $htv_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegass999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$htv_american_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$htv_american_roulette_after,2) . '</font>';
		}
	
		//// SavanVegas999
		if($casino_level['sv']== 1 && $sv_american_roulette_before <> $sv_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegass999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$sv_american_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$sv_american_roulette_after,2) . '</font>';
		}
	
		//// VirtuaVegas999
		if($casino_level['vv']== 1 && $virtua_american_roulette_before <> $virtua_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegass999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . number_format((float)$virtua_american_roulette_before,2) . '</font> to <font color=\"red\">' . number_format((float)$virtua_american_roulette_after,2) . '</font>';
		}
	
		// Slots
		list($slots_before, $slots_after) = explode(":",$slots);
		list($costa_slots_before, $htv_slots_before, $sv_slots_before, $virtua_slots_before) = explode("#", $slots_before);
		list($costa_slots_after, $htv_slots_after, $sv_slots_after, $virtua_slots_after) = explode("#", $slots_after);
	
		//// CostaVegas999
		if($casino_level['vig']== 1 && $costa_slots_before <> $costa_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegass999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . number_format((float)$costa_slots_before,2) . '</font> to <font color=\"red\">' . number_format((float)$costa_slots_after,2) . '</font>';
		}
	
		//// HatienVegas999
		if($casino_level['htv']== 1 && $htv_slots_before <> $htv_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegass999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . number_format((float)$htv_slots_before,2) . '</font> to <font color=\"red\">' . number_format((float)$htv_slots_after,2) . '</font>';
		}
	
		//// SavanVegas999
		if($casino_level['sv']== 1 && $sv_slots_before <> $sv_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegass999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . number_format((float)$sv_slots_before,2) . '</font> to <font color=\"red\">' . number_format((float)$sv_slots_after,2) . '</font>';
		}
	
		//// VirtuaVegas999
		if($casino_level['vv']== 1 && $virtua_slots_before <> $virtua_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegass999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . number_format((float)$virtua_slots_before,2) . '</font> to <font color=\"red\">' . number_format((float)$virtua_slots_after,2) . '</font>';
		}
	
	
		return $log_details;
	}
}