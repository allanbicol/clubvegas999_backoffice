//active menu color
document.getElementById('mnu_user_account').style.color="#5A0000";
document.getElementById('mnu_user_account').style.fontWeight="bold";

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}
function checkIt(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        status = "This field accepts numbers only.";
        return false;
    }
    status = "";
    return true;
}
function get_cookie(cookie_name)
 {
   var results = document.cookie.match ('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

   if (results)
     return (unescape(results[2]));
   else
     return null;
 }

var btype=0;
function submitNewUser(actionURL,locationURL) {
	var s_enable=0;
	if (trim(document.getElementById('txtAccountID').value)==''){document.getElementById('lblAccountID').innerHTML='Required field!';document.getElementById('txtAccountID').focus(); return false;}else{document.getElementById('lblAccountID').innerHTML='*';}
	if (trim(document.getElementById('txtAccountName').value)==''){document.getElementById('lblAccountName').innerHTML='Required field!';document.getElementById('txtAccountName').focus(); return false;}else{document.getElementById('lblAccountName').innerHTML='*';}
	if(s_edit==0){
		if (trim(document.getElementById('txtPassword').value)==''){document.getElementById('lblPassword').innerHTML='Required field!';document.getElementById('txtPassword').focus(); return false;}else{document.getElementById('lblPassword').innerHTML='*';}
	}
	
	
	if (document.getElementById('txtAccountID').value=='' || document.getElementById('txtAccountName').value==''){return false;}
	if (document.getElementById('chkEnable').checked==true){s_enable=1;}else{s_enable=0;}
	//var s_enable
	jQuery.ajax({
		url: actionURL,
		method: 'POST',
		data: {
    		'AccountID': document.getElementById('txtAccountID').value, 
    		'AccountName' : document.getElementById('txtAccountName').value, 
    		'Password' : document.getElementById('txtPassword').value,
    		'IsEnable' : s_enable,
    		'Level' : document.getElementById('cmbLevel').value,
    		'MaxDeposit' : document.getElementById('txtMaxDeposit').value,
    		'MaxWithdraw' : document.getElementById('txtMaxWithdraw').value,
    		's_edit': s_edit,
    		},
		context: '',
		success: function(msg) {
			 var data=msg.split(';');
			 if (data[0]==1){document.getElementById('lblAccountID').innerHTML='Account ID is already exist!';document.getElementById('txtAccountID').focus(); return false;}
			 //if (data[1]==1){document.getElementById('lblAccountName').innerHTML='Acount name is already exist!';document.getElementById('txtAccountName').focus(); return false;}
			 if (btype==1){
				if(s_edit==0){
    				 clearForm();
    			}else{
        			document.getElementById('txtErrMessage').value='Account saved!';
        		}
    		 }else{
        		 window.location=locationURL;
        	 }
    	}
	});
}
function clearForm(){
	document.getElementById('txtAccountID').value=''; 
	document.getElementById('txtAccountName').value=''; 
	document.getElementById('txtPassword').value='';
	document.getElementById('txtMaxDeposit').value='';
	document.getElementById('txtMaxWithdraw').value='';
	document.getElementById('chkEnable').checked=true;
}
function callCancel(locationURL) 
{
	window.location=locationURL;
}