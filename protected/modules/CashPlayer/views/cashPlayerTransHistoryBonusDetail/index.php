<!--@TODO: VIEWS FOR CASH PLAYER TRANSACTION HISTORY FOR BONUS DETAIL
    @AUTHOR: Allan
    @SINCE: 05082012
-->
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

<script type="text/javascript">
document.getElementById('cashplayerHeader').className="start active";
document.getElementById('mnu_trans1').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Bonus Transaction History Summary</a></li>");

function showFundpage() 
{
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund1";
}
function goBack()
{
	var dateFrom = (document.getElementById('datefrom').value).split("-");
	
	var monthF = dateFrom[1];
	var dayF = dateFrom[2];
	var yearF = dateFrom[0];
	var dateF=(yearF + "/" + monthF + "/" + dayF);
	var dateTo =(document.getElementById('dateto').value).split("-");
	var monthT = dateTo[1];
	var dayT = dateTo[2];
	var yearT = dateTo[0];
	if (dayT.length==1){
		dayT="0"+dayT;
		}
	if (dayF.length==1){
		dayF="0"+dayF;
		}
	if (monthT.length==1){
		monthT="0"+monthT;
		}
	if (monthF.length==1){
		monthF="0"+monthF;
		}
	var dateT= (yearT + "/" + monthT + "/" +dayT);
	var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
	var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryBonus1&dateFrom="+enc(dateSubmitFrom)+"&dateTo="+enc(dateSubmitTo)+"&id="+enc(document.getElementById('s_id').value);
}
</script>

<script type="text/javascript">

	var cRowNo=0;
	function tableTransHistoryToday(method_id,dateFrom,dateTo,accountID) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryBonusDetail/CashPlayerTransHistoryBonusDetail&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Transaction Number', 'Type','Currency','Amount','Bonus Type','Bonus Before','Bonus After','Submit Time','Finish Time','Cage Cashier Account','Status'],
			    colModel: [
					{name: 'transNumber', index: 'transNumber', width: 120, search:true,title:false},
					{name: 'transStatus', index: 'transStatus', width: 100,title:false},
					{name: 'currencyName', index: 'currencyName', width: 70,title:false},
					{name: 'Amount', index: 'Amount', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'trans_via', index: 'trans_via', width: 120,title:false},
					{name: 'balance_before', index: 'balance_before', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'balance_after', index: 'balance_after', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'submitTime', index: 'submitTime', width: 130, align:"right",title:false},
					{name: 'finishTime', index: 'finishTime', width: 130, align:"right",title:false},
					{name: 'cashierID', index: 'cashierID', width: 130, align:"right",title:false},
					{name: 'status', index: 'status', width: 100, align:"right",title:false},
			    ],
			   	loadtext:"",
			   	loadComplete: function() {
					var grid=jQuery("#list1");
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"balance_before","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"balance_after","",{background:'#cfe5fa'});
					}   
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    rowNum: 20,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'transNumber',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> BONUS HISTORY - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    onCellSelect :  function(rowid, index, contents, event) {
			    	var userlever='<?php echo Yii::app()->user->checkAccess('cashPlayer.writeTransHistorySummary');?>';
			    	if(userlever=='1'){
					    if (index==4){
					    	var myrow = grid.jqGrid('getRowData', rowid);
						    $createAlterBankName.dialog('open');
						   	document.getElementById('transNum').value=myrow.transNumber;
							document.getElementById('oldBonus').value=myrow.trans_via;
					    }
			    	}
			    }
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
		});
	} 

</script>
<script type="text/javascript">
var $createAlterBankName='';
$(document).ready(function() {
	$createAlterBankName = $('<div></div>')
		.html('<div>'
		 +'<form name="myForm"   action="" >'
		 +'<table id="tblDepositConfirmBody" width="100%" border="0" cellpadding="0" cellspacing="0">'
		 +'<tr><td class="row"></td></tr>'
		 +'<tr><td class="row"><b>Trans No.</b></td><td><input id="transNum" name="transNum" class="depConfirmInput"  readonly="true"/></td></tr>'
		 +'<tr><td class="row"><b>From: </b></td><td><input id="oldBonus" name="oldBonus" class="depConfirmInput"  readonly="true"/></td></tr>'
		 +'<tr><td class="row"><b>To: </b></td><td class="row1"><select class="bonusName" id="optBonusName" name="optBonusName" style="width:205px"></select></td></tr>'
		 +'<tr><td class="row"></td><td class="row1"><input id="btnOk" onclick="updateBonusItem();" type="button" value="Update"/></td></tr>'
		 +'</table></center>'
		 +'</form> </div>'
			)
		.dialog({
			autoOpen: false,
			width: 350,
			title: 'Update Bonus Item',
			resizable: false,
			modal: true,
			cache: false,
			close: function(event, ui){ 
		        location.reload(true); 
		    }
		});
		$('.bonusName').ready(function(event) {
			jQuery.ajax({
	   		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryBonusDetail/GetBonusItem',
	   		type: 'POST',
	   		data: '',
	   		context: jQuery(this).val(),
	   		success: function(data) {
	   			 document.getElementById('optBonusName').options.length=0;
	   			 var newOpt = data.split(',');
	   			 var selbox = document.getElementById('optBonusName');
	   			 var i=0;
	   			 for (i=0;i<=newOpt.length-2;i++)
	   			 {
		    			 var spltOpt = newOpt[i].split(':');
	   				 selbox.options[selbox.options.length] = new Option(spltOpt[0],spltOpt[1]);
	       		 }
		    	}
	   		});
		});
});

function updateBonusItem(){
	var oldBonus=document.getElementById('oldBonus').value;
	var newBonus=document.getElementById('optBonusName').value;
	var transNumber=document.getElementById('transNum').value;
	if (newBonus!=null && newBonus!="")
	  {
		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerAlterBankName/AlterBankName',
    		type: 'POST',
    		datatype:'json',
    		data: {'transNumber':transNumber, 'oldBankName': oldBonus, 'newBankName': newBonus},
    		context: '',
    		success: function(data) {
		    		if (data=='suc'){
			    		alert('Bank name updated!');
		    		}else if (data=='not'){
	    				return false;
			    	}else if (data=='err'){
	    				alert('You dont have permission to alter bank name.');
	    			}
		    		$createAlterBankName.dialog('close');
	    			$('#list1').trigger("reloadGrid");
		    	}
    	});
	  } else{
		  alert('Please select Bank name.');
	 }
}
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
 
function decryptparam()
{
	var strF = document.getElementById('f').value;
	var strT= document.getElementById('t').value;
	var encodeF = enc(strF);
	var encodeT = enc(strT);
	
	var dateF= encodeF.split("_");	
	var dateFrom=dateF[0];
	var dateT= encodeT.split("_");	
	var dateTo=dateT[0];
	var dateFSplit=dateFrom.split("/");
	var dateTSplit=dateTo.split("/");
	
	month1=dateFSplit[1].toString();
	day1=dateFSplit[2].toString();

	if (month1.length==1){
		month1='0'+month1;
	}
	if (day1.length==1){
		day1='0'+day1;
	}
	month2=dateTSplit[1].toString();
	day2=dateTSplit[2].toString();
	
	if (month2.length==1){
		month2='0'+month2;
	}
	if (day2.length==1){
		day2='0'+day2;
	}
	document.getElementById("datefrom").value=dateFSplit[0]+"-"+month1+"-"+day1;
	document.getElementById("dateto").value=dateTSplit[0]+"-"+month2+"-"+day2;

}
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

	function loadTransTable(selected_method,dateFrom,dateTo,accountID){
		if (selected_method=='ALL'){
			//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID);
	    	
		}
	    else
	    {
	    	//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID);
	    	
		}
	}

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable("ALL",datefrom,dateto,accountID);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;

			loadTransTable(methodtype,datefrom,dateto,accountID);
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			month=themonth.toString();
			day=theday.toString();
			
			if (month.length==1){
				month='0'+month;
			}
			if (day.length==1){
				day='0'+day;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+month+"-"+day);
			document.getElementById('dateto').value=(theyear+"-"+month+"-"+day);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(methodtype,datefrom,dateto,accountID);
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			
			loadTransTable(methodtype,dateSubmitFrom,dateSubmitTo,accountID);
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>

</head>
<body onload="javascript:decryptparam(); btnclick('Submit');">
<div id="parameter_area" style="width: 500px">
	<div class="header" >Cash Player Transaction History</div>
	<form action="">
		<table style="background-color:transparent;  width: 500px;">
		<tr><td style=" height: 35px; border-bottom: groove; ">
		<input class="btn red" type="button" value="FUND" onclick="showFundpage()">
		<input class="btn red disabled" type="button" value="BONUS" >
		<input type="hidden" id="txtAccountID" value="<?php echo $_GET['account_id'];?>" >
		</td></tr>
		</table>
		<table>
		<tr><td style="padding-left: 5px;">METHOD:</td>
			<td>
			<select id="MethodType">
				<option value="ALL">All</option>
  				<option value="1">Add</option>
  				<option value="2">Remove</option>
  				
			</select>
			</td>
		</tr>
		<tr><td width="30%" style="padding-left: 5px;">FROM:</td>
			<td>
			<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px; background-color: transparent;width: 50px" disabled>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">TO:</td>
			<td>
			<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; background-color: transparent;width: 50px" disabled>
			</td>
		</tr>
		</table>
		</br>
		<div align="center" style="height: 35px; v-align: middle;">
		<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
		<input onclick="javascript:goBack();" type="button" value="Back" class="btn red"></div>

		</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
		document.getElementById("dateto").value=(day + "-" + month + "-" + year);
		</script>
</div>		
	<div id="qry_result">
	</div>
	<div id="pager1"></div>
	<br/>
	<!--  
	<div id="qry_resultTotal">
	</div>-->
	<div><input id="f" value="<?php echo $_GET['f'];?>" hidden=true><input id="t" value="<?php echo $_GET['t'];?>" hidden=true><input id="s_id" value="<?php echo $_GET['s_id'];?>" hidden=true></div>
	
<?php 
	// Dialog----------------------------------------------------------------
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'alter-dialog',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Update Trans Item',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> true,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewAlterItem"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>

</body>
</html>