<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('mnu_casino_info').style.color="#5A0000";
	document.getElementById('mnu_casino_info').style.fontWeight="bold";
</script>
<script type="text/javascript">
	var cRowNo=0;
	function tableCasinoInformation(casino_id,currency_id,player_type,dateFrom,dateTo,txtPerHTV,txtPerSavan,txtPerCosta) { 
		
		var divTag3 = document.createElement("Table"); 
	    divTag3.id = 'list1'; 
	    divTag3.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag3);

	    var divTag4 = document.createElement("div"); 
	    divTag4.id = 'pager1'; 
	    divTag4.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag4);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/CasinoInformation/CasinoInformation&castype='+casino_id+'&curtype='+currency_id+'&platype='+player_type+'&datefrom='+dateFrom+'&dateto='+dateTo+'&htv='+txtPerHTV+'&sav='+txtPerSavan+'&cos='+txtPerCosta, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Casinos','Currency','Win/Loss','Commission','Company Win/Loss','Agent Win/Loss','Revenue Sharing(%)','Amount to Pay','Net Total'],
			    colModel: [
					{name: 'casino_name', index: 'casino_name', width: 120, search:true,title:false,summaryType:'count', summaryTpl : 'Total',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
					{name: 'currency_name', index: 'currency_name', width: 70,title:false},
					{name: 'winloss', index: 'winloss', width: 120,title:false,sorttype:'number', summaryType:'sum', align:"right",formatter:'number',hidden:true},
					{name: 'commission', index: 'commission', width: 120, align:"right",title:false,sorttype:'number', summaryType:'sum',hidden:true},
					{name: 'total', index: 'total', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum'},
					{name: 'agenttotal', index: 'agenttotal', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum'},
					{name: 'percentage', index: 'percentage', width: 130, align:"right",title:false,sorttype:'number',formatter:'integer', summaryType:'sum'},
					{name: 'amount_to_pay', index: 'amount_to_pay', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum',sortable:false},
					{name: 'net_total', index: 'net_total', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum',sortable:false},
			    ],
			    loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list1");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFundDetail&account_id="+text;
			                }
			                e.preventDefault();
			            });
			        } 
			        $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			     	
			    },
			      
			    //rowNum: 10,	
			    //rowList: [10, 20, 30],
			    //pager: '#pager1',
			    
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' CASINO INFORMATION - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
			   	groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   
			   	forceFit : true,
				cellEdit: true,
				footerrow:true,
				hidegrid: false,
				
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
			
		});
	
	} 
	function tableCasinoInformationTotal(casino_id,currency_id,player_type,dateFrom,dateTo,txtPerHTV,txtPerSavan,txtPerCosta) { 
		
		var divTag3 = document.createElement("Table"); 
	    divTag3.id = 'list2'; 
	    divTag3.style.margin = "0px auto"; 
	    document.getElementById("qry_resultTotal").appendChild(divTag3);

	    var divTag4 = document.createElement("div"); 
	    divTag4.id = 'pager2'; 
	    divTag4.style.margin = "0px auto"; 
	    document.getElementById("qry_resultTotal").appendChild(divTag4);

	    $(document).ready(function() {
			var grid=jQuery("#list2");
			
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/CasinoInformation/CasinoInformationTotal&castype='+casino_id+'&curtype='+currency_id+'&platype='+player_type+'&datefrom='+dateFrom+'&dateto='+dateTo+'&htv='+txtPerHTV+'&sav='+txtPerSavan+'&cos='+txtPerCosta, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Casinos','Currency','Win/Loss','Commission','Company Win/Loss','Agent Win/Loss','Revenue Sharing(%)','Amount to Pay','Net Total','Exchange Rate','Win/Loss','Commission','Total','Amount to Pay','Net Total'],
			    colModel: [
					{name: 'casino_name', index: 'casino_name', width: 120, search:true,title:false,summaryType:'count', summaryTpl : '<label style="color:red">Total<label>'},
					{name: 'currency_name', index: 'currency_name', width: 70,title:false},
					{name: 'winloss', index: 'winloss', width: 120,title:false,sorttype:'number',formatter:'number', summaryType:'sum', align:"right",hidden:true},
					{name: 'commission', index: 'commission', width: 120, align:"right",formatter:'number',title:false,sorttype:'number', summaryType:'sum',hidden:true},
					{name: 'total', index: 'total', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum'},
					{name: 'agenttotal', index: 'agenttotal', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum'},
					{name: 'revenue_sharing', index: 'revenue_sharing', width: 130, align:"right",title:false,sorttype:'number',formatter:'integer', summaryType:'sum',editable:true,editrules:{number:true},sortable:false},
					{name: 'amount_to_pay', index: 'amount_to_pay', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum',sortable:false},
					{name: 'net_total', index: 'net_total', width: 150, align:"right",title:false,sorttype:'number',formatter:'number', summaryType:'sum',sortable:false},
					{name: 'exchange_rate', index: 'exchange_rate', hidden:true },
					{name: 'winloss1', index: 'winloss',formatter:'number1', hidden:true},
					{name: 'commission1', index: 'commission1', formatter:'number', hidden:true},
					{name: 'total1', index: 'total',formatter:'number1', hidden:true},
					{name: 'amount_to_pay1', index: 'amount_to_pay1',formatter:'number', hidden:true},
					{name: 'net_total1', index: 'net_total1',formatter:'number', hidden:true},
				    ],
			    loadComplete: function() {
			        var myGrid = $("#list2");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFundDetail&account_id="+text;
			                }
			                e.preventDefault();
			            });
			        } 
			        var grid = $("#list2"),
			       
			        sumAmountToPay = grid.jqGrid('getCol', 'amount_to_pay', false, 'sum');
			        sumWinLoss=grid.jqGrid('getCol', 'winloss', false, 'sum');
			        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
			        sumTotal=grid.jqGrid('getCol', 'total', false, 'sum');
			        sumNetTotal=grid.jqGrid('getCol', 'net_total', false, 'sum');

			    	grid.jqGrid('footerData','set', {casino_name: '<label style="color:#8A590B">OVERALL TOTAL</label>',winloss:sumWinLoss,commission:sumCommission,total:sumTotal, amount_to_pay: sumAmountToPay,net_total:sumNetTotal});  
			    },
			   	loadtext:"",   
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: '<label style="color:red"><b>[TOTAL]<b></label>&nbsp;&nbsp;&nbsp;Amount to pay to casino in </label><select id="currencyTypeConvert" style="width:80px" onchange="javascript: convert();"><option>--</option><?php $dataReader = AgentCurrencyType::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>',
			    viewrecords: true,
			    grouping: true,
			   	groupingView : {
			   		groupField : ['casino_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],

					groupOrder: ['asc'],

					groupDataSorted : true
			   	},
			   
			   	forceFit : true,
				cellEdit: true,
				footerrow:true,
				hidegrid: false,
			});
			$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search: false});
			
		});
	
	} 
    function convert(){
    	var comboValue=document.getElementById("currencyTypeConvert").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	
		
    
		var noRow=$("#list2").getGridParam("reccount");
		var rate;
		var winloss1;
		var commission;
		var total;
		var amountToPay;
		var netTotal;
		var totalConversion;
		var winlossConversion;
		var commissionConversion;
		var amountToPayConversion;
		var netTotalConversion;
		
	    var i=1;
	    while (i<=noRow)
	      {
	    	rate=$("#list2").getCell(i, 'exchange_rate');
	    	winloss1=$("#list2").getCell(i, 'winloss1');
	    	commission=$("#list2").getCell(i, 'commission1');
			total=$("#list2").getCell(i, 'total1');
			amountToPay=$("#list2").getCell(i, 'amount_to_pay1');
			netTotal=$("#list2").getCell(i, 'net_total1');
			
	    		winlossConversion=((parseFloat(winloss1)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	commissionConversion=((parseFloat(commission)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		amountToPayConversion=((parseFloat(amountToPay)/parseFloat(rate)) * parseFloat(comboRange));
	    		netTotalConversion=((parseFloat(netTotal)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list2").jqGrid('setCell', i, 1, comboName);
	    		jQuery("#list2").jqGrid('setCell', i, 2, winlossConversion);
	    		jQuery("#list2").jqGrid('setCell', i, 3, commissionConversion);
	    		jQuery("#list2").jqGrid('setCell', i, 4, totalConversion);
	    		jQuery("#list2").jqGrid('setCell', i, 7, amountToPayConversion);
	    		jQuery("#list2").jqGrid('setCell', i, 8, netTotalConversion);	
			
	     	i++;
	      }
		var grid = $("#list2"),
        sumAmountToPay = grid.jqGrid('getCol', 'amount_to_pay', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'winloss', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total', false, 'sum');
        sumNetTotal=grid.jqGrid('getCol', 'net_total', false, 'sum');
        grid.jqGrid('footerData','set', {casino_name: '<label style="color:#8A590B">OVERALL TOTAL IN</label>',currency_name:'<label style="color:#8A590B">'+comboName+'</label>',winloss:sumWinLoss,commission:sumCommission,total:sumTotal, amount_to_pay: sumAmountToPay,net_total:sumNetTotal});  
      
	}
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	});

	function checkIt(evt) {
	    evt = (evt) ? evt : window.event
	    var charCode = (evt.which) ? evt.which : evt.keyCode
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        status = "This field accepts numbers only.";
	        return false;
	    }
	    status = "";
	    return true;
	}

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");
			var txtPerHTV=document.getElementById('txtPerHTV').value;
			var txtPerSavan=document.getElementById('txtPerSavan').value;
			var txtPerCosta=document.getElementById('txtPerCosta').value;
			
			document.getElementById('qry_result').innerHTML='';
			tableCasinoInformation("All","All","All",datefrom,dateto,txtPerHTV,txtPerSavan,txtPerCosta);
			document.getElementById('qry_resultTotal').innerHTML='';
			tableCasinoInformationTotal("All","All","All",datefrom,dateto,txtPerHTV,txtPerSavan,txtPerCosta);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
	
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");
			document.getElementById('datefrom').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var casinotype=document.getElementById('casinoType').value;
			var currencytype=document.getElementById('currencyType').value;
			var playertype=document.getElementById('playerType').value;
			var txtPerHTV=document.getElementById('txtPerHTV').value;
			var txtPerSavan=document.getElementById('txtPerSavan').value;
			var txtPerCosta=document.getElementById('txtPerCosta').value;
			
			document.getElementById('qry_result').innerHTML='';
			tableCasinoInformation(casinotype,currencytype,playertype,datefrom,dateto,txtPerHTV,txtPerSavan,txtPerCosta);
			document.getElementById('qry_resultTotal').innerHTML='';
			tableCasinoInformationTotal(casinotype,currencytype,playertype,datefrom,dateto,txtPerHTV,txtPerSavan,txtPerCosta);
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			
			var datefrom=(theyear+"-"+themonth+"-"+theyday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theyday+ " 23:59:59");
			document.getElementById('datefrom').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var casinotype=document.getElementById('casinoType').value;
			var currencytype=document.getElementById('currencyType').value;
			var playertype=document.getElementById('playerType').value;
			var txtPerHTV=document.getElementById('txtPerHTV').value;
			var txtPerSavan=document.getElementById('txtPerSavan').value;
			var txtPerCosta=document.getElementById('txtPerCosta').value;
			
			document.getElementById('qry_result').innerHTML='';
			tableCasinoInformation(casinotype,currencytype,playertype,datefrom,dateto,txtPerHTV,txtPerSavan,txtPerCosta);
			document.getElementById('qry_resultTotal').innerHTML='';
			tableCasinoInformationTotal(casinotype,currencytype,playertype,datefrom,dateto,txtPerHTV,txtPerSavan,txtPerCosta);
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[0];
			var yearF = dateFrom[2];
			var dateF=(yearF + "-" + monthF + "-" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[0];
			var yearT = dateTo[2];
			var dateT= (yearT + "-" + monthT + "-" +dayT);
			var dateSubmitFrom= (dateF+" "+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ " "+ document.getElementById('cbHourto').value +":59:59");
			var casinotype=document.getElementById('casinoType').value;
			var currencytype=document.getElementById('currencyType').value;
			var playertype=document.getElementById('playerType').value;
			var txtPerHTV=document.getElementById('txtPerHTV').value;
			var txtPerSavan=document.getElementById('txtPerSavan').value;
			var txtPerCosta=document.getElementById('txtPerCosta').value;
		
			document.getElementById('qry_result').innerHTML='';
			tableCasinoInformation(casinotype,currencytype,playertype,dateSubmitFrom,dateSubmitTo,txtPerHTV,txtPerSavan,txtPerCosta);
			document.getElementById('qry_resultTotal').innerHTML='';
			tableCasinoInformationTotal(casinotype,currencytype,playertype,dateSubmitFrom,dateSubmitTo,txtPerHTV,txtPerSavan,txtPerCosta);
			
		}
	}


</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/3d-loader.gif");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
<script language="Javascript"> 


function openNewWindow(fileName,windowName,theWidth,theHeight)
{
      window.open(fileName,windowName,"toolbar=0,location=0,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width="+theWidth+",height="+theHeight)
} 


</script> 
<body onload="javascript: btnclick('Load');">

<div style="position: relative;left: 5px; top:3px">
			<div style="position: relative;left: 0px; top:3px;background-color: #9C9A9A; width: 500px; height: 275px;border-radius: 3px;">
			<div style="position: relative;left: 3px; top:3px;background-color: black; width: 494px;height: 25px;border-radius: 3px;text-align:justify;"><h3>Clubvegas Report Format by Casinos</h3></div>
			<div style="position: relative;left: 3px; top:5px;background-color: #807D7D; width: 494px;height:242px;border-radius: 3px;">
			<div style="position: relative;left: 10px; top:5px;background-color: #807D7D;width: 480px">
			<table style="width: 100%">
				<tr><td style="width: 30%; color: white"><b>CASINOS</b></td>
				<td style="width: 70%">
					<select id="casinoType" style="width: 200px">
							<option value="All">All</option>
			  				<?php 
							$dataReader = CasinoType::model()->findAll();
							foreach ($dataReader as $row){
								echo '<option value="' . $row['casino_code'] . '">'. $row['casino_name'] . '</option>';
							}
							?>
						</select>
				</td>
			</tr>
			<tr><td style="width: 30%; color: white"><b>CURRENCY</b></td>
				<td>
					<select id="currencyType">
						<option value="All">ALL</option>
		  				<?php 
						$dataReader = AgentCurrencyType::model()->findAll();
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['currency_name']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr><td style="width: 30%; color: white"><b>PLAYER TYPE</b></td>
				<td>
				<select id="playerType">
						<option value="All">All</option>
						<option value="c">Cash Player</option>
						<option value="a">Agent Player</option>
				</select>
				</td>
			</tr>
			<tr><td style="width: 20%; color: white">DATE FROM</td>
				<td style="width: 80%;">
				<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />
				<select id="cbHourfrom">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
							foreach ($hHour as  $value){
							echo '<option>'.$value.'</option>';
							}
						?></select><input value=":00:00" style="border: 0px;width:50px; background-color: transparent; color: white;" disabled>
				</td>
			</tr>
			<tr><td style="width: 30%; color: white">DATE TO</td>
				<td>
				<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
					<select id="cbHourto">
						<?php 
							$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
							foreach ($hHour as  $value){
							echo '<option selected="true">'.$value.'</option>';
							}
						?></select><input value=":59:59"  style="border: 0px;width:50px; background-color: transparent; color: white;" disabled>
				</td>
			</tr>
			</table>
			<div style="background-color: transparent;width: 200;height:20px; position: relative; color: Black"><b>Set Revenue Sharing:</b></div>
			<div style="background-color: transparent;width: 200;height:26px; position: relative">
			<?php 
				$dataReader = CasinoType::model()->findAll();
				echo '<div style="color:white;">&nbsp;&nbsp;HTV999&nbsp;';
				foreach ($dataReader as $row){
					if ($row['casino_code']=='cv999_1'){
						echo '<input onKeyPress="return checkIt(event)" style="width:30px;" type="text" id="txtPerHTV" name="txtPerHTV" value="' .$row['revenue_sharing']. '"></input>' ;
					}
					elseif ($row['casino_code']=='cv999_2'){
						echo '&nbsp;SavanVegas999&nbsp;<input onKeyPress="return checkIt(event)" style="width:30px;" type="text" id="txtPerSavan" name="txtPerSavan" value="'.$row['revenue_sharing'].'"></input>';
					}
					elseif ($row['casino_code']=='cv999_3'){
						echo '&nbsp;CostaVegas999&nbsp;<input onKeyPress="return checkIt(event)" style="width:30px;" type="text" id="txtPerCosta" name="txtPerCosta" value="'.$row['revenue_sharing'].'"></input></div>';
					}
				}
				?>
			
			</div>
			<hr>
			<div align="center">
				<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btnclickMe">
				<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btnclickMe">
				<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btnclickMe">
				<input onclick="javascript:history.go(-1);" type="button" value="Back"></div>
			</div>
			</div>
		</div>
		
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
		document.getElementById("dateto").value=(day + "-" + month + "-" + year);
		</script>
		<br/>
		
	<div id="qry_result"></div>
	
	<div id="qry_resultTotal"></div>
	<a HREF="javascript:parent.openNewWindow('http://www.yahoo.com', 'WINDOW_NAME', 640, 500)">link name</a>	
</div>
</body>
</html>