<?php
class TableCashPlayerTransactionHistory extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player_trans_history';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}
