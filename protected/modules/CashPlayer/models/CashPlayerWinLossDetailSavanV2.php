<?php

class CashPlayerWinLossDetailSavanV2
{

	public function getPlayerWinLossDetailSavan($orderField, $sortType, $startIndex, $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_info_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_info_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_info_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("SELECT CH.id,CONCAT(table_name,'|',shoe_info_id,'|',game_info_id) AS table_shoe_game,end_time,CH.account_id,CP.account_name,currency,game_type,valid_stake,stake_amount,banker_result AS result,win_loss,win_loss AS winloss1,CH.commission,tips AS amount_tips,total_win_loss,balance_before,balance_after,'Savanvegas999' AS casino_name,'1' AS counter,ip AS IP FROM  tbl_savan_casino_history_v2 AS CH LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE CH.account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." AND  end_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			$command = $connection->createCommand("SELECT CH.id,CONCAT(table_name,'|',shoe_info_id,'|',game_info_id) AS table_shoe_game,end_time,CH.account_id,CP.account_name,currency,game_type,valid_stake,stake_amount,banker_result AS result,win_loss,win_loss AS winloss1,CH.commission,tips AS amount_tips,total_win_loss,balance_before,balance_after,'Savanvegas999' AS casino_name,'1' AS counter,ip AS IP FROM  tbl_savan_casino_history_v2 AS CH LEFT JOIN tbl_cash_player AS CP ON CH.account_id = CP.account_id WHERE CH.account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." AND game_type_id='".$_GET['methodId']."' AND  end_time BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountPlayerWinLossDetailSavan()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_info_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_info_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_info_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
	if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select COUNT(0) from tbl_savan_casino_history_v2 where   account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' "  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(0) from tbl_savan_casino_history_v2 where   account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type_id='".$_GET['methodId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getPlayerWinLossDetailSavanAllPages()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_POST['tableId']<>""){
			$andTableId="and table_info_id="."'".htmlEntities($_POST['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_POST['gameId']<>""){
			$andGameId="and game_info_id="."'".htmlEntities($_POST['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_POST['shoesId']<>""){
			$andShoesId="and shoe_info_id="."'".htmlEntities($_POST['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_POST['methodId']=="ALL")
		{
			$command = $connection->createCommand("SELECT COUNT(id) AS bet_count,FORMAT(SUM(valid_stake),2) AS available_bet,FORMAT(SUM(stake_amount),2) AS bet_amount,FORMAT(IFNULL(SUM(win_loss),0),2) AS win_loss,FORMAT(IFNULL(SUM(commission),0),2) AS commission,FORMAT(IFNULL(SUM(tips),0),2) AS amount_tips,FORMAT((SUM(total_win_loss)),2) AS total_win_loss,FORMAT((SELECT balance_after FROM tbl_savan_casino_history_v2 AS CH1 WHERE CH1.id=MAX(CH.id)),2) AS current_balance FROM  tbl_savan_casino_history_v2 AS CH WHERE  account_id='".$_POST['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." AND  end_time BETWEEN '".$_POST['dateFrom']."' AND '".$_POST['dateTo']."'"  );
		}
		else{
			$command = $connection->createCommand("SELECT COUNT(id) AS bet_count,FORMAT(SUM(valid_stake),2) AS available_bet,FORMAT(SUM(stake_amount),2) AS bet_amount,FORMAT(IFNULL(SUM(win_loss),0),2) AS win_loss,FORMAT(IFNULL(SUM(commission),0),2) AS commission,FORMAT(IFNULL(SUM(tips),0),2) AS amount_tips,FORMAT((SUM(total_win_loss)),2) AS total_win_loss,FORMAT((SELECT balance_after FROM tbl_savan_casino_history_v2 AS CH1 WHERE CH1.id=MAX(CH.id)),2) AS current_balance FROM  tbl_savan_casino_history_v2 AS CH WHERE  account_id='".$_POST['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and game_type_id='".$_POST['methodId']."' and end_time between '".$_POST['dateFrom']."' and '".$_POST['dateTo']."'"  );
		}
	
		$rows = $command->queryRow();
		$betCount= $rows['bet_count'];
		$validBet= $rows['available_bet'];
		$betAmount= $rows['bet_amount'];
		$winloss= $rows['win_loss'];
		$commission= $rows['commission'];
		$total= $rows['total_win_loss'];
		$currentBalance=$rows['current_balance'];
	
		echo $betCount.'#'.$validBet.'#'.$betAmount.'#'.$winloss.'#'.$commission.'#'.$total.'#'.$currentBalance;
	
	
	}
	public function getSavanVegasResultDetails(){
		$row = TableSavanCasinoHistoryV2::model()->find(array('select'=>'result_imgname','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
		 
		return 'https://pic01.clubvegas999.com:8443/uploadmanager/app/downLoadService/getResource?'.$row['result_imgname'];
	}
	public function getSavanVegasWinLossResultDetails()
	{
		$row = TableSavanCasinoHistoryV2::model()->find(array('select'=>'game_type_id,currency,banker_result,live_member_report_details','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
	
		echo $this->getWinLossBettingDetail($row['game_type_id'], $row['banker_result'], $row['live_member_report_details'], $row['currency']);
		
	}
	
	public function getSavanVegasWinLossResultCards()
	{
		$row = TableSavanCasinoHistoryV2::model()->find(array('select'=>'game_type_id,banker_result,live_member_report_details','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
		
		$opt1='';$opt2='';
		if ($row['game_type_id']==1){
			$opt1='PLAYER';$opt2='BANKER';
			$index1 = 0;
			$index2 = 1;
			
		}
		if ($row['game_type_id']==4){
			$opt1='DRAGON';$opt2='TIGER';
			$index1 = 0;
			$index2 = 1;
		}
		
		if ($row['game_type_id']!=2){
			$result = explode("~", $row['banker_result']);
			$spl = json_decode($result[1]);
			//echo count($spl[0]);
			echo '<div style="float:left;padding-right:20px;"><div style="background-color:#929292;color:#FFF;text-align:center;margin-bottom:5px"><b>'.$opt1.'</b></div>';
			$margin=0;
			$deg = count($spl[1])>1 ? -5: 0;

			for ($i=0; $i<= count($spl[$index1])-1; $i++){
				//-webkit-transform: rotate('.$deg.'deg);-moz-transform: rotate('.$deg.'deg);
				echo '<img style="margin-left:'.$margin.'px;"  src="'.Yii::app()->request->baseUrl.'/images/cards/'.$spl[$index1][$i].'.png">';
				$margin=-59;
				$deg +=5;
			}
			
			echo '</div><div style="float:right"><div style="background-color:#929292;color:#FFF;text-align:center;margin-bottom:5px"><b>'.$opt2.'</b></div>';
			
			$margin1=0;
			$deg = count($spl[0])>1 ? -5: 0;
			for ($i=0; $i<= count($spl[$index2])-1; $i++){
				//-webkit-transform: rotate('.$deg.'deg);-moz-transform: rotate('.$deg.'deg);
				echo '<img style="margin-left:'.$margin1.'px;"  src="'.Yii::app()->request->baseUrl.'/images/cards/'.$spl[$index2][$i].'.png">';
				$margin1=-59;
				$deg +=5;
			}
			echo '</div>';
			//print_r($spl);
		}
	}
	
	public function getWinLossBettingDetail($game_type, $banker_result, $live_member_report_details, $currency){
		 
	
		// table header
		$table_data = '<table border="0" cellspacing="2" cellpadding="0" width="100%">'.
				'<tr><th class="c_winlossresultheader">Type</th><th class="c_winlossresultheader">Bet Amount</th><th class="c_winlossresultheader">Win/Loss</th></tr>';
		$winloss_result='';
		if($game_type == 1){ // baccarat

			$result=json_decode($live_member_report_details, true);

			foreach ($result as $item) {
				// copy item to grouped
				$grouped[$item['betType']][] = $item;
			}
				
			foreach($grouped as $key => $value){
				$groupResult = $grouped[$key];
				$type='';
				$winloss = 0;
				$betAmount = 0;
				$winlossTotal = 0;
				for ($i = 0; $i <= count($groupResult)-1; $i++){
					if ($key==$groupResult[$i]['betType']){
						$winloss=$groupResult[$i]['winLossAmount'] - $groupResult[$i]['betAmount'];
						$betAmount+= $groupResult[$i]['betAmount'];
						$winlossTotal+= $winloss;
					}
				}
			
				switch ($key) {
					case 'BC_BANKER':
    					$type = '<font color="red">BANKER</font>';
    					break;
    				case 'BC_PLAYER':
    					$type = '<font color="#000c75">PLAYER</font>';
    					break;
    				case 'BC_TIE':
    					$type = '<font color="black">TIE</font>';
    					break;
    				case 'BC_SMALL':
    					$type = '<font color="blue">SMALL</font>';
    					break;
    				case 'BC_BIG':
    					$type = '<font color="green">BIG</font>';
    					break;
    				case 'BC_BANKER_PAIR':
    					$type = '<font color="red">BANKER PAIR</font>';
    					break;
    				case 'BC_PLAYER_PAIR':
    					$type = '<font color="#000c75">PLAYER PAIR</font>';
    					break;
    				case 'TIPS':
    					$type = '<font color="black">TIPS</font>';
    					break;
				}
				if ($winlossTotal<0){
					$winloss_result='<font color="red" >' . money_format('%i',$winlossTotal) . '</font>';
				}
				if ($winlossTotal>0){
					$winloss_result='<font color="#000c75">' . money_format('%i',$winlossTotal) . '</font>';
				}
				$table_data .= '<tr><td class="c_bet_type">' .$type. '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $betAmount) . '</td><td class="c_win_loss">' . $winloss_result . '</td></tr>';
			}
 
		}else if($game_type == 4){ // dragon tiger

			$result=json_decode($live_member_report_details, true);
			foreach ($result as $item) {
				// copy item to grouped
				$grouped[$item['betType']][] = $item;
			}
			
			foreach($grouped as $key => $value){
				$groupResult = $grouped[$key];
				$type='';
				$winloss = 0;
				$betAmount = 0;
				$winlossTotal = 0;
				for ($i = 0; $i <= count($groupResult)-1; $i++){
					if ($key==$groupResult[$i]['betType']){
						$winloss=$groupResult[$i]['winLossAmount'] - $groupResult[$i]['betAmount'];
						$betAmount+= $groupResult[$i]['betAmount'];
						$winlossTotal+= $winloss;
					}
				}
				
				switch ($key) {
					case 'DT_DRAGON':
						$type = '<font color="red">DRAGON</font>';
						break;
					case 'DT_TIGER':
						$type = '<font color="#000c75">TIGER</font>';
						break;
					case 'DT_TIE':
						$type = '<font color="black">TIE</font>';
						break;
					case 'TIPS':
						$type = '<font color="black">TIPS</font>';
						break;
				}
				if ($winlossTotal<0){
					$winloss_result='<font color="red" >' . money_format('%i',$winlossTotal) . '</font>';
				}
				if ($winlossTotal>0){
					$winloss_result='<font color="#000c75">' . money_format('%i',$winlossTotal) . '</font>';
				}
				$table_data .= '<tr><td class="c_bet_type">' .$type. '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $betAmount) . '</td><td class="c_win_loss">' . $winloss_result . '</td></tr>';
			}

		}else if($game_type == 2){ // roulette
			
			$result=json_decode($live_member_report_details, true);
			foreach ($result as $item) {
				// copy item to grouped
				$grouped[$item['betType']][] = $item;
			}
				
			foreach($grouped as $key => $value){
				$groupResult = $grouped[$key];
				$type='';
				$winloss = 0;
				$betAmount = 0;
				$winlossTotal = 0;
				for ($i = 0; $i <= count($groupResult)-1; $i++){
					if ($key==$groupResult[$i]['betType']){
						$winloss=$groupResult[$i]['winLossAmount'] - $groupResult[$i]['betAmount'];
						$betAmount+= $groupResult[$i]['betAmount'];
						$winlossTotal+= $winloss;
					}
				}
				$betType = str_replace("RL_","",$key);
				switch ($betType) {
					case 'RED':
						$type = '<font color="red">' . $betType . '</font>';
						break;
					case 'BLACK':
						$type = '<font color="black">' . $betType . '</font>';
						break;
					case 'ODD':
						$type = '<font color="#000c75">' . $betType . '</font>';
						break;
					case 'EVEN':
						$type = '<font color="#000c75">' . $betType . '</font>';
						break;
					case 'SMALL':
						$type = '<font color="green">' . $betType . '</font>';
						break;
					case 'BIG':
						$type = '<font color="blue">' . $betType . '</font>';
						break;
					default:
						$type = '<font color="black">' . $betType . '</font>';
				}
				if ($winlossTotal<0){
					$winloss_result='<font color="red" >' . money_format('%i',$winlossTotal) . '</font>';
				}
				if ($winlossTotal>0){
					$winloss_result='<font color="#000c75">' . money_format('%i',$winlossTotal) . '</font>';
				}
				$table_data .= '<tr><td class="c_bet_type">' .$type. '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $betAmount) . '</td><td class="c_win_loss">' . $winloss_result . '</td></tr>';
			}
			
		}
		 
		$table_data .= '</table>';
		return  $table_data;
	}
	public function getRouletteNumberTypeFromBetResultDetail($raw_type){
		// 0^0^112233 = 11&22&33
		$extract_type = split("\^",$raw_type);
		$type = $extract_type[count($extract_type)-1];
		$separator_number = 2;
		 
		$loop_until = (strpos($raw_type,'^') === false) ? count($extract_type)-1 : count($extract_type)-2;
		 
		for($i = 0; $i <= (strlen($type)/2)/2; $i++){
			$type = substr_replace($type,'&',$separator_number,0);
			$separator_number += 3;
		}
		$type = (substr_compare($type,"&",-1,1) == 0) ? substr($type,0,strlen($type) -1) : $type;
		$type = str_replace('&', '<font color="#c0d2fa">&</font>', $type);
		 
		return (strlen($raw_type) <= 2) ? $raw_type : $type;
	}
	
	public function amountIncentive($currency, $amount){
		$row = TableCurrency::model()->find(array('select'=>'incentive,incentive_amount','condition'=>'currency_name=:currency',
				'params'=>array(':currency'=>$currency),));
		 
		return ($row['incentive'] == 1) ? number_format($amount * $row['incentive_amount'],2,".",",") : number_format($amount,2,".",",");
	}
	
	public function getBaccaratBetTypeByBetDetailNumber($bet_detail_number){
		 
		switch ($bet_detail_number)
		{
			case 0:
				return '<font color="red">Banker</font>'; break;
			case 1:
				return '<font color="blue">Player</font>'; break;
			case 2:
				return '<font color="green">Tie</font>'; break;
			case 3:
				return '<font color="red">Banker pair</font>'; break;
			case 4:
				return '<font color="blue">Player pair</font>'; break;
			case 5:
				return '<font color="red">Big</font>'; break;
			case 6:
				return '<font color="blue">Small</font>'; break;
			default:
				return "Undefined";
		}
	}
	
	public function getDragonTigerBetTypeByBetDetailNumber($bet_detail_number){
		if($bet_detail_number == 0){
			return '<font color="red">Dragon</font>';
		}
		elseif($bet_detail_number == 1){
			return '<font color="blue">Tiger</font>';
		}
		elseif($bet_detail_number == 2){
			return '<font color="green">Tie</font>';
		}
	}
	
	public function getExportPlayerWinLossDetailSavan()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
	
		if ($_GET['tableId']<>""){
			$andTableId="and table_info_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_info_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_info_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
	
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,bet_result AS result,win_loss,win_loss as winloss1,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'Savenvegas999' as casino_name,'1' as counter,ip as IP from  tbl_savan_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		else{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,bet_result AS result,win_loss,win_loss as winloss1,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'Savenvegas999' as casino_name,'1' as counter,ip as IP from  tbl_savan_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
}



