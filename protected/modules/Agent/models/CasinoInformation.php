<?php

class CasinoInformation 
{
	public function updateRevenueSharing()
	{
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		$connection = Yii::app()->db_cv999_fd_master;
		
		//Get previous revenue sharing- Added by Allan |date:2012-07-02
		$command = $connection->createCommand("SELECT revenue_sharing from tbl_casino WHERE id=1");
		$rd=$command->queryRow();
		$rev_HTV=$rd['revenue_sharing'];
		$command1 = $connection->createCommand("SELECT revenue_sharing from tbl_casino WHERE id=2");
		$rd1=$command1->queryRow();
		$rev_savan=$rd1['revenue_sharing'];
		$command2 = $connection->createCommand("SELECT revenue_sharing from tbl_casino WHERE id=3");
		$rd2=$command2->queryRow();
		$rev_costa=$rd2['revenue_sharing'];
		
		$recComm=CasinoType::model()->findByPk(1);
		CasinoType::model()->updateAll(array('revenue_sharing'=>$_GET['htv']),'id=1');
		CasinoType::model()->updateAll(array('revenue_sharing'=>$_GET['sav']),'id=2');
		CasinoType::model()->updateAll(array('revenue_sharing'=>$_GET['cos']),'id=3');
		
		if ($rev_HTV!=$_GET['htv']){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Company';
			$postLog->operated_level='Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=17;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE revenue sharing for HTV from:<label style=\"color:red\">'.$rev_HTV.'</label> to <label style=\"color:red\">'.$_GET['htv'].'</label> .</b>';
			$postLog->save();
		}
		if ($rev_savan!=$_GET['sav']){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Company';
			$postLog->operated_level='Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=17;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE revenue sharing for SAVAN from:<label style=\"color:red\">'.$rev_savan.'</label> to <label style=\"color:red\">'.$_GET['sav'].'</label> .</b>';
			$postLog->save();
		}
		if ($rev_costa!=$_GET['costa']){
			$postLog=new TableLog;
			$postLog->operated_by=$AccountID;
			$postLog->operated_by_level=$level;
			$postLog->operated='Company';
			$postLog->operated_level='Company';
			$postLog->operation_time=$dateTime;
			$postLog->log_type_id=17;
			$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE revenue sharing for COSTA from:<label style=\"color:red\">'.$rev_costa.'</label> to <label style=\"color:red\">'.$_GET['cos'].'</label> .</b>';
			$postLog->save();
		}
	}
	
	public function getCasinoInformation()
	{

		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['castype']=="All" && $_GET['curtype']=="All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing as revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where transaction_date BETWEEN '".$_GET['datefrom']."' AND '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		elseif ($_GET['castype']=="All" && $_GET['curtype']=="All" && $_GET['platype']<>"All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where account_type='".$_GET['platype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']=="All" && $_GET['curtype']<>"All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where currency_id='".$_GET['curtype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']<>"All" && $_GET['curtype']=="All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']=="All" && $_GET['curtype']<>"All" && $_GET['platype']<>"All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where currency_id='".$_GET['curtype']."' and account_type='".$_GET['platype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']<>"All" && $_GET['curtype']=="All" && $_GET['platype']<>"All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and account_type='".$_GET['platype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']<>"All" && $_GET['curtype']<>"All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and currency_id='".$_GET['curtype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		elseif ($_GET['castype']<>"All" && $_GET['curtype']<>"All" && $_GET['platype']<>"All") 
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and account_type='".$_GET['platype']."' and currency_id='".$_GET['curtype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		

		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountCasinoInformation()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwGetCasinoBalanceTransmitter" );
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getCasinoInformationTotal()
	{
	

	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['castype']=="All" && $_GET['curtype']=="All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing as revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		elseif ($_GET['castype']=="All" && $_GET['curtype']=="All" && $_GET['platype']<>"All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where account_type='".$_GET['platype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']=="All" && $_GET['curtype']<>"All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where currency_id='".$_GET['curtype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']<>"All" && $_GET['curtype']=="All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']=="All" && $_GET['curtype']<>"All" && $_GET['platype']<>"All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where currency_id='".$_GET['curtype']."' and account_type='".$_GET['platype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']<>"All" && $_GET['curtype']=="All" && $_GET['platype']<>"All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and account_type='".$_GET['platype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		if ($_GET['castype']<>"All" && $_GET['curtype']<>"All" && $_GET['platype']=="All")
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and currency_id='".$_GET['curtype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		elseif ($_GET['castype']<>"All" && $_GET['curtype']<>"All" && $_GET['platype']<>"All") 
		{
			$command = $connection->createCommand("Select casino_name,currency_name,SUM(winloss) as winloss,SUM(commission) as commission,sum(total) as total,sum(agent_winloss) as agent_winloss,revenue_sharing,sum(amount_to_pay) as amount_to_pay,sum(net_total) as net_total,exchange_rate from vwGetCasinoBalanceTransmitter where casino_code='".$_GET['castype']."' and account_type='".$_GET['platype']."' and currency_id='".$_GET['curtype']."' and transaction_date between '".$_GET['datefrom']."' and '".$_GET['dateto']."'  GROUP BY casino_name,currency_name " );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getCountCasinoInformationTotal()
	{

	
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("Select COUNT(0) from vwGetCasinoBalanceTransmitter" );
		$rows = $command->query();
		return $rows;
	
	
	}
}

