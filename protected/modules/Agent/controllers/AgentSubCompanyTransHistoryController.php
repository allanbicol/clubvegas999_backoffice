<?php
class AgentSubCompanyTransHistoryController extends MyController
{
	public function actionAgentSubCompanyTransHistory()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new AgentSubCompanyTransHistory;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountAgentSubCompanyTransHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentSubCompanyTransHistory($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("transaction_number","trans_type_id","currency","amount","market_type","credit_before","credit_after","balance_before","balance_after","transaction_date","operator_id");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readTransactionHistory') ==true ||
		    Yii::app()->session['level'] =='SC' ||
			Yii::app()->session['level'] =='SMA' ||
			Yii::app()->session['level'] =='MA' ||
			Yii::app()->session['level'] =='AGT' 
		   )
		{
			// initialize
			$model = new AgentPublicDataValidation();
				
			// data validation
			if(!isset($_GET['account_id'])){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AgentDataValidationError");
			}
			if(User::getUserType() == 'agent'){
				// check if account_id is agent's sub agent
				if(!$model->isSubAgentAccountIdValid(Yii::app()->session['account_id'], $_GET['account_id'])){
					$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AgentDataValidationError");
				}
			}
			if (Yii::app()->session['level'] =='SC' && strlen($_GET['account_id'])<=2){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
			}
			if (Yii::app()->session['level'] =='SMA' && strlen($_GET['account_id'])<=4){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
			}
			if (Yii::app()->session['level'] =='MA' && strlen($_GET['account_id'])<=6){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
			}
			if (Yii::app()->session['level'] =='AGT' && strlen($_GET['account_id'])<=8){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
			}
			$this->render("index");
			
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
	
}