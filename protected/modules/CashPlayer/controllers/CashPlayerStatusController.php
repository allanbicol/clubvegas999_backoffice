<?php
/**
 * @todo CashPlayerStatusController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerStatusController extends MyController
{
	public function actionCashPlayerStatusProcess()
	{
		$cps = new CashPlayerStatus();
		$cps->changeCashPlayerStatus();
	}
}