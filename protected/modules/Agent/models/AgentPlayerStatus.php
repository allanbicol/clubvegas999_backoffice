<?php
/**
 * @todo AgentPlayerStatus Model
 * @copyright CE
 * @author Allan Bicol
 * @since 2012-05-11
 */
class AgentPlayerStatus
{
	public function changeAgentPlayerStatus()
	{
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		//$connection = Yii::app()->db_cv999_fd_master;
		$idCount=strlen($_GET['AccountID']);
		if ($_GET['status']=='Active'){
			$intStatID='1';
		}
		elseif($_GET['status']=='Closed'){
			$intStatID='3';
		}
		
		if ($idCount==2){
			$label='Sub Company';
		}else if ($idCount==4){
			$label='Senior Master';
		}else if ($idCount==6){
			$label='Master Agent';
		}else if ($idCount==8){
			$label='Agent';
		}
		
		if ($_GET['task']=='changeStatus')
		{

			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT agent_status_id from tbl_agent  WHERE account_id='" . $_GET['AccountID'] . "'");
			$rd=$command->queryRow();
			$statusVal=$rd['agent_status_id'];
			if ($statusVal==1){
				$status='Active';
			}else{$status='Closed';}
			
			$connection1 = Yii::app()->db_cv999_fd_master;
			$command1 = $connection1->createCommand("SELECT status_id from tbl_agent_player  WHERE account_id='" . $_GET['AccountID'] . "'");
			$rd1=$command1->queryRow();
			$statusValP=$rd1['status_id'];
			if ($statusValP==1){
				$statusP='Active';
			}else{$statusP='Closed';}
			
			if (strlen($_GET['AccountID'])==10)
			{
				TableAgentPlayer::model()->updateAll(array('status_id'=>$intStatID),'account_id="'.$_GET['AccountID'].'"');
				
				$postLog=new TableLog();
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level='Agent Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=8;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE status of <label style=\"color:green\">Agent Player</label> <label style=\"color:#7A5C00\">'.$_GET['AccountID'].'</label> from <label style=\"color:red\">'.$statusP.'</label> to <label style=\"color:red\">'.$_GET['status'].'</label></b>';
				$postLog->save();
			}
			else
			{

				TableAgent::model()->updateAll(array('agent_status_id'=>$intStatID),'Substring(account_id,1,'.$idCount.')="'.$_GET['AccountID'].'"');
				TableAgentPlayer::model()->updateAll(array('status_id'=>$intStatID),'Substring(account_id,1,'.$idCount.')="'.$_GET['AccountID'].'"');
				
				$postLog=new TableLog();
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_GET['AccountID'];
				$postLog->operated_level='Agent Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=8;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE status of <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.$_GET['AccountID'].'</label> from <label style=\"color:red\">'.$status.'</label> to <label style=\"color:red\">'.$_GET['status'].'</label></b>';
				$postLog->save();
			}
			
		}
	}
}