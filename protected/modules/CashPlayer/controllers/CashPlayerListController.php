<?php

class CashPlayerListController extends MyController
{
	public function actionLoadFormDeposit()
	{
		// initialize
		$model = new CashPlayerList();
		$cash_player = new CashPlayerDeposit();
		$bankList = $model->getTransactionItemBankList();
		$otherList = $model->getTransactionItemOtherList();
		
		$this->renderPartial("depositDialog",array('bankList'=>$bankList, 'otherList'=>$otherList,
				'currency'=>$cash_player->getCurrencyNameByAccountId($_GET['account_id'])));
	
	}
	public function actionLoadFormWithdraw()
	{
		// initialize
		$model = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type',
				'params'=>array(':item_type'=>4),'order'=>'transaction_item'));
		
		// generate array data
		$list = CHtml::listData($model, 'id', 'transaction_item');
		$this->renderPartial("withdrawDialog", array('list'=>$list));
	}
	
	/**
	 * @todo set bonus dialog
	 * @author leokarl
	 * @since 2012-12-21
	 */
	public function actionSetBonusDialog()
	{
		// initialize
		$cash_player = new CashPlayerBonus();
		
		$model = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type',
				'params'=>array(':item_type'=>6),'order'=>'transaction_item'));
		
		// generate array data
		$list = CHtml::listData($model, 'id', 'transaction_item');
	
		$this->renderPartial('setBonusDialog', array('list'=>$list,'currency'=>$cash_player->getCurrencyNameByAccountId($_GET['account_id'])));
	}
	
	/**
	 * @todo unlock player
	 * @author allan
	 * @since 2013-10-11
	 */
	public function actionUnlockPlayer()
	{
		// initialize
		$val=1;
		$unlock=TableCashPlayer::model()->updateAll(array(
		'kick_off'=>$val),
		'account_id="'.$_POST['accountID'].'"');
		if ($unlock){
			echo 'Player was succesfully unlock.';
		}else{
			echo 'This Player is already unlock.';
		}
	}
	
	public function actionPlayerList()
	{
		$cp = new CashPlayer;
		
		if (isset($_POST['page'])==''){
			$page=0;
		}else{
			$page = $_POST['page'];
		}
		$limit = $_POST['rows'];
		$orderField = $_POST['sidx'];
		$sortType = $_POST['sord'];
		
		$result=$cp->getCountPlayerList();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cp->getPlayerList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("account_id","account_name","account_type","currency_name","status_name","opening_acc_date","last_date_play","bonus","balance","balance");
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue ='';
		$count_permissions = 0;
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerTransHistory'))
		{
			$count_permissions += 1;
			$htmValue='<a class=\"btn mini red\"  href=\"' . $urlAS . '/index.php?r=CashPlayer/CashPlayerTransHistoryFund&Account_ID=\">Transaction History <i class=\"icon-list\"></i></a>';
		}
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerListLog'))
		{
			$count_permissions += 1;
			$htmValue .= '<a class=\"btn mini red\"  href=\"index.php?r=CashPlayer/CashPlayerLog/Cashplayerlog&Account_ID=\" class=\'list2\'>Log <i class=\"icon-list\"></i></a>';
		}
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerSetting'))
		{
			$count_permissions += 1;
			$htmValue .= '<a class=\"btn mini red\"  class=\"accountsettings\" href=\"' . $urlAS . '/index.php?r=CashPlayer/CashPlayerSetting&Account_ID=\">Setting <i class=\"icon-cogs\"></i></a>';			
		}
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerSetBonus'))
		{
			$count_permissions += 1;
		 	$htmValue .= 
		 	'<a class=\"btn mini red\"  href=\"index.php?r=CashPlayer/CashPlayerList/Setbonus&Account_ID=\" id=\'list2\' type=\"button\" value=\"Center\" onclick=\"javascript: getAccountIDforBunos(this);checkPlayerOnlineStatBonus(); return false \" > Set Bonus <i class=\"icon-star\"></i></a>';
		}
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerDeposit'))
		{
			$count_permissions += 1;
			$htmValue .= 
			//'<a class=\"deposit\"  href=\"#\" id=\"Account_ID=\" name=\"cur_name\" title=\"cRowNo\" onclick=\"javascript: deposit(this);getAccoundID(this);document.getElementById(\'txtAmount\').value=\'\'; getRowNo(this); document.getElementById(\'radDepositor1\').click(); processDeposit();\">Deposit</a>';
			'<a class=\"btn mini red\"  href=\"#\" id=\"Account_ID=\" name=\"cur_name\" title=\"cRowNo\" onclick=\"javascript: processDeposit(this); getRowNo(this);\">Deposit <i class=\"icon-download-alt\"></i></a>';
		}
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerWithdraw'))
		{
			$count_permissions += 1;
			$htmValue .= 
			'<a class=\"btn mini red\"  href=\"index.php?r=CashPlayer/CashPlayerList/Withdraw&Account_ID=\" id=\'list2\' type=\"button\" value=\"Center\" onclick=\"javascript:checkPlayerOnlineStatWithdraw(this); return false \">Withdraw <i class=\"icon-upload-alt\"></i></a>';
		}
// HIDE SUBWALLET LINK
/* 		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerWithdraw'))
 		{
 			$count_permissions += 1;
 			$htmValue .=
 			'<a class=\"btn mini red\"  href=\"index.php?r=CashPlayer/CashPlayerList/SubWallet&Account_ID=\" id=\'list2\' type=\"button\" value=\"Center\">Sub Wallet <i class=\"icon-share\"></i></a>';
 		}
 
 */
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerWithdraw'))
		{
			$count_permissions += 1;
			$htmValue .= 
					'<a class=\"btn mini red\"  href=\"index.php?r=CashPlayer/CashPlayerList/UnlocPlayerk&Account_ID=\" id=\'list2\' type=\"button\" value=\"Center\" onclick=\"javascript: return unlockPlayer(this); \">Unlock Player <i class=\"icon-unlock\"></i></a>';
		}
		

		$htmValue = ($count_permissions > 0) ? $htmValue : '-';
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmValue,9);
	}
	public function actionAccountSettings()
	{
		$this->render("accountSettings");
	}
	
	public function actionBankLookup()
	{
		$cp = new CashPlayer;
		
		$page = $_POST['page'];
		$limit = $_POST['rows'];
		$orderField = $_POST['sidx'];
		$sortType = $_POST['sord'];
		
		$result=$cp->getCountBankList();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cp->getBankList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("id","bank_name","bank_account","bank_address","contact_phone","zip_code","city","country");
		$urlAS= Yii::app()->request->baseUrl;
		$htmValue='';
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmValue,3);
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerList'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	} 
	
	public function actionSubWallet(){
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerList'))
			$this->render("subwallet");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	public function actionExcel(){
	    $file="CashPlayerInformation".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";

		header("Content-type: application/vnd.ms-excel;charset=UTF-8");
		header("Content-Disposition: attachment; filename=$file");
		HEADER("Pragma: no-cache");
		HEADER("Expires: 0");
		
		$buffer = $_POST['csvBuffer'];
		
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Information</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> = '.$params[1].'\n<b>Account Name</b> = '.$params[2].'\n<b>Type</b> = '.$params[3].'\n<b>Status</b> = '.$params[4].'\n<b>Test Currency</b> = '.$params[5];
			$postLog->save();
   	 			
   	 			
			}catch(Exception $e){

			}
	}
	public function actionExportAllToExcel(){
// 		$file="CashPlayerInformation".date('Y-m-d').".xls";
// 		$test="TheTableMarkupWillGoHere";
	
// 		header("Content-type: application/vnd.ms-excel;charset=UTF-8");
// 		header("Content-Disposition: attachment; filename=$file");
	
// 		$connection = Yii::app()->db_cv999_fd_master;
// 		$whereClause='';
// 		if ($_GET['test']==0){
// 			if ($_GET['id']=='All' && $_GET['name']=='All'){
// 				$whereClause='WHERE account_id <> "" ';
// 			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
// 				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' ";
// 			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
// 				$whereClause="WHERE account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ";
// 			}else{
// 				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' ";
// 			}
// 		}else{
// 			if ($_GET['id']=='All' && $_GET['name']=='All'){
// 				$whereClause="WHERE currency_name <>'TEST'";
// 			}else if ($_GET['id']=='All' && $_GET['name']!='All'){
// 				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and currency_name <>'TEST' ";
// 			}else if ($_GET['id']!='All' && $_GET['name']=='All'){
// 				$whereClause="WHERE account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' and currency_name <>'TEST'";
// 			}else{
// 				$whereClause="WHERE account_name like '%".htmlEntities($_GET['name'],ENT_QUOTES)."%' and account_id like '%".htmlEntities($_GET['id'],ENT_QUOTES)."%' and currency_name <>'TEST' ";
// 			}
// 		}
		
// 		$type='';
// 		if ($_GET['type']=="All"){
// 			$type='';
// 		}else{
// 			if($_GET['type']=="Cash"){
// 				$type="AND account_type='".$_GET['type']."' ";
// 			}else{
// 				$type="AND account_type='".$_GET['type']."'";
// 			}
// 		}
// 		$status='';
// 		if ($_GET['status']=="All"){
// 			$status='';
// 		}else{
// 			if($_GET['status']=="Active"){
// 				$status="AND status_name='".$_GET['status']."' ";
// 			}else{
// 				$status="AND status_name='".$_GET['status']."'";
// 			}
// 		}
		
// 		$command = $connection->createCommand("Select * from vwGetAllPlayerInfo $whereClause $type $status ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
// 		$rows = $command->query();
// 		return $rows;
		
// 		$buffer = $rows;
	
// 		//try{
				
				
// 			echo $buffer;
// 			$postLog = new TableLog;
// 			$postLog->operated_by = Yii::app()->session['account_id'];
// 			$postLog->operated_by_level = Yii::app()->session['level_name'];
// 			$postLog->operated ='Cash Player';
// 			$postLog->operated_level = 'Clubvegas999';
// 			$postLog->operation_time = date('Y-m-d H:i:s');
// 			$postLog->log_type_id = 23;
// 			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Information</label> to excel file</b>';
// 			$postLog->save();
	
	
// 		//}catch(Exception $e){
// 		//	echo $e;
// 		//}
		$rawData=Yii::app()->db->createCommand('SELECT * FROM tbl_bank')->query();
		// or using: $rawData=User::model()->findAll();
		$dataProvider=new CArrayDataProvider($rawData, array(
				'id'=>'user',
				'sort'=>array(
						'attributes'=>array(
								'id', 'username', 'email',
						),
				),
				'pagination'=>array(
						'pageSize'=>10,
				),
		));
		$test=array(array('asdasda','asdasdasda','asdasdas'));
		// $dataProvider->getData() will return a list of arrays.
 //		print_r($rawData);
		
// 		$connection = Yii::app()->db_cv999_fd_master;
// 		$command = $connection->createCommand("Select * from vwGetAllPlayerInfo");
// 		$rows = $command->query();
// 		$doc=array($rows);
// 		print_r($doc);
		
		
		
// 		$model = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type',
// 				'params'=>array(':item_type'=>6),'order'=>'transaction_item'));
	
// 		$count_cats = count($model);
// 		//exit($model);
// 		if($count_cats > 0){
// 			$arr_category = array();
// 			foreach($model as $cat)
// 				array_push($arr_category,$cat->attributes);
// 		}
// 		print_r($arr_category);
	//exit ($model);
// 		print_r($test);
		$data=$rawData->readAll();
		//print_r($data);
		//exit();
		$xls = new ExportToExcelAll();
		$xls->addArrayTitle ( $test);
		$xls->addArray ( $data);
		$xls->generateXML ("mytest");
		
		
	}

				
}

