<?php

class CashPlayerConsolidatedWinLossDetail
{

	public function getPlayerWinLossDetail($orderField, $sortType, $startIndex, $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			//$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + commission)-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT * FROM ((SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						FORMAT(amount_settlements - amount_wagers,2) AS win_los,FORMAT(commission,2) AS commission,
						FORMAT(casino_balance,2) AS balance,'cv999_3' AS casino
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,(SELECT TG.game_name FROM tbl_game AS TG WHERE TG.id=CH.game_type) AS game_type,FORMAT(bet_amount,2),
						FORMAT(available_bet,2) AS valid_bet,FORMAT(win_loss,2),FORMAT(commission,2),FORMAT(balance_after,2) AS balance,'cv999_1' AS casino 
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') 
						UNION
						(SELECT end_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,FORMAT(stake_amount,2),
						FORMAT(valid_stake,2) AS valid_bet,FORMAT(win_loss,2),FORMAT(commission,2),FORMAT(balance_after,2) AS balance,'cv999_2' AS casino 
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' AND account_id='".$_GET['accountId']."' and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,FORMAT(bet_amount,2),'' AS valid_bet,FORMAT(win_loss,2),FORMAT(commission,2),FORMAT(balance_after,2) AS balance,'cv999_1' AS casino
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT create_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_key,FORMAT(total_wager_amount,2),'' AS valid_bet,FORMAT(win_loss,2),FORMAT(0,2),FORMAT(current_balance,2) AS balance,'cv999_6' AS casino
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						FORMAT(win_loss,2) AS win_los,FORMAT(commission,2) AS commission,
						FORMAT(casino_balance,2) AS balance,'cv999_4' AS casino
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT transaction_date,account_id,currency_name,transaction_type,FORMAT(casino_balance,2),'','','','','','',casino_code 
						FROM tbl_casino_balance_transmitter TCBT INNER JOIN tbl_currency TC ON TC.id=currency_id WHERE account_type='c' and account_id='".$_GET['accountId']."' and  transaction_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')) AS consolidatedWinLossDetail order by $orderField $sortType LIMIT $startIndex , $limit
					");
		}
		else{
			//$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + commission)-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter  from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT * FROM ((SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						amount_settlements - amount_wagers AS win_los,commission,
						casino_balance AS balance,'cv999_3' AS casino
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,(SELECT TG.game_name FROM tbl_game AS TG WHERE TG.id=CH.game_type) AS game_type,bet_amount,
						available_bet AS valid_bet,win_loss,commission,balance_after AS balance,'cv999_1' AS casino 
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') 
						UNION
						(SELECT end_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,stake_amount,
						valid_stake AS valid_bet,win_loss,commission,balance_after AS balance,'cv999_2' AS casino 
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,bet_amount,'' AS valid_bet,win_loss,commission,balance_after AS balance,'cv999_1' AS casino
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT create_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,total_wager_amount,'' AS valid_bet,win_loss,0 as commission,current_balance AS balance,'cv999_6' AS casino
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_key='".$_GET['methodId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						FORMAT(win_loss,2) AS win_los,FORMAT(commission,2) AS commission,
						FORMAT(casino_balance,2) AS balance,'cv999_4' AS casino
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT transaction_date,account_id,currency_name,transaction_type,FORMAT(casino_balance,2),'','','','','','',casino_code 
						FROM tbl_casino_balance_transmitter TCBT INNER JOIN tbl_currency TC ON TC.id=currency_id WHERE account_type='c' and account_id='".$_GET['accountId']."' and  transaction_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')) AS consolidatedWinLossDetail order by $orderField $sortType LIMIT $startIndex , $limit
					");
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountPlayerWinLossDetail()
	{
		$connection = Yii::app()->db_cv999_fd_master;
	if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("SELECT COUNT(0) FROM ((SELECT bet_date
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time FROM tbl_htv_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') 
						UNION
						(SELECT end_time FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' AND account_id='".$_GET['accountId']."' and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT create_time FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_date FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c'  and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT transaction_date FROM tbl_casino_balance_transmitter TCBT INNER JOIN tbl_currency TC ON TC.id=currency_id WHERE account_type='c' and account_id='".$_GET['accountId']."' and  transaction_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')) AS consolidatedWinLossDetail "  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(0) from tbl_costavegas_casino_history where account_type='c' and account_id='".$_GET['accountId']."' and  game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getPlayerWinLossSummary()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT casino,account_id,currency_name,SUM(bet_count) AS bet_count,ROUND(SUM(total_stake),2) AS total_stake,ROUND(SUM(amount_wager),2) AS amount_wager,ROUND(SUM(avg_bet),2) AS avg_bet,ROUND(SUM(win_los),2) AS win_los,ROUND(SUM(commission),2) AS commission,SUM(bonus) AS bonus,SUM(amount_tips) AS amount_tips,ROUND(SUM(total_win_los),2) AS total_win_los, ROUND(SUM(p_l),2) AS p_l FROM ((SELECT 
				'COSTAVEGAS999' AS casino,account_id,currency_name,'0' AS bet_count,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
				SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
				0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
				IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
				(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l
				FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
				UNION
				(SELECT 'HTV999' AS casino,account_id,currency,COUNT(bet_amount) AS bet_count,SUM(bet_amount) AS total_stake,
				SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
				(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
				((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l 
				FROM tbl_htv_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') 
				UNION
				(SELECT 'SAVAN999' AS casino,account_id,currency,COUNT(stake_amount) AS bet_count,SUM(stake_amount) AS total_stake,
				SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
				SUM(total_win_loss) AS total_win_los,
				(SUM(total_win_loss))*-1 AS p_l 
				FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
				UNION
				(SELECT 'HTV999' AS casino,account_id,currency,COUNT(bet_amount) AS bet_count,0 AS total_stake,sum(bet_amount),AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l
				FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
				UNION
				(SELECT 'VIRTUAVEGAS999' AS casino,account_id,currency_name,'0' AS bet_count,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
				SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
				0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
				IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
				(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l
				FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' AND account_id='".$_GET['accountId']."' AND  bet_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."')
				UNION
				(SELECT 'SLOTSVEGAS999' AS casino,account_id,currency,COUNT(total_wager_amount) AS bet_count,sum(total_result_amount) AS total_stake,sum(total_wager_amount),AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l
				FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
				) AS consolidated_winloss GROUP BY casino
			");
		
			$rows = $command->query();
			return $rows;
		}
	
	public function getExportPlayerWinLossDetail()
	{

		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			//$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + commission)-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT * FROM ((SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						FORMAT(amount_settlements - amount_wagers,2) AS win_los,FORMAT(commission,2) AS commission,
						FORMAT(casino_balance,2) AS balance,'cv999_3' AS casino
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,(SELECT TG.game_name FROM tbl_game AS TG WHERE TG.id=CH.game_type) AS game_type,FORMAT(bet_amount,2),
						FORMAT(available_bet,2) AS valid_bet,FORMAT(win_loss,2),FORMAT(commission,2),FORMAT(balance_after,2) AS balance,'cv999_1' AS casino 
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') 
						UNION
						(SELECT end_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,FORMAT(stake_amount,2),
						FORMAT(valid_stake,2) AS valid_bet,FORMAT(win_loss,2),FORMAT(commission,2),FORMAT(balance_after,2) AS balance,'cv999_2' AS casino 
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' AND account_id='".$_GET['accountId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,FORMAT(bet_amount,2),'' AS valid_bet,FORMAT(win_loss,2),FORMAT(commission,2),FORMAT(balance_after,2) AS balance,'cv999_1' AS casino
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 and account_id='".$_GET['accountId']."' and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT create_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_key,FORMAT(total_wager_amount,2),'' AS valid_bet,FORMAT(win_loss,2),FORMAT(0,2),FORMAT(current_balance,2) AS balance,'cv999_6' AS casino
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						FORMAT(win_loss,2) AS win_los,FORMAT(commission,2) AS commission,
						FORMAT(casino_balance,2) AS balance,'cv999_4' AS casino
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT transaction_date,account_id,currency_name,transaction_type,FORMAT(casino_balance,2),'','','','','','',casino_code 
						FROM tbl_casino_balance_transmitter TCBT INNER JOIN tbl_currency TC ON TC.id=currency_id WHERE account_type='c' and account_id='".$_GET['accountId']."' and  transaction_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')) AS consolidatedWinLossDetail 
					");
		}
		else{
			//$command = $connection->createCommand("Select id,bet_date,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency_name,game_type,amount_wagers,(amount_settlements-amount_wagers) as win_loss,commission,amount_tips,((amount_settlements-amount_wagers) + commission)-amount_tips as total_win_loss,casino_balance,(Select TC.casino_name from tbl_casino as TC where TC.casino_code=CH.casino_code) as casino_name,'1' as counter  from  tbl_costavegas_casino_history AS CH where account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
			$command = $connection->createCommand("SELECT * FROM ((SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						amount_settlements - amount_wagers AS win_los,commission,
						casino_balance AS balance,'cv999_3' AS casino
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,(SELECT TG.game_name FROM tbl_game AS TG WHERE TG.id=CH.game_type) AS game_type,bet_amount,
						available_bet AS valid_bet,win_loss,commission,balance_after AS balance,'cv999_1' AS casino 
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') 
						UNION
						(SELECT end_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,stake_amount,
						valid_stake AS valid_bet,win_loss,commission,balance_after AS balance,'cv999_2' AS casino 
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_type,bet_amount,'' AS valid_bet,win_loss,commission,balance_after AS balance,'cv999_1' AS casino
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT create_time,account_id,currency,'' AS trans_type,'' AS trans_amount,game_key,total_wager_amount,'' AS valid_bet,win_loss,0 as commission,current_balance AS balance,'cv999_6' AS casino
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_key='".$_GET['methodId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT bet_date,account_id,currency_name,'' AS trans_type,'' AS trans_amount,game_type,'' AS bet_amount,'' AS valid_bet,
						FORMAT(win_loss,2) AS win_los,FORMAT(commission,2) AS commission,
						FORMAT(casino_balance,2) AS balance,'cv999_4' AS casino
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and account_id='".$_GET['accountId']."' and game_type='".$_GET['methodId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')
						UNION
						(SELECT transaction_date,account_id,currency_name,transaction_type,FORMAT(casino_balance,2),'','','','','','',casino_code 
						FROM tbl_casino_balance_transmitter TCBT INNER JOIN tbl_currency TC ON TC.id=currency_id WHERE account_type='c' and account_id='".$_GET['accountId']."' and  transaction_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."')) AS consolidatedWinLossDetail 
					");
		}
		$rows = $command->query();
		return $rows;

	
	}
	
}



