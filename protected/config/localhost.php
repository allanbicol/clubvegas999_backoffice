<?php

return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
        array(
        	'modules'=>array(
        					
        		'gii'=>array(
        			'class'=>'system.gii.GiiModule',
        			'password'=>'123456',
        			// If removed, Gii defaults to localhost only. Edit carefully to taste.
        			'ipFilters'=>array('127.0.0.1','::1'),
        		),
        				
        	),
            'components' => array(
//                 'db'=>array(
// 					'connectionString' => 'mysql:host=192.168.4.43;dbname=cv999_fd_master',
//                 	'username' => 'cv999_be',
// 		            'password' => 'jw42l8k23R',
// 					'charset' => 'utf8',
// 					),
// 		        'db_cv999_fd_master'=>array(
// 					'connectionString' => 'mysql:host=192.168.4.43;dbname=cv999_fd_master',
//                     'username' => 'cv999_be',
// 		            'password' => 'jw42l8k23R',
// 		            'class' => 'CDbConnection',
// 		        	'charset' => 'utf8',
// 				),
            		'db'=>array(
            				'connectionString' => 'mysql:host=127.0.0.1:3307;dbname=cv999_fd_master',
            				'username' => 'root',
            				'password' => '',
            				'charset' => 'utf8',
            		),
            		'db_cv999_fd_master'=>array(
            				'connectionString' => 'mysql:host=127.0.0.1:3307;dbname=cv999_fd_master',
            				'username' => 'root',
            				'password' => '',
            				'class' => 'CDbConnection',
            				'charset' => 'utf8',
            		),
            	'log'=>array(
            		'class'=>'CLogRouter',
            		'routes'=>array(
            		// Log to application.log under runtime directory.
            						array(
            							'class'=>'CFileLogRoute',
            							'levels'=>'error, warning',
            						),
 									/*           			
            						array(
            							'class'=>'CEmailLogRoute',
            							'levels'=>'error, warning',
            							'emails'=>'kim.mou@creativeentertainmentltd.com',
            						),
            						*/
            				),
            		),
            ),
     )
);