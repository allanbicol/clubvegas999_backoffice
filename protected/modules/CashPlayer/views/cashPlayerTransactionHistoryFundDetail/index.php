<!--@TODO: VIEWS FOR CASH PLAYER TRANSACTION HISTORY FOR FUND DETAIL
    @AUTHOR: Allan
    @SINCE: 05082012
-->
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

<script type="text/javascript">
function showBonuspage() 
{
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryBonus1";
}

</script>

<script type="text/javascript">

	var cRowNo=0;
	function tableTransHistoryToday(method_id,dateFrom,dateTo,accountID) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFundDetail/CashPlayerTransHistoryFundDetail&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Transaction Number', 'Type','Currency','Amount','Submit Time','Finish Time','Cage Cashier Account','Status'],
			    colModel: [
					{name: 'transNumber', index: 'transNumber', width: 120, search:true,title:false},
					{name: 'transTypeName', index: 'transTypeName', width: 100,title:false},
					{name: 'currencyName', index: 'currencyName', width: 70,title:false},
					{name: 'Amount', index: 'Amount', width: 120, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'submitTime', index: 'submitTime', width: 130, align:"right",title:false},
					{name: 'finishTime', index: 'finishTime', width: 130, align:"right",title:false},
					{name: 'cashierID', index: 'cashierID', width: 130, align:"right",title:false},
					{name: 'status', index: 'status', width: 100, align:"right",title:false},
			    ],
			    loadComplete: function(){
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
				},
			   	loadtext:"",
			   	cellEdit : true,
			    rowNum: 20,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'transNumber',
			    sortorder: 'ASC',
			    caption: ' <label style="color:red"><?php echo $_GET['account_id'];?></label> FUND HISTORY - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
		});
	} 


</script>
<script type="text/javascript">
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a ^ 999;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
         
function decryptparam()
{
	var strF = document.getElementById('f').value;
	var strT= document.getElementById('t').value;
	var encodeF = enc(strF);
	var encodeT = enc(strT);
	
	var dateF= encodeF.split(" ");	
	var dateFrom=dateF[0];
	var dateT= encodeT.split(" ");	
	var dateTo=dateT[0];
	var dateFSplit=dateFrom.split("-");
	var dateTSplit=dateTo.split("-");
	
	document.getElementById("datefrom").value=dateFSplit[2]+"-"+dateFSplit[1]+"-"+dateFSplit[0];
	document.getElementById("dateto").value=dateTSplit[2]+"-"+dateTSplit[1]+"-"+dateTSplit[0];

}
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	});

	function loadTransTable(selected_method,dateFrom,dateTo,accountID){
		if (selected_method=='ALL'){
			//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID);
 	
		}
	    else
	    {
	    	//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID);
	    
		}
	}

	function btnclick(btnname)
	{
		if (btnname=="Load")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable("ALL",datefrom,dateto,accountID);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
	
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");
			document.getElementById('datefrom').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(methodtype,datefrom,dateto,accountID);
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			
			var datefrom=(theyear+"-"+themonth+"-"+theyday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theyday+ " 23:59:59");
			document.getElementById('datefrom').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(methodtype,datefrom,dateto,accountID);
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[0];
			var yearF = dateFrom[2];
			var dateF=(yearF + "-" + monthF + "-" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[0];
			var yearT = dateTo[2];
			var dateT= (yearT + "-" + monthT + "-" +dayT);
			var dateSubmitFrom= (dateF+" "+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ " "+ document.getElementById('cbHourto').value +":59:59");
			var methodtype=document.getElementById('MethodType').value;
			var accountID=document.getElementById('txtAccountID').value;
			
			loadTransTable(methodtype,dateSubmitFrom,dateSubmitTo,accountID);
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="javascript:decryptparam(); btnclick('Submit');">
<div style="position: relative;left: 5px; top:3px">
	<form action="">
		<table style="background-color:transparent; border-style:ridge;  width: 500px;">
		<tr><td style=" background-color: black; color:white; height: 25px ">Cash Member Transaction History</td></tr>
		<tr><td style=" height: 35px; border-bottom: groove; ">
		<input class="btn red disabled" type="button" value="FUND">
		<input class="btn red" type="button" value="BONUS" onclick="showBonuspage()">
		<input type="text" id="txtAccountID" value="<?php echo $_GET['account_id'];?>" style="display: none;" >
		</td></tr>
		<tr><td>METHOD:
			<select id="MethodType">
				<option value="ALL">All</option>
  				<option value="1">Automatic Deposit</option>
  				<option value="2">Manual Deposit</option>
  				<option value="3">Automatic Withdraw</option>
  				<option value="4">Manual Withdraw</option>
			</select>

		</td></tr>
		<tr><td>FROM&nbsp;:<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px; background-color: transparent" disabled></td></tr><tr><td>TO &nbsp;&nbsp;&nbsp;&nbsp;:<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; background-color: transparent" disabled></td></tr>
			<tr><td><div align="center">
		<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
		<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red"></div>
		</td></tr>
		</table>
		</form>	
		
		<br/>
		
	<div id="qry_result">
	</div>
	<div id="pager1"></div>
	<br/>
	<!--
	<div id="qry_resultTotal"></div>
	<div id="qry_resultTotal1"></div>
	<br/>-->
	<div><input id="f" value="<?php echo $_GET['f'];?>" hidden=true><input id="t" value="<?php echo $_GET['t'];?>" hidden=true></div>
	
</div>

</body>
</html>