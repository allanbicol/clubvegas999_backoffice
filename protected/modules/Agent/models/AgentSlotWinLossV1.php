<?php
/**
 * @todo AgentSlotWinLoss Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-06-01
 */
class AgentSlotWinLossV1
{
	/**
     * @todo getHTVWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
	public function getHTVWinLoss($orderField, $sortType, $startIndex, $limit)
	{
		$orderField=split(",",$orderField);
		$orderField=$orderField[0];
	
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere=" ";
			}
			$strQueryWhereForBalance=" ";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance=" ";
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
					)*-1 as total,
					
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
					
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total
					
					FROM tbl_htv_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) 
					)*-1 as total,
					
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
					((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as mc_total,#sc_total,
					    
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
					)*-1 as pl_total
					    
					FROM tbl_htv_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
				$command = $connection->createCommand("
				#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) 
					)*-1 as total,
						
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
					((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as mc_total,#sma_total,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
					)*-1 as pl_total
					    
					FROM tbl_htv_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
				");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))  
					)*-1 as total,
					
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
					((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
					)*-1 as pl_total
					    
					FROM tbl_htv_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,8)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as bonus,
					((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as total,
					(SELECT balance FROM vwGetHTVMemberSlotWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
					
					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
					((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
					(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
										
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
					)*-1 as pl_total
					    
					FROM tbl_htv_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getHTVWinLossCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getHTVWinLossCount()
	{
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere=" ";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance=" ";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (SELECT
					SUBSTRING(a.account_id,1,2) as account_id
					FROM tbl_htv_bet_slot_record_casino_history a
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id
					
					FROM tbl_htv_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id
					FROM tbl_htv_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
			  	)t
			");
    	}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
    				SELECT  COUNT(0)
    				FROM  (
					    SELECT
						SUBSTRING(a.account_id,1,8) as account_id
						    
						FROM tbl_htv_bet_slot_record_casino_history a
						LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
						WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
						GROUP BY SUBSTRING(a.account_id,1,8)
					)t
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
	    		SELECT  COUNT(0)
	    		FROM  (
					SELECT
					a.account_id as account_id
					FROM tbl_htv_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
				)t
			");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999Total
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999Total()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    
    			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as win_loss,
			    (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
    			(sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)*-1) as bonus,
			    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)) as total,
					
				(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)) as pl_total
    
    			FROM tbl_htv_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    			");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999CSTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999CSTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    			
    			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
    			((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
    			((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as bonus,
    			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as total, #sc_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
				(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
				)*-1 as pl_total
    
    			FROM tbl_htv_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999SMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999SMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,4) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    			
    			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
    			((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
    			((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as bonus,
    			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as total, #sma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
				)*-1 as pl_total
    				
    			FROM tbl_htv_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999MATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999MATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,6) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    
    			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
    			((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
    			((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as bonus,
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as total, #ma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
				)*-1 as pl_total
    
    			FROM tbl_htv_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTV999AGTTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTV999AGTTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,8) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    			
    			(sum(a.win_loss * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
    			((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
    			(sum(a.jackpot_money * (a.agt_share/100))*-1) as bonus,
    			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as total, #agt_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
				)*-1 as pl_total
    
    			FROM tbl_htv_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVWinLossDetails($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
   	 	//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere=" ";
			}
			$strQueryWhereForBalance=" ";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance=" ";
		}
    	//Agent ID
    	$strAccountIDWhere='';
    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    
    	if(trim($_GET['accountID'])==''){
    		$strAccountIDWhere='';
    	}else{
    		if($intAccountIDLength!=10){
    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    		}else{
    			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    		}
    	}
    	$pl_total='';
    	if(trim($_GET['accountID'])==''){
    		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
    		)*-1 as pl_total";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    			SELECT  @rownum := @rownum + 1 row, t.*
    			FROM  (
	    			SELECT
	    			substring(a.account_id,1,$intAccountIDLength) as account_id,
	    			j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
	    			sum(a.win_loss) as mem_win_loss,
					sum(a.commission) as mem_comm,
					sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as mem_bonus,
					((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as mem_total,
					(SELECT balance FROM vwGetHTVMemberSlotWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
	    
	    			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
				    
				    ((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
				    ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
    				((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as ma_bonus,
				    (((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as ma_total,
				    
				    ((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
				    ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
    				((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as sma_bonus,
				    (((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as sma_total,
				    
				    ((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
				    ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
    				((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as sc_bonus,
				    (((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as sc_total,
				    
				    ((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
				    (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
				    ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)  as mc_bonus,
				    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
				        
				    ".$pl_total."
				        
	    				
	    			FROM tbl_htv_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
	    			GROUP BY substring(a.account_id,1,$intAccountIDLength)
	    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetailsCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetailsCount()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetHTVMemberSlotWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetHTVMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " order by t.bet_date desc,t.id desc  LIMIT $startIndex , $limit");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getHTVMemberWinLossDetailsSummaryFooter
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getHTVMemberWinLossDetailsSummaryFooter()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, 
    			count(t.win_loss) as bet_count,
    			sum(t.bet_amount) as bet_amount,
				sum(t.win_loss) as win_loss,
				sum(t.commission) as commission,
    			sum(t.bonus) as bonus,
				sum(t.total) as total,
				(Select t.balance from vwGetHTVMemberSlotWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
    			FROM vwGetHTVMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
    	$rows = $command->query();
    foreach($rows as $row){
			echo $row['bet_count'] . ';';
			echo $row['bet_amount'] . ';';
			echo $row['win_loss'] . ';';
			echo $row['commission'] . ';';
			echo $row['bonus'] . ';';
			echo $row['total'] . ';';
			echo $row['balance'];
		}
    }
    

    
    
    
    /**
     * @todo getSAVANWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLoss($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
		$orderField=$orderField[0];
	
		//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere=" ";
			}
			$strQueryWhereForBalance=" ";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance=" ";
		}
	
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
					)*-1 as total,
					
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
					
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total
					
					FROM tbl_savan_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) 
					)*-1 as total,
					
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
					((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as mc_total,#sc_total,
					    
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
					)*-1 as pl_total
					    
					FROM tbl_savan_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
				$command = $connection->createCommand("
				#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) 
					)*-1 as total,
						
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
					((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as mc_total,#sma_total,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
					)*-1 as pl_total
					    
					FROM tbl_savan_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
				");
		}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))  
					)*-1 as total,
					
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
					((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as mc_total,#ma_total,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
					)*-1 as pl_total
					    
					FROM tbl_savan_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,8)
				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
				#MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
					
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as bonus,
					((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as total,
					(SELECT balance FROM vwGetHTVMemberSlotWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
					
					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
					((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
					(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
										
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
					)*-1 as pl_total
					    
					FROM tbl_savan_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			");
		}
		$rows = $command->query();
		return $rows;
    }
    
    /**
     * @todo getSAVANWinLossCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLossCount()
    {
    	//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere=" ";
			}
			$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance=" ";
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		if(trim($_GET['accountID'])==''){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (SELECT
					SUBSTRING(a.account_id,1,2) as account_id
					FROM tbl_savan_bet_slot_record_casino_history a
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,2)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==2){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id
					
					FROM tbl_savan_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,4)
				)t
			");
		}elseif(strlen(trim($_GET['accountID']))==4){
			$command = $connection->createCommand("
				SELECT  COUNT(0)
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id
					FROM tbl_savan_bet_slot_record_casino_history a
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY SUBSTRING(a.account_id,1,6)
			  	)t
			");
    	}elseif(strlen(trim($_GET['accountID']))==6){
			$command = $connection->createCommand("
    				SELECT  COUNT(0)
    				FROM  (
					    SELECT
						SUBSTRING(a.account_id,1,8) as account_id
						    
						FROM tbl_savan_bet_slot_record_casino_history a
						LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
						WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
						GROUP BY SUBSTRING(a.account_id,1,8)
					)t
			");
		}elseif(strlen(trim($_GET['accountID']))==8){
			$command = $connection->createCommand("
	    		SELECT  COUNT(0)
	    		FROM  (
					SELECT
					a.account_id as account_id
					FROM tbl_savan_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
					GROUP BY a.account_id
				)t
			");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    
    			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as win_loss,
			    (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
    			((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1) as bonus,
			    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as total,
					
				(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total
    
    			FROM tbl_savan_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    			");
    	$rows = $command->query();
    	return $rows;
	}
	
	/**
	 * @todo getSAVANCSTotal
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
    public function getSAVANCSTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,2) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    			
    			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
    			((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
    			((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as bonus,
    			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as total, #sc_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
				(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
				)*-1 as pl_total
    
    			FROM tbl_savan_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANSMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANSMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,4) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    			
    			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
    			((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
    			((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as bonus,
    			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as total, #sma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
				)*-1 as pl_total
    				
    			FROM tbl_savan_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,6) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    
    			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
    			((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
    			((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as bonus,
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as total, #ma_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
				)*-1 as pl_total
    
    			FROM tbl_savan_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANAGTTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANAGTTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
    			SELECT
    			a.bet_time as bet_date,
    			SUBSTRING(a.account_id,1,8) as id,
    			a.currency as currency_name,
    			cu.exchange_rate,
    			count(a.account_id) as bet_count,
    			sum(a.bet_amount) as total_stake,
    			sum(a.bet_amount) as valid_bet,
    			
    			(sum(a.win_loss * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
    			((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
    			(sum(a.jackpot_money * (a.agt_share/100))*-1) as bonus,
    			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as total, #agt_total,
    			
    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
				)*-1 as pl_total
    
    			FROM tbl_savan_bet_slot_record_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.currency
    		)t, (SELECT @rownum := 0) r
    		");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANWinLossDetails($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
   	 	//additional parameter
		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere="";
			}
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance='';
		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
			if($_GET['testCurrency']==1){
				$strQueryWhere=" AND a.currency!='TEST'";
			}else{
				$strQueryWhere=" ";
			}
			$strQueryWhereForBalance=" ";
		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
			$strQueryWhereForBalance=" ";
		}
    	//Agent ID
    	$strAccountIDWhere='';
    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
    
    	if(trim($_GET['accountID'])==''){
    		$strAccountIDWhere='';
    	}else{
    		if($intAccountIDLength!=10){
    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
    		}else{
    			$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
    		}
    	}
    	$pl_total='';
    	if(trim($_GET['accountID'])==''){
    		$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
    		)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
    		)*-1 as pl_total";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    			SELECT  @rownum := @rownum + 1 row, t.*
    			FROM  (
	    			SELECT
	    			substring(a.account_id,1,$intAccountIDLength) as account_id,
	    			j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
	    			sum(a.win_loss) as mem_win_loss,
					sum(a.commission) as mem_comm,
					sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as mem_bonus,
					((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as mem_total,
					(SELECT balance FROM vwGetSAVANVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id 
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
	    
	    			(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
				    ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    				(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
				    ((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
				    
				    ((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
				    ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
    				((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as ma_bonus,
				    (((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as ma_total,
				    
				    ((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
				    ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
     				((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as sma_bonus,
				    (((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as sma_total,
				    
				    ((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
				    ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
    				((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as sc_bonus,
				    (((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as sc_total,
				    
				    ((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
				    (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
				    ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)  as mc_bonus,
				    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
				        
				    ".$pl_total."
				        
	    				
	    			FROM tbl_savan_bet_slot_record_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
	    			GROUP BY substring(a.account_id,1,$intAccountIDLength)
	    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetailsCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetailsCount()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetSAVANVEGASMemberSlotWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetails
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
       	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSAVANVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . "  ORDER BY t.bet_date DESC,t.id desc LIMIT $startIndex , $limit");
       	$rows = $command->query();
       	return $rows;
    }
    
    /**
     * @todo getSAVANMemberWinLossDetailsSummaryFooter
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getSAVANMemberWinLossDetailsSummaryFooter()
    {
    	if($_GET['intGame']!==''){
    		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
    	}else{
    		$addWhere="";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row,
    			count(t.win_loss) as bet_count,
    			sum(t.bet_amount) as bet_amount,
    			sum(t.win_loss) as win_loss,
    			sum(t.commission) as commission,
    			sum(t.bonus) as bonus,
    			sum(t.total) as total,
    			(Select t.balance from vwGetSAVANVEGASMemberSlotWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
    			FROM vwGetSAVANVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
    	$rows = $command->query();
    	foreach($rows as $row){
    		echo $row['bet_count'] . ';';
    		echo $row['bet_amount'] . ';';
    		echo $row['win_loss'] . ';';
    		echo $row['commission'] . ';';
    		echo $row['bonus'] . ';';
    		echo $row['total'] . ';';
    		echo $row['balance'];
    	}
    }

    
    
    
    
    
    /**
     * @todo getVIRTUAWinLoss
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getVIRTUAWinLoss($orderField, $sortType, $startIndex, $limit)
    {
    	$orderField=split(",",$orderField);
    	$orderField=$orderField[0];
    
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    		$strQueryWhereForBalance=" ";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance=" ";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
    				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    				FROM  (
    				SELECT
    				SUBSTRING(a.account_id,1,2) as account_id,
    				j.account_name,
    				a.currency_name as currency_name,
    				count(a.account_id) as bet_count,
    				avg(a.amount_wagers) as average_bet,
    				sum(a.amount_wagers) as total_stake,
    				sum(a.amount_wagers) as valid_bet,
    				sum(0) as tips,
    					
    				(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    				)*-1 as total,
    					
    				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
    				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
    				(sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)*-1) as mc_bonus,
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as mc_total,
    					
    				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as pl_total
    					
    				FROM tbl_virtuavegas_casino_history a
    				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    				GROUP BY SUBSTRING(a.account_id,1,2)
    		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    				");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
    			#SUB COMPANY WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,4) as account_id,
    			j.account_name,
    			a.currency_name as currency_name,
    			count(a.account_id) as bet_count,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_wagers) as total_stake,
    			sum(a.amount_wagers) as valid_bet,
    			sum(0) as tips,
    				
    			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    			)*-1 as total,
    				
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
    			((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
    			0 as mc_bonus,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
    				
    			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    			)*-1 as pl_total
    				
    			FROM tbl_virtuavegas_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,4)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    		");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$command = $connection->createCommand("
    			#SENIOR MASTER WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,6) as account_id,
    			j.account_name,
    			a.currency_name as currency_name,
    			count(a.account_id) as bet_count,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_wagers) as total_stake,
    			sum(a.amount_wagers) as valid_bet,
    			sum(0) as tips,
    						
    			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    			)*-1 as total,
    
 				((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
    			((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
    			0 as mc_bonus,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
    				
    			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    			)*-1 as pl_total
    						
    			FROM tbl_virtuavegas_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,6)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    		");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
    			#MASTER WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,8) as account_id,
    			j.account_name,
    			a.currency_name as currency_name,
    			count(a.account_id) as bet_count,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_wagers) as total_stake,
    			sum(a.amount_wagers) as valid_bet,
    			sum(0) as tips,
    										
    			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    			)*-1 as total,
    										
    			((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
    			((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
    			0 as mc_bonus,
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
    										
    			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    			)*-1 as pl_total
    										
    			FROM tbl_virtuavegas_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,8)
    			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    		");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$command = $connection->createCommand("
    			#MASTER WINLOSS
    			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
    			FROM  (
    			SELECT
    			a.account_id as account_id,
    			b.account_name,
    			a.currency_name as currency_name,
    			count(a.account_id) as bet_count,
    			avg(a.amount_wagers) as average_bet,
    			sum(a.amount_wagers) as total_stake,
    			sum(a.amount_wagers) as valid_bet,
    			sum(0) as tips,
    							
    			sum((a.amount_settlements - a.amount_wagers)) as win_loss,
    			sum(a.commission) as commission,
    			sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)  as bonus,
    			((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) as total,
    			(SELECT balance FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
    			AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
    						
    			(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
    			((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
    			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
    			)*-1 as pl_total
    							
    			FROM tbl_virtuavegas_casino_history a
    		    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    		    WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
        		GROUP BY a.account_id
    		    )t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
    	    ");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getVIRTUAWinLossCount
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getVIRTUAWinLossCount()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance='';
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    		$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    		$strQueryWhereForBalance=" ";
    	}
    
    	$connection = Yii::app()->db_cv999_fd_master;
    	if(trim($_GET['accountID'])==''){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (SELECT
    			SUBSTRING(a.account_id,1,2) as account_id
    			FROM tbl_virtuavegas_casino_history a
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,2)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==2){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,4) as account_id
    										
    			FROM tbl_virtuavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,4)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==4){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,6) as account_id
    			FROM tbl_virtuavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,6)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==6){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    			SELECT
    			SUBSTRING(a.account_id,1,8) as account_id
    			FROM tbl_virtuavegas_casino_history a
    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY SUBSTRING(a.account_id,1,8)
    			)t
    		");
    	}elseif(strlen(trim($_GET['accountID']))==8){
    		$command = $connection->createCommand("
    			SELECT  COUNT(0)
    			FROM  (
    			SELECT
    			a.account_id as account_id
    			FROM tbl_virtuavegas_casino_history a
    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    			GROUP BY a.account_id
    			)t
    		");
    	}
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getVIRTUATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getVIRTUATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
	    	SELECT  @rownum := @rownum + 1 row, t.*
	    	FROM  (
	    	SELECT
	    	a.bet_date as bet_date,
	    	SUBSTRING(a.account_id,1,2) as id,
	    	a.currency_name as currency_name,
	    	cu.exchange_rate,
	    	count(a.account_id) as bet_count,
	    	sum(a.amount_wagers) as total_stake,
	    	sum(a.amount_wagers) as valid_bet,
	    
	    	((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as win_loss,
			(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
    		(sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)*-1) as bonus,
    		(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as total,
    												
    		(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as pl_total
    
			FROM tbl_virtuavegas_casino_history a
    		LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    		LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    		LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
    		WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    		GROUP BY a.currency_name
    		)t, (SELECT @rownum := 0) r
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getVIRTUACSTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getVIRTUACSTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
    		FROM  (
    		SELECT
    		a.bet_date as bet_date,
    		SUBSTRING(a.account_id,1,2) as id,
    		a.currency_name as currency_name,
    		cu.exchange_rate,
    		count(a.account_id) as bet_count,
    		sum(a.amount_wagers) as total_stake,
    		sum(a.amount_wagers) as valid_bet,
    																 
    		((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
    		((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
    		0 as bonus,
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as total, #sc_total,
    																 
    		(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    		((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
    		)*-1 as pl_total
    
    		FROM tbl_virtuavegas_casino_history a
    		LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    		LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
    		LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
    		WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    		GROUP BY a.currency_name
    		)t, (SELECT @rownum := 0) r
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getVIRTUASMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getVIRTUASMATotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
    		FROM  (
    		SELECT
    		a.bet_date as bet_date,
    		SUBSTRING(a.account_id,1,4) as id,
    		a.currency_name as currency_name,
    		cu.exchange_rate,
    		count(a.account_id) as bet_count,
    		sum(a.amount_wagers) as total_stake,
    		sum(a.amount_wagers) as valid_bet,
    												 
    		((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
    		((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
    		0 as bonus,
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as total, #sma_total,
    														 
    		(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    		((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
    		)*-1 as pl_total
    
    		FROM tbl_virtuavegas_casino_history a
    		LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    		LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
    		LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
    		WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    		GROUP BY a.currency_name
    		)t, (SELECT @rownum := 0) r
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getVIRTUAMATotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getVIRTUAMATotal()
    {
 		//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("
    		SELECT  @rownum := @rownum + 1 row, t.*
    		FROM  (
    		SELECT
    		a.bet_date as bet_date,
    		SUBSTRING(a.account_id,1,6) as id,
    		a.currency_name as currency_name,
    		cu.exchange_rate,
    		count(a.account_id) as bet_count,
    		sum(a.amount_wagers) as total_stake,
    		sum(a.amount_wagers) as valid_bet,
    		((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
    		((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
    		0 as bonus,
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as total, #ma_total,
    															 
    		(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
    		((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
    		)*-1 as pl_total
    
    		FROM tbl_virtuavegas_casino_history a
    		LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    		LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
    		LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
    		WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    		GROUP BY a.currency_name
    		)t, (SELECT @rownum := 0) r
    	");
    	$rows = $command->query();
    	return $rows;
    }
    
    /**
     * @todo getVIRTUAAGTTotal
     * @copyright CE
     * @author Leo karl
     * @since 2012-06-01
     */
    public function getVIRTUAAGTTotal()
    {
    	//additional parameter
    	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere="";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
    		if($_GET['testCurrency']==1){
    			$strQueryWhere=" AND a.currency_name!='TEST'";
    		}else{
    			$strQueryWhere=" ";
    		}
    	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
    		$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
    	}
    	$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("
			SELECT  @rownum := @rownum + 1 row, t.*
			FROM  (
			SELECT
			a.bet_date as bet_date,
			SUBSTRING(a.account_id,1,8) as id,
			a.currency_name as currency_name,
			cu.exchange_rate,
			count(a.account_id) as bet_count,
			sum(a.amount_wagers) as total_stake,
			sum(a.amount_wagers) as valid_bet,
															 
			(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
			((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
			0 as bonus,
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as total, #agt_total,
						 
			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
			)*-1 as pl_total

			FROM tbl_virtuavegas_casino_history a
			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
			LEFT JOIN tbl_currency cu ON a.currency_name=cu.currency_name
			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
			GROUP BY a.currency_name
			)t, (SELECT @rownum := 0) r
		");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getVIRTUAWinLossDetails
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
	public function getVIRTUAWinLossDetails($orderField, $sortType, $startIndex, $limit)
	{
	 	$orderField=split(",",$orderField);
	 	$orderField=$orderField[0];
	 
	 	//additional parameter
	 	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
	 		if($_GET['testCurrency']==1){
	 			$strQueryWhere=" AND a.currency_name!='TEST'";
	 		}else{
	 			$strQueryWhere="";
	 		}
	 		$strQueryWhereForBalance='';
	 	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
		 	$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		 	$strQueryWhereForBalance='';
	 	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		 	if($_GET['testCurrency']==1){
		 		$strQueryWhere=" AND a.currency_name!='TEST'";
		 	}else{
			 	$strQueryWhere=" ";
		 	}
		 	$strQueryWhereForBalance=" ";
	 	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
		 	$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
		 	$strQueryWhereForBalance=" ";
	 	}
	 	//Agent ID
	 	$strAccountIDWhere='';
	 	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
	 
	 	if(trim($_GET['accountID'])==''){
	 		$strAccountIDWhere='';
	 	}else{
	 		if($intAccountIDLength!=10){
	 			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
		 	}else{
		 		$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
	 		}
	 	}
	 	$pl_total='';
	 	if(trim($_GET['accountID'])==''){
	 		$pl_total="(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as pl_total";
	 	}elseif(strlen(trim($_GET['accountID']))==2){
		 	$pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
		 	((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		 	(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
		 	(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
		 	(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
		 	)*-1 as pl_total";
	 	}elseif(strlen(trim($_GET['accountID']))==4){
			$pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
			)*-1 as pl_total";
    	}elseif(strlen(trim($_GET['accountID']))==6){
		    $pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
		    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
		    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
		    )*-1 as pl_total";
		}elseif(strlen(trim($_GET['accountID']))==8){
		    $pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
		    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
		    )*-1 as pl_total";
	    }
	    $connection = Yii::app()->db_cv999_fd_master;
	    $command = $connection->createCommand("
		    SELECT  @rownum := @rownum + 1 row, t.*
		    FROM  (
		    SELECT
		    substring(a.account_id,1,$intAccountIDLength) as account_id,
		    j.account_name,
		    a.currency_name as currency_name,
		    count(a.account_id) as bet_count,
		    avg(a.amount_wagers) as average_bet,
		    sum(a.amount_wagers) as total_stake,
		    sum(a.amount_wagers) as valid_bet,
		    sum(0) as tips,
		    sum((a.amount_settlements - a.amount_wagers)) as mem_win_loss,
		    sum(a.commission) as mem_comm,
		    sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)  as mem_bonus,
		    ((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) as mem_total,
		    (SELECT balance FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
		    AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
		     
		    (sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
		    ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
		    ((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
		    
		    ((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
		    ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
		    (((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
		    
		    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
		    ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
		    (((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
		    
		    ((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
		    ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
    		(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
    
			((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
			(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
			(sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)*-1)  as mc_bonus,
			(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as mc_total,
    
    		".$pl_total."
    
    	    
    		FROM tbl_virtuavegas_casino_history a
	    	LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
    		LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
    		WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
    	    GROUP BY substring(a.account_id,1,$intAccountIDLength)
    	    )t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
        ");
        $rows = $command->query();
        return $rows;
	}
	
	/**
	 * @todo getVIRTUAMemberWinLossDetailsCount
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-06-01
	 */
    public function getVIRTUAMemberWinLossDetailsCount()
    {
     	if($_GET['intGame']!==''){
     		$addWhere="AND game_id='" . $_GET['intGame'] . "'";
     	}else{
     		$addWhere="";
     	}
     	$connection = Yii::app()->db_cv999_fd_master;
     	$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
     	$rows = $command->query();
     	return $rows;
     }
     
     /**
      * @todo getVIRTUAMemberWinLossDetails
      * @copyright CE
      * @author Leo karl
      * @since 2012-06-01
      */
     public function getVIRTUAMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
     {
     	$orderField=split(",",$orderField);
     	$orderField=$orderField[0];
     	if($_GET['intGame']!==''){
     		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
     	}else{
     		$addWhere="";
     	}
     	$connection = Yii::app()->db_cv999_fd_master;
        	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . "  ORDER BY t.bet_date DESC,t.id desc LIMIT $startIndex , $limit");
        	$rows = $command->query();
        	return $rows;
     }
     
     /**
      * @todo getVIRTUAMemberWinLossDetailsSummaryFooter
      * @copyright CE
      * @author Leo karl
      * @since 2012-06-01
      */
     public function getVIRTUAMemberWinLossDetailsSummaryFooter()
     {
     	if($_GET['intGame']!==''){
     		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
     	}else{
     		$addWhere="";
     	}
     	$connection = Yii::app()->db_cv999_fd_master;
     	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row,
     			count(t.win_loss) as bet_count,
     			sum(t.bet_amount) as bet_amount,
     			sum(t.win_loss) as win_loss,
     			sum(t.commission) as commission,
     			sum(t.bonus) as bonus,
     			sum(t.total) as total,
     			(Select t.balance from vwGetVIRTUAVEGASMemberSlotWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
     			FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
     	$rows = $command->query();
     	foreach($rows as $row){
     		echo $row['bet_count'] . ';';
     		echo $row['bet_amount'] . ';';
     		echo $row['win_loss'] . ';';
     		echo $row['commission'] . ';';
     		echo $row['bonus'] . ';';
     		echo $row['total'] . ';';
     		echo $row['balance'];
     	}
     }
     
     
     
     /**
      * @todo getSLOTWinLoss
      * @copyright CE
      * @author Allan Bicol
      * @since 2014-01-10
      */
     public function getSLOTWinLoss($orderField, $sortType, $startIndex, $limit)
     {
     	$orderField=split(",",$orderField);
     	$orderField=$orderField[0];
     
     	//additional parameter
     	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     		if($_GET['testCurrency']==1){
     			$strQueryWhere=" AND a.currency!='TEST'";
     		}else{
     			$strQueryWhere="";
     		}
     		$strQueryWhereForBalance='';
     	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     		$strQueryWhereForBalance='';
     	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     		if($_GET['testCurrency']==1){
     			$strQueryWhere=" AND a.currency!='TEST'";
     		}else{
     			$strQueryWhere=" ";
     		}
     		$strQueryWhereForBalance=" ";
     	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     		$strQueryWhereForBalance=" ";
     	}
     
     	$connection = Yii::app()->db_cv999_fd_master;
     	if(trim($_GET['accountID'])==''){
     		$command = $connection->createCommand("
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,2) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
			
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
					)*-1 as total,
			
					((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
					(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
					((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
			
					(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total
			
					FROM tbl_savan_slot_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     				GROUP BY SUBSTRING(a.account_id,1,2)
     		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
     				");
     	}elseif(strlen(trim($_GET['accountID']))==2){
     	$command = $connection->createCommand("
     			#SUB COMPANY WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,4) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
			
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
					)*-1 as total,
			
					((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
					((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
					((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as mc_total,#sc_total,
					
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
					)*-1 as pl_total
					
					FROM tbl_savan_slot_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     					GROUP BY SUBSTRING(a.account_id,1,4)
     					)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
     					");
     	}elseif(strlen(trim($_GET['accountID']))==4){
				$command = $connection->createCommand("
     						#SENIOR MASTER WINLOSS
				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,6) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
			
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
					)*-1 as total,
     
					((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
					((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
					((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as mc_total,#sma_total,
			
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
					(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
					)*-1 as pl_total
					
					FROM tbl_savan_slot_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     						GROUP BY SUBSTRING(a.account_id,1,6)
     						)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
     								");
     	}elseif(strlen(trim($_GET['accountID']))==6){
     	$command = $connection->createCommand("
     			#MASTER WINLOSS
     			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					SUBSTRING(a.account_id,1,8) as account_id,
					j.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
			
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
					)*-1 as total,
			
					((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
					((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
					((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as mc_bonus,
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as mc_total,#ma_total,
			
					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
					(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
					)*-1 as pl_total
					
					FROM tbl_savan_slot_casino_history a
				    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY SUBSTRING(a.account_id,1,8)
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
     					");
     	}elseif(strlen(trim($_GET['accountID']))==8){
     	$command = $connection->createCommand("
     		#MASTER WINLOSS
     		SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
				FROM  (
					SELECT
					a.account_id as account_id,
					b.account_name,
					a.currency as currency_name,
					count(a.account_id) as bet_count,
					avg(a.bet_amount) as average_bet,
					sum(a.bet_amount) as total_stake,
					sum(a.bet_amount) as valid_bet,
					sum(0) as tips,
			
					sum(a.win_loss) as win_loss,
					sum(a.commission) as commission,
					sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as bonus,
					((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as total,
					(SELECT balance FROM vwGetSLOTVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
					AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     						
     					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
     					((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     					(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
     					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
     
     					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
			     		)*-1 as pl_total
			     			
			     		FROM tbl_savan_slot_casino_history a
			     		LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
			     		WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
			     				GROUP BY a.account_id
						)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
			     				");
			     	}
			     				$rows = $command->query();
			     				return $rows;
			     	}
     
     				/**
     				* @todo getSLOTWinLossCount
     				* @copyright CE
     				* @author Allan Bicol
		     		* @since 2014-01-10
		     		 */
		     		 public function getSLOTWinLossCount()
		     		 {
		     		 //additional parameter
		     		 if($_GET['strCurrency']=='' and $_GET['intGame']==''){
		     			if($_GET['testCurrency']==1){
		     				$strQueryWhere=" AND a.currency!='TEST'";
					}else{
		     			$strQueryWhere="";
					}
					$strQueryWhereForBalance='';
					}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
		     			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     			$strQueryWhereForBalance='';
		     			}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		     			if($_GET['testCurrency']==1){
		     			$strQueryWhere=" AND a.currency!='TEST'";
					}else{
		     			$strQueryWhere=" ";
					}
					$strQueryWhereForBalance=" AND game_type='" . $_GET['intGame'] . "'";
		     			}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
					$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     			$strQueryWhereForBalance=" ";
		     			}
		     
		     			$connection = Yii::app()->db_cv999_fd_master;
		     			if(trim($_GET['accountID'])==''){
		     					$command = $connection->createCommand("
		     							SELECT  COUNT(0)
		     							FROM  (SELECT
		     									SUBSTRING(a.account_id,1,2) as account_id
							FROM tbl_savan_slot_casino_history a
							WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		     					GROUP BY SUBSTRING(a.account_id,1,2)
		     					)t
		     							");
		     									}elseif(strlen(trim($_GET['accountID']))==2){
					$command = $connection->createCommand("
		     					SELECT  COUNT(0)
		     					FROM  (
		     							SELECT
		     							SUBSTRING(a.account_id,1,4) as account_id
					
							FROM tbl_savan_slot_casino_history a
							LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
							WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
							GROUP BY SUBSTRING(a.account_id,1,4)
		     					)t
		     							");
		     					}elseif(strlen(trim($_GET['accountID']))==4){
					$command = $connection->createCommand("
						SELECT  COUNT(0)
		     				FROM  (
		     						SELECT
		     						SUBSTRING(a.account_id,1,6) as account_id
							FROM tbl_savan_slot_casino_history a
							LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
							WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
							GROUP BY SUBSTRING(a.account_id,1,6)
		     									)t
		     					");
		    	}elseif(strlen(trim($_GET['accountID']))==6){
		         	$command = $connection->createCommand("
		         	SELECT  COUNT(0)
		         	FROM  (
		         			SELECT
								SUBSTRING(a.account_id,1,8) as account_id
		     
								FROM tbl_savan_slot_casino_history a
								LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
								WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		         			GROUP BY SUBSTRING(a.account_id,1,8)
		         			)t
		         			");
		         	}elseif(strlen(trim($_GET['accountID']))==8){
					$command = $connection->createCommand("
		     					SELECT  COUNT(0)
		     					FROM  (
		     							SELECT
		     							a.account_id as account_id
							FROM tbl_savan_slot_casino_history a
						    LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
							WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
							GROUP BY a.account_id
		     							)t
		     									");
		         	}
		         	$rows = $command->query();
		    	return $rows;
		     			}
		     
		     			/**
		     			* @todo getSLOTTotal
		     			* @copyright CE
		     			* @author Allan Bicol
		     			 * @since 2014-01-10
		     			 */
		     		public function getSLOTTotal()
		     			 {
		     			 //additional parameter
		     			if($_GET['strCurrency']=='' and $_GET['intGame']==''){
		     				if($_GET['testCurrency']==1){
		     				$strQueryWhere=" AND a.currency!='TEST'";
		     				}else{
		     						$strQueryWhere="";
		     				}
		     				}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
		     						$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     			}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		     					if($_GET['testCurrency']==1){
		    			$strQueryWhere=" AND a.currency!='TEST'";
		     						}else{
		    			$strQueryWhere=" ";
		     						}
		     						}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
		     						$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     			}
		     			$connection = Yii::app()->db_cv999_fd_master;
		     			$command = $connection->createCommand("
		     					SELECT  @rownum := @rownum + 1 row, t.*
		     			FROM  (
		     					SELECT
		     					a.bet_time as bet_date,
		    			SUBSTRING(a.account_id,1,2) as id,
		    			a.currency as currency_name,
		    			cu.exchange_rate,
		    			count(a.account_id) as bet_count,
		    			sum(a.bet_amount) as total_stake,
		    			sum(a.bet_amount) as valid_bet,
		     
		    			((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as win_loss,
					    (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as commission,
		    			((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1) as bonus,
					    (((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as total,
					
						(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total
		     
		    			FROM tbl_savan_slot_casino_history a
		    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
		    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
		    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
		    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		    			GROUP BY a.currency
		     			)t, (SELECT @rownum := 0) r
		     			");
		     					$rows = $command->query();
		     					return $rows;
			}
		     
			/**
			 * @todo getSLOTCSTotal
			 * @copyright CE
		     	 * @author Allan Bicol
		     	 * @since 2014-01-10
		     	 */
		     	 public function getSLOTCSTotal()
		     	 {
		     	 //additional parameter
		     	 if($_GET['strCurrency']=='' and $_GET['intGame']==''){
		     	 if($_GET['testCurrency']==1){
		     	 	$strQueryWhere=" AND a.currency!='TEST'";
		    		}else{
		     	 	$strQueryWhere="";
		     	 }
		     	 }else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
		    		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     	 }else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		     	 	if($_GET['testCurrency']==1){
		    			$strQueryWhere=" AND a.currency!='TEST'";
		     	 }else{
		     	 $strQueryWhere=" ";
		     	 }
		     	 }else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
		     	 	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     	 	}
		     	 	$connection = Yii::app()->db_cv999_fd_master;
		     	 			$command = $connection->createCommand("
		     	 					SELECT  @rownum := @rownum + 1 row, t.*
					FROM  (
		     			SELECT
		     			a.bet_time as bet_date,
		     			SUBSTRING(a.account_id,1,2) as id,
		     			a.currency as currency_name,
		    			cu.exchange_rate,
		    			count(a.account_id) as bet_count,
		    			sum(a.bet_amount) as total_stake,
		    			sum(a.bet_amount) as valid_bet,
		    
		    			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as win_loss,#sc_win_loss,
		    			((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as commission,#sc_comm,
		    			((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as bonus,
		    			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as total, #sc_total,
		    
		    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
						((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
		    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
						(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
						(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
						)*-1 as pl_total
		     
		    			FROM tbl_savan_slot_casino_history a
		    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
		    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
		    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
		    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,2) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		    			GROUP BY a.currency
		    		)t, (SELECT @rownum := 0) r
		         		");
		         				$rows = $command->query();
		         				return $rows;
		    }
		     
		         /**
		         * @todo getSLOTSMATotal
		         * @copyright CE
			     * @author Allan Bicol
			     * @since 2014-01-10
			     */
		        public function getSLOTSMATotal()
		         {
         			//additional parameter
         			if($_GET['strCurrency']=='' and $_GET['intGame']==''){
         			if($_GET['testCurrency']==1){
         			$strQueryWhere=" AND a.currency!='TEST'";
		     	 }else{
		     	 	$strQueryWhere="";
		     	 }
		     	 }else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
		     	 	$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		    	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		         	if($_GET['testCurrency']==1){
		         	$strQueryWhere=" AND a.currency!='TEST'";
		    		}else{
		         			$strQueryWhere=" ";
		         	}
		         	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
		         		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		    	}
		         				$connection = Yii::app()->db_cv999_fd_master;
		         				$command = $connection->createCommand("
		         						SELECT  @rownum := @rownum + 1 row, t.*
					FROM  (
		     					SELECT
		     					a.bet_time as bet_date,
		         				SUBSTRING(a.account_id,1,4) as id,
		         				a.currency as currency_name,
		         				cu.exchange_rate,
		    			count(a.account_id) as bet_count,
		    			sum(a.bet_amount) as total_stake,
		    			sum(a.bet_amount) as valid_bet,
		    
		    			((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as win_loss,#sma_win_loss,
		    			((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as commission,#sma_comm,
		    			((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as bonus,
		    			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as total, #sma_total,
		    
		    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
						((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
		    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
						(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
						)*-1 as pl_total
		     
		    			FROM tbl_savan_slot_casino_history a
		    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
		    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
		    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
		    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,4) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		         						GROUP BY a.currency
		    		)t, (SELECT @rownum := 0) r
		         								");
		    	$rows = $command->query();
		         			return $rows;
		     	 }
		     
		    /**
		     * @todo getSLOTMATotal
	          * @copyright CE
	          * @author Allan Bicol	
		     * @since 2014-01-10
		     */
		    public function getSLOTMATotal()
		    {
	         //additional parameter
	         if($_GET['strCurrency']=='' and $_GET['intGame']==''){
	         if($_GET['testCurrency']==1){
	         $strQueryWhere=" AND a.currency!='TEST'";
		     }else{
		     $strQueryWhere="";
		     }
		     }else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
		     $strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     }else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
		     if($_GET['testCurrency']==1){
		     $strQueryWhere=" AND a.currency!='TEST'";
		     }else{
		     $strQueryWhere=" ";
		     }
		     }else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
		     		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     }
		     				$connection = Yii::app()->db_cv999_fd_master;
		     				$command = $connection->createCommand("
		     				SELECT  @rownum := @rownum + 1 row, t.*
		     				FROM  (
		     				SELECT
		     				a.bet_time as bet_date,
		     				SUBSTRING(a.account_id,1,6) as id,
		     				a.currency as currency_name,
		    			cu.exchange_rate,
		     				count(a.account_id) as bet_count,
		     				sum(a.bet_amount) as total_stake,
		     				sum(a.bet_amount) as valid_bet,
		     
		    			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as win_loss,#ma_win_loss,
		    			((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as commission,#ma_comm,
		    			((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as bonus,
		    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as total, #ma_total,
		    
		    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
						((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
		    			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
						)*-1 as pl_total
		     
		    			FROM tbl_savan_slot_casino_history a
		    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
		    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
		    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
		    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,6) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		    			GROUP BY a.currency
		    		)t, (SELECT @rownum := 0) r
		    		");
		    	$rows = $command->query();
		    	return $rows;
		     }
		     
		     				/**
						    * @todo getSLOTAGTTotal
						    * @copyright CE
     						* @author Allan Bicol
     						* @since 2014-01-10
     						*/
					    public function getSLOTAGTTotal()
					         {
					     						//additional parameter
					     						if($_GET['strCurrency']=='' and $_GET['intGame']==''){
					    		if($_GET['testCurrency']==1){
					         		$strQueryWhere=" AND a.currency!='TEST'";
					     }else{
					     $strQueryWhere="";
					     }
					     }else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
					     $strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
					     				}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
					     				if($_GET['testCurrency']==1){
					     				$strQueryWhere=" AND a.currency!='TEST'";
					    		}else{
					    			$strQueryWhere=" ";
					     				}
					     				}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
					     					$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
					     				}
					     				$connection = Yii::app()->db_cv999_fd_master;
					    	$command = $connection->createCommand("
					         	SELECT  @rownum := @rownum + 1 row, t.*
					         	FROM  (
					    			SELECT
		         			a.bet_time as bet_date,
		         			SUBSTRING(a.account_id,1,8) as id,
		         			a.currency as currency_name,
		         					cu.exchange_rate,
		         					count(a.account_id) as bet_count,
		         							sum(a.bet_amount) as total_stake,
		         							sum(a.bet_amount) as valid_bet,
		         							 
		         							(sum(a.win_loss * (a.agt_share/100))*-1) as win_loss,#agt_win_loss,
		    			((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as commission,#agt_comm,
		    			(sum(a.jackpot_money * (a.agt_share/100))*-1) as bonus,
		    			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as total, #agt_total,
		    
		    			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
						((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
						)*-1 as pl_total
		     
		    			FROM tbl_savan_slot_casino_history a
		    			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
		    			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
		    			LEFT JOIN tbl_currency cu ON a.currency=cu.currency_name
		    			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND SUBSTRING(a.account_id,1,8) like '". $_GET['accountID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		    			GROUP BY a.currency
		    		)t, (SELECT @rownum := 0) r
		    		");
		    	$rows = $command->query();
		    	return $rows;
		    }
		     
		    /**
		     * @todo getSLOTWinLossDetails
		     * @copyright CE
		     * @author Allan Bicol
		     * @since 2013-01-10
		    */
		    public function getSLOTWinLossDetails($orderField, $sortType, $startIndex, $limit)
		         							{
		         								$orderField=split(",",$orderField);
		    	$orderField=$orderField[0];
		     
		         								//additional parameter
		         								if($_GET['strCurrency']=='' and $_GET['intGame']==''){
		         								if($_GET['testCurrency']==1){
		         								$strQueryWhere=" AND a.currency!='TEST'";
		     				}else{
		     				$strQueryWhere="";
		     				}
		     				$strQueryWhereForBalance='';
		     				}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
		     				$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     						$strQueryWhereForBalance='';
		     				}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
					if($_GET['testCurrency']==1){
		     			$strQueryWhere=" AND a.currency!='TEST'";
		     				}else{
		     				$strQueryWhere=" ";
		     				}
		     						$strQueryWhereForBalance=" ";
		     				}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
		     				$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
		     				$strQueryWhereForBalance=" ";
		     				}
		    	//Agent ID
		         	$strAccountIDWhere='';
		    	$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
		     
		         	if(trim($_GET['accountID'])==''){
		         	$strAccountIDWhere='';
		     				}else{
		     						if($intAccountIDLength!=10){
		    			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
		     						}else{
		     						$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
		    		}
		     				}
		     				$pl_total='';
		     				if(trim($_GET['accountID'])==''){
		     				$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total";
		     				}elseif(strlen(trim($_GET['accountID']))==2){
		     				$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
		    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
		    		(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
		    		)*-1 as pl_total";
		    	}elseif(strlen(trim($_GET['accountID']))==4){
		    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
		    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
		    		(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
		    		)*-1 as pl_total";
		    	}elseif(strlen(trim($_GET['accountID']))==6){
		    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
		    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
		    		(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
		    		)*-1 as pl_total";
		    	}elseif(strlen(trim($_GET['accountID']))==8){
		    		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
		    		((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
		    		)*-1 as pl_total";
		    	}
		    	$connection = Yii::app()->db_cv999_fd_master;
		    	$command = $connection->createCommand("
		    			SELECT  @rownum := @rownum + 1 row, t.*
		    			FROM  (
			    			SELECT
			    			substring(a.account_id,1,$intAccountIDLength) as account_id,
			    			j.account_name,
							a.currency as currency_name,
		         				count(a.account_id) as bet_count,
		         				avg(a.bet_amount) as average_bet,
		         				sum(a.bet_amount) as total_stake,
		         						sum(a.bet_amount) as valid_bet,
		         						sum(0) as tips,
		         						sum(a.win_loss) as mem_win_loss,
		         						sum(a.commission) as mem_comm,
		         						sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as mem_bonus,
		         						((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as mem_total,
		         						(SELECT balance FROM vwGetSLOTVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
		         						AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
		         						 
		         						(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
		         						((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
		         						(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
		         								((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
		     
		         								((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
		         								((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
		         								((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as ma_bonus,
		         								(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as ma_total,
		     
		         								((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
		         								((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
		         								((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as sma_bonus,
		         								(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as sma_total,
		     
		         								((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
		         								((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
		         								((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as sc_bonus,
		         								(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as sc_total,
		     
		         								((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
		         								(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
		         								((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)  as mc_bonus,
		         								(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
		     
		         								".$pl_total."
		     
		         								 
		         								FROM tbl_savan_slot_casino_history a
		         								LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
		         								LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
		         								WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
		         								GROUP BY substring(a.account_id,1,$intAccountIDLength)
			    	)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc LIMIT $startIndex , $limit
		         								");
		         										$rows = $command->query();
		         										return $rows;
		     				}
		     
		     				/**
		     				* @todo getSLOTMemberWinLossDetailsCount
		     				* @copyright CE
		     				* @author Allan Bicol
		     				* @since 2014-01-10
		     				*/
		         				public function getSLOTMemberWinLossDetailsCount()
		         				{
		         				 if($_GET['intGame']!==''){
		         				 $addWhere="AND game_id='" . $_GET['intGame'] . "'";
		     				}else{
		     				$addWhere="";
		     				}
		     				$connection = Yii::app()->db_cv999_fd_master;
		     				$command = $connection->createCommand("SELECT COUNT(0) FROM vwGetSLOTVEGASMemberSlotWinLossDetails WHERE bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='" . $_GET['dtTo'] . "' AND  account_id='" . $_GET['strAccountID'] . "' " . $addWhere);
					    	$rows = $command->query();
					    	return $rows;
					    }
		     
		     			/**
		     			* @todo getSLOTMemberWinLossDetails
		     			* @copyright CE
		     			* @author Allan Bicol	
		          		* @since 2014-01-10
	     				*/
	     				public function getSLOTMemberWinLossDetails($orderField, $sortType, $startIndex , $limit)
	     				{
	     				$orderField=split(",",$orderField);
	     				$orderField=$orderField[0];
	     				if($_GET['intGame']!==''){
	     				$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
	     				}else{
	     				$addWhere="";
	     				}
	     				$connection = Yii::app()->db_cv999_fd_master;
	     				$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSLOTVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . "  ORDER BY t.bet_date DESC,t.id desc LIMIT $startIndex , $limit");
	     				$rows = $command->query();
	       				return $rows;
	     				}
		     
			      /**
			      * @todo getSLOTMemberWinLossDetailsSummaryFooter
		          * @copyright CE
		          * @author Allan Bicol
		          * @since 2014-01-10
		          */
		          public function getSLOTMemberWinLossDetailsSummaryFooter()
		          {
		           if($_GET['intGame']!==''){
		         		$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
		         		}else{
		         		$addWhere="";
			     }
			     $connection = Yii::app()->db_cv999_fd_master;
			     $command = $connection->createCommand("SELECT @rownum := @rownum + 1 row,
			     count(t.win_loss) as bet_count,
			     		sum(t.bet_amount) as bet_amount,
			     		sum(t.win_loss) as win_loss,
			     		sum(t.commission) as commission,
			     		sum(t.bonus) as bonus,
			     		sum(t.total) as total,
			    			(Select t.balance from vwGetSLOTVEGASMemberSlotWinLossDetails t WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' ". $addWhere . " ORDER BY t.bet_date desc LIMIT 1) as balance
			    			FROM vwGetSLOTVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . " ORDER BY t.bet_date desc");
			    	$rows = $command->query();
			    	foreach($rows as $row){
			    		echo $row['bet_count'] . ';';
			    		echo $row['bet_amount'] . ';';
		         		echo $row['win_loss'] . ';';
		         				echo $row['commission'] . ';';
		         				echo $row['bonus'] . ';';
		         						echo $row['total'] . ';';
		         						echo $row['balance'];
		         						}
		         						}
		     
		     
		     
		     
		     
		     
		     
     //=====================================================EXPORT=============================================================
     public function getExportSAVANWinLoss()
     {
     	//additional parameter
     	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     		if($_GET['testCurrency']==1){
     			$strQueryWhere=" AND a.currency!='TEST'";
     		}else{
     			$strQueryWhere="";
     		}
     		$strQueryWhereForBalance='';
     	}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     		$strQueryWhereForBalance='';
     	}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     		if($_GET['testCurrency']==1){
     			$strQueryWhere=" AND a.currency!='TEST'";
     		}else{
     			$strQueryWhere=" ";
     		}
     		$strQueryWhereForBalance=" ";
     	}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     		$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     		$strQueryWhereForBalance=" ";
     	}
     
     	$connection = Yii::app()->db_cv999_fd_master;
     	if(trim($_GET['accountID'])==''){
     		$command = $connection->createCommand("
     				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     				FROM  (
     				SELECT
     				SUBSTRING(a.account_id,1,2) as account_id,
     				j.account_name,
     				a.currency as currency_name,
     				count(a.account_id) as bet_count,
     				avg(a.bet_amount) as average_bet,
     				sum(a.bet_amount) as total_stake,
     				sum(a.bet_amount) as valid_bet,
     				sum(0) as tips,
     					
     				(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
     				(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
     		)*-1 as total,
     					
     				((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
     				(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
     				((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1) as mc_bonus,
     				(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
     					
     				(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total
     					
     				FROM tbl_savan_bet_slot_record_casino_history a
     				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
     				WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     				GROUP BY SUBSTRING(a.account_id,1,2)
     		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     				");
     	}elseif(strlen(trim($_GET['accountID']))==2){
     	$command = $connection->createCommand("
     			#SUB COMPANY WINLOSS
     			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     			FROM  (
     			SELECT
     			SUBSTRING(a.account_id,1,4) as account_id,
     			j.account_name,
     			a.currency as currency_name,
     			count(a.account_id) as bet_count,
     			avg(a.bet_amount) as average_bet,
     			sum(a.bet_amount) as total_stake,
     			sum(a.bet_amount) as valid_bet,
     			sum(0) as tips,
     				
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
     	)*-1 as total,
     				
     			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
     			((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
     			((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as mc_bonus,
     			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as mc_total,#sc_total,
     				
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
     	)*-1 as pl_total
     				
     			FROM tbl_savan_bet_slot_record_casino_history a
     			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
     			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY SUBSTRING(a.account_id,1,4)
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     			");
     	}elseif(strlen(trim($_GET['accountID']))==4){
     	$command = $connection->createCommand("
     	#SENIOR MASTER WINLOSS
     	SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     	FROM  (
     	SELECT
     	SUBSTRING(a.account_id,1,6) as account_id,
     	j.account_name,
     	a.currency as currency_name,
     	count(a.account_id) as bet_count,
     	avg(a.bet_amount) as average_bet,
     	sum(a.bet_amount) as total_stake,
     	sum(a.bet_amount) as valid_bet,
     	sum(0) as tips,
     		
     	(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     	((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     	(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
     	)*-1 as total,
     
     	((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
     	((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
     	((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as mc_bonus,
     	(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as mc_total,#sma_total,
     		
     	(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     	((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     	(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     	(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
     	)*-1 as pl_total
     		
     	FROM tbl_savan_bet_slot_record_casino_history a
     	LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     	LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
     	WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     	GROUP BY SUBSTRING(a.account_id,1,6)
     	)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     	");
     	}elseif(strlen(trim($_GET['accountID']))==6){
     			$command = $connection->createCommand("
     			#MASTER WINLOSS
     			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     			FROM  (
     			SELECT
     			SUBSTRING(a.account_id,1,8) as account_id,
     			j.account_name,
     			a.currency as currency_name,
     			count(a.account_id) as bet_count,
     			avg(a.bet_amount) as average_bet,
     			sum(a.bet_amount) as total_stake,
     			sum(a.bet_amount) as valid_bet,
     			sum(0) as tips,
     				
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
     			)*-1 as total,
     				
     			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
     			((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
     			((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as mc_bonus,
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as mc_total,#ma_total,
     				
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
     			)*-1 as pl_total
     				
     			FROM tbl_savan_bet_slot_record_casino_history a
     			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
     			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY SUBSTRING(a.account_id,1,8)
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     			");
     	}elseif(strlen(trim($_GET['accountID']))==8){
     	$command = $connection->createCommand("
     	#MASTER WINLOSS
     	SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     	FROM  (
     	SELECT
     	a.account_id as account_id,
     	b.account_name,
     	a.currency as currency_name,
     	count(a.account_id) as bet_count,
     	avg(a.bet_amount) as average_bet,
     	sum(a.bet_amount) as total_stake,
     	sum(a.bet_amount) as valid_bet,
     	sum(0) as tips,
     		
     	sum(a.win_loss) as win_loss,
     	sum(a.commission) as commission,
     	sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as bonus,
     	((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as total,
     	(SELECT balance FROM vwGetSAVANVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
     	AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     		
     	(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
     	((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     	(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
     	((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
     
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
     	)*-1 as pl_total
     				
     			FROM tbl_savan_bet_slot_record_casino_history a
     			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY a.account_id
     	)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     			");
     	}
     			$rows = $command->query();
     			return $rows;
     	}
     	
     	public function getExportSAVANWinLossDetails()
     	{
     		//additional parameter
     		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency!='TEST'";
     			}else{
     				$strQueryWhere="";
     			}
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency!='TEST'";
     			}else{
     				$strQueryWhere=" ";
     			}
     			$strQueryWhereForBalance=" ";
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance=" ";
     		}
     		//Agent ID
     		$strAccountIDWhere='';
     		$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
     	
     		if(trim($_GET['accountID'])==''){
     			$strAccountIDWhere='';
     		}else{
     			if($intAccountIDLength!=10){
     				$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
     			}else{
     				$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
     			}
     		}
     		$pl_total='';
     		if(trim($_GET['accountID'])==''){
     			$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==2){
     			$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==4){
     			$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==6){
     			$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==8){
     			$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
     			)*-1 as pl_total";
     		}
     		$connection = Yii::app()->db_cv999_fd_master;
     		$command = $connection->createCommand("
     				SELECT  @rownum := @rownum + 1 row, t.*
     				FROM  (
     				SELECT
     				substring(a.account_id,1,$intAccountIDLength) as account_id,
     				j.account_name,
     				a.currency as currency_name,
     				count(a.account_id) as bet_count,
     				avg(a.bet_amount) as average_bet,
     				sum(a.bet_amount) as total_stake,
     				sum(a.bet_amount) as valid_bet,
     				sum(0) as tips,
     				sum(a.win_loss) as mem_win_loss,
     				sum(a.commission) as mem_comm,
     				sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as mem_bonus,
     				((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as mem_total,
     				(SELECT balance FROM vwGetSAVANVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
     				AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     				 
     				(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
     				((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     				(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
     				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
     	
     				((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
     				((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
     				((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as ma_bonus,
     				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as ma_total,
     	
     				((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
     				((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
     				((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as sma_bonus,
     				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as sma_total,
     	
     				((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
     				((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
     				((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as sc_bonus,
     				(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as sc_total,
     	
     				((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
     				(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
     				((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)  as mc_bonus,
     				(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
     	
     				".$pl_total."
     	
     				 
     				FROM tbl_savan_bet_slot_record_casino_history a
     				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
     						WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     						GROUP BY substring(a.account_id,1,$intAccountIDLength)
     						)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     						");
     						$rows = $command->query();
     						return $rows;
     	}
     	
     	public function getExportSAVANMemberWinLossDetails()
     	{
     		if($_GET['intGame']!==''){
     			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
     		}else{
     			$addWhere="";
     		}
     		$connection = Yii::app()->db_cv999_fd_master;
     		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSAVANVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . "  ORDER BY t.bet_date DESC,t.id desc ");
     		$rows = $command->query();
     		return $rows;
     	}
     	
     	public function getExportVIRTUAWinLoss()
     	{
     		//additional parameter
     		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency_name!='TEST'";
     			}else{
     				$strQueryWhere="";
     			}
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency_name!='TEST'";
     			}else{
     				$strQueryWhere=" ";
     			}
     			$strQueryWhereForBalance=" ";
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance=" ";
     		}
     	
     		$connection = Yii::app()->db_cv999_fd_master;
     		if(trim($_GET['accountID'])==''){
     			$command = $connection->createCommand("
     					SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     					FROM  (
     					SELECT
     					SUBSTRING(a.account_id,1,2) as account_id,
     					j.account_name,
     					a.currency_name as currency_name,
     					count(a.account_id) as bet_count,
     					avg(a.amount_wagers) as average_bet,
     					sum(a.amount_wagers) as total_stake,
     					sum(a.amount_wagers) as valid_bet,
     					sum(0) as tips,
     						
     					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     					((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     					(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     					(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
     					(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
     			)*-1 as total,
     						
     					((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
     					(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
     					(sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)*-1) as mc_bonus,
     					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as mc_total,
     						
     					(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as pl_total
     						
     					FROM tbl_virtuavegas_casino_history a
     					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     					LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
     					WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     					GROUP BY SUBSTRING(a.account_id,1,2)
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     					");
     		}elseif(strlen(trim($_GET['accountID']))==2){
     		$command = $connection->createCommand("
     				#SUB COMPANY WINLOSS
     				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     				FROM  (
     				SELECT
     				SUBSTRING(a.account_id,1,4) as account_id,
     				j.account_name,
     				a.currency_name as currency_name,
     				count(a.account_id) as bet_count,
     				avg(a.amount_wagers) as average_bet,
     				sum(a.amount_wagers) as total_stake,
     				sum(a.amount_wagers) as valid_bet,
     				sum(0) as tips,
     	
     				(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
     		)*-1 as total,
     	
     				((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
     				((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
     				0 as mc_bonus,
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as mc_total,#sc_total,
     	
     				(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
     		)*-1 as pl_total
     	
     				FROM tbl_virtuavegas_casino_history a
     				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
     				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     				GROUP BY SUBSTRING(a.account_id,1,4)
     				)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     				");
     		}elseif(strlen(trim($_GET['accountID']))==4){
     		$command = $connection->createCommand("
     		#SENIOR MASTER WINLOSS
     		SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     		FROM  (
     		SELECT
     		SUBSTRING(a.account_id,1,6) as account_id,
     			j.account_name,
     			a.currency_name as currency_name,
     			count(a.account_id) as bet_count,
     			avg(a.amount_wagers) as average_bet,
     			sum(a.amount_wagers) as total_stake,
     			sum(a.amount_wagers) as valid_bet,
     			sum(0) as tips,
     	
     			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
     			)*-1 as total,
     	
     			((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
     			((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
     			0 as mc_bonus,
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as mc_total,#sma_total,
     	
     			(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
     			)*-1 as pl_total
     	
     			FROM tbl_virtuavegas_casino_history a
     			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
     			WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY SUBSTRING(a.account_id,1,6)
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     			");
     		}elseif(strlen(trim($_GET['accountID']))==6){
     		$command = $connection->createCommand("
     		#MASTER WINLOSS
     		SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     		FROM  (
     		SELECT
     		SUBSTRING(a.account_id,1,8) as account_id,
     		j.account_name,
     		a.currency_name as currency_name,
     		count(a.account_id) as bet_count,
     		avg(a.amount_wagers) as average_bet,
     		sum(a.amount_wagers) as total_stake,
     		sum(a.amount_wagers) as valid_bet,
     		sum(0) as tips,
     	
     		(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
     		)*-1 as total,
     	
     				((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
     				((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
     				0 as mc_bonus,
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as mc_total,#ma_total,
     	
     				(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
     		)*-1 as pl_total
     	
     				FROM tbl_virtuavegas_casino_history a
     				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
     				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     				GROUP BY SUBSTRING(a.account_id,1,8)
     		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     				");
     		}elseif(strlen(trim($_GET['accountID']))==8){
     				$command = $connection->createCommand("
     				#MASTER WINLOSS
     				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     				FROM  (
     						SELECT
     						a.account_id as account_id,
     						b.account_name,
     						a.currency_name as currency_name,
     						count(a.account_id) as bet_count,
     				avg(a.amount_wagers) as average_bet,
     				sum(a.amount_wagers) as total_stake,
     				sum(a.amount_wagers) as valid_bet,
     				sum(0) as tips,
     					
     				sum((a.amount_settlements - a.amount_wagers)) as win_loss,
     				sum(a.commission) as commission,
     				sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)  as bonus,
     				((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) as total,
     						(SELECT balance FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
     						AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     	
     						(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
     						((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
     						(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     						((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
     						)*-1 as pl_total
     									
     								FROM tbl_virtuavegas_casino_history a
     								LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     								WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     								GROUP BY a.account_id
     						)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
     								");
     		}
     								$rows = $command->query();
     								return $rows;
     	}
     	
     	public function getExportVIRTUAWinLossDetails()
     	{
     		//additional parameter
     		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency_name!='TEST'";
     			}else{
     				$strQueryWhere="";
     			}
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency_name!='TEST'";
     			}else{
     				$strQueryWhere=" ";
     			}
     			$strQueryWhereForBalance=" ";
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     			$strQueryWhere=" AND a.currency_name='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance=" ";
     		}
     		//Agent ID
     		$strAccountIDWhere='';
     		$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
     	
     		if(trim($_GET['accountID'])==''){
     			$strAccountIDWhere='';
     		}else{
     			if($intAccountIDLength!=10){
     				$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
     			}else{
     				$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
     			}
     		}
     		$pl_total='';
     		if(trim($_GET['accountID'])==''){
     			$pl_total="(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==2){
     			$pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==4){
     			$pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==6){
     			$pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) +
     			(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==8){
     			$pl_total="(((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) +
     			((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))))
     			)*-1 as pl_total";
     		}
     		$connection = Yii::app()->db_cv999_fd_master;
     		$command = $connection->createCommand("
     				SELECT  @rownum := @rownum + 1 row, t.*
     				FROM  (
     				SELECT
     				substring(a.account_id,1,$intAccountIDLength) as account_id,
     				j.account_name,
     				a.currency_name as currency_name,
     				count(a.account_id) as bet_count,
     				avg(a.amount_wagers) as average_bet,
     				sum(a.amount_wagers) as total_stake,
     				sum(a.amount_wagers) as valid_bet,
     				sum(0) as tips,
     				sum((a.amount_settlements - a.amount_wagers)) as mem_win_loss,
     				sum(a.commission) as mem_comm,
     				sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)  as mem_bonus,
     				((sum((a.amount_settlements - a.amount_wagers)) + sum(a.commission) + sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END))) as mem_total,
     				(SELECT balance FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
     				AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     			  
     				(sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) as agt_win_loss,
     				((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     				((sum((a.amount_settlements - a.amount_wagers) * (a.agt_share/100))*-1) + ((sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission)))) as agt_total,
     	
     				((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
     				((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.amount_wagers * ((100 - a.agt_share)/100)) * (a.agt_commission/100))))) as ma_total,
     	
     				((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
     				((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.amount_wagers * ((100 - a.ma_share)/100)) * (a.ma_commission/100))))) as sma_total,
     	
     				((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
     				((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
     				(((sum((a.amount_settlements - a.amount_wagers) * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.amount_wagers * ((100 - a.sma_share)/100)) * (a.sma_commission/100))))) as sc_total,
     	
     				((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
     				(sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
     				(sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)*-1)  as mc_bonus,
     				(((sum((a.amount_settlements - a.amount_wagers) * ((100 - a.sc_share)/100)))*-1) + (sum((a.amount_wagers * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) - sum(CASE WHEN b.bonus IS NULL THEN 0 ELSE b.bonus END)) as mc_total,
     	
     				".$pl_total."
     	
     					
     				FROM tbl_virtuavegas_casino_history a
     				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
     				WHERE a.account_type='a' AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_date>='" . $_GET['dtFrom'] . "' AND a.bet_date<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     				GROUP BY substring(a.account_id,1,$intAccountIDLength)
     				)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc 
     				");
     				$rows = $command->query();
     				return $rows;
     	}
     	
     	public function getExportVIRTUAMemberWinLossDetails()
     	{
     		if($_GET['intGame']!==''){
     			$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
     		}else{
     			$addWhere="";
     		}
     		$connection = Yii::app()->db_cv999_fd_master;
     		$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetVIRTUAVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . "  ORDER BY t.bet_date DESC,t.id desc ");
     		$rows = $command->query();
     		return $rows;
     	}
     	 
     	
     	public function getExportSLOTWinLoss()
     	{
     		//additional parameter
     		if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency!='TEST'";
     			}else{
     				$strQueryWhere="";
     			}
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     			if($_GET['testCurrency']==1){
     				$strQueryWhere=" AND a.currency!='TEST'";
     			}else{
     				$strQueryWhere=" ";
     			}
     			$strQueryWhereForBalance=" ";
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance=" ";
     		}
     		 
     		$connection = Yii::app()->db_cv999_fd_master;
     		if(trim($_GET['accountID'])==''){
     			$command = $connection->createCommand("
     				SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     				FROM  (
     				SELECT
     				SUBSTRING(a.account_id,1,2) as account_id,
     				j.account_name,
     				a.currency as currency_name,
     				count(a.account_id) as bet_count,
     				avg(a.bet_amount) as average_bet,
     				sum(a.bet_amount) as total_stake,
     				sum(a.bet_amount) as valid_bet,
     				sum(0) as tips,
     	
     				(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     				((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     				(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     				(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
     				(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
     		)*-1 as total,
     	
     				((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
     				(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
     				((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1) as mc_bonus,
     				(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
     	
     				(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total
     	
     				FROM tbl_savan_slot_casino_history a
     				LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     				LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,2)
     				WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     				GROUP BY SUBSTRING(a.account_id,1,2)
     		)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
     				");
     		}elseif(strlen(trim($_GET['accountID']))==2){
     			$command = $connection->createCommand("
     			#SUB COMPANY WINLOSS
     			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     			FROM  (
     			SELECT
     			SUBSTRING(a.account_id,1,4) as account_id,
     			j.account_name,
     			a.currency as currency_name,
     			count(a.account_id) as bet_count,
     			avg(a.bet_amount) as average_bet,
     			sum(a.bet_amount) as total_stake,
     			sum(a.bet_amount) as valid_bet,
     			sum(0) as tips,
     
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
     	)*-1 as total,
     
     			((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as mc_win_loss,#sc_win_loss,
     			((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as mc_comm,#sc_comm,
     			((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as mc_bonus,
     			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as mc_total,#sc_total,
     
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
     	)*-1 as pl_total
     
     			FROM tbl_savan_slot_casino_history a
     			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,4)
     			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY SUBSTRING(a.account_id,1,4)
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
     			");
     		}elseif(strlen(trim($_GET['accountID']))==4){
     			$command = $connection->createCommand("
     	#SENIOR MASTER WINLOSS
     	SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     	FROM  (
     	SELECT
     	SUBSTRING(a.account_id,1,6) as account_id,
     	j.account_name,
     	a.currency as currency_name,
     	count(a.account_id) as bet_count,
     	avg(a.bet_amount) as average_bet,
     	sum(a.bet_amount) as total_stake,
     	sum(a.bet_amount) as valid_bet,
     	sum(0) as tips,
    
     	(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     	((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     	(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
     	)*-1 as total,
   
     	((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as mc_win_loss,#sma_win_loss,
     	((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as mc_comm,#sma_comm,
     	((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as mc_bonus,
     	(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as mc_total,#sma_total,
    
     	(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     	((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     	(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     	(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
     	)*-1 as pl_total
    
     	FROM tbl_savan_slot_casino_history a
     	LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     	LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,6)
     	WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     	GROUP BY SUBSTRING(a.account_id,1,6)
     	)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
     	");
     		}elseif(strlen(trim($_GET['accountID']))==6){
     			$command = $connection->createCommand("
     			#MASTER WINLOSS
     			SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     			FROM  (
     			SELECT
     			SUBSTRING(a.account_id,1,8) as account_id,
     			j.account_name,
     			a.currency as currency_name,
     			count(a.account_id) as bet_count,
     			avg(a.bet_amount) as average_bet,
     			sum(a.bet_amount) as total_stake,
     			sum(a.bet_amount) as valid_bet,
     			sum(0) as tips,
     
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
     			)*-1 as total,
     
     			((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as mc_win_loss,#ma_win_loss,
     			((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as mc_comm,#ma_comm,
     			((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as mc_bonus,
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as mc_total,#ma_total,
     
     			(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
     			)*-1 as pl_total
     
     			FROM tbl_savan_slot_casino_history a
     			LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     			LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,8)
     			WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND j.agent_parent_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     			GROUP BY SUBSTRING(a.account_id,1,8)
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
     			");
     		}elseif(strlen(trim($_GET['accountID']))==8){
     			$command = $connection->createCommand("
     	#MASTER WINLOSS
     	SELECT  @rownum := @rownum + 1 row, t.*, cu.exchange_rate
     	FROM  (
     	SELECT
     	a.account_id as account_id,
     	b.account_name,
     	a.currency as currency_name,
     	count(a.account_id) as bet_count,
     	avg(a.bet_amount) as average_bet,
     	sum(a.bet_amount) as total_stake,
     	sum(a.bet_amount) as valid_bet,
     	sum(0) as tips,
    
     	sum(a.win_loss) as win_loss,
     	sum(a.commission) as commission,
     	sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as bonus,
     	((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as total,
     	(SELECT balance FROM vwGetSLOTVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
     	AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     					 
     					(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
     					((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     					(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
     					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
     					 
     					(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     					((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
     			)*-1 as pl_total
     					 
     					FROM tbl_savan_slot_casino_history a
     					LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     					WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' AND b.agent_account_id='".$_GET['accountID']."' AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     					GROUP BY a.account_id
     			)t left join tbl_currency cu on t.currency_name=cu.currency_name, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
     			");
     	}
     			$rows = $command->query();
     					return $rows;
     	}
     	
     	public function getExportSLOTWinLossDetails()
     	{
     	//additional parameter
     	if($_GET['strCurrency']=='' and $_GET['intGame']==''){
     		if($_GET['testCurrency']==1){
     		$strQueryWhere=" AND a.currency!='TEST'";
     	}else{
     	$strQueryWhere="";
     		}
     		$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']!='' and $_GET['intGame']==''){
     			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance='';
     		}else if($_GET['strCurrency']=='' and $_GET['intGame']!=''){
     			if($_GET['testCurrency']==1){
     	     					$strQueryWhere=" AND a.currency!='TEST'";
     			}else{
     			$strQueryWhere=" ";
     	}
     	$strQueryWhereForBalance=" ";
     			}else if($_GET['strCurrency']!='' and $_GET['intGame']!=''){
     			$strQueryWhere=" AND a.currency='" . $_GET['strCurrency'] . "'";
     			$strQueryWhereForBalance=" ";
     	}
     	//Agent ID
     			$strAccountIDWhere='';
     					$intAccountIDLength=strlen(trim($_GET['accountID']))+2;
     	
     					if(trim($_GET['accountID'])==''){
     							$strAccountIDWhere='';
     			}else{
     			if($intAccountIDLength!=10){
     			$strAccountIDWhere=" AND j.agent_parent_id='".$_GET['accountID']."' ";
     	}else{
     					$strAccountIDWhere=" AND b.agent_account_id='".$_GET['accountID']."' ";
     			}
     			}
     					$pl_total='';
     						if(trim($_GET['accountID'])==''){
     							$pl_total="(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as pl_total";
     	}elseif(strlen(trim($_GET['accountID']))==2){
     		$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==4){
     			$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) +
     			(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==6){
     			$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) +
     			(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1))
     			)*-1 as pl_total";
     		}elseif(strlen(trim($_GET['accountID']))==8){
     			$pl_total="(((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) +
     			((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1))
     			)*-1 as pl_total";
     		}
     		$connection = Yii::app()->db_cv999_fd_master;
     		$command = $connection->createCommand("
     				SELECT  @rownum := @rownum + 1 row, t.*
     				FROM  (
     				SELECT
     				substring(a.account_id,1,$intAccountIDLength) as account_id,
     	     					j.account_name,
     	     					a.currency as currency_name,
     	     					count(a.account_id) as bet_count,
     	     					avg(a.bet_amount) as average_bet,
     	     					sum(a.bet_amount) as total_stake,
     	     					sum(a.bet_amount) as valid_bet,
     	     					sum(0) as tips,
     	     							sum(a.win_loss) as mem_win_loss,
     	     							sum(a.commission) as mem_comm,
     	     							sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END)  as mem_bonus,
     	     							((sum(a.win_loss) + sum(a.commission) + sum(CASE WHEN a.jackpot_money IS NULL THEN 0 ELSE a.jackpot_money END))) as mem_total,
     	     							(SELECT balance FROM vwGetSLOTVEGASMemberSlotWinLossDetails WHERE account_id=b.account_id
     	     							AND bet_date>='" . $_GET['dtFrom'] . "' AND bet_date<='". $_GET['dtTo'] ."' $strQueryWhereForBalance ORDER BY bet_date desc LIMIT 1) as current_balance,
     	
     	     							(sum(a.win_loss * (a.agt_share/100))*-1) as agt_win_loss,
     	     							((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) as agt_comm,
     	     							(sum(a.jackpot_money * (a.agt_share/100))*-1) as agt_bonus,
     	     							((sum(a.win_loss * (a.agt_share/100))*-1) + ((sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100))) - (sum(a.commission))) + (sum(a.jackpot_money * (a.agt_share/100))*-1)) as agt_total,
     	
     	     							((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) as ma_win_loss,
     	     							((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) as ma_comm,
     	     							((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1) as ma_bonus,
     	     							(((sum(a.win_loss * ((a.ma_share-a.agt_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100))) - (sum((a.bet_amount * ((100 - a.agt_share)/100)) * (a.agt_commission/100)))) + ((sum(a.jackpot_money * ((a.ma_share-a.agt_share)/100)))*-1)) as ma_total,
     	
     	     							((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) as sma_win_loss,
     	     							((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) as sma_comm,
     	     							((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1) as sma_bonus,
     	     							(((sum(a.win_loss * ((a.sma_share-a.ma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100))) - (sum((a.bet_amount * ((100 - a.ma_share)/100)) * (a.ma_commission/100)))) + ((sum(a.jackpot_money * ((a.sma_share-a.ma_share)/100)))*-1)) as sma_total,
     	
     	     							((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) as sc_win_loss,
     	     							((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) as sc_comm,
     	     							((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1) as sc_bonus,
     	     							(((sum(a.win_loss * ((a.sc_share-a.sma_share)/100)))*-1) + ((sum((a.bet_amount * ((100 - a.sc_share)/100)) * (a.sc_commission/100))) - (sum((a.bet_amount * ((100 - a.sma_share)/100)) * (a.sma_commission/100)))) + ((sum(a.jackpot_money * ((a.sc_share-a.sma_share)/100)))*-1)) as sc_total,
     	
     	     							((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) as mc_win_loss,
     	     							(sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) as mc_comm,
     	     							((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)  as mc_bonus,
     	     							(((sum(a.win_loss * ((100 - a.sc_share)/100)))*-1) + (sum((a.bet_amount * ((100 - a.sc_share)/100)) * (-a.sc_commission/100))) + ((sum(a.jackpot_money * ((100 - a.sc_share)/100)))*-1)) as mc_total,
     	
     	     							".$pl_total."
     	
     	
     	     							FROM tbl_savan_slot_casino_history a
     	     							LEFT JOIN tbl_agent_player b ON a.account_id=b.account_id
     	     							LEFT JOIN tbl_agent j ON j.account_id=SUBSTRING(a.account_id,1,$intAccountIDLength)
     	     							WHERE a.account_type=2 AND a.account_id like '". $_GET['specificID'] ."%' ". $strAccountIDWhere ." AND (a.bet_time>='" . $_GET['dtFrom'] . "' AND a.bet_time<='" . $_GET['dtTo'] . "') " . $strQueryWhere . "
     	     							GROUP BY substring(a.account_id,1,$intAccountIDLength)
     	     							)t, (SELECT @rownum := 0) r order by t.currency_name asc,t.account_id asc
     	     									");
     						$rows = $command->query();
     	     						return $rows;
     	}
     	
     	public function getExportSLOTMemberWinLossDetails()
     	{
     	if($_GET['intGame']!==''){
     	$addWhere="AND t.game_id='" . $_GET['intGame'] . "'";
     	}else{
     	$addWhere="";
     	}
     	$connection = Yii::app()->db_cv999_fd_master;
     	$command = $connection->createCommand("SELECT @rownum := @rownum + 1 row, t.* FROM vwGetSLOTVEGASMemberSlotWinLossDetails t,(SELECT @rownum := 0) r WHERE t.bet_date>='" . $_GET['dtFrom'] . "' AND t.bet_date<='" . $_GET['dtTo'] . "' AND  t.account_id='" . $_GET['strAccountID'] . "' " . $addWhere . "  ORDER BY t.bet_date DESC,t.id desc ");
     		$rows = $command->query();
     		return $rows;
     	}
}