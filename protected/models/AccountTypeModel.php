<?php
class AccountTypeModel
{
	const ALL_TYPES = 'all';

	/**
	 * @return array
	 */
	public static function getAllTypes()
	{
// 		$accoutTypes = array(
// 			'all' => 'All',
// 			'c'	=>	'Cash Player',
// 			'a' =>	'Agent Player',
// 			//'agt' => 'Agent',
// 			//'ma' => 'Master Agent',
// 			//'sma' => 'Senior Master Agent',
// 			//'sc' => 'Subcompany',
// 		);

// 		return $accoutTypes;
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spGetAccountTypes()");

		return $command->queryAll();
	}

	/**
	 * 
	 * @param string $code
	 * @return array
	 */
	public static function getTypeById($code)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spGetAccountTypeByCode('" . $code ."')");

		return $command->queryRow();
	}

}

