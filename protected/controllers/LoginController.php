<?php

/**
 * @todo Login 
 * @author Kimny MOUK
 * @since 2012-04-03
 */

class LoginController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
						'foreColor'=>0xFF5050,
						'minLength'=>4,
						'maxLength'=>4,
						'width'=>100,
						'height'=>35,
						'padding'=>0,
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page'=>array(
						'class'=>'CViewAction',
				),
		);
	}
        
    /**
	 * @todo Localization for login page
	 * @author Kimny MOUK
	 * @return
	 * @param
	 */
    public function actionIndex()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
           echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
            {
            	//$backendUser = BackendUser::model()->find('account_id=:account_id', array(':account_id'=>$model->username));
            	$backendUser = BackendUser::getBackendUsers($model->username,md5($model->password), $_SERVER['REMOTE_ADDR']);
				
            	
            	Yii::app()->session['id'] = $backendUser[0]['@po_id'];
            	Yii::app()->session['account_id'] = ($backendUser[0]['@po_account_type']!='agent_alias') ? strtoupper($model->username) : substr(strtoupper($model->username), 0, strlen($model->username)-3) ; // added by leokarl (agent_alias)
            	Yii::app()->session['password'] = md5($model->password);
            	#Yii::app()->session['account_type'] = $backendUser['account_type'];
            	#Yii::app()->session['level'] = $backendUser['level'];
            	#Yii::app()->session['level_name'] = $backendUser['level_name'];
            	#Yii::app()->session['max_deposit'] = $backendUser['max_deposit'];
            	#Yii::app()->session['max_withdraw'] = $backendUser['max_withdraw'];
            	Yii::app()->session['agent_alias_name'] = strtoupper($model->username); // added by leokarl (agent_alias)
            	Yii::app()->session['account_type'] = $backendUser[0]['@po_account_type'];
            	Yii::app()->session['level'] = $backendUser[0]['@po_level'];
            	Yii::app()->session['level_name'] = $backendUser[0]['@po_level_name'];
            	Yii::app()->session['max_deposit'] = $backendUser[0]['@po_max_deposit'];
            	Yii::app()->session['max_withdraw'] = $backendUser[0]['@po_max_withdraw'];
            	
            	$lang = isset($_GET['lang'])? $_GET['lang'] : "en";
            	Yii::app()->session['chooselanaguage'] = $lang;
            	
         		/* create user session in db */
            	$userSessionModel = new UserLoginSession;
            	$userSessionModel->account_id = $model->username;
            	$userSessionModel->login_ip_address = $_SERVER['REMOTE_ADDR'];
            	$userSessionModel->session = date("Y-m-d H:i:s"); 
            	$userSessionModel->onlive = 1;
            	$userSessionModel->browser = $_SERVER['HTTP_USER_AGENT'];
            	$userSessionModel->save();
            	
            	
            	/* create user session hisotry in db */
            	$userSessionHistoryModel = new UserLoginSessionHistory();
            	$userSessionHistoryModel->account_id = $model->username;
            	$userSessionHistoryModel->login_ip_address = $_SERVER['REMOTE_ADDR'];
            	$userSessionHistoryModel->session = date("Y-m-d H:i:s");
            	$userSessionHistoryModel->browser = $_SERVER['HTTP_USER_AGENT'];
            	$userSessionHistoryModel->save();
				
//             	if (User::getUserType() === Constants::ROLE_AGENT)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
//             	else if (User::getRoleType() === Constants::ROLE_ROOT)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
//             	else if (User::getRoleType() === Constants::ROLE_MANAGER)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
//             	else if (User::getRoleType() === Constants::ROLE_GAMING)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
//             	else if (User::getRoleType() === Constants::ROLE_ADMINISTRATOR)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=CashPlayer/CashPlayerList");
//             	else if (User::getRoleType() === Constants::ROLE_FRAUD)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
//             	else if (User::getRoleType() === Constants::ROLE_FINANCE)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
//             	else if (User::getRoleType() === Constants::ROLE_MARKETING)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
//             	else if (User::getRoleType() === Constants::ROLE_PROVIDER)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=CashPlayer/CashPlayerWinLoss");
//             	else if (User::getRoleType() === Constants::ROLE_CAGE)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=CashPlayer/CashPlayerList");
//             	else if (User::getRoleType() === Constants::ROLE_CS)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=CashPlayer/CashPlayerList");
//             	else if (User::getRoleType() === Constants::ROLE_TRANSLATOR)
//             		$this->redirect(Yii::app()->request->baseUrl ."/index.php/translate/edit/admin");

            	$connection = Yii::app()->db_cv999_fd_master;
            	$command = $connection->createCommand("Select count(account_id) as cnt from tbl_locked_user_auth where account_id='".$model->username."'");
            	$rows = $command->queryRow();

            	if ($rows['cnt']!=0){
	            	$deleteLockedUser=TableLockedUser::model()->findByPk($model->username);
	            	$deleteLockedUser->delete();
	            	yii::app()->session['no_attempt']=0;
            	}
            	//Set account id  in the coockie
            	yii::app()->request->cookies['login_account']=new CHttpCookie('login_account', CV999Utility::encryption($model->username));
            	//yii::app()->request->cookies['logout_account']=new CHttpCookie('logout_account', CV999Utility::decryption(Constants::KEY_LOGIN,Yii::app()->request->cookies['login_account']->value));
            	if (User::getUserType() === Constants::ROLE_AGENT){
            		$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=Agent/AgentInformation");
            	}else{
            		$this->redirect(Yii::app()->request->baseUrl . User::getRoleHomePageUrl());
            	}
            		
            }
        }
        // display the login form
       $this->layout='index';
       $this->render('index',array('model'=>$model));
        
    }
    public function actionLogin1()
    {
    	$this->layout='login1';
    	$this->render('login1');
    }
    
    public function actionLogout()
    {
    	
    	$user_session = new UserLoginSession;
    	$criteria=new CDbCriteria;
    	$criteria->condition='account_id=:account_id';
    	$criteria->params=array(':account_id'=>Yii::app()->session['account_id']);
    	$user_session->deleteAll($criteria);
    	
    	//UserLoginSession::model()->updateAll(array('onlive'=>0), 'account_id=hany');
    	//$criteria->addInCondition( "account_id" , Yii::app()->session['account_id']);
    	//$user_session->updateAll(array('onlive' => 0), $criteria);
    	//Yii::app()->cache->delete(Yii::app()->session['account_id']);
    	//Yii::app()->cache->delete(Yii::app()->session['account_id']);
    	$account_id='';
    	if (yii::app()->session['account_id']==null || yii::app()->session['account_id']==''){
    		if (isset(Yii::app()->request->cookies['login_account'])){
    			$account_id = htmlentities(strtoupper(CV999Utility::decryption(Yii::app()->request->cookies['login_account']->value)));
    		}else{
    			$account_id='';
    		}
    	}else{
    		$account_id =strtoupper(Yii::app()->session['account_id']);
    	}
    	
    	//Logout User Session==========================
    	$browser=CV999Utility::getBrowser();
    	$postUserSessionLog=new TableUserSessionAuditLog;
    	$postUserSessionLog->account_id=$account_id;
    	$postUserSessionLog->session_type=0;
    	$postUserSessionLog->session_date=date('Y-m-d H:i:s');
    	$postUserSessionLog->session_id='';
    	$postUserSessionLog->login_ip_address=$_SERVER['REMOTE_ADDR'];
    	$postUserSessionLog->browser=$browser['name'].' '.$browser['version'];
    	$postUserSessionLog->event=0;
    	$postUserSessionLog->fail_reason='';
    	$postUserSessionLog->save();
    	
    	//==========================================
    	unset(Yii::app()->request->cookies['login_account']);
    	
    	Yii::app()->session->destroy();
    	Yii::app()->user->logout();
    	// display the login form
    	$model=new LoginForm;
    	$this->layout='index';
    	$this->render('index',array('model'=>$model));
    }
	
    public function actionAddLockUser()
    {

    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("Select count(account_id) as cnt,locked_duration,extended_locked_duration,locked  from tbl_locked_user_auth where account_id='".$_POST['accountId']."'");
    	$rows = $command->queryRow();
    	
    	$duration=0;
    	if ($_POST['accountId'] !=''){
	    	if (isset($_POST['lockedDuration'])){
		    	$cnt=$rows['cnt'];
		    	if ($cnt==0){
			    	$postLockedUser=new TableLockedUser;
			    	$postLockedUser->account_id = $_POST['accountId'];
			    	$postLockedUser->locked_duration = $_POST['lockedDuration'];
			    	$postLockedUser->extended_locked_duration = $_POST['extendedDuration'];
			    	$postLockedUser->locked = 1;
			 		$postLockedUser->save();
			 		
			 		$duration=$_POST['lockedDuration'];
		
		    	}else{
		    		if (isset($_POST['lockedDuration'])){
		    			TableLockedUser::model()->updateAll(array('locked_duration'=>$rows['locked_duration'] + 0,'locked'=>1), 'account_id = "'.$_POST['accountId'].'"');
		    			$duration=$rows['locked_duration'] + 0;
		    		}else{
			    		TableLockedUser::model()->updateAll(array('locked_duration'=>$rows['locked_duration'] + $rows['extended_locked_duration'],'locked'=>1), 'account_id = "'.$_POST['accountId'].'"');
			    		$duration=$rows['locked_duration'] + $rows['extended_locked_duration'];
		    		}
		    	}
	    	}
    	
	    	//Login User Session==========================
	    	if ($_POST['result']==1){
	    		$failReason='Incorrect username/password.';
	    	}else if ($_POST['result']==2){
	    		$failReason='User account already closed.';
	    	}else if ($_POST['result']==3){
	    		$failReason='User account was black listed.';
	    	}else if ($_POST['result']==4){
	    		$failReason='User account was Locked.';
	    	}else{
	    		$failReason='';
	    	}
	    	$browser=CV999Utility::getBrowser();
	    	$postUserSessionLog=new TableUserSessionAuditLog;
	    	$postUserSessionLog->account_id=strtoupper($_POST['accountId']);
	    	$postUserSessionLog->session_type=1;
	    	$postUserSessionLog->session_date=date('Y-m-d H:i:s');
	    	$postUserSessionLog->session_id='';
	    	$postUserSessionLog->login_ip_address=$_SERVER['REMOTE_ADDR'];
	    	$postUserSessionLog->browser=$browser['name'].' '.$browser['version'];//CV999Utility::getBrowser();
	    	$postUserSessionLog->event=$_POST['event'];
	    	$postUserSessionLog->fail_reason=$failReason;
	    	$postUserSessionLog->save();
    	}
    	//==========================================
    	echo $duration;

    }
    public function actionGetDuration()
    {
    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("Select count(account_id) as cnt,locked_duration,extended_locked_duration,locked  from tbl_locked_user_auth where account_id='".$_POST['accountId']."'");
    	$rows = $command->queryRow();
    	
    	$duration= $rows['locked_duration'];
    	echo $duration;
    }
    
    public function actionRemoveLockUser()
    {
    	$deleteLockedUser=TableLockedUser::model()->findByPk($_POST['accountId']);
        $deleteLockedUser->delete();
        yii::app()->session->clear();
    
    }

    public function actionCheckLocked()
    {

    	$backendUser = BackendUser::getBackendUsers($_POST['accountId'], md5($_POST['password']), $_SERVER['REMOTE_ADDR']);

    	if ($backendUser[0]['@po_authentication'] == 1) {
    	
    		if ($backendUser[0]['@po_status'] != 1) {
    			$error=2; //Error Code for Inactive user =2
    		} else {
    			if ($backendUser[0]['@po_is_allowed'] == 0) {
    				$error=3; //Error Code for black list user =3
    			} else {
    				$error=0;
    			}
    		}
    	}else{
    		$error=1; //Error Code for Wrong password =1
    	}


    	$connection = Yii::app()->db_cv999_fd_master;
    	$command = $connection->createCommand("Select locked  from tbl_locked_user_auth where account_id='".$_POST['accountId']."'");
    	$rows = $command->queryRow();
    	if ($rows['locked']==''){
    		$locked=0;
    	}else{
    		$locked=$rows['locked'];
    	}
    	echo $locked."#".$error ;

    }

}


?>
