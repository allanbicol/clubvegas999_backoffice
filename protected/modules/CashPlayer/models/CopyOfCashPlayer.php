<?php

/**
 * This is the model class for table "tbl_cash_player".
 *
 * The followings are the available columns in table 'tbl_cash_player':
 * @property integer $id
 * @property string $account_id
 * @property string $account_name
 * @property string $first_name
 * @property string $last_name
 * @property string $player_password
 * @property string $email
 * @property string $phone_number
 * @property string $country
 * @property string $last_date_play
 * @property integer $currency_id
 * @property string $digit_security_num
 * @property string $sex
 * @property string $dob
 * @property string $address
 * @property string $zip_code
 * @property string $city
 * @property integer $status_id
 * @property string $opening_acc_date
 * @property string $balance
 * @property string $last_update_balance
 * @property integer $casino_id
 * @property integer $last_game_play_id
 */
class CashPlayer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CashPlayer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player';
	}
	
	public function primaryKey()
	{
		return 'id';
		// For composite primary key, return an array like the following
		// return array('pk1', 'pk2');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, account_name, first_name, last_name, player_password, email, phone_number, country, currency_id', 'required'),
			array('currency_id, status_id, casino_id, last_game_play_id', 'numerical', 'integerOnly'=>true),
			array('account_id, account_name, first_name, last_name, player_password, email, phone_number, country, digit_security_num', 'length', 'max'=>45),
			array('sex', 'length', 'max'=>1),
			array('address', 'length', 'max'=>100),
			array('zip_code, balance', 'length', 'max'=>10),
			array('city', 'length', 'max'=>20),
			array('last_date_play, dob, opening_acc_date, last_update_balance', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, account_id, account_name, first_name, last_name, player_password, email, phone_number, country, last_date_play, currency_id, digit_security_num, sex, dob, address, zip_code, city, status_id, opening_acc_date, balance, last_update_balance, casino_id, last_game_play_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'account_name' => 'Account Name',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'player_password' => 'Player Password',
			'email' => 'Email',
			'phone_number' => 'Phone Number',
			'country' => 'Country',
			'last_date_play' => 'Last Date Play',
			'currency_id' => 'Currency',
			'digit_security_num' => 'Digit Security Num',
			'sex' => 'Sex',
			'dob' => 'Dob',
			'address' => 'Address',
			'zip_code' => 'Zip Code',
			'city' => 'City',
			'status_id' => 'Status',
			'opening_acc_date' => 'Opening Acc Date',
			'balance' => 'Balance',
			'last_update_balance' => 'Last Update Balance',
			'casino_id' => 'Casino',
			'last_game_play_id' => 'Last Game Play',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id,true);
		$criteria->compare('account_name',$this->account_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('player_password',$this->player_password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('last_date_play',$this->last_date_play,true);
		$criteria->compare('currency_id',$this->currency_id);
		$criteria->compare('digit_security_num',$this->digit_security_num,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('zip_code',$this->zip_code,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('opening_acc_date',$this->opening_acc_date,true);
		$criteria->compare('balance',$this->balance,true);
		$criteria->compare('last_update_balance',$this->last_update_balance,true);
		$criteria->compare('casino_id',$this->casino_id);
		$criteria->compare('last_game_play_id',$this->last_game_play_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}