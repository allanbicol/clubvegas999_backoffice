<!--@TODO: VIEWS FOR CASH PLAYER TRANSACTION HISTORY FOR BONUS
	@AUTHOR: Allan
	@SINCE: 04232012
-->
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

<script type="text/javascript">
document.getElementById('cashplayerHeader').className="start active";
document.getElementById('mnu_cash_player').className="active";
$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Cash Player Bonus Transaction History</a></li>");

function showFundpage() 
{
	var accountID = document.getElementById('txtAccountID').value;
	window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryFund&Account_ID=" + accountID;
}
</script>

<script type="text/javascript">
function tableTransHistoryBonus(status,accountId,dateFrom,dateTo) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'listlog'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
	$(document).ready(function() {
	$("#listlog").trigger("reloadGrid");
	jQuery("#listlog").jqGrid({ 
		url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerTransHistoryBonus/CashPlayerTransHistoryBonus&stat='+status+'&id='+accountId+'&datefrom='+dateFrom+'&dateto='+dateTo, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    width:'auto',
	    colNames: ['Transaction Number', 'Type','Currency','Amount','Bonus Type','Balance Before','Balance After','Submit Time','Operator Name'],
	    colModel: [
	            {name: 'transNumber', index: 'transNumber', width: 120, search:true,title:false},
	     	    {name: 'transTypeName', index: 'transTypeName', width: 90,title:false},
	     	    {name: 'currencyName', index: 'currencyName', width: 100,title:false},
	     	    {name: 'Amount', index: 'Amount', width: 100, align: "right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	     	   	{name: 'deposit_via', index: 'deposit_via', width: 100,title:false},
	     	   	{name: 'balance_before', index: 'balance_before', width: 100, align: "right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	     	  	{name: 'balance_after', index: 'balance_after', width: 100, align: "right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
	     	    {name: 'submitTime', index: 'submitTime', width: 125,title:false},
	     	    {name: 'cashierID', index: 'cashierID', width: 150,title:false},
	    ],
	    loadtext:"",
		loadComplete: function() {
			var grid=jQuery("#listlog");
	        var i=0; 
		    for(i=0;i<=grid.getGridParam("reccount");i++)
		    {
		    	var myrow = grid.jqGrid('getRowData', i);
		    	grid.jqGrid('setCell',i,"balance_before","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"balance_after","",{background:'#cfe5fa'});
			}   
		    $("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
	    },
	    rownumbers:true,
	    rowNum: 20,	
	    rowList: [20, 50, 100,200,500,99999],
	    pager: '#pager2',
	    sortname: 'transNumber',
	    sortorder: 'ASC',
	    caption: '<?php echo '<label style="color:#D84A38">' .strtoupper($_GET["Account_ID"]).'</label>'; ?> BONUS - &nbsp;&nbsp;&nbsp;'+ document.getElementById('datefrom').value + '&nbsp;' + document.getElementById('cbHourfrom').value + ':00:00 to '+ document.getElementById('dateto').value + '&nbsp;' + document.getElementById('cbHourto').value +':59:59',
	    viewrecords: true
	});
	
	$('#listlog').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:false });
	
	});
}
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "dd-mm-yy"
	    });
	});
	function btnclick(btnname)
	{
		if (btnname=="Load")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('qry_result').innerHTML='';
			var accountId= document.getElementById('txtAccountID').value;
			tableTransHistoryBonus("All",accountId,datefrom,dateto);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
	
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var statustype=document.getElementById('statusType').value;
			var accountId=document.getElementById('txtAccountID').value;
			document.getElementById('qry_result').innerHTML='';
			tableTransHistoryBonus(statustype,accountId,datefrom,dateto);
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			
			var datefrom=(theyear+"/"+themonth+"/"+theyday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theyday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('dateto').value=(theyday+"-"+themonth+"-"+theyear);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var statustype=document.getElementById('statusType').value;
			var accountId=document.getElementById('txtAccountID').value;
			document.getElementById('qry_result').innerHTML='';
			tableTransHistoryBonus(statustype,accountId,datefrom,dateto);
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[0];
			var yearF = dateFrom[2];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[0];
			var yearT = dateTo[2];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var statustype=document.getElementById('statusType').value;
			var accountId=document.getElementById('txtAccountID').value;
			document.getElementById('qry_result').innerHTML='';
			tableTransHistoryBonus(statustype,accountId,dateSubmitFrom,dateSubmitTo);
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>

</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area">
	<div class="header">Cash Member Transaction History</div>
	<form action="">
			<div style="padding:5px 0px 5px 95px; height: 35px; border-bottom:1px solid #333333;">
		<input class="btn red" type="button" value="FUND" onclick="showFundpage()">
		<input class="btn red disabled" type="button" value="BONUS">
		<input style=" width: 30px; display:none;" type="text" value="<?php echo $_GET["Account_ID"]; ?>"  id="txtAccountID">
		</div>
		<table  width="100%">
		<tr><td>METHOD:</td><td>
			<select id="statusType">
  				<option value="All">All</option>
  				<option value="1">Add</option>
  				<option value="2">Remove</option>
			</select>

		</td></tr>
		<tr><td>FROM:</td><td><input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px;width:50px; background-color: transparent" disabled></td></tr>
		<tr><td>TO</td><td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px;width:50px; background-color: transparent" disabled></td></tr>
		</table>
		<div align="center" style="padding-top: 10px;">
		<input onclick="javacript:btnclick(this.value);" type="button" value="Submit" class="btn red">
		<input onclick="javascript:btnclick(this.value);" type="button" value="Yesterday" class="btn red">
		<input onclick="javascript:btnclick(this.value);" type="button" value="Today" class="btn red">
		<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red"></div>
	</form>	
	<script type="text/javascript">
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
	document.getElementById("dateto").value=(day + "-" + month + "-" + year);
	</script>
	
</div>

	<div id="qry_result"></div>
	<div id="pager2"></div>
</body>
</html>