<?php
class CashPlayerWinLossDetailSavanV2Controller extends MyController
{
	public function actionCashPlayerWinLossDetailSavanV2()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLossDetailSavanV2;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerWinLossDetailSavan();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerWinLossDetailSavan($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("id","table_shoe_game","account_id","account_name","currency","game_type","stake_amount","valid_stake","result","win_loss","winloss1","commission","amount_tips","total_win_loss","balance_after","end_time","casino_name","counter","IP");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionGetRecordAllPagesV2()
	{
		$playersRecordAllPages=new CashPlayerWinLossDetailSavanV2();
		$playersRecordAllPages->getPlayerWinLossDetailSavanAllPages();
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readWinLoss'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionSavanWinLossDetailExcelV2(){
		$file="CashPlayerSavan_WinLossDetail".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		try{
			echo $buffer;
		}catch(Exception $e){
	
		}
	}
	
	public function actionSavanWinLossExcelV2(){
		$file="CashPlayerSavan_WinLossDetail".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
		
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Savanvegas Winloss details</label> to excel file</b>\n'.$params[0].'\n<b>Method</b> = '.$params[1].'\n<b>Date From</b> = '.$params[2].'\n<b>Date To</b> = '.$params[3];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionSavanVegasResultDetailsV2(){
		$bl=new CashPlayerWinLossDetailSavanV2();
		echo $bl->getSavanVegasResultDetails();
	}
	public function actionSavanVegasWinLossResultDetailsV2()
	{
		$bl=new CashPlayerWinLossDetailSavanV2();
		$bl->getSavanVegasWinLossResultDetails();
	}
	public function actionSavanVegasWinLossResultCardsV2()
	{
		$bl=new CashPlayerWinLossDetailSavanV2();
		$bl->getSavanVegasWinLossResultCards();
	}
	public function actionExportCashPlayerWinLossDetailSavanV2()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLossDetailSavanV2;
		$result=$cpHfund->getCountPlayerWinLossDetailSavan();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records = $cpHfund->getExportPlayerWinLossDetailSavan();
		$fieldNames = array("table_shoe_game","account_id","account_name","currency","game_type","stake_amount","valid_stake","result","win_loss","winloss1","commission","total_win_loss","balance_after","end_time","casino_name","counter","IP");
		$headerNames=array("Table | Shoe | Game","Betting Time","Account Id","Account Name","Currency","Game Type","Bet Amount","Valid Bet","Winloss","Commission","Total Winloss","Balance After","IP Address");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
			
	}
}