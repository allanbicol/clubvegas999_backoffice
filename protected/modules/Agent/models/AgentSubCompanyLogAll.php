<?php
/**
 * @todo agentSubCompanyLog Model
 * @copyright CE
 * @author Allan
 * @since 2012-06-29
 */
class AgentSubCompanyLogAll
{
	public function getAgentSubCompanyLogAll($orderField, $sortType, $startIndex , $limit)
	{	
		$connection = Yii::app()->db_cv999_fd_master;
		$level= Yii::app()->session['level_name'];
		if ($_GET['op']!='All'){
			if ($_GET['op']==0){ // 0 is operated
				if ($_GET['lt']==0){
					$command = $connection->createCommand("SELECT * from tbl_log where operated_by='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."' ORDER BY operation_time desc  LIMIT $startIndex , $limit");
				}else{
					$command = $connection->createCommand("SELECT * from tbl_log where log_type_id='".$_GET['lt']."' and operated_by='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."' ORDER BY operation_time desc  LIMIT $startIndex , $limit");
				}
				
			}else{
				if ($_GET['lt']==0){ // lt is log type
					$command = $connection->createCommand("SELECT * from tbl_log where  operated='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."' ORDER BY operation_time desc LIMIT $startIndex , $limit");
				}else{
					$command = $connection->createCommand("SELECT * from tbl_log where log_type_id='".$_GET['lt']."' and operated='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."' ORDER BY operation_time desc LIMIT $startIndex , $limit");
				}
			}	
		}else{ // 1 is operating
			if (User::getUserType()!='agent'){
				if ($_GET['user']=='SC'){
					$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=2 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
				}elseif($_GET['user']=='SMA'){
					$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=4 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
				}elseif($_GET['user']=='MA'){
					$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=6 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
				}elseif($_GET['user']=='AGT'){
					$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=8 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
				}elseif($_GET['user']=='ALL'){
					$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
				}
			}else{
				
				if ($level=='Sub Company'){
					if($_GET['user']=='SMA'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=4 OR LENGTH(operated_by)=4) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}elseif($_GET['user']=='MA'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=6 OR LENGTH(operated_by)=6) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}elseif($_GET['user']=='AGT'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=8 OR LENGTH(operated_by)=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}elseif($_GET['user']=='ALL'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated) >=4 OR LENGTH(operated_by) >=4) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}
					
				}
				
				else if($level=='Senior Master Agent'){
					if($_GET['user']=='MA'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=6 OR LENGTH(operated_by)=6) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}elseif($_GET['user']=='AGT'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=8 OR LENGTH(operated_by)=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}elseif($_GET['user']=='ALL'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)>=6 OR LENGTH(operated_by)>=6) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}
				}
				
				else if($level=='Master Agent'){
					if($_GET['user']=='AGT'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=8 OR LENGTH(operated_by)=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}elseif($_GET['user']=='ALL'){
						$command = $connection->createCommand("SELECT * FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)>=8 OR LENGTH(operated_by)>=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'  ORDER BY operation_time DESC LIMIT $startIndex , $limit");
					}
				}

			}		
		}
		$rows = $command->query();
		return $rows;
	}

	public function getCountAgentSubCompanyLogAll()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$level= Yii::app()->session['level_name'];
		
		if ($_GET['op']!='All'){
			if ($_GET['op']==0){
				if ($_GET['lt']==0){
					$command = $connection->createCommand("SELECT COUNT(0) from tbl_log where operated_by='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."' ");
				}else{
					$command = $connection->createCommand("SELECT COUNT(0) from tbl_log where log_type_id='".$_GET['lt']."' and operated_by='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."' ");
				}
				
			}else{
				if ($_GET['lt']==0){
					$command = $connection->createCommand("SELECT COUNT(0) from tbl_log where  operated='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."'");
				}else{
					$command = $connection->createCommand("SELECT COUNT(0) from tbl_log where log_type_id='".$_GET['lt']."' and operated='".yii::app()->session['account_id']."' and operation_time between '".$_GET['dtfrom']."' and '".$_GET['dtto']."'");
				}
			}	
		}
		else{
			
			if (User::getUserType()!='agent'){
				if($_GET['user']=='SC'){
					$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=2 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
				}
				if($_GET['user']=='SMA'){
					$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=4 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
				}elseif($_GET['user']=='MA'){
					$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=6 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
				}elseif($_GET['user']=='AGT'){
					$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND LENGTH(operated)=8 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
				}elseif($_GET['user']=='ALL'){
					$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
				}
					
			}
			else {
					if ($level=='Sub Company'){
						if($_GET['user']=='SMA'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=4 OR LENGTH(operated_by)=4) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}elseif($_GET['user']=='MA'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=6 OR LENGTH(operated_by)=6) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}elseif($_GET['user']=='AGT'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=8 OR LENGTH(operated_by)=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}elseif($_GET['user']=='ALL'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated) >=4 OR LENGTH(operated_by) >=4) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}				
					}
		
					elseif($level=='Senior Master Agent'){
						if($_GET['user']=='MA'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=6 OR LENGTH(operated_by)=6) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}elseif($_GET['user']=='AGT'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=8 OR LENGTH(operated_by)=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}elseif($_GET['user']=='ALL'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)>=6 OR LENGTH(operated_by)>=6) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}
					}
		
					elseif($level=='Master Agent'){
						if($_GET['user']=='AGT'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)=8 OR LENGTH(operated_by)=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}elseif($_GET['user']=='ALL'){
							$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_log WHERE log_type_id=6 AND (operated_by LIKE '".yii::app()->session['account_id']."%' OR operated LIKE '".yii::app()->session['account_id']."%') AND (LENGTH(operated)>=8 OR LENGTH(operated_by)>=8) AND operation_time BETWEEN '".$_GET['dtfrom']."' AND '".$_GET['dtto']."'");
						}
					}
		
			}
			
		}
		$rows = $command->query();
		return $rows;
	}
	
}