<?php

class AgentRevenueSharingAdjustmentController extends MyController
{
	/**
	 * @todo revenue sharing adjustment
	 * @author leokarl
	 * @since 2013-03-16
	 *
	 * @note Baccarat|Dragon Tiger|European Roulette|American Roulette|Blackjack|Slots
	 * @note update also when upper agent change this
	 */
	public function actionUpdate(){
		if(Yii::app()->user->checkAccess('agent.writeRevenueSharingAdjustment'))
		{
			$info = TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id' => Yii::app()->session['account_id']),));
			$casino_level = TableCasinoAccessLevelSetting::model()->find(array('select'=>'*','condition'=>'level_id=:level_id','params'=>array(':level_id' => $info['casino_access_level']),));
			$model = new AgentRevenueSharingAdjustment();
			$limit_from = $model->getLimitFrom();
			$limit_to = $model->getLimitTo();

			// save date limit validation
			// 		if(!$model->saveLimitValidation()){
			// 			exit('save_date_limit');
			// 		}
				// Sharing
			list($vig_share_baccarat, $htv_share_baccarat, $sv_share_baccarat, $vv_share_baccarat) = explode("#",$info['share_baccarat']);
			list($vig_share_roulette, $htv_share_roulette, $sv_share_roulette, $vv_share_roulette) = explode("#",$info['share_roulette']);
			list($vig_share_dragontiger, $htv_share_dragontiger, $sv_share_dragontiger, $vv_share_dragontiger) = explode("#",$info['share_dragon_tiger']);
			list($vig_share_blackjack, $htv_share_blackjack, $sv_share_blackjack, $vv_share_blackjack) = explode("#",$info['share_blackjack']);
			list($vig_share_americanroulette, $htv_share_americanroulette, $sv_share_americanroulette, $vv_share_americanroulette) = explode("#",$info['share_american_roulette']);
			list($vig_share_slots, $htv_share_slots, $sv_share_slots, $vv_share_slots) = explode("#",$info['share_slots']);

			// Commission
			list($vig_comm_baccarat, $htv_comm_baccarat, $sv_comm_baccarat, $vv_comm_baccarat) = explode("#",$info['commission_baccarat']);
			list($vig_comm_roulette, $htv_comm_roulette, $sv_comm_roulette, $vv_comm_roulette) = explode("#",$info['commission_roulette']);
			list($vig_comm_dragontiger, $htv_comm_dragontiger, $sv_comm_dragontiger, $vv_comm_dragontiger) = explode("#",$info['commission_dragon_tiger']);
			list($vig_comm_blackjack, $htv_comm_blackjack, $sv_comm_blackjack, $vv_comm_blackjack) = explode("#",$info['commission_blackjack']);
			list($vig_comm_americanroulette, $htv_comm_americanroulette, $sv_comm_americanroulette, $vv_comm_americanroulette) = explode("#",$info['commission_american_roulette']);
			list($vig_comm_slots, $htv_comm_slots, $sv_comm_slots, $vv_comm_slots) = explode("#",$info['commission_slots']);

			/** @todo is_numeric validation */

			if($casino_level['vig']==1){
				if(!is_numeric($_POST['txtVIGShareBaccarat'])) exit('invalid_vig_share_baccarat');
				if(!is_numeric($_POST['txtVIGCommBaccarat'])) exit('invalid_vig_comm_baccarat');
				if(!is_numeric($_POST['txtVIGShareRoulette'])) exit('invalid_vig_share_roulette');
				if(!is_numeric($_POST['txtVIGCommRoulette'])) exit('invalid_vig_comm_roulette');
				if(!is_numeric($_POST['txtVIGShareBlackjack'])) exit('invalid_vig_share_blackjack');
				if(!is_numeric($_POST['txtVIGCommBlackjack'])) exit('invalid_vig_comm_blackjack');
				if(!is_numeric($_POST['txtVIGShareAmericanRoulette'])) exit('invalid_vig_share_american_roulette');
				if(!is_numeric($_POST['txtVIGCommAmericanRoulette'])) exit('invalid_vig_comm_american_roulette');
					
				if ($vig_share_baccarat!=intval($_POST['txtVIGShareBaccarat'])){
					if(intval($_POST['txtVIGShareBaccarat'])<10) exit('lessthan10_vig_share_baccarat');
				}
				if ($vig_share_roulette !=intval($_POST['txtVIGShareRoulette'])){
					if(intval($_POST['txtVIGShareRoulette'])<10) exit('lessthan10_vig_share_roulette');
				}
				if ($vig_share_blackjack != intval($_POST['txtVIGShareBlackjack'])){
					if(intval($_POST['txtVIGShareBlackjack'])<10) exit('lessthan10_vig_share_blackjack');
				}
				if ($vig_share_americanroulette != intval($_POST['txtVIGShareAmericanRoulette'])){
					if(intval($_POST['txtVIGShareAmericanRoulette'])<10) exit('lessthan10_vig_share_american_roulette');
				}
			}
			if($casino_level['htv']==1){
				if(!is_numeric($_POST['txtHTVShareBaccarat'])) exit('invalid_htv_share_baccarat');
				if(!is_numeric($_POST['txtHTVCommBaccarat'])) exit('invalid_htv_comm_baccarat');
				if(!is_numeric($_POST['txtHTVShareRoulette'])) exit('invalid_htv_share_roulette');
				if(!is_numeric($_POST['txtHTVCommRoulette'])) exit('invalid_htv_comm_roulette');
				if(!is_numeric($_POST['txtHTVShareDragonTiger'])) exit('invalid_htv_share_dragon_tiger');
				if(!is_numeric($_POST['txtHTVCommDragonTiger'])) exit('invalid_htv_comm_dragon_tiger');
				if(!is_numeric($_POST['txtHTVShareSlots'])) exit('invalid_htv_share_slots');
				if(!is_numeric($_POST['txtHTVCommSlots'])) exit('invalid_htv_comm_slots');
					
				if ($htv_share_baccarat != intval($_POST['txtHTVShareBaccarat'])){
					if(intval($_POST['txtHTVShareBaccarat'])<10) exit('lessthan10_htv_share_baccarat');
				}
				if ($htv_comm_roulette != intval($_POST['txtHTVShareRoulette'])){
					if(intval($_POST['txtHTVShareRoulette'])<10) exit('lessthan10_htv_share_roulette');
				}
				if ($htv_comm_dragontiger != intval($_POST['txtHTVShareDragonTiger'])){
					if(intval($_POST['txtHTVShareDragonTiger'])<10) exit('lessthan10_htv_share_dragon_tiger');
				}
				if ($htv_share_slots != intval($_POST['txtHTVShareSlots'])){
					if(intval($_POST['txtHTVShareSlots'])<10) exit('lessthan10_htv_share_slots');
				}
			}
			if($casino_level['sv']==1){
				if(!is_numeric($_POST['txtSVShareBaccarat'])) exit('invalid_sv_share_baccarat');
				if(!is_numeric($_POST['txtSVCommBaccarat'])) exit('invalid_sv_comm_baccarat');
				if(!is_numeric($_POST['txtSVShareRoulette'])) exit('invalid_sv_share_roulette');
				if(!is_numeric($_POST['txtSVCommRoulette'])) exit('invalid_sv_comm_roulette');
				if(!is_numeric($_POST['txtSVShareDragonTiger'])) exit('invalid_sv_share_dragon_tiger');
				if(!is_numeric($_POST['txtSVCommDragonTiger'])) exit('invalid_sv_comm_dragon_tiger');
				if(!is_numeric($_POST['txtSVShareSlots'])) exit('invalid_sv_share_slots');
				if(!is_numeric($_POST['txtSVCommSlots'])) exit('invalid_sv_comm_slots');
					
				if ($sv_share_baccarat != intval($_POST['txtSVShareBaccarat'])){
					if(intval($_POST['txtSVShareBaccarat'])<10) exit('lessthan10_sv_share_baccarat');
				}
				if ($sv_share_roulette != intval($_POST['txtSVShareRoulette'])){
					if(intval($_POST['txtSVShareRoulette'])<10) exit('lessthan10_sv_share_roulette');
				}
				if ($sv_share_dragontiger != intval($_POST['txtSVShareDragonTiger'])){
					if(intval($_POST['txtSVShareDragonTiger'])<10) exit('lessthan10_sv_share_dragon_tiger');
				}
				if ($sv_share_slots != intval($_POST['txtSVShareSlots'])){
					if(intval($_POST['txtSVShareSlots'])<10) exit('lessthan10_sv_share_slots');
				}
			}
			if($casino_level['vv']==1){
				if(!is_numeric($_POST['txtVVShareSlots'])) exit('invalid_vv_share_slots');
				if(!is_numeric($_POST['txtVVCommSlots'])) exit('invalid_vv_comm_slots');
					
				if ($vv_share_slots != intval($_POST['txtVVShareSlots'])){
					if(intval($_POST['txtVVShareSlots'])<10) exit('lessthan10_vv_share_slots');
				}
			}

			/** @todo value vs limits validation */

			if($casino_level['vig']==1){
				if(!$model->sValVSlimitValid($_POST['txtVIGShareBaccarat'], $limit_from['vig_share_baccarat_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_share_baccarat, $limit_to['vig_share_baccarat_to']))) exit('vig_share_baccarat_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVIGCommBaccarat'], $limit_from['vig_comm_baccarat_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_comm_baccarat, $limit_to['vig_comm_baccarat_to']))) exit('vig_comm_baccarat_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVIGShareRoulette'], $limit_from['vig_share_roulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_share_roulette, $limit_to['vig_share_e_roulette_to']))) exit('vig_share_roulette_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVIGCommRoulette'], $limit_from['vig_comm_roulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_comm_roulette, $limit_to['vig_comm_e_roulette_to']))) exit('vig_comm_roulette_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVIGShareBlackjack'], $limit_from['vig_share_blackjack_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_share_blackjack, $limit_to['vig_share_blackjack_to']))) exit('vig_share_blackjack_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVIGCommBlackjack'], $limit_from['vig_comm_blackjack_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_comm_blackjack, $limit_to['vig_comm_blackjack_to']))) exit('vig_comm_blackjack_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVIGShareAmericanRoulette'], $limit_from['vig_share_americanroulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_share_americanroulette, $limit_to['vig_share_a_roulette_to']))) exit('vig_share_american_roulette_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVIGCommAmericanRoulette'], $limit_from['vig_comm_americanroulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vig_comm_americanroulette, $limit_to['vig_comm_a_roulette_to']))) exit('vig_comm_american_roulette_beyond_limit');
			}
			if($casino_level['htv']==1){
				if(!$model->sValVSlimitValid($_POST['txtHTVShareBaccarat'], $limit_from['htv_share_baccarat_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_share_baccarat, $limit_to['htv_share_baccarat_to']))) exit('htv_share_baccarat_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtHTVCommBaccarat'], $limit_from['htv_comm_baccarat_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_comm_baccarat, $limit_to['htv_comm_baccarat_to']))) exit('htv_comm_baccarat_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtHTVShareRoulette'], $limit_from['htv_share_roulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_share_roulette, $limit_to['htv_share_e_roulette_to']))) exit('htv_share_roulette_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtHTVCommRoulette'], $limit_from['htv_comm_roulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_comm_roulette, $limit_to['htv_comm_e_roulette_to']))) exit('htv_comm_roulette_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtHTVShareDragonTiger'], $limit_from['htv_share_dragontiger_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_share_dragontiger, $limit_to['htv_share_dragon_tiger_to']))) exit('htv_share_dragontiger_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtHTVCommDragonTiger'], $limit_from['htv_comm_dragontiger_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_comm_dragontiger, $limit_to['htv_comm_dragon_tiger_to']))) exit('htv_comm_dragontiger_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtHTVShareSlots'], $limit_from['htv_share_slots_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_share_slots, $limit_to['htv_share_slots_to']))) exit('htv_share_slots_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtHTVCommSlots'], $limit_from['htv_comm_slots_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $htv_comm_slots, $limit_to['htv_comm_slots_to']))) exit('htv_comm_slots_beyond_limit');
			}
			if($casino_level['sv']==1){
				if(!$model->sValVSlimitValid($_POST['txtSVShareBaccarat'], $limit_from['sv_share_baccarat_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_share_baccarat, $limit_to['sv_share_baccarat_to']))) exit('sv_share_baccarat_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtSVCommBaccarat'], $limit_from['sv_comm_baccarat_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_comm_baccarat, $limit_to['sv_comm_baccarat_to']))) exit('sv_comm_baccarat_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtSVShareRoulette'], $limit_from['sv_share_roulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_share_roulette, $limit_to['sv_share_e_roulette_to']))) exit('sv_share_roulette_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtSVCommRoulette'], $limit_from['sv_comm_roulette_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_comm_roulette, $limit_to['sv_comm_e_roulette_to']))) exit('sv_comm_roulette_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtSVShareDragonTiger'], $limit_from['sv_share_dragontiger_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_share_dragontiger, $limit_to['sv_share_dragon_tiger_to']))) exit('sv_share_dragontiger_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtSVCommDragonTiger'], $limit_from['sv_comm_dragontiger_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_comm_dragontiger, $limit_to['sv_comm_dragon_tiger_to']))) exit('sv_comm_dragontiger_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtSVShareSlots'], $limit_from['sv_share_slots_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_share_slots, $limit_to['sv_share_slots_to']))) exit('sv_share_slots_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtSVCommSlots'], $limit_from['sv_comm_slots_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $sv_comm_slots, $limit_to['sv_comm_slots_to']))) exit('sv_comm_slots_beyond_limit');
			}
			if($casino_level['vv']==1){
				if(!$model->sValVSlimitValid($_POST['txtVVShareSlots'], $limit_from['vv_share_slots_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vv_share_slots, $limit_to['vv_share_slots_to']))) exit('vv_share_slots_beyond_limit');
				if(!$model->sValVSlimitValid($_POST['txtVVCommSlots'], $limit_from['vv_comm_slots_from'], $model->getNewLimitTo($info['revenue_sharing_limit_to'], $vv_comm_slots, $limit_to['vv_comm_slots_to']))) exit('vv_comm_slots_beyond_limit');
			}


			/** @todo set value for sharing/commission */

			// vig
			$txtVIGShareBaccarat = ($casino_level['vig']==1) ? $_POST['txtVIGShareBaccarat'] : 0;
			$txtVIGCommBaccarat = ($casino_level['vig']==1) ? $_POST['txtVIGCommBaccarat'] : 0;
			$txtVIGShareRoulette = ($casino_level['vig']==1) ? $_POST['txtVIGShareRoulette'] : 0;
			$txtVIGCommRoulette = ($casino_level['vig']==1) ? $_POST['txtVIGCommRoulette'] : 0;
			$txtVIGShareBlackjack = ($casino_level['vig']==1) ? $_POST['txtVIGShareBlackjack'] : 0;
			$txtVIGCommBlackjack = ($casino_level['vig']==1) ? $_POST['txtVIGCommBlackjack'] : 0;
			$txtVIGShareAmericanRoulette = ($casino_level['vig']==1) ? $_POST['txtVIGShareAmericanRoulette'] : 0;
			$txtVIGCommAmericanRoulette =  ($casino_level['vig']==1) ? $_POST['txtVIGCommAmericanRoulette'] : 0;
				
			// htv
			$txtHTVShareBaccarat = ($casino_level['htv']==1) ? $_POST['txtHTVShareBaccarat'] : 0;
			$txtHTVCommBaccarat = ($casino_level['htv']==1) ? $_POST['txtHTVCommBaccarat'] : 0;
			$txtHTVShareRoulette = ($casino_level['htv']==1) ? $_POST['txtHTVShareRoulette'] : 0;
			$txtHTVCommRoulette = ($casino_level['htv']==1) ? $_POST['txtHTVCommRoulette'] : 0;
			$txtHTVShareDragonTiger = ($casino_level['htv']==1) ? $_POST['txtHTVShareDragonTiger'] : 0;
			$txtHTVCommDragonTiger = ($casino_level['htv']==1) ? $_POST['txtHTVCommDragonTiger'] : 0;
			$txtHTVShareSlots = ($casino_level['htv']==1) ? $_POST['txtHTVShareSlots'] : 0;
			$txtHTVCommSlots = ($casino_level['htv']==1) ? $_POST['txtHTVCommSlots'] : 0;

			// sv
			$txtSVShareBaccarat = ($casino_level['sv']==1) ? $_POST['txtSVShareBaccarat'] : 0;
			$txtSVCommBaccarat = ($casino_level['sv']==1) ? $_POST['txtSVCommBaccarat'] : 0;
			$txtSVShareRoulette = ($casino_level['sv']==1) ? $_POST['txtSVShareRoulette'] : 0;
			$txtSVCommRoulette = ($casino_level['sv']==1) ? $_POST['txtSVCommRoulette'] : 0;
			$txtSVShareDragonTiger = ($casino_level['sv']==1) ? $_POST['txtSVShareDragonTiger'] : 0;
			$txtSVCommDragonTiger = ($casino_level['sv']==1) ? $_POST['txtSVCommDragonTiger'] : 0;
			$txtSVShareSlots = ($casino_level['sv']==1) ? $_POST['txtSVShareSlots'] : 0;
			$txtSVCommSlots = ($casino_level['sv']==1) ? $_POST['txtSVCommSlots'] : 0;

			// vv
			$txtVVShareSlots = ($casino_level['vv']==1) ? $_POST['txtVVShareSlots'] : 0;
			$txtVVCommSlots =  ($casino_level['vv']==1) ? $_POST['txtVVCommSlots'] : 0;


			/** @todo save commission and sharing */

			$connection = Yii::app()->db_cv999_fd_master;
			$transaction = $connection->beginTransaction(); // begin transaction

			$post = TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
			$post->share_baccarat = $model->generateSharingCommissionString($info['share_baccarat'], $txtVIGShareBaccarat."#".$txtHTVShareBaccarat."#".$txtSVShareBaccarat."#0.00", $casino_level);
			$post->share_roulette = $model->generateSharingCommissionString($info['share_roulette'], $txtVIGShareRoulette."#".$txtHTVShareRoulette."#".$txtSVShareRoulette."#0.00", $casino_level);
			$post->share_dragon_tiger = $model->generateSharingCommissionString($info['share_dragon_tiger'], "0.00#".$txtHTVShareDragonTiger."#".$txtSVShareDragonTiger."#0.00", $casino_level);
			$post->share_blackjack = $model->generateSharingCommissionString($info['share_blackjack'], $txtVIGShareBlackjack."#0.00#0.00#0.00", $casino_level);
			$post->share_american_roulette = $model->generateSharingCommissionString($info['share_american_roulette'], $txtVIGShareAmericanRoulette."#0.00#0.00#0.00", $casino_level);
			$post->share_slots = $model->generateSharingCommissionString($info['share_slots'], "0.00#".$txtHTVShareSlots."#".$txtSVShareSlots."#".$txtVVShareSlots, $casino_level);
				
			$post->commission_baccarat = $model->generateSharingCommissionString($info['commission_baccarat'], $txtVIGCommBaccarat."#".$txtHTVCommBaccarat."#".$txtSVCommBaccarat."#0.00", $casino_level);
			$post->commission_roulette = $model->generateSharingCommissionString($info['commission_roulette'], $txtVIGCommRoulette."#".$txtHTVCommRoulette."#".$txtSVCommRoulette."#0.00", $casino_level);
			$post->commission_dragon_tiger = $model->generateSharingCommissionString($info['commission_dragon_tiger'], "0.00#".$txtHTVCommDragonTiger."#".$txtSVCommDragonTiger."#0.00", $casino_level);
			$post->commission_blackjack = $model->generateSharingCommissionString($info['commission_blackjack'], $txtVIGCommBlackjack."#0.00#0.00#0.00", $casino_level);
			$post->commission_american_roulette = $model->generateSharingCommissionString($info['commission_american_roulette'], $txtVIGCommAmericanRoulette."#0.00#0.00#0.00", $casino_level);
			$post->commission_slots = $model->generateSharingCommissionString($info['commission_slots'], "0.00#".$txtHTVCommSlots."#".$txtSVCommSlots."#".$txtVVCommSlots, $casino_level);

			// Baccarat|Dragon Tiger|European Roulette|American Roulette|Blackjack|Slots
			if(trim($info['revenue_sharing_limit_to']) == ''){
				$post->revenue_sharing_limit_to = $info['share_baccarat'] .'^'. $info['commission_baccarat']
				.'|'. $info['share_dragon_tiger'] .'^'. $info['commission_dragon_tiger']
				.'|'. $info['share_roulette'] .'^'. $info['commission_roulette']
				.'|'. $info['share_american_roulette'] .'^'. $info['commission_american_roulette']
				.'|'. $info['share_blackjack'] .'^'. $info['commission_blackjack']
				.'|'. $info['share_slots'] .'^'. $info['commission_slots'];
			}
			$post->save();
				
			/** @todo write log */
				
			// commission log
			if($model->isSharingCommissionChange($info['commission_baccarat'], $txtVIGCommBaccarat."#".$txtHTVCommBaccarat."#".$txtSVCommBaccarat."#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['commission_roulette'], $txtVIGCommRoulette."#".$txtHTVCommRoulette."#".$txtSVCommRoulette."#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['commission_dragon_tiger'], "0.00#".$txtHTVCommDragonTiger."#".$txtSVCommDragonTiger."#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['commission_blackjack'], $txtVIGCommBlackjack."#0.00#0.00#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['commission_american_roulette'], $txtVIGCommAmericanRoulette."#0.00#0.00#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['commission_slots'], "0.00#".$txtHTVCommSlots."#".$txtSVCommSlots."#".$txtVVCommSlots, $casino_level)){

	 		$postLog=new TableLog();
	 		$postLog->operated_by = Yii::app()->session['account_id'];
	 		$postLog->operated_by_level = Yii::app()->session['level_name'];
	 		$postLog->operated = Yii::app()->session['account_id'];
	 		$postLog->operated_level = Yii::app()->session['level_name'];
	 		$postLog->operation_time = date('Y-m-d H:i:s');
	 		$postLog->log_type_id = 7;
	 		$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> CHANGE his own COMMISSION from ' .
	 	 		$model->getSharingCommissionLogDetails($info['commission_baccarat'] . ':' . $txtVIGCommBaccarat."#".$txtHTVCommBaccarat."#".$txtSVCommBaccarat."#0.00",
	 	 				$info['commission_roulette'] . ':' . $txtVIGCommRoulette."#".$txtHTVCommRoulette."#".$txtSVCommRoulette."#0.00",
	 	 				$info['commission_dragon_tiger'] . ':' . "0.00#".$txtHTVCommDragonTiger."#".$txtSVCommDragonTiger."#0.00",
	 	 				$info['commission_blackjack'] . ':' . $txtVIGCommBlackjack."#0.00#0.00#0.00",
	 	 				$info['commission_american_roulette'] . ':' . $txtVIGCommAmericanRoulette."#0.00#0.00#0.00",
	 	 				$info['commission_slots'] . ':' . "0.00#".$txtHTVCommSlots."#".$txtSVCommSlots."#".$txtVVCommSlots,
	 	 				$casino_level) .'</b>';
	 		$postLog->save();
			}
				
			// sharing log
			if($model->isSharingCommissionChange($info['share_baccarat'], $txtVIGShareBaccarat."#".$txtHTVShareBaccarat."#".$txtSVShareBaccarat."#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['share_roulette'], $txtVIGShareRoulette."#".$txtHTVShareRoulette."#".$txtSVShareRoulette."#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['share_dragon_tiger'], "0.00#".$txtHTVShareDragonTiger."#".$txtSVShareDragonTiger."#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['share_blackjack'], $txtVIGShareBlackjack."#0.00#0.00#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['share_american_roulette'], $txtVIGShareAmericanRoulette."#0.00#0.00#0.00", $casino_level) ||
			$model->isSharingCommissionChange($info['share_slots'], "0.00#".$txtHTVShareSlots."#".$txtSVShareSlots."#".$txtVVShareSlots, $casino_level)){
					
				$postLog=new TableLog();
				$postLog->operated_by = Yii::app()->session['account_id'];
				$postLog->operated_by_level = Yii::app()->session['level_name'];
				$postLog->operated = Yii::app()->session['account_id'];
				$postLog->operated_level = Yii::app()->session['level_name'];
				$postLog->operation_time = date('Y-m-d H:i:s');
					
				$postLog->log_type_id = 6;
				$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> CHANGE his own SHARING from ' .
						$model->getSharingCommissionLogDetails($info['share_baccarat'] . ':' . $txtVIGShareBaccarat."#".$txtHTVShareBaccarat."#".$txtSVShareBaccarat."#0.00",
								$info['share_roulette'] . ':' . $txtVIGShareRoulette."#".$txtHTVShareRoulette."#".$txtSVShareRoulette."#0.00",
								$info['share_dragon_tiger'] . ':' . "0.00#".$txtHTVShareDragonTiger."#".$txtSVShareDragonTiger."#0.00",
								$info['share_blackjack'] . ':' . $txtVIGShareBlackjack."#0.00#0.00#0.00",
								$info['share_american_roulette'] . ':' . $txtVIGShareAmericanRoulette."#0.00#0.00#0.00",
								$info['share_slots'] . ':' . "0.00#".$txtHTVShareSlots."#".$txtSVShareSlots."#".$txtVVShareSlots,
								$casino_level) .'</b>';
				$postLog->save();
			}
				
			$transaction->commit(); // end transaction

		}
	}

		public function actionIndex(){
			if(!isset(Yii::app()->session['account_id'])){
				$this->redirect(Yii::app()->request->baseUrl ."/index.php");
			}

			if(Yii::app()->user->checkAccess('agent.writeRevenueSharingAdjustment')){
				$info = TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id','params'=>array(':account_id' => Yii::app()->session['account_id']),));
				$casino_level = TableCasinoAccessLevelSetting::model()->find(array('select'=>'*','condition'=>'level_id=:level_id','params'=>array(':level_id' => $info['casino_access_level']),));
				$model = new AgentRevenueSharingAdjustment();
					
				$this->render('index',array('info'=>$info,'casino_level'=>$casino_level,
						'limit_from'=>$model->getLimitFrom(),
						'limit_to'=>$model->getLimitTo()
				));
			}else{
				$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
			}
		}
	}