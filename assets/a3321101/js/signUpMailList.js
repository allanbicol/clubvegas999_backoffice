var cRowNo=0;
$(document).ready(function() {
	var grid=jQuery("#listMail");
	grid.jqGrid({ 
		url: currencyListURL, 
		datatype: 'json',
	    mtype: 'GET',
	    height: 'auto',
	    colNames: ['id','Description','Operation'],
	    colModel: [
	      {name: 'id', index: 'id', width: 100,hidden:true},
	      {name: 'email_desc', index: 'email_desc', width: 200},
	      {name: 'id', index: 'id', width: 170,formatter:update},
	      
	    ],
//	    beforeSelectRow: function(rowid,e){
//	    	cRowNo=rowid;
//	    	var myrow = grid.jqGrid('getRowData', rowid);
//	    	var intval=myrow.exchange_rate;
//	    	var intval1=intval.replace(",","");
//	    	var intval2=intval1.replace(",","");
//	    	var intval3=intval2.replace(",","");
//	    	var intval4=intval3.replace(",","");
//	    	var intval5=intval4.replace(",","");
//	    	var intval6=intval5.replace(",","");
//	    	var intval7=intval6.replace(",","");
//	    	if (intval7.length>9){
//		    	var trueval=intval7.split(".");
//	    		document.getElementById('txtUExchangeRate').value = trueval[0];
//		    }else{
//		    	document.getElementById('txtUExchangeRate').value = intval7.replace(",","");
//			}
//	    	
//		},
		loadComplete:function(){
			$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
		},
	    rowNum: 20,
	    rownumbers: true,
	    rowList: [5,10, 20, 30],
	    pager: '#pagerMail',
	    sortname: 'email_desc',
	    sortorder: 'ASC',
	    hidegrid: false,
	    height: 'auto',
	    caption: 'Email Type List',
	    viewrecords: true
	});
	$('#listMail').jqGrid('navGrid', '#pagerMail', {edit: false, add: false, del:false,search: false, refresh: false});
	});

function update(cellvalue, options, rowObject){
	// initialize
	var desc =  (rowObject[1] === undefined) ? rowObject.email_desc : rowObject[1];
	
	return '<a id="updateEmail" href="#" onclick="SignUpEdit(\'' + desc + '\')">Update</a>';
	
}

function SignUpEdit(desc){
	
	jQuery.ajax({
		url: setSignupSettingURL,
		type: 'POST',
		data: {'desc':desc},
		success: function(msg) {
			
			var content = msg.split("||");
//			var editorEN = CKEDITOR.instances.editorEng;
//			var editorTra = CKEDITOR.instances.editorCN;
//			var editorSim = CKEDITOR.instances.editorHK;
//			var editorVN = CKEDITOR.instances.editorViet;
//			var editorTH = CKEDITOR.instances.editorThai;
			
			document.getElementById( 'signupName' ).innerHTML= '<div id="signupName"><h3>'+content[0]+'</h3></div>';
//			editorEN.setData(content[1]);
//			editorTra.setData(content[2]);
//			editorSim.setData(content[3]);
//			editorVN.setData(content[4]);
//			editorTH.setData(content[5]);
			window.location.reload();
    	}
	});
}