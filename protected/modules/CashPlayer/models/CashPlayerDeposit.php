<?php
/**
 * @todo CashPlayerDeposit Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class CashPlayerDeposit
{
	/**
	 * @todo save deposit
	 * @author leokarl
	 * @since 2012-12-24
	 */
	public function saveCashPlayerDeposit($account_id, $amount, $trans_item)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		/*
		 	IN pi_account_id CHAR(45),
		    IN pi_cashier_id CHAR(20),
		    IN pi_deposit_amount FLOAT(15,2),
		    IN pi_status_id INT(11),
		    IN pi_trans_number VARCHAR(11),
		    IN pi_trans_item INT(11) 
		 */
		$command = $connection->createCommand("CALL spUpdateCashPlayerDeposit('" . $account_id . "',
				'" . Yii::app()->session['account_id'] . "'," . $amount . ",6,
				'" . TransactionNumber::generateTransactionNumber("MD") . "', ". $trans_item .")");
		$command->execute();
		
		$val=1;
		$unlock=TableCashPlayer::model()->updateAll(array(
				'kick_off'=>$val),
				'account_id="'.$account_id.'"');
		
		$log = new TableLog;
		$log->operated_by = Yii::app()->session['account_id'];
		$log->operated_by_level = Yii::app()->session['level_name'];
		$log->operated = $account_id;
		$log->operated_level = 'Player';
		$log->operation_time = date('Y-m-d H:i:s');
		$log->log_type_id = 3;
		$log->log_details = '<b>'. Yii::app()->session['level_name'] .' <label style=\"color:#7A5C00\">'. Yii::app()->session['account_id'] .
			'</label> Deposit:<label style=\"color:red\">'.number_format($amount,2,'.',',').'</label></b>';
		$log->save();
		
		echo 'deposit_done';
		
		//header('Location: '.  Yii::app()->request->baseUrl . '/index.php?r=CashPlayer/CashPlayerList');
	}
	
	/**
	 * @todo get currency name by account id
	 * @author leokarl
	 * @since 2012-12-27
	 * @param string $account_id
	 */
	public function getCurrencyNameByAccountId($account_id){
		$model = TableCashPlayer::model()->find(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);
		return $this->getCurrencyNameByCurrencyId($model['currency_id']);
	}
	
	/**
	 * @todo get currency name using currency id
	 * @author leokarl
	 * @since 2012-12-27
	 * @param int $currency_id
	 */
	public function getCurrencyNameByCurrencyId($currency_id){
		$model = TableCurrency::model()->find(array('condition'=>'id=:currency_id',
				'params'=>array(':currency_id' => $currency_id),)
		);
		return $model['currency_name'];
	}
	
	/**
	 * @todo disallow special characters
	 * @author leokarl
	 * @since 2012-12-08
	 * @param string $mystring
	 * @return boolean
	 */
	public function isSpecialCharPresent($mystring){
		if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
				&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
				&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false && strpos($mystring, '_') === false
				&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '`') === false && strpos($mystring, '-') === false
				&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
				&& strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
				&& strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){
	
			return FALSE; // special character not present
		}else{
			return TRUE; // special character present
		}
	}
	
	/**
	 * @todo is cashplayer account_id exist
	 * @author leokarl
	 * @since 2012-12-21
	 * @param string $account_id
	 * @return boolean
	 */
	public function isAccountIdExist($account_id){
		$count = TableCashPlayer::model()->count(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);
	
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo check deposit type value
	 * @author leokarl
	 * @since 2012-12-24
	 * @param int $deposit_type
	 * @return boolean
	 */
	public function isDepositTypeValid($deposit_type){
		return ($deposit_type == 0 || $deposit_type == 1) ? TRUE : FALSE;
	}
	
	/**
	 * @todo check transaction item
	 * @author leokarl
	 * @since 2012-12-24
	 * @param int $trans_item
	 * @param int $deposit_type
	 */
	public function isTransactionItemValid($trans_item,$deposit_type){
		$count = TableTransactionItem::model()->count(array('condition'=>'id=:item_id and transaction_bank=:trans_bank and item_type=2',
				'params'=>array(':item_id' => $trans_item, ':trans_bank' => $deposit_type),)
		);
		
		return ($count > 0) ? TRUE : FALSE;
	}
	
	public function makeCashPlayerDeposit()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if (isset($_POST['task'])==''){
			exit;
		}else{
			if ($_POST['task']=='playerDeposit')
			{
				//$rd=User::model()->find(array('select'=>'level_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
				$command = $connection->createCommand("SELECT b.currency_name from tbl_cash_player a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE a.account_id='" . $_POST['AccountID'] . "'");
				$rd1=$command->queryRow();
				//echo $rd['level_id'] . ";" . $rd1['currency_name'];
				echo $rd1['currency_name'];
			}
			if ($_POST['task']=='playerDepositConfirm')
			{
				$command = $connection->createCommand("CALL spCheckOnliveStatusGamesByPlayer('" . $_POST["AccountID"] . "',@po_result);");
				$command->execute();
				$command = $connection->createCommand("SELECT @po_result as poResult");
				$rd=$command->queryRow();
				
				$redis= new RedisManager();	
				if ($redis->isExistingOnLobbyByPlayer($_POST["AccountID"])==0)
				{
					//javascript to php validation
					//$rd=User::model()->find(array('select'=>'level_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>Yii::app()->session['account_id']),));
					$command = $connection->createCommand("SELECT b.currency_name from tbl_cash_player a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE a.account_id='" . $_POST['AccountID'] . "'");
					$rd1=$command->queryRow();
					if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerDeposit'))
					{
						//javascript to php validation
						if($_POST['dblAmount']>0){
							$command = $connection->createCommand("CALL spUpdateCashPlayerDeposit('" . $_POST["AccountID"] . "','" . Yii::app()->session['account_id'] . "'," . $_POST['dblAmount'] . ",6,'" . TransactionNumber::generateTransactionNumber("MD") . "'," . $_POST['intDepositor'] . "," . $_POST['intSelectedDepositor'] . ")");
							$command->execute();
							
							$dateTime = date('Y-m-d H:i:s');
							$AccountID = Yii::app()->session['account_id'];
							$level=Yii::app()->session['level_name'];
							
							$postLog=new TableLog;
							$postLog->operated_by=$AccountID;
							$postLog->operated_by_level=$level;
							$postLog->operated=$_POST["AccountID"];
							$postLog->operated_level='Player';
							$postLog->operation_time=$dateTime;
							$postLog->log_type_id=3;
							$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> Deposit:<label style=\"color:red\">'.number_format($_POST['dblAmount'],2,'.',',').'</label></b>';
							$postLog->save();
							header('Location: '.  Yii::app()->request->baseUrl . '/index.php?r=CashPlayer/CashPlayerList');
						}
					}
				}
				else
				{
					echo "<script type=\"text/javascript\">";
					echo "alert('Player is still on a lobby. Please try again!');";
					echo "history.back();";
					echo "</script>";
					
				}
			}
			if ($_POST['task']=='playerDepositorList')
			{
				$htmlCode='';
				if ($_POST['intDepositor']==0)
				{
					$dataReader = TableBank::model()->findAll();
					foreach ($dataReader as $row){
						$htmlCode.= $row['bank_name'] . ':' . $row['id'] . ',';
					}
				}
				else 
				{
					$dataReader = TableRunner::model()->findAll();
					foreach ($dataReader as $row){
						$htmlCode.= $row['runner_name'] . ':' . $row['id'] . ',';
					}
				}
				echo $htmlCode;
			}
			
			if($_POST['task']=='checkPlayerOnlineStat')
			{
				if (Yii::app()->session['account_id']!=null){
			
					$this->onlinePlayer=new RedisManager();
					$checkLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['accountID']);//($_POST['AccountID']);
			
					$casinoId=$this->onlinePlayer->getCasinoIDByPlayer($_POST['accountID']);
			
						
					if ($checkLobby!=0){
						echo $casinoId;
					}else{
			
							$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
							$rd=$command->queryRow();
							if ($rd['deposit_withdrawal'] == 1) // display withdrawal wating message.
							{
								echo'w';
							}else {
								//display deposit dialog
								echo '0';
							}
						}
					}
					else
					{
						echo 'die';
					}
			}

		}
	}
}