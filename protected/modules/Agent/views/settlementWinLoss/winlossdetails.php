<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentwinloss.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/settlementwinlossdetails.js"></script>
	<script type="text/javascript">
		var wd_baseURL = '<?php echo Yii::app()->request->baseUrl;?>';
		var wd_urlHTV999WinLossDetailsSummaryFooter = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTV999WinLossDetailsSummaryFooter';
		var wd_urlSAVANWinLossBettingDetailsSummaryFooter = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANWinLossBettingDetailsSummaryFooter';
		var wd_urlWinLossArrow = '<?php echo $this->module->assetsUrl; ?>';
		var wd_urlWinLossResultDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasWinLossResultDetails';
		var wd_urlHTVWinLossResultDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HatienVegasWinLossResultDetails';
		var wd_urlSAVANWinLossResultDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SavanVegasWinLossResultDetails';
		var wd_urlCostaVegasTotal = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasTotal';
		var wd_lblMainCompany = '<?php echo Yii::t('agent','agent.agentwinloss.main_company');?>';
		var wd_lblSubCompany = '<?php echo Yii::t('agent','agent.agentwinloss.sub_company');?>';
		var wd_lblSeniorMasterAgent = '<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent');?>';
		var wd_lblMasterAgent = '<?php echo Yii::t('agent','agent.agentwinloss.master_agent');?>';
		var wd_lblAgent = '<?php echo Yii::t('agent','agent.agentwinloss.agent');?>';
		var wd_lblSubCompanyTotal = '<?php echo Yii::t('agent','agent.agentwinloss.sub_company_total');?>';
		var wd_lblSeniorMasterAgentTotal = '<?php echo Yii::t('agent','agent.agentwinloss.senior_master_agent_total');?>';
		var wd_lblMasterAgentTotal = '<?php echo Yii::t('agent','agent.agentwinloss.master_agent_total');?>';
		var wd_lblAgentTotal = '<?php echo Yii::t('agent','agent.agentwinloss.agent_total');?>';
		var wd_lblNumber = '<?php echo Yii::t('agent','agent.agentwinloss.number');?>';
		var wd_lblCurrency = '<?php echo Yii::t('agent','agent.agentwinloss.currency');?>';
		var wd_lblValidStake = '<?php echo Yii::t('agent','agent.agentwinloss.valid_stake');?>';
		var wd_lblWinLoss = '<?php echo Yii::t('agent','agent.agentwinloss.winloss');?>';
		var wd_lblCommission = '<?php echo Yii::t('agent','agent.agentwinloss.commission');?>';
		var wd_lblTips = '<?php echo Yii::t('agent','agent.agentwinloss.tips');?>';
		var wd_lblTotal = '<?php echo Yii::t('agent','agent.agentwinloss.total');?>';
		var wd_lblCompany = '<?php echo Yii::t('agent','agent.agentwinloss.company');?>';
		var wd_captionCostaVegasWinLossTotal = "<font color='#D84A38'><label id='lblCostaTotalWinLossID'></label></font> COSTA VEGAS TOTAL <select id='currencyTypeConvertCOSTA' onchange='javascript: wd_costaTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select> <label style='<?php if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=9){echo 'display: inline;';}else{echo 'display: none;';}?>'>VIG Sharing:</label> <input type='text' id='wd_txtVIGsharing1' onkeypress='javascript: return wd_checkIt(event,\"wd_txtVIGsharing\"); ' onKeyup='javascript:wd_copyVIGValue(this.value);' style='<?php if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=9){echo 'display: inline;';}else{echo 'display: none;';}?>'><label style='<?php if(Yii::app()->session['level']>=1 && Yii::app()->session['level']<=9){echo 'display: inline;';}else{echo 'display: none;';}?>'>%</label>";
		var wd_urlCostaVegasSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasSummaryWinLoss';
		var wd_lblAccountId = '<?php echo Yii::t('agent','agent.agentwinloss.account_id');?>';
		var wd_lblAccountName = '<?php echo Yii::t('agent','agent.agentwinloss.account_name');?>';
		var wd_lblSharingCommission = '<?php echo Yii::t('agent','agent.agentwinloss.sharing_commission');?>';
		var wd_lblAverageBet = '<?php echo Yii::t('agent','agent.agentwinloss.average_bet');?>';
		var wd_urlAgentSharingAndCommission = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SharingAndCommission';
		var wd_captionCostaVegasWinLoss = "<label id='wd_lblAgentID'></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_title');?> <label id='wd_lblDateFrom_costa'></label> - <label id='wd_lblDateTo_costa'></label> <input type='button' id='btnBack_costa' value='<<' onclick='wd_backToParent(document.getElementById(\"wd_lblAgentID\").innerHTML);'> <select id='currencyTypeConvertCOSTAWinLoss' onchange='javascript: wd_costaWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_urlCostaVegasSummaryMemberWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasSummaryMemberWinLoss';
		var wd_captionCostaVegasMemberWinLoss = "<label id='wd_lblAgentID'></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_title');?> <label id='wd_lblDateFrom_costa'></label> - <label id='wd_lblDateTo_costa'></label> <input type='button' value='<<' id='btnBack_costa' onclick='wd_backToParent(document.getElementById(\"wd_lblAgentID\").innerHTML);'> <input type='button' id='btnDetails' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='wd_showDetails(document.getElementById(\"wd_lblAgentID\").innerHTML);'> <select id='currencyTypeConvertCOSTAWinLoss' onchange='javascript: wd_costaMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_lblPlayer = '<?php echo Yii::t('agent','agent.agentwinloss.player');?>';
		var wd_lblBalance = '<?php echo Yii::t('agent','agent.agentwinloss.balance');?>';
		var wd_urlCostaVegasWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasWinLossDetails';
		var wd_urlExportAllCostaVegasWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllCostaVegasWinLossDetails';
		var wd_captionCostaVegasWinLossDetails = "<label id='lblDetailAgentID'></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_winloss_details');?> <label id='lblDetailDateFrom_costa_details'></label> - <label id='lblDetailDateTo_costa_details'></label> <input type='button' id='btnDetailBack' value='<<' onclick='wd_backToParentDetail(document.getElementById(\"lblDetailAgentID\").innerHTML);'>";
		var wd_urlCostaVegasMemberWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasMemberWinLossDetails';
		var wd_urlExportAllCostaVegasMemberWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllCostaVegasMemberWinLossDetails';
		var wd_lblBetTime = '<?php echo Yii::t('agent','agent.agentwinloss.bet_datetime');?>';
		var wd_lblGameType = '<?php echo Yii::t('agent','agent.agentwinloss.game_type');?>';
		var wd_lblCasino = '<?php echo Yii::t('agent','agent.agentwinloss.casino');?>';
		var wd_cationMemberWinLossCostaVegasDetails = '<b><label id="lblBettingDetailAgentID"></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_details_title');?></b> <label id="lblDDateFrom"></label> - <label id="lblDDateTo"></label> <input type="button" value="<<" onclick="wd_hideBettingDetails();">';
		var wd_cationMemberWinLossCostaVegasDetails1 = '<b><label id="lblBettingDetailAgentID"></label> <?php echo Yii::t('agent','agent.agentwinloss.costavegas_details_title');?></b> <label id="lblDDateFrom_details"></label> - <label id="lblDDateTo_details"></label> <input type="button" value="<<" onclick="hideBettingDetails1();">';
		var wd_urlHTV999Total = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTV999Total';
		var wd_lblTotalStake = '<?php echo Yii::t('agent','agent.agentwinloss.total_stake');?>';
		var wd_cationHTVWinLossTotal = "<font color='#D84A38'><label id='lblHTVTotalWinLossID'></label></font> HTV TOTAL <select id='currencyTypeConvertHTV' onchange='javascript: wd_htvTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_urlHTVSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTVSummaryWinLoss';
		var wd_lblBetCount = '<?php echo Yii::t('agent','agent.agentwinloss.bet_count');?>';
		var wd_captionHTVWinLoss = "<label id='wd_lblAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='wd_lblDateFrom_htv'></label> - <label id='wd_lblDateTo_htv'></label> <input type='button' id='btnBack_htv' value='<<' onclick='wd_backToParent_htv(document.getElementById(\"wd_lblAgentID_htv\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: wd_htvWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_urlHTVSummaryMemberWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTVSummaryMemberWinLoss';
		var wd_captionHTVMemberWinLoss = "<label id='wd_lblAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_title');?> <label id='wd_lblDateFrom_htv'></label> - <label id='wd_lblDateTo_htv'></label> <input type='button' value='<<' id='btnBack_htv' onclick='wd_backToParent_htv(document.getElementById(\"wd_lblAgentID_htv\").innerHTML);'> <input type='button' id='btnDetails_htv' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='wd_showDetails_htv(document.getElementById(\"wd_lblAgentID_htv\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: wd_htvMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_urlHTVWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTVWinLossDetails';
		var wd_captionHTVWinLossDetails = "<label id='lblDetailAgentID_htv'></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_winloss_details');?> <label id='lblDetailDateFrom_htv'></label> - <label id='lblDetailDateTo_htv'></label> <input type='button' id='btnDetailBack_htv' value='<<' onclick='wd_backToParentDetail_htv(document.getElementById(\"lblDetailAgentID_htv\").innerHTML);'>";
		var wd_urlHTV999WinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/HTV999WinLossDetails';
		var wd_lblTableShoeGame = "<?php echo Yii::t('agent','agent.agentwinloss.table_shoe_game');?>";
		var wd_lblBetAmount = "<?php echo Yii::t('agent','agent.agentwinloss.bet_amount');?>";
		var wd_lblValidAmount = '<?php echo Yii::t('agent','agent.agentwinloss.valid_amount');?>';
		var wd_lblResult = "<?php echo Yii::t('agent','agent.agentwinloss.result');?>";
		var wd_lblBalanceBefore = "<?php echo Yii::t('agent','agent.agentwinloss.balance_before');?>";
		var wd_lblBalanceAfter = "<?php echo Yii::t('agent','agent.agentwinloss.balance_after');?>";
		var wd_captionMemberWinLossHTV999details = '<b><label id="lblBettingDetailAgentID_htv"></label> <?php echo Yii::t('agent','agent.agentwinloss.htv_betting_details_title');?></b> <label id="lblDDateFrom_htv"></label> - <label id="lblDDateTo_htv"></label> <input type="button" value="<<" onclick="wd_hideBettingDetails_htv();">';
		var wd_urlSAVANTotal = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANTotal';
		var wd_captionSAVANWinLossTotal = "<font color='##D84A38'><label id='lblSAVANTotalWinLossID'></label></font> SAVANVEGAS TOTAL <select id='currencyTypeConvertSAVAN' onchange='javascript: wd_savanTotalConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_urlSAVANSummaryWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANSummaryWinLoss';
		var wd_captionSAVANWinLoss = "<label id='wd_lblAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='wd_lblDateFrom_savan'></label> - <label id='wd_lblDateTo_savan'></label> <input type='button' id='btnBack_savan' value='<<' onclick='wd_backToParent_savan(document.getElementById(\"wd_lblAgentID_savan\").innerHTML);'> <select id='currencyTypeConvertSAVANWinLoss' onchange='javascript: wd_savanWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_urlSAVANSummaryMemberWinLoss = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANSummaryMemberWinLoss';
		var wd_captionSAVANMemberWinLoss = "<label id='wd_lblAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_title');?> <label id='wd_lblDateFrom_savan'></label> - <label id='wd_lblDateTo_savan'></label> <input type='button' value='<<' id='btnBack_savan' onclick='wd_backToParent_savan(document.getElementById(\"wd_lblAgentID_savan\").innerHTML);'> <input type='button' id='btnDetails_savan' value='<?php echo Yii::t('agent','agent.agentwinloss.details');?>' onclick='wd_showDetails_savan(document.getElementById(\"wd_lblAgentID_savan\").innerHTML);'> <select id='currencyTypeConvertHTVWinLoss' onchange='javascript: wd_savanMemberWinLossConversion(this.value);'><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value=' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' >'. strtoupper($row['currency_name']) . '</option>';}?></select>";
		var wd_urlSAVANWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANWinLossDetails';
		var wd_urlExportAllSAVANWinLossDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllSAVANWinLossDetails';
		var wd_captionSAVANWinLossDetails = "<label id='lblDetailAgentID_savan'></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_winloss_details');?> <label id='lblDetailDateFrom_savan'></label> - <label id='lblDetailDateTo_savan'></label> <input class='btn red' type='button' id='btnDetailBack_savan' value='<<' onclick='wd_backToParentDetail_savan(document.getElementById(\"lblDetailAgentID_savan\").innerHTML);'>";
		var wd_urlSAVANWinLossBettingDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/SAVANWinLossBettingDetails';
		var wd_urlExportAllSAVANWinLossBettingDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/ExportAllSAVANMemberWinLossDetails';
		var wd_lblValidBet = "<?php echo Yii::t('agent','agent.agentwinloss.valid_bet');?>";
		var wd_captionMemberWinLossSavanVegasDetails = '<b><label id="lblBettingDetailAgentID_savan"></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_betting_details_title');?></b> <label id="lblDDateFrom_savan"></label> - <label id="lblDDateTo_savan"></label> <input class="btn red" type="button" value="<<" onclick="wd_hideBettingDetails_savan();">';
		var wd_captionMemberWinLossSavanVegasDetails1 = '<b><label id="lblBettingDetailAgentID_savan"></label> <?php echo Yii::t('agent','agent.agentwinloss.savan_betting_details_title');?></b> <label id="lblDDateFrom_savan_details"></label> - <label id="lblDDateTo_savan_details"></label> <input type="button" value="<<" onclick="hideBettingDetails_savan1();">';
		var wd_sessionLevel = '<?php echo Yii::app()->session['level'];?>';
		var wd_sessionAccountId = '<?php echo Yii::app()->session['account_id'];?>';
		var wd_account_type='<?php echo User::getUserType();?>';
		var wd_dtToday='<?php echo date("Y-m-d");?>';
		var wd_dtYesterday='<?php echo date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d"))));?>';
		var wd_readCostavegasWinLoss = '<?php echo Yii::app()->user->checkAccess('agent.readCostaWinLoss');?>';
		var wd_readHatienvegasWinLoss = '<?php echo Yii::app()->user->checkAccess('agent.readHTVWinLoss');?>';
		var wd_readSavanvegasWinLoss = '<?php echo Yii::app()->user->checkAccess('agent.readSavanWinLoss');?>';
		var wd_urlCostaVegasMemberWinLossDetailsSummaryFooter = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentWinLoss/CostaVegasMemberWinLossDetailsSummaryFooter';
		var wd_lblIpAddress = '<?php echo Yii::t('agent','agent.agentwinloss.ip_address');?>';;
	</script>
	
</head>
<body>
	<div id="parameter_area">
		<div class="header"><font color="#f0e62b"><?php echo Yii::app()->session['account_id'];?></font> <?php echo Yii::t('agent','agent.agentwinloss.agent_winloss');?></div>
		<div class="body">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.game');?></td>
			<td class="right">
				<select name="wd_cmbGame" id="wd_cmbGame" class="cmb">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. strtoupper($row['game_name']) . '</option>';
					}
					?>
				</select>
			</td></tr>
			<tr><td class="left"><?php echo Yii::t('agent','agent.agentwinloss.currency');?></td>
			<td class="right">
				<select name="wd_cmbCurrency" id="wd_cmbCurrency" class="cmb" onchange="if(this.value!=''){document.getElementById('wd_chkTestCurrency').style.display='none';document.getElementById('lblTestCurrency').style.display='none';}else{document.getElementById('wd_chkTestCurrency').style.display='inline';document.getElementById('lblTestCurrency').style.display='inline';}">
					<option value="">ALL</option>
					<?php 
					$dataReader = TableCurrency::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['currency_name'] . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>
				<input type="checkbox" id="wd_chkTestCurrency" checked/><label id="lblTestCurrency" for="wd_chkTestCurrency"><?php echo Yii::t('agent','agent.agentwinloss.except_test_currency');?></label>
			</td></tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.from');?></td>
				<td class="right">
					<input type="text" id="wd_dtFrom" value="<?php echo date("Y-m-d");?>">
					<select id="wd_cmbTime1">
					<?php 
						for($i=0;$i<=23;$i++){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					?>
					</select> :00:00
				</td>
			</tr>
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.agentwinloss.to');?></td>
				<td class="right">
					<input type="text" id="wd_dtTo" value="<?php echo date("Y-m-d");?>">
					<select id="wd_cmbTime2">
					<?php 
						for($i=0;$i<=23;$i++){
							if($i<23){
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}else{
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}
						}
					?> 
					</select> :59:59
				</td>
			</tr>
			<tr>
				<td class="left">Account ID</td>
				<td class="right"><input type="text" id="wd_txtAccountID"></td>
			</tr>
			<tr>
				<td class="left">Casino</td>
				<td class="right">
				<?php 
					// Costavegas
					if (Yii::app()->user->checkAccess('agent.readCostaWinLoss') ||	User::getUserType() === 'agent'){
						echo '<input type="checkbox" id="wd_chkVIG" checked><label id="wd_lblVIG" for="wd_chkVIG">Costavegas999</label>';
					}else{ // for provider role
						echo '<input type="checkbox" id="wd_chkVIG" style="display:none;"><label id="wd_lblVIG" for="wd_chkVIG" style="display:none;">Costavegas999</label>';
						echo '<input type="hidden" id="wd_lblAgentID"><input type="hidden" id="wd_lblDateFrom_costa"><input type="hidden" id="wd_lblDateTo_costa"><input type="hidden" id="btnBack_costa">';
					}
					
					// Hatienvegas enable or disable HTV999 
// 					if (Yii::app()->user->checkAccess('agent.readHTVWinLoss') ||	User::getUserType() === 'agent'){
// 						echo '<input type="checkbox" id="wd_chkHTV" checked><label for="wd_chkHTV">Hatienvegas999</label> ';
// 					}else{
						//echo '<input type="checkbox" id="wd_chkHTV" ><label for="wd_chkHTV">Hatienvegas999</label> ';
						echo '<input type="checkbox" id="wd_chkHTV" disabled><label for="wd_chkHTV">Hatienvegas999</label> ';
						echo '<input type="hidden" id="wd_lblAgentID_htv"><input type="hidden" id="wd_lblDateFrom_htv"><input type="hidden" id="wd_lblDateTo_htv"><input type="hidden" id="btnBack_htv">';
//					}
					
					// Savanvegas
					if (Yii::app()->user->checkAccess('agent.readSavanWinLoss') ||	User::getUserType() === 'agent'){
						echo '<input type="checkbox" id="wd_chkSAVAN" checked><label for="wd_chkSAVAN">Savanvegas999</label></td>';
					}else{
						echo '<input type="checkbox" id="wd_chkSAVAN" style="display:none;"><label for="wd_chkSAVAN" style="display:none;">Savanvegas999</label> ';
						echo '<input type="hidden" id="wd_lblAgentID_savan"><input type="hidden" id="wd_lblDateFrom_savan"><input type="hidden" id="wd_lblDateTo_savan"><input type="hidden" id="btnBack_savan">';
					}
				?>
				 
				
				
			</tr>
			</table>
			<div class="footer"><input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.submit');?>" onclick="javascript: wd_showDetails(''); wd_showDetails_savan(''); clearContainer();"> <input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.yesterday');?>" onclick="javascript: wd_btnYesterday(); clearContainer();"> <input type="button" class="btn red" value="<?php echo Yii::t('agent','agent.agentwinloss.today');?>" onclick="javascript: wd_btnToDay(); clearContainer();"></div>
		</div>
	</div>
	<div id="agent_winloss_body">
		<div id="vig_sharing"><input type='text' id='wd_txtVIGsharing' value='10'/></div>
	<div id="dv_costavegas">
		<div id="wd_qry_costavegas_total"></div>
    	<div id="wd_qry_costavegas_result"></div>
    	<div id="wd_qry_costavegas_result_details"></div>
    	<div id="wd_qry_costavegas_betting_details"></div>
    	<div id="wd_qry_costavegas_betting_details_summary_footer" style="display: none;">
    		<div id="betting_footer_header"><label id="wd_lblCostaBettingSummaryPlayerID"></label> COSTAVEGAS BETTING HISTORY SUMMARY</div>
    		<table id="betting_footer" cellpadding="5" cellspacing="0">
    		<tr>
    			<td class="theader">Type</td>
    			<td class="theader">No. of Bet</td>
    			<td class="theader">Valid Stake</td>
    			<td class="theader">Win/Loss</td>
    			<td class="theader">Commission</td>
    			<td class="theader">Tips</td>
    			<td class="theader">Total</td>
    			<td class="theader">Balance</td>
    		</tr>
    		<tr>
    			<td>Current Page</td>
    			<td><label id="wd_lblCPNoOfBet">0</label></td>
    			<td><label id="wd_lblCPValidStake">0</label></td>
    			<td><label id="wd_lblCPWinLoss">0</label></td>
    			<td><label id="wd_lblCPCommission">0</label></td>
    			<td><label id="wd_lblCPTips">0</label></td>
    			<td><label id="wd_lblCPTotal">0</label></td>
    			<td><label id="wd_lblCPBalance">N/A</label></td>
    		</tr>
    		<tr>
    			<td>All Page</td>
    			<td><label id="wd_lblAPNoOfBet">0</label></td>
    			<td><label id="wd_lblAPValidStake">0</label></td>
    			<td><label id="wd_lblAPWinLoss">0</label></td>
    			<td><label id="wd_lblAPCommission">0</label></td>
    			<td><label id="wd_lblAPTips">0</label></td>
    			<td><label id="wd_lblAPTotal">0</label></td>
    			<td><label id="wd_lblAPBalance">0</label></td>
    		</tr>
    		</table>
    	</div>
    </div>
    <div id="dv_hatienvegas">
    	<div id="wd_qry_htv999_total"></div>
    	<div id="wd_qry_htv999_result"></div>
    	<div id="wd_qry_htv999_result_details"></div>
    	<div id="wd_qry_htv999_betting_details"></div>
    	<div id="wd_qry_htv999_betting_details_summary_footer" style="display: none;">
    		<div id="betting_footer_header"><label id="wd_lblHTVBettingSummaryPlayerID"></label> HTV BETTING HISTORY SUMMARY</div>
    		<table id="betting_footer" cellpadding="5" cellspacing="0">
    		<tr>
    			<td class="theader">Type</td>
    			<td class="theader">No. of Bet</td>
    			<td class="theader">Bet Amount</td>
    			<td class="theader">Valid Bet</td>
    			<td class="theader">Win/Loss</td>
    			<td class="theader">Commission</td>
    			<td class="theader">Total</td>
    			<td class="theader">Balance</td>
    		</tr>
    		<tr>
    			<td>Current Page</td>
    			<td><label id="wd_lblCPNoOfBet_htv">0</label></td>
    			<td><label id="wd_lblCPBetAmount_htv">0</label></td>
    			<td><label id="wd_lblCPValidBet_htv">0</label></td>
    			<td><label id="wd_lblCPWinLoss_htv">0</label></td>
    			<td><label id="wd_lblCPCommission_htv">0</label></td>
    			<td><label id="wd_lblCPTotal_htv">0</label></td>
    			<td><label id="wd_lblCPBalance_htv">0</label></td>
    		</tr>
    		<tr>
    			<td>All Page</td>
    			<td><label id="wd_lblAPNoOfBet_htv">0</label></td>
    			<td><label id="wd_lblAPBetAmount_htv">0</label></td>
    			<td><label id="wd_lblAPValidBet_htv">0</label></td>
    			<td><label id="wd_lblAPWinLoss_htv">0</label></td>
    			<td><label id="wd_lblAPCommission_htv">0</label></td>
    			<td><label id="wd_lblAPTotal_htv">0</label></td>
    			<td><label id="wd_lblAPBalance_htv">0</label></td>
    		</tr>
    		</table>
    	</div>
    </div>
    <br/>
    <div id="dv_savanvegas">
    	<div id="wd_qry_savan_vegas_total"></div>
    	<div id="wd_qry_savan_vegas_result"></div>
    	<div id="wd_qry_savan_vegas_result_details"></div>
    	<div id="wd_qry_savan_vegas_betting_details"></div>
	    <div id="wd_qry_savan_vegas_betting_details_summary_footer" style="display: none;">
    		<div id="betting_footer_header"><label id="wd_lblSAVANBettingSummaryPlayerID"></label> SAVANVEGAS BETTING HISTORY SUMMARY</div>
    		<table id="betting_footer" cellpadding="5" cellspacing="0">
    		<tr>
    			<td class="theader">Type</td>
    			<td class="theader">No. of Bet</td>
    			<td class="theader">Bet Amount</td>
    			<td class="theader">Valid Bet</td>
    			<td class="theader">Win/Loss</td>
    			<td class="theader">Commission</td>
    			<td class="theader">Total</td>
    			<td class="theader">Balance</td>
    		</tr>
    		<tr>
    			<td>Current Page</td>
    			<td><label id="wd_lblCPNoOfBet_savan">0</label></td>
    			<td><label id="wd_lblCPBetAmount_savan">0</label></td>
    			<td><label id="wd_lblCPValidBet_savan">0</label></td>
    			<td><label id="wd_lblCPWinLoss_savan">0</label></td>
    			<td><label id="wd_lblCPCommission_savan">0</label></td>
    			<td><label id="wd_lblCPTotal_savan">0</label></td>
    			<td><label id="wd_lblCPBalance_savan">0</label></td>
    		</tr>
    		<tr>
    			<td>All Page</td>
    			<td><label id="wd_lblAPNoOfBet_savan">0</label></td>
    			<td><label id="wd_lblAPBetAmount_savan">0</label></td>
    			<td><label id="wd_lblAPValidBet_savan">0</label></td>
    			<td><label id="wd_lblAPWinLoss_savan">0</label></td>
    			<td><label id="wd_lblAPCommission_savan">0</label></td>
    			<td><label id="wd_lblAPTotal_savan">0</label></td>
    			<td><label id="wd_lblAPBalance_savan">0</label></td>
    		</tr>
    		</table>
    	</div>
    </div>
	</div>
	<script type="text/javascript">
		<?php if(Yii::app()->user->checkAccess('agent.readCostavegasWinLoss')) { ?>
		$('#wd_chkVIG').hide();
		$('#wd_lblVIG').hide();
		<?php }?>
	</script>
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
	</form>
	<?php 
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		    'id'=>'wd_winloss_result_img',
		    // additional javascript options for the dialog plugin
		    'options'=>array(
		        'title'=>'Screen Shot',
		        'autoOpen'=>false,
	    		'width'=>'auto',
	    		'height'=>'auto',
		    	'modal'=> true,
		    	'resizable'=> false,
		    ),
		));
	
	// Initialize dialog content
    echo '<div id="viewWinLossResultImg"></div>';
	
    // End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'wd_winloss_bet_result_detail',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Win/Loss Betting Detail',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> true,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewResultDetail"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	?>
</body>
</html>