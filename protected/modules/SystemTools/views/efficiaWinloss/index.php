<?php 
//apply restrictions
if(Yii::app()->session['level']==9 || Yii::app()->session['level']==10){
	echo '<div style="position: relative; top:28px; left:415px"><img src="'.Yii::app()->request->baseUrl.'/images/process_warning.png"/></div>';
	echo '<div style="position: relative; top:30px; left:315px"><b><label style="color:red">Warning:  </label>You are not authorized to access this page!</b></div>';
}else{
?>
<!--@TODO: VIEWS FOR CASH PLAYER WIN/LOSS
    @AUTHOR: Allan
    @SINCE: 05212012
-->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/button.css" />

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentwinloss.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('mnu_winloss').style.color="#5A0000";
	document.getElementById('mnu_winloss').style.fontWeight="bold";
</script>
<script type="text/javascript">

	var cRowNo=0;
		function tableWinLossTodayEfficia(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultEfficia").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultEfficia").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemTools/EfficiaWinloss/EfficiaWinloss&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','Account ID', 'Account Name','Currency','Bet Count','Total Stake','Valid Stake','Average Bet','Win/Loss'],
			    colModel: [
						{name: 'no', index: 'no', width: 25,title:false,sortable:false},
						{name: 'account_id', index: 'account_id', width: 120, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>'},
						{name: 'account_name', index: 'account_name', width: 130,title:false},
						{name: 'currency', index: 'currency', width: 55,title:false},
						{name: 'bet_count', index: 'bet_count', width: 60, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bet_count">{0}</label>'},
						{name: 'total_stake', index: 'total_stake', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_stake">{0}</label>'},
						{name: 'amount_wager', index: 'amount_wager', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_wager">{0}</label>'},
						{name: 'avg_bet', index: 'avg_bet', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_bet">{0}</label>'},
						{name: 'win_los', index: 'win_los', width: 140, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
						],
			    
				loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list1");
			        
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"bonus","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
				    	grid.jqGrid('setCell',i,"account_id","",{'color':'black'});
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
					    }
				    	$(".dateSummaryFooter").each(function(i) {this.style.color='#000000';});
				    	 $(".bet_count").each(function(i) {this.style.color='#000000';});
					     $(".total_stake").each(function(i) {this.style.color='#000000';});
				    	 $(".amount_wager").each(function(i) {this.style.color='#000000';});
					     $(".avg_bet").each(function(i) {this.style.color='#000000';});
					     $(".win_los").each(function(i) {this.style.color='#000000';});
					     $(".commission").each(function(i) {this.style.color='#000000';});
					     $(".bonus").each(function(i) {this.style.color='#000000';});
					     $(".total_win_los").each(function(i) {this.style.color='#000000';});//pl
					     $(".current_balance").each(function(i) {this.style.color='#000000';});
					     $(".p_l").each(function(i) {this.style.color='#000000';});//pl

				    	 $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//total_winloss
					     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					     $(".p_l").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					} 
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			   
			    hidegrid: false,  
			    rowNum: 20,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'currency',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#7A0000">HTV</label> Player Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
				groupingView : {
			   		groupField : ['currency'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   
			   	
			});
			$('#list1').jqGrid('navGrid', '#page1', {edit: false, add: false, del:false, search: false});
		});
	} 

		function tableWinLossTodayEfficia1(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
			var divTag = document.createElement("Table"); 
		    divTag.id = 'list2'; 
		    divTag.style.margin = "0px auto"; 
		    document.getElementById("qry_resultEfficia1").appendChild(divTag);

		    var divTag1 = document.createElement("div"); 
		    divTag1.id = 'pager2'; 
		    divTag1.style.margin = "0px auto"; 
		    document.getElementById("qry_resultEfficia1").appendChild(divTag1);

		    $(document).ready(function() {
				var grid=jQuery("#list2");
				grid.jqGrid({ 
					url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemTools/EfficiaWinloss/EfficiaWinloss1&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
					datatype: 'json',
				    mtype: 'GET',
				    height: 'auto',
				    colNames: ['','Account ID', 'Account Name','Currency','Bet Count','Total Stake','Valid Stake','Average Bet','Win/Loss'],
				    colModel: [
							{name: 'no', index: 'no', width: 25,title:false,sortable:false},
							{name: 'account_id', index: 'account_id', width: 120, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>'},
							{name: 'account_name', index: 'account_name', width: 130,title:false},
							{name: 'currency', index: 'currency', width: 55,title:false},
							{name: 'bet_count', index: 'bet_count', width: 60, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="bet_count">{0}</label>'},
							{name: 'total_stake', index: 'total_stake', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_stake">{0}</label>'},
							{name: 'amount_wager', index: 'amount_wager', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_wager">{0}</label>'},
							{name: 'avg_bet', index: 'avg_bet', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_bet">{0}</label>'},
							{name: 'win_los', index: 'win_los', width: 140, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
							],
				    
					loadtext:"",
				    loadComplete: function() {
				        var myGrid = $("#list2");
				        
				        var i=0; 
					    for(i=0;i<=grid.getGridParam("reccount");i++)
					    {
					    	var myrow = grid.jqGrid('getRowData', i);
					    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
					    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
					    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
					    	grid.jqGrid('setCell',i,"bonus","",{background:'#f7f7c8'});
					    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
					    	grid.jqGrid('setCell',i,"account_id","",{'color':'black'});
					    	if(myrow.win_los < 0){
					    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
						    }
					    	if(myrow.total_win_los < 0){
					    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
						    }
					    	if(myrow.p_l < 0){
					    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
						    }
					    	$(".dateSummaryFooter").each(function(i) {this.style.color='#000000';});
					    	 $(".bet_count").each(function(i) {this.style.color='#000000';});
						     $(".total_stake").each(function(i) {this.style.color='#000000';});
					    	 $(".amount_wager").each(function(i) {this.style.color='#000000';});
						     $(".avg_bet").each(function(i) {this.style.color='#000000';});
						     $(".win_los").each(function(i) {this.style.color='#000000';});
						     $(".commission").each(function(i) {this.style.color='#000000';});
						     $(".bonus").each(function(i) {this.style.color='#000000';});
						     $(".total_win_los").each(function(i) {this.style.color='#000000';});//pl
						     $(".current_balance").each(function(i) {this.style.color='#000000';});
						     $(".p_l").each(function(i) {this.style.color='#000000';});//pl

					    	 $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//total_winloss
						     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
						     $(".p_l").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
						} 
					    $("tr.jqgrow:odd").css("background", "#DDDDDC");
				    	$("tr.jqgrow:even").css("background", "#ffffff");
				    },
				   
				    hidegrid: false,  
				    rowNum: 20,	
				    rowList: [20, 50, 100,200,500,99999],
				    pager: '#pager2',
				    sortname: 'currency',
				    sortorder: 'ASC',
				    caption: ' <label style="color:#7A0000">SAVAN</label> Player Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
				    viewrecords: true,
				    grouping: true,
					groupingView : {
				   		groupField : ['currency'],
				   		groupColumnShow : [true],
				   		groupText : ['<b>{0}</b>'],
				   		groupCollapse : false,
						groupOrder: ['asc'],
						groupSummary : [true],
						groupDataSorted : true
				   	},
				   
				   	
				});
				$('#list2').jqGrid('navGrid', '#page2', {edit: false, add: false, del:false, search: false});
			});
		} 
</script>

<script type="text/javascript">
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a ^ 999;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}

function validate()
{
	var pattern = /^-?[-\0-9]+(.[0-9]{1,2})?$/; 
	var text = document.getElementById('txtVIGSharing1').value;
    if (text.match(pattern)==null) 
    {
		alert('Invalid VIG sharing value.');
		return false;
    }
	else
	{
	  return true;
	}
}

</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});


	function btnclick(btnname)
	{
		
		
		if (btnname=="Load")
		{
			document.getElementById("chkTest").checked=true;
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");

			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			
			var year = currentTime.getFullYear();
		
			var dayL=day.toString();
			var monthL=month.toString();

			if (monthL.length==1){
				month="0"+month;
			}
			if (dayL.length==1){
				day="0"+day;
			}

			
			
			document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
			document.getElementById("dateto").value=(year + "-" + month + "-" + day);
			
			if (document.getElementById('defaultTimeFrom').value=='')
			{
				document.getElementById('qry_resultEfficia').innerHTML='';
				tableWinLossTodayEfficia("All","All","All",datefrom,dateto,testChecked);
				document.getElementById('qry_resultEfficia1').innerHTML='';
				tableWinLossTodayEfficia1("All","All","All",datefrom,dateto,testChecked);

			}
			else
			{
				var datefrom=enc(document.getElementById('defaultTimeFrom').value);
				var dateto=enc(document.getElementById('defaultTimeTo').value);
				var accountId="All";

				id=enc('<?php  if (isset($_GET['id'])){ echo $_GET['id'];}?>');

				
				if (id==''){
					document.getElementById('txtAccountId').value='';
				}else{
					document.getElementById('txtAccountId').value=id;
					accountId=id;
				}
				var splitDateFrom=datefrom.split(" ");
				var splitDateTo=dateto.split(" ");
				var splitDateFrom1=splitDateFrom[0].split("-");
				var splitDateTo1=splitDateTo[0].split("-");
				document.getElementById("datefrom").value=splitDateFrom1[0]+"-"+splitDateFrom1[1]+"-"+splitDateFrom1[2];
				document.getElementById("dateto").value=splitDateTo1[0]+"-"+splitDateTo1[1]+"-"+splitDateTo1[2];
				
				
				document.getElementById('qry_resultEfficia').innerHTML='';
				tableWinLossTodayEfficia(accountId,"All","All",datefrom,dateto,testChecked);
				document.getElementById('qry_resultEfficia1').innerHTML='';
				tableWinLossTodayEfficia1(accountId,"All","All",datefrom,dateto,testChecked);

			}			
		}
		else if (btnname=="Today")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			var dayL=theday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theday="0"+theday;
			}
			var datefrom=(theyear+"-"+themonth+"-"+theday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theday+ " 23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
		
			document.getElementById('qry_resultEfficia').innerHTML='';
			tableWinLossTodayEfficia(accountId,currencytype,gameType,datefrom,dateto,testChecked);
			document.getElementById('qry_resultEfficia1').innerHTML='';
			tableWinLossTodayEfficia1(accountId,currencytype,gameType,datefrom,dateto,testChecked);

		}
		else if (btnname=="Yesterday")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			var dayL=theyday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theyday="0"+theyday;
			}
			var datefrom=(theyear+"-"+themonth+"-"+theyday + " 00:00:00");
			var dateto=(theyear+"-"+themonth+"-"+theyday+ " 23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}

			document.getElementById('qry_resultEfficia').innerHTML='';
			tableWinLossTodayEfficia(accountId,currencytype,gameType,datefrom,dateto,testChecked);
			document.getElementById('qry_resultEfficia1').innerHTML='';
			tableWinLossTodayEfficia1(accountId,currencytype,gameType,datefrom,dateto,testChecked);

		}
		else if (btnname=="Submit")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "-" + monthF + "-" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "-" + monthT + "-" +dayT);
			var dateSubmitFrom= (dateF+" "+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ " "+ document.getElementById('cbHourto').value +":59:59");
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			

			document.getElementById('qry_resultEfficia').innerHTML='';
			tableWinLossTodayEfficia(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
			document.getElementById('qry_resultEfficia1').innerHTML='';
			tableWinLossTodayEfficia1(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/3d-loader.gif");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
div.dateSummaryFooter { text-align: Left; }
tr.jqfoot td { background-color: #AD8533; color: #FFF; }
tr.footrow-ltr td{color:black;}
</style>

</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area" style="width: 500px;height: 210px">
	<div class="header" >Efficia Player Win/Loss</div>
	<form action="">
		<table style="background-color:transparent; width: 500px;">
		<tr>
			<td width="25%">ACCOUNT ID:</td>
			<td width="75%"><input type="text" id="txtAccountId" style="width: 150px;"></td>
		</tr>
		<tr>
			<td>GAME TYPE:</td>
			<td>
				<select id="gameType">
					<option value="All">All</option>
	  				<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. $row['game_name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>CURRENCY TYPE:</td>
			<td>
				<select id="currencyType" style="width: 99px" onChange="javascript:changeCurrency();">
					<option value="All">ALL</option>
  					<?php 
					$dataReader = CashPlayerCurrencyType::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . strtoupper($row['currency_name']) . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>&nbsp;&nbsp;&nbsp;
				<input type="checkbox" id="chkTest" ><label id="lblExcept">Except TEST currency</label>
			</td>
		</tr>
		<tr>
			<td>FROM:</td>
			<td>
			<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px;width:50px; background-color: transparent" disabled></td>
		</tr>
		<tr><td>TO:</td>
			<td>
			<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; width:50px; background-color: transparent" disabled="disabled" ondblclick="this.disabled=false;">
			</td>
		</tr>
		</table>
		<div align="center">
			<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btnclickMe">
			<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btnclickMe">
			<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btnclickMe">
			<input onclick="javascript:history.go(-1);" type="button" value="Back">
		</div>
		</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (document.getElementById('defaultTimeFrom').value==''){
		document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
		document.getElementById("dateto").value=(day + "-" + month + "-" + year);
		}else{
			var datefrom=enc(document.getElementById('defaultTimeFrom').value);
			var dateto=enc(document.getElementById('defaultTimeTo').value);
			document.getElementById("datefrom").value=datefrom;
			document.getElementById("dateto").value=dateto;
		}

		function changeCurrency(){
			var lblExcept = document.getElementById('lblExcept');
			var chkTest = document.getElementById('chkTest');
			if (document.getElementById('currencyType').value != "All"){
				chkTest.style.visibility = 'hidden';
				lblExcept.style.visibility = 'hidden';
				chkTest.checked=false;
			 }else{
				chkTest.style.visibility = 'visible';
				lblExcept.style.visibility = 'visible';
		     }
		}
		</script>
</div>

	<div id="qry_resultEfficia"></div>
	</br>
	<div id="qry_resultEfficia1"></div>
	</br>
	<!--  <div id="pager2"></div>-->
	<input type="hidden" id="defaultTimeFrom" value="<?php  if (isset($_GET['dateFrom'])){ echo $_GET['dateFrom'];}?>" > <input type="hidden" id="defaultTimeTo" value="<?php  if (isset($_GET['dateTo'])){ echo $_GET['dateTo'];}?>">


</body>
</html>
<?php }?>