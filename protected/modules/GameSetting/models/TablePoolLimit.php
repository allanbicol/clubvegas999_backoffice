<?php
/**
 * @todo bind tbl_pool_limit to TablePoolLimit CActiveRecord
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-02
 */
class TablePoolLimit extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_pool_limit';
	}
}