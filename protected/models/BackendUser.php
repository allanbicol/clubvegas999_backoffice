<?php
/**
 * @todo		Backend User Model
 * @copyright	CE
 * @author		Kimny MOUK
 * @since	 	2012-03-22
 * 
 */
class BackendUser 
{
    public static function getBackendUsers($username, $password, $remoteIP)
    {
        /*
        $connection = Yii::app()->db_cv999_fd_master;
        $command = $connection->createCommand("SELECT * FROM vwGetAllBackendUserAccount WHERE account_id=:account_id AND password=:password");
        $command->bindParam(":account_id", $username, PDO::PARAM_STR);
        $command->bindParam(":password", $password, PDO::PARAM_STR);
        $rows=$command->queryRow();
        return $rows;
        */
 
		$outputParam = '@po_authentication, @po_status, @po_id, @po_account_type, @po_level, @po_level_name, @po_max_deposit, @po_max_withdraw, @po_is_allowed, @po_successful';
        $command = Yii::app()->db->createCommand('CALL spBEGetAllBackendUserAccount(:pi_account_id, :pi_password, :pi_ip, ' . $outputParam . ')');
        $command->execute(array('pi_account_id' => $username, 'pi_password' => $password, 'pi_ip' => $remoteIP));
        return Yii::app()->db->createCommand('SELECT ' . $outputParam . ';')->queryAll();
    }

// 	public static function getIfAlreadyOnline($username)
//     {
//         $connection=Yii::app()->db_cv999_fd_master;
//         $command=$connection->createCommand("SELECT COUNT(account_id) AS users FROM tbl_user_login_session WHERE account_id=:account_id");
//         $command->bindParam(":account_id",$username,PDO::PARAM_STR);
//         $rows=$command->queryRow();
//         return $rows;
//     }

}

?>
