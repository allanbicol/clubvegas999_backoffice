<?php

class PoolLimit
{
	public function addNewPoolLimit()
	{
		$post=new TablePoolLimit();
		$post->currency_id=$_GET['currency_id'];
		$post->game_id=$_GET['game_id'];
		$post->amount=$_GET['amount'];
		$post->last_update=new CDbExpression('NOW()');
		$post->operator_id=Yii::app()->session['account_id'];
		$post->save();
		
		$rd=TablePoolLimit::model()->find(array('select'=>'id,last_update','order'=>'id DESC','limit'=>1));
		$timestamp = strtotime($rd['last_update']);
		$newdate = date("Y-m-d", $timestamp);
		$newtime= date("H:i:s", $timestamp);
 		$rd1=TableGame::model()->find('id=:id', array(':id'=>$_GET['game_id']));
		echo $rd['id'].";".$newdate . " " . $newtime . ";" . $rd1['id'] . ";" . $rd1['game_name'];
	}
	public function updatePoolLimit()
	{
		TablePoolLimit::model()->updateAll(array(
				'amount'=>$_GET['amount'],
				'last_update'=>new CDbExpression('NOW()'),
				'operator_id'=>Yii::app()->session['account_id'],
				),
				'id="'.$_GET['upool_id'].'"');
		
		$rd=TablePoolLimit::model()->find(array('select'=>'last_update','condition'=>'id=:id','params'=>array(':id'=>$_GET['upool_id']),));
		$timestamp = strtotime($rd['last_update']);
		$newdate = date("Y-m-d", $timestamp);
		$newtime= date("H:i:s", $timestamp);
		echo $newdate . ' ' . $newtime;
	}
}