<?php

class CashPlayerConsolidatedWinLoss
{

	public function getPlayerConsolidatedWinLoss($orderField, $sortType, $startIndex, $limit)
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw, 
							(SELECT IF(ISNULL(SUM(TTH.amount)),0,SUM(TTH.amount)) FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history WHERE account_type=1 ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history WHERE account_type='c' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)	
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name order by $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw, 
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total, 
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history as CH WHERE account_type=1 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history as CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name  order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and betdate between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name  order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}else{
				$command = $connection->createCommand("SELECT '',wc.account_id,currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name  order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0  AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name  order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0  AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name  order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountPlayerConsolidatedWinLoss()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(0) from (SELECT wc.account_id
							FROM ((SELECT 
							account_id FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id FROM tbl_htv_bet_slot_record_casino_history WHERE account_type=1 ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_slotsvegas_casino_history WHERE account_type='c' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)	
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id) as counter"  );
			}else{
				$command = $connection->createCommand("Select COUNT(0) FROM (SELECT wc.account_id
							FROM ((SELECT 
							account_id FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id FROM tbl_htv_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id FROM tbl_htv_bet_slot_record_casino_history as CH WHERE account_type=1 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_slotsvegas_casino_history as CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id) as counter"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("Select COUNT(0) FROM (SELECT wc.account_id
							FROM ((SELECT 
							account_id FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id) 
							UNION
							(SELECT account_id FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and betdate between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_slotvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION 
							(SELECT account_id FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id) as counter"  );
			}else{
				$command = $connection->createCommand("Select COUNT(0) FROM (SELECT wc.account_id
							FROM ((SELECT 
							account_id FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) 
							UNION
							(SELECT account_id FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_slotvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id) as counter"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("Select COUNT(0) FROM (SELECT wc.account_id
							FROM ((SELECT 
							account_id FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_slotvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id FROM tbl_cash_player_used_bonus WHERE used_bonus<>0  AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id) as counter"  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(0) FROM (SELECT wc.account_id
							FROM ((SELECT 
							account_id FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) 
							UNION
							(SELECT account_id FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id) as counter"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayerConsolidatedWinLossTotal()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
		
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT currency_name,COUNT(account_id) AS player_count,ROUND(SUM(total_stake),2) AS total_stake,ROUND(SUM(amount_wager),2) AS amount_wager,
							ROUND(AVG(avg_bet),2) AS avg_bet,ROUND(SUM(win_los),2) AS win_los,ROUND(SUM(commission),2) AS commission,ROUND(SUM(bonus_used),2) AS bonus,
							ROUND(SUM(amount_tips),2) AS amount_tips,ROUND(SUM(total_win_los),2) AS total_win_los,ROUND(SUM(p_l),2) AS p_l,(ROUND(SUM(p_l),2)/COUNT(account_id)) AS avg_player_value,
							(SELECT exchange_rate FROM tbl_currency WHERE tbl_currency.currency_name=consolidated_winloss.currency_name) AS rate
							FROM 
							((SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw, 
							(SELECT IF(ISNULL(SUM(TTH.amount)),0,SUM(TTH.amount)) FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history WHERE account_type=1 ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history WHERE account_type='c' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)	
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name)) 
							AS consolidated_winloss GROUP BY currency_name"  );
			}else{
				$command = $connection->createCommand("SELECT currency_name,COUNT(account_id) AS player_count,ROUND(SUM(total_stake),2) AS total_stake,ROUND(SUM(amount_wager),2) AS amount_wager,
							ROUND(AVG(avg_bet),2) AS avg_bet,ROUND(SUM(win_los),2) AS win_los,ROUND(SUM(commission),2) AS commission,ROUND(SUM(bonus_used),2) AS bonus,
							ROUND(SUM(amount_tips),2) AS amount_tips,ROUND(SUM(total_win_los),2) AS total_win_los,ROUND(SUM(p_l),2) AS p_l,(ROUND(SUM(p_l),2)/COUNT(account_id)) AS avg_player_value,
							(SELECT exchange_rate FROM tbl_currency WHERE tbl_currency.currency_name=consolidated_winloss.currency_name) AS rate
							FROM 
							((SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw, 
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total, 
							(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history as CH WHERE account_type=1 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history as CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name)) AS consolidated_winloss GROUP BY currency_name"  );
			}
		}
		elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT currency_name,COUNT(account_id) AS player_count,ROUND(SUM(total_stake),2) AS total_stake,ROUND(SUM(amount_wager),2) AS amount_wager,
							ROUND(AVG(avg_bet),2) AS avg_bet,ROUND(SUM(win_los),2) AS win_los,ROUND(SUM(commission),2) AS commission,ROUND(SUM(bonus_used),2) AS bonus,
							ROUND(SUM(amount_tips),2) AS amount_tips,ROUND(SUM(total_win_los),2) AS total_win_los,ROUND(SUM(p_l),2) AS p_l,(ROUND(SUM(p_l),2)/COUNT(account_id)) AS avg_player_value,
							(SELECT exchange_rate FROM tbl_currency WHERE tbl_currency.currency_name=consolidated_winloss.currency_name) AS rate
							FROM 
							((SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							ROUND(SUM(wc.bonus_used),2) AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and betdate between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name)) 
							AS consolidated_winloss GROUP BY currency_name"  );
			}else{
				$command = $connection->createCommand("SELECT currency_name,COUNT(account_id) AS player_count,ROUND(SUM(total_stake),2) AS total_stake,ROUND(SUM(amount_wager),2) AS amount_wager,
							ROUND(AVG(avg_bet),2) AS avg_bet,ROUND(SUM(win_los),2) AS win_los,ROUND(SUM(commission),2) AS commission,ROUND(SUM(bonus_used),2) AS bonus,
							ROUND(SUM(amount_tips),2) AS amount_tips,ROUND(SUM(total_win_los),2) AS total_win_los,ROUND(SUM(p_l),2) AS p_l,(ROUND(SUM(p_l),2)/COUNT(account_id)) AS avg_player_value,
							(SELECT exchange_rate FROM tbl_currency WHERE tbl_currency.currency_name=consolidated_winloss.currency_name) AS rate
							FROM 
							((SELECT '',wc.account_id,currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							ROUND(SUM(wc.bonus_used),2) AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name)) 
							AS consolidated_winloss GROUP BY currency_name"  );
			}
		}
		elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
		{
			$command = $connection->createCommand("SELECT currency_name,COUNT(account_id) AS player_count,ROUND(SUM(total_stake),2) AS total_stake,ROUND(SUM(amount_wager),2) AS amount_wager,
							ROUND(AVG(avg_bet),2) AS avg_bet,ROUND(SUM(win_los),2) AS win_los,ROUND(SUM(commission),2) AS commission,ROUND(SUM(bonus_used),2) AS bonus,
							ROUND(SUM(amount_tips),2) AS amount_tips,ROUND(SUM(total_win_los),2) AS total_win_los,ROUND(SUM(p_l),2) AS p_l,(ROUND(SUM(p_l),2)/COUNT(account_id)) AS avg_player_value,
							(SELECT exchange_rate FROM tbl_currency WHERE tbl_currency.currency_name=consolidated_winloss.currency_name) AS rate
							FROM 
							((SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							ROUND(SUM(wc.bonus_used),2) AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name)) 
							AS consolidated_winloss GROUP BY currency_name"  );
		}
		else{
			$command = $connection->createCommand("SELECT currency_name,COUNT(account_id) AS player_count,ROUND(SUM(total_stake),2) AS total_stake,ROUND(SUM(amount_wager),2) AS amount_wager,
							ROUND(AVG(avg_bet),2) AS avg_bet,ROUND(SUM(win_los),2) AS win_los,ROUND(SUM(commission),2) AS commission,ROUND(SUM(bonus_used),2) AS bonus,
							ROUND(SUM(amount_tips),2) AS amount_tips,ROUND(SUM(total_win_los),2) AS total_win_los,ROUND(SUM(p_l),2) AS p_l,(ROUND(SUM(p_l),2)/COUNT(account_id)) AS avg_player_value,
							(SELECT exchange_rate FROM tbl_currency WHERE tbl_currency.currency_name=consolidated_winloss.currency_name) AS rate
							FROM 
							((SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
							ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
							SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
							(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
							ROUND(SUM(wc.bonus_used),2) AS bonus_used
							FROM ((SELECT 
							account_id,currency_name,SUM(amount_wagers) AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
							SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
							0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
							IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
							UNION
							(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
							SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
							(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
							((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id) 
							UNION
							(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
							SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
							SUM(total_win_loss) AS total_win_los,
							(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
							FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
							(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
							FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
							UNION
							(SELECT account_id,currency,sum(bet_amount) AS total_stake,sum(bet_amount) as amount_wager,AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION
							(SELECT account_id,currency,sum(total_result_amount) AS total_stake,sum(total_wager_amount) as amount_wager,AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
							FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
							UNION 
							(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
							) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name)) 
							AS consolidated_winloss GROUP BY currency_name"  );
		}
		$rows = $command->query();
		return $rows;
	}
	
	
	public function getExportPlayerConsolidatedWinLoss()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['accountId']=="All"){
			$andAccountId='';
		}else{
			$andAccountId="and account_id="."'".htmlEntities($_GET['accountId'],ENT_QUOTES)."'";
		}
	
		if ($_GET['currencyId']=="All" && $_GET['game']=="All")
		{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
						ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
						SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw,
						(SELECT IF(ISNULL(SUM(TTH.amount)),0,SUM(TTH.amount)) FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
						(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
						FROM ((SELECT
						account_id,currency_name,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
						SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
						0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
						IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
						UNION
						(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
						(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
						((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
						SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
						SUM(total_win_loss) AS total_win_los,
						(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(bet_amount),AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_htv_bet_slot_record_casino_history WHERE account_type=1 ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(total_wager_amount),AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_slotsvegas_casino_history WHERE account_type='c' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
				) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name");
			}else{
			$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
						ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
						SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
						(SELECT IF(ISNULL(SUM(tcpub.used_bonus)),0,SUM(tcpub.used_bonus))FROM tbl_cash_player_used_bonus tcpub WHERE tcpub.account_id=wc.account_id AND tcpub.currency_name = wc.currency_name AND  tcpub.transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."') AS bonus_used
						FROM ((SELECT
						account_id,currency_name,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
						SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
						0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
						IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
						UNION
						(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
						(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
						((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
						SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
						SUM(total_win_loss) AS total_win_los,
						(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' ".$andAccountId." and  bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(bet_amount),AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_htv_bet_slot_record_casino_history as CH WHERE account_type=1 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' ".$andAccountId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(total_wager_amount),AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_slotsvegas_casino_history as CH WHERE account_type='c' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' ".$andAccountId." and  create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
						) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name "  );
				}
			}
	elseif ($_GET['currencyId']=="All" && $_GET['game']<>"All")
			{
			if ($_GET['test']==0){
				$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
						ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
						SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
						ROUND(SUM(wc.bonus_used),2) AS bonus_used
						FROM ((SELECT
						account_id,currency_name,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
						SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
						0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
						IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency_name,account_id )
						UNION
						(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
						(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
						((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
						SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
						SUM(total_win_loss) AS total_win_los,
						(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'   GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and betdate between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(bet_amount),AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(total_wager_amount),AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
						) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name  "  );
				}else{
				$command = $connection->createCommand("SELECT '',wc.account_id,currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
						ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
						SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
						ROUND(SUM(wc.bonus_used),2) AS bonus_used
						FROM ((SELECT
						account_id,currency_name,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
						SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
						0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
						IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_id='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
						UNION
						(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
						(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
						((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
						SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
						SUM(total_win_loss) AS total_win_los,
						(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and game_type='".$_GET['game']."' and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency_name<>'TEST' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(bet_amount),AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'TEST' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(total_wager_amount),AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId."  and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=CH.account_id)<>1 AND currency<>'FUN' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and (SELECT test_real_currency FROM tbl_cash_player TCP WHERE TCP.account_id=tbl_cash_player_used_bonus.account_id)<>1 AND currency_name<>'TEST' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
						) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name  "  );
				}
	
			}
			elseif ($_GET['currencyId']<>"All" && $_GET['game']=="All")
			{
				$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
						ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
						SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
						ROUND(SUM(wc.bonus_used),2) AS bonus_used
						FROM ((SELECT
						account_id,currency_name,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
						SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
						0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
						IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
						UNION
						(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
						(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
						((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
						SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
						SUM(total_win_loss) AS total_win_los,
						(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency_name='".$_GET['currencyId']."' and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(bet_amount),AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and currency='".$_GET['currencyId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(total_wager_amount),AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and currency_id='".$_GET['currencyId']."' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
				) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name "  );
			}
			else{
				$command = $connection->createCommand("SELECT '',wc.account_id,wc.currency_name,ROUND(SUM(wc.total_stake),2) AS total_stake,ROUND(SUM(wc.amount_wager),2) AS amount_wager,
						ROUND(AVG(wc.avg_bet),2) AS avg_bet,ROUND(SUM(wc.win_los),2) AS win_los,SUM(wc.commission) AS commission,tcp.bonus AS bonus,
						SUM(wc.amount_tips) AS amount_tips,ROUND(SUM(wc.total_win_los),2) AS total_win_los,ROUND(IF (SUM(wc.win_los)=0,0,(IF(SUM(wc.win_los)>0,((SUM(wc.total_win_los))-SUM(wc.bonus_used)),((SUM(wc.total_win_los))+(SUM(wc.bonus_used))))*-1)),2) AS p_l,(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 1 AND 2 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS deposit,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id BETWEEN 3 AND 4 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS withdraw ,
						(SELECT SUM(TTH.amount)FROM tbl_cash_player_trans_history TTH WHERE TTH.player_id=wc.account_id AND TTH.cash_player_trans_type_id=6 and submit_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."') AS bonus_total,
						ROUND(SUM(wc.bonus_used),2) AS bonus_used
						FROM ((SELECT
						account_id,currency_name,0 AS total_stake,SUM(amount_wagers) AS amount_wager,AVG(amount_wagers) AS avg_bet,
						SUM(amount_settlements - amount_wagers) AS win_los,SUM(commission) AS commission,
						0 AS bonus,IF (SUM(amount_tips)>0,CONCAT('-',SUM(amount_tips)),0) AS amount_tips,
						IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_costavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  GROUP BY currency_name,account_id )
						UNION
						(SELECT account_id,currency,SUM(bet_amount) AS total_stake,
						SUM(available_bet) AS amount_wager,AVG(available_bet) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(0) AS amount_tips,
						(SUM(win_loss)+SUM(commission)+SUM(0)) AS total_win_los,
						((SUM(win_loss)+SUM(commission)+SUM(0)))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_htv_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,SUM(stake_amount) AS total_stake,
						SUM(valid_stake) AS amount_wager,AVG(valid_stake) AS avg_bet,SUM(win_loss) AS win_los,SUM(commission) AS commission,SUM(0) AS bonus,SUM(tips) AS amount_tips,
						SUM(total_win_loss) AS total_win_los,
						(SUM(total_win_loss))*-1 AS p_l ,0 AS bonus_used
						FROM tbl_savan_casino_history_v2 AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."'  and end_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,SUM(amount_wagers),AVG(amount_wagers),SUM(amount_settlements - amount_wagers),SUM(commission),0,SUM(amount_tips),
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips),SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)) AS total_win_los,
						(IF(SUM(amount_settlements - amount_wagers)>0,(SUM(amount_settlements - amount_wagers)+SUM(commission))+SUM(amount_tips),(SUM(amount_settlements - amount_wagers)+SUM(commission))-SUM(amount_tips)))*-1 AS p_l,0 AS bonus_used
						FROM tbl_virtuavegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency_name='".$_GET['currencyId']."'  and bet_date between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency_name,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(bet_amount),AVG(bet_amount) AS avg_bet,sum(win_loss),sum(commission),0 AS bonus,0 AS amount_tips,sum(total_win_loss),(sum(total_win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_htv_bet_slot_record_casino_history AS CH WHERE account_type=1 ".$andAccountId." and  currency='".$_GET['currencyId']."'  and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency,0 AS total_stake,sum(total_wager_amount),AVG(total_wager_amount) AS avg_bet,sum(win_loss),0 as commission,0 AS bonus,0 AS amount_tips,sum(win_loss),(sum(_win_loss))*-1 AS p_l,0 AS bonus_used
						FROM tbl_slotsvegas_casino_history AS CH WHERE account_type='c' ".$andAccountId." and  currency='".$_GET['currencyId']."' and create_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' GROUP BY currency,account_id)
						UNION
						(SELECT account_id,currency_name,0,0,0,0,0,0,0,0,0,SUM(used_bonus) FROM tbl_cash_player_used_bonus WHERE used_bonus<>0 and  currency_id='".$_GET['currencyId']."' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' ".$andAccountId." GROUP BY account_id)
				) AS wc INNER JOIN tbl_cash_player tcp ON tcp.account_id=wc.account_id GROUP BY wc.account_id,wc.currency_name "  );
			}
			$rows = $command->query();
			return $rows;
	}
}