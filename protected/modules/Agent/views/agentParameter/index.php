<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentparameter.css" />
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.7.2/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
		//active menu color
		document.getElementById('agentHeader').className="start active";
		document.getElementById('mnu_agent_parameter').className="active";
		$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Agent Parameter</a></li>");
	</script>
	<script type="text/javascript">
		function trim(stringToTrim) {
			return stringToTrim.replace(/^\s+|\s+$/g,"");
		}
		function sNumber(text)
		{
			var pattern = /^-?[0-9]+(.[0-9]{1,2})?$/; 
		    if (text.match(pattern)==null) 
		    {
				return false;
		    }
			else
			{
				return true;
			}
		}
		function validate_form()
		{
			if(trim(document.getElementById('txtBaccaratMaxSharing').value)!=''){
				if(sNumber(trim(document.getElementById('txtBaccaratMaxSharing').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> baccarat <?php echo Yii::t('agent','agent.parameter.sharing');?>!';
					document.getElementById('txtBaccaratMaxSharing').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Baccarat <?php echo Yii::t('agent','agent.parameter.sharing');?> is <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtBaccaratMaxSharing').focus();
				return false;
			}

			if(trim(document.getElementById('txtBaccaratMaxCommission').value)!=''){
				if(sNumber(trim(document.getElementById('txtBaccaratMaxCommission').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> baccarat <?php echo Yii::t('agent','agent.parameter.commission');?>!';
					document.getElementById('txtBaccaratMaxCommission').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Baccarat <?php echo Yii::t('agent','agent.parameter.commission');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtBaccaratMaxCommission').focus();
				return false;
			}

			if(trim(document.getElementById('txtRouletteMaxSharing').value)!=''){
				if(sNumber(trim(document.getElementById('txtRouletteMaxSharing').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> European roulette <?php echo Yii::t('agent','agent.parameter.sharing');?>!';
					document.getElementById('txtRouletteMaxSharing').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='European Roulette <?php echo Yii::t('agent','agent.parameter.sharing');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtRouletteMaxSharing').focus();
				return false;
			}

			if(trim(document.getElementById('txtRouletteMaxCommission').value)!=''){
				if(sNumber(trim(document.getElementById('txtRouletteMaxCommission').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> European roulette <?php echo Yii::t('agent','agent.parameter.commission');?>!';
					document.getElementById('txtRouletteMaxCommission').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='European Roulette <?php echo Yii::t('agent','agent.parameter.commission');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtRouletteMaxCommission').focus();
				return false;
			}

			if(trim(document.getElementById('txtAmericanRouletteMaxSharing').value)!=''){
				if(sNumber(trim(document.getElementById('txtAmericanRouletteMaxSharing').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> American roulette <?php echo Yii::t('agent','agent.parameter.sharing');?>!';
					document.getElementById('txtAmericanRouletteMaxSharing').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='American Roulette <?php echo Yii::t('agent','agent.parameter.sharing');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtAmericanRouletteMaxSharing').focus();
				return false;
			}

			if(trim(document.getElementById('txtAmericanRouletteMaxCommission').value)!=''){
				if(sNumber(trim(document.getElementById('txtAmericanRouletteMaxCommission').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> American roulette <?php echo Yii::t('agent','agent.parameter.commission');?>!';
					document.getElementById('txtAmericanRouletteMaxCommission').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='American Roulette<?php echo Yii::t('agent','agent.parameter.commission');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtAmericanRouletteMaxCommission').focus();
				return false;
			}
			
			if(trim(document.getElementById('txtDragonTigerMaxSharing').value)!=''){
				if(sNumber(trim(document.getElementById('txtDragonTigerMaxSharing').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> dragon tiger <?php echo Yii::t('agent','agent.parameter.sharing');?>!';
					document.getElementById('txtDragonTigerMaxSharing').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Dragon tiger <?php echo Yii::t('agent','agent.parameter.sharing');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtDragonTigerMaxSharing').focus();
				return false;
			}
			
			if(trim(document.getElementById('txtDragonTigerMaxCommission').value)!=''){
				if(sNumber(trim(document.getElementById('txtDragonTigerMaxCommission').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> dragon tiger <?php echo Yii::t('agent','agent.parameter.commission');?>!';
					document.getElementById('txtDragonTigerMaxCommission').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Dragon tiger <?php echo Yii::t('agent','agent.parameter.commission');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtDragonTigerMaxCommission').focus();
				return false;
			}

			if(trim(document.getElementById('txtBlackjackMaxSharing').value)!=''){
				if(sNumber(trim(document.getElementById('txtBlackjackMaxSharing').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> blackjack <?php echo Yii::t('agent','agent.parameter.sharing');?>!';
					document.getElementById('txtBlackjackMaxSharing').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Blackjack <?php echo Yii::t('agent','agent.parameter.sharing');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtBlackjackMaxSharing').focus();
				return false;
			}
			
			if(trim(document.getElementById('txtBlackjackMaxCommission').value)!=''){
				if(sNumber(trim(document.getElementById('txtBlackjackMaxCommission').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> blackjack <?php echo Yii::t('agent','agent.parameter.commission');?>!';
					document.getElementById('txtblackjackMaxCommission').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Blackjack <?php echo Yii::t('agent','agent.parameter.commission');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtBlackjackMaxCommission').focus();
				return false;
			}
			if(trim(document.getElementById('txtSlotMaxSharing').value)!=''){
				if(sNumber(trim(document.getElementById('txtSlotMaxSharing').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> slot <?php echo Yii::t('agent','agent.parameter.sharing');?>!';
					document.getElementById('txtSlotMaxSharing').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Slot <?php echo Yii::t('agent','agent.parameter.sharing');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtSlotMaxSharing').focus();
				return false;
			}
			if(trim(document.getElementById('txtSlotMaxCommission').value)!=''){
				if(sNumber(trim(document.getElementById('txtSlotMaxCommission').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> slot <?php echo Yii::t('agent','agent.parameter.commission');?>!';
					document.getElementById('txtSlotMaxCommission').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='Slot <?php echo Yii::t('agent','agent.parameter.commission');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtSlotMaxCommission').focus();
				return false;
			}
			
			if(trim(document.getElementById('txtMaxWinMultiple').value)!=''){
				if(sNumber(trim(document.getElementById('txtMaxWinMultiple').value))==false){
					document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.invalid');?> <?php echo Yii::t('agent','agent.parameter.maxwin');?>!';
					document.getElementById('txtMaxWinMultiple').focus();
					return false;
				}else{
					document.getElementById('txtErrMsg').value='';
				}
			}else{
				document.getElementById('txtErrMsg').value='<?php echo Yii::t('agent','agent.parameter.maxwin');?> <?php echo Yii::t('agent','agent.parameter.required');?>!';
				document.getElementById('txtMaxWinMultiple').focus();
				return false;
			}
		}
		function btnReSet()
		{
			document.getElementById('txtBaccaratMaxSharing').value = document.getElementById('txtBaccaratMaxSharing').name;
			document.getElementById('txtBaccaratMaxCommission').value = document.getElementById('txtBaccaratMaxCommission').name;
			document.getElementById('txtRouletteMaxSharing').value = document.getElementById('txtRouletteMaxSharing').name;
			document.getElementById('txtRouletteMaxCommission').value = document.getElementById('txtRouletteMaxCommission').name;
			document.getElementById('txtAmericanRouletteMaxSharing').value = document.getElementById('txtAmericanRouletteMaxSharing').name;
			document.getElementById('txtAmericanRouletteMaxCommission').value = document.getElementById('txtAmericanRouletteMaxCommission').name;
			document.getElementById('txtDragonTigerMaxSharing').value = document.getElementById('txtDragonTigerMaxSharing').name;
			document.getElementById('txtDragonTigerMaxCommission').value = document.getElementById('txtDragonTigerMaxCommission').name;
			document.getElementById('txtBlackjackMaxSharing').value = document.getElementById('txtBlackjackMaxSharing').name;
			document.getElementById('txtBlackjackMaxCommission').value = document.getElementById('txtBlackjackMaxCommission').name;
			document.getElementById('txtSlotMaxSharing').value = document.getElementById('txtSlotMaxSharing').name;
			document.getElementById('txtSlotMaxCommission').value = document.getElementById('txtSlotMaxCommission').name;
			document.getElementById('txtMaxWinMultiple').value = document.getElementById('txtMaxWinMultiple').name;
		}
		function btnConfirm()
		{
			validate_form();
			if(validate_form()==false){return false;}
			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentParameter/SaveAgentParameter',
	    		method: 'POST',
	    		data: {'baccarat_max_sharing': trim(document.getElementById('txtBaccaratMaxSharing').value),
		    		'baccarat_max_commission': trim(document.getElementById('txtBaccaratMaxCommission').value),
		    		'roulette_max_sharing': trim(document.getElementById('txtRouletteMaxSharing').value),
		    		'roulette_max_commission': trim(document.getElementById('txtRouletteMaxCommission').value),
		    		'american_roulette_max_sharing': trim(document.getElementById('txtAmericanRouletteMaxSharing').value),
		    		'american_roulette_max_commission': trim(document.getElementById('txtAmericanRouletteMaxCommission').value),
		    		'dragon_tiger_max_sharing': trim(document.getElementById('txtDragonTigerMaxSharing').value),
		    		'dragon_tiger_max_commission': trim(document.getElementById('txtDragonTigerMaxCommission').value),
		    		'blackjack_max_sharing': trim(document.getElementById('txtBlackjackMaxSharing').value),
		    		'blackjack_max_commission': trim(document.getElementById('txtBlackjackMaxCommission').value),
		    		'slot_max_sharing': trim(document.getElementById('txtSlotMaxSharing').value),
		    		'slot_max_commission': trim(document.getElementById('txtSlotMaxCommission').value),
		    		'max_win_multiple': trim(document.getElementById('txtMaxWinMultiple').value),
		    		},
	    		success: function(data) {
	    			alert(data);
		    	}
	    	});
		}
	</script>
</head>
<body>
<b><?php echo Yii::t('agent','agent.parameter.title');?></b>
<div id="body_wrapper">
	<div id="title_header1"><?php echo Yii::t('agent','agent.parameter.subtitle1');?> </div>
	<div id="table_body">
		<table id="max_sharing_commission" border="0" cellpadding="1" cellspacing="0" width="100%">
		<tr>
			<td class="left"><b>Baccarat</b> <?php echo Yii::t('agent','agent.parameter.sharing');?></td>
			<td><input type="text" id="txtBaccaratMaxSharing" value="<?php echo $rd['baccarat_max_sharing']?>" name="<?php echo $rd['baccarat_max_sharing']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>Baccarat</b> <?php echo Yii::t('agent','agent.parameter.commission');?></td>
			<td><input type="text" id="txtBaccaratMaxCommission" value="<?php echo $rd['baccarat_max_commission']?>" name="<?php echo $rd['baccarat_max_commission']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>European Roulette</b> <?php echo Yii::t('agent','agent.parameter.sharing');?></td>
			<td><input type="text" id="txtRouletteMaxSharing" value="<?php echo $rd['roulette_max_sharing']?>" name="<?php echo $rd['roulette_max_sharing']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>European Roulette</b> <?php echo Yii::t('agent','agent.parameter.commission');?></td>
			<td><input type="text" id="txtRouletteMaxCommission" value="<?php echo $rd['roulette_max_commission']?>" name="<?php echo $rd['roulette_max_commission']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>American Roulette</b> <?php echo Yii::t('agent','agent.parameter.sharing');?></td>
			<td><input type="text" id="txtAmericanRouletteMaxSharing" value="<?php echo $rd['american_roulette_max_sharing']?>" name="<?php echo $rd['american_roulette_max_sharing']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>American Roulette</b> <?php echo Yii::t('agent','agent.parameter.commission');?></td>
			<td><input type="text" id="txtAmericanRouletteMaxCommission" value="<?php echo $rd['american_roulette_max_commission']?>" name="<?php echo $rd['american_roulette_max_commission']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>DragonTiger</b> <?php echo Yii::t('agent','agent.parameter.sharing');?></td>
			<td><input type="text" id="txtDragonTigerMaxSharing" value="<?php echo $rd['dragon_tiger_max_sharing']?>" name="<?php echo $rd['dragon_tiger_max_sharing']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>DragonTiger</b> <?php echo Yii::t('agent','agent.parameter.commission');?></td>
			<td><input type="text" id="txtDragonTigerMaxCommission" value="<?php echo $rd['dragon_tiger_max_commission']?>" name="<?php echo $rd['dragon_tiger_max_commission']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>Blackjack</b> <?php echo Yii::t('agent','agent.parameter.sharing');?></td>
			<td><input type="text" id="txtBlackjackMaxSharing" value="<?php echo $rd['blackjack_max_sharing']?>" name="<?php echo $rd['blackjack_max_sharing']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>Blackjack</b> <?php echo Yii::t('agent','agent.parameter.commission');?></td>
			<td><input type="text" id="txtBlackjackMaxCommission" value="<?php echo $rd['blackjack_max_commission']?>" name="<?php echo $rd['blackjack_max_commission']?>"/></td>
		</tr>
		
		
		<tr>
			<td class="left"><b>Slot</b> <?php echo Yii::t('agent','agent.parameter.sharing');?></td>
			<td><input type="text" id="txtSlotMaxSharing" value="<?php echo $rd['slot_max_sharing']?>" name="<?php echo $rd['slot_max_sharing']?>"/></td>
		</tr>
		<tr>
			<td class="left"><b>Slot</b> <?php echo Yii::t('agent','agent.parameter.commission');?></td>
			<td><input type="text" id="txtSlotMaxCommission" value="<?php echo $rd['slot_max_commission']?>" name="<?php echo $rd['slot_max_commission']?>"/></td>
		</tr>
		</table>
	</div>
	<div id="title_header2"><?php echo Yii::t('agent','agent.parameter.maxwin');?> </div>
	<div id="table_body">
		<table id="max_win_multiple" border="0" cellpadding="1" cellspacing="0" width="100%">
			<tr>
				<td class="left"><?php echo Yii::t('agent','agent.parameter.maxwin');?></td>
				<td><input type="text" id="txtMaxWinMultiple" value="<?php echo $rd['max_win_multiple']?>" name="<?php echo $rd['max_win_multiple']?>"/></td>
			</tr>
		</table>
	</div>
	<div id="title_footer">
		<?php 
			if(Yii::app()->user->checkAccess('agent.writeAgentParameters')){
		?>
		<button class="btn red" onclick="javascript: btnConfirm()"/><?php echo Yii::t('agent','agent.parameter.confirm');?></button>
		<?php }?> 
		<button class="btn red" onclick="javascript: btnReSet();"/><?php echo Yii::t('agent','agent.parameter.reset');?> <i class="icon-refresh"></i></button>
	</div>
	<input type="text" id="txtErrMsg" readonly="true"/>
</div>
</body>
</html>