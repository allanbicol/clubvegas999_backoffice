<?php

/**
 * This is the model class for table "tbl_cash_player_bonus".
 @Author: Allan Bicol
 @Since: 04/20/2012
 */



class CashPlayerSetBonus extends CActiveRecord
{
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**																		
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player_bonus';
	}
	
	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}
	
}

class CashPlayerSetBonusHistory extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player_balance_history';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}

class CashPlayerSetBonusPlayerUpdateBalance extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}

class CashPlayerSetBonusPlayerTransHistory extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player_trans_history';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}

