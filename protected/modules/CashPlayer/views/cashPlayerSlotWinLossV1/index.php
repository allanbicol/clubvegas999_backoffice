<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_slot_winloss_v1').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Cash Player Slot Win/Loss Version 1</a></li>");
</script>
<script type="text/javascript">
	function validate()
	{
		var pattern = /^-?[-\0-9]+(.[0-9]{1,2})?$/; 
		var text = document.getElementById('txtVIGSharing1').value;
	    if (text.match(pattern)==null) 
	    {
			alert('Invalid VIG sharing value.');
			return false;
	    }
		else
		{
		  return true;
		}
	}
	
	var cRowNo=0;
	//VIRTUAVEGAS999
	function tableWinLossTodayVirtua(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list7'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_virtua").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager7'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_virtua").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list7");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/CashPlayerSlotWinLossVirtua&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked,
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','Account ID', 'Account Name','Last Bet Date','Currency','Valid Stake','Average Bet','Win/Loss','Commission','Tips','<b>Total</b>','Balance','<b>P/L</b>'],
			    colModel: [
			        {name: 'no', index: 'no', width: 25,title:false,sortable:false},
					{name: 'account_id', index: 'account_id', width: 120, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
					{name: 'account_name', index: 'account_name', width: 120,title:false},
					{name: 'last_bet_date', index: 'last_bet_date', width: 130,title:false,hidden:true},
					{name: 'currency_name', index: 'currency_name', width: 55,title:false},
					{name: 'amount_wager', index: 'amount_wager', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_wager">{0}</label>'},
					{name: 'avg_bet', index: 'avg_bet', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="avg_bet">{0}</label>'},
					{name: 'win_los', index: 'win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="commission">{0}</label>'},
					{name: 'amount_tips', index: 'amount_tips', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="amount_tips">{0}</label>',hidden:true},
					{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'current_balance', index: 'current_balance', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="current_balance">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
				    ],

				loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list7");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailVirtua&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo)+"&id="+enc(document.getElementById('txtAccountId').value);
			                }

			                e.preventDefault();
			            });
			        } 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"amount_tips","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
				    	//valid_bet
				    	grid.jqGrid('setCell',i,"account_id","",{'text-decoration':'underline'});
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
					    }
				    	if(myrow.amount_tips == '-0.00'){
				    		grid.jqGrid('setCell',i,"amount_tips","0.00");	
					    }
				    	if(myrow.amount_tips < 0){
				    		grid.jqGrid('setCell',i,"amount_tips","",{color:'red'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
					    }
				    	
				    	 $(".dateSummaryFooter").each(function(i) {this.style.color='#000000';});
				    	 $(".amount_wager").each(function(i) {this.style.color='#000000';});
					     $(".avg_bet").each(function(i) {this.style.color='#000000';});
				    	 $(".win_los").each(function(i) {this.style.color='#000000';});
					     $(".commission").each(function(i) {this.style.color='#000000';});
					     $(".amount_tips").each(function(i) {this.style.color='#000000';});
					     $(".total_win_los").each(function(i) {this.style.color='#000000';});
					     $(".current_balance").each(function(i) {this.style.color='#000000';});
					     $(".p_l").each(function(i) {this.style.color='#000000';});//pl
						

					     $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//total_winloss
					     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#730000';}});//winloss
					     $(".p_l").each(function(i) {if ($(this).val().replace(",","") < 0){this.style.color='#730000';}});//pl
					    
					}
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");   
			    },
			   
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager7',
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">VIRTUA VEGAS </label> </label> Slot Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
				groupingView : {
			   		groupField : ['currency_name'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   	afterInsertRow : function(rowid, rowdata)
			    {
			        if (rowdata.p_l < 0)
			        {
			           $(this).jqGrid('setCell', rowid,5 ,'', {color:'#990000'});
			          // alert("sefsdfsD");
			        }

			    },
			});
			$('#list7').jqGrid('navGrid', '#pager7', {edit: false, add: false, del:false, search: false});

			jQuery("#list7").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_los', numberOfColumns: 4, titleText: '<label ><b>PLAYER</b></label>'},
				
				  ]
			});
			jQuery("#list7").jqGrid('navButtonAdd','#pager7',{
	            caption:"Export current page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportVirtuaVegasSlotWinLossToExcel();
	           }, 
	            position:"last"
	        });     
			jQuery("#list7").jqGrid('navButtonAdd','#pager7',{
	            caption:"Export all pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllVirtuaVegasSlotWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked);
	           }, 
	            position:"last"
	        }); 
		
		});
	} 
	function exportVirtuaVegasSlotWinLossToExcel(){
		var accountId = $('#txtAccountId')[0].value;
		var gameType = $('#gameType')[0].value;
		var currencyType = $('#currencyType')[0].value;
		testChecked=0;
		if (document.getElementById("chkTest").checked==true){
			 testChecked=1;
		}
		var testC	=	testChecked;
		var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
		var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';

		
		var table1= document.getElementById('qry_result_virtua');
		var table2= document.getElementById('qry_result_virtua_summary');
		var html1 = table1.outerHTML;
		var html2 = table2.outerHTML;
		document.forms[2].csvBuffer.value="";
		document.forms[2].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+accountId+'#'+gameType+'#'+currencyType+'#'+testC+'#'+dateFrom+'#'+dateTo;
		document.forms[2].csvBuffer.value=html1+''+html2 ;
	    document.forms[2].method='POST';
	    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/VirtuaVegasSlotWinLossExcel';  // send it to server which will open this contents in excel file
	    document.forms[2].target='_top';
	    document.forms[2].submit(); 
	}
	function exportAllVirtuaVegasSlotWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked){
	    var vigSharing=document.getElementById("txtVIGSharing1").value;
	    $.ajax({	
			 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/ExportAllCashPlayerSlotWinLossVirtua&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&vig='+vigSharing, 
			 async:true,
			 success: function(result) {
					var data =result.split("<BREAK>");
				    
				    var accountId = $('#txtAccountId')[0].value;
					var gameType = $('#gameType')[0].value;
					var currencyType = $('#currencyType')[0].value;
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					var testC	=	testChecked;
					var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
					var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';

					document.forms[2].csvBuffer.value="";
					document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+gameType+'#'+currencyType+'#'+testC+'#'+dateFrom+'#'+dateTo;
					document.forms[2].csvBuffer.value=data[0]+""+data[2];
				    document.forms[2].method='POST';
				    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/VirtuaVegasSlotWinLossExcel';  // send it to server which will open this contents in excel file
				    document.forms[2].target='_top';
				    document.forms[2].submit(); 
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown) { 
		 }});
	}
	// VIRTUAVEGAS ALL PAGES SUMMARY
	function tableWinLossTotalVirtua(id,currency_id,gameType,dateFrom,dateTo,testChecked,vigSharing) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list8'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_virtua_summary").appendChild(divTag);
	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager8'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_virtua_summary").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list8");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/CashPlayerSlotWinLossVirtuaTotal&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked+'&vig='+vigSharing, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Currency','Valid Stake','Win/Loss','Commission','Tips','<b>Total</b>','<b><span style="color:green">Win/Loss</span><b>','<b><span style="color:green">Tips</span></b>','<b><span style="color:green">Total</span></b>','<b>P/L</b>','Ex-Rate','CU Valid Stake','CU WinLoss','CU Commission','CU Tips','CU Total','CU VIG WinLoss','CU VIG Tips','CU VIG Total','CU P/L'],
			    colModel: [
					{name: 'currency_name', index: 'currency_name', width: 60,title:false},
					{name: 'amount_wager', index: 'amount_wager', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 120, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'amount_tips', index: 'amount_tips', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'total_win_los', index: 'total_win_los', width: 120, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'vig_winloss', index: 'vig_winloss', width: 120, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},			
					{name: 'vig_tips', index: 'vig_tips', width: 90, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
					{name: 'vig_total', index: 'vig_total', width: 120, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'rate', index: 'rate', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',hidden:true},
					{name: 'amount_wager1', index: 'amount_wager1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'win_los1', index: 'win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>',hidden:true},
					{name: 'commission1', index: 'commission1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'amount_tips1', index: 'amount_tips1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'total_win_los1', index: 'total_win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>',hidden:true},
					{name: 'vig_winloss1', index: 'vig_winloss1', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
					{name: 'vig_tips1', index: 'vig_tips1', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
					{name: 'vig_total1', index: 'vig_total1', width: 110, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
					{name: 'p_l1', index: 'p_l1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
			    ],
				loadtext:"",
				loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"currency_name","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_wager","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"amount_tips","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"vig_winloss","",{background:'#002E00',color:'white'});
				    	grid.jqGrid('setCell',i,"vig_tips","",{background:'#002E00',color:'white'});
				    	grid.jqGrid('setCell',i,"vig_total","",{background:'#002E00',color:'white'});
				    	grid.jqGrid('setCell',i,"p_l","",{background:'#D84A38',color:'white'});
	
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'yellow'});	
					    }
				    	if(myrow.total_win_los == '-0.00'){
				    		grid.jqGrid('setCell',i,"total_win_los","0.00");	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'yellow'});	
					    }
				    	if(myrow.vig_winloss < 0){
				    		grid.jqGrid('setCell',i,"vig_winloss","",{color:'yellow'});	
					    }
				    	if(myrow.vig_total < 0){
				    		grid.jqGrid('setCell',i,"vig_total","",{color:'yellow'});	
					    }
				    	if(myrow.amount_tips < 0){
				    		grid.jqGrid('setCell',i,"amount_tips","",{color:'yellow'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'yellow'});	
					    }			    
					}   
			    },
			    hidegrid: false,  
			    rowNum: 25,	
			    rowList: [25, 50, 75],
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">VIRTUA VEGAS </label> Slot Win/Loss Summary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convert this to </label><select id="currencyTypeConvert" style="width:80px" onchange="javascript: convert();"><option value="0-0">--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>&nbsp;&nbsp;VIG SHARING<input type="text" id="txtVIGSharing1" style="width: 50px;" onkeydown="if (event.keyCode == 13){computeVIGSharing(); }" >%',
			    viewrecords: true,
			    footerrow:true,
			    
			});
			$('#list8').jqGrid('navGrid', '#pager8', {edit: false, add: false, del:false, search: false});

			jQuery("#list8").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'amount_wager', numberOfColumns: 5, titleText: '<label ><b>PLAYER</b></label>'},
					{startColumnName: 'vig_winloss', numberOfColumns: 3, titleText: '<label ><b>VIG</b></label>'}
				  ]
			});
		});
	} 
	function computeVIGSharing(){
		validate();
		testChecked=0;
		if (document.getElementById("chkTest").checked==true){
			 testChecked=1;
		}

		var dateFrom = (document.getElementById('datefrom').value).split("-");
		
		var monthF = dateFrom[1];
		var dayF = dateFrom[2];
		var yearF = dateFrom[0];
		var dateF=(yearF + "/" + monthF + "/" + dayF);
		var dateTo =(document.getElementById('dateto').value).split("-");
		var monthT = dateTo[1];
		var dayT = dateTo[2];
		var yearT = dateTo[0];
		var dateT= (yearT + "/" + monthT + "/" +dayT);
		var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
		var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
		var gameType=document.getElementById('gameType').value;
		var currencytype=document.getElementById('currencyType').value;
		if (document.getElementById('txtAccountId').value==""){
			accountId="All";
		}else{
			accountId=document.getElementById('txtAccountId').value;
		}
		var vigSharing=document.getElementById("txtVIGSharing1").value;
		if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
			alert('Invalid VIG sharing value.');
			return false;
		}
		document.getElementById('qry_result_virtua_summary').innerHTML='';
		tableWinLossTotalVirtua(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked,vigSharing);
		document.getElementById("txtVIGSharing1").value=vigSharing;
	}
	
	function convert(){
    	var comboValue=document.getElementById("currencyTypeConvert").value;
    	var vigSharing=document.getElementById("txtVIGSharing1").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	
		
		if (comboRange==0){
   		 	$('#list8').trigger("reloadGrid");
   			$('#list8').jqGrid('footerData','set', {amount_wager:'',win_los:'',commission:'',total_win_los:'', amount_tips:'',vig_winloss:'',vig_tips:'',vig_total:'',p_l:''});  
   		 return false;
       	}
	       
		var noRow=$("#list8").getGridParam("reccount");
		var rate;
		var stake;
		var winloss;
		var commission;
		var total;
		var tips;
		var vigWinLoss;
		var vigTips;
		var vigTotal;
		var pl;
		var stakeConversion;
		var totalConversion;
		var winlossConversion;
		var commissionConversion;
		var tipsConversion;
		var vigWinLossConversion;
		var vigTipsConversion;
		var vigTotalConversion;
		var plConversion;

	    var x=1;
	    while (x<=noRow)
	      {
	    	rate=$("#list8").getCell(x, 'rate');
	    	stake=$("#list8").getCell(x, 'amount_wager1');
	    	winloss=$("#list8").getCell(x, 'win_los1');
	    	commission=$("#list8").getCell(x, 'commission1');
			total=$("#list8").getCell(x, 'total_win_los1');
			tips=$("#list8").getCell(x, 'amount_tips1');
			vigWinLoss=$("#list8").getCell(x, 'vig_winloss1');
			vigTips=$("#list8").getCell(x, 'vig_tips1');
			vigTotal=$("#list8").getCell(x, 'vig_total1');
			pl=$("#list8").getCell(x, 'p_l1');

				stakeConversion=((parseFloat(stake)/parseFloat(rate)) * parseFloat(comboRange));
	    		winlossConversion=((parseFloat(winloss)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	commissionConversion=((parseFloat(commission)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		tipsConversion=((parseFloat(tips)/parseFloat(rate)) * parseFloat(comboRange));
	    		vigWinLossConversion=((parseFloat(vigWinLoss)/parseFloat(rate)) * parseFloat(comboRange));
	    		vigTipsConversion=((parseFloat(vigTips)/parseFloat(rate)) * parseFloat(comboRange));
	    		vigTotalConversion=((parseFloat(vigTotal)/parseFloat(rate)) * parseFloat(comboRange));
	    		plConversion=((parseFloat(pl)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list8").jqGrid('setCell', x, 0, comboName);
	    		jQuery("#list8").jqGrid('setCell', x, 1, stakeConversion);
	    		jQuery("#list8").jqGrid('setCell', x, 2, winlossConversion);
	    		jQuery("#list8").jqGrid('setCell', x, 3, commissionConversion);
	    	    jQuery("#list8").jqGrid('setCell', x, 4, tipsConversion);
	    		jQuery("#list8").jqGrid('setCell', x, 5, totalConversion);	
	    		jQuery("#list8").jqGrid('setCell', x, 6, vigWinLossConversion);
	    	    jQuery("#list8").jqGrid('setCell', x, 7, vigTipsConversion);
	    		jQuery("#list8").jqGrid('setCell', x, 8, vigTotalConversion);	
	    		jQuery("#list8").jqGrid('setCell', x, 9, plConversion);	
			
	     	x++;
	      }
	    var grid = $("#list8"),
        sumStake= grid.jqGrid('getCol', 'amount_wager', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_los', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');
        sumTips=grid.jqGrid('getCol', 'amount_tips', false, 'sum');
        sumVIGWinLoss=grid.jqGrid('getCol', 'vig_winloss', false, 'sum');
        sumVIGTips=grid.jqGrid('getCol', 'vig_tips', false, 'sum');
        sumVIGTotal=grid.jqGrid('getCol', 'vig_total', false, 'sum');
        sumPl=grid.jqGrid('getCol', 'p_l', false, 'sum');
        if (sumTotal >=0){
        	sumVIGWinLoss=0;
        	sumVIGTotal=sumVIGWinLoss+sumVIGTips;
        }else{
        	sumVIGWinLoss=sumWinLoss*-(vigSharing/100);
        	sumVIGTotal=sumVIGWinLoss+sumVIGTips;
        }
        sumPl=(sumTotal+sumVIGTotal)*-1;
        $("tr.jqgrow","#list8").css({display:"none"});
        grid.jqGrid('footerData','set', {currency_name:comboName,amount_wager:sumStake,win_los:sumWinLoss,commission:sumCommission,total_win_los:sumTotal, amount_tips: sumTips,vig_winloss:sumVIGWinLoss,vig_tips:sumVIGTips,vig_total:sumVIGTotal,p_l:sumPl});  
	}

	
	function tableWinLossTodayHTV(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list2'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultHTV").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager2'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultHTV").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list2");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/CashPlayerSlotWinLossHTV&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','Account ID', 'Account Name','Currency','Bet Count','Total Stake','Average Bet','Win/Loss','Commission','Jackpot','<b>Total</b>','Balance','<b>P/L</b>'],
			    colModel: [
					{name: 'no', index: 'no', width: 25,title:false},
					{name: 'account_id', index: 'account_id', width: 115, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
					{name: 'account_name', index: 'account_name', width: 115,title:false},
					{name: 'currency', index: 'currency', width: 55,title:false},
					{name: 'bet_count', index: 'bet_count', width: 60, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_stake', index: 'total_stake', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'avg_bet', index: 'avg_bet', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'jackpot', index: 'jackpot', width: 75, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'current_balance', index: 'current_balance', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'p_l', index: 'p_l', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
			    ],
			    
				loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list2");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailHTV&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo);

				                }

			                e.preventDefault();
			            });
			            //myGrid.jqGrid('setCell', i,'account_id' , {color:'red'});
			          
			        }
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"jackpot","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
				    	grid.jqGrid('setCell',i,"account_id","",{'text-decoration':'underline'});
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
					    }
				    	 $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#f25757';}});//total_winloss
					     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#f25757';}});//winloss
					     $(".p_l").each(function(i) {if ($(this).val().replace(",","") < 0){this.style.color='#f25757';}});//pl
					} 
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			   
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager2',
			    sortname: 'currency',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">HTV</label> Slot Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
				groupingView : {
			   		groupField : ['currency'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   
			   	
			});
			$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search: false});

			jQuery("#list2").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_los', numberOfColumns: 4, titleText: '<label ><b>PLAYER</b></label>'},
				
				  ]
			});
			jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
	            caption:"Export to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportHTVSlotWinLossToExcel();
	           }, 
	            position:"last"
	        });     
			
		});
	} 
	function exportHTVSlotWinLossToExcel(){
		var accountId = $('#txtAccountId')[0].value;
		var gameType = $('#gameType')[0].value;
		var currencyType = $('#currencyType')[0].value;
		testChecked=0;
		if (document.getElementById("chkTest").checked==true){
			 testChecked=1;
		}
		var testC	=	testChecked;
		var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
		var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
		
		var table1= document.getElementById('qry_resultHTV');
		var table2= document.getElementById('qry_result_HTV_summary');
		var html1 = table1.outerHTML;
		var html2 = table2.outerHTML;
		document.forms[2].csvBuffer.value="";
		document.forms[2].txtParams.value=$('.ui-paging-info')[1].innerHTML+'#'+accountId+'#'+gameType+'#'+currencyType+'#'+testC+'#'+dateFrom+'#'+dateTo;
		document.forms[2].csvBuffer.value=html1+''+html2 ;
	    document.forms[2].method='POST';
	    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/HTVSlotWinLossExcel';  // send it to server which will open this contents in excel file
	    document.forms[2].target='_top';
	    document.forms[2].submit(); 
	}
	// HTV ALL PAGES SUMMARY
	function tableWinLossHTVTotal(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list4'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_HTV_summary").appendChild(divTag);
	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager4'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_HTV_summary").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list4");
			grid.jqGrid({ 
				url:'',//<?php //echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLoss/CashPlayerSlotWinLossHTVTotal&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Currency','Bet Count','Total Stake','Win/Loss','Commission','Jackpot','<b>Total</b>','<b>P/L</b>','Ex-Rate','CU Total Stake','CU WinLoss','CU Commission','CU Jackpot','CU Total','CU P/L'],
			    colModel: [
					{name: 'currency', index: 'currency', width: 60,title:false},
					{name: 'bet_count', index: 'bet_count', width: 70, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_stake', index: 'total_stake', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'jackpot', index: 'jackpot', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_win_los', index: 'total_win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'rate', index: 'rate', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','},hidden:true},
					{name: 'total_stake1', index: 'total_stake1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'win_los1', index: 'win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>',hidden:true},
					{name: 'commission1', index: 'commission1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'jackpot1', index: 'jackpot1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'total_win_los1', index: 'total_win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>',hidden:true},
					{name: 'p_l1', index: 'p_l1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
				    ],

				loadtext:"",
				loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"currency","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bet_count","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_stake","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"jackpot","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"p_l","",{background:'#D84A38',color:'white'});
				    	
						
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'yellow'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'yellow'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'yellow'});	
					    }			    
					}   
			    },
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">HTV </label>Slot Win/Loss Summary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convert this to </label><select id="currencyTypeConvertHTV" style="width:80px" onchange="javascript: convertHTV();"><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>',
			    viewrecords: true,
			    footerrow:true,
			    
			});
			$('#list4').jqGrid('navGrid', '#pager4', {edit: false, add: false, del:false, search: false});

			
		});
	} 

	function convertHTV(){
    	var comboValue=document.getElementById("currencyTypeConvertHTV").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	
		
    
		var noRow=$("#list4").getGridParam("reccount");
		var rate;
		var totalStake;
		var winloss;
		var commission;
		var total;
		var jackpot;
		var pl;
		var totalStakeconversion;
		var totalConversion;
		var winlossConversion;
		var commissionConversion;
		var jackpotConversion;
		var plConversion;

	    var x=1;
	    while (x<=noRow)
	      {
	    	rate=$("#list4").getCell(x, 'rate');
	    	totalStake=$("#list4").getCell(x, 'total_stake1');
	    	winloss=$("#list4").getCell(x, 'win_los1');
	    	commission=$("#list4").getCell(x, 'commission1');
			total=$("#list4").getCell(x, 'total_win_los1');
			jackpot=$("#list4").getCell(x, 'jackpot1');
			pl=$("#list4").getCell(x, 'p_l1');
				totalStakeconversion=((parseFloat(totalStake)/parseFloat(rate)) * parseFloat(comboRange));
	    		winlossConversion=((parseFloat(winloss)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	commissionConversion=((parseFloat(commission)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		jackpotConversion=((parseFloat(jackpot)/parseFloat(rate)) * parseFloat(comboRange));
	    		plConversion=((parseFloat(pl)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list4").jqGrid('setCell', x, 0, comboName);
	    		jQuery("#list4").jqGrid('setCell', x, 2, totalStakeconversion);
	    		jQuery("#list4").jqGrid('setCell', x, 3, winlossConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 4, commissionConversion);
	    	    jQuery("#list4").jqGrid('setCell', x, 5, jackpotConversion);
	    		jQuery("#list4").jqGrid('setCell', x, 6, totalConversion);	
	    		jQuery("#list4").jqGrid('setCell', x, 7, plConversion);	
			
	     	x++;
	      }
	    var grid = $("#list4"),
	    sumTotalStake=grid.jqGrid('getCol', 'total_stake', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_los', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');
        sumJackpot=grid.jqGrid('getCol', 'jackpot', false, 'sum');
        sumPl=grid.jqGrid('getCol', 'p_l', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake:sumTotalStake,win_los:sumWinLoss,commission:sumCommission,total_win_los:sumTotal, jackpot: sumJackpot,p_l:sumPl});  
	}

	//SAVANVEGAS WINLOSS
		function tableWinLossTodaySAVAN(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list5'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_resultSAVAN").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager5'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_resultSAVAN").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list5");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/CashPlayerSlotWinLossSAVAN&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['','Account ID', 'Account Name','Currency','Bet Count','Total Stake','Average Bet','Win/Loss','Commission','Jackpot','<b>Total</b>','Balance','<b>P/L</b>'],
			    colModel: [
					{name: 'no', index: 'no', width: 25,title:false},
					{name: 'account_id', index: 'account_id', width: 115, search:true,title:false,summaryType:'count', summaryTpl : '<div class=\"dateSummaryFooter\">Total<\/div>',formatter:'showlink',formatoptions:{baseLinkUrl:'#'}},
					{name: 'account_name', index: 'account_name', width: 115,title:false},
					{name: 'currency', index: 'currency', width: 55,title:false},
					{name: 'bet_count', index: 'bet_count', width: 60, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_stake', index: 'total_stake', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'avg_bet', index: 'avg_bet', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 80, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'jackpot', index: 'jackpot', width: 75, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'current_balance', index: 'current_balance', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'p_l', index: 'p_l', width: 100, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					],
			    
				loadtext:"",
			    loadComplete: function() {
			        var myGrid = $("#list5");
			        var ids = myGrid.getDataIDs();
			        for (var i = 0, idCount = ids.length; i < idCount; i++) {
			            $("#"+ids[i]+" a",myGrid[0]).click(function(e) {
			                var hash=e.currentTarget.hash;// string like "#?id=0"
			                if (hash.substring(0,5) === '#?id=') {
			                    var id = hash.substring(5,hash.length);
			                    var text = this.textContent || this.innerText;
			                    location.href="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossDetailSavan&account_id="+text+"&f="+enc(dateFrom)+"&t="+enc(dateTo);

				                }

			                e.preventDefault();
			            });
			            //myGrid.jqGrid('setCell', i,'account_id' , {color:'red'});
			          
			        }
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"no",i,{background:'#E6E6E6'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"jackpot","",{background:'#f7f7c8'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#cfe5fa'});
				    	grid.jqGrid('setCell',i,"account_id","",{'text-decoration':'underline'});
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'red'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'red'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'red'});	
					    }
				    	 $(".total_win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#f25757';}});//total_winloss
					     $(".win_los").each(function(i) {if (this.innerHTML.replace(",","") < 0){this.style.color='#f25757';}});//winloss
					     $(".p_l").each(function(i) {if ($(this).val().replace(",","") < 0){this.style.color='#f25757';}});//pl
					} 
				    $("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			   
			    hidegrid: false,  
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager5',
			    sortname: 'currency',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">SAVAN VEGAS</label> Slot Win/Loss - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    grouping: true,
				groupingView : {
			   		groupField : ['currency'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
					groupDataSorted : true
			   	},
			   
			   	
			});
			$('#list5').jqGrid('navGrid', '#pager5', {edit: false, add: false, del:false, search: false});

			jQuery("#list5").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'win_los', numberOfColumns: 4, titleText: '<label ><b>PLAYER</b></label>'},
				
				  ]
			});
			jQuery("#list5").jqGrid('navButtonAdd','#pager5',{
	            caption:"Export current page to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportSavanSlotWinLossToExcel();
	           }, 
	            position:"last"
	        });      
			jQuery("#list5").jqGrid('navButtonAdd','#pager5',{
	            caption:"Export all pages to Excel", 
	            buttonicon:"ui-icon-calculator", 
	            onClickButton: function(){ 
	            	exportAllSavanSlotWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked);
	           }, 
	            position:"last"
	        }); 
		});
	} 
	function exportSavanSlotWinLossToExcel(){
		var accountId = $('#txtAccountId')[0].value;
		var gameType = $('#gameType')[0].value;
		var currencyType = $('#currencyType')[0].value;
		testChecked=0;
		if (document.getElementById("chkTest").checked==true){
			 testChecked=1;
		}
		var testC	=	testChecked;
		var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
		var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
		
		var table1= document.getElementById('qry_resultSAVAN');
		var table2= document.getElementById('qry_result_SAVAN_summary');
		var html1 = table1.outerHTML;
		var html2 = table2.outerHTML;
		document.forms[2].csvBuffer.value="";
		document.forms[2].txtParams.value=$('.ui-paging-info')[2].innerHTML+'#'+accountId+'#'+gameType+'#'+currencyType+'#'+testC+'#'+dateFrom+'#'+dateTo;
		document.forms[2].csvBuffer.value=html1+''+html2 ;
	    document.forms[2].method='POST';
	    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/SavanSlotWinLossExcel';  // send it to server which will open this contents in excel file
	    document.forms[2].target='_top';
	    document.forms[2].submit(); 
	}
	function exportAllSavanSlotWinLossToExcel(id,currency_id,gameType,dateFrom,dateTo,testChecked){
	    $.ajax({	
			 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/ExportAllCashPlayerSlotWinLossSAVAN&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
			 async:true,
			 success: function(result) {
					var data =result.split("<BREAK>");
					
					var accountId = $('#txtAccountId')[0].value;
					var gameType = $('#gameType')[0].value;
					var currencyType = $('#currencyType')[0].value;
					testChecked=0;
					if (document.getElementById("chkTest").checked==true){
						 testChecked=1;
					}
					var testC	=	testChecked;
					var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
					var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';

					document.forms[2].csvBuffer.value="";
					document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+gameType+'#'+currencyType+'#'+testC+'#'+dateFrom+'#'+dateTo;
					document.forms[2].csvBuffer.value=data[0]+""+data[2];
				    document.forms[2].method='POST';
				    document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/SavanSlotWinLossExcel';  // send it to server which will open this contents in excel file
				    document.forms[2].target='_top';
				    document.forms[2].submit(); 
			 },
			 error: function(XMLHttpRequest, textStatus, errorThrown) { 
		 }});
	}
	// SAVAN ALL PAGES SUMMARY
	function tableWinLossSAVANTotal(id,currency_id,gameType,dateFrom,dateTo,testChecked) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list6'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_SAVAN_summary").appendChild(divTag);
	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager6'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_SAVAN_summary").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list6");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerSlotWinLossV1/CashPlayerSlotWinLossSAVANTotal&accountId='+id+'&currencyId='+currency_id+'&game='+gameType+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&test='+testChecked, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['Currency','Bet Count','Total Stake','Win/Loss','Commission','Jackpot','<b>Total</b>','<b>P/L</b>','Ex-Rate','CU Total Stake','CU WinLoss','CU Commission','CU Jackpot','CU Total','CU P/L'],
			    colModel: [
					{name: 'currency', index: 'currency', width: 60,title:false},
					{name: 'bet_count', index: 'bet_count', width: 70, align:"right",title:false,sorttype:'number',formatter:'integer',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_stake', index: 'total_stake', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'win_los', index: 'win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>'},
					{name: 'commission', index: 'commission', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'jackpot', index: 'jackpot', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum'},
					{name: 'total_win_los', index: 'total_win_los', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>'},
					{name: 'p_l', index: 'p_l', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>'},
					{name: 'rate', index: 'rate', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','},hidden:true},
					{name: 'total_stake1', index: 'total_stake1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'win_los1', index: 'win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="win_los">{0}</label>',hidden:true},
					{name: 'commission1', index: 'commission1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'jackpot1', index: 'jackpot1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',hidden:true},
					{name: 'total_win_los1', index: 'total_win_los1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="total_win_los">{0}</label>',hidden:true},
					{name: 'p_l1', index: 'p_l1', width: 130, align:"right",title:false,sorttype:'number',formatter:'currency',formatoptions: {thousandsSeparator:','}, summaryType:'sum',summaryTpl:'<label class="p_l">{0}</label>',hidden:true},
				    ],

				loadtext:"",
				loadComplete: function() { 
			        var i=0; 
				    for(i=0;i<=grid.getGridParam("reccount");i++)
				    {
				    	var myrow = grid.jqGrid('getRowData', i);
				    	grid.jqGrid('setCell',i,"currency","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"bet_count","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_stake","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"commission","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"jackpot","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"total_win_los","",{background:'#D84A38',color:'white'});
				    	grid.jqGrid('setCell',i,"p_l","",{background:'#D84A38',color:'white'});
				    	
						
				    	if(myrow.win_los < 0){
				    		grid.jqGrid('setCell',i,"win_los","",{color:'yellow'});	
					    }
				    	if(myrow.total_win_los < 0){
				    		grid.jqGrid('setCell',i,"total_win_los","",{color:'yellow'});	
					    }
				    	if(myrow.p_l < 0){
				    		grid.jqGrid('setCell',i,"p_l","",{color:'yellow'});	
					    }			    
					}   
			    },
			    hidegrid: false,  
			    rowNum: 25,	
			    rowList: [25, 50, 75],
			    sortname: 'currency_name',
			    sortorder: 'ASC',
			    caption: ' <label style="color:#D84A38">SAVAN VEGAS</label> Slot Win/Loss Summary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Convert this to </label><select id="currencyTypeConvertSAVAN" style="width:80px" onchange="javascript: convertSAVAN();"><option>--</option><?php $dataReader = TableCurrency::model()->findAll();foreach ($dataReader as $row){ echo '<option value="' .strtoupper($row['currency_name']).'-'.$row['exchange_rate'].' ">'. strtoupper($row['currency_name']) . '</option>';}?></select>',
			    viewrecords: true,
			    footerrow:true,
			    
			});
			$('#list6').jqGrid('navGrid', '#pager6', {edit: false, add: false, del:false, search: false});

			
		});
	} 
	
	function convertSAVAN(){
    	var comboValue=document.getElementById("currencyTypeConvertSAVAN").value;
		var comboSplit=comboValue.split("-");
		var comboName=comboSplit[0];	
		var comboRange=comboSplit[1];	
		
    
		var noRow=$("#list6").getGridParam("reccount");
		var rate;
		var totalStake;
		var winloss;
		var commission;
		var total;
		var jackpot;
		var pl;
		var totalStakeconversion;
		var totalConversion;
		var winlossConversion;
		var commissionConversion;
		var jackpotConversion;
		var plConversion;

	    var x=1;
	    while (x<=noRow)
	      {
	    	rate=$("#list6").getCell(x, 'rate');
	    	totalStake=$("#list6").getCell(x, 'total_stake1');
	    	winloss=$("#list6").getCell(x, 'win_los1');
	    	commission=$("#list6").getCell(x, 'commission1');
			total=$("#list6").getCell(x, 'total_win_los1');
			jackpot=$("#list6").getCell(x, 'jackpot1');
			pl=$("#list6").getCell(x, 'p_l1');
				totalStakeconversion=((parseFloat(totalStake)/parseFloat(rate)) * parseFloat(comboRange));
	    		winlossConversion=((parseFloat(winloss)/parseFloat(rate)) * parseFloat(comboRange));
	    	 	commissionConversion=((parseFloat(commission)/parseFloat(rate)) * parseFloat(comboRange));
	    		totalConversion=((parseFloat(total)/parseFloat(rate)) * parseFloat(comboRange));
	    		jackpotConversion=((parseFloat(jackpot)/parseFloat(rate)) * parseFloat(comboRange));
	    		plConversion=((parseFloat(pl)/parseFloat(rate)) * parseFloat(comboRange));
	    		
	    		jQuery("#list6").jqGrid('setCell', x, 0, comboName);
	    		jQuery("#list6").jqGrid('setCell', x, 2, totalStakeconversion);
	    		jQuery("#list6").jqGrid('setCell', x, 3, winlossConversion);
	    		jQuery("#list6").jqGrid('setCell', x, 4, commissionConversion);
	    	    jQuery("#list6").jqGrid('setCell', x, 5, jackpotConversion);
	    		jQuery("#list6").jqGrid('setCell', x, 6, totalConversion);	
	    		jQuery("#list6").jqGrid('setCell', x, 7, plConversion);	
			
	     	x++;
	      }
	    var grid = $("#list6"),
	    sumTotalStake=grid.jqGrid('getCol', 'total_stake', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_los', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');
        sumJackpot=grid.jqGrid('getCol', 'jackpot', false, 'sum');
        sumPl=grid.jqGrid('getCol', 'p_l', false, 'sum');
        grid.jqGrid('footerData','set', {total_stake:sumTotalStake,win_los:sumWinLoss,commission:sumCommission,total_win_los:sumTotal, jackpot: sumJackpot,p_l:sumPl});  
	}

</script>

<script type="text/javascript">
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}
var sChar=false;
function alphanumeric(inputtxt)  
{  
	var letters = /^[0-9a-zA-Z]+$/;  
	if(inputtxt.match(letters) || inputtxt=='')  
	{  
		sChar=false;  
	}else{  
		sChar=true;  
	}  
} 
</script>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});


	function btnclick(btnname)
	{
		
		var id='';
		if (btnname=="Load")
		{
			document.getElementById("chkTest").checked=true;
			
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			<?php if (isset($_GET['dateFrom'])==''){?>
				var mydate= new Date();
				mydate.setDate(mydate.getDate());
				var theyear=mydate.getFullYear();
				var themonth=mydate.getMonth()+1;
				var theday=mydate.getDate();
				
				var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
				var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
	
				var currentTime = new Date();
				var month = currentTime.getMonth() + 1;
				var day = currentTime.getDate();
				
				var year = currentTime.getFullYear();
			
				var dayL=day.toString();
				var monthL=month.toString();
	
				if (monthL.length==1){
					month="0"+month;
				}
				if (dayL.length==1){
					day="0"+day;
				}

				document.getElementById("datefrom").value=(year + "-" + month + "-" + day);
				document.getElementById("dateto").value=(year + "-" + month + "-" + day);
			<?php }else{?>
				var datefrom=enc('<?php echo $_GET['dateFrom'];?>');
				var dateto=enc('<?php echo $_GET['dateTo'];?>');
				var id=enc('<?php echo $_GET['id'];?>');
				var from=datefrom.split("_");
				var to=dateto.split("_");
				document.getElementById('datefrom').value=from[0].replace("/","-").replace("/","-");
				document.getElementById('dateto').value=to[0].replace("/","-").replace("/","-");
				document.getElementById('txtAccountId').value=id;
			<?php }?>
			
			var accountId=id;
			if (accountId==''){
				accountId='All';
			}else{
				accountId=id;
			}
				var vigSharing=document.getElementById("txtVIGSharing").value;
				if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
					alert('Invalid VIG sharing value.');
					return false;
				}
				
				<?php if(Yii::app()->user->checkAccess('cashPlayer.readVirtuaWinLoss')){?>
					document.getElementById('qry_result_virtua').innerHTML='';
					tableWinLossTodayVirtua(accountId,"All","All",datefrom,dateto,testChecked);
					document.getElementById('qry_result_virtua_summary').innerHTML='';
					tableWinLossTotalVirtua(accountId,"All","All",datefrom,dateto,testChecked,vigSharing);
				<?php }?>
				<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVSlotWinLoss')){?>
					document.getElementById('qry_resultHTV').innerHTML='';
					tableWinLossTodayHTV(accountId,"All","All",datefrom,dateto,testChecked);
					document.getElementById('qry_result_HTV_summary').innerHTML='';
					tableWinLossHTVTotal(accountId,"All","All",datefrom,dateto,testChecked);
				<?php }?>
				<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanSlotWinLoss')){?>
					document.getElementById('qry_resultSAVAN').innerHTML='';
					tableWinLossTodaySAVAN(accountId,"All","All",datefrom,dateto,testChecked);
					document.getElementById('qry_result_SAVAN_summary').innerHTML='';
					tableWinLossSAVANTotal(accountId,"All","All",datefrom,dateto,testChecked);

				<?php }?>
						
		}
		else if (btnname=="Today")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			var dayL=theday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theday="0"+theday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			var vigSharing=document.getElementById("txtVIGSharing").value;
			if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
				alert('Invalid VIG sharing value.');
				return false;
			}
			alphanumeric(accountId);
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readVirtuaWinLoss')){?>
				document.getElementById('qry_result_virtua').innerHTML='';
				tableWinLossTodayVirtua(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_virtua_summary').innerHTML='';
				tableWinLossTotalVirtua(accountId,currencytype,gameType,datefrom,dateto,testChecked,vigSharing);
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVSlotWinLoss')){?>
				document.getElementById('qry_resultHTV').innerHTML='';
				tableWinLossTodayHTV(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_HTV_summary').innerHTML='';
				tableWinLossHTVTotal(accountId,currencyType,gameType,datefrom,dateto,testChecked);
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanSlotWinLoss')){?>
				document.getElementById('qry_resultSAVAN').innerHTML='';
				tableWinLossTodaySAVAN(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_SAVAN_summary').innerHTML='';
				tableWinLossSAVANTotal(accountId,currencyType,gameType,datefrom,dateto,testChecked);

			<?php }?>
		}
		else if (btnname=="Yesterday")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}
			
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			var dayL=theyday.toString();
			var monthL=themonth.toString();
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theyday="0"+theyday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theyday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theyday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			var vigSharing=document.getElementById("txtVIGSharing").value;
			if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
				alert('Invalid VIG sharing value.');
				return false;
			}
			alphanumeric(accountId);
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readVirtuaWinLoss')){?>
			if (sChar==false){
				document.getElementById('qry_result_virtua').innerHTML='';
				tableWinLossTodayVirtua(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_virtua_summary').innerHTML='';
				tableWinLossTotalVirtua(accountId,currencytype,gameType,datefrom,dateto,testChecked,vigSharing);
			}
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVSlotWinLoss')){?>
			if (sChar==false){
				document.getElementById('qry_resultHTV').innerHTML='';
				tableWinLossTodayHTV(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_HTV_summary').innerHTML='';
				tableWinLossHTVTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked);
			}
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanSlotWinLoss')){?>
			if (sChar==false){
				document.getElementById('qry_resultSAVAN').innerHTML='';
				tableWinLossTodaySAVAN(accountId,currencytype,gameType,datefrom,dateto,testChecked);
				document.getElementById('qry_result_SAVAN_summary').innerHTML='';
				tableWinLossSAVANTotal(accountId,currencytype,gameType,datefrom,dateto,testChecked);

				
			}
			<?php }?>
		}
		else if (btnname=="Submit")
		{
			testChecked=0;
			if (document.getElementById("chkTest").checked==true){
				 testChecked=1;
			}

			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var gameType=document.getElementById('gameType').value;
			var currencytype=document.getElementById('currencyType').value;
			if (document.getElementById('txtAccountId').value==""){
				accountId="All";
			}else{
				accountId=document.getElementById('txtAccountId').value;
			}
			var vigSharing=document.getElementById("txtVIGSharing").value;
			if (parseFloat(vigSharing)<0 || parseFloat(vigSharing)>100){
				alert('Invalid VIG sharing value.');
				return false;
			}
			alphanumeric(accountId);
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readVirtuaWinLoss')){?>
			if (sChar==false){
				document.getElementById('qry_result_virtua').innerHTML='';
				tableWinLossTodayVirtua(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
				document.getElementById('qry_result_virtua_summary').innerHTML='';
				tableWinLossTotalVirtua(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked,vigSharing);
			}
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readHTVSlotWinLoss')){?>
			if (sChar==false){
				document.getElementById('qry_resultHTV').innerHTML='';
				tableWinLossTodayHTV(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
				document.getElementById('qry_result_HTV_summary').innerHTML='';
				tableWinLossHTVTotal(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
			}
			<?php }?>
			<?php if(Yii::app()->user->checkAccess('cashPlayer.readSavanSlotWinLoss')){?>
			if (sChar==false){
				document.getElementById('qry_resultSAVAN').innerHTML='';
				tableWinLossTodaySAVAN(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);
				document.getElementById('qry_result_SAVAN_summary').innerHTML='';
				tableWinLossSAVANTotal(accountId,currencytype,gameType,dateSubmitFrom,dateSubmitTo,testChecked);

			}
			<?php }?>
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
div.dateSummaryFooter { text-align: Left; }
tr.jqfoot td { background-color: #AD8533; color: #FFF; }
</style>

</head>
<body onload="javascript: btnclick('Load');">
<div id="parameter_area" style="width: 500px;height: 250px">
	<div class="header" >Member Slot Win/Loss</div>
	<form action="">
		<table style="background-color:transparent; width: 500px; margin-top: 5px;">
		<tr>
			<td width="25%" style="padding-left: 5px;">ACCOUNT ID:</td>
			<td width="75%"><input type="text" id="txtAccountId" style="width: 150px;" onkeydown="if (event.keyCode == 13){btnclick('Submit');}"></td>
		</tr>
		<tr hidden="true">
			<td width="25%" style="padding-left: 5px;">VIG SHARING:</td>
			<td width="75%"><input type="text" id="txtVIGSharing" style="width: 150px;" value="10"></td>
		</tr>
		<tr style="display: none;">
			<td style="padding-left: 5px;">GAME TYPE:</td>
			<td>
				<select id="gameType">
					<option value="All">All</option>
	  				<?php 
					$dataReader = TableGame::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . $row['id'] . '">'. $row['game_name'] . '</option>';
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">CURRENCY TYPE:</td>
			<td>
				<select id="currencyType" style="width: 99px" onChange="javascript:changeCurrency();">
					<option value="All">ALL</option>
  					<?php 
					$dataReader = CashPlayerCurrencyType::model()->findAll();
					foreach ($dataReader as $row){
						echo '<option value="' . strtoupper($row['currency_name']) . '">'. strtoupper($row['currency_name']) . '</option>';
					}
					?>
				</select>&nbsp;&nbsp;&nbsp;
				<input type="checkbox" id="chkTest" checked><label id="lblExcept">Except TEST currency</label>
			</td>
		</tr>
		<tr>
			<td style="padding-left: 5px;">FROM:</td>
			<td>
			<input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px;width:50px; background-color: transparent" disabled></td>
		</tr>
		<tr><td style="padding-left: 5px;">TO:</td>
			<td>
			<input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; width:50px; background-color: transparent" disabled="disabled" ondblclick="this.disabled=false;">
			</td>
		</tr>
		</table>
		<div align="center" style="margin-top: 5px;">
			<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
			<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
			<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
			<input onclick="javascript:history.go(-1);" type="button" value="Back" class="btn red">
		</div>
		</form>	
		<script type="text/javascript">
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		//if (document.getElementById('defaultTimeFrom').value==''){
		document.getElementById("datefrom").value=(day + "-" + month + "-" + year);
		document.getElementById("dateto").value=(day + "-" + month + "-" + year);
		//}else{
		//	var datefrom=enc(document.getElementById('defaultTimeFrom').value);
		//	var dateto=enc(document.getElementById('defaultTimeTo').value);
		//	document.getElementById("datefrom").value=datefrom;
		//	document.getElementById("dateto").value=dateto;
		//}

		function changeCurrency(){
			var lblExcept = document.getElementById('lblExcept');
			var chkTest = document.getElementById('chkTest');
			if (document.getElementById('currencyType').value != "All"){
				chkTest.style.visibility = 'hidden';
				lblExcept.style.visibility = 'hidden';
				chkTest.checked=false;
			 }else{
				chkTest.style.visibility = 'visible';
				lblExcept.style.visibility = 'visible';
		     }
		}
		</script>
</div>
	<div id="qry_result_virtua"></div>
	<br/>
	<div id="qry_result_virtua_summary"></div>
	<br/><br/><br/>	
	<div id="qry_resultHTV"></div>
	<br/>
	<div id="qry_result_HTV_summary"></div>
	<br/><br/><br/>
	<div id="qry_resultSAVAN"></div>
	<br/>
	<div id="qry_result_SAVAN_summary"></div>
	<br/>
	<!--  <div id="pager2"></div>
	<input type="hidden" id="defaultTimeFrom" value="<?php  if (isset($_GET['dateFrom'])){ echo $_GET['dateFrom'];}?>" > <input type="hidden" id="defaultTimeTo" value="<?php  if (isset($_GET['dateTo'])){ echo $_GET['dateTo'];}?>">
	-->
	<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
	</form>

</body>
</html>