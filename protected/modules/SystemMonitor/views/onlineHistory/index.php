<html>
<head>
	<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/onlinehistory.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/onlinehistory.js"></script>
	<script type="text/javascript">
		var urlGetOnlineHistory='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineHistory/GetOnlineHistory';
		var urlGetOnlineHistoryDetails='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/OnlineHistory/GetOnlineHistoryDetails';
		var dtToday='<?php echo date("Y-m-d");?>';
		var dtYesterday='<?php echo date("Y-m-d",strtotime('-1 day',strtotime(date("Y-m-d"))));?>';
	</script>
</head>
<body onload="javascript: btnToDay();">
	<div id="param_box">
		<div id="param_box_header">Online History</div>
		<div id="param_box_body">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="left">From:</td>
					<td class="right">
						<input type="text" class="txt" id="dtFrom">
						<select id="cmbTime1">
						<?php 
							for($i=0;$i<=23;$i++){
								echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
							}
						?>
						</select> :00:00
					</td>
				</tr>
				<tr>
					<td class="left">To:</td>
					<td class="right">
						<input type="text" class="txt" id="dtTo">
						<select id="cmbTime2">
						<?php 
							for($i=0;$i<=23;$i++){
								if($i<23){
									echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
								}else{
									echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
								}
							}
						?> 
						</select> :59:59
					</td>
				</tr>
				<tr><td class="left"></td>
					<td class="right">
						<input type="button" id="btnSubmit" value="Submit" onclick="javascript: btnSubmit();">
						<input type="button" id="btnToday" value="Today" onclick="javascript: btnToDay();">
						<input type="button" id="btnYesterday" value="Yesterday" onclick="javascript: btnYesterday();">
					</td>
				</tr>
			</table>
		</div>
	</div>
	
	<div id="qry_result"></div>
	<div id="qry_result_details"></div>
</body>
</html>