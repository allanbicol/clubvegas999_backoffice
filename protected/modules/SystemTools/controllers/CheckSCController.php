<?php
/**
 * @todo Login
 * @author Kimny MOUK
 * @since 2012-07-03
 * @last revision 2013-05-01
 */
class CheckSCController extends MyController
{
	public function actionIndex()
	{

		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * from tbl_agent where agent_type='sc' ORDER BY currency_id");
		$scList=$command->queryAll();
		foreach ($scList as $sc)
		{
			echo $sc['account_id'] . ": Currency: " . $sc['currency_id'] . "</br>";
		
				
			$command = $connection->createCommand("SELECT * from tbl_agent where agent_type='sma' and agent_parent_id='". $sc['account_id'] ."'");
			$smaList=$command->queryAll();
			/* Compare between subcompany's sharing/commission and senior master's one */  
			foreach ($smaList as $sma)
			{
				/* Compare sharing */
				//share_baccarat costa#htv#savan#virtua
				$sma_share_baccarat = explode("#", $sma['share_roulette']);
				$sc_share_baccarat = explode("#", $sc['share_roulette']);
				if($sma_share_baccarat[0] > $sc_share_baccarat[0]) // costa
					echo "------Error Sharing Baccarat----:" . $sma['account_id'] . "  :costa=". $sma_share_baccarat[0]. " > ". $sc['account_id']. ": " . $sc_share_baccarat[0]. " </br>";
				else if($sma_share_baccarat[1] > $sc_share_baccarat[1]) //htv
					echo "------Error Sharing Baccarat----:" . $sma['account_id'] . "  :htv=". $sma_share_baccarat[1]. " > ". $sc['account_id']. ": " . $sc_share_baccarat[1]. " </br>";
				else if($sma_share_baccarat[2] > $sc_share_baccarat[2]) //savan
					echo "------Error Sharing Baccarat----:" . $sma['account_id'] . "  :savan=". $sma_share_baccarat[2]. " > ". $sc['account_id']. ": " . $sc_share_baccarat[2]. " </br>";
				else if($sma_share_baccarat[3] > $sc_share_baccarat[3]) // virtua
					echo "------Error Sharing Baccarat----:" . $sma['account_id'] . "  :virtua=". $sma_share_baccarat[3]. " > ". $sc['account_id']. ": " . $sc_share_baccarat[3]. " </br>";
				
				//share_roulette costa#htv#savan#virtua
				$sma_share_roulette = explode("#", $sma['share_roulette']);
				$sc_share_roulette = explode("#", $sc['share_roulette']);
				if($sma_share_roulette[0] > $sc_share_roulette[0]) // costa
					echo "------Error Sharing Roulette----:" . $sma['account_id'] . "  :costa=". $sma_share_roulette[0]. " > ". $sc['account_id']. ": " . $sc_share_roulette[0]. " </br>";
				else if($sma_share_roulette[1] > $sc_share_roulette[1]) //htv
					echo "------Error Sharing Roulette----:" . $sma['account_id'] . "  :htv=". $sma_share_roulette[1]. " > ". $sc['account_id']. ": " . $sc_share_roulette[1]. " </br>";
				else if($sma_share_roulette[2] > $sc_share_roulette[2]) //savan
					echo "------Error Sharing Roulette----:" . $sma['account_id'] . "  :savan=". $sma_share_roulette[2]. " > ". $sc['account_id']. ": " . $sc_share_roulette[2]. " </br>";
				else if($sma_share_roulette[3] > $sc_share_roulette[3]) // virtua
					echo "------Error Sharing Roulette----:" . $sma['account_id'] . "  :virtua=". $sma_share_roulette[3]. " > ". $sc['account_id']. ": " . $sc_share_roulette[3]. " </br>";
				
				$sma_share_dragon_tiger = explode("#", $sma['share_dragon_tiger']);
				$sc_share_dragon_tiger = explode("#", $sc['share_dragon_tiger']);
				if($sma_share_dragon_tiger[0] > $sc_share_dragon_tiger[0]) // costa
					echo "------Error Sharing D/T----:" . $sma['account_id'] . "  :costa=". $sma_share_dragon_tiger[0]. " > ". $sc['account_id']. ": " . $sc_share_dragon_tiger[0]. " </br>";
				else if($sma_share_dragon_tiger[1] > $sc_share_dragon_tiger[1]) //htv
					echo "------Error Sharing D/T----:" . $sma['account_id'] . "  :htv=". $sma_share_dragon_tiger[1]. " > ". $sc['account_id']. ": " . $sc_share_dragon_tiger[1]. " </br>";
				else if($sma_share_dragon_tiger[2] > $sc_share_dragon_tiger[2]) //savan
					echo "------Error Sharing D/T----:" . $sma['account_id'] . "  :savan=". $sma_share_dragon_tiger[2]. " > ". $sc['account_id']. ": " . $sc_share_dragon_tiger[2]. " </br>";
				else if($sma_share_dragon_tiger[3] > $sc_share_dragon_tiger[3]) // virtua
					echo "------Error Sharing D/T----:" . $sma['account_id'] . "  :virtua=". $sma_share_dragon_tiger[3]. " > ". $sc['account_id']. ": " . $sc_share_dragon_tiger[3]. " </br>";
				
				$sma_share_blackjack = explode("#", $sma['share_blackjack']);
				$sc_share_blackjack = explode("#", $sc['share_blackjack']);
				if($sma_share_blackjack[0] > $sc_share_blackjack[0]) // costa
					echo "------Error Sharing Blackjack----:" . $sma['account_id'] . "  :costa=". $sma_share_blackjack[0]. " > ". $sc['account_id']. ": " . $sc_share_blackjack[0]. " </br>";
				else if($sma_share_blackjack[1] > $sc_share_blackjack[1]) //htv
					echo "------Error Sharing Blackjack----:" . $sma['account_id'] . "  :htv=". $sma_share_blackjack[1]. " > ". $sc['account_id']. ": " . $sc_share_blackjack[1]. " </br>";
				else if($sma_share_blackjack[2] > $sc_share_blackjack[2]) //savan
					echo "------Error Sharing Blackjack----:" . $sma['account_id'] . "  :savan=". $sma_share_blackjack[2]. " > ". $sc['account_id']. ": " . $sc_share_blackjack[2]. " </br>";
				else if($sma_share_blackjack[3] > $sc_share_blackjack[3]) // virtua
					echo "------Error Sharing Blackjack----:" . $sma['account_id'] . "  :virtua=". $sma_share_blackjack[3]. " > ". $sc['account_id']. ": " . $sc_share_blackjack[3]. " </br>";
				
				$sma_share_american_roulette = explode("#", $sma['share_american_roulette']);
				$sc_share_american_roulette = explode("#", $sc['share_american_roulette']);
				if($sma_share_american_roulette[0] > $sc_share_american_roulette[0]) // costa
					echo "------Error Sharing American Roulette----:" . $sma['account_id'] . "  :costa=". $sma_share_american_roulette[0]. " > ". $sc['account_id']. ": " . $sc_share_american_roulette[0]. " </br>";
				else if($sma_share_american_roulette[1] > $sc_share_american_roulette[1]) //htv
					echo "------Error Sharing American Roulette----:" . $sma['account_id'] . "  :htv=". $sma_share_american_roulette[1]. " > ". $sc['account_id']. ": " . $sc_share_american_roulette[1]. " </br>";
				else if($sma_share_american_roulette[2] > $sc_share_american_roulette[2]) //savan
					echo "------Error Sharing American Roulette----:" . $sma['account_id'] . "  :savan=". $sma_share_american_roulette[2]. " > ". $sc['account_id']. ": " . $sc_share_american_roulette[2]. " </br>";
				else if($sma_share_american_roulette[3] > $sc_share_american_roulette[3]) // virtua
					echo "------Error Sharing American Roulette----:" . $sma['account_id'] . "  :virtua=". $sma_share_american_roulette[3]. " > ". $sc['account_id']. ": " . $sc_share_american_roulette[3]. " </br>";
				
				$sma_share_slots = explode("#", $sma['share_slots']);
				$sc_share_slots = explode("#", $sc['share_slots']);
				if($sma_share_slots[3] > $sc_share_slots[3]) // costa
					echo "------Error Sharing Slot Virtua----:" . $sma['account_id'] . "  :costa=". $sma_share_slots[3]. " > ". $sc['account_id']. ": " . $sc_share_slots[3]. " </br>";
				
				/* Compare commission */
				
				$sma_commission_baccarat = explode("#", $sma['commission_baccarat']);
				$sc_commission_baccarat = explode("#", $sc['commission_baccarat']);
				if($sma_commission_baccarat[0] > $sc_commission_baccarat[0]) // costa
					echo "------Error Commission Baccarat----:" . $sma['account_id'] . "  :costa=". $sma_commission_baccarat[0]. " > ". $sc['account_id']. ": " . $sc_commission_baccarat[0]. " </br>";
				else if($sma_commission_baccarat[1] > $sc_commission_baccarat[1]) //htv
					echo "------Error Commission Baccarat----:" . $sma['account_id'] . "  :htv=". $sma_commission_baccarat[1]. " > ". $sc['account_id']. ": " . $sc_commission_baccarat[1]. " </br>";
				else if($sma_commission_baccarat[2] > $sc_commission_baccarat[2]) //savan
					echo "------Error Commission Baccarat----:" . $sma['account_id'] . "  :savan=". $sma_commission_baccarat[2]. " > ". $sc['account_id']. ": " . $sc_commission_baccarat[2]. " </br>";
				else if($sma_commission_baccarat[3] > $sc_commission_baccarat[3]) // virtua
					echo "------Error Commission Baccarat----:" . $sma['account_id'] . "  :virtua=". $sma_commission_baccarat[3]. " > ". $sc['account_id']. ": " . $sc_commission_baccarat[3]. " </br>";
				
				$sma_commission_roulette = explode("#", $sma['commission_roulette']);
				$sc_commission_roulette = explode("#", $sc['commission_roulette']);
				if($sma_commission_roulette[0] > $sc_commission_roulette[0]) // costa
					echo "------Error Commission Roulette----:" . $sma['account_id'] . "  :costa=". $sma_commission_roulette[0]. " > ". $sc['account_id']. ": " . $sc_commission_roulette[0]. " </br>";
				else if($sma_commission_roulette[1] > $sc_commission_roulette[1]) //htv
					echo "------Error Commission Roulette----:" . $sma['account_id'] . "  :htv=". $sma_commission_roulette[1]. " > ". $sc['account_id']. ": " . $sc_commission_roulette[1]. " </br>";
				else if($sma_commission_roulette[2] > $sc_commission_roulette[2]) //savan
					echo "------Error Commission Roulette----:" . $sma['account_id'] . "  :savan=". $sma_commission_roulette[2]. " > ". $sc['account_id']. ": " . $sc_commission_roulette[2]. " </br>";
				else if($sma_commission_roulette[3] > $sc_commission_roulette[3]) // virtua
					echo "------Error Commission Roulette----:" . $sma['account_id'] . "  :virtua=". $sma_commission_roulette[3]. " > ". $sc['account_id']. ": " . $sc_commission_roulette[3]. " </br>";
				
				$sma_commission_dragon_tiger = explode("#", $sma['commission_dragon_tiger']);
				$sc_commission_dragon_tiger = explode("#", $sc['commission_dragon_tiger']);
				if($sma_commission_dragon_tiger[0] > $sc_commission_dragon_tiger[0]) // costa
					echo "------Error Commission D/T----:" . $sma['account_id'] . "  :costa=". $sma_commission_dragon_tiger[0]. " > ". $sc['account_id']. ": " . $sc_commission_dragon_tiger[0]. " </br>";
				else if($sma_commission_dragon_tiger[1] > $sc_commission_dragon_tiger[1]) //htv
					echo "------Error Commission D/T----:" . $sma['account_id'] . "  :htv=". $sma_commission_dragon_tiger[1]. " > ". $sc['account_id']. ": " . $sc_commission_dragon_tiger[1]. " </br>";
				else if($sma_commission_dragon_tiger[2] > $sc_commission_dragon_tiger[2]) //savan
					echo "------Error Commission D/T----:" . $sma['account_id'] . "  :savan=". $sma_commission_dragon_tiger[2]. " > ". $sc['account_id']. ": " . $sc_commission_dragon_tiger[2]. " </br>";
				else if($sma_commission_dragon_tiger[3] > $sc_commission_dragon_tiger[3]) // virtua
					echo "------Error Commission D/T----:" . $sma['account_id'] . "  :virtua=". $sma_commission_dragon_tiger[3]. " > ". $sc['account_id']. ": " . $sc_commission_dragon_tiger[3]. " </br>";
				
				$sma_commission_blackjack = explode("#", $sma['commission_blackjack']);
				$sc_commission_blackjack = explode("#", $sc['commission_blackjack']);
				if($sma_commission_blackjack[0] > $sc_commission_blackjack[0]) // costa
					echo "------Error Commission Blackjack----:" . $sma['account_id'] . "  :costa=". $sma_commission_blackjack[0]. " > ". $sc['account_id']. ": " . $sc_commission_blackjack[0]. " </br>";
				else if($sma_commission_blackjack[1] > $sc_commission_blackjack[1]) //htv
					echo "------Error Commission Blackjack----:" . $sma['account_id'] . "  :htv=". $sma_commission_blackjack[1]. " > ". $sc['account_id']. ": " . $sc_commission_blackjack[1]. " </br>";
				else if($sma_commission_blackjack[2] > $sc_commission_blackjack[2]) //savan
					echo "------Error Commission Blackjack----:" . $sma['account_id'] . "  :savan=". $sma_commission_blackjack[2]. " > ". $sc['account_id']. ": " . $sc_commission_blackjack[2]. " </br>";
				else if($sma_commission_blackjack[3] > $sc_commission_blackjack[3]) // virtua
					echo "------Error Commission Blackjack----:" . $sma['account_id'] . "  :virtua=". $sma_commission_blackjack[3]. " > ". $sc['account_id']. ": " . $sc_commission_blackjack[3]. " </br>";
				
				$sma_commission_american_roulette = explode("#", $sma['commission_american_roulette']);
				$sc_commission_american_roulette = explode("#", $sc['commission_american_roulette']);
				if($sma_commission_american_roulette[0] > $sc_commission_american_roulette[0]) // costa
					echo "------Error Commission American Roulette----:" . $sma['account_id'] . "  :costa=". $sma_commission_american_roulette[0]. " > ". $sc['account_id']. ": " . $sc_commission_american_roulette[0]. " </br>";
				else if($sma_commission_american_roulette[1] > $sc_commission_american_roulette[1]) //htv
					echo "------Error Commission American Roulette----:" . $sma['account_id'] . "  :htv=". $sma_commission_american_roulette[1]. " > ". $sc['account_id']. ": " . $sc_commission_american_roulette[1]. " </br>";
				else if($sma_commission_american_roulette[2] > $sc_commission_american_roulette[2]) //savan
					echo "------Error Commission American Roulette----:" . $sma['account_id'] . "  :savan=". $sma_commission_american_roulette[2]. " > ". $sc['account_id']. ": " . $sc_commission_american_roulette[2]. " </br>";
				else if($sma_commission_american_roulette[3] > $sc_commission_american_roulette[3]) // virtua
					echo "------Error Commission American Roulette----:" . $sma['account_id'] . "  :virtua=". $sma_commission_american_roulette[3]. " > ". $sc['account_id']. ": " . $sc_commission_american_roulette[3]. " </br>";
				
				$sma_commission_slots = explode("#", $sma['commission_slots']);
				$sc_commission_slots = explode("#", $sc['commission_slots']);
				if($sma_commission_slots[0] > $sc_commission_slots[0]) // costa
					echo "------Error Commission Slot Virtua----:" . $sma['account_id'] . "  :costa=". $sma_commission_slots[0]. " > ". $sc['account_id']. ": " . $sc_commission_slots[0]. " </br>";
				else if($sma_commission_slots[1] > $sc_commission_slots[1]) //htv
					echo "------Error Commission Slot Virtua----:" . $sma['account_id'] . "  :htv=". $sma_commission_slots[1]. " > ". $sc['account_id']. ": " . $sc_commission_slots[1]. " </br>";
				else if($sma_commission_slots[2] > $sc_commission_slots[2]) //savan
					echo "------Error Commission Slot Virtua----:" . $sma['account_id'] . "  :savan=". $sma_commission_slots[2]. " > ". $sc['account_id']. ": " . $sc_commission_slots[2]. " </br>";
				else if($sma_commission_slots[3] > $sc_commission_slots[3]) // virtua
					echo "------Error Commission Slot Virtua----:" . $sma['account_id'] . "  :virtua=". $sma_commission_slots[3]. " > ". $sc['account_id']. ": " . $sc_commission_slots[3]. " </br>";
				
				
				/* Compare between senior master's sharing/commission and master's one */				
				$command = $connection->createCommand("SELECT * from tbl_agent where agent_type='ma' and agent_parent_id='". $sma['account_id'] ."'");
				$maList=$command->queryAll();

				foreach ($maList as $ma)
				{
					/* Compare sharing */
					//share_baccarat costa#htv#savan#virtua
					$ma_share_baccarat = explode("#", $ma['share_baccarat']);
					$sma_share_baccarat = explode("#", $sma['share_baccarat']);
					
					if($ma_share_baccarat[0] > $sma_share_baccarat[0]) // costa
						echo "------Error Sharing Baccarat----:" . $ma['account_id'] . "  :costa=". $ma_share_baccarat[0]. " > ". $sma['account_id']. ": " . $sma_share_baccarat[0]. " </br>";
					else if($ma_share_baccarat[1] > $sma_share_baccarat[1]) //htv
						echo "------Error Sharing Baccarat----:" . $ma['account_id'] . "  :htv=". $ma_share_baccarat[1]. " > ". $sma['account_id']. ": " . $sma_share_baccarat[1]. " </br>";
					else if($ma_share_baccarat[2] > $sma_share_baccarat[2]) //savan
						echo "------Error Sharing Baccarat----:" . $ma['account_id'] . "  :savan=". $ma_share_baccarat[2]. " > ". $sma['account_id']. ": " . $sma_share_baccarat[2]. " </br>";
					else if($ma_share_baccarat[3] > $sma_share_baccarat[3]) // virtua
						echo "------Error Sharing Baccarat----:" . $ma['account_id'] . "  :virtua=". $ma_share_baccarat[3]. " > ". $sma['account_id']. ": " . $sma_share_baccarat[3]. " </br>";
					
					//share_roulette costa#htv#savan#virtua
					$ma_share_roulette = explode("#", $ma['share_roulette']);
					$sma_share_roulette = explode("#", $sma['share_roulette']);
					if($ma_share_roulette[0] > $sma_share_roulette[0]) // costa
						echo "------Error Sharing Roulette----:" . $ma['account_id'] . "  :costa=". $ma_share_roulette[0]. " > ". $sma['account_id']. ": " . $sma_share_roulette[0]. " </br>";
					else if($ma_share_roulette[1] > $sma_share_roulette[1]) //htv
						echo "------Error Sharing Roulette----:" . $ma['account_id'] . "  :htv=". $ma_share_roulette[1]. " > ". $sma['account_id']. ": " . $sma_share_roulette[1]. " </br>";
					else if($ma_share_roulette[2] > $sma_share_roulette[2]) //savan
						echo "------Error Sharing Roulette----:" . $ma['account_id'] . "  :savan=". $ma_share_roulette[2]. " > ". $sma['account_id']. ": " . $sma_share_roulette[2]. " </br>";
					else if($ma_share_roulette[3] > $sma_share_roulette[3]) // virtua
						echo "------Error Sharing Roulette----:" . $ma['account_id'] . "  :virtua=". $ma_share_roulette[3]. " > ". $sma['account_id']. ": " . $sma_share_roulette[3]. " </br>";
					
					$ma_share_dragon_tiger = explode("#", $ma['share_dragon_tiger']);
					$sma_share_dragon_tiger = explode("#", $sma['share_dragon_tiger']);
					if($ma_share_dragon_tiger[0] > $sma_share_dragon_tiger[0]) // costa
						echo "------Error Sharing D/T----:" . $ma['account_id'] . "  :costa=". $ma_share_dragon_tiger[0]. " > ". $sma['account_id']. ": " . $sma_share_dragon_tiger[0]. " </br>";
					else if($ma_share_dragon_tiger[1] > $sma_share_dragon_tiger[1]) //htv
						echo "------Error Sharing D/T----:" . $ma['account_id'] . "  :htv=". $ma_share_dragon_tiger[1]. " > ". $sma['account_id']. ": " . $sma_share_dragon_tiger[1]. " </br>";
					else if($ma_share_dragon_tiger[2] > $sma_share_dragon_tiger[2]) //savan
						echo "------Error Sharing D/T----:" . $ma['account_id'] . "  :savan=". $ma_share_dragon_tiger[2]. " > ". $sma['account_id']. ": " . $sma_share_dragon_tiger[2]. " </br>";
					else if($ma_share_dragon_tiger[3] > $sma_share_dragon_tiger[3]) // virtua
						echo "------Error Sharing D/T----:" . $ma['account_id'] . "  :virtua=". $ma_share_dragon_tiger[3]. " > ". $sma['account_id']. ": " . $sma_share_dragon_tiger[3]. " </br>";
					
					$ma_share_blackjack = explode("#", $ma['share_blackjack']);
					$sma_share_blackjack = explode("#", $sma['share_blackjack']);
					if($ma_share_blackjack[0] > $sma_share_blackjack[0]) // costa
						echo "------Error Sharing Blackjack----:" . $ma['account_id'] . "  :costa=". $ma_share_blackjack[0]. " > ". $sma['account_id']. ": " . $sma_share_blackjack[0]. " </br>";
					else if($ma_share_blackjack[1] > $sma_share_blackjack[1]) //htv
						echo "------Error Sharing Blackjack----:" . $ma['account_id'] . "  :htv=". $ma_share_blackjack[1]. " > ". $sma['account_id']. ": " . $sma_share_blackjack[1]. " </br>";
					else if($ma_share_blackjack[2] > $sma_share_blackjack[2]) //savan
						echo "------Error Sharing Blackjack----:" . $ma['account_id'] . "  :savan=". $ma_share_blackjack[2]. " > ". $sma['account_id']. ": " . $sma_share_blackjack[2]. " </br>";
					else if($ma_share_blackjack[3] > $sma_share_blackjack[3]) // virtua
						echo "------Error Sharing Blackjack----:" . $ma['account_id'] . "  :virtua=". $ma_share_blackjack[3]. " > ". $sma['account_id']. ": " . $sma_share_blackjack[3]. " </br>";
					
					$ma_share_american_roulette = explode("#", $ma['share_american_roulette']);
					$sma_share_american_roulette = explode("#", $sma['share_american_roulette']);
					if($ma_share_american_roulette[0] > $sma_share_american_roulette[0]) // costa
						echo "------Error Sharing American Roulette----:" . $ma['account_id'] . "  :costa=". $ma_share_american_roulette[0]. " > ". $sma['account_id']. ": " . $sma_share_american_roulette[0]. " </br>";
					else if($ma_share_american_roulette[1] > $sma_share_american_roulette[1]) //htv
						echo "------Error Sharing American Roulette----:" . $ma['account_id'] . "  :htv=". $ma_share_american_roulette[1]. " > ". $sma['account_id']. ": " . $sma_share_american_roulette[1]. " </br>";
					else if($ma_share_american_roulette[2] > $sma_share_american_roulette[2]) //savan
						echo "------Error Sharing American Roulette----:" . $ma['account_id'] . "  :savan=". $ma_share_american_roulette[2]. " > ". $sma['account_id']. ": " . $sma_share_american_roulette[2]. " </br>";
					else if($ma_share_american_roulette[3] > $sma_share_american_roulette[3]) // virtua
						echo "------Error Sharing American Roulette----:" . $ma['account_id'] . "  :virtua=". $ma_share_american_roulette[3]. " > ". $sma['account_id']. ": " . $sma_share_american_roulette[3]. " </br>";
					
					$ma_share_slots = explode("#", $ma['share_slots']);
					$sma_share_slots = explode("#", $sma['share_slots']);
					if($ma_share_slots[3] > $sma_share_slots[3]) // costa
						echo "------Error Sharing Slot Virtua----:" . $ma['account_id'] . "  :costa=". $ma_share_slots[3]. " > ". $sma['account_id']. ": " . $sma_share_slots[3]. " </br>";
					
					/* Compare commission */
					
					$ma_commission_baccarat = explode("#", $ma['commission_baccarat']);
					$sma_commission_baccarat = explode("#", $sma['commission_baccarat']);

					if($ma_commission_baccarat[0] > $sma_commission_baccarat[0]) // costa
						echo "------Error Commission Baccarat----:" . $ma['account_id'] . "  :costa=". $ma_commission_baccarat[0]. " > ". $sma['account_id']. ": " . $sma_commission_baccarat[0]. " </br>";
					else if($ma_commission_baccarat[1] > $sma_commission_baccarat[1]) //htv
						echo "------Error Commission Baccarat----:" . $ma['account_id'] . "  :htv=". $ma_commission_baccarat[1]. " > ". $sma['account_id']. ": " . $sma_commission_baccarat[1]. " </br>";
					else if($ma_commission_baccarat[2] > $sma_commission_baccarat[2]) //savan
						echo "------Error Commission Baccarat----:" . $ma['account_id'] . "  :savan=". $ma_commission_baccarat[2]. " > ". $sma['account_id']. ": " . $sma_commission_baccarat[2]. " </br>";
					else if($ma_commission_baccarat[3] > $sma_commission_baccarat[3]) // virtua
						echo "------Error Commission Baccarat----:" . $ma['account_id'] . "  :virtua=". $ma_commission_baccarat[3]. " > ". $sma['account_id']. ": " . $sma_commission_baccarat[3]. " </br>";
					
					$ma_commission_roulette = explode("#", $ma['commission_roulette']);
					$sma_commission_roulette = explode("#", $sma['commission_roulette']);
					if($ma_commission_roulette[0] > $sma_commission_roulette[0]) // costa
						echo "------Error Commission Roulette----:" . $ma['account_id'] . "  :costa=". $ma_commission_roulette[0]. " > ". $sma['account_id']. ": " . $sma_commission_roulette[0]. " </br>";
					else if($ma_commission_roulette[1] > $sma_commission_roulette[1]) //htv
						echo "------Error Commission Roulette----:" . $ma['account_id'] . "  :htv=". $ma_commission_roulette[1]. " > ". $sma['account_id']. ": " . $sma_commission_roulette[1]. " </br>";
					else if($ma_commission_roulette[2] > $sma_commission_roulette[2]) //savan
						echo "------Error Commission Roulette----:" . $ma['account_id'] . "  :savan=". $ma_commission_roulette[2]. " > ". $sma['account_id']. ": " . $sma_commission_roulette[2]. " </br>";
					else if($ma_commission_roulette[3] > $sma_commission_roulette[3]) // virtua
						echo "------Error Commission Roulette----:" . $ma['account_id'] . "  :virtua=". $ma_commission_roulette[3]. " > ". $sma['account_id']. ": " . $sma_commission_roulette[3]. " </br>";
					
					$ma_commission_dragon_tiger = explode("#", $ma['commission_dragon_tiger']);
					$sma_commission_dragon_tiger = explode("#", $sma['commission_dragon_tiger']);
					if($ma_commission_dragon_tiger[0] > $sma_commission_dragon_tiger[0]) // costa
						echo "------Error Commission D/T----:" . $ma['account_id'] . "  :costa=". $ma_commission_dragon_tiger[0]. " > ". $sma['account_id']. ": " . $sma_commission_dragon_tiger[0]. " </br>";
					else if($ma_commission_dragon_tiger[1] > $sma_commission_dragon_tiger[1]) //htv
						echo "------Error Commission D/T----:" . $ma['account_id'] . "  :htv=". $ma_commission_dragon_tiger[1]. " > ". $sma['account_id']. ": " . $sma_commission_dragon_tiger[1]. " </br>";
					else if($ma_commission_dragon_tiger[2] > $sma_commission_dragon_tiger[2]) //savan
						echo "------Error Commission D/T----:" . $ma['account_id'] . "  :savan=". $ma_commission_dragon_tiger[2]. " > ". $sma['account_id']. ": " . $sma_commission_dragon_tiger[2]. " </br>";
					else if($ma_commission_dragon_tiger[3] > $sma_commission_dragon_tiger[3]) // virtua
						echo "------Error Commission D/T----:" . $ma['account_id'] . "  :virtua=". $ma_commission_dragon_tiger[3]. " > ". $sma['account_id']. ": " . $sma_commission_dragon_tiger[3]. " </br>";
					
					$ma_commission_blackjack = explode("#", $ma['commission_blackjack']);
					$sma_commission_blackjack = explode("#", $sma['commission_blackjack']);
					if($ma_commission_blackjack[0] > $sma_commission_blackjack[0]) // costa
						echo "------Error Commission Blackjack----:" . $ma['account_id'] . "  :costa=". $ma_commission_blackjack[0]. " > ". $sma['account_id']. ": " . $sma_commission_blackjack[0]. " </br>";
					else if($ma_commission_blackjack[1] > $sma_commission_blackjack[1]) //htv
						echo "------Error Commission Blackjack----:" . $ma['account_id'] . "  :htv=". $ma_commission_blackjack[1]. " > ". $sma['account_id']. ": " . $sma_commission_blackjack[1]. " </br>";
					else if($ma_commission_blackjack[2] > $sma_commission_blackjack[2]) //savan
						echo "------Error Commission Blackjack----:" . $ma['account_id'] . "  :savan=". $ma_commission_blackjack[2]. " > ". $sma['account_id']. ": " . $sma_commission_blackjack[2]. " </br>";
					else if($ma_commission_blackjack[3] > $sma_commission_blackjack[3]) // virtua
						echo "------Error Commission Blackjack----:" . $ma['account_id'] . "  :virtua=". $ma_commission_blackjack[3]. " > ". $sma['account_id']. ": " . $sma_commission_blackjack[3]. " </br>";
					
					$ma_commission_american_roulette = explode("#", $ma['commission_american_roulette']);
					$sma_commission_american_roulette = explode("#", $sma['commission_american_roulette']);
					if($ma_commission_american_roulette[0] > $sma_commission_american_roulette[0]) // costa
						echo "------Error Commission American Roulette----:" . $ma['account_id'] . "  :costa=". $ma_commission_american_roulette[0]. " > ". $sma['account_id']. ": " . $sma_commission_american_roulette[0]. " </br>";
					else if($ma_commission_american_roulette[1] > $sma_commission_american_roulette[1]) //htv
						echo "------Error Commission American Roulette----:" . $ma['account_id'] . "  :htv=". $ma_commission_american_roulette[1]. " > ". $sma['account_id']. ": " . $sma_commission_american_roulette[1]. " </br>";
					else if($ma_commission_american_roulette[2] > $sma_commission_american_roulette[2]) //savan
						echo "------Error Commission American Roulette----:" . $ma['account_id'] . "  :savan=". $ma_commission_american_roulette[2]. " > ". $sma['account_id']. ": " . $sma_commission_american_roulette[2]. " </br>";
					else if($ma_commission_american_roulette[3] > $sma_commission_american_roulette[3]) // virtua
						echo "------Error Commission American Roulette----:" . $ma['account_id'] . "  :virtua=". $ma_commission_american_roulette[3]. " > ". $sma['account_id']. ": " . $sma_commission_american_roulette[3]. " </br>";
					
					$ma_commission_slots = explode("#", $ma['commission_slots']);
					$sma_commission_slots = explode("#", $sma['commission_slots']);
					if($ma_commission_slots[0] > $sma_commission_slots[0]) // costa
						echo "------Error Commission Slot Virtua----:" . $ma['account_id'] . "  :costa=". $ma_commission_slots[0]. " > ". $sma['account_id']. ": " . $sma_commission_slots[0]. " </br>";
					else if($ma_commission_slots[1] > $sma_commission_slots[1]) //htv
						echo "------Error Commission Slot Virtua----:" . $ma['account_id'] . "  :htv=". $ma_commission_slots[1]. " > ". $sma['account_id']. ": " . $sma_commission_slots[1]. " </br>";
					else if($ma_commission_slots[2] > $sma_commission_slots[2]) //savan
						echo "------Error Commission Slot Virtua----:" . $ma['account_id'] . "  :savan=". $ma_commission_slots[2]. " > ". $sma['account_id']. ": " . $sma_commission_slots[2]. " </br>";
					else if($ma_commission_slots[3] > $sma_commission_slots[3]) // virtua
						echo "------Error Commission Slot Virtua----:" . $ma['account_id'] . "  :virtua=". $ma_commission_slots[3]. " > ". $sma['account_id']. ": " . $sma_commission_slots[3]. " </br>";
					
					/* Compare between master's sharing/commission and agent's one */
					$command = $connection->createCommand("SELECT * from tbl_agent where agent_type='agt' and agent_parent_id='". $ma['account_id'] ."'");
					$agtList=$command->queryAll();
						
					foreach ($agtList as $a)
					{
						
						/* Compare sharing */
						//share_baccarat costa#htv#savan#virtua
						$a_share_baccarat = explode("#", $a['share_blackjack']);
						$ma_share_baccarat = explode("#", $ma['share_blackjack']);
							
						if($a_share_baccarat[0] > $ma_share_baccarat[0]) // costa
							echo "------Error Sharing Baccarat----:" . $a['account_id'] . "  :costa=". $a_share_baccarat[0]. " > ". $ma['account_id']. ": " . $ma_share_baccarat[0]. " </br>";
						else if($a_share_baccarat[1] > $ma_share_baccarat[1]) //htv
							echo "------Error Sharing Baccarat----:" . $a['account_id'] . "  :htv=". $a_share_baccarat[1]. " > ". $ma['account_id']. ": " . $ma_share_baccarat[1]. " </br>";
						else if($a_share_baccarat[2] > $ma_share_baccarat[2]) //savan
							echo "------Error Sharing Baccarat----:" . $a['account_id'] . "  :savan=". $a_share_baccarat[2]. " > ". $ma['account_id']. ": " . $ma_share_baccarat[2]. " </br>";
						else if($a_share_baccarat[3] > $ma_share_baccarat[3]) // virtua
							echo "------Error Sharing Baccarat----:" . $a['account_id'] . "  :virtua=". $a_share_baccarat[3]. " > ". $ma['account_id']. ": " . $ma_share_baccarat[3]. " </br>";
							
						//share_roulette costa#htv#savan#virtua
						$a_share_roulette = explode("#", $a['share_roulette']);
						$ma_share_roulette = explode("#", $ma['share_roulette']);
						if($a_share_roulette[0] > $ma_share_roulette[0]) // costa
							echo "------Error Sharing Roulette----:" . $a['account_id'] . "  :costa=". $a_share_roulette[0]. " > ". $ma['account_id']. ": " . $ma_share_roulette[0]. " </br>";
						else if($a_share_roulette[1] > $ma_share_roulette[1]) //htv
							echo "------Error Sharing Roulette----:" . $a['account_id'] . "  :htv=". $a_share_roulette[1]. " > ". $ma['account_id']. ": " . $ma_share_roulette[1]. " </br>";
						else if($a_share_roulette[2] > $ma_share_roulette[2]) //savan
							echo "------Error Sharing Roulette----:" . $a['account_id'] . "  :savan=". $a_share_roulette[2]. " > ". $ma['account_id']. ": " . $ma_share_roulette[2]. " </br>";
						else if($a_share_roulette[3] > $ma_share_roulette[3]) // virtua
							echo "------Error Sharing Roulette----:" . $a['account_id'] . "  :virtua=". $a_share_roulette[3]. " > ". $ma['account_id']. ": " . $ma_share_roulette[3]. " </br>";
							
						$a_share_dragon_tiger = explode("#", $a['share_dragon_tiger']);
						$ma_share_dragon_tiger = explode("#", $ma['share_dragon_tiger']);
						if($a_share_dragon_tiger[0] > $ma_share_dragon_tiger[0]) // costa
							echo "------Error Sharing D/T----:" . $a['account_id'] . "  :costa=". $a_share_dragon_tiger[0]. " > ". $ma['account_id']. ": " . $ma_share_dragon_tiger[0]. " </br>";
						else if($a_share_dragon_tiger[1] > $ma_share_dragon_tiger[1]) //htv
							echo "------Error Sharing D/T----:" . $a['account_id'] . "  :htv=". $a_share_dragon_tiger[1]. " > ". $ma['account_id']. ": " . $ma_share_dragon_tiger[1]. " </br>";
						else if($a_share_dragon_tiger[2] > $ma_share_dragon_tiger[2]) //savan
							echo "------Error Sharing D/T----:" . $a['account_id'] . "  :savan=". $a_share_dragon_tiger[2]. " > ". $ma['account_id']. ": " . $ma_share_dragon_tiger[2]. " </br>";
						else if($a_share_dragon_tiger[3] > $ma_share_dragon_tiger[3]) // virtua
							echo "------Error Sharing D/T----:" . $a['account_id'] . "  :virtua=". $a_share_dragon_tiger[3]. " > ". $ma['account_id']. ": " . $ma_share_dragon_tiger[3]. " </br>";
							
						$a_share_blackjack = explode("#", $a['share_blackjack']);
						$ma_share_blackjack = explode("#", $ma['share_blackjack']);
						if($a_share_blackjack[0] > $ma_share_blackjack[0]) // costa
							echo "------Error Sharing Blackjack----:" . $a['account_id'] . "  :costa=". $a_share_blackjack[0]. " > ". $ma['account_id']. ": " . $ma_share_blackjack[0]. " </br>";
						else if($a_share_blackjack[1] > $ma_share_blackjack[1]) //htv
							echo "------Error Sharing Blackjack----:" . $a['account_id'] . "  :htv=". $a_share_blackjack[1]. " > ". $ma['account_id']. ": " . $ma_share_blackjack[1]. " </br>";
						else if($a_share_blackjack[2] > $ma_share_blackjack[2]) //savan
							echo "------Error Sharing Blackjack----:" . $a['account_id'] . "  :savan=". $a_share_blackjack[2]. " > ". $ma['account_id']. ": " . $ma_share_blackjack[2]. " </br>";
						else if($a_share_blackjack[3] > $ma_share_blackjack[3]) // virtua
							echo "------Error Sharing Blackjack----:" . $a['account_id'] . "  :virtua=". $a_share_blackjack[3]. " > ". $ma['account_id']. ": " . $ma_share_blackjack[3]. " </br>";
							
						$a_share_american_roulette = explode("#", $a['share_american_roulette']);
						$ma_share_american_roulette = explode("#", $ma['share_american_roulette']);
						if($a_share_american_roulette[0] > $ma_share_american_roulette[0]) // costa
							echo "------Error Sharing American Roulette----:" . $a['account_id'] . "  :costa=". $a_share_american_roulette[0]. " > ". $ma['account_id']. ": " . $ma_share_american_roulette[0]. " </br>";
						else if($a_share_american_roulette[1] > $ma_share_american_roulette[1]) //htv
							echo "------Error Sharing American Roulette----:" . $a['account_id'] . "  :htv=". $a_share_american_roulette[1]. " > ". $ma['account_id']. ": " . $ma_share_american_roulette[1]. " </br>";
						else if($a_share_american_roulette[2] > $ma_share_american_roulette[2]) //savan
							echo "------Error Sharing American Roulette----:" . $a['account_id'] . "  :savan=". $a_share_american_roulette[2]. " > ". $ma['account_id']. ": " . $ma_share_american_roulette[2]. " </br>";
						else if($a_share_american_roulette[3] > $ma_share_american_roulette[3]) // virtua
							echo "------Error Sharing American Roulette----:" . $a['account_id'] . "  :virtua=". $a_share_american_roulette[3]. " > ". $ma['account_id']. ": " . $ma_share_american_roulette[3]. " </br>";
							
						$a_share_slots = explode("#", $a['share_slots']);
						$ma_share_slots = explode("#", $ma['share_slots']);
						if($a_share_slots[3] > $ma_share_slots[3]) // costa
							echo "------Error Sharing Slot Virtua----:" . $a['account_id'] . "  :costa=". $a_share_slots[3]. " > ". $ma['account_id']. ": " . $ma_share_slots[3]. " </br>";
							
						/* Compare commission */
							
						$a_commission_baccarat = explode("#", $a['commission_baccarat']);
						$ma_commission_baccarat = explode("#", $ma['commission_baccarat']);
						if($a_commission_baccarat[0] > $ma_commission_baccarat[0]) // costa
							echo "------111Error Commission Baccarat----:" . $a['account_id'] . "  :costa=". $a_commission_baccarat[0]. " > ". $ma['account_id']. ": " . $ma_commission_baccarat[0]. " </br>";
						else if($a_commission_baccarat[1] > $ma_commission_baccarat[1]) //htv
							echo "------Error Commission Baccarat----:" . $a['account_id'] . "  :htv=". $a_commission_baccarat[1]. " > ". $ma['account_id']. ": " . $ma_commission_baccarat[1]. " </br>";
						else if($a_commission_baccarat[2] > $ma_commission_baccarat[2]) //savan
							echo "------Error Commission Baccarat----:" . $a['account_id'] . "  :savan=". $a_commission_baccarat[2]. " > ". $ma['account_id']. ": " . $ma_commission_baccarat[2]. " </br>";
						else if($a_commission_baccarat[3] > $ma_commission_baccarat[3]) // virtua
							echo "------Error Commission Baccarat----:" . $a['account_id'] . "  :virtua=". $a_commission_baccarat[3]. " > ". $ma['account_id']. ": " . $ma_commission_baccarat[3]. " </br>";
							
						$a_commission_roulette = explode("#", $a['commission_roulette']);
						$ma_commission_roulette = explode("#", $ma['commission_roulette']);
						if($a_commission_roulette[0] > $ma_commission_roulette[0]) // costa
							echo "------Error Commission Roulette----:" . $a['account_id'] . "  :costa=". $a_commission_roulette[0]. " > ". $ma['account_id']. ": " . $ma_commission_roulette[0]. " </br>";
						else if($a_commission_roulette[1] > $ma_commission_roulette[1]) //htv
							echo "------Error Commission Roulette----:" . $a['account_id'] . "  :htv=". $a_commission_roulette[1]. " > ". $ma['account_id']. ": " . $ma_commission_roulette[1]. " </br>";
						else if($a_commission_roulette[2] > $ma_commission_roulette[2]) //savan
							echo "------Error Commission Roulette----:" . $a['account_id'] . "  :savan=". $a_commission_roulette[2]. " > ". $ma['account_id']. ": " . $ma_commission_roulette[2]. " </br>";
						else if($a_commission_roulette[3] > $ma_commission_roulette[3]) // virtua
							echo "------Error Commission Roulette----:" . $a['account_id'] . "  :virtua=". $a_commission_roulette[3]. " > ". $ma['account_id']. ": " . $ma_commission_roulette[3]. " </br>";
							
						$a_commission_dragon_tiger = explode("#", $a['commission_dragon_tiger']);
						$ma_commission_dragon_tiger = explode("#", $ma['commission_dragon_tiger']);
						if($a_commission_dragon_tiger[0] > $ma_commission_dragon_tiger[0]) // costa
							echo "------Error Commission D/T----:" . $a['account_id'] . "  :costa=". $a_commission_dragon_tiger[0]. " > ". $ma['account_id']. ": " . $ma_commission_dragon_tiger[0]. " </br>";
						else if($a_commission_dragon_tiger[1] > $ma_commission_dragon_tiger[1]) //htv
							echo "------Error Commission D/T----:" . $a['account_id'] . "  :htv=". $a_commission_dragon_tiger[1]. " > ". $ma['account_id']. ": " . $ma_commission_dragon_tiger[1]. " </br>";
						else if($a_commission_dragon_tiger[2] > $ma_commission_dragon_tiger[2]) //savan
							echo "------Error Commission D/T----:" . $a['account_id'] . "  :savan=". $a_commission_dragon_tiger[2]. " > ". $ma['account_id']. ": " . $ma_commission_dragon_tiger[2]. " </br>";
						else if($a_commission_dragon_tiger[3] > $ma_commission_dragon_tiger[3]) // virtua
							echo "------Error Commission D/T----:" . $a['account_id'] . "  :virtua=". $a_commission_dragon_tiger[3]. " > ". $ma['account_id']. ": " . $ma_commission_dragon_tiger[3]. " </br>";
							
						$a_commission_blackjack = explode("#", $a['commission_blackjack']);
						$ma_commission_blackjack = explode("#", $ma['commission_blackjack']);
						if($a_commission_blackjack[0] > $ma_commission_blackjack[0]) // costa
							echo "------Error Commission Blackjack----:" . $a['account_id'] . "  :costa=". $a_commission_blackjack[0]. " > ". $ma['account_id']. ": " . $ma_commission_blackjack[0]. " </br>";
						else if($a_commission_blackjack[1] > $ma_commission_blackjack[1]) //htv
							echo "------Error Commission Blackjack----:" . $a['account_id'] . "  :htv=". $a_commission_blackjack[1]. " > ". $ma['account_id']. ": " . $ma_commission_blackjack[1]. " </br>";
						else if($a_commission_blackjack[2] > $ma_commission_blackjack[2]) //savan
							echo "------Error Commission Blackjack----:" . $a['account_id'] . "  :savan=". $a_commission_blackjack[2]. " > ". $ma['account_id']. ": " . $ma_commission_blackjack[2]. " </br>";
						else if($a_commission_blackjack[3] > $ma_commission_blackjack[3]) // virtua
							echo "------Error Commission Blackjack----:" . $a['account_id'] . "  :virtua=". $a_commission_blackjack[3]. " > ". $ma['account_id']. ": " . $ma_commission_blackjack[3]. " </br>";
							
						$a_commission_american_roulette = explode("#", $ma['commission_american_roulette']);
						$ma_commission_american_roulette = explode("#", $ma['commission_american_roulette']);
						if($a_commission_american_roulette[0] > $ma_commission_american_roulette[0]) // costa
							echo "------Error Commission American Roulette----:" . $a['account_id'] . "  :costa=". $a_commission_american_roulette[0]. " > ". $ma['account_id']. ": " . $ma_commission_american_roulette[0]. " </br>";
						else if($a_commission_american_roulette[1] > $ma_commission_american_roulette[1]) //htv
							echo "------Error Commission American Roulette----:" . $a['account_id'] . "  :htv=". $a_commission_american_roulette[1]. " > ". $ma['account_id']. ": " . $ma_commission_american_roulette[1]. " </br>";
						else if($a_commission_american_roulette[2] > $ma_commission_american_roulette[2]) //savan
							echo "------Error Commission American Roulette----:" . $a['account_id'] . "  :savan=". $a_commission_american_roulette[2]. " > ". $ma['account_id']. ": " . $ma_commission_american_roulette[2]. " </br>";
						else if($a_commission_american_roulette[3] > $ma_commission_american_roulette[3]) // virtua
							echo "------Error Commission American Roulette----:" . $a['account_id'] . "  :virtua=". $a_commission_american_roulette[3]. " > ". $ma['account_id']. ": " . $ma_commission_american_roulette[3]. " </br>";
							
						$a_commission_slots = explode("#", $a['commission_slots']);
						$ma_commission_slots = explode("#", $ma['commission_slots']);
						if($a_commission_slots[0] > $ma_commission_slots[0]) // costa
							echo "------Error Commission Slot Virtua----:" . $a['account_id'] . "  :costa=". $a_commission_slots[0]. " > ". $ma['account_id']. ": " . $ma_commission_slots[0]. " </br>";
						else if($a_commission_slots[1] > $ma_commission_slots[1]) //htv
							echo "------Error Commission Slot Virtua----:" . $a['account_id'] . "  :htv=". $a_commission_slots[1]. " > ". $ma['account_id']. ": " . $ma_commission_slots[1]. " </br>";
						else if($a_commission_slots[2] > $ma_commission_slots[2]) //savan
							echo "------Error Commission Slot Virtua----:" . $a['account_id'] . "  :savan=". $a_commission_slots[2]. " > ". $ma['account_id']. ": " . $ma_commission_slots[2]. " </br>";
						else if($a_commission_slots[3] > $ma_commission_slots[3]) // virtua
							echo "------Error Commission Slot Virtua----:" . $a['account_id'] . "  :virtua=". $a_commission_slots[3]. " > ". $ma['account_id']. ": " . $ma_commission_slots[3]. " </br>";
							
						/* Compare between agent's commission and player's commission */	
						$command = $connection->createCommand("SELECT * from tbl_agent_player where agent_account_id='". $a['account_id'] ."'");
						$playerList=$command->queryAll();
							
						foreach ($playerList as $p)
						{
							// Compare commission
							$p_commission_baccarat = explode("#", $p['commission_baccarat']);
							$a_commission_baccarat = explode("#", $a['commission_baccarat']);
							if($p_commission_baccarat[0] > $a_commission_baccarat[0]) // costa
								echo "------Error Player's Commission Baccarat----:" . $p['account_id'] . "  :costa=". $p_commission_baccarat[0]. " > ". $a['account_id']. ": " . $a_commission_baccarat[0]. " </br>";
							else if($p_commission_baccarat[1] > $a_commission_baccarat[1]) //htv
								echo "------Error Player's Commission Baccarat----:" . $p['account_id'] . "  :htv=". $p_commission_baccarat[1]. " > ". $a['account_id']. ": " . $a_commission_baccarat[1]. " </br>";
							else if($p_commission_baccarat[2] > $a_commission_baccarat[2]) //savan
								echo "------Error Player's Commission Baccarat----:" . $p['account_id'] . "  :savan=". $p_commission_baccarat[2]. " > ". $a['account_id']. ": " . $a_commission_baccarat[2]. " </br>";
							else if($p_commission_baccarat[3] > $a_commission_baccarat[3]) // virtua
								echo "------Error Player's Commission Baccarat----:" . $p['account_id'] . "  :virtua=". $p_commission_baccarat[3]. " > ". $a['account_id']. ": " . $a_commission_baccarat[3]. " </br>";
								
							$p_commission_roulette = explode("#", $p['commission_roulette']);
							$a_commission_roulette = explode("#", $a['commission_roulette']);
							if($p_commission_roulette[0] > $a_commission_roulette[0]) // costa
								echo "------Error Player's Commission Roulette----:" . $p['account_id'] . "  :costa=". $p_commission_roulette[0]. " > ". $a['account_id']. ": " . $a_commission_roulette[0]. " </br>";
							else if($p_commission_roulette[1] > $a_commission_roulette[1]) //htv
								echo "------Error Player's Commission Roulette----:" . $p['account_id'] . "  :htv=". $p_commission_roulette[1]. " > ". $a['account_id']. ": " . $a_commission_roulette[1]. " </br>";
							else if($p_commission_roulette[2] > $a_commission_roulette[2]) //savan
								echo "------Error Player's Commission Roulette----:" . $p['account_id'] . "  :savan=". $p_commission_roulette[2]. " > ". $a['account_id']. ": " . $a_commission_roulette[2]. " </br>";
							else if($p_commission_roulette[3] > $a_commission_roulette[3]) // virtua
								echo "------Error Player's Commission Roulette----:" . $p['account_id'] . "  :virtua=". $p_commission_roulette[3]. " > ". $a['account_id']. ": " . $a_commission_roulette[3]. " </br>";
								
							$p_commission_dragon_tiger = explode("#", $p['commission_dragon_tiger']);
							$a_commission_dragon_tiger = explode("#", $a['commission_dragon_tiger']);
							if($p_commission_dragon_tiger[0] > $a_commission_dragon_tiger[0]) // costa
								echo "------Error Player's Commission D/T----:" . $p['account_id'] . "  :costa=". $p_commission_dragon_tiger[0]. " > ". $a['account_id']. ": " . $a_commission_dragon_tiger[0]. " </br>";
							else if($p_commission_dragon_tiger[1] > $a_commission_dragon_tiger[1]) //htv
								echo "------Error Player's Commission D/T----:" . $p['account_id'] . "  :htv=". $p_commission_dragon_tiger[1]. " > ". $a['account_id']. ": " . $a_commission_dragon_tiger[1]. " </br>";
							else if($p_commission_dragon_tiger[2] > $a_commission_dragon_tiger[2]) //savan
								echo "------Error Player's Commission D/T----:" . $p['account_id'] . "  :savan=". $p_commission_dragon_tiger[2]. " > ". $a['account_id']. ": " . $a_commission_dragon_tiger[2]. " </br>";
							else if($p_commission_dragon_tiger[3] > $a_commission_dragon_tiger[3]) // virtua
								echo "------Error Player's Commission D/T----:" . $p['account_id'] . "  :virtua=". $p_commission_dragon_tiger[3]. " > ". $a['account_id']. ": " . $a_commission_dragon_tiger[3]. " </br>";
								
							$p_commission_blackjack = explode("#", $p['commission_blackjack']);
							$a_commission_blackjack = explode("#", $a['commission_blackjack']);
							if($p_commission_blackjack[0] > $a_commission_blackjack[0]) // costa
								echo "------Error Player's Commission Blackjack----:" . $p['account_id'] . "  :costa=". $p_commission_blackjack[0]. " > ". $a['account_id']. ": " . $a_commission_blackjack[0]. " </br>";
							else if($p_commission_blackjack[1] > $a_commission_blackjack[1]) //htv
								echo "------Error Player's Commission Blackjack----:" . $p['account_id'] . "  :htv=". $p_commission_blackjack[1]. " > ". $a['account_id']. ": " . $a_commission_blackjack[1]. " </br>";
							else if($p_commission_blackjack[2] > $a_commission_blackjack[2]) //savan
								echo "------Error Player's Commission Blackjack----:" . $p['account_id'] . "  :savan=". $p_commission_blackjack[2]. " > ". $a['account_id']. ": " . $a_commission_blackjack[2]. " </br>";
							else if($p_commission_blackjack[3] > $a_commission_blackjack[3]) // virtua
								echo "------Error Player's Commission Blackjack----:" . $p['account_id'] . "  :virtua=". $p_commission_blackjack[3]. " > ". $a['account_id']. ": " . $a_commission_blackjack[3]. " </br>";
								
							$p_commission_american_roulette = explode("#", $p['commission_american_roulette']);
							$a_commission_american_roulette = explode("#", $a['commission_american_roulette']);
							if($p_commission_american_roulette[0] > $a_commission_american_roulette[0]) // costa
								echo "------Error Player's Commission American Roulette----:" . $p['account_id'] . "  :costa=". $p_commission_american_roulette[0]. " > ". $a['account_id']. ": " . $a_commission_american_roulette[0]. " </br>";
							else if($p_commission_american_roulette[1] > $a_commission_american_roulette[1]) //htv
								echo "------Error Player's Commission American Roulette----:" . $p['account_id'] . "  :htv=". $p_commission_american_roulette[1]. " > ". $a['account_id']. ": " . $a_commission_american_roulette[1]. " </br>";
							else if($p_commission_american_roulette[2] > $a_commission_american_roulette[2]) //savan
								echo "------Error Player's Commission American Roulette----:" . $p['account_id'] . "  :savan=". $p_commission_american_roulette[2]. " > ". $a['account_id']. ": " . $a_commission_american_roulette[2]. " </br>";
							else if($p_commission_american_roulette[3] > $a_commission_american_roulette[3]) // virtua
								echo "------Error Player's Commission American Roulette----:" . $p['account_id'] . "  :virtua=". $p_commission_american_roulette[3]. " > ". $a['account_id']. ": " . $a_commission_american_roulette[3]. " </br>";
								
							$p_commission_slots = explode("#", $p['commission_slots']);
							$a_commission_slots = explode("#", $a['commission_slots']);
							if($p_commission_slots[0] > $a_commission_slots[0]) // costa
								echo "------Error Player's Commission Slot Virtua----:" . $p['account_id'] . "  :costa=". $p_commission_slots[0]. " > ". $a['account_id']. ": " . $a_commission_slots[0]. " </br>";
							else if($p_commission_slots[1] > $a_commission_slots[1]) //htv
								echo "------Error Player's Commission Slot Virtua----:" . $p['account_id'] . "  :htv=". $p_commission_slots[1]. " > ". $a['account_id']. ": " . $a_commission_slots[1]. " </br>";
							else if($p_commission_slots[2] > $a_commission_slots[2]) //savan
								echo "------Error Player's Commission Slot Virtua----:" . $p['account_id'] . "  :savan=". $p_commission_slots[2]. " > ". $a['account_id']. ": " . $a_commission_slots[2]. " </br>";
							else if($p_commission_slots[3] > $a_commission_slots[3]) // virtua
								echo "------Error Player's Commission Slot Virtua----:" . $p['account_id'] . "  :virtua=". $p_commission_slots[3]. " > ". $a['account_id']. ": " . $a_commission_slots[3]. " </br>";
								

						}
					}
				}
			}
		}
	}
}