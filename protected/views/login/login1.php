<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Clubvegas999 | Login</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/select2/select2_metro.css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/resource/css/pages/login.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/loginCheckAttempt.js"></script>
</head>
<style>
#lang ul {
            display:table-row;
        }
#lang ul li
{
   display: table-cell;
   list-style-type: none;
   margin: 10px;
   vertical-align: middle;
   padding-left: 5px;
}
</style>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login"  onload="loadCheck();">
<script type="text/javascript">
	var checkLocked='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/CheckLocked';
	var addLockedUser='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/AddLockUser';
	var removeLockedUser='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/RemoveLockUser';
	var countAttempt='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/CountAttempt';
	var getDuration='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Login/GetDuration';
	var attempt_count='<?php echo yii::app()->session['no_attempt']; ?>';
	var user_that_attempt='<?php echo yii::app()->session['user_that_attempt']; ?>'
</script>

<!-- BEGIN LOGO -->
<div class="logo">
	<img src="<?php echo Yii::app()->request->baseUrl; ?>/resource/img/logo_flat1.png" alt="" /> 
</div>
<br/>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<div class="form-vertical login-form">
		<div class="form" align="center">
			<?php 
			$lang = isset($_POST['chooselan'])? $_POST['chooselan'] : "en";
			Yii::app()->session['chooselanaguage'] = $lang;
			Yii::app()->language=$lang;
			
			$form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>Yii::app()->baseUrl .'/index.php?r=Login&lang='. $lang,
				'enableClientValidation'=>true,
				'clientOptions'=>array('validateOnSubmit'=>true,),
			)); ?>
			<center>
			<form id="loginform" name="loginform" method="post" action="">
		    <input type="hidden" value="en" name="chooselan" id="chooselan">
		    <div id="lang">
		        <ul id="lang_lanjian">
		          <li><a onclick="document.getElementById('chooselan').value='en';document.getElementById('loginform').submit();" href="#"><img  title="English" alt="English" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/en.png"></a></li>
		          <li><a onclick="document.getElementById('chooselan').value='th';document.getElementById('loginform').submit();" href="#"><img  title="ไทย" alt="ไทย" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/th.png"></a></li>	          
		          <li><a onclick="document.getElementById('chooselan').value='vi';document.getElementById('loginform').submit();" href="#"><img title="Tiếng Việt" alt="Tiếng Việt" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/vn.png"></a></li>
		     	  <li><a onclick="document.getElementById('chooselan').value='zh_cn';document.getElementById('loginform').submit();" href="#"><img  title="中文简体" alt="中文简体" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/cn.png"></a></li>	          
		          <li><a onclick="document.getElementById('chooselan').value='zh_hk';document.getElementById('loginform').submit();" href="#"><img  title="中文繁体" alt="中文繁体" src="<?php echo Yii::app()->request->baseUrl; ?>/images/languages/hk.png"></a></li>
		     	</ul>
		    </div>
			</form>
			<h3 class="form-title">Login</h3>
			</center>
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				<span>Enter any username and password.</span>
			</div>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,Yii::t('login','LBL_USER_NAME')); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<?php echo $form->textField($model,'username',array('onkeydown'=>'if (event.keyCode == 13){return validation();}','class'=>'m-wrap placeholder-no-fix','placeholder'=>'Username')); ?>
						<!-- <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/> -->
					</div>
					<?php echo $form->error($model,'username'); ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,Yii::t('login','LBL_PASSWORD')); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<!--<input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>  -->
						<?php echo $form->passwordField($model,'password',array('onkeydown'=>'if (event.keyCode == 13){return validation();}','class'=>'m-wrap placeholder-no-fix','placeholder'=>'Password')); ?>
					</div>
					<?php echo $form->error($model,'password'); ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,Yii::t('login','LBL_CODE_CAPTCHA')); ?></label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<!--  <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" placeholder="Code" name="password"/>-->
						<?php echo $form->textField($model,'verifyCode',array('onkeydown'=>'if (event.keyCode == 13){return validation();}','class'=>'m-wrap placeholder-no-fix','placeholder'=>'Code')); ?>
					</div>
					<?php echo $form->error($model,'verifyCode'); ?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Captcha</label>
				<div class="controls">
					<div class="input-icon left">
						<?php $this->widget('CCaptcha', array('buttonLabel'=>'','clickableImage'=>true)); ?>
					</div>
					
				</div>
			</div>
			<div class="form-actions">
				<button type="submit" class="btn red pull-right">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
			<?php $this->endWidget(); ?>
		</div>
		</div>
		<!-- END LOGIN FORM -->        
		
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		2013 &copy; Metronic. Admin Dashboard Template.
	</div>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   <script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/resource/plugins/select2/select2.min.js"></script>     
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/app.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/resource/scripts/login.js" type="text/javascript"></script> 
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script>
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();
		
		 
		
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>