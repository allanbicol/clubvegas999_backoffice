<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_cashplayer_param').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Cash Player Parameter</a></li>");
</script>
<script type="text/javascript">
function checkInput(e, decimal)
{
	var key;
	var keychar;

	if (window.event) {
	   key = window.event.keyCode;
	}
	else if (e) {
	   key = e.which;
	}
	else {
	   return true;
	}
	keychar = String.fromCharCode(key);

	if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
	   return true;
	}
	else if ((("0123456789").indexOf(keychar) > -1)) {
	   return true;
	}
	else if (decimal && (keychar == ".")) { 
	  return true;
	}
	else
	   return false;
}

function resetFields()
{
	<?php 
			$recComm=CashPlayerMaxCommission::model()->findByPk(1);
	?>
	document.getElementById("txtBaccarat").value=<?php echo $recComm['baccarat_max_commission']; ?>;
	document.getElementById("txtRoulette").value=<?php echo $recComm['roulette_max_commission']; ?>;
	document.getElementById("txtARoulette").value=<?php echo $recComm['american_roulette_max_commission']; ?>;
	document.getElementById("txtDragonTiger").value=<?php echo $recComm['dragon_tiger_max_commission']; ?>;
	document.getElementById("txtSlots").value=<?php echo $recComm['slot_max_commission']; ?>;
	document.getElementById("txtBlackJack").value=<?php echo $recComm['blackjack_max_commission']; ?>;
	document.getElementById("lblAlert").value="Games maximum commission has been reseted.";
}

function confirmButton(){
	currenctStatusValue ='';
	jQuery.ajax({
		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerMaxCommission/UpdateCommission',
		method: 'POST',
		data: {'txtBaccarat': document.getElementById("txtBaccarat").value, 'txtRoulette': document.getElementById("txtRoulette").value,'txtARoulette':document.getElementById("txtARoulette").value, 'txtDragonTiger': document.getElementById("txtDragonTiger").value,'txtSlots':document.getElementById("txtSlots").value,'txtBlackJack':document.getElementById("txtBlackJack").value},
		context: '',
		success: function(data) {
			 document.getElementById("lblAlert").value=data;
    	}
	});
}
</script>

</head>
<body>
<?php 
	$recComm=CashPlayerMaxCommission::model()->findByPk(1);
?>
<div style="left: 5px; top:5px; border:1px solid #333333; width:510px; height: 320px ">
<form style=" width: 510px;  "  method="POST">
<div id="dvDeposit">
			<div style="background-color: #333333; color:white; height: 30px;padding-top: 5px;">&nbsp;<b>MAXIMUM COMMISSION</b></div>
			<div style=" height: 10px"></div>
			<div align="center">
			<table width="100%">
				<tr><td style="width: 50%;padding-left:5px;">Baccarat Max Commission</td><td>:</td><td><input type="text" name="txtBaccarat" id="txtBaccarat" onKeyPress="return checkInput(event, true);" value="<?php echo $recComm['baccarat_max_commission']; ?>"></td></tr>
				<tr><td style="width: 50%;padding-left:5px;">European Roulette Max Commission</td><td>:</td><td><input type="text" name="txtRoulette" id="txtRoulette" onKeyPress="return checkInput(event, true);" value="<?php echo $recComm['roulette_max_commission']; ?>"></td></tr>
				<tr><td style="width: 50%;padding-left:5px;">American Roulette Max Commission</td><td>:</td><td><input type="text" name="txtARoulette" id="txtARoulette" onKeyPress="return checkInput(event, true);" value="<?php echo $recComm['american_roulette_max_commission']; ?>"></td></tr>
				<tr><td style="width: 50%;padding-left:5px;">DragonTiger Max Commission</td><td>:</td><td><input type="text" name="txtDragonTiger" id="txtDragonTiger" onKeyPress="return checkInput(event, true);" value="<?php echo $recComm['dragon_tiger_max_commission']; ?>"></td></tr>
				<tr><td style="width: 50%;padding-left:5px;">Blackjack Max Commission</td><td>:</td><td><input type="text" name="txtBlackJack" id="txtBlackJack" onKeyPress="return checkInput(event, true);" value="<?php echo $recComm['blackjack_max_commission']; ?>"></td></tr>
				<tr><td style="width: 50%;padding-left:5px;">Slots Max Commission</td><td>:</td><td><input type="text" name="txtSlots" id="txtSlots" onKeyPress="return checkInput(event, true);" value="<?php echo $recComm['slot_max_commission']; ?>"></td>
					
			</table>
			</div>
			<div style=" height: 10px"></div>
			<div align="center"><input class="btn red" style="width: 80px" type="button" onclick="javascript:confirmButton();"  value="Confirm">&nbsp;<input class="btn red" style="width: 80px" type="button"  value="Reset" onclick="javascript:resetFields();"></div>
			<div style=" height: 15px"></div>
			<div><center><input id="lblAlert" style="width: 320px; color: red; background-color: #FFF; border: 0; font-style: italic;" readonly></center></div>
</div>	
</form></div>
<br>
<br>
</body>
</html>