<?php
class CashPlayerConsolidatedWinLossDetailController extends MyController
{
	public function actionCashPlayerConsolidatedWinLossDetail()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerConsolidatedWinLossDetail;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerWinLossDetail();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerWinLossDetail($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("bet_date","account_id","currency_name","trans_type","trans_amount","game_type","bet_amount","valid_bet","win_los","commission","balance","casino");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	public function actionCashPlayerConsolidatedWinLossSummary()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerConsolidatedWinLossDetail;
		
		$player_records = $cpHfund->getPlayerWinLossSummary();
		//$test = $records->readAll();
	
		$filedNames = array("casino","account_id","currency_name","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","bonus","amount_tips","total_win_los","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), 0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
// 	public function actionGetRecordAllPages()
// 	{
// 		$playersRecordAllPages=new CashPlayerConsolidatedWinLossDetail();
// 		$playersRecordAllPages->getPlayerWinLossDetailAllPages();
// 	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionConsolidatedDetailExcel(){
		$file="CashPlayerConsolidated_WinLossDetail".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		try{
			echo $buffer;
		}catch(Exception $e){
	
		}
	}
	
	public function actionExportAllCashPlayerConsolidatedWinLossDetail()
	{

		$cpHfund = new CashPlayerConsolidatedWinLossDetail;
		
		$result=$cpHfund->getCountPlayerWinLossDetail();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records = $cpHfund->getExportPlayerWinLossDetail();
	
		$fieldNames = array("bet_date","account_id","currency_name","trans_type","trans_amount","game_type","bet_amount","valid_bet","win_los","commission","balance","casino");
		$headerNames=array("Bet Date","Account Id","Currency","Trans Type","Trans Amount","Game Type","Bet Amount","Valid Bet","Winloss","Commission","Balance","Casino");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerWinLossSummary();
		
		$fieldNames = array("casino","currency_name","bet_count","total_stake","amount_wager","avg_bet","win_los","commission","bonus","amount_tips","total_win_los","p_l");
		$headerNames=array("Casino","Currency","Bet Count","Total Stake","Valid Stake","Average Bet","Winloss","Commission","Bonus","Tips","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
}