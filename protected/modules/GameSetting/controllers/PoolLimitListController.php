<?php
/**
 * @todo PoolLimitListController
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-02
 */
class PoolLimitListController extends MyController
{
	public function actionPoolLimit()
	{
 		$bl=new GetPoolLimit();
 		$result = $bl->getCountPoolLimitRecord();
 		$data=$result->readAll();
 		$records=$data[0]['COUNT(0)'];
 		$player_records=$bl->getPoolLimitList();
		
 		$filedNames = array("id","game_name","amount","last_update","game_id");
 		$urlAS= Yii::app()->request->baseUrl;
 		
 		$htmValue='<img  src=\"'.Yii::app()->request->baseUrl.'/images/setting.png\" width=\"14\" heigth=\"11\" /> <a href=\'#\' onclick=\'javascript: $(\"#viewContent1\").load(\"'. Yii::app()->request->baseUrl .'/index.php?r=GameSetting/PoolLimitList/LoadPoolLimitSettingContent&pool_id\");$(\"#dialogSettingPoolLimit\").dialog(\"open\");\'>Setting</a>';
 		echo JsonUtil::jsonEncodeSystemSetting($player_records->readAll(),1, 1, $records,$filedNames,$htmValue,4);
	}
	public function actionCreatePoolLimit()
	{
		$tl=new PoolLimit();
		$tl->addNewPoolLimit();
	}
	public function actionUpdatePoolLimit()
	{
		$tl=new PoolLimit();
		$tl->updatePoolLimit();
	}
	public function actionLoadPoolLimitContent()
	{
		$this->renderPartial("dialogPoolLimitContent");
	}
	public function actionLoadPoolLimitSettingContent()
	{
		$this->renderPartial("dialogPoolLimitSettingContent");
	}
	public function actionIndex()
	{
		$this->render("index");
	}
}