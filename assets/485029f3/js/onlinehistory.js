//active menu color
document.getElementById('mnu_online_history').style.color="#5A0000";
document.getElementById('mnu_online_history').style.fontWeight="bold";

jQuery(document).ready(function() {
    jQuery("#dtFrom").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
        //dateFormat: "dd-mm-yy"
    });

 	jQuery("#dtTo").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd"
    });
});

function tableOnlineHistory(dtfrom,dtto) { 
	document.getElementById('qry_result').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list1'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("Div"); 
    divTag1.id = 'pager1'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list1");
		grid.jqGrid({ 
			url: urlGetOnlineHistory +'&dtfrom='+ dtfrom + '&dtto=' + dtto, 
			datatype: 'json',
		    mtype: 'POST',
		    height: 'auto',
		    colNames: ['Account ID','account','Account Type','CLUBVEGAS999','HTV999','SAVANVEGAS999'],
		    colModel: [
		      {name: 'account_id', index: 'account_id', width: 70, sortable: false,},
		      {name: 'account_id', index: 'account_id', width: 70, sortable: false,hidden:true},
		      {name: 'account_type', index: 'account_type', width: 90, sortable: false},
		      {name: 'clubvegas', index: 'clubvegas', width: 70, sortable: false},
		      {name: 'htv', index: 'htv', width: 70, sortable: false},
		      {name: 'savan', index: 'savan', width: 70,sortable: false}
		    ],
		    loadComplete: function(){
		    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
			},
		    loadtext:"",
			width: 650,
            rownumbers: true,
            sortable: false,
		    sortname: 'account_id',
		    sortorder: 'ASC',
		    caption: 'Online History <label id="lblFrom"></label> - <label id="lblTo"></label>',
		    hidegrid: false,
		    viewrecords: true,
		    pager: "#pager1"
		});
		$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false,search: false,refresh:false});
	});
}

function tableOnlineHistoryDetails(account_id,dtfrom,dtto) { 
	document.getElementById('qry_result_details').innerHTML='';
	var divTag = document.createElement("Table"); 
    divTag.id = 'list2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result_details").appendChild(divTag);

    var divTag1 = document.createElement("Div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);
    
    $(document).ready(function() {
		var grid=jQuery("#list2");
		grid.jqGrid({ 
			url: urlGetOnlineHistoryDetails +'&dtfrom='+ dtfrom + '&dtto=' + dtto + '&account_id=' + account_id, 
			datatype: 'json',
		    mtype: 'POST',
		    height: 'auto',
		    colNames: ['Account ID','Account Type','Clubvegas Status','Casino Status','Log-in Date','Log-in IP Address'],
		    colModel: [
		      {name: 'account_id', index: 'account_id', width: 110, sortable: false,},
		      {name: 'account_type', index: 'account_type', width: 90, sortable: false},
		      {name: 'clubvegas_status', index: 'clubvegas_status', width: 110, sortable: false},
		      {name: 'casino_status', index: 'casino_status', width: 150, sortable: false},
		      {name: 'login_date', index: 'login_date', width: 130, sortable: false},
		      {name: 'ip_address', index: 'ip_address', width: 130,sortable: false}
		    ],
		    loadComplete: function(){
		    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
		    	$("tr.jqgrow:even").css("background", "#ffffff");
			},
		    loadtext:"",
            rownumbers: true,
            sortable: false,
		    sortname: 'account_id',
		    sortorder: 'ASC',
		    caption: 'Online History Details <label id="lblDFrom"></label> - <label id="lblDTo"></label> <input type="button" value="<<" onclick="document.getElementById(\'qry_result_details\').innerHTML=\'\';">',
		    hidegrid: false,
		    viewrecords: true,
		    pager: "#pager2"
		});
		$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false,search: false,refresh:false});
	});
}

function btnSubmit()
{
	var dtfrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtto=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
	tableOnlineHistory(dtfrom,dtto);
	document.getElementById('lblFrom').innerHTML=dtfrom;
	document.getElementById('lblTo').innerHTML=dtto;
	document.getElementById('qry_result_details').innerHTML='';
}

function btnToDay()
{
	document.getElementById('dtFrom').value=dtToday;
	document.getElementById('dtTo').value=dtToday;
	document.getElementById('cmbTime1').value='00';
	document.getElementById('cmbTime2').value='23';
	var dtfrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtto=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
	tableOnlineHistory(dtfrom,dtto);
	document.getElementById('lblFrom').innerHTML=dtfrom;
	document.getElementById('lblTo').innerHTML=dtto;
	document.getElementById('qry_result_details').innerHTML='';
}

function btnYesterday()
{
	document.getElementById('dtFrom').value=dtYesterday;
	document.getElementById('dtTo').value=dtYesterday;
	document.getElementById('cmbTime1').value='00';
	document.getElementById('cmbTime2').value='23';
	var dtfrom=document.getElementById('dtFrom').value + " " + document.getElementById('cmbTime1').value + ":00:00";
	var dtto=document.getElementById('dtTo').value + " " + document.getElementById('cmbTime2').value + ":59:59";
	tableOnlineHistory(dtfrom,dtto);
	document.getElementById('lblFrom').innerHTML=dtfrom;
	document.getElementById('lblTo').innerHTML=dtto;
	document.getElementById('qry_result_details').innerHTML='';
}
function playerDetails(account_id){
	var dtfrom=document.getElementById('lblFrom').innerHTML;
	var dtto=document.getElementById('lblTo').innerHTML;
	tableOnlineHistoryDetails(account_id,dtfrom,dtto);
	document.getElementById('lblDFrom').innerHTML=dtfrom;
	document.getElementById('lblDTo').innerHTML=dtto;
}