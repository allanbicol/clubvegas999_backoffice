<?php
/**
 * @todo AgentPlayer Model
 * @copyright CE
 * @author Sitthykun
 * @since 2013-06-014
 */

class AgentPlayer
{

	/**
	 * @return array
	 * @param string $agent
	 */
	public function  getAgentPlayersByAgent($agent) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetAgentPlayersByAgent(:agent)");
		$command->bindParam(":agent", $agent, PDO::PARAM_STR);
		// query as array
		return $command->query();
	}

	/**
	 * @return scalare
	 * @param string $agent
	 */
	public function getCountedAgentPlayerByAgent($agent) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeCountAgentPlayersByAgent(:agent, @out)");
		$command->bindParam(":agent", $agent, PDO::PARAM_STR);
		$command->execute();

		return $connection->createCommand("SELECT @out AS result;")->queryScalar();
	}

	/**
	 * @return string
	 * @param string $agent
	 */
	public function getKickOffStatus($agent) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetKickoffStatusByAgent(:agent, @status, @kickoffTime, @kickonTime)");
		$command->bindParam(":agent", $agent, PDO::PARAM_STR);
		$command->execute();

		$command = $connection->createCommand("SELECT @status, @kickOffTime, @kickOnTime;");
		return $command->queryRow(); 
	}
}

?>