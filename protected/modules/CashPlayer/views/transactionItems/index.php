<html>
<head>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl;?>/css/transactionitems.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl;?>/js/transactionitems.js"></script>
	<script type="text/javascript">
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_transaction_items').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Transaction Items</a></li>");
	
		var urlTransactionItems = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/TransactionItems/TransactionItemsList';
		var urlAddTransactionItemDialog = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/TransactionItems/LoadAddTransactionItem';
		var urlAddTransactionItem = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/TransactionItems/AddTransactionItem';
		var urlUpdateItemBankTransaction = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/TransactionItems/UpdateItemBankTransaction';
		var permissionWrite = ('<?php echo (Yii::app()->user->checkAccess('cashPlayer.writeTransactionItem')) ? 1 : 0;?>' == 1) ? true : false;
	</script>
</head>
<body>
	<div id="jqgrid_transaction_items"></div>
	
	
<?php 
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		    'id'=>'add_transaction_item_dialog',
		    // additional javascript options for the dialog plugin
		    'options'=>array(
		        'title'=>'Add Transaction Item',
		        'autoOpen'=>false,
	    		'width'=>'auto',
	    		'height'=>'auto',
		    	'modal'=> true,
		    	'resizable'=> false,
		    ),
		));
	
	// Initialize dialog content
    echo '<div id="viewAddTransactionItem"></div>';
    
    // End block create Dialog
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<br>
</body>
</html>