<?php

class InstantPoolsController extends MyController
{
	private $svInstantPool;
	
	public function actionInstantPools()
	{
		$this->svInstantPool = new Constants();
		$homepage = file_get_contents($this->svInstantPool->savanInstantPool);
		
		//echo  $http_response_header;
		$homepage = json_decode($homepage, true);
		
		//BACCARAT MB-01 INSTANT POOL
	echo '<div style="padding:8px 5px;background-color: #333333; color:white;"><b>BACCARAT MB-O1</b> <font color="#85BCD7">Shoe</font> : '.$homepage["params"]["poolList"][1]["shoeNo"].' <font color="#85BCD7">Game</font> : '.$homepage["params"]["poolList"][1]["gameNo"].'</div>';
		echo '<table class="example-table">
		<tbody><th>Account Id</th><th>Banker</th><th>Player</th><th>Tie</th><th>B Pair</th><th>P Pair</th><th>Big</th><th>Small</th><th>Currency</th><th>Win/Loss</th><th>Balance</th><th>IP</th>';
		
		$result_bet= $homepage["params"]["poolList"][1]["playerInfos"];
		if (Yii::app()->session['account_type']=='agent'){
			if (count($result_bet)>0){
// 				if (Yii::app()->session['account_id']==$account_id){
					$agent_counter=0;
					for ($i=0;$i<=count($result_bet)-1;$i++){
						
						$account_id='';
						switch (Yii::app()->session['level']) {
							case 'SC':
								$account_id	=	substr($result_bet[$i]['userName'],0,2);
								break;
							case 'SMA':
								$account_id	=	substr($result_bet[$i]['userName'],0,4);
								break;
							case 'MA':
								$account_id	=	substr($result_bet[$i]['userName'],0,6);
								break;
							case 'AGT':
								$account_id	=	substr($result_bet[$i]['userName'],0,8);
								break;
						}
						$bets=$result_bet[$i]['bets'];
						if($account_id == Yii::app()->session['account_id'] && strlen($result_bet[$i]['userName'])==10 ){
							echo '<tr>';
							echo '<td>'.$result_bet[$i]['userName'].'</td>';
							echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
							echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
							echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
							echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
							echo '<td>'.$result_bet[$i]['currency'].'</td>';
							echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
							echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
							echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
							echo '</tr>';
							$agent_counter+=1;
						}
					}
					if ($agent_counter!=0){
						echo '</tbody></table>';
					}else{
						echo '</tbody></table>';
						echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
					}
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}
			
		}else{
		
			if (count($result_bet)>0){
				for ($i=0;$i<=count($result_bet)-1;$i++){
					$bets=$result_bet[$i]['bets'];
					echo '<tr>';
					echo '<td>'.$result_bet[$i]['userName'].'</td>';
					echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
					echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
					echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
					echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
					echo '<td>'.$result_bet[$i]['currency'].'</td>';
					echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
					echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
					echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
					echo '</tr>';
						
				}
				echo '</tbody></table>';
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}

		}
		
		//BACCARAT MB-02 INSTANT POOL
		echo '<div style="padding:8px 5px;background-color: #333333; color:white;"><b>BACCARAT MB-O2</b> <font color="#85BCD7">Shoe</font> : '.$homepage["params"]["poolList"][2]["shoeNo"].' <font color="#85BCD7">Game</font> : '.$homepage["params"]["poolList"][2]["gameNo"].'</div>';
		echo '<table class="example-table">
		<tbody><th>Account Id</th><th>Banker</th><th>Player</th><th>Tie</th><th>B Pair</th><th>P Pair</th><th>Big</th><th>Small</th><th>Currency</th><th>Win/Loss</th><th>Balance</th><th>IP</th>';

	$result_bet= $homepage["params"]["poolList"][2]["playerInfos"];
		if (Yii::app()->session['account_type']=='agent'){
			if (count($result_bet)>0){
// 				if (Yii::app()->session['account_id']==$account_id){
					$agent_counter=0;
					for ($i=0;$i<=count($result_bet)-1;$i++){
						
						$account_id='';
						switch (Yii::app()->session['level']) {
							case 'SC':
								$account_id	=	substr($result_bet[$i]['userName'],0,2);
								break;
							case 'SMA':
								$account_id	=	substr($result_bet[$i]['userName'],0,4);
								break;
							case 'MA':
								$account_id	=	substr($result_bet[$i]['userName'],0,6);
								break;
							case 'AGT':
								$account_id	=	substr($result_bet[$i]['userName'],0,8);
								break;
						}
						$bets=$result_bet[$i]['bets'];
						if($account_id == Yii::app()->session['account_id'] && strlen($result_bet[$i]['userName'])==10 ){
							echo '<tr>';
							echo '<td>'.$result_bet[$i]['userName'].'</td>';
							echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
							echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
							echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
							echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
							echo '<td>'.$result_bet[$i]['currency'].'</td>';
							echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
							echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
							echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
							echo '</tr>';
							$agent_counter+=1;
						}
					}
					if ($agent_counter!=0){
						echo '</tbody></table>';
					}else{
						echo '</tbody></table>';
						echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
					}
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}
			
		}else{
		
			if (count($result_bet)>0){
				for ($i=0;$i<=count($result_bet)-1;$i++){
					$bets=$result_bet[$i]['bets'];
					echo '<tr>';
					echo '<td>'.$result_bet[$i]['userName'].'</td>';
					echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
					echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
					echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
					echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
					echo '<td>'.$result_bet[$i]['currency'].'</td>';
					echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
					echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
					echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
					echo '</tr>';
						
				}
				echo '</tbody></table>';
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}

		}
		
		//BACCARAT MB-03 INSTANT POOL
			echo '<div style="padding:8px 5px;background-color: #333333; color:white;"><b>BACCARAT MB-O3</b> <font color="#85BCD7">Shoe</font> : '.$homepage["params"]["poolList"][3]["shoeNo"].' <font color="#85BCD7">Game</font> : '.$homepage["params"]["poolList"][3]["gameNo"].'</div>';
		echo '<table class="example-table">
		<tbody><th>Account Id</th><th>Banker</th><th>Player</th><th>Tie</th><th>B Pair</th><th>P Pair</th><th>Big</th><th>Small</th><th>Currency</th><th>Win/Loss</th><th>Balance</th><th>IP</th>';
		
		
	$result_bet= $homepage["params"]["poolList"][3]["playerInfos"];
		if (Yii::app()->session['account_type']=='agent'){
			if (count($result_bet)>0){
// 				if (Yii::app()->session['account_id']==$account_id){
					$agent_counter=0;
					for ($i=0;$i<=count($result_bet)-1;$i++){
						
						$account_id='';
						switch (Yii::app()->session['level']) {
							case 'SC':
								$account_id	=	substr($result_bet[$i]['userName'],0,2);
								break;
							case 'SMA':
								$account_id	=	substr($result_bet[$i]['userName'],0,4);
								break;
							case 'MA':
								$account_id	=	substr($result_bet[$i]['userName'],0,6);
								break;
							case 'AGT':
								$account_id	=	substr($result_bet[$i]['userName'],0,8);
								break;
						}
						$bets=$result_bet[$i]['bets'];
						if($account_id == Yii::app()->session['account_id'] && strlen($result_bet[$i]['userName'])==10 ){
							echo '<tr>';
							echo '<td>'.$result_bet[$i]['userName'].'</td>';
							echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
							echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
							echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
							echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
							echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
							echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
							echo '<td>'.$result_bet[$i]['currency'].'</td>';
							echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
							echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
							echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
							echo '</tr>';
							$agent_counter+=1;
						}
					}
					if ($agent_counter!=0){
						echo '</tbody></table>';
					}else{
						echo '</tbody></table>';
						echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
					}
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}
			
		}else{
		
			if (count($result_bet)>0){
				for ($i=0;$i<=count($result_bet)-1;$i++){
					$bets=$result_bet[$i]['bets'];
					echo '<tr>';
					echo '<td>'.$result_bet[$i]['userName'].'</td>';
					echo (isset($bets['BC_BANKER'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER'])).'</td>' 				: '<td class="col2"></td>';
					echo (isset($bets['BC_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_TIE'])).'</td>' 				: '<td class="col3"></td>';
					echo (isset($bets['BC_BANKER_PAIR'])) 	? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BANKER_PAIR'])).'</td>' 		: '<td class="col1"></td>';
					echo (isset($bets['BC_PLAYER_PAIR'])) 	? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_PLAYER_PAIR'])).'</td>' 		: '<td class="col2"></td>';
					echo (isset($bets['BC_BIG'])) 			? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_BIG'])).'</td>' 				: '<td class="col1"></td>';
					echo (isset($bets['BC_SMALL'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['BC_SMALL'])).'</td>' 				: '<td class="col2"></td>';
					echo '<td>'.$result_bet[$i]['currency'].'</td>';
					echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
					echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
					echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
					echo '</tr>';
						
				}
				echo '</tbody></table>';
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}

		}
		
		// DRAGON-TIGER INSTANT POOL
	echo '<div style="padding:8px 5px;background-color: #333333; color:white;"><b>DRAGON TIGER DT-O1</b> <font color="#85BCD7">Shoe</font> : '.$homepage["params"]["poolList"][5]["shoeNo"].' <font color="#85BCD7">Game</font> : '.$homepage["params"]["poolList"][5]["gameNo"].'</div>';
		echo '<table class="example-table">
		<tbody><th>Account Id</th><th>Dragon</th><th>Tiger</th><th>Tie</th><th>Currency</th><th>Win/Loss</th><th>Balance</th><th>IP</th>';
		
		
		$result_bet= $homepage["params"]["poolList"][5]["playerInfos"];
		if (Yii::app()->session['account_type']=='agent'){
			if (count($result_bet)>0){
				// 				if (Yii::app()->session['account_id']==$account_id){
				$agent_counter=0;
				for ($i=0;$i<=count($result_bet)-1;$i++){
		
					$account_id='';
					switch (Yii::app()->session['level']) {
						case 'SC':
							$account_id	=	substr($result_bet[$i]['userName'],0,2);
							break;
						case 'SMA':
							$account_id	=	substr($result_bet[$i]['userName'],0,4);
							break;
						case 'MA':
							$account_id	=	substr($result_bet[$i]['userName'],0,6);
							break;
						case 'AGT':
							$account_id	=	substr($result_bet[$i]['userName'],0,8);
							break;
					}
					$bets=$result_bet[$i]['bets'];
					if($account_id == Yii::app()->session['account_id'] && strlen($result_bet[$i]['userName'])==10 ){
						echo '<tr>';
						echo '<td>'.$result_bet[$i]['userName'].'</td>';
						echo (isset($bets['DT_DRAGON'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['DT_DRAGON'])).'</td>' 			: '<td class="col1"></td>';
						echo (isset($bets['DT_TIGER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['DT_TIGER'])).'</td>' 			: '<td class="col2"></td>';
						echo (isset($bets['DT_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['DT_TIE'])).'</td>' 			: '<td class="col3"></td>';
						echo '<td>'.$result_bet[$i]['currency'].'</td>';
						echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
						echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
						echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
						echo '</tr>';
						$agent_counter+=1;
					}
				}
				if ($agent_counter!=0){
					echo '</tbody></table>';
				}else{
					echo '</tbody></table>';
					echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
				}
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}
				
		}else{
			if (count($result_bet)>0){
				for ($i=0;$i<=count($result_bet)-1;$i++){
					$bets=$result_bet[$i]['bets'];
					echo '<tr>';
					echo '<td>'.$result_bet[$i]['userName'].'</td>';
					echo (isset($bets['DT_DRAGON'])) 		? '<td class="col1">'.CV999Utility::formatMoney(money_format('%i',$bets['DT_DRAGON'])).'</td>' 			: '<td class="col1"></td>';
					echo (isset($bets['DT_TIGER'])) 		? '<td class="col2">'.CV999Utility::formatMoney(money_format('%i',$bets['DT_TIGER'])).'</td>' 			: '<td class="col2"></td>';
					echo (isset($bets['DT_TIE'])) 			? '<td class="col3">'.CV999Utility::formatMoney(money_format('%i',$bets['DT_TIE'])).'</td>' 			: '<td class="col3"></td>';
					echo '<td>'.$result_bet[$i]['currency'].'</td>';
					echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
					echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
					echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
					echo '</tr>';
						
				}
				echo '</tbody></table>';
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}	
		}
		
		//ROULETTE INSTANT POOL
	echo '<div style="padding:8px 5px;background-color: #333333; color:white;"><b>ROULETTE RL-O1</b> <font color="#85BCD7">Shoe</font> : '.$homepage["params"]["poolList"][11]["shoeNo"].' <font color="#85BCD7">Game</font> : '.$homepage["params"]["poolList"][11]["gameNo"].'</div>';
		echo '<table class="example-table">
		<tbody><th>Account Id</th><th>Number</th><th>Red</th><th>Black</th><th>Odd</th><th>Even</th><th>Big</th><th>Small</th><th>Currency</th><th>Win/Loss</th><th>Balance</th><th>IP</th>';
		
		
		$result_bet= $homepage["params"]["poolList"][11]["playerInfos"];
		if (Yii::app()->session['account_type']=='agent'){
			if (count($result_bet)>0){
				// 				if (Yii::app()->session['account_id']==$account_id){
				$agent_counter=0;
				for ($i=0;$i<=count($result_bet)-1;$i++){
		
					$account_id='';
					switch (Yii::app()->session['level']) {
						case 'SC':
							$account_id	=	substr($result_bet[$i]['userName'],0,2);
							break;
						case 'SMA':
							$account_id	=	substr($result_bet[$i]['userName'],0,4);
							break;
						case 'MA':
							$account_id	=	substr($result_bet[$i]['userName'],0,6);
							break;
						case 'AGT':
							$account_id	=	substr($result_bet[$i]['userName'],0,8);
							break;
					}
					$bets=$result_bet[$i]['bets'];
					if($account_id == Yii::app()->session['account_id'] && strlen($result_bet[$i]['userName'])==10 ){
						echo '<tr>';
						echo '<td>'.$result_bet[$i]['userName'].'</td>';
						echo $this->getRouletteRestult($bets);
						echo '<td>'.$result_bet[$i]['currency'].'</td>';
						echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
						echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
						echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
						echo '</tr>';
						$agent_counter+=1;
					}
				}
				if ($agent_counter!=0){
					echo '</tbody></table>';
				}else{
					echo '</tbody></table>';
					echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
				}
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}
		
		}else{
			if (count($result_bet)>0){
				for ($i=0;$i<=count($result_bet)-1;$i++){
					$bets=$result_bet[$i]['bets'];
					echo '<tr>';
					echo '<td>'.$result_bet[$i]['userName'].'</td>';
					echo $this->getRouletteRestult($bets);
					echo '<td>'.$result_bet[$i]['currency'].'</td>';
					echo ($result_bet[$i]['winLose']) < 0	? '<td><font color="red">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>':'<td><font color="black">'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['winLose'])).'</font></td>';
					echo '<td>'.CV999Utility::formatMoney(money_format('%i',$result_bet[$i]['balance'])).'</td>';
					echo '<td style="background-color:#FEFBD4">'.$result_bet[$i]['lastLoginIp'].'(<font color="#338FBD">'.Yii::app()->geoip->lookupCountryName($result_bet[$i]['lastLoginIp']).'</font>)</td>';
					echo '</tr>';
						
				}
				echo '</tbody></table>';
			}else{
				echo '</tbody></table>';
				echo'<div style="color:red;font-size:13px; padding-left:10px;padding-bottom:5px;padding-top:5px;background-color:#EEEEEE;">NO PLAYER!</div>';
			}
		}
// 		$homepage = file_get_contents(Constants::Url_MB_all);
// 		$homepage = json_decode($homepage, true);
// 		$i=1;
// 		while ($i<=5){
// 			if ($i==1){
// 				$filedNames=array("ACCTID","BANK","PLAYER","TIE","BANKPAIR","PLAYPAIR","BIG","SMALL","CURRENCYID","WINLOSS","CREDITQUANTITY","BALANCE","IP");
// 				echo '<div><b><font face="verdana">MB-01</font></b><div>';
// 				echo JsonUtil::jsonJqgridInstantPools($homepage["params"]["betDetail1"], 1, 1, 1,$filedNames);	
// 				echo '</br>';
// 			}elseif ($i==2){
// 				echo '<div><b><font face="verdana">MB-02</font></b><div>';
// 				$filedNames=array("ACCTID","BANK","PLAYER","TIE","BANKPAIR","PLAYPAIR","BIG","SMALL","CURRENCYID","WINLOSS","CREDITQUANTITY","BALANCE","IP");
// 				echo JsonUtil::jsonJqgridInstantPools($homepage["params"]["betDetail2"], 1, 1, 1,$filedNames);
// 				echo '</br>';
// 			}elseif ($i==3){
// 				echo '<div><b><font face="verdana">MB-03</font></b><div>';
// 				$filedNames=array("ACCTID","BANK","PLAYER","TIE","BANKPAIR","PLAYPAIR","BIG","SMALL","CURRENCYID","WINLOSS","CREDITQUANTITY","BALANCE","IP");
// 				echo JsonUtil::jsonJqgridInstantPools($homepage["params"]["betDetail3"], 1, 1, 1,$filedNames);
// 				echo '</br>';
// 			}elseif ($i==4){
// 				echo '<div><b><font face="verdana">RL-01</font></b><div>';;
// 				$filedNames=array("ACCTID","REDBET","BLACKBET","ODDBET","EVENBET","BIG","SMALL","DIRECTBET","SEPARATEBET","STREETBET","ANGLEBET","LINEBET","THREEBET","FOURBET","FRISTROW","SNDROW","THRROW","FRISTCOL","SNDCOL","THRCOL","CURRENCYID","WINLOSS","CREDITQUANTITY","BALANCE","IP");
// 				echo JsonUtil::jsonJqgridInstantPools($homepage["params"]["betDetail21"], 2, 1, 1,$filedNames);
// 				echo '</br>';
// 			}elseif ($i==5){
// 				echo '<div><b><font face="verdana">DT-01</font></b><div>';
// 				$filedNames=array("ACCTID","BANK","PLAYER","TIE","CURRENCYID","WINLOSS","CREDITQUANTITY","BALANCE","IP");
// 				echo JsonUtil::jsonJqgridInstantPools($homepage["params"]["betDetail41"], 3, 1, 1,$filedNames);
// 				echo '</br>';
// 			}
// 			$i++;
// 		}	
	}

	public function getRouletteRestult($param){
		$result	='';
		$red	='';
		$black	='';
		$odd	='';
		$even	='';
		$big	='';
		$small	='';
		$other	='<table>';
		
		foreach($param as $key => $value){
			//echo $key . ':' . $value .'<br/>';
			if ($key=='RL_RED'){
				$red	=	$value;
			}elseif ($key=='RL_BLACK'){
				$black	=	$value;
			}elseif ($key=='RL_ODD'){
				$odd	=	$value;
			}elseif ($key=='RL_EVEN'){
				$even	=	$value;
			}elseif ($key=='RL_BIG'){
				$big	=	$value;
			}elseif ($key=='RL_SMALL'){
				$small	=	$value;
			}else{
				$other.=	'<tr><td class="insideL">'.$key.'</td><td class="insideR">'.$value.'</td></tr>';
			}
		}
		$other.='</table>';
		return '<td style="padding:5px 5px !important;" class="col3">'.$other.'</td><td class="col1">'.$red.'</td><td class="col2">'.$black.'</td><td class="col1">'.$odd.'</td><td class="col2">'.$even.'</td><td class="col1">'.$big.'</td><td class="col2">'.$small.'</td>';
	}
	
	
	public function actionChecking()
	{
		echo '1';
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemMonitor.readInstantPools') || User::getUserType() === Constants::ROLE_AGENT){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
}