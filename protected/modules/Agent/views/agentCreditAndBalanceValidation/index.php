<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agentcreditandbalancevalidation.css" />
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.7.2/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript">
	function tableAgentCreditAndBalanceValidation(ID) { 
		document.getElementById('qry_result').innerHTML='';
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentCreditAndBalanceValidation/CreditAndBalance&account_id='+ID, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ["Account ID","Agent Type","Currency","Available Credit","Remaining Balance","Assigned Credit","Total Downline Credit","Balance Validation","Amount","Withdraw","Deposit","Status"],
			    colModel: [
			      {name: 'account_id', index: 'account_id', width: 100, sortable: true,},
			      {name: 'agent_type', index: 'agent_type', width: 100,sortable: true},
			      {name: 'currency_name', index: 'currency_name', width: 100,sortable: true},
			      {name: 'available_credit', index: 'available_credit', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'balance', index: 'balance', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'assigned_credit', index: 'assigned_credit', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'total_downline_credit', index: 'total_downline_credit', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'balance_validation', index: 'balance_validation', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'amount', index: 'amount', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'withdraw', index: 'withdraw', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'deposit', index: 'deposit', width: 120,sortable: true, align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'return_value', index: 'return_value', width: 100,sortable: true,cellattr: function (rowId, tv, rawObject, cm, rdata) { return tv == 'Passed' ? 'style="color:blue;"' : 'style="color:red;"';}},
			    ],
			    loadComplete: function() {
			        //$("tr.jqgrow:odd").css("background", "#DDDDDC");
			        //$("tr.jqgrow:even").css("background", "#ffffff");
			    },
			    loadtext:"",
                rownumbers: true,
                sortable: false,
			    sortname: 'account_id',
			    sortorder: 'ASC',
			    caption: 'Agent credit and balance validation',
			    hidegrid: false,
			    viewrecords: true,
			    pager: '#pager1',
			   	rowNum: 25,	
			    rowList: [25, 50, 100, 200, 99999],
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search:false,refresh:false});
			$("#list1").jqGrid('setGroupHeaders', {
				  useColSpanStyle: true, 
				  groupHeaders:[
					{startColumnName: 'amount', numberOfColumns: 3, titleText: 'Transaction History'},
				  ]
			});
		});
	} 
	function tableAgentPlayerCreditValidation(ID) { 
		document.getElementById('qry_result_player').innerHTML='';
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list2'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result_player").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager2'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result_player").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#list2");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentCreditAndBalanceValidation/AgentPlayerCreditValidation&account_id='+ID, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ["Agent ID","Account ID","Credit","Balance","Amount","Withdraw","Deposit","Status"],
			    colModel: [
				  {name: 'agent_id', index: 'agent_id', width: 100, sortable: false, align:"right",hidden: true},
			      {name: 'account_id', index: 'account_id', width: 100, sortable: true,align:"right",},
			      {name: 'available_credit', index: 'available_credit', width: 120,sortable: true,summaryType:'sum', align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="credit">{0}</label>'},
			      {name: 'balance', index: 'balance', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'amount', index: 'amount', width: 120,sortable: true,summaryType:'sum', align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="amount">{0}</label>'},
			      {name: 'withdraw', index: 'withdraw', width: 120,sortable: true,summaryType:'sum', align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="withdraw">{0}</label>'},
			      {name: 'deposit', index: 'deposit', width: 120,sortable: true,summaryType:'sum', align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="deposit">{0}</label>'},
			      {name: 'return_value', index: 'return_value', width: 100,sortable: true,summaryType:'sum',cellattr: function (rowId, tv, rawObject, cm, rdata) { return tv == 'Passed' ? 'style="color:blue;"' : 'style="color:red;"';},summaryTpl:'<label class="stat">{0}</label>'},
			    ],
			    loadComplete: function() {
			        //$("tr.jqgrow:odd").css("background", "#DDDDDC");
			        //$("tr.jqgrow:even").css("background", "#ffffff");
			        var stat = document.getElementsByClassName("stat");
			        var amount = document.getElementsByClassName("amount");
			        var x=0;
			         $(".credit").each(function(i) {
				         if(this.innerHTML!=amount[x].innerHTML){
			        	 	stat[x].innerHTML='Failed';
				         }else{
				        	stat[x].innerHTML='<font color="blue">Passed</font>';
					     }
			        	 x=x+1;
			         });
			    },
			    loadtext:"",
                rownumbers: true,
                sortable: false,
			    sortname: 'account_id',
			    sortorder: 'ASC',
			    caption: 'Agent Player credit validationqq',
			    hidegrid: false,
			    viewrecords: true,
			    grouping: true,
			   	groupingView : {
			   		groupField : ['agent_id'],
			   		groupColumnShow : [true],
			   		groupText : ['<b>{0}</b>'],
			   		groupCollapse : false,
					groupOrder: ['asc'],
					groupSummary : [true],
			   	},
			    pager: '#pager2',
			   	rowNum: 800,	
			    rowList: [25, 50, 100, 200, 99999],
			});
			$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:false,refresh:false});
		});
	    $("#list2").jqGrid('setGroupHeaders', {
			  useColSpanStyle: true, 
			  groupHeaders:[
				{startColumnName: 'agent_id', numberOfColumns: 4, titleText: 'Agent Player'},
				{startColumnName: 'amount', numberOfColumns: 4, titleText: 'Transaction History'},
			  ]
		});
	} 
	function tableDuplicateAgentTransHistory() { 
		document.getElementById('qry_agent_trans_history').innerHTML='';
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list3'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_agent_trans_history").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager3'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_agent_trans_history").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#list3");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentCreditAndBalanceValidation/DuplicateAgentTransHistory', 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ["agent_account_id","transaction_date","transaction_number","trans_type_id","currency_id","amount","credit_before","credit_after","balance_before","balance_after","upper_agent_credit_before","upper_agent_credit_after","operator_id"],
			    colModel: [
				  {name: 'agent_account_id', index: 'agent_account_id', width: 100, sortable: false, align:"right",hidden: false},
			      {name: 'transaction_date', index: 'transaction_date', width: 100, sortable: true,align:"right",},
			      {name: 'transaction_number', index: 'transaction_number', width: 120,sortable: true, align:"right"},
			      {name: 'trans_type_id', index: 'trans_type_id', width: 120,sortable: true},
			      {name: 'currency_id', index: 'currency_id', width: 120,sortable: true,},
			      {name: 'amount', index: 'amount', width: 120,sortable: true,summaryType:'sum', align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="withdraw">{0}</label>'},
			      {name: 'credit_before', index: 'credit_before', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'credit_after', index: 'credit_after', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'balance_before', index: 'balance_before', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'balance_after', index: 'balance_after', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'upper_agent_credit_before', index: 'upper_agent_credit_before', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'upper_agent_credit_after', index: 'upper_agent_credit_after', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'operator_id', index: 'operator_id', width: 120,sortable: true,},
			    ],
			    loadtext:"",
                rownumbers: true,
                sortable: false,
			    sortname: 'account_id',
			    sortorder: 'ASC',
			    caption: 'Agent Transaction History',
			    hidegrid: false,
			    viewrecords: true,
			    pager: '#pager3',
			   	rowNum: 25,	
			    rowList: [25, 50, 100, 200, 99999],
			});
			$('#list3').jqGrid('navGrid', '#pager3', {edit: false, add: false, del:false, search:false,refresh:false});
		});
	} 
	function tableDuplicateAgentPlayerTransHistory() { 
		document.getElementById('qry_agent_player_trans_history').innerHTML='';
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list4'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_agent_player_trans_history").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager4'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_agent_player_trans_history").appendChild(divTag1);
	    
	    $(document).ready(function() {
			var grid=jQuery("#list4");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentCreditAndBalanceValidation/DuplicateAgentPlayerTransHistory', 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ["agent_account_id","transaction_date","transaction_number","trans_type_id","currency_id","amount","credit_before","credit_after","balance_before","balance_after","upper_agent_credit_before","upper_agent_credit_after","operator_id"],
			    colModel: [
				  {name: 'agent_account_id', index: 'agent_account_id', width: 100, sortable: false, align:"right",hidden: false},
			      {name: 'transaction_date', index: 'transaction_date', width: 100, sortable: true,align:"right",},
			      {name: 'transaction_number', index: 'transaction_number', width: 120,sortable: true, align:"right"},
			      {name: 'trans_type_id', index: 'trans_type_id', width: 120,sortable: true},
			      {name: 'currency_id', index: 'currency_id', width: 120,sortable: true,},
			      {name: 'amount', index: 'amount', width: 120,sortable: true,summaryType:'sum', align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','},summaryTpl:'<label class="withdraw">{0}</label>'},
			      {name: 'credit_before', index: 'credit_before', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'credit_after', index: 'credit_after', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'balance_before', index: 'balance_before', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'balance_after', index: 'balance_after', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'upper_agent_credit_before', index: 'upper_agent_credit_before', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'upper_agent_credit_after', index: 'upper_agent_credit_after', width: 120,sortable: true, summaryType:'sum',align:"right",formatter:'currency',formatoptions: {thousandsSeparator:','}},
			      {name: 'operator_id', index: 'operator_id', width: 120,sortable: true,},
			    ],
			    loadtext:"",
                rownumbers: true,
                sortable: false,
			    sortname: 'account_id',
			    sortorder: 'ASC',
			    caption: 'Agent Player Transaction History',
			    hidegrid: false,
			    viewrecords: true,
			    pager: '#pager4',
			   	rowNum: 25,	
			    rowList: [25, 50, 100, 200, 99999],
			});
			$('#list4').jqGrid('navGrid', '#pager4', {edit: false, add: false, del:false, search:false,refresh:false});
		});
	} 
	function submitQuery(ID){
		tableAgentCreditAndBalanceValidation(ID);
		tableAgentPlayerCreditValidation(ID);
		tableDuplicateAgentTransHistory();
		tableDuplicateAgentPlayerTransHistory();
	}
	function submitAgentID(ID){
		jQuery.ajax({
			url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Agent/AgentCreditAndBalanceValidation/CreditAndBalance',
			type: 'POST',
			data: {'account_id': ID},
			success: function(data){
				var dblVal= data.split(';');
				$('#lblAgentType')[0].innerHTML=dblVal[0];
				$('#lblCurrencyName')[0].innerHTML=dblVal[1];
				$('#lblAvailableCredit')[0].innerHTML=dblVal[2];
				$('#lblRemainingBalance')[0].innerHTML=dblVal[3];
				$('#lblAssignedCredit')[0].innerHTML=dblVal[4];
				$('#lblAvailableDownlineCredit')[0].innerHTML=dblVal[5];
	    	}
		});
	}
	</script>
</head>
<body>
	<b>Agent Credit and Balance Validation</b>
	<div id="body_wrapper">
		<b>Agent ID:</b> <input type="text" id="txtAgentID"/><input type="button" id="btnSubmit" value="Submit" onclick="javascript: submitQuery(document.getElementById('txtAgentID').value);"><br/><br/>
		<div id="qry_result"></div>
		<div id="qry_result_player"></div>
		<div id="qry_agent_trans_history"></div>
		<div id="qry_agent_player_trans_history"></div>
	</div>
</body>
</html>