<?php
/**
 * @TODO store all session variables CV999
 * @copyright Creative Entertainment Ltd.
 * @author sitthykun
 * @since 2012-04-05
 */

class CV999OnlinePlayersYii extends CV999RedisCore {
	private $lobby;
	private $lobbyPlayer;
	private $lobbyAgentPlayer;
	private $lobbyCashPlayer;
	private $login;
	private $apiTrans;

	/**
	 * 
	 */
	public function CV999OnlinePlayersYii() {
		// 
		parent::CV999RedisCore();

		// initialize
		$this->lobby = array(
				'0' =>  parent::LOBBY . ':*',
				'1' =>  parent::LOBBY . ':*:a:*',
				'2' =>  parent::LOBBY . ':*:c:*',
// 				'3' =>  parent::LOBBY . ':1:*',
// 				'4' =>  parent::LOBBY . ':1:a:*',
// 				'5' =>  parent::LOBBY . ':1:c:*',
// 				'6' =>  parent::LOBBY . ':2:*',
// 				'7' =>  parent::LOBBY . ':2:a:*',
// 				'8' =>  parent::LOBBY . ':2:c:*',
// 				'9' =>  parent::LOBBY . ':3:*',
// 				'10' => parent::LOBBY . ':3:a:*',
// 				'11' => parent::LOBBY . ':3:c:*',
// 				'12' => parent::LOBBY . ':4:*',
// 				'13' => parent::LOBBY . ':4:a:*',
// 				'14' => parent::LOBBY . ':4:c:*'
		);

		// 
		$this->lobbyAgentPlayer	= parent::LOBBY . ':*:a:';
		$this->lobbyCashPlayer	= parent::LOBBY . ':*:c:';
		$this->lobbyPlayer		= parent::LOBBY . ':*:*:';

		// 
		$this->login = array(
				'0' => parent::LOGIN . ':*',
				'1' => parent::LOGIN . ':a:*',
				'2' => parent::LOGIN . ':a:',
				'3' => parent::LOGIN . ':c:*',
				'4' => parent::LOGIN . ':c:*'
		);

		// api:eff:trans:1:a:TESTCEDEV3
		$this->apiTrans = array(
				'0'	=>	parent::TRANS . ':*',
				'1'	=>	parent::TRANS . ':*:a:*',
				'2'	=>	parent::TRANS . ':*:c:*',
				'3'	=>	parent::TRANS . ':1:a:*',
				'4'	=>	parent::TRANS . ':1:c:*',
				'5'	=>	parent::TRANS . ':2:a:*',
				'6'	=>	parent::TRANS . ':2:c:*',
				'7'	=>	parent::TRANS . ':3:a:*',
				'8'	=>	parent::TRANS . ':3:c:*',
				'9'	=>	parent::TRANS . ':4:a:*',
				'10'=>	parent::TRANS . ':4:c:*',
				'11'=>	parent::TRANS . ':*:*'
		);
	}

	/**
	 * 
	 * @param string $playerName
	 */
	public function checkAPITransByPlayer($playerName) {
		$key = $this->redis->keys($this->apiTrans['11'] . ':' . $playerName);

		return isset($key) && isset($key[0]) ? 1 : 0;
	}

	/**
	 *
	 * @param string $playerName
	 */
	public function deleteAgentLobbyByPlayer($playerName) {
		$this->redis->del($this->lobbyAgentPlayer . $playerName);
	}

	/**
	 *
	 * @param string $playerName
	 */
	public function deleteCashLobbyByPlayer($playerName) {
		$this->redis->del($this->lobbyCashPlayer . $playerName);
	}
	
	/**
	 *
	 * @param string $playerName
	 */
	public function deleteLobbyByPlayer($playerName) {
		$key = $this->redis->keys($this->lobby['0'] . ':' . $playerName);
		//
		if (isset($key[0])) {
			$this->redis->del($key[0]);
		}
	}
	
	/**
	 *
	 * @param string $playerName
	 */
	public function deleteAgentLoginByPlayer($playerName) {
		$this->redis->del($this->login['2'] . $playerName);
	}
	
	/**
	 *
	 * @param string $playerName
	 */
	public function deleteCashLoginByPlayer($playerName) {
		$this->redis->del($this->login['4'] . $playerName);
	}
	/**
	 *
	 * @param string $playerName
	 */
	public function deleteLoginByPlayer($playerName) {
		$key = $this->redis->keys($this->login['0'] . ':' . $playerName);
		//
		if (isset($key[0])) {
			$this->redis->del($key[0]);
		}
	}
	/**
	 * 
	 * @param string $casinoID
	 * @param string $type
	 */
	public function getPlayersOnLobby($casinoID = '*', $type = '*') {
		$query = parent::LOBBY . ':';
		if ($casinoID != '*') {
			if ($type != '*') {
				$query = $query . $casinoID . ':' . $type . ':*';
			} else {
				$query = $query . $casinoID . ':*';
			}
		} else {
			if ($type != '*') {
				$query = $query . '*:' . $type . ':*';
			} else {
				$query = $query . '*';
			}
		}
		//
		return $this->redis->keys($query);
	}

	/**
	 *
	 * @param string $casinoID
	 * @param string $type
	 */
	public function getPlayersOnLobbyJSON($casinoID = '*', $type = '*') {
		return json_encode($this->getPlayersOnLobby($casinoID, $type));
	}

	/**
	 *
	 * @param string/integer $casinoID
	 * @param string/integer $type
	 */
	public function countPlayersOnLobby($casinoID = '*', $type = '*') {
		return count($this->getPlayersOnLobby($casinoID, $type));
	}
	
	/**
	 * @todo Online History Model
	 * @copyright CE
	 * @author Leo karl
	 * @since 11/22/2012
	 */
	public function getAllAgentPlayersJSONToJQGrid($agentName){
		$loginStore = $this->getUnderAgent($agentName);
		$list ="";
		$num=1;
		
		// login
		foreach ($loginStore as $login) {
			list($casinoStatus, $playerType, $playerName) = explode(':', $login);
			list($dtDate, $strIP) = $this->getDateIPByKey($login);
			list($site) = $this->getSiteByKey($login);
			// convert microtime to datetime
			$dtDate = date('Y-m-d H:i:s', $dtDate);
			$list.='{"id":"'. $num . '","cell":["'.$playerName.'","'.$playerType.'","'.$dtDate.'","' . $this->getCasinoIDByPlayer($playerName) . '","","'.$strIP.'","<label style=\'color:#006666;\'>"'.Yii::app()->geoip->lookupCountryName($strIP).'"</label>","<a href=\"'.$site.'\">'.$site.'</a>"]},';
			$num++;
		}
		
		return $list;
	}
	

	public function getAllPlayersJSONToJQGrid() {
		$lobbyStore = $this->getAllPlayersOnLobby();
		$loginStore = $this->getAllPlayersOnSite();
		$list ="";
		
		$num=1;
		// login			
		foreach ($loginStore as $login) {
			list($casinoStatus, $playerType, $playerName) = explode(':', $login);
			list($dtDate, $strIP) = $this->getDateIPByKey($login);
			list($site) = $this->getSiteByKey($login);
			// convert microtime to datetime
			if (strlen($dtDate) > 3) {
				$dtDate = date('Y-m-d H:i:s', $dtDate);
			} else {
				$dtDate = '';
			}
			
			$list.='{"id":"'. $num . '","cell":["'.$playerName.'","'.$playerType.'","'.$dtDate.'","' . $this->getCasinoIDByPlayer($playerName) . '","","'.$strIP.'","'.Yii::app()->geoip->lookupCountryName($strIP).'","<a href=\"'.$site.'\">'.$site.'</a>"]},';
			$num++;
		}

		return $list;
	}
	/*
	public function getAllPlayersJSONToJQGrid($playerNameLabel, $playerTypeLabel, $casinoStatusLabel, $casinoIDLabel, $dateLabel, $ipLabel) {
	
		$lobbyStore = $this->getAllPlayersOnLobby();
		$loginStore = $this->getAllPlayersOnSite();
		$list = array();
	
		// lobby
		foreach ($lobbyStore as $lobby) {
	
			list($casinoStatus, $casinoID, $playerType, $playerName) = explode(':', $lobby);
			$num = count($list);
			$list[$num] = array();
	
			$list[$num][$playerNameLabel]	= $playerName;
			$list[$num][$playerTypeLabel]	= $playerType;
			$list[$num][$casinoStatusLabel]	= $casinoStatus;
			$list[$num][$casinoIDLabel]		= $casinoID;
			$list[$num][$dateLabel]			= '';
			$list[$num][$ipLabel]			= '';
		}
	
		// login
		foreach ($loginStore as $login) {
			list($casinoStatus, $playerType, $playerName) = explode(':', $login);
			//
			$num = count($list);
			$list[$num] = array();
			//
			$list[$num][$playerNameLabel]		= $playerName;
			$list[$num][$playerTypeLabel]		= $playerType;
			$list[$num][$casinoStatusLabel]		= $casinoStatus;
			$list[$num][$casinoIDLabel]			= '0';
			list($list[$num][$dateLabel], $list[$num][$ipLabel]) = $this->getDateIPByKey($login);
			// convert microtime to datetime
			$list[$num][$dateLabel] = date('Y-m-d H:i:s', $list[$num][$dateLabel]);
		}
	
		return json_encode($list);
	}
	*/
	/**
	 * 
	 * @param string $playerName
	 */
	public function getCasinoIDByPlayer($playerName) {
		$key = $this->redis->keys($this->lobbyPlayer . $playerName);

		if (isset($key[0])) {
			list($unuseful, $casinoID) = explode(':', $key[0]);
			return $casinoID;
		}

		return 0;
	}

	/**
	 *
	 * @param string $agentName
	 * @return array object
	 */
	public function getUnderAgent($agentName) {
		return $this->redis->keys($this->login[2] . $agentName . '*');
	}
	/**
	 *
	 * @param string $agentName
	 * @return count
	 */
	public function getCountUnderAgent($agentName) {
		return count($this->getUnderAgent($agentName));
	}
	
	/**
	 * 
	 */
	public function getDateIPByKey($key) {
		return $this->redis->hmget($key, 'dt', 'ip');
	}
	
	public function getSiteByKey($key) {
		return $this->redis->hmget($key, 'site');
	}
	
	/**
	 *
	 */
	public function countAllPlayersOnSite() {
		return count($this->getAllPlayersOnSite());
	}

	public function getAllPlayersOnSite() {
		return $this->redis->keys($this->login['0']);
	}
	
	/**
	 * 
	 */
	public function countAllAgentPlayersOnSite() {
		//$this->redisLogin->get
		return count($this->getAllAgentPlayerOnSite());
	}

	public function getAllAgentPlayerOnSite() {
		return $this->redis->keys($this->login['1']);
	}
	
	/**
	 *
	 */
	public function countAllCashPlayersOnSite() {
		return count($this->getAllCashPlayerOnSite());
	}

	/**
	 * 
	 */
	public function getAllCashPlayerOnSite() {
		return $this->redis->keys($this->login['2']);
	}
	
	/**
	 *
	 */
	public function countAllPlayersOnLobby() {
		return count($this->getAllPlayersOnLobby());
	}

	/**
	 * 
	 */
	public function getAllPlayersOnLobby() {
		return $this->redis->keys($this->lobby['0']);
	}
	
	/**
	 *
	 */
	public function countAllAgentPlayersOnLobby() {
		return count($this->getAllAgentPlayerOnLobby());
	}

	/**
	 * 
	 */
	public function getAllAgentPlayerOnLobby() {
		return $this->redis->keys($this->lobby['1']);
	}
	
	/**
	 *
	 */
	public function countAllCashPlayersOnLobby() {
		return count($this->getAllCashPlayersOnLobby());
	}

	/**
	 * 
	 */
	public function getAllCashPlayersOnLobby() {
		return $this->redis->keys($this->lobby['2']);
	}
	
	/**
	 * 
	 * @param string $playerName
	 * @return boolean
	 */
	public function isExistingOnSiteByPlayer($playerName) {
		$key = $this->redis->keys($this->login['0'] . ':' . $playerName);
		return isset($key) && isset($key[0]) ? 1 : 0;
	}

	/**
	 * 
	 * @param string $playerName
	 * @return boolean
	 */
	public function isExistingOnLobbyByPlayer($playerName) {
		$key = $this->redis->keys($this->lobby['0'] . ':' . $playerName);
		return isset($key) && isset($key[0]) ? 1 : 0;
	}

}

?>