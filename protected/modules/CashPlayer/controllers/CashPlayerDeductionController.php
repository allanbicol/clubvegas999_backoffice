<?php

class CashPlayerDeductionController extends MyController
{
	private $onlinePlayer;
	
	public function actionProcessWithdrawalRequest()
	{
		$withdrawal = new RedisLobbyManager();
		$withdrawal->ProcessWithdrawalRequest($_POST['accountID']);
	
	}
	
	public function actionCheckPlayerOnlineState()
	{

		if (Yii::app()->session['account_id']!=null){
				
			$this->onlinePlayer=new RedisManager();
			$checkLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['accountID']);//($_POST['AccountID']);
				
			$casinoId=$this->onlinePlayer->getCasinoIDByPlayer($_POST['accountID']);
				
	
			if ($checkLobby!=0){
				echo $casinoId;
			}else{
					
				$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
				$rd=$command->queryRow();
				if ($rd['deposit_withdrawal'] == 1) // display withdrawal wating message.
				{
					echo'w';
				}else {
					//display deposit dialog
					echo '0';
				}
			}
		}
		else
		{
			echo 'die';
		}
	}
	
	public function actionDeduct()
	{
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		
		//$postPlayerUpdateBalance=new CashPlayer;
		$postPlayerBalanceHistory=new CashPlayerDeduction;
		$postPlayerTransHistory=new CashPlayerDeductionTransHistory;
		$postLog=new TableLog;
		
		if($_POST['Amount'] <=0){
			exit('Invalid value entered.');
		}
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT balance from tbl_cash_player  where account_id='".$_POST["accountID"]."'");
		$rd=$command->queryRow();
		
 		$dateTime = date('Y-m-d H:i:s');
 		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerDeduct'))
 		{
	 		if ($rd['balance'] >= $_POST['Amount']){
		 		$balanceTotal= $rd['balance'] - $_POST['Amount'];
		 		$transNum= TransactionNumber::generateTransactionNumber("DE");
				
			
				$postPlayerBalanceHistory->cash_player_account_id=$_POST['accountID'];
				$postPlayerBalanceHistory->balance=$_POST['Amount'];
				$postPlayerBalanceHistory->trans_type_id=11;
				$postPlayerBalanceHistory->trans_date=$dateTime;
				$postPlayerBalanceHistory->transaction_number=$transNum;
						
				TableCashPlayer::model()->updateAll(array(
						'balance'=>$balanceTotal,'last_update_balance'=>$dateTime),
						'account_id="'.$_POST['accountID'].'"');
				
				$postPlayerTransHistory->player_id=$_POST['accountID'];
				$postPlayerTransHistory->trans_number=$transNum;
				$postPlayerTransHistory->cash_player_trans_type_id=11;
				$postPlayerTransHistory->currency_id=$_POST['currency_id'];
				$postPlayerTransHistory->amount=$_POST['Amount'];
				$postPlayerTransHistory->balance_before=$rd['balance'];
				$postPlayerTransHistory->balance_after=$balanceTotal;
				$postPlayerTransHistory->submit_time=$dateTime;
				$postPlayerTransHistory->finish_time=$dateTime;
				$postPlayerTransHistory->cashier_id=Yii::app()->session['account_id'];
				$postPlayerTransHistory->status_id=6;
				
				$dateTime = date('Y-m-d H:i:s');
				
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_POST['accountID'];
				$postLog->operated_level='Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=15;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> Deduct balance:<label style=\"color:red\">'.number_format($_POST['Amount'],2,'.',',').'</label></b>';
				
				$postPlayerBalanceHistory->save();
				//$postPlayerUpdateBalance->save();
				$postPlayerTransHistory->save();
				$postLog->save();
				echo 'Deduction process complete!';
	 		}
	 		else{
	 			echo 'Amount entered is greater than the balance.';
	 		}
		//header('location: ' . Yii::app()->request->baseUrl.'/index.php?r=CashPlayer/CashPlayerList');
 		}else{
 			echo 'You dont have a permission to do this operation!';
 		}
	} 
	 
	
}
