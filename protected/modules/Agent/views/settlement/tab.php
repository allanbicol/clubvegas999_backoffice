<script>
	//active menu color
	document.getElementById('agentHeader').className="start active";
	document.getElementById('mnu_agent_settlement_report').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Agent System <i class='icon-angle-right'></i></li><a href='#'>Settlement Report</a></li>");
</script>

<?php
$winloss =$this->renderPartial('application.modules.Agent.views.settlementWinLoss.index',null,TRUE);
$winlossDetails =$this->renderPartial('application.modules.Agent.views.settlementWinLoss.winlossdetails',null,TRUE);
$slotwinloss =$this->renderPartial('application.modules.Agent.views.settlementSlotWinLoss.index',null,TRUE);
$slotwinlossDetails =$this->renderPartial('application.modules.Agent.views.settlementSlotWinLoss.slotwinlossdetails',null,TRUE);
$kickoffPlayer =$this->renderPartial('application.modules.Agent.views.settlement.onlineuser',null,TRUE);

$account_type=Yii::app()->session['level'];
$account_id=Yii::app()->session['account_id'];

if (Yii::app()->session['level']=='SC')
{
	$oneClickTransfer=$this->renderPartial('application.modules.Agent.views.settlementOneClickTransfer.seniorMaster',null,TRUE);
}
elseif (Yii::app()->session['level']=='SMA')
{
	$oneClickTransfer=$this->renderPartial('application.modules.Agent.views.settlementOneClickTransfer.master',null,TRUE);
}
elseif (Yii::app()->session['level']=='MA')
{
	$oneClickTransfer=$this->renderPartial('application.modules.Agent.views.settlementOneClickTransfer.agent',null,TRUE);
}
elseif (Yii::app()->session['level']=='AGT')
{
	$oneClickTransfer=$this->renderPartial('application.modules.Agent.views.settlementOneClickTransfer.agentPlayer',null,TRUE);
}

//echo "<center><h1>Settlement Report</h1></center>";

if (User::getUserType() === 'agent')
{

	$this->widget('zii.widgets.jui.CJuiTabs', array(

		'tabs' => array(
				'KickOff Player'=>array('content'=>$kickoffPlayer, 'id'=>'tadKickOff'),
				'Win/Loss'=>array('content'=>$winloss, 'id'=>'tadWinloss'),
				'Win/Loss Details'=>array('content'=>$winlossDetails, 'id'=>'tadWinlossDetails'),
				'Slot Win/Loss'=>array('content'=>$slotwinloss, 'id'=>'tadSlotWinloss'),
				'Slot Win/Loss Details'=>array('content'=>$slotwinlossDetails, 'id'=>'tadSlotWinlossDetails'),
				'One Click Transfer'=>array('content'=>$oneClickTransfer, 'id'=>'tadOneClick'),
		),
		// additional javascript options for the tabs plugin
		'options' => array(
				'collapsible' => false,
		),
		// set id for this widgets
		'id'=>'MyTab',
	));
}
else 
{
	$this->widget('zii.widgets.jui.CJuiTabs', array(
	
			'tabs' => array(
					'Win/Loss'=>array('content'=>$winloss, 'id'=>'tadWinloss'),
					'Win/Loss Details'=>array('content'=>$winlossDetails, 'id'=>'tadWinlossDetails'),
					'Slot Win/Loss'=>array('content'=>$slotwinloss, 'id'=>'tadSlotWinloss'),
					'Slot Win/Loss Details'=>array('content'=>$slotwinlossDetails, 'id'=>'tadSlotWinlossDetails'),
			),
			// additional javascript options for the tabs plugin
			'options' => array(
					'collapsible' => false,
			),
			// set id for this widgets
			'id'=>'MyTab',
	));
}
