<?php


class CashPlayerSetBonusController extends MyController
{
	private $onlinePlayer;
	
	/**
	 * @todo save player bonus
	 * @author leokarl
	 * @since 2012-12-21
	 */
	public function actionSetPlayerBonus(){
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerSetBonus'))
		{
			// initialize
			//$checkLobby = new RedisManager();
			$model = new CashPlayerBonus();
		
			// data validation
			if(!isset($_POST['account_id'])){
				exit('invalid_submit_method');
			}
			if(!isset($_POST['amount'])){
				exit('invalid_submit_method');
			}
			if($model->isSpecialCharPresent($_POST['amount'])){
				exit('invalid_bonus');
			}
			if(!is_numeric($_POST['amount'])){
				exit('invalid_bonus');
			}
			if ($_POST['amount'] < 0){
				// remove bonus value can't be lesser than the remaining bonus
				if (($_POST['amount'] * -1) > $model->getCashPlayerFieldValueById('bonus',$_POST['account_id'])){
					exit('insufficient_bonus');
				}
			}
			if($_POST['amount'] == 0){
				exit('invalid_bonus');
			}
			if(!$model->isAccountIdExist($_POST['account_id'])){
				exit('acount_doesnt_exist');
			}
			if(!isset($_POST['bonus_type'])){
				exit('invalid_bonus_type');
			}
			if(trim($_POST['bonus_type'])==''){
				exit('select_bonus_type');
			}
			if(!$model->isBonusTypeValid($_POST['bonus_type'])){
				exit('invalid_bonus_type');
			}
//			if ($checkLobby->isExistingOnLobbyByPlayer($_POST['account_id']) <> 0){
//				exit('player_on_the_lobby');
//			}
		
			// continue saving
			$model->saveSetPlayerBonus($_POST['account_id'], $_POST['amount'], $_POST['bonus_type']);
			exit('set_bonus_complete');
			
		}else{
			
			exit('no_permission');
		}
	}
	
	/**
	 * @todo check player's online status
	 * @author leokarl
	 * @since 2012-12-21
	 */
	public function actionCheckPlayerOnlineStatus(){
		if(!isset(Yii::app()->session['account_id'])){
			exit('die');
		}
		
		if(!Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerSetBonus')){
			exit('no_permission');
		}
		// initialize
		//$redis = new RedisManager();
		$checkLobby = 0;//$redis->isExistingOnLobbyByPlayer($_POST['accountID']);
                
		//$casinoId = $redis->getCasinoIDByPlayer($_POST['accountID']);
	
		if ($checkLobby!=0){
			echo $casinoId;
		}
		else
		{
			$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer
					WHERE account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
			$rd = $command->queryRow();
			if ($rd['deposit_withdrawal'] == 1)
			{
				// display wating message.
				echo 'w';
			}else {
				
				// lock player before display bonus dialog
				TableCashPlayer::model()->updateAll(array(
				'kick_off'=>3),
				'account_id="'.$_POST["accountID"].'"');
				
				//display setbonus dialog
				echo '0';
			}
		}
	
	}
	
	/**
	 * @todo process withdraw request
	 * @author allan
	 */
	public function actionProcessWithdrawalRequest()
	{
		$withdrawal = new RedisLobbyManager();
		$withdrawal->ProcessWithdrawalRequest($_POST['accountID']);
	
	}
	
	/**
	 * @todo setbonus (old function)
	 * @author allan
	 */
	public function actionSetbonus()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerSetBonus'))
		{
			
			if (isset($_POST['task'])==''){
				exit;
			
			}else{
				if($_POST['task']=='confirmBonus'){
					
					$command1 = Yii::app()->db->createCommand("SELECT TW.deposit_withdrawal,TC.casino_name from tbl_player_wallet_transfer  as TW INNER JOIN tbl_casino as TC ON TC.id=TW.casino_id where account_id='".$_POST["accountID"]."'");
					$rd1=$command1->queryRow();
					$this->onlinePlayer=new RedisManager();
					$checkLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['accountID']);
					
					
					if ($checkLobby ==0) // check player on lobby
					{
						
						$postPlayerBonus = new CashPlayerSetBonus;
						$postPlayerBonusHistory=new CashPlayerSetBonusHistory;
						$postPlayerUpdateBalance=new CashPlayerSetBonusPlayerUpdateBalance;
						$postPlayerTransHistory=new CashPlayerSetBonusPlayerTransHistory;
						$postLog=new TableLog;
						
				 		$dateTime = date('Y-m-d H:i:s');
				 		//$balanceTotal= $_POST['balance'] + $_POST['Amount'];
				 		
				 		if ($rd1['deposit_withdrawal']==1)
				 		{
				 			exit ("Please wait 45 seconds in order process balance transfer from ".$rd1['casino_name'].".");
				 		}
				 		
				 		if ($_POST['Amount'] <0)
				 		{
				 			if (($_POST['Amount'] * -1) > $_POST['bonus'])
				 			{
				 				exit("You don't enough bonus to be removed!");
				 				
				 			}
				 			
				 		}
				 		$command = Yii::app()->db->createCommand("SELECT bonus FROM tbl_cash_player where account_id='".$_POST["accountID"]."'");
				 		$rd=$command->queryRow();
				 		$totalBonus= $rd['bonus'] + $_POST['Amount'];
				 		$transNum= TransactionNumber::generateTransactionNumber("BU");
						
						$postPlayerBonus->cash_player_account_id=$_POST['accountID'];
						$postPlayerBonus->bonus_amount=$_POST['Amount'];
						$postPlayerBonus->giving_date=$dateTime;
						$postPlayerBonus->currency_id=$_POST['currency_id'];
						$postPlayerBonus->operator_id=Yii::app()->session['account_id'];
						$postPlayerBonus->trans_number=$transNum;
					
						$postPlayerBonusHistory->cash_player_account_id=$_POST['accountID'];
						$postPlayerBonusHistory->balance=$_POST['Amount'];
						$postPlayerBonusHistory->trans_type_id=6;
						$postPlayerBonusHistory->trans_date=$dateTime;
						$postPlayerBonusHistory->transaction_number=$transNum;
						
						TableCashPlayer::model()->updateAll(array(
								'based_bonus'=>$totalBonus,'bonus'=>$totalBonus,'last_update_balance'=>$dateTime),
								'account_id="'.$_POST['accountID'].'"');
						
						$postPlayerTransHistory->player_id=$_POST['accountID'];
						$postPlayerTransHistory->trans_number=$transNum;
						$postPlayerTransHistory->cash_player_trans_type_id=6;
						$postPlayerTransHistory->currency_id=$_POST['currency_id'];
						$postPlayerTransHistory->amount=$_POST['Amount'];
						$postPlayerTransHistory->balance_before=$_POST['bonus'];
						$postPlayerTransHistory->balance_after=$totalBonus;
						$postPlayerTransHistory->submit_time=$dateTime;
						$postPlayerTransHistory->finish_time=$dateTime;
						$postPlayerTransHistory->cashier_id=Yii::app()->session['account_id'];
						$postPlayerTransHistory->status_id=6;
						
						//$dateTime = date('Y-m-d H:i:s');
						
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=$_POST['accountID'];
						$postLog->operated_level='Player';
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=13;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> Added Bonus:<label style=\"color:red\">'.number_format($_POST['Amount'],2,'.',',').'</label></b>';
						
						$postPlayerBonusHistory->save();
						$postPlayerBonus->save();
						$postPlayerTransHistory->save();
						$postLog->save();
						
							echo 'Bonus process complete!.';
						}
							else
							{
								echo 'Player is still on a lobby, you cannot complete the bonus process.';
							}
						}

				if($_POST['task']=='checkPlayerOnlineStat')
				{
					if (Yii::app()->session['account_id']!=null){

						$this->onlinePlayer=new RedisManager();
						$checkLobby=$this->onlinePlayer->isExistingOnLobbyByPlayer($_POST['accountID']);//($_POST['AccountID']);

						$casinoId=$this->onlinePlayer->getCasinoIDByPlayer($_POST['accountID']);

						if ($checkLobby!=0){
							echo $casinoId;
						}
						else
						{
							$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
							$rd=$command->queryRow();
							if ($rd['deposit_withdrawal'] == 1) // display withdrawal wating message.
							{
								echo'w';
							}else {
								//display deposit dialog
								echo '0';
							}
						}
					}
					else
					{
						echo 'die';
					}
				}
				if ($_POST['task']=='checkIfStillProcessing'){
// 					$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
// 					$rd=$command->queryRow();
					echo 0;
				}
				if ($_POST['task']=='logout'){
					//Logout Process
					$this->onlinePlayer=new RedisManager();
					
					$checkLobby=$this->onlinePlayer->getCasinoIDByPlayer($_POST['accountID']);
					TableCashPlayer::model()->updateAll(array(
					'kick_off'=>3),
					'account_id="'.$_POST['accountID'].'"');
					
					$this->onlinePlayer->forceLogoutOnLobbyByPlayer($_POST['accountID']);
				}

			}
		}
	}
	
	 
	
}

