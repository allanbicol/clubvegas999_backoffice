<?php
Class CashPlayerCommision
{
	public function getCashPlayerCommission()
	{
		$rd = TableCashPlayerCommission::model()->find("id = 1");
		$autoBaccaratComList='';
		for($i=0.0;$i<=$rd['baccarat_max_commission'];$i+=0.1){
				$autoBaccaratComList.=number_format($i, 1, '.','') .',';
		}
		$autoRouletteComList='';
		for($i=0.0;$i<=$rd['roulette_max_commission'];$i+=0.1){
			$autoRouletteComList.=number_format($i, 1, '.','') .',';
		}
		$autoDragonTigerComList='';
		for($i=0.0;$i<=$rd['dragon_tiger_max_commission'];$i+=0.1){
			$autoDragonTigerComList.=number_format($i, 1, '.','') .',';
		}
		$autoBlackjackComList='';
		for($i=0.0;$i<=$rd['blackjack_max_commission'];$i+=0.1){
			$autoBlackjackComList.=number_format($i, 1, '.','') .',';
		}
		$autoAmericanRouletteComList='';
		for($i=0.0;$i<=$rd['american_roulette_max_commission'];$i+=0.1){
			$autoAmericanRouletteComList.=number_format($i, 1, '.','') .',';
		}
		echo $autoBaccaratComList . ';' . $autoRouletteComList . ';' . $autoDragonTigerComList . ';' . $autoBlackjackComList . ';' . $autoAmericanRouletteComList;
	}
}