<?php
/**
 * @todo marketing general purpose model
 * @author Allan
 * @since 2013-09-06
 *
 */

class MarketingLogs
{
	
	public function saveLog($operated_by, $operated_by_level, $operated, $operated_level, $log_type_id, $log_details){
		$log = new TableLog();
		$log->operated_by = $operated_by;
		$log->operated_by_level = $operated_by_level;
		$log->operated = $operated;
		$log->operated_level = $operated_level;
		$log->operation_time = date('Y-m-d H:i:s');
		$log->log_type_id = $log_type_id;
		$log->log_details = $log_details;
		$log->save();
	}
}