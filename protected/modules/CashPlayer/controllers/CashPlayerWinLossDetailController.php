<?php
class CashPlayerWinLossDetailController extends MyController
{
	public function actionCashPlayerWinLossDetail()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLossDetail;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerWinLossDetail();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerWinLossDetail($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("id","bet_date","account_id","account_name","currency_name","game_type","amount_wagers","win_loss","commission","amount_tips","total_win_loss","casino_balance","casino_name","counter");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	public function actionGetRecordAllPages()
	{
		$playersRecordAllPages=new CashPlayerWinLossDetail();
		$playersRecordAllPages->getPlayerWinLossDetailAllPages();
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readWinLoss')){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
	public function actionCostaVegasWinLossDetailExcel(){
		$file="CashPlayerCostaVegas_WinLossDetail".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Costavegas Winloss details</label> to excel file</b>\n'.$params[0].'\n<b>Method</b> = '.$params[1].'\n<b>Date From</b> = '.$params[2].'\n<b>Date To</b> = '.$params[3];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	
	public function actionExportAllCashPlayerWinLossDetail()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerWinLossDetail;
		$result=$cpHfund->getCountPlayerWinLossDetail();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
	
		$player_records = $cpHfund->getExportPlayerWinLossDetail();
	
		$fieldNames = array("bet_date","currency_name","game_type","amount_wagers","win_loss","commission","amount_tips","total_win_loss","casino_balance","casino_name");
		$headerNames=array("Bet DateTime","Currency","Game type","Valid Stake","Winloss","Commission","Tips","Total","Balance","Casino");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
		
			
	}
}