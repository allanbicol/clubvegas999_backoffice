<?php
class CashPlayerLogController extends MyController
{
	
	
	public function actionCashplayerlog()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readCashPlayerListLog'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	
}

