<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerwinloss.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/winlosstotalpages.css"/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript">
	var imgURL='<?php echo $this->module->assetsUrl;?>/images/';
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_winloss').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><li><a href='#'>Cash Player Win/Loss</a><i class='icon-angle-right'></i></li><li><a href='#'>Betting History</a></li>");
</script>
<script type="text/javascript">
	$(document).ready(function() { 
	var htmTag = document.createElement("div"); 
	htmTag.id = "dvWinLossResultDetails"; 
	htmTag.setAttribute("align", "center"); 
	htmTag.style.margin = "0px auto"; 
	htmTag.className = "dynamicDiv"; 
    document.body.appendChild(htmTag);
    hideDiv('dvWinLossResultDetails');
	    
    var htmTag1 = document.createElement("div"); 
	htmTag1.id = "dvResultDetails"; 
	htmTag1.setAttribute("align", "center"); 
	//htmTag1.setAttribute("onmouseout","javascript: document.getElementById('dvResultDetails').addEventListener('mouseout',onMouseOutResultDetails,true);");
	htmTag1.style.margin = "0px auto"; 
	htmTag1.className = "dynamicDiv"; 
    document.body.appendChild(htmTag1);
    hideDiv('dvResultDetails');
    
	var htmTag2 = document.createElement("div"); 
	htmTag2.id = "dvWinLossResultCards"; 
	htmTag2.setAttribute("align", "center"); 
	htmTag2.style.margin = "0px auto"; 
	htmTag2.className = "dynamicDiv"; 
    document.body.appendChild(htmTag2);
    hideDiv('dvWinLossResultCards');
	});
// 	function onMouseOutResultDetails(event) {
// 	    e = event.toElement || event.relatedTarget;
// 	    if (e.parentNode == this || e == this) {
// 	       return;
// 	    }
// 	    hideDiv("dvResultDetails");
// 	}
	function hideDiv(divID) { 
		if (document.getElementById) { 
		 	document.getElementById(divID).style.visibility = 'hidden';
		} 
		
	}
		
	function showDiv(divID,objXY,x,y) { 
		//get link position
		for (var lx=0, ly=0;
		objXY != null;
	    lx += objXY.offsetLeft, ly += objXY.offsetTop, objXY = objXY.offsetParent);
	    
	    //set div new position
	    window_size = $(window).height();
	    var offsetHeight =parseFloat($("#"+divID).css('height'));
	    if (parseFloat(y + 110 + offsetHeight) < parseFloat(window_size)){
		document.getElementById(divID).style.left= (x+335) +"px";
		document.getElementById(divID).style.top= (y+110) +"px";
	    }else{
	    	document.getElementById(divID).style.left= (x+335) +"px";
			document.getElementById(divID).style.top= parseFloat(y+100-offsetHeight) +"px";
	    }  
		document.getElementById(divID).style.visibility = 'visible'; 

	}

	function showWinLossResultDetails_savan(winLossId,obj,x,y){
		var urlSAVANWinLossResultDetails = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/SavanVegasWinLossResultDetailsV2';
		document.getElementById('dvWinLossResultDetails').innerHTML="<b>Loading...</b>";
		
		jQuery.ajax({
			url: urlSAVANWinLossResultDetails + '&winlossid=' + winLossId,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				//document.getElementById('dvWinLossResultDetails').innerHTML=data;
				$("#winloss_bet_result_detail").html(data);
				$("#winloss_bet_result_detail").dialog("open");
				//showDiv('dvWinLossResultDetails',obj,x,y);
			}
		});
		
	}

	function showResultDetails_savan(winLossId,obj,x,y){
		//document.getElementById('dvResultDetails').innerHTML="<b>Loading image...</b>";
		//showDiv('dvResultDetails',obj,x,y);	
		var baseURL = '<?php echo Yii::app()->request->baseUrl;?>';
		jQuery.ajax({
			url: baseURL + '/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/SavanVegasResultDetailsV2&winlossid=' + winLossId,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				
				//document.getElementById('dvResultDetails').innerHTML="<div style=\"width:445px; height:300px; border:none;\"><iframe  src='" + data + "'></iframe></div>";
 			var result =	$("#winloss_result_img").html("<iframe class='resultImage' style=\"width:453px; height:305px; border:none;\"   src='" + data + "'></iframe>");

 				$("#winloss_result_img").dialog("open"); return false;
			}
		});
		
	}

	function showWinLossCards_savan(winLossId,obj,x,y){
		var urlSAVANWinLossCards = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/SavanVegasWinLossResultCardsV2';
		document.getElementById('dvWinLossResultCards').innerHTML="<b>Loading...</b>";
		
		jQuery.ajax({
			url: urlSAVANWinLossCards + '&winlossid=' + winLossId,
			method: 'POST',
			data: {},
			context: '',
			success: function(data){
				//document.getElementById('dvWinLossResultDetails').innerHTML=data;
				$("#dvWinLossResultCards").html(data);
				showDiv('dvWinLossResultCards',obj,x+35,y);	
				//$("#winloss_bet_result_detail").dialog("open"); return false;
			}
		});
		
	}
	function gotoBack()
	{
		var dateFrom = (document.getElementById('datefrom').value).split("-");
		
		var monthF = dateFrom[1];
		var dayF = dateFrom[2];
		var yearF = dateFrom[0];
		var dateF=(yearF + "/" + monthF + "/" + dayF);
		var dateTo =(document.getElementById('dateto').value).split("-");
		var monthT = dateTo[1];
		var dayT = dateTo[2];
		var yearT = dateTo[0];
		if (dayT.length==1){
			dayT="0"+dayT;
			}
		if (dayF.length==1){
			dayF="0"+dayF;
			}
		if (monthT.length==1){
			monthT="0"+monthT;
			}
		if (monthF.length==1){
			monthF="0"+monthF;
			}
		var dateT= (yearT + "/" + monthT + "/" +dayT);
		var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
		var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
		var accountId=enc(document.getElementById('txtId').value);
		window.location="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLoss&dateFrom="+enc(dateSubmitFrom)+"&dateTo="+enc(dateSubmitTo)+"&id="+accountId;
	}

	var cRowNo=0;
	function tableTransHistoryToday(method_id,dateFrom,dateTo,accountID,tableId,gameId,shoesId) { 
		var divTag = document.createElement("Table"); 
	    divTag.id = 'list1'; 
	    divTag.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag);

	    var divTag1 = document.createElement("div"); 
	    divTag1.id = 'pager1'; 
	    divTag1.style.margin = "0px auto"; 
	    document.getElementById("qry_result").appendChild(divTag1);

	    $(document).ready(function() {
			var grid=jQuery("#list1");
			grid.jqGrid({ 
				url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/CashPlayerWinLossDetailSavanV2&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId, 
				datatype: 'json',
			    mtype: 'GET',
			    height: 'auto',
			    colNames: ['id','Table|Shoe|Game', 'Account ID','Account Name','Currency','Game Type','Bet Amount','Valid Bet','Result','Win/Loss','Win/Loss','Commission','Tips','Total','Balance After','Bet DateTime','Casino','counter','IP Address'],
			    colModel: [
					{name: 'id', index: 'id', width: 90, search:true,title:false,hidden:true},
					{name: 'table_shoe_game', index: 'table_shoe_game', width: 110, search:true,title:false,formatter:table_shoe_game},
					{name: 'account_id', index: 'account_id', width: 90,title:false,hidden:true},
					{name: 'account_name', index: 'account_name', width: 120,title:false,hidden:true},
					{name: 'currency', index: 'currency', width: 60, align:"center",title:false,hidden:true},
					{name: 'game_type', index: 'game_type', width: 120, align:"left",title:false},
					{name: 'stake_amount', index: 'stake_amount', width: 130, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'valid_stake', index: 'valid_stake', width: 130, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'result', index: 'result', width: 180, search:true , align:"right!important",title:false,formatter:resultArrow_savan},
					{name: 'win_loss', index: 'win_loss', width: 130, align:"right!important",title:false,hidden:true,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'winloss1', index: 'winloss1', width: 130, align:"right!important",title:false,formatter:winLossArrow_savan,formatoptions: {thousandsSeparator:','}},
					{name: 'commission', index: 'commission', width: 70, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'amount_tips', index: 'amount_tips', width: 70, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'total_win_los', index: 'total_win_los', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'balance_after', index: 'balance_after', width: 100, align:"right",title:false,formatter:'currency',formatoptions: {thousandsSeparator:','}},
					{name: 'end_time', index: 'end_time', width: 125, search:true,title:false},
					{name: 'casino_name', index: 'casino_name', width: 100, align:"right",title:false,hidden:true},
					{name: 'counter', index: 'counter', width: 20, align:"right",title:false,hidden:true},
					{name: 'IP', index: 'IP', width: 230, align:"left",title:false},
			    ],
			    afterInsertRow : function(rowid, rowdata)
			    {
			        if (rowdata.win_loss < 0)
			        {
			           $(this).jqGrid('setCell', rowid,8 ,'', {color:'#990000'});
			           $(this).jqGrid('setCell', rowid,10 ,'', {color:'#990000'});
			        }
			        getTotal();

			    },
			    loadComplete: function(){
			    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
			    	$("tr.jqgrow:even").css("background", "#ffffff");

			    	$('.savanwinlossarrow').click(function(e) {
			    		var x = e.pageX - this.offsetLeft;
			    		var y = e.pageY - this.offsetTop;
			    		showWinLossResultDetails_savan(this.alt,this,x-190,y-100);

			    	});
// 			    	$('.savanwinlossarrow').mouseout(function(e) {
// 			    		hideDiv("dvWinLossResultDetails");
// 			    	});
			    	$('.savanresultarrow').click(function(e) {
			    		var x = e.pageX - this.offsetLeft;
			    		var y = e.pageY - this.offsetTop;
			    		showResultDetails_savan(this.id,this,x-190,y-100);
			    	});
			    	$('.savanresultarrowCards').mouseenter(function(e) {
			    		var x = e.pageX - this.offsetLeft;
			    		var y = e.pageY - this.offsetTop;
			    		showWinLossCards_savan(this.alt,this,x-190,y-100);
			    	});
			    	$('.savanresultarrowCards').mouseout(function(e) {
			    		hideDiv("dvWinLossResultCards");
			    	});
			    	$('.dvWinLossResultCards').mouseenter(function(e) {
			    		hideDiv("dvWinLossResultCards");
			    	});
			    	$("#content").scroll(function(){
			    		hideDiv("dvWinLossResultCards");
			    	});
				},
				hidegrid: false, 
			    loadtext:"",
			    rownumbers:true,
			    rowNum: 100,	
			    rowList: [20, 50, 100,200,500,99999],
			    pager: '#pager1',
			    sortname: 'id',
			    sortorder: 'DESC',
			    caption: ' <label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> Betting History - &nbsp;&nbsp;&nbsp;'+ dateFrom + ' to '+ dateTo,
			    viewrecords: true,
			    footerrow: true,
			});
			$('#list1').jqGrid('navGrid', '#pager1', {edit: false, add: false, del:false, search: false});
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
                caption:"Export current page to Excel", 
                buttonicon:"ui-icon-calculator", 
                onClickButton: function(){ 
                	exportToExcel();
               }, 
                position:"last"
            });
			jQuery("#list1").jqGrid('navButtonAdd','#pager1',{
                caption:"Export all pages to Excel", 
                buttonicon:"ui-icon-calculator", 
                onClickButton: function(){ 
                	exportAllToExcel(method_id,dateFrom,dateTo,accountID,tableId,gameId,shoesId);
               }, 
                position:"last"
            });
		
			function table_shoe_game(cellvalue, options, rowObject) {
		        $("cellvalue").val(cellvalue);
		        var newValue=cellvalue.split("|");
		        	return '<label style="color:#4C4C4C">'+newValue[0]+'</label> | <label style="color:#6B4724">'+ newValue[1]+'</label> | <label style="color:#3E4A56">'+ newValue[2]+'</label>';
			}; 
		});
		
	    function exportToExcel(){
	    	var method = $('#gameType')[0].value;
			var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
			var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';
			
	    	var table1= document.getElementById('qry_result');
	    	var table2= document.getElementById('tableTotalPages');
	    	var html1 = table1.outerHTML;
	    	var html2 = table2.outerHTML;
	    	document.forms[2].csvBuffer.value="";
	    	document.forms[2].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+method+'#'+dateFrom+'#'+dateTo;
	    	document.forms[2].csvBuffer.value=html1+''+html2 ;
	        document.forms[2].method='POST';
	        document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/SavanWinLossExcelV2';  // send it to server which will open this contents in excel file
	        document.forms[2].target='_top';
	        document.forms[2].submit();
		}

	    function exportAllToExcel(){
	        $.ajax({	
				 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer//CashPlayerWinLossDetailSavanV2/ExportCashPlayerWinLossDetailSavanV2&methodId='+method_id+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&accountId='+accountID+'&tableId='+tableId+'&gameId='+gameId+'&shoesId='+shoesId,
				 async:true,
				 success: function(result) {
						var data =result.split("<BREAK>");

					    var method = $('#gameType')[0].value;
						var dateFrom	=	$('#datefrom')[0].value + ' ' + $('#cbHourfrom')[0].value + ':00:00';
						var dateTo	=	$('#dateto')[0].value + ' ' + $('#cbHourto')[0].value + ':59:59';

				    	document.forms[2].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+method+'#'+dateFrom+'#'+dateTo;
				    	document.forms[2].csvBuffer.value=data[0];
				        document.forms[2].method='POST';
				        document.forms[2].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/SavanWinLossExcelV2';  // send it to server which will open this contents in excel file
				        document.forms[2].target='_top';
				        document.forms[2].submit();
				 },
				 error: function(XMLHttpRequest, textStatus, errorThrown) { 
			 }});
		}
		
	} 
	function getTotal(){
		var grid = $("#list1"),
		sumBetCount=grid.jqGrid('getCol', 'counter', false, 'sum');
        sumBetAmount = grid.jqGrid('getCol', 'stake_amount', false, 'sum');
        sumWinLoss=grid.jqGrid('getCol', 'win_loss', false, 'sum');
        sumCommission=grid.jqGrid('getCol', 'commission', false, 'sum');
        sumValidBet=grid.jqGrid('getCol', 'valid_stake', false, 'sum');
        sumTotal=grid.jqGrid('getCol', 'total_win_los', false, 'sum');

        document.getElementById('currentBetcount').innerHTML=sumBetCount;
        document.getElementById('currentValidBet').innerHTML=addCommas(sumValidBet.toFixed(2));
        document.getElementById('currentBetAmount').innerHTML=addCommas(sumBetAmount.toFixed(2));
        document.getElementById('currentWinloss').innerHTML=addCommas(sumWinLoss.toFixed(2));
        document.getElementById('currentCommission').innerHTML=addCommas(sumCommission.toFixed(2));
        document.getElementById('currentTotal').innerHTML=addCommas(sumTotal.toFixed(2));
       
       // grid.jqGrid('footerData','set', {bet_date: '<div class=\"dateSummaryFooter\">TOTAL</div>',win_loss:sumWinLoss,commission:sumCommission,amount_tips:sumAmountTips,total_win_los:sumTotal});  
	}
	
	function winLossArrow_savan(cellvalue, options, rowObject) {
		var urlWinLossArrow = '<?php echo $this->module->assetsUrl; ?>';
		var imgsrc="<img style='opacity:0.5;' class='savanwinlossarrow' alt='"+ rowObject[0] +"' onmouseout='hideDiv(\"dvWinLossResultDetails\");' src='" + urlWinLossArrow + "/images/details.png'/>";
		if(parseFloat(cellvalue)<0){
			return "<font color='red'>" + cellvalue + "</font>" + imgsrc;
		}else{
			return "<font color='black'>" + cellvalue + "</font>"+ imgsrc;
		}
		
	}
	function resultArrow_savan(cellvalue, options, rowObject){
		var urlWinLossArrow = '<?php echo $this->module->assetsUrl; ?>';
		var imgsrc="";
		if(rowObject[5]!='ROULETTE'){
			imgsrc="<img style='opacity:0.5;' class='savanresultarrowCards' onmouseout='hideDiv(\"dvWinLossResultCards\");' alt='"+ rowObject[0] +"'  src='" + urlWinLossArrow + "/images/details.png'/>";
		}
		/* Baccarat (3#1#1#1#^9#9#0#9#9#0#)
		 　　 1(Banker Win),2(Player Win),3(Tie Win)　 # 0(No Banker Pair),1(Banker pair) # 0(No Player Pair),1(Player Pair)　 # 1(Small),2(Big) #
		 　　 ^
		 	Player poker one # Player poker two #  Player poker three　# Banker poker one # Banker poker two #  Banker poker three
		*/

		if(rowObject[5]=='BACCARAT'){	
			var cell = cellvalue.split('~');
			//var strData=(cellvalue.substring(0,cellvalue.indexOf('^')-1)).split('#');
			var strResult = cell[0].substring(1,cell[0].length-1);
			var strData = strResult.split(',');
			var strResult='';
			// 1(Banker Win),2(Player Win),3(Tie Win)
			if(strData[0]==1){
				strResult='<a class="savanresultarrow" id="'+ rowObject[0] +'"><font color="red">Banker Win</font>';
			}else if(strData[0]==2){
				strResult='<a class="savanresultarrow" id="'+ rowObject[0] +'"><font color="blue">Player Win</font>';
			}else if(strData[0]==3){
				strResult='<a class="savanresultarrow" id="'+ rowObject[0] +'"><font color="green">Tie</font>';
			}
			// 0(No Banker Pair),1(Banker pair)
			if(strData[1]==1){
				strResult+=', <font color="red">B Pair</font>';
			}
			// 0(No Player Pair),1(Player Pair)
			if(strData[1]==2){
				strResult+=', <font color="blue">P Pair</font>';
			}
			// 1(Small),2(Big)
			if(strData[2]==1){
				strResult+=', <font color="blue">Small</font>';
			}else if(strData[2]==2){
				strResult+=', <font color="red">Big</font>';
			}
			strResult+='<font color="black">('+strData[3]+')</font></a>';
			
			return strResult + imgsrc;
		}
		/* Dragon Tiger (1#^25#36#)
			1(Dragon Win ),2(Tiger Win),3(Tie Win)　 #
			^　
			Dragon poker  # Tiger poker  #
		*/
		else if(rowObject[5]=='DRAGON_TIGER'){
			var cell = cellvalue.split('~');
			//var strData=(cellvalue.substring(0,cellvalue.indexOf('^')-1)).split('#');
			var strResult = cell[0].substring(1,cell[0].length-1);
			var strData = strResult.split(',');
			var strResult='';
			if(strData[0]==2){
				strResult='<a class="savanresultarrow" id="'+ rowObject[0] +'"><font color="red">Dragon Win</font>';
			}else if(strData[0]==1){
				strResult='<a class="savanresultarrow" id="'+ rowObject[0] +'"><font color="blue">Tiger Win</font>';
			}else if(strData[0]==3){
				strResult='<a class="savanresultarrow" id="'+ rowObject[0] +'"><font color="green">Tie</font>';
			}
			strResult+='('+strData[1]+')</a>';
			return strResult + imgsrc;
		}
		/* Roulette (2)
			2
		*/
		else if(rowObject[5]=='ROULETTE'){
			var imgResult='';
			imgResult='<a class="savanresultarrow" id="'+ rowObject[0] +'"><img	style="width:40px;height:18px;padding:3px 3px 0px 5px;" src="'+imgURL+'roulette/r_'+cellvalue+'.png" /></a>';
			return imgResult + imgsrc;
			
		}
	}
</script>
<script type="text/javascript">
function enc(str) {
    var encoded = "";
    for (i=0; i<str.length;i++) {
        var a = str.charCodeAt(i);
        var b = a;    // bitwise XOR with any number, e.g. 123
        encoded = encoded+String.fromCharCode(b);
    }
    return encoded;
}

function addCommas(nStr){
    nStr += '';
    c = nStr.split(','); // Split the result on commas
    nStr = c.join('');  // Make it back to a string without the commas
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function decryptparam()
{
	var strF = document.getElementById('f').value;
	var strT= document.getElementById('t').value;
	var encodeF = enc(strF);
	var encodeT = enc(strT);
	
	var dateF= encodeF.split("_");	
	var dateFrom=dateF[0];
	var dateT= encodeT.split("_");	
	var dateTo=dateT[0];
	var dateFSplit=dateFrom.split("/");
	var dateTSplit=dateTo.split("/");
	
	if (dateFSplit[1].length==1){
		omonthF="0"+ dateFSplit[1];
	}else{omonthF=dateFSplit[1]; }
	if (dateTSplit[1].length==1){
		omonthT="0"+ dateTSplit[1];
	}else{omonthT=dateTSplit[1];}
	if (dateFSplit[2].length==1){
		odayF="0"+ dateFSplit[2];
	}else{odayF=dateFSplit[2];}
	if (dateTSplit[2].length==1){
		odayT="0"+ dateTSplit[2];
	}else{odayT=dateTSplit[2];}
	document.getElementById("datefrom").value=dateFSplit[0]+"-"+omonthF+"-"+odayF;
	document.getElementById("dateto").value=dateTSplit[0]+"-"+omonthT+"-"+odayT;

}
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	    
	    jQuery("#datefrom").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	
	    jQuery("#dateto").datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: "yy-mm-dd"
	    });
	});

	function loadTransTable(selected_method,dateFrom,dateTo,accountID,tableId,gameId,shoesId){
		if (selected_method=='ALL'){
			//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID,tableId,gameId,shoesId);
		}
	    else
	    {
	    	//for fund group by currency
	    	document.getElementById('qry_result').innerHTML='';
	    	tableTransHistoryToday(selected_method,dateFrom,dateTo,accountID,tableId,gameId,shoesId);

		}
	}

	function btnclick(btnname)
	{
		document.getElementById('currentBetcount').innerHTML="0";
		document.getElementById('currentValidBet').innerHTML="0.00";
        document.getElementById('currentBetAmount').innerHTML="0.00";
        document.getElementById('currentWinloss').innerHTML="0.00";
        document.getElementById('currentCommission').innerHTML="0.00";
        document.getElementById('currentTotal').innerHTML="0.00";
        
		document.getElementById('allBetcount').innerHTML="0";
		document.getElementById('allValidBet').innerHTML="0.00";
        document.getElementById('allBetAmount').innerHTML="0.00";
        document.getElementById('allWinloss').innerHTML="0.00";
       	document.getElementById('allCommission').innerHTML="0.00";
       	document.getElementById('allTotal').innerHTML="0.00";
       	document.getElementById('currentBalance').innerHTML="0.00";
		var tableId=document.getElementById("txtTableId").value;
		var gameId=document.getElementById("txtGameId").value;
		var shoesId=document.getElementById("txtShoesId").value;
		
       	
		if (btnname=="Load")
		{
			var datefrom=((document.getElementById('datefrom').value).replace("-","/").replace("-","/")+" "+ document.getElementById('cbHourfrom').value +":00:00");
			var dateto=((document.getElementById('dateto').value).replace("-","/").replace("-","/")+" "+ document.getElementById('cbHourto').value +":59:59");
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable("ALL",datefrom,dateto,accountID,accountID,tableId,gameId,shoesId);
		}
		else if (btnname=="Today")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate());
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theday=mydate.getDate();
			var dayL=theday.toString;
			var monthL=themonth.toString;
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theday="0"+theday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('gameType').value;
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(methodtype,datefrom,dateto,accountID,accountID,tableId,gameId,shoesId);

			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/GetRecordAllPagesV2',
	    		type: 'POST',
	    		data: {'accountId':accountID,
	        			'methodId':methodtype,
	        			'dateFrom':datefrom,
	        			'dateTo':dateto,
	        			'tableId':tableId,
	        			'gameId':gameId,
	        			'shoesId':shoesId},
	    		context:'',
	    		success: function(data) {
	        		var resultAllPages=data.split("#");
	        		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	        		document.getElementById('allValidBet').innerHTML=resultAllPages[1];
	                document.getElementById('allBetAmount').innerHTML=resultAllPages[2];
	                document.getElementById('allWinloss').innerHTML=resultAllPages[3];
	               	document.getElementById('allCommission').innerHTML=resultAllPages[4];
	               	document.getElementById('allTotal').innerHTML=resultAllPages[5];
	               	document.getElementById('currentBalance').innerHTML=resultAllPages[6];
		    	}
	    	});
		}
		else if (btnname=="Yesterday")
		{
			var mydate= new Date();
			mydate.setDate(mydate.getDate()-1);
			var theyear=mydate.getFullYear();
			var themonth=mydate.getMonth()+1;
			var theyday=mydate.getDate();
			var dayL=theyday.toString;
			var monthL=themonth.toString;
			if (monthL.length==1){
				themonth="0"+themonth;
			}
			if (dayL.length==1){
				theyday="0"+theyday;
			}
			var datefrom=(theyear+"/"+themonth+"/"+theyday + "_00:00:00");
			var dateto=(theyear+"/"+themonth+"/"+theyday+ "_23:59:59");
			document.getElementById('datefrom').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('dateto').value=(theyear+"-"+themonth+"-"+theyday);
			document.getElementById('cbHourfrom').value="00";
			document.getElementById('cbHourto').value="23";
			var methodtype=document.getElementById('gameType').value;
			var accountID=document.getElementById('txtAccountID').value;
			loadTransTable(methodtype,datefrom,dateto,accountID,accountID,tableId,gameId,shoesId);

			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/GetRecordAllPagesV2',
	    		type: 'POST',
	    		data: {'accountId':accountID,
	        			'methodId':methodtype,
	        			'dateFrom':datefrom,
	        			'dateTo':dateto,
	        			'tableId':tableId,
	        			'gameId':gameId,
	        			'shoesId':shoesId},
	    		context:'',
	    		success: function(data) {
	        		var resultAllPages=data.split("#");
	        		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	        		document.getElementById('allValidBet').innerHTML=resultAllPages[1];
	                document.getElementById('allBetAmount').innerHTML=resultAllPages[2];
	                document.getElementById('allWinloss').innerHTML=resultAllPages[3];
	               	document.getElementById('allCommission').innerHTML=resultAllPages[4];
	               	document.getElementById('allTotal').innerHTML=resultAllPages[5];
	               	document.getElementById('currentBalance').innerHTML=resultAllPages[6];
		    	}
	    	});
		}
		else if (btnname=="Submit")
		{
			
			var dateFrom = (document.getElementById('datefrom').value).split("-");
			
			var monthF = dateFrom[1];
			var dayF = dateFrom[2];
			var yearF = dateFrom[0];
			var dateF=(yearF + "/" + monthF + "/" + dayF);
			var dateTo =(document.getElementById('dateto').value).split("-");
			var monthT = dateTo[1];
			var dayT = dateTo[2];
			var yearT = dateTo[0];
			var dateT= (yearT + "/" + monthT + "/" +dayT);
			var dateSubmitFrom= (dateF+"_"+ document.getElementById('cbHourfrom').value +":00:00");
			var dateSubmitTo=(dateT+ "_"+ document.getElementById('cbHourto').value +":59:59");
			var methodtype=document.getElementById('gameType').value;
			var accountID=document.getElementById('txtAccountID').value;
			
			loadTransTable(methodtype,dateSubmitFrom,dateSubmitTo,accountID,tableId,gameId,shoesId);

			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerWinLossDetailSavanV2/GetRecordAllPagesV2',
	    		type: 'POST',
	    		data: {'accountId':accountID,
	        			'methodId':methodtype,
	        			'dateFrom':dateSubmitFrom,
	        			'dateTo':dateSubmitTo,
	        			'tableId':tableId,
	        			'gameId':gameId,
	        			'shoesId':shoesId},
	    		context:'',
	    		success: function(data) {
	        		var resultAllPages=data.split("#");
	        		document.getElementById('allBetcount').innerHTML=resultAllPages[0];
	        		document.getElementById('allValidBet').innerHTML=resultAllPages[1];
	                document.getElementById('allBetAmount').innerHTML=resultAllPages[2];
	                document.getElementById('allWinloss').innerHTML=resultAllPages[3];
	               	document.getElementById('allCommission').innerHTML=resultAllPages[4];
	               	document.getElementById('allTotal').innerHTML=resultAllPages[5];
	               	document.getElementById('currentBalance').innerHTML=resultAllPages[6];
		    	}
	    	});
			
		}
	}
</script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
.ui-widget-overlay{z-index: 100!important;}
div.dateSummaryFooter { text-align: Left; }
tr.footrow-ltr td { background-color: #AD8533; color: #FFF; }\
.param_label {display: inline-block;width:60px;}
</style>
</head>
<body onload="javascript:decryptparam();btnclick('Submit');">
<div style="position: relative;left: 5px; top:3px">
<div id="parameter_area" style="width: 500px;height: 180px">
	<div class="header" ><label style="color:#D84A38"><?php echo $_GET['account_id'];?></label> Betting History</div>
	<form action="">
		<input type="text" id="txtAccountID" value="<?php echo $_GET['account_id'];?>" style="display: none;" >
		<table style=" width: 500px; margin-top:5px;">
		<tr><td width="25%" style="padding-left: 5px;">METHOD:</td>
			<td><select id="gameType">
				<option value="ALL">ALL</option>
  				<?php 
				$dataReader = TableGame::model()->findAll();
				foreach ($dataReader as $row){
					echo '<option value="' . $row['game_name'] . '">'. strtoupper($row['game_name']) . '</option>';
				}
				?>
			</select>
		</td></tr>
		<tr><td width="25%" style="padding-left: 5px;">FROM:</td><td><input style="width: 130px"  type="text"  id="datefrom" name="datefrom" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourfrom">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option>'.$value.'</option>';
					}
				?></select>
			<input value=":00:00" style="border: 0px; background-color: transparent;width: 50px;" disabled></td></tr>
			<tr>
			<td width="25%" style="padding-left: 5px;">TO :</td><td><input style="width: 130px" type="text"  id="dateto" name="dateto" value="" />&nbsp;&nbsp;&nbsp;
			<select id="cbHourto">
				<?php 
					$hHour=array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
					foreach ($hHour as  $value){
					echo '<option selected="true">'.$value.'</option>';
					}
				?></select>
			<input value=":59:59" style="border: 0px; background-color: transparent;width: 50px;" disabled></td></tr>
		<tr></table>
		<div align="center" style="margin-top: 5px;" >
		<input onclick="javacript: btnclick(this.value);" id="Submit" type="button" value="Submit" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtYesterday" type="button" value="Yesterday" class="btn red">
		<input onclick="javascript: btnclick(this.value);" id="txtToday" type="button" value="Today" class="btn red">
		<input onclick="javascript: gotoBack();" type="button" value="Back" class="btn red"></div>
		
		</form>	
		</div>
		<br/>
		
	<div id="qry_result">
	</div>
	<div id="pager1"></div>
	<br/>
	<table id="tableTotalPages">
		<tr>
			<th class="header" colspan=8 align="left"><b>Total</b></th>
		</tr>
		<tr class="tr" >
			<th class="th" width="100px" align="left">Type</th>
			<th class="th" width="120px">Number of Bet</th>
			<th class="th" width="150px">Valid Bet</th>
			<th class="th" width="150px">Bet Amount</th>
			<th class="th" width="150px">Win/Loss</th>
			<th class="th" width="70px">Commission</th>
			<th class="th" width="150px">Total</th>
			<th class="th" width="100px">Balance</th>
		</tr>
		<tr class="tr" >
			<td class="td" ><b>Current Page</b></td>
			<td class="td" id="currentBetcount" align="right"></td>
			<td class="td" id="currentValidBet" align="right"></td>
			<td class="td" id="currentBetAmount" align="right"></td>
			<td class="td" id="currentWinloss" align="right"></td>
			<td class="td" id="currentCommission" align="right"></td>
			<td class="td" id="currentTotal" align="right"></td>
			<td class="td" id="currentBalance" align="center" rowspan="2"></td>
		</tr>
		<tr class="tr" >
			<td class="td" ><b>All Pages</b></td>
			<td class="td" id="allBetcount" align="right"></td>
			<td class="td" id="allValidBet" align="right"></td>
			<td class="td" id="allBetAmount" align="right"></td>
			<td class="td" id="allWinloss" align="right"></td>
			<td class="td" id="allCommission" align="right"></td>
			<td class="td" id="allTotal" align="right"></td>
		</tr>
	</table>
	<br/>
	<!--
	<div id="qry_resultTotal"></div>
	<div id="qry_resultTotal1"></div>
	<br/>-->
	<div><input id="f" value="<?php echo $_GET['f'];?>" hidden=true><input id="t" value="<?php echo $_GET['t'];?>" hidden=true></div>
	<div><input id="txtTableId" value="<?php echo $_GET['tableId'];?>" hidden=true><input id="txtGameId" value="<?php echo $_GET['gameId'];?>" hidden=true><input id="txtShoesId" value="<?php echo $_GET['shoesId'];?>" hidden=true><input id="txtId" value="<?php echo $_GET['id'];?>" hidden=true></div>
</div>
<form method="post" action="">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
</form>
<?php 
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		    'id'=>'winloss_result_img',
		    // additional javascript options for the dialog plugin
		    'options'=>array(
		        'title'=>'Screen Shot',
		        'autoOpen'=>false,
	    		'width'=>'479px',
	    		'height'=>'auto',
		    	'modal'=> true,
		    	'resizable'=> false,
				'position'=>'center',
		    ),
		));
	
	// Initialize dialog content
    echo '<div id="viewWinLossResultImg"></div>';
	
    // End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');
	
	//Create Dialog
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			'id'=>'winloss_bet_result_detail',
			// additional javascript options for the dialog plugin
			'options'=>array(
					'title'=>'Win/Loss Betting Detail',
					'autoOpen'=>false,
					'width'=>'auto',
					'height'=>'auto',
					'modal'=> true,
					'resizable'=> false,
			),
	));
	
	// Initialize dialog content
	echo '<div id="viewResultDetail"></div>';
	
	// End block create Dialog
	$this->endWidget('zii.widgets.jui.CJuiDialog');

?>

</body>
</html>