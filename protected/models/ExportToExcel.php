<?php
/**
 * @todo ExportToExcel model
 * @author leokarl
 * @since 2013-01-24
 */
class ExportToExcel
{
	/**
	 * @todo export to excel from grid
	 * @author leokarl
	 * @since 2013-01-24
	 * @param string $file_name - should have no whitespace
	 */
	public function exportToExcelFromGrid($file_name, $csv_buffer,$parameters){
		$file = $file_name . ".xls";
		
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename =" . $file);
		
		$exportParams=str_replace("View", "<b>Exported Page</b>", $parameters);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($params[1] != ''){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[1] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';
		}
		
		try{
			echo $csv_buffer; //$_POST['csvBuffer'];
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Agent';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">'.$file.'</label> to excel file</b>\n'.$params[0].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[2].'\n<b>Test Currency</b> = '.$params[3].'\n<b>Date From</b> = '.$params[4].'\n<b>Date To</b> = '.$params[5];
			$postLog->save();
		}catch(Exception $e){}
	}
}