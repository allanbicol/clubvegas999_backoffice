<?php
/**
 * @todo 
 * @copyright CE
 * @author Chamnan Nop
 * @since 2012
 *
 */
class Banking 
{
	/**
	 * @author Chamnan Nop
	 * 
	 * @param string $orderField
	 * @param string $sortType
	 * @param int $startIndex
	 * @param int $limit
	 * @return array();
	 */
	public function getPayment($orderField, $sortType, $startIndex , $limit)
	{
		$qry_test_currency = ($_GET['except_test_currency']==1) ? ' AND currency_name!="TEST"' : '';
		$qry_cancel = ($_GET['except_cancel']==1) ? ' AND status_name!="Cancelled"' : '';
		$qry_currency = (trim($_GET['currencyID']) == '') ? '' : ' AND currency_name=' . $this->getCurrencyNameById($_GET['currencyID']);
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM vwCashPlayerWithdraw WHERE transaction_date>='".$_GET['dtFrom']."' AND transaction_date<='".$_GET['dtTo']."'
				AND account_id like '%" . $_GET['accountID'] . "%' AND email like '%" . $_GET['email'] . "%' ". $_GET['currencyID'] ."
				$qry_test_currency $qry_cancel");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo get currency name by currency id
	 * @author leokarl
	 * @since 2012-12-24
	 * @param int $currency_id
	 */
	public function getCurrencyNameById($currency_id){
		$model = TableCurrency::model()->find(array('select'=>'currency_name','condition'=>'id=:currency_id',
				'params'=>array(':currency_id' => $currency_id),)
		);
		return $model['currency_name'];
	}
	/**
	 * @author Chamnan Nop
	 * 
	 * @return array();
	 */
	public function getCoundPaymentRecords()
	{
		
		$qry_test_currency = ($_GET['except_test_currency']==1) ? ' AND currency_name!="TEST"' : '';
		$qry_cancel = ($_GET['except_cancel']==1) ? ' AND status_name!="Cancelled"' : '';
		$qry_currency = (trim($_GET['currencyID']) == '') ? '' : ' AND currency_name=' . $this->getCurrencyNameById($_GET['currencyID']);
		
		
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM vwCashPlayerWithdraw WHERE transaction_date>='".$_GET['dtFrom']."' AND transaction_date<='".$_GET['dtTo']."'
				AND account_id like '%" . $_GET['accountID'] . "%' AND email like '%" . $_GET['email'] . "%' ". $_GET['currencyID'] ."
				$qry_test_currency $qry_cancel");
		$rows = $command->query();
		return $rows;
	}
	
	public function getWithdrawInfo($account_id, $transaction_id){
	
		$connection = Yii::app()->db_cv999_fd_master;
	
		$command = $connection->createCommand("SELECT * FROM tbl_cash_player_withdraw AS tcpw
												INNER JOIN `tbl_currency` AS tc
												ON tcpw.`currency_id` = tc.`id` where cash_player_account_id='".$account_id."' and transaction_number='".$transaction_id."'");
		
		$rows = $command->query();
		return $rows->readAll();
	}
	
	public function getDepositInfo($account_id, $transaction_id){
	
		$connection = Yii::app()->db_cv999_fd_master;
	
		$command = $connection->createCommand("SELECT * FROM vwGetAllPayment where account_id='".$account_id."' and transaction_number='".$transaction_id."'");
		$rows = $command->query();
		return $rows->readAll();
	}
	
	public function getCashPlayerBalance($account_id){
		
		$connection = Yii::app()->db_cv999_fd_master;
		
		$command = $connection->createCommand("SELECT * FROM vwGetCashPlayerOnlineBalance where account_id='".$account_id."'");
		$rows = $command->query();
		return $rows->readAll();
		
	}
}