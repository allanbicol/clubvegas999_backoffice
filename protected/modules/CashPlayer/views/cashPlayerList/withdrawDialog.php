<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/deposit.css" />
<script type="text/javascript">
optSelect('bank');
function optSelect(val){
	if (val=="bank"){
		document.getElementById('selectBank').value='ALL';
		document.getElementById('rwBank').style.display='table-row';
		document.getElementById('rwWithdrawType').style.display='none';
		document.getElementById('txtSelectedOption').value='bank';
	}else{
		document.getElementById('withdrawType').value='ALL';
		document.getElementById('rwBank').style.display='none';
		document.getElementById('rwWithdrawType').style.display='table-row';
		document.getElementById('txtSelectedOption').value='others';
	}
}
function selectedItem(val){
	if (val=="bank"){
		document.getElementById('txtTransItem').value=document.getElementById('selectBank').value;
		
	}else{
		document.getElementById('txtTransItem').value=document.getElementById('withdrawType').value;
	}
}

isProcessingUnlock = false;
function cancelButton(account_id){
	
	if(isProcessingUnlock){
	       return;
	}
	
	jQuery.ajax({
		url: baseUrl+'/index.php?r=CashPlayer/CashPlayerList/UnlockPlayer',
		type: 'POST',
		data: {'accountID' :account_id},
		context: '',
		async: false,
		success: function(data){
			isProcessingUnlock = false;
		}	
	});
	  
}

$(".ui-dialog-titlebar-close").click(function() {
	cancelButton("<?php echo $_GET['account_id']; ?>");
});

</script>
<table border="0" cellpadding="0" cellspacing="0" >
<tr>
	<td class="left">Account ID</td>
	<td class="right">
		<?php echo CHtml::textField('txtAccountId',$_GET['account_id'],array('id'=>'txtAccountId','disabled'=>'true','style'=>'color:black;'));?></td>
</tr>
<tr>
	<td class="left">Amount</td>
	<td class="right">
		<?php echo CHtml::textField('txtWithdrawAmount','',array('id'=>'txtWithdrawAmount'));?> 
	</td>
</tr>

<tr>
			<td class="left">Withdrawal Type</td>
			<td class="right"><input type="radio" name="radOption" Checked onclick="javascript:optSelect('bank');" value="bank">Banks<input type="radio" name="radOption" onclick="javascript:optSelect('others');" value="others">Others</td>
			</tr>
			<tr id="rwBank" style="display: none;">
			<td class="left"><label id="lblSelectBank">Transaction Item</label></td>
			<td class="right"><select id="selectBank">
						<option value="">(Select type)</option>
		  				<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='id,transaction_item';  
		  				$criteria->condition='item_type=:type and transaction_bank=:transBank';
		  				$criteria->params=array(':type'=>4,':transBank'=>1);
						$dataReader = TableTransactionItem::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['transaction_item']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
			<tr id="rwWithdrawType" style="display: none;">
			<td class="left"><label id="lblWithdrawType">Transaction Item</label></td>
			<td class="right"><select id="withdrawType" >
						<option value="">(Select type)</option>
		  				<?php 
		  				$criteria=new CDbCriteria;
		  				$criteria->select='id,transaction_item';
		  				$criteria->condition='item_type=:type and transaction_bank=:transBank';
		  				$criteria->params=array(':type'=>4,':transBank'=>0);
						$dataReader = TableTransactionItem::model()->findAll($criteria);
						foreach ($dataReader as $row){
							echo '<option value="' . $row['id'] . '">'. strtoupper($row['transaction_item']) . '</option>';
						}
						?>
					</select>
				</td>
			</tr>
</table>
<hr/>
<div style="text-align: center;">
<?php
	echo CHtml::textField('txtTransItem','',array('id'=>'txtTransItem','style'=>'display:none'));
	echo CHtml::textField('txtSelectedOption','',array('id'=>'txtSelectedOption','style'=>'display:none'));
	// submit button 
	echo CHtml::button('Confirm', array('id'=>'btnSubmitWithdraw', 'class'=>'btn red',
			'onclick'=>'selectedItem($("#txtSelectedOption").val());checkAmountToWithdraw($("#txtTransItem").val());'));
	echo '&nbsp;&nbsp;';
	// cancel button
	echo CHtml::button('Cancel', array('id'=>'btnCancelWithdraw','class'=>'btn red',
			'onclick'=>'$("#withdraw-dialog").dialog("close"); cancelButton("'.$_GET['account_id'].'");'));
?>
</div>