


var isProcessingSetBonus = false;	
function checkPlayerOnlineStatBonus(){
	if(isProcessingSetBonus){
	       return;
	}
	isProcessingSetBonus = true;
	
	jQuery.ajax({
	//url: baseUrl + '/index.php?r=CashPlayer/CashPlayerSetBonus/Setbonus',
	url: baseUrl + '/index.php?r=CashPlayer/CashPlayerSetBonus/CheckPlayerOnlineStatus',
	type: 'POST',
	//data: {'task': 'checkPlayerOnlineStat', 'accountID' : document.getElementById('accountID').value},
	data: {'accountID' : $('#accountID').val()},
	context: '',
	async:false,
	success: function(data){
		isProcessingSetBonus = false;
		if(data == 'no_permission'){
			alert('Warning: You have no permission to set cash player bonus!');
			return false;
		}
		if (data!='die'){
			if (data==0){
				//$createSetBonusDialog.dialog('open');
				$('#viewSetBonus').load(baseUrl + '/index.php?r=CashPlayer/CashPlayerList/SetBonusDialog&account_id=' + $('#accountID').val(),
    			function(response, status, xhr) {		    	
	  				if (status == "success") {			    		  									
		  				$("#set_bonus_dialog").dialog("open"); return false;
		  			}else{
	  					$('#viewSetBonus').load('<p style="color:red;">Something wrong during request!!!</p>');
	  					$("#set_bonus_dialog").dialog("open"); return false;
		  			}
	  			});
				return false;
			}
			else{
				if(data=='w'){
					$createWaitWithdrawProcessDialog.dialog('open');
					$(".ui-dialog-titlebar-close").hide();
					var check =setInterval(function(){
						jQuery.ajax({
							url: baseUrl + '/index.php?r=CashPlayer/CashPlayerSetBonus/ProcessWithdrawalRequest',
							type: 'POST',
							data: {'accountID' : document.getElementById('accountID').value},
							context: '',
							async:false,
							success: function(data){
								if (data==0){
									$(".ui-dialog-titlebar-close").show();
									$createWaitWithdrawProcessDialog.dialog('close');
									$('#list2').trigger("reloadGrid");
									
									//$createSetBonusDialog.dialog('open');
									$('#viewSetBonus').load(baseUrl + '/index.php?r=CashPlayer/CashPlayerList/SetBonusDialog&account_id=' + $('#accountID').val(),
					    			function(response, status, xhr) {		    	
						  				if (status == "success") {			    		  									
							  				$("#set_bonus_dialog").dialog("open"); return false;
							  			}else{
						  					$('#viewSetBonus').load('<p style="color:red;">Something wrong during request!!!</p>');
						  					$("#set_bonus_dialog").dialog("open"); return false;
							  			}
						  			});
									
									clearInterval(check);
									return false;
								}
								else if(data=='h'){
									$createWaitWithdrawProcessDialog.dialog('close');
									alert('Cannot connect to HTV withdrawal API Server.Please try again!.');
									clearInterval(check);
									return false;
								}else if(data=='s'){
									$createWaitWithdrawProcessDialog.dialog('close');
									alert('Cannot connect to Savan withdrawal API Server.Please try again!.');
									clearInterval(check);
									return false;
								}
								else if(data=='vc'){
									$createWaitWithdrawProcessDialog.dialog('close');
									alert('Cannot connect to VirtualVegas withdrawal API Server.Please try again!.');
									clearInterval(check);
									return false;
								}else if(data=='svs'){
									$createWaitWithdrawProcessDialog.dialog('close');
									alert('Cannot connect to SlotVegas withdrawal API Server.Please try again!.');
									clearInterval(check);
									return false;
								}
												
					    	}
						});
						},2000);
						
				}
				else{
					//this is for force logout
					$createForceLogoutDialog.dialog('open');
					$('#imgLogout').hide();
					document.getElementById("accountIDLogout").value=$('#accountID').val();
					document.getElementById("casinoId").value = data;
					$(".ui-dialog-titlebar-close").hide();
					//document.getElementById("submitBonus").disabled = false;
					return false;
				}
				
			}
		}else{
			window.location= baseUrl + "/index.php?r=Login";
		}
	}
});
}

/**
 * @todo set bonus javascript
 * @author leokarl
 * @since 2012-12-22
 * @param account_id
 * @param amount
 * @param bonus_type
 */
function confirmBonus(account_id, amount, bonus_type){
	jQuery.ajax({
		url: baseUrl + '/index.php?r=CashPlayer/CashPlayerSetBonus/SetPlayerBonus',
		type: 'POST',
		async: false,
		data: {'account_id': account_id, 'amount' : amount, 'bonus_type' : bonus_type},
		context: '',
		success: function(msg) { 
			if(msg == 'invalid_bonus'){
				alert('Invalid bonus amount!');
				$('#txtSetBonusAmount').focus();
			}else if(msg == 'invalid_submit_method'){
				alert('Invalid submit method!');
			}else if(msg == 'acount_doesnt_exist'){
				alert('Account doensn\'t exist!');
			}else if(msg == 'invalid_bonus_type'){
				alert('Invalid bonus type!');
				$('#cmbBonusType').focus();
			}else if(msg == 'player_on_the_lobby'){
				alert('Player is still on a lobby, you cannot complete the bonus process.');
			}else if(msg == 'insufficient_bonus'){
				alert('Warning: Insufficient bonus!');
				$('#txtSetBonusAmount').focus();
			}else if(msg == 'no_permission'){
				alert('Warning: You have no permission to set cash player bonus!');
			}else if(msg == 'select_bonus_type'){
				alert('Please select bonus type!');
				$('#cmbBonusType').focus();
			}else if(msg == 'set_bonus_complete'){
				// alert success message, close dialog and reload grid
				alert('Set bonus complete!');
				$("#set_bonus_dialog").dialog("close");
				$('#list2').trigger("reloadGrid");	
			}
			
    	}
	});
}

/* OLD confirmBonus() function
function confirmBonus()
{
	jQuery.ajax({
		url: baseUrl + '/index.php?r=CashPlayer/CashPlayerSetBonus/Setbonus',
		type: 'POST',
		async:false,
		data: { 'task':'confirmBonus',
    		   'accountID': document.getElementById('accountID').value, 
    		   'Amount' :document.getElementById('txtBunosAmount').value,
    		   'balance' :document.getElementById('balanceAmount').value,
    		   'currency_id' :document.getElementById('currencyID').value,
    		   'bonus' :document.getElementById('bonus').value
    		   },
		context: '',
		success: function(data) {
			 $createSetBonusDialog.dialog('close');
			 alert(data);
			 $('#list2').trigger("reloadGrid");
    	}
	});
}

*/