<?php
/**
 * @todo AgentNewSubCompany Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-10
 */
class AgentNewSubCompany
{
	/**
	 * @todo get higher agent's downline revenue value
	 * @author leokarl
	 * @since 2013-03-29
	 */
	public function getRevenueValue($account_id, $game, $type, $casino){
		/**
		 * @variable defination
		 * $game: 1=bac, 2=dra, 3=eur, 4=ame, 5=bla, 6=slots
		 * $type: 1=share, 2=commission
		 * $casino: 1=vig, 2=htv, 3=sv, 4=vv
		 */
		$connection = Yii::app()->db_cv999_fd_master;

		$command = $connection->createCommand('
				SELECT SPLIT_STR(SPLIT_STR(SPLIT_STR(revenue_sharing_limit_to, "|",'. $game .'),"^",'. $type .'), "#",'. $casino .') AS val FROM tbl_agent WHERE agent_parent_id="'. $account_id .'"
				ORDER BY SPLIT_STR(SPLIT_STR(SPLIT_STR(revenue_sharing_limit_to, "|",'. $game .'),"^",'. $type .'), "#",'. $casino .') DESC LIMIT 1
				');
		$rows = $command->queryAll();
		return (isset($rows[0]['val'])) ? (float)$rows[0]['val'] : 0;
	}

	/**
	 * @todo check account_id availability
	 * @author leokarl
	 * @since 2012-12-14
	 * @param string $account_id
	 * @return boolean
	 */
	public function isAccountAvailable($account_id){
		$existed = 0;
		if($this->isAccountExistedInAgent($account_id)){
			$existed +=1;
		}
		if($this->isAccountExistedInAgentPlayer($account_id)){
			$existed +=1;
		}
		if($this->isAccountExistedInCashPlayer($account_id)){
			$existed +=1;
		}
		return ($existed == 0) ? TRUE : FALSE;
	}

	/**
	 * @todo get credit_limit_to by agent
	 * @author leokarl
	 * @since 2012-12-11
	 * @param string $account_id
	 * @param string $agent_type
	 */
	public function getCreditLimitToByAgent($account_id,$agent_type){
		if($agent_type == 'SC'){

			$mc_credit = TableMainCompany::model()->find(array('select'=>'credit,balance','condition'=>'company_id=:company_id',
					'params'=>array(':company_id'=>1),)
			);

			// credit_val_limit_to
			return ($mc_credit['credit'] <= $mc_credit['balance']) ? $mc_credit['credit'] : $mc_credit['balance'];

		}else{

			$parent_credt=TableAgent::model()->find(array('select'=>'credit,balance',
					'condition'=>'account_id=:account_id',
					'params'=>array(':account_id' => substr($account_id,0,strlen($account_id)-2)),)
			);

			// credit_val_limit_to
			return ($parent_credt['credit'] <= $parent_credt['balance']) ? $parent_credt['credit'] : $parent_credt['balance'];

		}
	}

	/**
	 * @todo check agent's currency_id. If agent/player's currency_id == parent's currency_id, return TRUE
	 * @author leokarl
	 * @since 2012-12-11
	 * @param string $account_id
	 * @param int $currency_id
	 */
	public function isCurrencyIdValid($account_id,$currency_id){
		$rd = TableAgent::model()->find(array('select'=>'currency_id',
				'condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => substr($account_id,0,strlen($account_id)-2)),)
		);

		return ($currency_id == $rd['currency_id']) ? TRUE : FALSE;
	}
	
	/**
	 * @todo Check if agent has permission to access special module
	 * @param unknown $accountID is upper agent user account
	 * @author Kimny MOUK
	 * @since 2013-06-07
	 */
	public function isUpperAgentOnSpecialPermissionGroup($accountID)
	{   
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT COUNT(0) FROM AuthAssignment WHERE userid ='" . $accountID . "'");
		$row=$command->queryScalar();
		return $row;
	}

	/**
	 * @todo save agent/player settings
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-10
	 */
	public function saveAgentNewSubCompany()
	{
		// session validation: continue if session is not expired!
		if(!isset(Yii::app()->session['account_id'])){
			exit('session_expired');
		}

		if(!isset($_POST['account_id'])){
			exit('invalid_data_method');
		}

		// check cash player account
		if($this->isAccountExistedInCashPlayer($_POST['account_id'])){
			exit('conflict_account_with_cashplayer');
		}
		if(!isset($_POST['account_name'])){
			exit("invalid_data_method");
		}

		// Disallow special characters on $_POST['account_name'] like ~!@#$%^&*()_+=-`;':",.<>/?\|
		if($this->isSpecialCharPresent($_POST['account_name'])){
			exit("invalid_account_name");
		}

		// Credit must be numeric
		if(!is_numeric($_POST['credit'])){
			exit('credit_must_be_number');
		}

		// Negative credit not allowed
		if($_POST['credit'] < 0){
			exit('negative_credit_not_allowed');
		}


		// Sharing/Commission validation
		if(!$this->isSharingCommissionValid( $_POST['baccarat_sharing'], $_POST['roullete_sharing'], $_POST['dragontiger_sharing'], $_POST['blackjack_sharing'], $_POST['american_roulette_sharing'], $_POST['slot_sharing'],
				$_POST['baccarat_commission'], $_POST['roullete_commission'], $_POST['dragontiger_commission'], $_POST['blackjack_commission'], $_POST['american_roulette_commission'], $_POST['slot_commission']
		)){
			exit("invalid_sharing_commission");
		}

		// Currency must be equal to parent's currency ($_POST['currency_id'])
		if($_POST['agent_type'] != 'SC')
		{
			if(!$this->isCurrencyIdValid($_POST['account_id'], $_POST['currency_id'])){
				exit("invalid_currency");
			}
		}
		//
		if ($_POST['agent_type']=='MEM'){
			if($_POST['max_win_multiple'] < -1 || !is_numeric($_POST['max_win_multiple'])){
				exit("invalid_max_win_multiple");
			}
			if($_POST['max_win_daily'] < 0 || !is_numeric($_POST['max_win_daily'])){
				exit("invalid_daily_max_win");
			}
		}
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;
		$AccountID = Yii::app()->session['account_id'];
		$level = Yii::app()->session['level_name'];
		$dateTime = date('Y-m-d H:i:s');
		$label = $this->getAgentLevel($_POST['account_id']); // tracing agent level---added by Allan/2012-06-19
		$agent_current_casinos = $this->getCasinoLevelSettingsByAccountId($_POST['account_id']); // agent's current casinos
		$credit_before_save = $this->getFieldData($_POST['account_id'], 'credit'); // instead of $_POST['credit_before']
		$casino_access_level_before = $this->getFieldData($_POST['account_id'], 'casino_access_level');
		$baccarat_limit_before = $this->getFieldData($_POST['account_id'], 'limit_baccarat');
		$roulette_limit_before = $this->getFieldData($_POST['account_id'], 'limit_roulette');
		$dragon_tiger_limit_before = $this->getFieldData($_POST['account_id'], 'limit_dragon_tiger');
		$winlosslimit = new WinLossLimit();
		//$submit_daily_max_win = array();
		

		if ($_POST['agent_type']!='MEM'){ // instead of $_POST['credit_assigned']
			if($this->isAddAgentTransaction($_POST['account_id'])){ // for new account
				$total_assigned_credit = $_POST['credit'];
			}else{ // for update account
				$total_assigned_credit = $this->getFieldData($_POST['account_id'], 'credit_assigned') + ($_POST['credit'] - $credit_before_save);
			}
		}

		$transaction = $connection->beginTransaction(); // begin transaction
		// transaction from SC to AGT
		if ($_POST['agent_type']!='MEM')
		{
			
			// continue if data doesn't exist in the database
			if($this->agentDuplicateEntryValidation($_POST['credit'], $credit_before_save, $dateTime, $_POST['account_id'],
					Yii::app()->session['account_id']) == ''){

					
				// get casino level setting id
				//$casinoLevel=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv','params'=>array(':htv'=>$_POST['htv999'],':sv'=>$_POST['savan999'],':vig'=>$_POST['costa999'],':vv'=>$_POST['virtua999']),));
				$casinoLevel_=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv and sb=:sb and svs=:svs','params'=>array(':htv'=>$_POST['htv999'],':sv'=>$_POST['savan999'],':vig'=>$_POST['costa999'],':vv'=>$_POST['virtua999'],':sb'=>$_POST['sportsbook999'],':svs'=>$_POST['slotsvegas999'])));
				if ($casinoLevel_['level_id']==''){
					$postLevelSetting= new TableCasinoAccessLevelSetting;
					$postLevelSetting->htv = $_POST['htv999'];
					$postLevelSetting->sv  = $_POST['savan999'];
					$postLevelSetting->vig = $_POST['costa999'];
					$postLevelSetting->vv  = $_POST['virtua999'];
					$postLevelSetting->sb  = $_POST['sportsbook999'];
					$postLevelSetting->svs = $_POST['slotsvegas999'];
					$postLevelSetting->save();
						
					
				}
				$casinoLevel = $this->getCasinoLevelSettingId($_POST['htv999'],$_POST['savan999'],$_POST['costa999'],$_POST['virtua999'],$_POST['sportsbook999'],$_POST['slotsvegas999']);

				// create new agent if isAddAgentTransaction returns TRUE
				if ($this->isAddAgentTransaction($_POST['account_id'])){
					
					// If upper agent has special permission this permission must be inherited to down-line agent
					if ((int)$this->isUpperAgentOnSpecialPermissionGroup($_POST['agent_parent_id']) ==1)
					{
						$auth = Yii::app()->authManager;
						$auth->assign('agentSpecialPermissionGroup',$_POST['account_id']);
					}
					
					// agent can only create account under his/her account.
					if(User::getUserType() == 'agent'){
						if(!$this->isAccountForAddValid(Yii::app()->session['account_id'], $_POST['account_id'])){
							exit('invalid_account_id');
						}
					}

					//Credit Limit: If credit <= balance then credit limit to is equal to credit. balance in the other way around.
					$credit_val_limit_to = $this->getCreditLimitToByAgent($_POST['account_id'], $_POST['agent_type']);

					// credit limit validation
					if($_POST['credit'] <= $credit_val_limit_to){
							
						if ($_POST['agent_parent_id']!=''){

							//deduct assigned credit from its parent agent
							$rd=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_POST['agent_parent_id']),));
							$upperAgentBalanceBefore=$rd['balance'];
							$upperAgentCreditBefore=$rd['credit'];
							TableAgent::model()->updateAll(array('credit'=>$rd['credit'] - $_POST['credit'],'balance'=>$rd['balance'] - $_POST['credit']),'account_id="'.$_POST['agent_parent_id'].'"');
							$upperAgentBalanceAfter=$rd['balance'] - $_POST['credit'];
							$upperAgentCreditAfter=$rd['credit'] - $_POST['credit'];

						}else{

							//deduct assigned credit from main company
							$rd=TableMainCompany::model()->find(array('select'=>'credit,balance','condition'=>'company_id=:company_id','params'=>array(':company_id'=>1),));
							$upperAgentBalanceBefore=$rd['balance'];
							$upperAgentCreditBefore=$rd['credit'];
							$get=TableCurrency::model()->find(array('select'=>'exchange_rate','condition'=>'id=:id','params'=>array(':id'=>$_POST['currency_id'])));

							$convertedCredit=(($_POST['credit'])/$get['exchange_rate'])*1;
							TableMainCompany::model()->updateAll(array('credit'=>$rd['credit'] - $convertedCredit,'balance'=>$rd['balance'] - $convertedCredit),'company_id=1');

							$upperAgentBalanceAfter=$rd['balance'] - $convertedCredit;
							$upperAgentCreditAfter=$rd['credit'] - $convertedCredit;

						}
							
						//end credit limit

						$post = new TableAgent();
						$post->account_id = strtoupper($_POST['account_id']);
						$post->account_name = trim($_POST['account_name']);
						if ($_POST['agent_parent_id']!=''){
							$post->agent_parent_id = $_POST['agent_parent_id'];
						}
						$post->agent_type = $_POST['agent_type'];
						$post->agent_password = CV999Utility::generateOneWayPassword($_POST['agent_password']);
						$post->phone_number = trim($_POST['phone_number']);
						$post->agent_email = trim($_POST['agent_email']);
						if($total_assigned_credit>=0){
							$post->credit_assigned = $total_assigned_credit;
						}
							
						$post->credit = $_POST['credit'];
						$post->balance = $_POST['credit'];
						if($_POST['credit_percentage']>=0){
							$post->credit_percentage=$_POST['credit_percentage'];
						}
						$post->permission = $_POST['permission'];
						$post->currency_id = $_POST['currency_id'];
						$post->agent_status_id = 1;
						$post->opening_acc_date = $dateTime;
						$post->casino_id = 1;
						$post->market_type = $_POST['marketType'];
						
						if($_POST['selected_baccarat_limit'] != 0){
							$post->limit_baccarat = $_POST['selected_baccarat_limit'];
						}else{
							$post->limit_baccarat = $_POST['baccarat'];
						}
						if($_POST['selected_roullete_limit'] != 0){
							$post->limit_roulette = $_POST['selected_roullete_limit'];
						}else{
							$post->limit_roulette = $_POST['roullete'];
						}
						if($_POST['selected_dragon_tiger_limit'] != 0){
							$post->limit_dragon_tiger = $_POST['selected_dragon_tiger_limit'];
						}else{
							$post->limit_dragon_tiger = $_POST['dragontiger'];
						}
						$post->limit_blackjack = $_POST['blackjack'];
						$post->limit_american_roulette = $_POST['american_roulette'];
						$post->limit_slots = $_POST['slot'];
						$post->share_baccarat = $_POST['baccarat_sharing'];
						$post->share_roulette = $_POST['roullete_sharing'];
						$post->share_dragon_tiger = $_POST['dragontiger_sharing'];
						$post->share_blackjack = $_POST['blackjack_sharing'];
						$post->share_american_roulette = $_POST['american_roulette_sharing'];
						$post->share_slots = $_POST['slot_sharing'];
						$post->commission_baccarat = $_POST['baccarat_commission'];
						$post->commission_roulette = $_POST['roullete_commission'];
						$post->commission_dragon_tiger = $_POST['dragontiger_commission'];
						$post->commission_blackjack = $_POST['blackjack_commission'];
						$post->commission_american_roulette = $_POST['american_roulette_commission'];
						$post->commission_slots = $_POST['slot_commission'];
						//$post->casino_access_level=$casinoLevel['level_id'];
						$post->casino_access_level = $casinoLevel;
						//get the white_list of the parent
						if(substr($_POST['account_id'],0,strlen($_POST['account_id'])-2)!=''){
							$agent_parent_white_list = TableAgent::model()->find(array('select'=>'white_list','condition'=>'account_id=:account_id','params'=>array(':account_id'=>substr($_POST['account_id'],0,strlen($_POST['account_id'])-2)),));
							$post->white_list = $agent_parent_white_list['white_list'];
							// 							}else{
							// 								$post->white_list = 0;
						}
						// Baccarat|Dragon Tiger|European Roulette|American Roulette|Blackjack|Slots
						$post->revenue_sharing_limit_to = $_POST['baccarat_sharing'] .'^'. $_POST['baccarat_commission']
						.'|'. $_POST['dragontiger_sharing'] .'^'. $_POST['dragontiger_commission']
						.'|'. $_POST['roullete_sharing'] .'^'. $_POST['roullete_commission']
						.'|'. $_POST['american_roulette_sharing'] .'^'. $_POST['american_roulette_commission']
						.'|'. $_POST['blackjack_sharing'] .'^'. $_POST['blackjack_commission']
						.'|'. $_POST['slot_sharing'] .'^'. $_POST['slot_commission'];
						$post->save();
							
						$getValue=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_POST['account_id']),));
							
						$updt= new TableAgentTransactionHistory();
						$updt->transaction_number= TransactionNumber::generateTransactionNumber("AC");
						$updt->agent_account_id=strtoupper($_POST['account_id']);
						$updt->currency_id=$_POST['currency_id'];
						$updt->trans_type_id=7;
						$updt->amount=$_POST['credit'];
						$updt->balance_before=0;
						$updt->balance_after=$getValue['balance'];
						$updt->credit_before=0;
						$updt->credit_after=$getValue['credit'];
						$updt->sharing_before='';
						$updt->sharing_after=$_POST['baccarat_sharing'].'^'.$_POST['roullete_sharing'].'^'.$_POST['dragontiger_sharing'].'^'.$_POST['blackjack_sharing'].'^'.$_POST['american_roulette_sharing'].'^'.$_POST['slot_sharing'];
						$updt->commission_before='';
						$updt->commission_after=$_POST['baccarat_commission'].'^'.$_POST['roullete_commission'].'^'.$_POST['dragontiger_commission'].'^'.$_POST['blackjack_commission'].'^'.$_POST['american_roulette_commission'].'^'.$_POST['slot_commission'];
						$updt->upper_agent_balance_before=$upperAgentBalanceBefore;
						$updt->upper_agent_balance_after=$upperAgentBalanceAfter;
						$updt->upper_agent_credit_before=$upperAgentCreditBefore;
						$updt->upper_agent_credit_after=$upperAgentCreditAfter;
						$updt->transaction_date=$dateTime;
						$updt->operator_id=Yii::app()->session['account_id'];
						$updt->save();
							
							
						$postLog=new TableLog();
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=strtoupper($_POST['account_id']);
						$postLog->operated_level=$label;
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=1;
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> create new account:<label style=\"color:green\">'.$label.'</label> <label style=\"color:red\">'.strtoupper($_POST['account_id']).'</label>| Assigned credit:<label style=\"color:red\">'.$_POST['credit'].'</label></b>';
						$postLog->save();
							
							
						$postLog=new TableLog();
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=strtoupper($_POST['account_id']);
						$postLog->operated_level=$label;
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=7; // 7 is change commission.
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ASSIGNED COMMISSION to <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> '.
								$this->newSharingCommissionLogDetails($_POST['baccarat_commission'], $_POST['roullete_commission'], $_POST['dragontiger_commission'],
										$_POST['blackjack_commission'], $_POST['american_roulette_commission'], $_POST['slot_commission']) .'</b>';
						$postLog->save();
							
						$postLog=new TableLog();
						$postLog->operated_by=$AccountID;
						$postLog->operated_by_level=$level;
						$postLog->operated=strtoupper($_POST['account_id']);
						$postLog->operated_level=$label;
						$postLog->operation_time=$dateTime;
						$postLog->log_type_id=6; // 6 is change sharing.
						$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ASSIGNED SHARING to <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> '.
								$this->newSharingCommissionLogDetails($_POST['baccarat_sharing'], $_POST['roullete_sharing'], $_POST['dragontiger_sharing'],
										$_POST['blackjack_sharing'], $_POST['american_roulette_sharing'], $_POST['slot_sharing']) .'</b>';
						$postLog->save();
						echo 1;
					}else{
						exit('credit_exceeded');
					}
				}
				else
				{
					// isAddAgentTransaction function returns FALSE. Update Agent
					if($_POST['s_edit']==1){
						// if account exist, continue updates
						if(!$this->isAccountExistedInAgent(strtoupper($_POST['account_id']))){
							exit('account_doenst_exist');
						}

						// checking old record  added by Allan date:2012-06-19
						$connection = Yii::app()->db_cv999_fd_master;
						$command = $connection->createCommand("SELECT agent_password,currency_id,credit,credit_percentage,agent_status_id,share_baccarat,share_roulette,share_dragon_tiger,share_blackjack,share_american_roulette,share_slots,commission_baccarat,commission_roulette,commission_dragon_tiger,commission_blackjack,commission_american_roulette,commission_slots from tbl_agent  WHERE account_id='" . $_POST['account_id'] . "'");
						$rd=$command->queryRow();
						$password=$rd['agent_password'];
						$currency=$rd['currency_id'];
						$credit=$rd['credit'];
						$securityDeposit=$rd['credit_percentage'];
						$status=$rd['agent_status_id'];
						$baccaratShare=$rd['share_baccarat'];
						$roulleteEURShare=$rd['share_roulette'];
						$dragonTigerShare=$rd['share_dragon_tiger'];
						$blackjackShare=$rd['share_blackjack'];
						$roulleteAMEShare=$rd['share_american_roulette'];
						$slotShare=$rd['share_slots'];
						$baccaratCommission=$rd['commission_baccarat'];
						$roulleteEURCommission=$rd['commission_roulette'];
						$dragonTigerCommission=$rd['commission_dragon_tiger'];
						$blackjackCommission=$rd['commission_blackjack'];
						$roulleteAMECommission=$rd['commission_american_roulette'];
						$slotCommission=$rd['commission_slots'];

						//Get old credit and old balance
						$bal = TableAgent::model()->find(array('select'=>'balance,credit,agent_type','condition'=>'account_id=:account_id',
								'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
						if($bal['agent_type'] != 'SC'){
							$prnt_id = TableAgent::model()->find(array('select'=>'agent_parent_id,credit,balance','condition'=>'account_id=:account_id',
									'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
							$rd_credit = TableAgent::model()->find(array('select'=>'credit','condition'=>'account_id=:account_id',
									'params'=>array(':account_id'=>$prnt_id['agent_parent_id']),));
						}else{
							$rd_credit = TableMainCompany::model()->find(array('select'=>'credit','condition'=>'company_id=:company_id',
									'params'=>array(':company_id'=>1),));
						}
							
						$fltCreditLimitTo = ($rd_credit['credit']+$bal['credit']); // credit_limit_to
						$fltCreditLimitFrom = (($bal['credit'] - $bal['balance']) >= 0) ? ($bal['credit']-$bal['balance']) : 0; // credit_limit_from

						// credit limit validation
						if(($_POST['credit'] >= $fltCreditLimitFrom) && ($_POST['credit'] <= $fltCreditLimitTo)){

							//$casinoLevel=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv','params'=>array(':htv'=>$_POST['htv999'],':sv'=>$_POST['savan999'],':vig'=>$_POST['costa999'],':vv'=>$_POST['virtua999']),));
							$casinoLevel_=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv and sb=:sb and svs=:svs','params'=>array(':htv'=>$_POST['htv999'],':sv'=>$_POST['savan999'],':vig'=>$_POST['costa999'],':vv'=>$_POST['virtua999'],':sb'=>$_POST['sportsbook999'],':svs'=>$_POST['slotsvegas999'])));
							if ($casinoLevel_['level_id']==''){
								$postLevelSetting= new TableCasinoAccessLevelSetting;
								$postLevelSetting->htv = $_POST['htv999'];
								$postLevelSetting->sv  = $_POST['savan999'];
								$postLevelSetting->vig = $_POST['costa999'];
								$postLevelSetting->vv  = $_POST['virtua999'];
								$postLevelSetting->sb  = $_POST['sportsbook999'];
								$postLevelSetting->svs = $_POST['slotsvegas999'];
								$postLevelSetting->save();
							
							}
							$casinoLevel=$this->getCasinoLevelSettingId($_POST['htv999'],$_POST['savan999'],$_POST['costa999'],$_POST['virtua999'],$_POST['sportsbook999'],$_POST['slotsvegas999']);
								
							$agentCasinoLevelIDBefore=TableAgent::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id',
									'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
							$agentCasinoBefore=TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id',
									'params'=>array(':level_id'=>$agentCasinoLevelIDBefore['casino_access_level']),));


							$post=TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id',
									'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
							$post->account_name=trim($_POST['account_name']);
							if(trim($_POST['agent_password'])!=''){
								$post->agent_password=CV999Utility::generateOneWayPassword($_POST['agent_password']);
							}
							$post->phone_number=trim($_POST['phone_number']);
							$post->agent_email=trim($_POST['agent_email']);
							if($credit_before_save>=0){
								if($_POST['credit']!=$credit_before_save){
									if($total_assigned_credit>=0){
										$post->credit_assigned=$total_assigned_credit;
									}
								}
							}
							$post->credit=$_POST['credit'];
							if($_POST['credit_percentage']>=0){
								$post->credit_percentage=$_POST['credit_percentage'];
							}
							$post->balance=$bal['balance'] + ($_POST['credit'] - $bal['credit']);
							$post->permission=$_POST['permission'];
							$post->currency_id=$_POST['currency_id'];
							if($_POST['selected_baccarat_limit']!=0){
								$post->limit_baccarat=$_POST['selected_baccarat_limit'];
							}else{
								$post->limit_baccarat=$_POST['baccarat'];
							}
							if($_POST['selected_roullete_limit']!=0){
								$post->limit_roulette=$_POST['selected_roullete_limit'];
							}else{
								$post->limit_roulette=$_POST['roullete'];
							}
							if($_POST['selected_dragon_tiger_limit']!=0){
								$post->limit_dragon_tiger=$_POST['selected_dragon_tiger_limit'];
							}else{
								$post->limit_dragon_tiger=$_POST['dragontiger'];
							}
							$post->limit_blackjack=$_POST['selected_blackjack_limit'];
							$post->limit_blackjack=$_POST['blackjack'];
							$post->limit_american_roulette=$_POST['american_roulette'];
							$post->limit_slots=$_POST['slot'];
							$post->share_baccarat=$_POST['baccarat_sharing'];
							$post->share_roulette=$_POST['roullete_sharing'];
							$post->share_dragon_tiger=$_POST['dragontiger_sharing'];
							$post->share_blackjack=$_POST['blackjack_sharing'];
							$post->share_american_roulette=$_POST['american_roulette_sharing'];
							$post->share_slots=$_POST['slot_sharing'];
							$post->commission_baccarat=$_POST['baccarat_commission'];
							$post->commission_roulette=$_POST['roullete_commission'];
							$post->commission_dragon_tiger=$_POST['dragontiger_commission'];
							$post->commission_blackjack=$_POST['blackjack_commission'];
							$post->commission_american_roulette=$_POST['american_roulette_commission'];
							$post->commission_slots=$_POST['slot_commission'];
							$post->casino_access_level=$casinoLevel;
							$post->market_type = isset($_POST['marketType'])? $_POST['marketType'] : 1;
							$post->save();
								
							
							//Start: log for casino_access_level_setting
							$log_details='';
							if($agentCasinoBefore['htv']!=$_POST['htv999']){
								$strWord='';
								$strCasinoSelectOrUnSelect='';
								if($_POST['htv999']==1){
									$strWord='SELECT';
								}else{$strWord='UNSELECT';
								}
								$log_details.='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> '.$strWord.' HTV999 for <label style=\"color:green\">'. strtoupper($_POST['account_id']) .'</label></b>. \n';
							}
							if($agentCasinoBefore['sv']!=$_POST['savan999']){
								$strWord='';
								if($_POST['savan999']==1){
									$strWord='SELECT';
								}else{$strWord='UNSELECT';
								}
								$log_details.='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> '.$strWord.' SAVANVEGAS999 for <label style=\"color:green\">'. strtoupper($_POST['account_id']) .'</label></b>. \n';
							}
							if($agentCasinoBefore['vig']!=$_POST['costa999']){
								$strWord='';
								if($_POST['costa999']==1){
									$strWord='SELECT';
								}else{$strWord='UNSELECT';
								}
								$log_details.='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> '.$strWord.' COSTAVEGAS999 for <label style=\"color:green\">'. strtoupper($_POST['account_id']) .'</label></b>. \n';
							}
							if($agentCasinoBefore['vv']!=$_POST['virtua999']){
								$strWord='';
								if($_POST['virtua999']==1){
									$strWord='SELECT';
								}else{$strWord='UNSELECT';
								}
								$log_details.='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> '.$strWord.' VIRTUAVEGAS999 for <label style=\"color:green\">'. strtoupper($_POST['account_id']) .'</label></b>. \n';
							}
							if($agentCasinoBefore['svs']!=$_POST['slotsvegas999']){
								$strWord='';
								if($_POST['slotsvegas999']==1){
									$strWord='SELECT';
								}else{$strWord='UNSELECT';
								}
								$log_details.='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> '.$strWord.' SLOTSVEGAS999 for <label style=\"color:green\">'. strtoupper($_POST['account_id']) .'</label></b>. \n';
							}
							if($log_details!=''){
								$postLog=new TableLog();
								$postLog->operated_by=$AccountID;
								$postLog->operated_by_level=$level;
								$postLog->operated=strtoupper($_POST['account_id']);
								$postLog->operated_level=$label;
								$postLog->operation_time=$dateTime;
								$postLog->log_type_id=14;
								$postLog->log_details=$log_details;
								$postLog->save();
							}
							//End: log for casino_access_level_setting
								
							/*
							 * @todo casino_access_level_setting to downline agents
							* @author leokarl
							*/
							$this->updateLowerAgentAndPlayerCasinoLevel($casinoLevel, strtoupper($_POST['account_id']), $agent_current_casinos);

							/*
							 * @todo Sharing and Commission: Remove values from downline agents if limit,sharing or commission is 0
							* @author leokarl
							*/

							if($_POST['selected_baccarat_limit']==0 && $_POST['baccarat']==0){
								TableAgent::model()->updateAll(array('limit_baccarat'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
								TableAgentPlayer::model()->updateAll(array('limit_baccarat'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
							}
							if($_POST['selected_roullete_limit']==0 && $_POST['roullete']==0){
								TableAgent::model()->updateAll(array('limit_roulette'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
								TableAgentPlayer::model()->updateAll(array('limit_roulette'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
							}
							if($_POST['selected_dragon_tiger_limit']==0 && $_POST['dragontiger']==0){
								TableAgent::model()->updateAll(array('limit_dragon_tiger'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
								TableAgentPlayer::model()->updateAll(array('limit_dragon_tiger'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
							}
							if($_POST['blackjack']==0){
								TableAgent::model()->updateAll(array('limit_blackjack'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
								TableAgentPlayer::model()->updateAll(array('limit_blackjack'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
							}
							if($_POST['american_roulette']==0){
								TableAgent::model()->updateAll(array('limit_american_roulette'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
								TableAgentPlayer::model()->updateAll(array('limit_american_roulette'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
							}
							if($_POST['slot']==0){
								TableAgent::model()->updateAll(array('limit_slots'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
								TableAgentPlayer::model()->updateAll(array('limit_slots'=>0),'account_id like "'.strtoupper($_POST['account_id']).'%"');
							}
								
							
							/*
							 * @todo check agent's sharing value. If agent unselect casino, agent's sharing
							* including his/her lower agents and players will be 0.
							* @author leokarl
							*/
							$this->upperAgentSharingWithZeroValue(strtoupper($_POST['account_id']), $_POST['baccarat_sharing'],
									$_POST['roullete_sharing'], $_POST['dragontiger_sharing'], $_POST['blackjack_sharing'],
									$_POST['american_roulette_sharing'], $_POST['slot_sharing']);


							/*
							 * @todo check agent's commission value. If agent unselect casino, agent's commission
							* including his/her lower agents and players will be 0.
							* @author leokarl
							*/
							$this->upperAgentCommissionWithZeroValue(strtoupper($_POST['account_id']), $_POST['baccarat_commission'],
									$_POST['roullete_commission'], $_POST['dragontiger_commission'], $_POST['blackjack_commission'],
									$_POST['american_roulette_commission'], $_POST['slot_commission']);


							
							//deduct credit from the parent
							if($_POST['agent_type']!='SC'){
								$prnt_id=TableAgent::model()->find(array('select'=>'agent_parent_id','condition'=>'account_id=:account_id','params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
								$rd=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$prnt_id['agent_parent_id']),));
								$upperAgentBalanceBefore = $rd['balance'];
								$upperAgentCreditBefore = $rd['credit'];
								$upperAgentCreditAfter = $rd['credit'];
								$upperAgentBalanceAfter = $rd['balance'];
									
								if($credit_before_save >= 0){
									if($_POST['credit'] != $credit_before_save){
										if($_POST['credit'] > $credit_before_save){
											//TableAgent::model()->updateAll(array('credit'=>$rd['credit'] - ($_POST['credit']-$credit_before_save),'balance'=>$rd['credit'] - ($_POST['credit']-$credit_before_save)),'account_id="'.$prnt_id['agent_parent_id'].'"');
											TableAgent::model()->updateAll(array('credit'=>$rd['credit'] - ($_POST['credit']-$credit_before_save),'balance'=>$rd['balance'] - ($_POST['credit']-$credit_before_save)),'account_id="'.$prnt_id['agent_parent_id'].'"');
											$upperAgentBalanceAfter=$rd['balance'] - ($_POST['credit']-$credit_before_save);
											$upperAgentCreditAfter=$rd['credit'] - ($_POST['credit']-$credit_before_save);
										}else{
											TableAgent::model()->updateAll(array('credit'=>$rd['credit'] + ($credit_before_save-$_POST['credit']),'balance'=>$rd['balance'] + ($credit_before_save-$_POST['credit'])),'account_id="'.$prnt_id['agent_parent_id'].'"');
											$upperAgentBalanceAfter=$rd['balance'] + ($credit_before_save-$_POST['credit']);
											$upperAgentCreditAfter=$rd['credit'] + ($credit_before_save-$_POST['credit']);
										}
									}
								}
							}else{

								//deduct assigned credit from main company
									
								if($credit_before_save>=0){
									$rdCom=TableMainCompany::model()->find(array('select'=>'credit,balance','condition'=>'company_id=:company_id','params'=>array(':company_id'=>1),));
									$upperAgentBalanceBefore=$rdCom['balance'];
									$upperAgentCreditBefore=$rdCom['credit'];
									$upperAgentBalanceAfter=$rdCom['balance'];
									$upperAgentCreditAfter=$rdCom['credit'];
									$get=TableCurrency::model()->find(array('select'=>'exchange_rate','condition'=>'id=:id','params'=>array(':id'=>$_POST['currency_id'])));

									if($_POST['credit']!=$credit_before_save){
										$convertedCurrentCredit=(($_POST['credit']-$credit_before_save)/$get['exchange_rate'])*1;
										if($_POST['credit']>$credit_before_save){
											$upperAgentBalanceAfter=$rdCom['balance'] - $convertedCurrentCredit;
											$upperAgentCreditAfter=$rdCom['credit'] - $convertedCurrentCredit;
											TableMainCompany::model()->updateAll(array('credit'=>$rdCom['credit'] - $convertedCurrentCredit,'balance'=>$rdCom['balance'] - $convertedCurrentCredit),'company_id=1');

										}else{
											TableMainCompany::model()->updateAll(array('credit'=>$rdCom['credit'] + $convertedCurrentCredit,'balance'=>$rdCom['balance'] + $convertedCurrentCredit),'company_id=1');
											$upperAgentBalanceAfter=$rdCom['balance'] + $convertedCurrentCredit;
											$upperAgentCreditAfter=$rdCom['credit'] + $convertedCurrentCredit;
										}
									}
								}
							}
							
							//Transaction history added by Allan/ Date: 2012-06-19
							$getValue=TableAgent::model()->find(array('select'=>'credit,balance',
									'condition'=>'account_id=:account_id','params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));

							/*
							 * @todo detect if there are changes on sharing and commission
							* @author leokarl
							* @var int $s_sharing_commission_chage
							*/
							
							$s_sharing_commission_chage = 0;
							if($this->isSharingCommissionChange($baccaratCommission, $_POST['baccarat_commission']) ||
							$this->isSharingCommissionChange($roulleteEURCommission, $_POST['roullete_commission']) ||
							$this->isSharingCommissionChange($dragonTigerCommission, $_POST['dragontiger_commission']) ||
							$this->isSharingCommissionChange($blackjackCommission, $_POST['blackjack_commission']) ||
							$this->isSharingCommissionChange($roulleteAMECommission, $_POST['american_roulette_commission']) ||
							$this->isSharingCommissionChange($slotCommission, $_POST['slot_commission'])
							||
							$this->isSharingCommissionChange($baccaratShare, $_POST['baccarat_sharing']) ||
							$this->isSharingCommissionChange($roulleteEURShare, $_POST['roullete_sharing']) ||
							$this->isSharingCommissionChange($dragonTigerShare, $_POST['dragontiger_sharing']) ||
							$this->isSharingCommissionChange($blackjackShare, $_POST['blackjack_sharing']) ||
							$this->isSharingCommissionChange($roulleteAMEShare, $_POST['american_roulette_sharing']) ||
							$this->isSharingCommissionChange($slotShare, $_POST['slot_sharing'])
							// 									||
							// 									($baccarat_limit_before != $_POST['selected_baccarat_limit'] || $roulette_limit_before != $_POST['selected_roullete_limit']
									// 											|| $dragon_tiger_limit_before != $_POST['selected_dragon_tiger_limit'])
							){

								$s_sharing_commission_chage = 1;

								$post=TableAgent::model()->find(array('select'=>'*','condition'=>'account_id=:account_id',
										'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
									
								$post->revenue_sharing_limit_to = $_POST['baccarat_sharing'] .'^'. $_POST['baccarat_commission']
								.'|'. $_POST['dragontiger_sharing'] .'^'. $_POST['dragontiger_commission']
								.'|'. $_POST['roullete_sharing'] .'^'. $_POST['roullete_commission']
								.'|'. $_POST['american_roulette_sharing'] .'^'. $_POST['american_roulette_commission']
								.'|'. $_POST['blackjack_sharing'] .'^'. $_POST['blackjack_commission']
								.'|'. $_POST['slot_sharing'] .'^'. $_POST['slot_commission'];
								$post->save();
							}
							
							if(($_POST['credit'] <> $credit_before_save)){
									
								$updt= new TableAgentTransactionHistory();
								if($_POST['credit'] > $credit_before_save){

									$updt->transaction_number= TransactionNumber::generateTransactionNumber("AC");
									$updt->trans_type_id = 7;

								}else if($_POST['credit'] < $credit_before_save){

									$updt->transaction_number= TransactionNumber::generateTransactionNumber("RC");
									$updt->trans_type_id = 8;

								}
									
								$updt->amount=$_POST['credit']- $credit_before_save;
								$updt->agent_account_id=strtoupper($_POST['account_id']);
								$updt->currency_id=$_POST['currency_id'];
								$updt->balance_before=$bal['balance'];
								$updt->balance_after=$getValue['balance'];
								$updt->credit_before=$credit_before_save;
								$updt->credit_after=$getValue['credit'];
								$updt->sharing_before=$baccaratShare.'^'.$roulleteEURShare.'^'.$dragonTigerShare.'^'.$blackjackShare.'^'.$roulleteAMEShare.'^'.$slotShare;
								$updt->sharing_after=$_POST['baccarat_sharing'].'^'.$_POST['roullete_sharing'].'^'.$_POST['dragontiger_sharing'].'^'.$_POST['blackjack_sharing'].'^'.$_POST['american_roulette_sharing'].'^'.$_POST['slot_sharing'];
								$updt->commission_before=$baccaratCommission.'^'.$roulleteEURCommission.'^'.$dragonTigerCommission.'^'.$blackjackCommission.'^'.$roulleteAMECommission.'^'.$slotCommission;
								$updt->commission_after=$_POST['baccarat_commission'].'^'.$_POST['roullete_commission'].'^'.$_POST['dragontiger_commission'].'^'.$_POST['blackjack_commission'].'^'.$_POST['american_roulette_commission'].'^'.$_POST['slot_commission'];
								$updt->upper_agent_balance_before=$upperAgentBalanceBefore;
								$updt->upper_agent_balance_after=$upperAgentBalanceAfter;
								$updt->upper_agent_credit_before=$upperAgentCreditBefore;
								$updt->upper_agent_credit_after=$upperAgentCreditAfter;
								$updt->transaction_date=$dateTime;
								$updt->operator_id=Yii::app()->session['account_id'];
								$updt->save();
									
									
							}
							echo 2;

							//insert changes to Log added by Allan/ Date: 2012-06-19
							if ($securityDeposit!= $_POST['credit_percentage']){
								$postLog=new TableLog();
								$postLog->operated_by=$AccountID;
								$postLog->operated_by_level=$level;
								$postLog->operated=strtoupper($_POST['account_id']);
								$postLog->operated_level=$label;
								$postLog->operation_time=$dateTime;
								$postLog->log_type_id=25;
								$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Security Deposit from <label style=\"color:green\">'.$securityDeposit.'</label> to <label style=\"color:red\">'.$_POST['credit_percentage'].'</label></b>';
								$postLog->save();
							}

							if ($password != CV999Utility::generateOneWayPassword($_POST['agent_password']) && trim($_POST['agent_password']) !=''){
								$postLog=new TableLog();
								$postLog->operated_by=$AccountID;
								$postLog->operated_by_level=$level;
								$postLog->operated=strtoupper($_POST['account_id']);
								$postLog->operated_level=$label;
								$postLog->operation_time=$dateTime;
								$postLog->log_type_id=10;
								$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE password:<label style=\"color:green\">'.$label.'</label> <label style=\"color:red\">'.strtoupper($_POST['account_id']).'</label></b>';
								$postLog->save();
							}
							if ($credit != $_POST['credit']){
								$postLog=new TableLog();
								$postLog->operated_by=$AccountID;
								$postLog->operated_by_level=$level;
								$postLog->operated=strtoupper($_POST['account_id']);
								$postLog->operated_level=$label;
								$postLog->operation_time=$dateTime;
								$postLog->log_type_id=5;
								$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE credit of <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> from <label style=\"color:red\">'.$credit.'</label> to <label style=\"color:red\">'.$_POST['credit'].'</label></b>';
								$postLog->save();
							}
							if($this->isSharingCommissionChange($baccaratCommission, $_POST['baccarat_commission']) ||
							$this->isSharingCommissionChange($roulleteEURCommission, $_POST['roullete_commission']) ||
							$this->isSharingCommissionChange($dragonTigerCommission, $_POST['dragontiger_commission']) ||
							$this->isSharingCommissionChange($blackjackCommission, $_POST['blackjack_commission']) ||
							$this->isSharingCommissionChange($roulleteAMECommission, $_POST['american_roulette_commission']) ||
							$this->isSharingCommissionChange($slotCommission, $_POST['slot_commission'])){
								$postLog=new TableLog();
								$postLog->operated_by=$AccountID;
								$postLog->operated_by_level=$level;
								$postLog->operated=strtoupper($_POST['account_id']);
								$postLog->operated_level=$label;
								$postLog->operation_time=$dateTime;
								$postLog->log_type_id=7;
								$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE COMMISSION of <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> from '.
										$this->getSharingCommissionLogDetails($baccaratCommission . ':' . $_POST['baccarat_commission'],
												$roulleteEURCommission . ':' . $_POST['roullete_commission'],
												$dragonTigerCommission . ':' . $_POST['dragontiger_commission'],
												$blackjackCommission . ':' . $_POST['blackjack_commission'],
												$roulleteAMECommission . ':' . $_POST['american_roulette_commission'],
												$slotCommission . ':' . $_POST['slot_commission']) .'</b>';
								$postLog->save();
							}

							if($this->isSharingCommissionChange($baccaratShare, $_POST['baccarat_sharing']) ||
							$this->isSharingCommissionChange($roulleteEURShare, $_POST['roullete_sharing']) ||
							$this->isSharingCommissionChange($dragonTigerShare, $_POST['dragontiger_sharing']) ||
							$this->isSharingCommissionChange($blackjackShare, $_POST['blackjack_sharing']) ||
							$this->isSharingCommissionChange($roulleteAMEShare, $_POST['american_roulette_sharing']) ||
							$this->isSharingCommissionChange($slotShare, $_POST['slot_sharing'])){
								$postLog=new TableLog();
								$postLog->operated_by=$AccountID;
								$postLog->operated_by_level=$level;
								$postLog->operated=strtoupper($_POST['account_id']);
								$postLog->operated_level=$label;
								$postLog->operation_time=$dateTime;
								$postLog->log_type_id=6;
								$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE SHARING of <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> from '.
										$this->getSharingCommissionLogDetails($baccaratShare . ':' . $_POST['baccarat_sharing'],
												$roulleteEURShare . ':' . $_POST['roullete_sharing'],
												$dragonTigerShare . ':' . $_POST['dragontiger_sharing'],
												$blackjackShare . ':' . $_POST['blackjack_sharing'],
												$roulleteAMEShare . ':' . $_POST['american_roulette_sharing'],
												$slotShare . ':' . $_POST['slot_sharing']) .'</b>';
								$postLog->save();
							}

							if($baccarat_limit_before != $_POST['selected_baccarat_limit'] || $roulette_limit_before != $_POST['selected_roullete_limit'] || $dragon_tiger_limit_before != $_POST['selected_dragon_tiger_limit']){
								$postLog=new TableLog();
								$postLog->operated_by=$AccountID;
								$postLog->operated_by_level=$level;
								$postLog->operated=strtoupper($_POST['account_id']);
								$postLog->operated_level=$label;
								$postLog->operation_time=$dateTime;
								$postLog->log_type_id=11;
								$postLog->log_details= '<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE TABLE LIMIT of <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label>\'s '.
										$this->getTableLimitLogDetails($baccarat_limit_before . ':' . $_POST['selected_baccarat_limit'],
												$roulette_limit_before . ':' . $_POST['selected_roullete_limit'],
												$dragon_tiger_limit_before . ':' . $_POST['selected_dragon_tiger_limit']) . '</b>';
								$postLog->save();
							}
						}else{
							exit('credit_exceeded');
						}

					}
					else{
						exit('0');
					}
				}
			}

				
			$submit_daily_max_win = array('username'=>'', 'daily_max_win'=>0);
			// Transaction for Agent Players
		}else{
				

			// continue if data doesn't exist in the database
			if($this->playerDuplicateEntryValidation($_POST['credit'], $credit_before_save, $dateTime, strtoupper($_POST['account_id']),
					Yii::app()->session['account_id'])==''){

				// get casino level setting id
				$casinoLevel = $this->getCasinoLevelSettingId($_POST['htv999'],$_POST['savan999'],$_POST['costa999'],$_POST['virtua999'],$_POST['sportsbook999'],$_POST['slotsvegas999']);
					
					
				// isAddPlayerTransaction returns TRUE. Continue add player.
				if ($this->isAddPlayerTransaction(strtoupper($_POST['account_id']))){

					// agent can only create account under his/her account.
					if(User::getUserType() == 'agent'){
						if(!$this->isAccountForAddValid(Yii::app()->session['account_id'], strtoupper($_POST['account_id']))){
							exit('invalid_account_id');
						}
					}

					if($_POST['max_win_multiple']>=-1){
							
						// Credit Limit: If credit <= balance then credit limit to is equal to credit. balance in the other way around.
						$parent_credit=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id',
								'params'=>array(':account_id'=>substr(strtoupper($_POST['account_id']),0,8)),));
							
						$credit_val_limit_to = ($parent_credit['credit'] <= $parent_credit['balance']) ? $parent_credit['credit'] : $parent_credit['balance'];
							
							
						if($_POST['credit'] <= $credit_val_limit_to){

							if ($_POST['agent_parent_id']!=''){
									
								//deduct assigned credit from it's parent agent
								$rd=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id','params'=>array(':account_id'=>$_POST['agent_parent_id']),));
								$upperAgentBalanceBefore=$rd['balance'];
								$upperAgentCreditBefore=$rd['credit'];

								TableAgent::model()->updateAll(array('credit'=>$rd['credit'] - $_POST['credit'],'balance'=>$rd['balance'] - $_POST['credit']),'account_id="'.$_POST['agent_parent_id'].'"');
								$upperAgentBalanceAfter=$rd['balance'] - $_POST['credit'];
								$upperAgentCreditAfter=$rd['credit'] - $_POST['credit'];
									
							}else{
									
								//deduct assigned credit from main company
								$rd=TableMainCompany::model()->find(array('select'=>'credit,balance','condition'=>'company_id=:company_id','params'=>array(':company_id'=>1),));
								TableMainCompany::model()->updateAll(array('credit'=>$rd['credit'] - $_POST['credit'],'balance'=>$rd['balance'] - $_POST['credit']),'company_id=1');
									
							}
							//end credit limit
							$post=new TableAgentPlayer();
							$post->account_id=strtoupper($_POST['account_id']);
							$post->account_name=$_POST['account_name'];
							$post->agent_account_id=$_POST['agent_parent_id'];
							$post->password=CV999Utility::generateOneWayPassword($_POST['agent_password']);
							$post->currency_id=$_POST['currency_id'];
							$post->credit=$_POST['credit'];
							$post->balance=$_POST['credit'];
							$post->phone_number=$_POST['phone_number'];
							$post->email=$_POST['agent_email'];
							$post->status_id=1;
							$post->opening_acc_date=$dateTime;
							$post->casino_id=1;
							$post->bonus=0;
							//$post->balance=0;
							$post->max_win_multiple=$_POST['max_win_multiple'];
							$post->max_win=$_POST['max_win_daily'];
							$post->key_code=CV999Utility::generateKeyCode();

							//new field(game limits)
							if($_POST['selected_baccarat_limit']!=0){
								$post->limit_baccarat=$_POST['selected_baccarat_limit'];
							}else{
								$post->limit_baccarat=$_POST['baccarat'];
							}
							if($_POST['selected_roullete_limit']!=0){
								$post->limit_roulette=$_POST['selected_roullete_limit'];
							}else{
								$post->limit_roulette=$_POST['roullete'];
							}
							//$post->limit_roulette=$_POST['selected_roullete_limit'];
							if($_POST['selected_dragon_tiger_limit']!=0){
								$post->limit_dragon_tiger=$_POST['selected_dragon_tiger_limit'];
							}else{
								$post->limit_dragon_tiger=$_POST['dragontiger'];
							}
							//$post->limit_dragon_tiger=$_POST['selected_dragon_tiger_limit'];
							$post->limit_blackjack=$_POST['blackjack'];
							$post->limit_american_roulette=$_POST['american_roulette'];
							$post->limit_slots=$_POST['slot'];
							$post->commission_baccarat=$_POST['baccarat_commission'];
							$post->commission_roulette=$_POST['roullete_commission'];
							$post->commission_dragon_tiger=$_POST['dragontiger_commission'];
							$post->commission_blackjack=$_POST['blackjack_commission'];
							$post->commission_american_roulette=$_POST['american_roulette_commission'];
							$post->commission_slots=$_POST['slot_commission'];
							$post->casino_access_level=$casinoLevel;
							//get the white_list of the parent
							if(substr(strtoupper($_POST['account_id']),0,strlen(strtoupper($_POST['account_id']))-2)!=''){
								$agent_parent_white_list=TableAgent::model()->find(array('select'=>'white_list','condition'=>'account_id=:account_id',
										'params'=>array(':account_id'=>substr(strtoupper($_POST['account_id']),0,strlen(strtoupper($_POST['account_id']))-2)),));
								$post->white_list=$agent_parent_white_list['white_list'];
							}else{
								$post->white_list=0;
							}
							$post->market_type = $_POST['marketType'];
							$post->save();

							$credit_after=TableAgentPlayer::model()->find(array('select'=>'credit,balance',
									'condition'=>'account_id=:account_id','params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));

							$updt= new TableAgentPlayerTransactionHistory();
							$updt->transaction_number= TransactionNumber::generateTransactionNumber("AC");
							$updt->agent_account_id=strtoupper($_POST['account_id']);
							$updt->currency_id=$_POST['currency_id'];
							$updt->trans_type_id=7;
							$updt->amount=$_POST['credit'];
							$updt->balance_before=0;
							$updt->balance_after=$credit_after['balance'];
							$updt->credit_before=0;
							$updt->credit_after=$credit_after['credit'];
							$updt->upper_agent_balance_before=$upperAgentBalanceBefore;
							$updt->upper_agent_balance_after=$upperAgentBalanceAfter;
							$updt->upper_agent_credit_before=$upperAgentCreditBefore;
							$updt->upper_agent_credit_after=$upperAgentCreditAfter;
							$updt->commission_before='';
							$updt->commission_after=$_POST['baccarat_commission'].'^'.$_POST['roullete_commission'].'^'.$_POST['dragontiger_commission'].'^'.$_POST['blackjack_commission'].'^'.$_POST['american_roulette_commission'].'^'.$_POST['slot_commission'];
							$updt->transaction_date=$dateTime;
							$updt->operator_id=Yii::app()->session['account_id'];
							$updt->save();

							$postLog=new TableLog();
							$postLog->operated_by=$AccountID;
							$postLog->operated_by_level=$level;
							$postLog->operated=strtoupper($_POST['account_id']);
							$postLog->operated_level='Agent Player';
							$postLog->operation_time=$dateTime;
							$postLog->log_type_id=1;
							$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> create new account:<label style=\"color:green\">Agent Player</label> <label style=\"color:red\">'.strtoupper($_POST['account_id']).'</label>| Assigned credit:<label style=\"color:red\">'.$_POST['credit'].'</label></b>';
							$postLog->save();

							$postLog=new TableLog();
							$postLog->operated_by=$AccountID;
							$postLog->operated_by_level=$level;
							$postLog->operated=strtoupper($_POST['account_id']);
							$postLog->operated_level='Agent Player';
							$postLog->operation_time=$dateTime;
							$postLog->log_type_id=7; // 7 is change commission.
							$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ASSIGNED COMMISSION to <label style=\"color:green\">Agent Player</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> '.
									$this->newSharingCommissionLogDetails($_POST['baccarat_commission'], $_POST['roullete_commission'], $_POST['dragontiger_commission'],
											$_POST['blackjack_commission'], $_POST['american_roulette_commission'], $_POST['slot_commission']) .'</b>';
							$postLog->save();

							$submit_daily_max_win = array('username'=>strtoupper($_POST['account_id']), 'daily_max_win'=>$_POST['max_win_daily']);
							echo 1;
						}else{
							exit('credit_exceeded');
						}
					}
				}
				else
				{
					$connection = Yii::app()->db_cv999_fd_master;
					$command = $connection->createCommand("SELECT password,currency_id,credit,status_id from tbl_agent_player  WHERE account_id='" . strtoupper($_POST['account_id']) . "'");
					$rd=$command->queryRow();
					$password=$rd['password'];
					$currency=$rd['currency_id'];
					$credit=$rd['credit'];
					$status=$rd['status_id'];


					if($_POST['s_edit']==1){
						
						$bal=TableAgentPlayer::model()->find(array('select'=>'credit,balance,agent_account_id,commission_baccarat,commission_roulette,commission_dragon_tiger,commission_blackjack,commission_american_roulette,commission_slots',
								'condition'=>'account_id=:account_id',
								'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
						
						// if account exist, continue updates
						if(!$this->isAccountExistedInAgentPlayer(strtoupper($_POST['account_id']))){
							exit('account_doenst_exist');
						}
							
						// exit if player is on the lobby, continue if not
						if($_POST['credit'] != $bal['credit']){
							if($this->isPlayerOnTheLobby(strtoupper($_POST['account_id']))){
								exit('player_is_on_lobby');
							}
							
						}
						if(!$this->isPlayerOnTheLobby(strtoupper($_POST['account_id']))){
							$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer
									WHERE account_id='".$_POST['account_id']."' and deposit_withdrawal<>0");
							$rd = $command->queryRow();
							if ($rd['deposit_withdrawal'] == 1)
							{
								exit('player_is_transfering_balance');
							}
						}
						if($_POST['max_win_multiple'] >= -1){
							//$casinoLevel=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv','params'=>array(':htv'=>$_POST['htv999'],':sv'=>$_POST['savan999'],':vig'=>$_POST['costa999'],':vv'=>$_POST['virtua999']),));
							$casinoLevel_=TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv and sb=:sb and svs=:svs','params'=>array(':htv'=>$_POST['htv999'],':sv'=>$_POST['savan999'],':vig'=>$_POST['costa999'],':vv'=>$_POST['virtua999'],':sb'=>$_POST['sportsbook999'],':svs'=>$_POST['slotsvegas999'])));
							if ($casinoLevel_['level_id']==''){
								$postLevelSetting= new TableCasinoAccessLevelSetting;
								$postLevelSetting->htv = $_POST['htv999'];
								$postLevelSetting->sv  = $_POST['savan999'];
								$postLevelSetting->vig = $_POST['costa999'];
								$postLevelSetting->vv  = $_POST['virtua999'];
								$postLevelSetting->sb  = $_POST['sportsbook999'];
								$postLevelSetting->svs = $_POST['slotsvegas999'];
								$postLevelSetting->save();
									
							}
							$casinoLevel=$this->getCasinoLevelSettingId($_POST['htv999'],$_POST['savan999'],$_POST['costa999'],$_POST['virtua999'],$_POST['sportsbook999'],$_POST['slotsvegas999']);
							
							//$casinoLevel=$this->getCasinoLevelSettingId($_POST['htv999'],$_POST['savan999'],$_POST['costa999'],$_POST['virtua999'],$_POST['sportsbook999'],$_POST['slotsvegas999']);

							$max_win_multiple_before = $this->getFieldData($_POST['account_id'], 'max_win_multiple');
							$daily_max_win_before = $this->getFieldData($_POST['account_id'], 'max_win');


							

							$baccaratCommission=$bal['commission_baccarat'];
							$roulleteEURCommission=$bal['commission_roulette'];
							$dragonTigerCommission=$bal['commission_dragon_tiger'];
							$blackjackCommission=$bal['commission_blackjack'];
							$roulleteAMECommission=$bal['commission_american_roulette'];
							$slotCommission=$bal['commission_slots'];

							$rd_credit=TableAgent::model()->find(array('select'=>'credit','condition'=>'account_id=:account_id',
									'params'=>array(':account_id'=>$bal['agent_account_id']),));

							$fltCreditLimitFrom=0;
							$fltCreditLimitTo=($rd_credit['credit']+$bal['credit']);
							if(($bal['credit']-$bal['balance'])>=0){
								$fltCreditLimitFrom=($bal['credit']-$bal['balance']);
							}else{
								$fltCreditLimitFrom=0;
							}

							// credit limit validation
							if(($_POST['credit'] >= $fltCreditLimitFrom) && ($_POST['credit'] <= $fltCreditLimitTo)){

								$post=TableAgentPlayer::model()->find(array('select'=>'*','condition'=>'account_id=:account_id',
										'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));

								$post->account_name=$_POST['account_name'];
								if(trim($_POST['agent_password'])!=''){
									$post->password=CV999Utility::generateOneWayPassword($_POST['agent_password']);
								}
								$post->currency_id=$_POST['currency_id'];
								$post->credit=$_POST['credit'];
								$post->balance=$bal['balance'] + ($_POST['credit'] - $bal['credit']);
								$post->phone_number=$_POST['phone_number'];
								$post->email=$_POST['agent_email'];
								//$post->status_id=1;
								//$post->opening_acc_date=$dateTime;
								$post->max_win_multiple=$_POST['max_win_multiple'];
								//$post->max_win=$_POST['max_win_daily'];
								//new field(game limits)
								if($_POST['selected_baccarat_limit']!=0){
									$post->limit_baccarat=$_POST['selected_baccarat_limit'];
								}else{
									$post->limit_baccarat=$_POST['baccarat'];
								}
								if($_POST['selected_roullete_limit']!=0){
									$post->limit_roulette=$_POST['selected_roullete_limit'];
								}else{
									$post->limit_roulette=$_POST['roullete'];
								}
								//$post->limit_roulette=$_POST['selected_roullete_limit'];
								if($_POST['selected_dragon_tiger_limit']!=0){
									$post->limit_dragon_tiger=$_POST['selected_dragon_tiger_limit'];
								}else{
									$post->limit_dragon_tiger=$_POST['dragontiger'];
								}
								//$post->limit_dragon_tiger=$_POST['selected_dragon_tiger_limit'];
								$post->limit_blackjack=$_POST['blackjack'];
								$post->limit_american_roulette=$_POST['american_roulette'];
								$post->limit_slots=$_POST['slot'];
								$post->commission_baccarat=$_POST['baccarat_commission'];
								$post->commission_roulette=$_POST['roullete_commission'];
								$post->commission_dragon_tiger=$_POST['dragontiger_commission'];
								$post->commission_blackjack=$_POST['blackjack_commission'];
								$post->commission_american_roulette=$_POST['american_roulette_commission'];
								$post->commission_slots=$_POST['slot_commission'];
								$post->casino_access_level=$casinoLevel;
								$post->market_type = isset($_POST['marketType'])? $_POST['marketType'] : 1;
								$post->save();

								$credit_after=TableAgentPlayer::model()->find(array('select'=>'credit,balance',
										'condition'=>'account_id=:account_id',
										'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
									
								if($_POST['agent_type']!='SC'){
									$prnt_id=TableAgentPlayer::model()->find(array('select'=>'agent_account_id','condition'=>'account_id=:account_id',
											'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
									$rd=TableAgent::model()->find(array('select'=>'credit,balance','condition'=>'account_id=:account_id',
											'params'=>array(':account_id'=>$prnt_id['agent_account_id']),));
									$upperAgentBalanceBefore=$rd['balance'];
									$upperAgentBalanceAfter=$rd['balance'];
									$upperAgentCreditBefore=$rd['credit'];
									$upperAgentCreditAfter=$rd['credit'];

									if($credit_before_save >= 0){

										if($_POST['credit'] > $credit_before_save){
											TableAgent::model()->updateAll(array('credit'=>$rd['credit'] - ($_POST['credit']-$credit_before_save),
											'balance'=>$rd['balance'] - ($_POST['credit']-$credit_before_save)),
											'account_id="'.$prnt_id['agent_account_id'].'"');
											$upperAgentBalanceAfter=$rd['balance'] - ($_POST['credit']-$credit_before_save);
											$upperAgentCreditAfter=$rd['credit'] - ($_POST['credit']-$credit_before_save);
										}else{
											TableAgent::model()->updateAll(array('credit'=>$rd['credit'] + ($credit_before_save-$_POST['credit']),
											'balance'=>$rd['balance'] + ($credit_before_save-$_POST['credit'])),
											'account_id="'.$prnt_id['agent_account_id'].'"');
											$upperAgentBalanceAfter=$rd['balance'] + ($credit_before_save-$_POST['credit']);
											$upperAgentCreditAfter=$rd['credit'] + ($credit_before_save-$_POST['credit']);
										}

										$s_commisson_change = 0;
										if($this->isSharingCommissionChange($baccaratCommission, $_POST['baccarat_commission']) ||
										$this->isSharingCommissionChange($roulleteEURCommission, $_POST['roullete_commission']) ||
										$this->isSharingCommissionChange($dragonTigerCommission, $_POST['dragontiger_commission']) ||
										$this->isSharingCommissionChange($blackjackCommission, $_POST['blackjack_commission']) ||
										$this->isSharingCommissionChange($roulleteAMECommission, $_POST['american_roulette_commission']) ||
										$this->isSharingCommissionChange($slotCommission, $_POST['slot_commission'])){
												
											$s_commisson_change = 1;
										}
											
										// add to transaction history when credit and commission change
										if($bal['credit'] <> $credit_after['credit']){
												
											$updt= new TableAgentPlayerTransactionHistory();
											if($_POST['credit'] > $credit_before_save){

												$updt->transaction_number = TransactionNumber::generateTransactionNumber("AC");
												$updt->trans_type_id = 7;

											}else if($_POST['credit'] < $credit_before_save){

												$updt->transaction_number = TransactionNumber::generateTransactionNumber("RC");
												$updt->trans_type_id = 8;

											}
												
											$updt->amount = $_POST['credit']- $credit_before_save;
											$updt->agent_account_id = strtoupper($_POST['account_id']);
											$updt->currency_id = $_POST['currency_id'];
											$updt->balance_before = $bal['balance'];
											$updt->balance_after = $credit_after['balance'];
											$updt->credit_before = $credit_before_save;
											$updt->credit_after = $credit_after['credit'];
											$updt->commission_before = $baccaratCommission.'^'.$roulleteEURCommission.'^'.$dragonTigerCommission.'^'.$blackjackCommission.'^'.$roulleteAMECommission.'^'.$slotCommission;
											$updt->commission_after = $_POST['baccarat_commission'].'^'.$_POST['roullete_commission'].'^'.$_POST['dragontiger_commission'].'^'.$_POST['blackjack_commission'].'^'.$_POST['american_roulette_commission'].'^'.$_POST['slot_commission'];
											$updt->upper_agent_balance_before = $upperAgentBalanceBefore;
											$updt->upper_agent_balance_after = $upperAgentBalanceAfter;
											$updt->upper_agent_credit_before = $upperAgentCreditBefore;
											$updt->upper_agent_credit_after = $upperAgentCreditAfter;
											$updt->transaction_date = $dateTime;
											$updt->operator_id = Yii::app()->session['account_id'];
											$updt->save();
										}
											
									}
								}
									
									
									
								if($max_win_multiple_before != $_POST['max_win_multiple']){
									$postLog=new TableLog();
									$postLog->operated_by=$AccountID;
									$postLog->operated_by_level=$level;
									$postLog->operated=strtoupper($_POST['account_id']);
									$postLog->operated_level='Agent Player';
									$postLog->operation_time=$dateTime;
									$postLog->log_type_id=12;
									$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Max Balancefrom <label style=\"color:blue\">'. $max_win_multiple_before .'</label> to <label style=\"color:red\">'. $_POST['max_win_multiple'] .'</label></b>';
									$postLog->save();
								}
									
								if($daily_max_win_before != $_POST['max_win_daily']){
									// 										$postLog=new TableLog();
									// 										$postLog->operated_by=$AccountID;
									// 										$postLog->operated_by_level=$level;
									// 										$postLog->operated=strtoupper($_POST['account_id']);
									// 										$postLog->operated_level='Agent Player';
									// 										$postLog->operation_time=$dateTime;
									// 										$postLog->log_type_id=12;
									// 										$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Daily Max Win from <label style=\"color:blue\">'. $max_win_multiple_before .'</label> to <label style=\"color:red\">'. $_POST['max_win_multiple'] .'</label></b>';
									// 										$postLog->save();

									$submit_daily_max_win = array('username'=>strtoupper($_POST['account_id']), 'daily_max_win'=>$_POST['max_win_daily']);
								}else{
									$submit_daily_max_win = array('username'=>'', 'daily_max_win'=>0);
								}
									
								//insert changes to Log added by Allan/ Date: 2012-06-19
								if ($password != CV999Utility::generateOneWayPassword($_POST['agent_password']) && trim($_POST['agent_password']) !=''){
									$postLog=new TableLog();
									$postLog->operated_by=$AccountID;
									$postLog->operated_by_level=$level;
									$postLog->operated=strtoupper($_POST['account_id']);
									$postLog->operated_level='Agent Player';
									$postLog->operation_time=$dateTime;
									$postLog->log_type_id=10;
									$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE password:<label style=\"color:green\">Agent Player</label> <label style=\"color:red\">'.strtoupper($_POST['account_id']).'</label></b>';
									$postLog->save();
								}
								if ($credit != $_POST['credit']){
									$postLog=new TableLog();
									$postLog->operated_by=$AccountID;
									$postLog->operated_by_level=$level;
									$postLog->operated=strtoupper($_POST['account_id']);
									$postLog->operated_level='Agent Player';
									$postLog->operation_time=$dateTime;
									$postLog->log_type_id=5;
									$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE credit limit of <label style=\"color:green\">Agent Player</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> from <label style=\"color:red\">'.$credit.'</label> to <label style=\"color:red\">'.$_POST['credit'].'</label></b>';
									$postLog->save();
								}
									
								if ($s_commisson_change == 1){
									$postLog=new TableLog();
									$postLog->operated_by=$AccountID;
									$postLog->operated_by_level=$level;
									$postLog->operated=strtoupper($_POST['account_id']);
									$postLog->operated_level='Agent Player';
									$postLog->operation_time=$dateTime;
									$postLog->log_type_id=7;
									$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE COMMISSION of <label style=\"color:green\">Agent Player</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label> from '.
											$this->getSharingCommissionLogDetails($baccaratCommission . ':' . $_POST['baccarat_commission'],
													$roulleteEURCommission . ':' . $_POST['roullete_commission'],
													$dragonTigerCommission . ':' . $_POST['dragontiger_commission'],
													$blackjackCommission . ':' . $_POST['blackjack_commission'],
													$roulleteAMECommission . ':' . $_POST['american_roulette_commission'],
													$slotCommission . ':' . $_POST['slot_commission']) .'</b>';
									$postLog->save();
								}
									
								if($baccarat_limit_before != $_POST['selected_baccarat_limit'] || $roulette_limit_before != $_POST['selected_roullete_limit'] || $dragon_tiger_limit_before != $_POST['selected_dragon_tiger_limit']){
									$postLog=new TableLog();
									$postLog->operated_by=$AccountID;
									$postLog->operated_by_level=$level;
									$postLog->operated=strtoupper($_POST['account_id']);
									$postLog->operated_level=$label;
									$postLog->operation_time=$dateTime;
									$postLog->log_type_id=11;
									$postLog->log_details= '<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE TABLE LIMIT of <label style=\"color:green\">'.$label.'</label> <label style=\"color:#7A5C00\">'.strtoupper($_POST['account_id']).'</label>\'s '.
											$this->getTableLimitLogDetails($baccarat_limit_before . ':' . $_POST['selected_baccarat_limit'],
													$roulette_limit_before . ':' . $_POST['selected_roullete_limit'],
													$dragon_tiger_limit_before . ':' . $_POST['selected_dragon_tiger_limit']) . '</b>';
									$postLog->save();
								}

								echo 2;
							}else{
								exit('credit_exceeded');
							}
						}

					}
					else{
						exit('0');
					}
				}
			}
		}
			
		$transaction->commit(); // commit all changes
		/*
		 * @todo submit daily winloss
		* @author leokarl
		*/
		if($submit_daily_max_win['username'] != ''){
			$return_val = $winlosslimit->submitWinLossLimit(trim($submit_daily_max_win['username']), $this->getPlayerCurrencyName(trim($submit_daily_max_win['username'])), $submit_daily_max_win['daily_max_win']);
			if(isset($return_val['response']['status']) && $return_val['response']['status'] == 'OK'){
					
				$post=TableAgentPlayer::model()->find(array('select'=>'*','condition'=>'account_id=:account_id',
						'params'=>array(':account_id'=>strtoupper($_POST['account_id'])),));
				$post->max_win=$_POST['max_win_daily'];
				$post->save();
					
				$postLog=new TableLog();
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=strtoupper($_POST['account_id']);
				$postLog->operated_level='Agent Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=12;
				if(isset($daily_max_win_before)){
					$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE Daily Max Win from <label style=\"color:blue\">'. $daily_max_win_before .'</label> to <label style=\"color:red\">'. $_POST['max_win_daily'] .'</label></b>';
				}else{
					$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> ASSIGNED Daily Max Win <label style=\"color:blue\">'. $_POST['max_win_daily'] .'</label> to Agent Player <label style=\"color:green\"> '. strtoupper($_POST['account_id']) .'</label></b>';
				}
				$postLog->save();
			}else{
				//exit($return_val['response']['description']);
				exit('error_submit_win_loss_limit');
			}
		}
			

	}

	/**
	 * @todo get player's currency
	 * @author leokarl
	 * @since 2013-02-14
	 * @param string $account_id
	 */
	public function getPlayerCurrencyName($account_id){
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT b.currency_name FROM tbl_agent_player a INNER JOIN tbl_currency b ON b.id=a.currency_id WHERE a.account_id = '". $account_id ."'");
		$rows = $command->query();
		$currency = $rows->readAll();
		return $currency[0]['currency_name'];
	}

	/**
	 * @todo get table limit log details
	 * @author leokarl
	 * @since 2013-01-23
	 * @param string $baccarat_limit
	 * @param string $roulette_limit
	 * @param string $dragon_tiger_limit
	 */
	public function getTableLimitLogDetails($baccarat_limit, $roulette_limit, $dragon_tiger_limit){
		$log_details='';
		list($baccarat_before, $baccarat_after) = explode(":",$baccarat_limit);
		list($roulette_before, $roulette_after) = explode(":",$roulette_limit);
		list($dragon_tiger_before, $dragon_tiger_after) = explode(":",$dragon_tiger_limit);

		$baccarat_limit_before = split(",",substr_replace($baccarat_before,'',strlen($baccarat_before)-1,1));
		$baccarat_limit_after = split(",",substr_replace($baccarat_after,'',strlen($baccarat_after)-1,1));
		$roulette_limit_before = split(",",substr_replace($roulette_before,'',strlen($roulette_before)-1,1));
		$roulette_limit_after = split(",",substr_replace($roulette_after,'',strlen($roulette_after)-1,1));
		$dragon_tiger_limit_before = split(",",substr_replace($dragon_tiger_before,'',strlen($dragon_tiger_before)-1,1));
		$dragon_tiger_limit_after = split(",",substr_replace($dragon_tiger_after,'',strlen($dragon_tiger_after)-1,1));

		// baccarat
		if($baccarat_before != $baccarat_after){
			$baccarat_before_detail = '';
			$baccarat_after_detail = '';
			for($i=0; $i<count($baccarat_limit_before); $i++){
				$detail = $this->getBaccaratTableLimitDetails($baccarat_limit_before[$i]);
				if(isset($detail[0]['bac_all'])){
					$separator = ($i < count($baccarat_limit_before)-1) ? ', ' : '';
					$baccarat_before_detail .= '<br/>[All: ' . $detail[0]['bac_all'] . ', Banker Player: ' . $detail[0]['banker_player'] . ', Tie: ' . $detail[0]['tie'] .
					', Pair: ' . $detail[0]['pair'] . ', Big: ' . $detail[0]['big'] . ', Small: ' . $detail[0]['small'] . ']' . $separator;
				}
			}
				
			for($i=0; $i<count($baccarat_limit_after); $i++){
				$detail = $this->getBaccaratTableLimitDetails($baccarat_limit_after[$i]);
				$separator = ($i < count($baccarat_limit_after)-1) ? ', ' : '';
				$baccarat_after_detail .= '<br/>[All: ' . $detail[0]['bac_all'] . ', Banker Player: ' . $detail[0]['banker_player'] . ', Tie: ' . $detail[0]['tie'] .
				', Pair: ' . $detail[0]['pair'] . ', Big: ' . $detail[0]['big'] . ', Small: ' . $detail[0]['small'] . ']' . $separator;

			}
			$log_details .= '<font color=\"blue\">BACCARAT LIMIT from</font> '. $baccarat_before_detail .' <font color=\"red\"></br>to</font> '. $baccarat_after_detail .'.<br/>';
		}

		// europoean roulette
		if($roulette_before != $roulette_after){
			$roulette_before_detail = '';
			$roulette_after_detail = '';
			for($i=0; $i<count($roulette_limit_before); $i++){
				$detail = $this->getRouletteTableLimitDetails($roulette_limit_before[$i]);
				if(isset($detail[0]['roul_all'])){
					$separator = ($i < count($roulette_limit_before)-1) ? ', ' : '';
					$roulette_before_detail .= '<br/>[All: ' . $detail[0]['roul_all'] . ', 35 to 1: ' . $detail[0]['35_to_1'] . ', 17 to 1: ' . $detail[0]['18_to_1'] .
					', 11 to 1: ' . $detail[0]['12_to_1'] . ', 8 to 1: ' . $detail[0]['9_to_1'] . ', 5 to 1: ' . $detail[0]['6_to_1'] . ', 2 to 1' . $detail[0]['2_to_1'] .
					', 1 to 1' . $detail[0]['1_to_1'] . ']' . $separator;
				}
			}

			for($i=0; $i<count($roulette_limit_after); $i++){
				$detail = $this->getRouletteTableLimitDetails($roulette_limit_after[$i]);
				$separator = ($i < count($roulette_limit_after)-1) ? ', ' : '';
				$roulette_after_detail .= '<br/>[All: ' . $detail[0]['roul_all'] . ', 35 to 1: ' . $detail[0]['35_to_1'] . ', 17 to 1: ' . $detail[0]['18_to_1'] .
				', 11 to 1: ' . $detail[0]['12_to_1'] . ', 8 to 1: ' . $detail[0]['9_to_1'] . ', 5 to 1: ' . $detail[0]['6_to_1'] . ', 2 to 1' . $detail[0]['2_to_1'] .
				', 1 to 1' . $detail[0]['1_to_1'] . ']' . $separator;
			}
			$log_details .= '<font color=\"blue\">EUROPEAN ROULETTE LIMIT from</font> '. $roulette_before_detail .' <font color=\"red\"></br>to</font> '. $roulette_after_detail .'.<br/>';
		}

		// dragon tiger
		if($dragon_tiger_before != $dragon_tiger_after){
			$dragon_tiger_before_detail = '';
			$dragon_tiger_after_detail = '';
			for($i=0; $i<count($dragon_tiger_limit_before); $i++){
				$detail = $this->getDragonTigerTableLimitDetails($dragon_tiger_limit_before[$i]);
				if(isset($detail[0]['dra_all'])){
					$separator = ($i < count($dragon_tiger_limit_before)-1) ? ', ' : '';
					$dragon_tiger_before_detail .= '<br/>[All: ' . $detail[0]['dra_all'] . ', Dragon Tiger: ' . $detail[0]['dragon_tiger'] . ', Tie: ' . $detail[0]['tie'] . ']' . $separator;
				}
			}

			for($i=0; $i<count($dragon_tiger_limit_after); $i++){
				$detail = $this->getDragonTigerTableLimitDetails($dragon_tiger_limit_after[$i]);
				$separator = ($i < count($dragon_tiger_limit_after)-1) ? ', ' : '';
				$dragon_tiger_after_detail .= '<br/>[All: ' . $detail[0]['dra_all'] . ', Dragon Tiger: ' . $detail[0]['dragon_tiger'] . ', Tie: ' . $detail[0]['tie'] . ']' . $separator;
			}
			$log_details .= '<font color=\"blue\">DRAGON TIGER LIMIT from</font> '. $dragon_tiger_before_detail .' <font color=\"red\"></br>to</font> '. $dragon_tiger_after_detail .'.<br/>';
		}

		return $log_details;
	}

	/**
	 * @todo get baccarat table limit details
	 * @author leokarl
	 * @since 2013-01-23
	 * @param int $limit_id
	 */
	public function getBaccaratTableLimitDetails($limit_id){
		if(trim($limit_id) != ''){
			$limit_id = preg_replace("/[^0-9]/", '', $limit_id);
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwBacarratTableLimit WHERE id = " . $limit_id);
			$rows = $command->query();
			return $rows->readAll();
		}
	}

	/**
	 * @todo get european roulette table limit details
	 * @author leokarl
	 * @since 2013-01-23
	 * @param int $limit_id
	 */
	public function getRouletteTableLimitDetails($limit_id){
		if(trim($limit_id) != ''){
			$limit_id = preg_replace("/[^0-9]/", '', $limit_id);
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwEuropeanRouletteTableLimit WHERE id = " . $limit_id);
			$rows = $command->query();
			return $rows->readAll();
		}
	}

	/**
	 * @todo get dragon tiger table limit details
	 * @author leokarl
	 * @since 2013-01-23
	 * @param int $limit_id
	 */
	public function getDragonTigerTableLimitDetails($limit_id){
		if(trim($limit_id) != ''){
			$limit_id = preg_replace("/[^0-9]/", '', $limit_id);
			$connection = Yii::app()->db_cv999_fd_master;
			$command = $connection->createCommand("SELECT * FROM vwDragontigerTableLimit WHERE id = " . $limit_id);
			$rows = $command->query();
			return $rows->readAll();
		}
	}

	/**
	 * @todo compare agent player's commission
	 * @author leokarl
	 * @since 2012-12-19
	 * @param string $before
	 * @param string $after
	 * @return boolean
	 */
	public function isSharingCommissionChange($before, $after){
		// ex. 0.00#90.00#30.00#0.00
//		$change_count = 0;
//		list($costa_before,$htv_before,$sv_before,$virtua_before,$slots_before) = explode("#", $before);
//		list($costa_after,$htv_after,$sv_after,$virtua_after,$slots_after) = explode("#", $after);
//
//		// compare costa commission
//		if($costa_before <> $costa_after){
//			$change_count +=1;
//		}
//
//		// compare hatien commission
//		if($htv_before <> $htv_after){
//			$change_count +=1;
//		}
//
//		// compare savan commission
//		if($sv_before <> $sv_after){
//			$change_count +=1;
//		}
//
//		// compare virtua commission
//		if($virtua_before <> $virtua_after){
//			$change_count +=1;
//		}
//		// compare virtua commission
//		if($slots_before <> $slots_after){
//			$change_count +=1;
//		}

		return (1 > 0) ? TRUE : FALSE;
	}

	/**
	 * @todo new sharing and commission log details
	 * @author leokarl
	 * @since 2013-01-15
	 * @param string $baccarat
	 * @param string $roulette
	 * @param string $dragon_tiger
	 * @param string $blackjack
	 * @param string $american_roulette
	 * @param string $slots
	 */
	public function newSharingCommissionLogDetails($baccarat, $roulette, $dragon_tiger, $blackjack, $american_roulette, $slots){
		$log_details='';

		// Baccarat
		list($costa_baccarat, $htv_baccarat, $sv_baccarat, $virtua_baccarat) = explode("#", $baccarat);

		//// CostaVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value: <font color=\"red\">' . $costa_baccarat . '</font>';


		//// HatienVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value: <font color=\"red\">' . $htv_baccarat . '</font>';


		//// SavanVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value: <font color=\"red\">' . $sv_baccarat . '</font>';

		//// VirtuaVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value: <font color=\"red\">' . $virtua_baccarat . '</font>';


		// Roulette
		list($costa_roulette, $htv_roulette, $sv_roulette, $virtua_roulette) = explode("#", $roulette);

		//// CostaVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value: <font color=\"red\">' . $costa_roulette . '</font>';

		//// HatienVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value: <font color=\"red\">' . $htv_roulette . '</font>';

		//// SavanVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value: <font color=\"red\">' . $sv_roulette . '</font>';

		//// VirtuaVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value: <font color=\"red\">' . $virtua_roulette . '</font>';


		// Dragon Tiger
		list($costa_dragon_tiger, $htv_dragon_tiger, $sv_dragon_tiger, $virtua_dragon_tiger) = explode("#", $dragon_tiger);

		//// CostaVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value: <font color=\"red\">' . $costa_dragon_tiger . '</font>';

		//// HatienVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value: <font color=\"red\">' . $htv_dragon_tiger . '</font>';

		//// SavanVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value: <font color=\"red\">' . $sv_dragon_tiger . '</font>';

		//// VirtuaVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value: <font color=\"red\">' . $virtua_dragon_tiger . '</font>';

		// Blackjack
		list($costa_blackjack, $htv_blackjack, $sv_blackjack, $virtua_blackjack) = explode("#", $blackjack);

		//// CostaVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value: <font color=\"red\">' . $costa_blackjack . '</font>';

		//// HatienVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value: <font color=\"red\">' . $htv_blackjack . '</font>';


		//// SavanVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value: <font color=\"red\">' . $sv_blackjack . '</font>';

		//// VirtuaVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value: <font color=\"red\">' . $virtua_blackjack . '</font>';

		// American Roulette
		list($costa_american_roulette, $htv_american_roulette, $sv_american_roulette, $virtua_american_roulette) = explode("#", $american_roulette);

		//// CostaVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value: <font color=\"red\">' . $costa_american_roulette . '</font>';

		//// HatienVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value: <font color=\"red\">' . $htv_american_roulette . '</font>';

		//// SavanVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value: <font color=\"red\">' . $sv_american_roulette . '</font>';

		//// VirtuaVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value: <font color=\"red\">' . $virtua_american_roulette . '</font>';

		// Slots
		list($costa_slots, $htv_slots, $sv_slots, $virtua_slots) = explode("#", $slots);

		//// CostaVegas999
		//$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value: <font color=\"red\">' . $costa_slots . '</font>';

		//// HatienVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value: <font color=\"red\">' . $htv_slots . '</font>';

		//// SavanVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value: <font color=\"red\">' . $sv_slots . '</font>';

		//// VirtuaVegas999
		$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value: <font color=\"red\">' . $virtua_slots . '</font>';


		return $log_details;
	}

	/**
	 * @todo get sharing and commission log details
	 * @author leokarl
	 * @since 2013-01-15
	 * @param string $baccarat
	 * @param string $roulette
	 * @param string $dragon_tiger
	 * @param string $blackjack
	 * @param string $american_roulette
	 * @param string $slots
	 */
	public function getSharingCommissionLogDetails($baccarat, $roulette, $dragon_tiger, $blackjack, $american_roulette, $slots){
		$log_details='';

		// Baccarat
		list($baccarat_before, $baccarat_after) = explode(":",$baccarat);
		list($costa_baccarat_before, $htv_baccarat_before, $sv_baccarat_before, $virtua_baccarat_before) = explode("#", $baccarat_before);
		list($costa_baccarat_after,$htv_baccarat_after, $sv_baccarat_after, $virtua_baccarat_after) = explode("#", $baccarat_after);

		//// CostaVegas999
		if($costa_baccarat_before <> $costa_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . $costa_baccarat_before . '</font> to <font color=\"red\">' . $costa_baccarat_after . '</font>';
		}

		//// HatienVegas999
		if($htv_baccarat_before <> $htv_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . $htv_baccarat_before . '</font> to <font color=\"red\">' . $htv_baccarat_after . '</font>';
		}

		//// SavanVegas999
		if($sv_baccarat_before <> $sv_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . $sv_baccarat_before . '</font> to <font color=\"red\">' . $sv_baccarat_after . '</font>';
		}

		//// VirtuaVegas999
		if($virtua_baccarat_before <> $virtua_baccarat_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Baccarat</font>; Value from <font color=\"blue\">' . $virtua_baccarat_before . '</font> to <font color=\"red\">' . $virtua_baccarat_after . '</font>';
		}

		// Roulette
		list($roulette_before, $roulette_after) = explode(":",$roulette);
		list($costa_roulette_before, $htv_roulette_before, $sv_roulette_before, $virtua_roulette_before) = explode("#", $roulette_before);
		list($costa_roulette_after, $htv_roulette_after, $sv_roulette_after, $virtua_roulette_after) = explode("#", $roulette_after);

		//// CostaVegas999
		if($costa_roulette_before <> $costa_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . $costa_roulette_before . '</font> to <font color=\"red\">' . $costa_roulette_after . '</font>';
		}

		//// HatienVegas999
		if($htv_roulette_before <> $htv_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . $htv_roulette_before . '</font> to <font color=\"red\">' . $htv_roulette_after . '</font>';
		}

		//// SavanVegas999
		if($sv_roulette_before <> $sv_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . $sv_roulette_before . '</font> to <font color=\"red\">' . $sv_roulette_after . '</font>';
		}

		//// VirtuaVegas999
		if($virtua_roulette_before <> $virtua_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Roulette</font>; Value from <font color=\"blue\">' . $virtua_roulette_before . '</font> to <font color=\"red\">' . $virtua_roulette_after . '</font>';
		}

		// Dragon Tiger
		list($dragon_tiger_before, $dragon_tiger_after) = explode(":",$dragon_tiger);
		list($costa_dragon_tiger_before, $htv_dragon_tiger_before, $sv_dragon_tiger_before, $virtua_dragon_tiger_before) = explode("#", $dragon_tiger_before);
		list($costa_dragon_tiger_after, $htv_dragon_tiger_after, $sv_dragon_tiger_after, $virtua_dragon_tiger_after) = explode("#", $dragon_tiger_after);

		//// CostaVegas999
		if($costa_dragon_tiger_before <> $costa_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . $costa_dragon_tiger_before . '</font> to <font color=\"red\">' . $costa_dragon_tiger_after . '</font>';
		}

		//// HatienVegas999
		if($htv_dragon_tiger_before <> $htv_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . $htv_dragon_tiger_before . '</font> to <font color=\"red\">' . $htv_dragon_tiger_after . '</font>';
		}

		//// SavanVegas999
		if($sv_dragon_tiger_before <> $sv_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . $sv_dragon_tiger_before . '</font> to <font color=\"red\">' . $sv_dragon_tiger_after . '</font>';
		}

		//// VirtuaVegas999
		if($virtua_dragon_tiger_before <> $virtua_dragon_tiger_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Dragon Tiger</font>; Value from <font color=\"blue\">' . $virtua_dragon_tiger_before . '</font> to <font color=\"red\">' . $virtua_dragon_tiger_after . '</font>';
		}

		// Blackjak
		list($blackjack_before, $blackjack_after) = explode(":",$blackjack);
		list($costa_blackjack_before, $htv_blackjack_before, $sv_blackjack_before, $virtua_blackjack_before) = explode("#", $blackjack_before);
		list($costa_blackjack_after, $htv_blackjack_after, $sv_blackjack_after, $virtua_blackjack_after) = explode("#", $blackjack_after);

		//// CostaVegas999
		if($costa_blackjack_before <> $costa_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . $costa_blackjack_before . '</font> to <font color=\"red\">' . $costa_blackjack_after . '</font>';
		}

		//// HatienVegas999
		if($htv_blackjack_before <> $htv_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . $htv_blackjack_before . '</font> to <font color=\"red\">' . $htv_blackjack_after . '</font>';
		}

		//// SavanVegas999
		if($sv_blackjack_before <> $sv_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . $sv_blackjack_before . '</font> to <font color=\"red\">' . $sv_blackjack_after . '</font>';
		}

		//// VirtuaVegas999
		if($virtua_blackjack_before <> $virtua_blackjack_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Blackjack</font>; Value from <font color=\"blue\">' . $virtua_blackjack_before . '</font> to <font color=\"red\">' . $virtua_blackjack_after . '</font>';
		}

		// American Roulette
		list($american_roulette_before, $american_roulette_after) = explode(":",$american_roulette);
		list($costa_american_roulette_before, $htv_american_roulette_before, $sv_american_roulette_before, $virtua_american_roulette_before) = explode("#", $american_roulette_before);
		list($costa_american_roulette_after, $htv_american_roulette_after, $sv_american_roulette_after, $virtua_american_roulette_after) = explode("#", $american_roulette_after);

		//// CostaVegas999
		if($costa_american_roulette_before <> $costa_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . $costa_american_roulette_before . '</font> to <font color=\"red\">' . $costa_american_roulette_after . '</font>';
		}

		//// HatienVegas999
		if($htv_american_roulette_before <> $htv_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . $htv_american_roulette_before . '</font> to <font color=\"red\">' . $htv_american_roulette_after . '</font>';
		}

		//// SavanVegas999
		if($sv_american_roulette_before <> $sv_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . $sv_american_roulette_before . '</font> to <font color=\"red\">' . $sv_american_roulette_after . '</font>';
		}

		//// VirtuaVegas999
		if($virtua_american_roulette_before <> $virtua_american_roulette_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">American Roulette</font>; Value from <font color=\"blue\">' . $virtua_american_roulette_before . '</font> to <font color=\"red\">' . $virtua_american_roulette_after . '</font>';
		}

		// Slots
		list($slots_before, $slots_after) = explode(":",$slots);
		list($costa_slots_before, $htv_slots_before, $sv_slots_before, $virtua_slots_before) = explode("#", $slots_before);
		list($costa_slots_after, $htv_slots_after, $sv_slots_after, $virtua_slots_after) = explode("#", $slots_after);

		//// CostaVegas999
		if($costa_slots_before <> $costa_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">CostaVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . $costa_slots_before . '</font> to <font color=\"red\">' . $costa_slots_after . '</font>';
		}

		//// HatienVegas999
		if($htv_slots_before <> $htv_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">HatienVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . $htv_slots_before . '</font> to <font color=\"red\">' . $htv_slots_after . '</font>';
		}

		//// SavanVegas999
		if($sv_slots_before <> $sv_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">SavanVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . $sv_slots_before . '</font> to <font color=\"red\">' . $sv_slots_after . '</font>';
		}

		//// VirtuaVegas999
		if($virtua_slots_before <> $virtua_slots_after){
			$log_details .= '<br/>Casino: <font color=\"blue\">VirtuaVegas999</font>; Game: <font color=\"blue\">Slots</font>; Value from <font color=\"blue\">' . $virtua_slots_before . '</font> to <font color=\"red\">' . $virtua_slots_after . '</font>';
		}


		return $log_details;
	}

	/**
	 * @todo getAgentAssignedCredit
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-05-10
	 */
	public function getAgentAssignedCredit()
	{
		$rd=TableAgent::model()->find(array('select'=>'credit,credit_assigned','condition'=>'account_id=:account_id',
				'params'=>array(':account_id'=>$_GET['account_id']),));
		//echo $rd['credit_assigned'];
		echo $rd['credit'];
	}


	/**
	 * @todo compare player_win_max to balance
	 * @author
	 * @since 2012-05-10
	 */
	public function comparePlayerWinMaxToBalance()
	{
		$rd = TableAgent::model()->find(array('select'=>'balance','condition'=>'account_id=:account_id',
				'params'=>array(':account_id'=>substr($_GET['agent_parent_id'],0,8)),));
		echo $rd['balance'];
	}



	/**
	 * @todo disallow special characters
	 * @author leokarl
	 * @since 2012-12-08
	 * @param string $mystring
	 */
	public function isSpecialCharPresent($mystring){
		if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
		&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
		&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false && strpos($mystring, '_') === false
		&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '-') === false && strpos($mystring, '`') === false
		&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
		&& strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
		&& strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){

			return FALSE; // special character not present
		}else{
			return TRUE; // special character present
		}
	}

	/**
	 * @todo return casino_level_setting_id
	 * @author leokarl
	 * @since 2012-12-08
	 * @param int $htv
	 * @param int $sv
	 * @param int $vig
	 * @param int $vv
	 */
	public function getCasinoLevelSettingId($htv,$sv,$vig,$vv,$sb,$svs){
		$model = TableCasinoAccessLevelSetting::model()->find(array('select'=>'level_id','condition'=>'htv=:htv and sv=:sv and vig=:vig and vv=:vv and sb=:sb and svs=:svs',
				'params'=>array(':htv'=>$htv,':sv'=>$sv,':vig'=>$vig,':vv'=>$vv,':sb'=>$sb,':svs'=>$svs),));
		return $model['level_id'];
	}

	/**
	 * @todo return casino level settings by casino_access_level => array
	 * @author leokarl
	 * @date 2012-12-10
	 * @param int $casino_access_level
	 */
	public function getCasinoLevelSettingsByCasinoAccessLevel($casino_access_level){
		$casino_level_settings = TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs','condition'=>'level_id=:level_id',
				'params'=>array(':level_id'=>$casino_access_level),));
		return $casino_level_settings;
	}

	/**
	 * @todo return casino level settings by agent's account_id => array
	 * @author leokarl
	 * @date 2012-12-10
	 * @param string $account_id
	 */
	public function getCasinoLevelSettingsByAccountId($account_id){
		$agent = TableAgent::model()->find(array('select'=>'casino_access_level','condition'=>'account_id=:account_id',
				'params'=>array(':account_id'=>$account_id),));
		// return array
		return $this->getCasinoLevelSettingsByCasinoAccessLevel($agent['casino_access_level']);
	}

	/**
	 * @todo sharing and commission validation
	 * @author leokarl
	 * @since 2012-12-10
	 * @return true if sharing(s) or commission(s) is valid, false if not
	 * @param string $baccarat_sharing
	 * @param string $roullete_sharing
	 * @param string $dragontiger_sharing
	 * @param string $blackjack_sharing
	 * @param string $american_roulette_sharing
	 * @param string $slot_sharing
	 * @param string $baccarat_commission
	 * @param string $roullete_commission
	 * @param string $dragontiger_commission
	 * @param string $blackjack_commission
	 * @param string $american_roulette_commission
	 * @param string $slot_commission
	 */
	public function isSharingCommissionValid($baccarat_sharing, $roullete_sharing, $dragontiger_sharing, $blackjack_sharing, $american_roulette_sharing, $slot_sharing,
			$baccarat_commission, $roullete_commission, $dragontiger_commission, $blackjack_commission, $american_roulette_commission, $slot_commission){
		// initialize
		$no_of_char=4;

		// column number check
		if(substr_count($baccarat_sharing,"#")!=$no_of_char || substr_count($roullete_sharing,"#")!=$no_of_char || substr_count($dragontiger_sharing,"#")!=$no_of_char
		|| substr_count($blackjack_sharing,"#")!=$no_of_char || substr_count($american_roulette_sharing,"#")!=$no_of_char || substr_count($slot_sharing,"#")!=$no_of_char
		|| substr_count($baccarat_commission,"#")!=$no_of_char || substr_count($roullete_commission,"#")!=$no_of_char || substr_count($dragontiger_commission,"#")!=$no_of_char
		|| substr_count($blackjack_commission,"#")!=$no_of_char || substr_count($american_roulette_commission,"#")!=$no_of_char || substr_count($slot_commission,"#")!=$no_of_char)
		{
			return FALSE;
		}
		// is sharing and commission numeric
		else
		{
			// initialize
			$baccarat_sharing=split("#",$baccarat_sharing);
			$roullete_sharing=split("#",$roullete_sharing);
			$dragontiger_sharing=split("#",$dragontiger_sharing);
			$blackjack_sharing=split("#",$blackjack_sharing);
			$american_roulette_sharing=split("#",$american_roulette_sharing);
			$slot_sharing=split("#",$slot_sharing);
			$baccarat_commission=split("#",$baccarat_commission);
			$roullete_commission=split("#",$roullete_commission);
			$dragontiger_commission=split("#",$dragontiger_commission);
			$blackjack_commission=split("#",$blackjack_commission);
			$american_roulette_commission=split("#",$american_roulette_commission);
			$slot_commission=split("#",$slot_commission);

			$s_not_numeric_counter=0;

			// add 1 to $s_not_numeric_counter if sharing or commission is not numeric

			for ($i = 0; $i <=($no_of_char-1); $i++) {
				if(!is_numeric($baccarat_sharing[$i]) || !is_numeric($roullete_sharing[$i]) || !is_numeric($dragontiger_sharing[$i]) || !is_numeric($blackjack_sharing[$i])
				|| !is_numeric($american_roulette_sharing[$i]) || !is_numeric($slot_sharing[$i]) || !is_numeric($baccarat_commission[$i]) || !is_numeric($roullete_commission[$i])
				|| !is_numeric($dragontiger_commission[$i]) || !is_numeric($blackjack_commission[$i]) || !is_numeric($american_roulette_commission[$i]) || !is_numeric($slot_commission[$i]))
				{
					$s_not_numeric_counter+=1;
				}
			}

			// return false if $s_not_numeric_counter > 0
			return ($s_not_numeric_counter == 0) ? TRUE : FALSE;
		}
	}


	/**
	 * @todo get agent level name by string length of account_id
	 * @author leokarl (from allan's code)
	 * @date 2012-12-10
	 * @param string $account_id
	 */
	public function getAgentLevel($account_id){
		if (strlen($account_id)==2){
			return 'Sub Company';
		}else if (strlen($account_id)==4){
			return 'Senior Master';
		}else if (strlen($account_id)==6){
			return 'Master Agent';
		}else if (strlen($account_id)==8){
			return 'Agent';
		}else if (strlen($account_id)==10){
			return 'Player';
		}
	}

	/**
	 * @todo update child agent's sharing and commission to zero
	 * @author leokarl
	 * @date 2012-12-10
	 * @param int $casino_id
	 * @param string $field_name
	 * @param string $account_id
	 */
	public function updateChildAgentSharingCommissionToZero($casino_id,$field_name,$account_id){
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;

		/*
		 * $casino_id definition
		* 1 = HatienVegas999
		* 2 = SavanVegas999
		* 3 = CostaVegas999
		* 4 = VirtuaVegas999
		*/

		if($casino_id == 1){ // HatienVegas999
				
			// sharing
			$command = $connection->createCommand("UPDATE tbl_agent SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,
					locate('#'," . $field_name . ")),'0',SUBSTRING(" . $field_name . ",locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1),
					length(" . $field_name . "))) WHERE account_id like '" . $account_id . "%'"
			);
			$command->execute();
				
			// commission
			if(strpos($field_name, 'commission') !== false){ // agent player has no sharing
				$command1 = $connection->createCommand("UPDATE tbl_agent_player SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,
						locate('#'," . $field_name . ")),'0',SUBSTRING(" . $field_name . ",locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1),
						length(" . $field_name . "))) WHERE account_id like '" . $account_id . "%'");
				$command1->execute();
			}
				
				
		}else if($casino_id == 2){ // SavanVegas999
				
			// sharing
			$command = $connection->createCommand("UPDATE tbl_agent SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,
					locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)),'0',SUBSTRING(" . $field_name . ",locate('#'," . $field_name . ",
					locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)+1),length(" . $field_name . ")))
					WHERE account_id like '" . $account_id . "%'"
			);
			$command->execute();
				
			// commission
			if(strpos($field_name, 'commission') !== false){ // agent player has no sharing
				$command1 = $connection->createCommand("UPDATE tbl_agent_player SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,
						locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)),'0',SUBSTRING(" . $field_name . ",locate('#'," . $field_name . ",
						locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)+1),length(" . $field_name . ")))
						WHERE account_id like '" . $account_id . "%'");
				$command1->execute();
			}
// 			// sharing
// 			$command = $connection->createCommand("UPDATE tbl_agent SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,
// 					locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)),'0',SUBSTRING(" . $field_name . ",locate('#'," . $field_name . ",
// 					locate('#'," . $field_name . ")+1),length(" . $field_name . ")))
// 					WHERE account_id like '" . $account_id . "%'"
// 			);
// 			$command->execute();
			
// 			// commission
// 			if(strpos($field_name, 'commission') !== false){ // agent player has no sharing
// 				$command1 = $connection->createCommand("UPDATE tbl_agent_player SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,
// 						locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)),'0',SUBSTRING(" . $field_name . ",locate('#'," . $field_name . ",
// 						locate('#'," . $field_name . ")+1),length(" . $field_name . ")))
// 						WHERE account_id like '" . $account_id . "%'");
// 				$command1->execute();
// 			}
				
				
		}else if($casino_id == 3){ // CostaVegas999
				
			// sharing
			$command = $connection->createCommand("UPDATE tbl_agent SET " . $field_name . "=CONCAT('0',SUBSTRING(" . $field_name . ",
					locate('#'," . $field_name . "),length(" . $field_name . "))) WHERE account_id like '" . $account_id . "%'"
			);
			$command->execute();
				
			// commission
			if(strpos($field_name, 'commission') !== false){ // agent player has no sharing
				$command1 = $connection->createCommand("UPDATE tbl_agent_player SET " . $field_name . "=CONCAT('0',SUBSTRING(" . $field_name . ",
						locate('#'," . $field_name . "),length(" . $field_name . "))) WHERE account_id like '" . $account_id . "%'");
				$command1->execute();
			}
				
// 		}else if($casino_id == 4){ // VirtuaVegas999
				
// 			// sharing
// 			$command = $connection->createCommand("UPDATE tbl_agent SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,locate('#'," . $field_name . ",
// 					locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)+1)),'0') WHERE account_id like '" . $account_id . "%'"
// 			);
// 			$command->execute();
				
// 			// commission
// 			if(strpos($field_name, 'commission') !== false){ // agent player has no sharing
// 				$command1 = $connection->createCommand("UPDATE tbl_agent_player SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,locate('#'," . $field_name . ",
// 						locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)+1)),'0') WHERE account_id like '" . $account_id . "%'");
// 				$command1->execute();
// 			}
		}else if($casino_id == 6){ //SlotsVegas999
		// sharing
			$command = $connection->createCommand("UPDATE tbl_agent SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,locate('#'," . $field_name . ",locate('#'," . $field_name . ",
					locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)+1)+1)),'0') WHERE account_id like '" . $account_id . "%'"
			);
			$command->execute();
				
			// commission
			if(strpos($field_name, 'commission') !== false){ // agent player has no sharing
				$command1 = $connection->createCommand("UPDATE tbl_agent_player SET " . $field_name . "=CONCAT(SUBSTRING(" . $field_name . ",1,locate('#'," . $field_name . ",locate('#'," . $field_name . ",
						locate('#'," . $field_name . ",locate('#'," . $field_name . ")+1)+1)+1)),'0') WHERE account_id like '" . $account_id . "%'");
				$command1->execute();
			}
		}
	}


	/**
	 * @todo check agent's sharing value. If agent unselect casino, agent's sharing
	 * including his/her lower agents and players will be 0.
	 * @author leokarl
	 * @param string $account_id
	 * @param string $baccarat
	 * @param string $roullete
	 * @param string $dragontiger
	 * @param string $blackjack
	 * @param string $american_roulette
	 * @param string $slot
	 */
	public function upperAgentSharingWithZeroValue($account_id,$baccarat,$roullete,$dragontiger,$blackjack,$american_roulette,$slot){
		// initialize
		$baccarat_sharing_val=split('#',$baccarat);
		$roullete_sharing_val=split('#',$roullete);
		$dragontiger_sharing_val=split('#',$dragontiger);
		$blackjack_sharing_val=split('#',$blackjack);
		$american_roulette_sharing_val=split('#',$american_roulette);
		$slot_sharing_val=split('#',$slot);

		//update sharing for costavegas
		if($baccarat_sharing_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'share_baccarat', $account_id);
		}
		if($roullete_sharing_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'share_roulette', $account_id);
		}
		if($dragontiger_sharing_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'share_dragon_tiger', $account_id);
		}
		if($blackjack_sharing_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'share_blackjack', $account_id);
		}
		if($american_roulette_sharing_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'share_american_roulette', $account_id);
		}
		if($slot_sharing_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'share_slots', $account_id);
		}

		//update sharing for hatienvegas
		if($baccarat_sharing_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'share_baccarat', $account_id);
		}
		if($roullete_sharing_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'share_roulette', $account_id);
		}
		if($dragontiger_sharing_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'share_dragon_tiger', $account_id);
		}
		if($blackjack_sharing_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'share_blackjack', $account_id);
		}
		if($american_roulette_sharing_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'share_american_roulette', $account_id);
		}
		if($slot_sharing_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'share_slots', $account_id);
		}

		//update sharing for savanvegas
		if($baccarat_sharing_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'share_baccarat', $account_id);
		}
		if($roullete_sharing_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'share_roulette', $account_id);
		}
		if($dragontiger_sharing_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'share_dragon_tiger', $account_id);
		}
		if($blackjack_sharing_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'share_blackjack', $account_id);
		}
		if($american_roulette_sharing_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'share_american_roulette', $account_id);
		}
		if($slot_sharing_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'share_slots', $account_id);
		}

		//update sharing for virtuavegas
		if($baccarat_sharing_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'share_baccarat', $account_id);
		}
		if($roullete_sharing_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'share_roulette', $account_id);
		}
		if($dragontiger_sharing_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'share_dragon_tiger', $account_id);
		}
		if($blackjack_sharing_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'share_blackjack', $account_id);
		}
		if($american_roulette_sharing_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'share_american_roulette', $account_id);
		}
		if($slot_sharing_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'share_slots', $account_id);
		}
		//update sharing for slotsvegas
		if($baccarat_sharing_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'share_baccarat', $account_id);
		}
		if($roullete_sharing_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'share_roulette', $account_id);
		}
		if($dragontiger_sharing_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'share_dragon_tiger', $account_id);
		}
		if($blackjack_sharing_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'share_blackjack', $account_id);
		}
		if($american_roulette_sharing_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'share_american_roulette', $account_id);
		}
		if($slot_sharing_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'share_slots', $account_id);
		}
	}

	/**
	 * @todo check agent's commission value. If agent unselect casino, agent's commission
	 * including his/her lower agents and players will be 0.
	 * @author leokarl
	 * @param string $account_id
	 * @param string $baccarat
	 * @param string $roullete
	 * @param string $dragontiger
	 * @param string $blackjack
	 * @param string $american_roulette
	 * @param string $slot
	 */
	public function upperAgentCommissionWithZeroValue($account_id,$baccarat,$roullete,$dragontiger,$blackjack,$american_roulette,$slot){
		// initialize
		$baccarat_commission_val=split('#',$baccarat);
		$roullete_commission_val=split('#',$roullete);
		$dragontiger_commission_val=split('#',$dragontiger);
		$blackjack_commission_val=split('#',$blackjack);
		$american_roulette_commission_val=split('#',$american_roulette);
		$slot_commission_val=split('#',$slot);

		//update commission for costavegas
		if($baccarat_commission_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'commission_baccarat', $account_id);
		}
		if($roullete_commission_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'commission_roulette', $account_id);
		}
		if($dragontiger_commission_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'commission_dragon_tiger', $account_id);
		}
		if($blackjack_commission_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'commission_blackjack', $account_id);
		}
		if($american_roulette_commission_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'commission_american_roulette', $account_id);
		}
		if($slot_commission_val[0]==0){
			$this->updateChildAgentSharingCommissionToZero(3,'commission_slots', $account_id);
		}

		//update commission for hatienvegas
		if($baccarat_commission_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'commission_baccarat', $account_id);
		}
		if($roullete_commission_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'commission_roulette', $account_id);
		}
		if($dragontiger_commission_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'commission_dragon_tiger', $account_id);
		}
		if($blackjack_commission_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'commission_blackjack', $account_id);
		}
		if($american_roulette_commission_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'commission_american_roulette', $account_id);
		}
		if($slot_commission_val[1]==0){
			$this->updateChildAgentSharingCommissionToZero(1,'commission_slots', $account_id);
		}

		//update commission for savanvegas
		if($baccarat_commission_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'commission_baccarat', $account_id);
		}
		if($roullete_commission_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'commission_roulette', $account_id);
		}
		if($dragontiger_commission_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'commission_dragon_tiger', $account_id);
		}
		if($blackjack_commission_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'commission_blackjack', $account_id);
		}
		if($american_roulette_commission_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'commission_american_roulette', $account_id);
		}
		if($slot_commission_val[2]==0){
			$this->updateChildAgentSharingCommissionToZero(2,'commission_slots', $account_id);
		}

		//update commission for virtuavegas
		if($baccarat_commission_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'commission_baccarat', $account_id);
		}
		if($roullete_commission_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'commission_roulette', $account_id);
		}
		if($dragontiger_commission_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'commission_dragon_tiger', $account_id);
		}
		if($blackjack_commission_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'commission_blackjack', $account_id);
		}
		if($american_roulette_commission_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'commission_american_roulette', $account_id);
		}
		if($slot_commission_val[3]==0){
			$this->updateChildAgentSharingCommissionToZero(4,'commission_slots', $account_id);
		}
		
		//update commission for slotsvegas
		if($baccarat_commission_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'commission_baccarat', $account_id);
		}
		if($roullete_commission_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'commission_roulette', $account_id);
		}
		if($dragontiger_commission_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'commission_dragon_tiger', $account_id);
		}
		if($blackjack_commission_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'commission_blackjack', $account_id);
		}
		if($american_roulette_commission_val[4]==0){
			$this->updateChildAgentSharingCommissionToZero(6,'commission_american_roulette', $account_id);
		}
		if($slot_commission_val[4]==0){
			//exit($slot_commission_val[4]);
			$this->updateChildAgentSharingCommissionToZero(6,'commission_slots', $account_id);
		}
	}


	/**
	 * @todo update lower agent and player casino access level
	 * @author leokarl
	 * @date 2012-12-10
	 * @param int $casino_level_id
	 * @param string $account_id
	 * @param array $agent_current_casinos
	 */
	public function updateLowerAgentAndPlayerCasinoLevel($casino_level_id, $account_id, $agent_current_casinos){
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;
		$parentCasinoLevel = TableCasinoAccessLevelSetting::model()->find(array('select'=>'htv,sv,vig,vv,sb,svs',
				'condition'=>'level_id=:level_id',
				'params'=>array(':level_id'=>$casino_level_id),)
		);


		$command = $connection->createCommand("
				Select a.account_id,a.agent_type,a.casino_access_level,b.* from tbl_agent a left join tbl_casino_access_level_setting b on b.level_id=a.casino_access_level
				where a.account_id like '" . $account_id . "%'
				union
				Select a.account_id,'MEM' as agent_type,a.casino_access_level,b.* from tbl_agent_player a left join tbl_casino_access_level_setting b on b.level_id=a.casino_access_level
				where a.account_id like '" . $account_id . "%';"
		);
		$rows = $command->query();

		foreach($rows as $child){
			// parent agent's casino access level versus child agent/player casino access level
			// 			$htv = ($parentCasinoLevel['htv'] == 0) ? 0 : $child['htv'];
			// 			$sv = ($parentCasinoLevel['sv'] == 0) ? 0 : $child['sv'];
			// 			$vig = ($parentCasinoLevel['vig'] == 0) ? 0 : $child['vig'];
			// 			$vv = ($parentCasinoLevel['vv'] == 0) ? 0 : $child['vv'];

			if($parentCasinoLevel['htv'] == 0){
				$htv = 0;
			}else{
				// if parent agent's current casino did not change, keep the child agent's casino.
				$htv = ($agent_current_casinos['htv'] == $parentCasinoLevel['htv']) ? $child['htv'] : 1;
			}
				
			if($parentCasinoLevel['sv'] == 0){
				$sv = 0;
			}else{
				// if parent agent's current casino did not change, keep the child agent's casino.
				$sv = ($agent_current_casinos['sv'] == $parentCasinoLevel['sv']) ? $child['sv'] : 1;
			}
				
			if($parentCasinoLevel['vig'] == 0){
				$vig = 0;
			}else{
				// if parent agent's current casino did not change, keep the child agent's casino.
				$vig = ($agent_current_casinos['vig'] == $parentCasinoLevel['vig']) ? $child['vig'] : 1;
			}
				
			if($parentCasinoLevel['vv'] == 0){
				$vv = 0;
			}else{
				// if parent agent's current casino did not change, keep the child agent's casino.
				$vv = ($agent_current_casinos['vv'] == $parentCasinoLevel['vv']) ? $child['vv'] : 1;
			}
			if($parentCasinoLevel['sb'] == 0){
				$sb = 0;
			}else{
				// if parent agent's current casino did not change, keep the child agent's casino.
				$sb = ($agent_current_casinos['sb'] == $parentCasinoLevel['sb']) ? $child['sb'] : 1;
			}
			if($parentCasinoLevel['svs'] == 0){
				$svs = 0;
			}else{
				// if parent agent's current casino did not change, keep the child agent's casino.
				$svs = ($agent_current_casinos['svs'] == $parentCasinoLevel['svs']) ? $child['svs'] : 1;
			}
				
			// casino access level id for child agent
			$casinoLevelSettingID=$this->getCasinoLevelSettingId($htv,$sv,$vig,$vv,$sb,$svs);
				
			if($child['agent_type']!='MEM'){
				TableAgent::model()->updateAll(array('casino_access_level'=>$casinoLevelSettingID),'account_id="'.$child['account_id'].'"');
			}else{
				TableAgentPlayer::model()->updateAll(array('casino_access_level'=>$casinoLevelSettingID),'account_id="'.$child['account_id'].'"');
			}
		}
	}

	/**
	 * @todo know if current data is already exist in the database
	 * @author leokarl
	 * @since 2012-12-11
	 * @param float $credit
	 * @param float $credit_before
	 * @param datetime $datetime
	 * @param string $agent_account_id
	 * @param string $operator_id
	 */
	public function agentDuplicateEntryValidation($credit, $credit_before, $datetime, $agent_account_id, $operator_id){
		$intTransType = ($credit > $credit_before) ? 7 : 8; // 7 => add, 8 => remove

		$rd = TableAgentTransactionHistory::model()->find(array('select'=>'*',
				'condition'=>'transaction_date=:transaction_date AND agent_account_id=:agent_account_id AND trans_type_id=:trans_type_id AND amount=:amount AND operator_id=:operator_id',
				'params'=>array(':transaction_date' => $datetime,
						':agent_account_id' => $agent_account_id,
						':trans_type_id' => $intTransType,
						':amount' => $credit,
						':operator_id' => $operator_id),)
		);

		return $rd['operator_id'];
	}


	/**
	 * @todo know if current data is already exist in the database
	 * @author leokarl
	 * @since 2012-12-11
	 * @param float $credit
	 * @param float $credit_before
	 * @param datetime $datetime
	 * @param string $player_account_id
	 * @param string $operator_id
	 */
	public function playerDuplicateEntryValidation($credit, $credit_before, $datetime, $player_account_id, $operator_id){
		$intTransType = ($credit > $credit_before) ? 7 : 8; // 7 => add, 8 => remove

		$rd = TableAgentPlayerTransactionHistory::model()->find(array('select'=>'*',
				'condition'=>'transaction_date=:transaction_date AND agent_account_id=:agent_account_id AND trans_type_id=:trans_type_id AND amount=:amount AND operator_id=:operator_id',
				'params'=>array(':transaction_date'=>$datetime,
						':agent_account_id'=>$player_account_id,
						':trans_type_id'=>$intTransType,
						':amount'=> $credit,
						':operator_id'=>$operator_id),)
		);

		return $rd['operator_id'];
	}

	/**
	 * @todo return TRUE if add transaction, FALSE if update.
	 * @author leokarl
	 * @since 2012-12-11
	 * @param string $agent_account_id
	 */
	public function isAddAgentTransaction($agent_account_id){
		$countAccountID = TableAgent::model()->count(array('select'=>'account_id',
				'condition'=>'account_id=:account_id',
				'params'=>array(':account_id'=>$agent_account_id),)
		);
		return ($countAccountID == 0) ? TRUE : FALSE;
	}

	/**
	 * @todo return TRUE if add transaction, FALSE if update.
	 * @author leokarl
	 * @since 2012-12-11
	 * @param string $player_account_id
	 */
	public function isAddPlayerTransaction($player_account_id){
		$countAccountID = TableAgentPlayer::model()->count(array('select'=>'account_id',
				'condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $player_account_id),)
		);
		return ($countAccountID == 0) ? TRUE : FALSE;
	}

	/**
	 * @todo check account_id in tbl_cash_player
	 * @author leokarl
	 * @since 2012-12-14
	 * @param string $account_id
	 * @return boolean
	 */
	public function isAccountExistedInCashPlayer($account_id){
		$cash_player_count = TableCashPlayer::model()->count(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);

		return ($cash_player_count > 0) ? TRUE : FALSE;
	}

	/**
	 * @todo check account_id in tbl_agent
	 * @author leokarl
	 * @since 2012-12-14
	 * @param string $account_id
	 * @return boolean
	 */
	public function isAccountExistedInAgent($account_id){
		$agent_count = TableAgent::model()->count(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);

		return ($agent_count > 0) ? TRUE : FALSE;
	}

	/**
	 * @todo get agent and agent player field data by account_id
	 * @author leokarl
	 * @since 2013-01-04
	 * @param string $account_id
	 * @param string $field_name
	 */
	public function getFieldData($account_id,$field_name){
		if($this->getAccountTypeCodeByAccountId($account_id) != 'MEM'){
			$model = TableAgent::model()->find(array('select'=>$field_name,
					'condition'=>'account_id=:account_id',
					'params'=>array(':account_id'=>$account_id))
			);
		}else{
			$model = TableAgentPlayer::model()->find(array('select'=>$field_name,
					'condition'=>'account_id=:account_id',
					'params'=>array(':account_id'=>$account_id))
			);
		}

		return $model[$field_name];
	}

	/**
	 * @todo check account_id in tbl_agent_player
	 * @author leokarl
	 * @since 2012-12-14
	 * @param unknown_type $account_id
	 * @return boolean
	 */
	public function isAccountExistedInAgentPlayer($account_id){
		$agent_player_count = TableAgentPlayer::model()->count(array('condition'=>'account_id=:account_id',
				'params'=>array(':account_id' => $account_id),)
		);

		return ($agent_player_count > 0) ? TRUE : FALSE;
	}

	/**
	 * @todo check player if on the lobby
	 * @param string $account_id
	 * @return boolean
	 */
	public function isPlayerOnTheLobby($account_id){
		$redis = new RedisManager();
		return ($redis->isExistingOnLobbyByPlayer(strtoupper($_POST['account_id'])) == 1) ? TRUE : FALSE;
	}


	/**
	 * @todo account_id vs account_type validation
	 * @author leokarl
	 * @since 2012-12-17
	 * @param string $account_id
	 * @param string $type
	 * @return boolean
	 */
	public function isAccountIdVStypeValid($account_id, $type){
		return ($type == $this->getAccountTypeCodeByAccountId($account_id)) ? TRUE : FALSE;
	}

	/**
	 * @todo get account type code using account_id
	 * @author leokarl
	 * @since 2012-12-17
	 * @param string $account_id
	 * @return account code
	 */
	public function getAccountTypeCodeByAccountId($account_id){
		return $this->getAccountTypeByCharLength(strlen($account_id));
	}

	/**
	 * @todo get account type using length
	 * @author leokarl
	 * @since 2012-12-18
	 * @param int $length
	 * @return account code
	 */
	public function getAccountTypeByCharLength($length){
		switch ($length)
		{
			case 2:
				return 'SC'; break;
			case 4:
				return 'SMA'; break;
			case 6:
				return 'MA'; break;
			case 8:
				return 'AGT'; break;
			case 10:
				return 'MEM'; break;
			default:
				return 'Undefined';
		}
	}

	/**
	 * @todo if account_id for add valid with creating agent.
	 * @author leokarl
	 * @since 2012-12-18
	 * @param string $agent_account_id
	 * @param string $account_id
	 * @return boolean
	 */
	public function isAccountForAddValid($agent_account_id,$account_id){
		if(strtoupper(substr($account_id, 0, strlen($agent_account_id))) == strtoupper($agent_account_id)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/**
	 * @todo if player balance is stock on a lobby
	 * @author Allan
	 * @since 2013-08-20
	 */
	public function isStockOnTheLobby(){
		$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["AccountID"]."' and deposit_withdrawal<>0");
		$rd=$command->queryRow();
		if ($rd['deposit_withdrawal'] == 1){
			echo "w#w#w";
			if ($rd['casino_id']==1 || $rd['casino_id']==2){
				$checkBalanceIfLock = new RedisLobbyManager();
				$result=$checkBalanceIfLock->withdrawFromCasinoLobby($_POST['AccountID']);
				//if there's a problem in HTV and SAVAN
				if ($result['errorCode'] == 1 && $result['casino'] === 'HTV') // error
				{
					//to display message that there is an error during lobby withdraw ih HTV
					echo 'h';
				}
				else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN') // error
				{
					//to display message that there is an error during lobby withdraw ih SAVAN
					echo 's';
				}
			}
			else // balance stock on Costavegas & VirtuaVegas
			{
				echo '0';
			}
		}
		else {
			echo 0;
		}
	
	}
}
