<?php
/**
 * @todo CreateUserController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-24
 */
class BanAgentPlayerController extends MyController
{

	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('systemSetting.readBanAgentPlayer'))
		{
			$this->render("index");
		}
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}

}
