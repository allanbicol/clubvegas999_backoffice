<?php
/**
 * @todo AgentKickOff Model
 * @copyright CE
 * @author Sitthykun
 * @since 2013-06-014
 */
class AgentKickOff
{
	/**
	 * @return string
	 * @param string $agent
	 */
	public function getKickOffStatus($agent) {
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("CALL spBeGetKickoffStatusByAgent(:agent, @status, @kickOffTime, @kickOnTime)");
		$command->bindParam(":agent", $agent, PDO::PARAM_STR);
		$command->execute();

		$command = $connection->createCommand("SELECT @status, @kickOffTime, @kickOnTime;");
		return $command->queryRow(); 
	}

	/**
	 * 
	 * @param string $agent
	 * @param tinyint $kickOffValue
	 */
	public function setKickOffAgentByAgent($agent, $kickOffValue = 1) {	
		$command = Yii::app()->db->createCommand('CALL spBeSetKickoffAgentByAgent(:agent,:kickOff)');
		$command->execute(array('agent' => $agent, 'kickOff' => $kickOffValue));
	}

	/**
	 * 
	 * @param string $agent
	 * @param tinyint $kickOffValue
	 */
	public function setKickOffAgentPlayerByAgent($agent, $kickOffValue = 1) {
		$command = Yii::app()->db->createCommand('CALL spBeSetKickoffAgentPlayersByAgent(:agent,:kickOff)');
		$command->execute(array('agent' => $agent, 'kickOff' => $kickOffValue));
	}
}