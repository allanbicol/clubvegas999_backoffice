<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/button.css"/>
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/instantPoolsTable.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/instantPools.js"></script>

<script type="text/javascript">
	var urlInstantPools='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPools/InstantPools';
	var urlInstantPoolsMB02='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPools/InstantPoolsMB02';
	var urlInstantPoolsMB03='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPools/InstantPoolsMB03';
	var urlInstantPoolsRL01='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPools/InstantPoolsRL01';
	var urlInstantPoolsDT01='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPools/InstantPoolsDT01';
	var urlChecking='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPools/Checking';
	var urlMB02='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPoolsMB02';
	var urlMB03='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPoolsMB03';
	var urlRL01='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPoolsRL01';
	var urlDT01='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPoolsDT01';
	var urlMB01='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemMonitor/InstantPoolsMB01';
</script>

</head>
<body onload="btnALL();">

<div id="parameter_area" style="width: 500px">
	<div class="header" >Instant Pools</div>
	<br>
	<form action="" style="padding-left: 15px;">
			<div align="left">
			<input onclick="javascript:showMB01();" id="mb01" type="button" value="Baccarat (MB-01)" class="btn red">
			<input onclick="javascript:showMB02();" id="mb02" type="button" value="Baccarat (MB-02)" class="btn red">
			<input onclick="javascript:showMB03();" id="mb03" type="button" value="Baccarat (MB-03)" class="btn red">
			</div>
			<div align="left">	
			<input onclick="showRL01();" id="rl01" type="button" value="Roulette (RL-01)" class="btn red">
			<input onclick="showDT01();" id="dt01" type="button" value="Dragon Tiger (DT-01)" class="btn red">
			<input onclick="btnALL();" id="all" type="button" value="All" class="btn red disabled">
			</div>
	</form>	
	<br>
</div>
	<div class="instantPools" id="qry_result_mb_01"></div>
</body>
</html>