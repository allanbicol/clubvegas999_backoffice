<?php
/**
 * @todo data validation model
 * @copyright CE
 * @author Leo karl
 * @since 2012-12-22
 */
class AgentPublicDataValidation
{
	
	/**
	 * @todo get account type using length
	 * @author leokarl
	 * @since 2012-12-18
	 * @param int $length
	 * @return account code
	 */
	public function getAccountTypeByCharLength($length){
		switch ($length)
		{
			case 2:
				return 'SC'; break;
			case 4:
				return 'SMA'; break;
			case 6:
				return 'MA'; break;
			case 8:
				return 'AGT'; break;
			case 10:
				return 'MEM'; break;
			default:
				return 'Undefined';
		}
	}
	
	/**
	 * @todo compare accoun_id.
	 * @author leokarl
	 * @since 2012-12-18
	 * @param string $agent_account_id
	 * @param string $account_id
	 * @return boolean
	 */
	public function isSubAgentAccountIdValid($agent_account_id,$account_id){
		// initialize
		$validation_count = 0;
		
		// parent account_id vs child account_id
		if(strtoupper(substr($account_id, 0, strlen($agent_account_id))) != strtoupper($agent_account_id)){
			$validation_count +=1;
		}
		
		// is account_id exist
		//if(strlen($agent_account_id) <= 6){
			$data_count_agent = TableAgent::model()->count(array('condition'=>'account_id=:account_id',
					'params'=>array(':account_id' => $account_id),)
			);
			$data_count_player = TableAgentPlayer::model()->count(array('condition'=>'account_id=:account_id',
					'params'=>array(':account_id' => $account_id),)
			);
		
		if(($data_count_agent + $data_count_player)  == 0){
			$validation_count +=1;
		}
		
		return ($validation_count == 0) ? TRUE : FALSE;
	}
}