<?php
/**
 * @todo Deposit Model
 * @copyright CE
 * @author Allan Bicol
 * @since 2012-07-09
 *
 */
class Deposit
{
	public function makeDeposit(){
		$connection = Yii::app()->db_cv999_fd_master;
		if($_POST['task']=='checkPlayerOnlineStat')
				{
					$command = $connection->createCommand("CALL spIsPlayerOnlineInCasino('" . $_POST["accountID"] . "',@po_result);");
					$command->execute();
					$command = $connection->createCommand("SELECT @po_result as poResult");
					$rd=$command->queryRow();
					echo $rd['poResult'];
				
		}elseif ($_POST['task']=='updateBalance'){

				$AccountID = Yii::app()->session['account_id'];
				$level=Yii::app()->session['level_name'];
				
				$postUpdateSkrill= new TableSkrillDeposit;
				$postUpdatePlayerBalance= new TableCashPlayer;
				$postPlayerTransHistory=new TableCashPlayerTransHistory;
				$postLog=new TableLog;
				
		 		$dateTime = date('Y-m-d H:i:s');
		 		$command = $connection->createCommand("SELECT balance,currency_id from tbl_cash_player  WHERE account_id='" . $_POST["accountID"] . "'");
				$rd=$command->queryRow();
				$currency=$rd['currency_id'];
				$balance=$rd['balance'];
				
				
		 		$balanceTotal= $balance + $_POST['amount'];
				
				TableSkrillDeposit::model()->updateAll(array('transaction_status_id'=>6),'transaction_number="'.$_POST['transNo'].'"');
				
				$postUpdatePlayerBalance=TableCashPlayer::model()->findByPk($_POST["accountID"]);
				$postUpdatePlayerBalance->balance=$balanceTotal;
				$postUpdatePlayerBalance->last_update_balance=$dateTime;
				
				$postPlayerTransHistory->player_id=$_POST["accountID"];
				$postPlayerTransHistory->trans_number=$_POST['transNo'];
				$postPlayerTransHistory->cash_player_trans_type_id=1;
				$postPlayerTransHistory->currency_id=$currency;
				$postPlayerTransHistory->amount=$_POST["amount"];
				$postPlayerTransHistory->balance_bafore=$balance;
				$postPlayerTransHistory->balance_after=$balanceTotal;
				$postPlayerTransHistory->submit_time=$_POST['submitDate'];
				$postPlayerTransHistory->finish_time=$dateTime;
				$postPlayerTransHistory->cashier_id=Yii::app()->session['account_id'];
				$postPlayerTransHistory->status_id=6;
				$postPlayerTransHistory->deposit_via='moneybookers:Moneybookers';
				
				$dateTime = date('Y-m-d H:i:s');
				
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated=$_POST["accountID"];
				$postLog->operated_level='Cash Player';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=3;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> Confirmed deposit:<label style=\"color:red\">'.number_format($_POST["amount"],2,'.',',').' of '.$_POST["accountID"].'- Status:</label><label style=\"color:green\">Completed</label></b>';
				
				$postUpdatePlayerBalance->save();
				$postPlayerTransHistory->save();
				$postLog->save();
				
				echo 'Completed';
		}
		
	}
}
