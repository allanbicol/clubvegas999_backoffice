<html>
<head>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.7.2/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/emailsetting.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/signUpMailList.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ckeditor/ckeditor.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/emailsetting.css" />
	<script type="text/javascript">
	var currencyListURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/SignUpEmailList';
	var saveEmailSettingURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/SaveEmailSetting1';
	var setSignupSettingURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/EmailSetting/SetSignupSetting';
	//The instanceReady event is fired, when an instance of CKEditor has finished
	//its initialization.
	var editor;
	CKEDITOR.on( 'instanceReady', function( ev ) {
		// Show this sample buttons.
		document.getElementById( 'eButtons' ).style.display = 'block';
	});
	editor.setReadOnly('True');

	</script>
	
</head>


<body>
	<table id="listMail" ><tr><td/></tr></table>
	<div id="pagerMail"></div>
	<br/>
	<br/>
	
	<?php 
	$signupName=(isset(Yii::app()->session['signupEmail'])) ? Yii::app()->session['signupEmail'] : 'Email Verification';
	echo '<div id="signupName"><h3>'.$signupName.'</h3></div>';
	$language='';
	$dataReader = TableLanguage::model()->findAll();
	foreach ($dataReader as $row){
		$language=$language.",".$row['language'];
	}
	$langValue=split(",",$language);

	$enTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.signupEmail.enSetting',null,TRUE);
	$thTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.signupEmail.thSetting',null,TRUE);
	$vnTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.signupEmail.vnSetting',null,TRUE);
	$cnTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.signupEmail.cnSetting',null,TRUE);
	$hkTab =$this->renderPartial('application.modules.SystemSetting.views.emailSetting.signupEmail.hkSetting',null,TRUE);
	$this->widget('zii.widgets.jui.CJuiTabs', array(
		'tabs' => array(
				'English'=>array('content'=>$enTab, 'id'=>'tabSignupEN'),
				'Thai'=>array('content'=>$thTab, 'id'=>'tabSignupTH'),
				'Vietnamese'=>array('content'=>$vnTab, 'id'=>'tabSignupVN'),
				'Traditional Chinese'=>array('content'=>$cnTab, 'id'=>'tabSignupCN'),
				'Simplified chinese'=>array('content'=>$hkTab, 'id'=>'tabSignupHK'),
		),
		// additional javascript options for the tabs plugin
		'options' => array(
				'collapsible' => false,
		),
		// set id for this widgets
		'id'=>'EmailSignupTab',
		'htmlOptions'=>array(
				'style'=>'width:1000px;'
		),
	));
?>
</body>      
</html>

