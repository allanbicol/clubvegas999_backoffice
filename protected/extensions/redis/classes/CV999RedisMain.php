<?php
/**
 * @TODO store all variables CV999
 * @copyright Creative Entertainment Ltd.
 * @author sitthykun
 * @since 2012-04-04
 */

class CV999RedisMain extends CV999RedisCore {
	private $mainKey		= '';			// ex: lobby
	private $playerTypeID	= '';
	private $playerName		= '';
	// exclude login
	private $casinoID		= '';			// ex: savan = 2
	// symbol
	protected $_keySeparateList	= ':';

	/**
	 * 
	 */
	public function CV999RedisMain($mainKey) {
		// call super class
		parent::CV999RedisCore();
		// main key
		$this->mainKey = $mainKey;
	}

	/**
	 *
	 * @param integer $playerTypeID
	 * @param string $playerName
	 */
	public function addPlayer($playerTypeID, $playerName, $casinoID = NULL, $value = '1') {
		//
		$keyTemp = '';
		// validate
		if ($casinoID != NULL) {
			$keyTemp = $this->generateKeyTemp(array(strval($casinoID), $playerTypeID, $playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($playerTypeID, $playerName));
		}

		// store key
		$this->setKey($keyTemp);

		// $this->redis->lpush($keyTemp, ''); // for list
		$this->redis->setnx($keyTemp, $value);

		// check expiration
		if ($this->getExpiration() != NULL) {
			$this->redis->expire($keyTemp, $this->getExpiration());
		}
	}

	/**
	 *
	 */
	protected function addPlayerWithoutParams() {
		$keyTemp = '';
		// 
		if ($this->casinoID != '') {
			$keyTemp = $this->generateKeyTemp(array($this->casinoID, $this->playerType, $this->playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($this->playerType, $this->playerName));
		}

		// store key
		$this->setKey($keyTemp);

		// $this->redis->lpush($keyTemp, ''); // for list
		$this->redis->setnx($keyTemp, '');
		$this->redis->expire($keyTemp, $this->getExpiration());
	}

	/**
	 *
	 * @param integer $playerTypeID
	 * @param string $playerName
	 * @param string $casinoName
	 */
	public function deletePlayer($playerTypeID, $playerName, $casinoID = NULL) {
		//
		$keyTemp = '';
		// validate
		if ($casinoID != NULL) {
			$keyTemp = $this->generateKeyTemp(array(strval($casinoID), $playerTypeID, $playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($playerTypeID, $playerName));
		}

		// $this->redis->lrem($keyTemp . ' 0 ""'); // for list
		$this->redis->del($keyTemp);
	}

	/**
	 *
	 */
	protected function deletePlayerWithoutParams() {
		$keyTemp = '';
		// 
		if ($this->casinoID != '') {
			$keyTemp = $this->generateKeyTemp(array($this->casinoID, $this->playerType, $this->playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($this->playerType, $this->playerName));
		}

		// $this->redis->lrem($keyTemp . ' 0 ""'); // for list
		$this->redis->del($keyTemp);
	}

	/**
	 * 
	 * @param array $array
	 */
	public function generateKeyTemp($array) {
		array_unshift($array, $this->mainKey);
		return @implode($this->_keySeparateList, $array);
	}
	
	/**
	 * 
	 * @param string $playerTypeID
	 * @param string $playerName
	 * @param integer $casinoID
	 */
	public function getPlayer($playerTypeID, $playerName, $casinoID = NULL) {
		//
		$keyTemp = '';
		// validate
		if ($casinoID != NULL) {
			$keyTemp = $this->generateKeyTemp(array($casinoID, $playerTypeID, $playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($playerTypeID, $playerName));
		}

		// $this->redis->lrem($keyTemp . ' 0 ""'); // for list
		return $this->redis->get($keyTemp);
	}

	/**
	 * 
	 */
	protected function getMainKey() {
		return $this->mainKey;
	}

	/**
	 *
	 * @param string $playerTypeID
	 * @param string $playerName
	 */
	public function isExistingKeys($playerTypeID = '*', $playerName = '*', $casinoID = '*') {
		// key temp
		$keyTemp = $this->generateKeyTemp(array($casinoID, $playerTypeID, $playerName));
		return (count($this->redis->keys($keyTemp)) > 0 ? TRUE : FALSE);
	}

	/**
	 * 
	 * @param string $casinoID
	 */
	protected function setCasinoID($casinoID = NULL) {
		$this->casinoID = $casinoID;
	}

	/**
	 * 
	 * @param integer $playerTypeID
	 * @param string $playerName
	 * @param integer $casinoID
	 */
	public function updateExpiration($playerTypeID, $playerName, $casinoID = NULL, $expiration =  NULL) {
		//
		$keyTemp = '';
		// validate
		if ($casinoID != NULL) {
			$keyTemp = $this->generateKeyTemp(array(strval($casinoID), $playerTypeID, $playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($playerTypeID, $playerName));
		}
		
		// store key
		$this->setKey($keyTemp);
		
		// $this->redis->lpush($keyTemp, ''); // for list
		//$this->redis->set($keyTemp, $value);
		if ($expiration != NULL) {
			$this->redis->expire($keyTemp, $expiration);
		} else {
			$this->redis->expire($keyTemp, $this->getExpiration());
		}
	}

	/**
	 *
	 * @param integer $playerTypeID
	 * @param string $playerName
	 * @param integer $casinoID
	 */
	public function updatePlayer($playerTypeID, $playerName, $casinoID = NULL, $value =  NULL) {
		//
		$keyTemp = '';

		// validate
		if ($casinoID != NULL) {
			$keyTemp = $this->generateKeyTemp(array(strval($casinoID), $playerTypeID, $playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($playerTypeID, $playerName));
		}

		// store key
		$this->setKey($keyTemp);

		if ($value != NULL) {
			$this->redis->set($keyTemp, $value);
		} else if ($this->isExistingKeys($playerTypeID, $playerName, $casinoID)) {
			$this->redis->set($keyTemp, $this->redis->get($keyTemp));
		} else {
			$this->redis->set($keyTemp, 1);
		}
	}

	/**
	 *
	 * @param integer $playerTypeID
	 * @param string $playerName
	 * @param integer $casinoID
	 */
	public function updateValue($playerTypeID, $playerName, $casinoID = NULL, $value =  NULL) {
		//
		$keyTemp = '';
		// validate
		if ($casinoID != NULL) {
			$keyTemp = $this->generateKeyTemp(array(strval($casinoID), $playerTypeID, $playerName));
		} else {
			$keyTemp = $this->generateKeyTemp(array($playerTypeID, $playerName));
		}
	
		// store key
		$this->setKey($keyTemp);
	
		if ($value != NULL) {
			$this->redis->set($keyTemp, $value);
		}
	}

}

?>