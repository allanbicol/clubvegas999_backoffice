<?php

class CashPlayerTransHistoryFundDetail 
{

	public function getPlayerTransHistoryFundDetail($orderField, $sortType, $startIndex , $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			//$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		
			if ($_GET['opt']=='bank'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6  and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
				}else{
					//$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and hbg.transaction_item='".$_GET['item']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}else{
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."'  AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}
				}
			}elseif ($_GET['opt']=='others'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
				}else{
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}else{
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."'  AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}
				}
			}else{
				$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6  and status='Completed' and player_id='".$_GET['accountId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}	
		}
		else{
			//$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
			if ($_GET['opt']=='bank'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6  and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
				}else{
					//$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.transaction_item='".$_GET['item']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}else{
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."'  AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}
				}
			}elseif ($_GET['opt']=='others'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
				}else{
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}else{
						$command = $connection->createCommand("SELECT * from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."'  AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
					}
				}
			}else{
				$command = $connection->createCommand("Select * from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6  and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
			}
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountPlayerTransHistoryFundDetail()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			//$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory where cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'" );
			if ($_GET['opt']=='bank'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6  and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
				}else{
					//$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and hbg.transaction_item='".$_GET['item']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' "  );
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}else{
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."'  AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'"  );
					}
				}
			}elseif ($_GET['opt']=='others'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
				}else{
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}else{
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."'  AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'"  );
					}
				}
			}else{
				$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6  and status='Completed' and player_id='".$_GET['accountId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
			}
		}
		else{
			//$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory where cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' " );
			if ($_GET['opt']=='bank'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6  and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
				}else{
					//$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.transaction_item='".$_GET['item']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' "  );
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}else{
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank=1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'"  );
					}
				}
			}elseif ($_GET['opt']=='others'){
				if ($_GET['item']=='ALL-ALL'){
					$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
				}else{
					list($deposit, $withdraw) = explode('-', $_GET['item']);
					if ($deposit=='ALL' && $withdraw<>'ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id<=2 OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND  tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}elseif ($deposit<>'ALL' && $withdraw =='ALL'){
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE (hbg.cash_player_trans_type_id=4 OR (hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' "  );
					}else{
						$command = $connection->createCommand("SELECT COUNT(0) from vwGetTransactionHistory hbg INNER JOIN tbl_transaction_item tti ON tti.id=hbg.transaction_item WHERE ((hbg.cash_player_trans_type_id<=2 AND hbg.transaction_item='".$deposit."' ) OR (hbg.cash_player_trans_type_id=4 AND hbg.transaction_item='".$withdraw."') ) AND tti.transaction_bank<>1 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' AND hbg.finishTime BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."'"  );
					}
				}
			}else{
				$command = $connection->createCommand("Select COUNT(0) from vwGetTransactionHistory hbg inner join tbl_transaction_item tti on tti.id=hbg.transaction_item where hbg.cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and hbg.finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
			}
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	//Begin functions for current page total--------------------------------------------------
	public function getTotalPlayerTransHistoryFundDetailCurrentPage($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("SELECT Count(vwGetTransactionHistory.transNumber) AS trans_count,(Select Sum(Amount) from vwGetTransactionHistory where cash_player_trans_type_id=1 or  cash_player_trans_type_id=2) AS total_deposit,(Select Sum(Amount) from vwGetTransactionHistory where cash_player_trans_type_id=3 or  cash_player_trans_type_id=4) AS total_withdraw,(Select Sum(Amount) from vwGetTransactionHistory where cash_player_trans_type_id=1 or  cash_player_trans_type_id=2) +(Select Sum(Amount) from vwGetTransactionHistory where cash_player_trans_type_id=3 or  cash_player_trans_type_id=4) AS total FROM vwGetTransactionHistory  player_id='".$_GET['accountId']."' and status='Completed' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		}
		else{
			$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' ORDER BY $orderField $sortType LIMIT $startIndex , $limit" );
		}
		$rows = $command->query();
		return $rows;
	}
	
	//End functions for current page total--------------------------------------------------
	
	
	public function getTotalPlayerTransHistoryFundDetailAllPage()
	{
	
	
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' " );
		}
		else{
			$command = $connection->createCommand("Select * from vwGetTransactionHistory where cash_player_trans_type_id<>6 and status='Completed' and player_id='".$_GET['accountId']."' and cash_player_trans_type_id='".$_GET['methodId']."' and finishTime between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'" );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	
}



