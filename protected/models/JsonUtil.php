<?php
/**
 * @todo	Generating Json data format
 * @copyright	CE
 * @author Eric
 * @since	2012-04-07
 */

class JsonUtil
{
	/**
	 * @todo	Generating Json data format
	 * @author	Kimny MOUK
	 * @param	array dataTable	
	 * @param	array fieldNames
	 * @return  string json_data
	 */
	public static function jsonEncode($dataTable,$total,$page,$records,$fieldNames,$htmValue,$htmValueColNo)
	{
		$cells = "";		
		$id = 1;
		foreach($dataTable as $row)
		{
			if ($htmValue!=''){$fieldNamesCount=count($fieldNames)+1;}else{$fieldNamesCount=count($fieldNames);}
			
			$fieldNumber=0;
			$cells .= '{"id":"' . $id . '", "cell":["';
			
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
				if (($htmValue !='') && ($i == $htmValueColNo))
				{
					//$cells .= str_replace('AccountID=CurrencyID=PlayerID=BalanceAmount=', '&AccountID=' . $row[$fieldNames[0]].'&CurrencyID=' .$row[$fieldNames[9]]. '&PlayerID='.$row[$fieldNames[10]]. '&BalanceAmount='.$row[$fieldNames[6]], $htmValue);
					$linkValue=str_replace('AccountID=CurrencyID=PlayerID=BalanceAmount=Status=Bonus=', '&AccountID=' . $row[$fieldNames[0]].'&CurrencyID=' .$row[$fieldNames[11]]. '&PlayerID='.$row[$fieldNames[12]]. '&BalanceAmount='.$row[$fieldNames[6]]. '&Status='.$row[$fieldNames[9]]. '&Bonus='.$row[$fieldNames[6]], $htmValue);
					$linkValue=str_replace('Account_ID=','Account_ID='.$row[$fieldNames[0]],$linkValue);
					$cells .=str_replace('cRowNo',$id,$linkValue);
						
					$fieldNumber-=1;
				}
				else
				{
					$cells .= $row[$fieldNames[$fieldNumber]];
				}
				if ($i != $fieldNamesCount - 1)

				$cells .=  '","';
				$fieldNumber++;
			}

			$cells .= '"]},';
			$id ++;
		}
				
		//End: Edited by Leo karl B. Ladeno 04/09/2012
		$cells = rtrim($cells,",");
		$header = '{' .
			
			'"total": "' . $total . '",' .
			
			'"page": "' . $page . '",' .
			
			'"records": "' . $records . '",' .
			
			'"rows" : [';
			
			$footer = ']' .
			
			'}';
			
		$json_data =  $header . $cells . $footer;
		return $json_data;
	}
	public static function jsonAgentTransHist($dataTable,$total,$page,$records,$fieldNames,$htmValue,$htmValueColNo)
	{
		$cells = "";		
		$id = 1;
		foreach($dataTable as $row)
		{
			if ($htmValue!=''){
				$fieldNamesCount=count($fieldNames)+1;
			}else{$fieldNamesCount=count($fieldNames);
			}
				
			$fieldNumber=0;
			$cells .= '{"id":"' . $id . '", "cell":["';
				
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
				if (($htmValue !='') && ($i == $htmValueColNo))
				{
					
					$linkValue=str_replace('agentaccountid',$row[$fieldNames[0]],$htmValue);
					$linkValue=str_replace('currency_id',$row[$fieldNames[5]],$linkValue);
					$linkValue=str_replace('id1',$row[$fieldNames[1]],$linkValue);
					$cells .=$linkValue;
//					if ($fieldNames[$fieldNumber]=="ip_address"){
//						$cells .='<label style=\"color:#006666;\">('.Yii::app()->geoip->lookupCountryName($row[$fieldNames[$fieldNumber]]).')</label>';
//					}
						
					$fieldNumber-=1;
				}
				else
				{
					$cells .= $row[$fieldNames[$fieldNumber]];
//					if ($fieldNames[$fieldNumber]=="ip_address"){
//						$cells .='<label style=\"color:#006666;\">('.Yii::app()->geoip->lookupCountryName($row[$fieldNames[$fieldNumber]]).')</label>';
//					}
				}
				if ($i != $fieldNamesCount - 1)
		
					$cells .=  '","';
					$fieldNumber++;
				}
		
				$cells .= '"]},';
				$id ++;
			}
	
			//End: Edited by Leo karl B. Ladeno 04/09/2012
			$cells = rtrim($cells,",");
			$header = '{' .
			
			'"total": "' . $total . '",' .
			
			'"page": "' . $page . '",' .
			
			'"records": "' . $records . '",' .
			
			'"rows" : [';
			
			$footer = ']' .
			
			'}';
			
			$json_data =  $header . $cells . $footer;
			
			return $json_data;
	}
	public static function jsonOnlineHist($dataTable,$total,$page,$records,$fieldNames,$htmValue,$htmValueColNo)
	{
		$cells = "";
		$id = 1;
		foreach($dataTable as $row)
		{
			if ($htmValue!=''){
				$fieldNamesCount=count($fieldNames)+1;
			}else{$fieldNamesCount=count($fieldNames);
			}
	
			$fieldNumber=0;
			$cells .= '{"id":"' . $id . '", "cell":["';
	
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
			if (($htmValue !='') && ($i == $htmValueColNo))
			{
				
			$linkValue=str_replace('accountid',$row[$fieldNames[0]],$htmValue);
			
			$cells .=$linkValue;
			
			$fieldNumber-=1;
			}
			else
				{
					
				$cells .= $row[$fieldNames[$fieldNumber]];
			}
			if ($i != $fieldNamesCount - 1)
	
				$cells .=  '","';
				$fieldNumber++;
		}
	
				$cells .= '"]},';
				$id ++;
		}
	
		//End: Edited by Leo karl B. Ladeno 04/09/2012
		$cells = rtrim($cells,",");
		$header = '{' .
			
		'"total": "' . $total . '",' .
			
		'"page": "' . $page . '",' .
			
		'"records": "' . $records . '",' .
			
		'"rows" : [';
			
		$footer = ']' .
			
		'}';
			
		$json_data =  $header . $cells . $footer;
			
		return $json_data;
	}
	public static function jsonEncodeSystemSetting($dataTable,$total,$page,$records,$fieldNames,$htmValue,$htmValueColNo)
	{
		$cells = "";		
		$id = 1;
		foreach($dataTable as $row)
		{
	
			if ($htmValue!=''){
				$fieldNamesCount=count($fieldNames)+1;
			}else{$fieldNamesCount=count($fieldNames);
			}
				
			$fieldNumber=0;
			$cells .= '{"id":"' . $id . '", "cell":["';
				
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
			if (($htmValue !='') && ($i == $htmValueColNo))
			{
				$linkValue=str_replace('currency',$row[$fieldNames[0]] . ':' . $row[$fieldNames[1]] . ':' . $row[$fieldNames[2]],$htmValue);
				$linkValue=str_replace('limit_id',$row[$fieldNames[0]],$linkValue);
				$linkValue=str_replace('pool_id','pool_id='.$row[$fieldNames[0]].'&pool_game_id='.$row[$fieldNames[4]],$linkValue);
				$cells .=$linkValue;
	
				$fieldNumber-=1;
			}
			else
			{
			$cells .= $row[$fieldNames[$fieldNumber]];
			}
			if ($i != $fieldNamesCount - 1)
	
				$cells .=  '","';
				$fieldNumber++;
			}
	
			$cells .= '"]},';
			$id ++;
			}
	
			//End: Edited by Leo karl B. Ladeno 04/09/2012
			$cells = rtrim($cells,",");
			$header = '{' .
			
			'"total": "' . $total . '",' .
			
			'"page": "' . $page . '",' .
			
			'"records": "' . $records . '",' .
			
			'"rows" : [';
			
			$footer = ']' .
			
			'}';
			
			$json_data =  $header . $cells . $footer;
			
			return $json_data;
	}
	
	/**
	 * @todo Generating Json data for jqgrid
	 * @author Chamnan Nop
	 * @param array $dataTable
	 * @param int $total
	 * @param int $page
	 * @param int $records
	 * @param array $fieldNames
	 * @param string $htmValue
	 * @param int $htmValueColNo
	 * @return json
	 */
	public static function generateJqgridData($dataTable,$total,$page,$records,$fieldNames,$htmValue,$htmValueColNo)
	{
		
		$cells = "";		
		$id = 1;
		
		//for($e=0; $e<count($dataTable); $e++)
		foreach($dataTable as $row)
		{
		
			if ($htmValue!='')
			{
				$fieldNamesCount=count($fieldNames)+1;
			}else
				{$fieldNamesCount=count($fieldNames);
			}
		
			$fieldNumber=0;
			$cells .= '{"id":"' . $id . '", "cell":["';
			
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
				
				if (($htmValue !='') && ($i == $htmValueColNo))
				{
					$linkValue=str_replace('AccountID=', '&AccountID=' . $row[$fieldNames[0]], $htmValue);
					$linkValue=str_replace('Account_ID=','Account_ID='.$row[$fieldNames[0]],$linkValue);
					$linkValue=str_replace('cur_name',$row[$fieldNames[2]],$linkValue);
					if (strlen($row[$fieldNames[0]])==4){
						$linkValue=str_replace('&type=SC','&type=SMA',$linkValue);
					}elseif (strlen($row[$fieldNames[0]])==6){
						$linkValue=str_replace('&type=SC','&type=MA',$linkValue);
						$linkValue=str_replace('&type=SMA','&type=MA',$linkValue);
					}elseif (strlen($row[$fieldNames[0]])==8){
						$linkValue=str_replace('&type=SC','&type=AGT',$linkValue);
						$linkValue=str_replace('&type=SMA','&type=AGT',$linkValue);
						$linkValue=str_replace('&type=MA','&type=AGT',$linkValue);
					}elseif (strlen($row[$fieldNames[0]])==10){
						$linkValue=str_replace('&type=SC','&type=MEM',$linkValue);
						$linkValue=str_replace('&type=SMA','&type=MEM',$linkValue);
						$linkValue=str_replace('&type=MA','&type=MEM',$linkValue);
						$linkValue=str_replace('&type=AGT','&type=MEM',$linkValue);
					}
					
					$linkValue=str_replace('account_id=b=','account_id='.$row[$fieldNames[0]].'&b='.$row[$fieldNames[0]],$linkValue);
					$linkValue=str_replace('account_id=','account_id='.$row[$fieldNames[0]],$linkValue);
					//$linkValue=str_replace('currency',$row[$fieldNames[0]] . ':' . $row[$fieldNames[1]] . ':' . $row[$fieldNames[2]],$htmValue);
//					if ($fieldNames[$fieldNumber]=="IP" || $fieldNames[$fieldNumber]=="login_ip_address" || $fieldNames[$fieldNumber]=="ip_address"){
//						$cells .= '<label style=\"color:#006666;\">('.Yii::app()->geoip->lookupCountryName($row[$fieldNames[$fieldNumber]]).')</label>';
//					}
					$cells .=str_replace('cRowNo',$id,$linkValue);
				
					$fieldNumber-=1;
				}				
				else
				{
					if (strpos($row[$fieldNames[$fieldNumber]],"\\\'")===false){
						$cells .= $row[$fieldNames[$fieldNumber]];
					}else{
						$cells .= str_replace("\\","",str_replace("\\\'","&#39;",$row[$fieldNames[$fieldNumber]]));
					}
					
//					if ($fieldNames[$fieldNumber]=="IP" || $fieldNames[$fieldNumber]=="login_ip_address" || $fieldNames[$fieldNumber]=="ip_address"){
//						$cells .='<label style=\"color:#006666;\">('.Yii::app()->geoip->lookupCountryName($row[$fieldNames[$fieldNumber]]).')</label>';
//					}
				}
				if ($i != $fieldNamesCount - 1)
		
				$cells .=  '","';
				$fieldNumber++;
			}
			$cells .= '"]},';
			$id ++;
		}
		
		//End: Edited by Leo karl B. Ladeno 04/09/2012
		$cells = rtrim($cells,",");
		$header = '{' .
		
		'"total": "' . $total . '",' .
		
		'"page": "' . $page . '",' .
		
		'"records": "' . $records . '",' .
		
		'"rows" : [';
		
		$footer = ']' .
		
		'}';
		
		$json_data =  $header . $cells . $footer;
		
		return $json_data;
	}
	/**
	 * @todo generate jqgrid data from redis
	 * @author leokarl
	 * @since 2012-12-18
	 * @param unknown_type $cell_data
	 * @param unknown_type $total
	 * @param unknown_type $page
	 * @param unknown_type $records
	 * @return string
	 */
	public static function jsonRedisData($cell_data,$total,$page,$records)
	{
			$cell_data = rtrim($cell_data,",");
			$header = '{' .
				
			'"total": "' . $total . '",' .
				
			'"page": "' . $page . '",' .
				
			'"records": "' . $records . '",' .
				
			'"rows" : [';
				
			$footer = ']' .
				
			'}';
				
			$json_data =  $header . $cell_data . $footer;
			return $json_data;
	}
	
	/**
	 * @todo generate jqgrid data without
	 * @param unknown_type $dataTable
	 * @param unknown_type $total
	 * @param unknown_type $page
	 * @param unknown_type $records
	 * @param unknown_type $fieldNames
	 * @return string
	 */
	public static function jsonJqgridData($dataTable,$total,$page,$records,$fieldNames)
	{
		$cells = "";
		$id = 1;
	
		foreach($dataTable as $row)
		{
			// initialize
			$fieldNamesCount = count($fieldNames);
			$fieldNumber = 0;
			$cells .= '{"id":"' . $id . '", "cell":["';
	
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
	
				$cells .= $row[$fieldNames[$fieldNumber]];
			
				if ($i != $fieldNamesCount - 1)
				
				$cells .=  '","';
				$fieldNumber++;
			}
			
			$cells .= '"]},';
			$id ++;
		}
		
		$cells = rtrim($cells,",");
		$header = '{' .
	
		'"total": "' . $total . '",' .
		'"page": "' . $page . '",' .
		'"records": "' . $records . '",' .
		'"rows" : [';
	
		$footer = ']' .
	
		'}';
	
		$json_data =  $header . $cells . $footer;
	
		return $json_data;
	}
	
	/**
	 * @todo generate data from datatable
	 * @author allan
	 * @since 2013-08-22
	 * @return Html file to export in excel sheet
	 */
	public static function jsonJqgridDataExport($dataTable,$fieldNames,$headerNames,$records)
	{
		$cells = "";
		$id = 1;
		
		//Header
		$cells.="<tr>";
		$fCount = count($headerNames);
		for ($i =0 ; $i < $fCount; $i++)
			{
				$cells .='<td><b>'. $headerNames[$i].'</b></td>';
			}
		$cells.="</tr>";
		//Rows
		foreach($dataTable as $row)
		{
			// initialize
			$fieldNamesCount = count($fieldNames);
			$fieldNumber = 0;
			$cells .= '<tr>';
			
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{

//			if ($fieldNames[$fieldNumber]=="IP" || $fieldNames[$fieldNumber]=="login_ip_address"){
//				$cells .='<td>'.$row[$fieldNames[$fieldNumber]].'('.Yii::app()->geoip->lookupCountryName($row[$fieldNames[$fieldNumber]]).')</td>';
//			}else{
				$cells .='<td>'. $row[$fieldNames[$fieldNumber]].'</td>';
//			}
			
			if ($i != $fieldNamesCount - 1)
	
				$cells .=  '</td>';
				$fieldNumber++;
			}
				
			$cells .= '</tr>';
			$id ++;
		}
	
		$cells = rtrim($cells,",");
		$header = '<div><table>';
	
		$footer = '</table><div>';
	
		$json_data =  $header . $cells . $footer;
	
		return $json_data.'<BREAK>'.$records.'<BREAK>';
	}
	
	
	public static function jsonJqgridSignUpEmailList($dataTable,$total,$page,$records,$fieldNames,$htmValue,$htmValueColNo)
	{
		$cells = "";		
		$id = 1;
		foreach($dataTable as $row)
		{
	
			if ($htmValue!=''){
				$fieldNamesCount=count($fieldNames)+1;
			}else{$fieldNamesCount=count($fieldNames);
			}
				
			$fieldNumber=0;
			$cells .= '{"id":"' . $id . '", "cell":["';
				
			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
			if (($htmValue !='') && ($i == $htmValueColNo))
			{
				$linkValue=str_replace('id',$row[$fieldNames[0]] . ':' . $row[$fieldNames[1]] . ':' . $row[$fieldNames[2]],$htmValue);
				//$linkValue=str_replace('limit_id',$row[$fieldNames[0]],$linkValue);
				//$linkValue=str_replace('pool_id','pool_id='.$row[$fieldNames[0]].'&pool_game_id='.$row[$fieldNames[4]],$linkValue);
				$cells .=$linkValue;
	
				$fieldNumber-=1;
			}
			else
			{
				$cells .= $row[$fieldNames[$fieldNumber]];
			}
			if ($i != $fieldNamesCount - 1)
	
				$cells .=  '","';
				$fieldNumber++;
			}
	
			$cells .= '"]},';
			$id ++;
			}
	
			//End: Edited by Leo karl B. Ladeno 04/09/2012
			$cells = rtrim($cells,",");
			$header = '{' .
			
			'"total": "' . $total . '",' .
			
			'"page": "' . $page . '",' .
			
			'"records": "' . $records . '",' .
			
			'"rows" : [';
			
			$footer = ']' .
			
			'}';
			
			$json_data =  $header . $cells . $footer;
			
			return $json_data;
	}
	public static function jsonJqgridInstantPools($dataTable,$type,$page,$records,$fieldNames)
	{
		if ($type==1){
			$cells = '<div><table  class="instant1">';
			$cells .='<tr><td>Account Id</td><td>Banker 1:0.95</td><td>Player 1:1</td><td>Tie 1:8</td><td>B Pair 1:11</td><td>P Pair 1:11</td><td>Big 1:0.54</td><td>Small 1:1.5</td><td>Currency</td><td>Total Win/Loss</td><td>Credit</td><td>Balance</td><td>IP</td><tr>';
		}elseif ($type==2){
			$cells = '<div><table  class="instant2">';
			$cells .='<tr><td>Account ID</td><td>Red 1:1</td><td>Black 1:1</td><td>Odd 1:1</td><td>Even 1:1</td><td>Big 1:1</td><td>Small 1:1</td><td>Straight 1:35</td><td>Split Bet 1:17</td><td>Triple Bet 1:11</td><td>Corner Bet 1:8</td><td>Line Bet 1:5</td><td>Three Numbers 1:5</td><td>Four Numbers 1:5</td><td>Row One 1:2</td><td>Row Two 1:2</td><td>Row Three 1:2</td><td>Column One 1:2</td><td>Column Two 1:2</td><td>Column Three 1:2</td><td>Currency</td><td>Total Win/Loss</td><td>Credit</td><td>Balance</td><td>IP</td><tr>';
		}elseif ($type==3){
			$cells = '<div><table  class="instant3">';
			$cells .='<tr><td>Account ID</td><td>Dragon 1:1<td>Tiger 1:1</td><td>Tie 1:8</td><td>Currency</td><td>Total Win/Loss</td><td>Credit</td><td>Balance</td><td>IP</td><tr>';
		}
		$id = 1;
		$bank=0;
		$player=0;
		$tie=0;
		$bPair=0;
		$pPair=0;
		$big=0;
		$small=0;
		$redBet=0;
		$blackBet=0;
		$oddBet=0;
		$evenBet=0;
		$big=0;
		$small=0;
		$threeBet=0;
		$fourBet=0;
		$firstRow=0;
		$sndRow=0;
		$thrRow=0;
		$firstCol=0;
		$sndCol=0;
		$thrCol=0;
		
		for($e=0; $e<count($dataTable); $e++)
		{
			// initialize
			//$cells .= '<tr>';
			$cnt=0;
			if (Yii::app()->session['level']=='SC'){
				$cnt=2;
			}elseif (Yii::app()->session['level']=='SMA'){
				$cnt=4;
			}elseif (Yii::app()->session['level']=='MA'){
				$cnt=6;
			}elseif (Yii::app()->session['level']=='AGT'){
				$cnt=8;
			}
			if (User::getUserType()=="agent"){
				if(substr($dataTable[$e]["ACCTID"],0,$cnt)==substr(Yii::app()->session['account_id'],0,$cnt) && strlen($dataTable[$e]["ACCTID"])==10){	
					
					$fieldNamesCount = count($fieldNames);
					$fieldNumber = 0;
					$cells .= '<tr>';
					if ($type==1){
						$bank=$bank + ($dataTable[$e]["BANK"]);
						$player=$player + ($dataTable[$e]["PLAYER"]);
						$tie=$tie + ($dataTable[$e]["TIE"]);
						$bPair=$bPair + ($dataTable[$e]["BANKPAIR"]);
						$pPair=$pPair + ($dataTable[$e]["PLAYPAIR"]);
						$big=$bPair + ($dataTable[$e]["BIG"]);
						$small=$small + ($dataTable[$e]["SMALL"]);
					}
					if ($type==2){
						$redBet=$redBet + ($dataTable[$e]["REDBET"]);
						$blackBet=$blackBet + ($dataTable[$e]["BLACKBET"]);
						$oddBet=$oddBet + ($dataTable[$e]["ODDBET"]);
						$evenBet=$evenBet + ($dataTable[$e]["EVENBET"]);
						$big=$big + ($dataTable[$e]["BIG"]);
						$small=$small + ($dataTable[$e]["SMALL"]);
						$threeBet=$threeBet + ($dataTable[$e]["THREEBET"]);
						$fourBet=$fourBet + ($dataTable[$e]["FOURBET"]);
						$firstRow=$firstRow + ($dataTable[$e]["FRISTROW"]);
						$sndRow=$sndRow + ($dataTable[$e]["SNDROW"]);
						$thrRow=$thrRow + ($dataTable[$e]["THRROW"]);
						$firstCol=$firstCol + ($dataTable[$e]["FRISTCOL"]);
						$sndCol=$sndCol + ($dataTable[$e]["SNDCOL"]);
						$thrCol=$thrCol + ($dataTable[$e]["THRCOL"]);
					}
					if ($type==3){
						$bank=$bank + ($dataTable[$e]["BANK"]);
						$player=$player + ($dataTable[$e]["PLAYER"]);
						$tie=$tie + ($dataTable[$e]["TIE"]);
					}
					
					for ($i =0 ; $i < $fieldNamesCount; $i++)
					{
						if ($fieldNames[$fieldNumber]=="CURRENCYID"){
							if ($dataTable[$e][$fieldNames[$fieldNumber]]==1){
								$cells .="<td>USD</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==2){
								$cells .="<td>SGD</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==3){
								$cells .="<td>MYR</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==4){
								$cells .="<td>THB</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==5){
								$cells .="<td>CNY</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==6){
								$cells .="<td>HKD</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==7){
								$cells .="<td>TWD</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==8){
								$cells .="<td>VND</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==9){
								$cells .="<td>TEST</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==10){
								$cells .="<td>KHR</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==11){
								$cells .="<td>IDR</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==12){
								$cells .="<td>EUR</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==13){
								$cells .="<td>KRW</td>";
							}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==14){
								$cells .="<td>IDRII</td>";
							}
						}elseif($fieldNames[$fieldNumber]=="BANK" || $fieldNames[$fieldNumber]=="PLAYER" || $fieldNames[$fieldNumber]=="TIE" || $fieldNames[$fieldNumber]=="BANKPAIR" || $fieldNames[$fieldNumber]=="PLAYPAIR" || $fieldNames[$fieldNumber]=="BIG" || $fieldNames[$fieldNumber]=="SMALL"
								|| $fieldNames[$fieldNumber]=="REDBET" || $fieldNames[$fieldNumber]=="BLACKBET" || $fieldNames[$fieldNumber]=="ODDBET" || $fieldNames[$fieldNumber]=="EVENBET" || $fieldNames[$fieldNumber]=="FRISTROW"
								|| $fieldNames[$fieldNumber]=="SNDROW" || $fieldNames[$fieldNumber]=="THRROW" || $fieldNames[$fieldNumber]=="FRISTCOL" || $fieldNames[$fieldNumber]=="SNDCOL" || $fieldNames[$fieldNumber]=="THRCOL" || $fieldNames[$fieldNumber]=="WINLOSS" || $fieldNames[$fieldNumber]=="CREDITQUANTITY" || $fieldNames[$fieldNumber]=="BALANCE"){
							$cells .='<td >'. number_format($dataTable[$e][$fieldNames[$fieldNumber]]) . '</td>';
//						}elseif ($fieldNames[$fieldNumber]=="IP"){
//
//							$cells .= '<td><label style=\"color:#006666;\">('.Yii::app()->geoip->lookupCountryName($dataTable[$e][$fieldNames[$fieldNumber]]).')</label></td>';
//
						}else{
								$cells .='<td >'. $dataTable[$e][$fieldNames[$fieldNumber]] . '</td>';
								
						}

						if ($i != $fieldNamesCount - 1)
				
							$cells .=  '</td>';
							$fieldNumber++;
					}
						
					$cells .= '</tr>';
					$id ++;
				}
			}else{
				$fieldNamesCount = count($fieldNames);
				$fieldNumber = 0;
				$cells .= '<tr>';
				if ($type==1){
					$bank=$bank + ($dataTable[$e]["BANK"]);
					$player=$player + ($dataTable[$e]["PLAYER"]);
					$tie=$tie + ($dataTable[$e]["TIE"]);
					$bPair=$bPair + ($dataTable[$e]["BANKPAIR"]);
					$pPair=$pPair + ($dataTable[$e]["PLAYPAIR"]);
					$big=$bPair + ($dataTable[$e]["BIG"]);
					$small=$small + ($dataTable[$e]["SMALL"]);
				}
				if ($type==2){
					$redBet=$redBet + ($dataTable[$e]["REDBET"]);
					$blackBet=$blackBet + ($dataTable[$e]["BLACKBET"]);
					$oddBet=$oddBet + ($dataTable[$e]["ODDBET"]);
					$evenBet=$evenBet + ($dataTable[$e]["EVENBET"]);
					$big=$big + ($dataTable[$e]["BIG"]);
					$small=$small + ($dataTable[$e]["SMALL"]);
					$threeBet=$threeBet + ($dataTable[$e]["THREEBET"]);
					$fourBet=$fourBet + ($dataTable[$e]["FOURBET"]);
					$firstRow=$firstRow + ($dataTable[$e]["FRISTROW"]);
					$sndRow=$sndRow + ($dataTable[$e]["SNDROW"]);
					$thrRow=$thrRow + ($dataTable[$e]["THRROW"]);
					$firstCol=$firstCol + ($dataTable[$e]["FRISTCOL"]);
					$sndCol=$sndCol + ($dataTable[$e]["SNDCOL"]);
					$thrCol=$thrCol + ($dataTable[$e]["THRCOL"]);
				}
				if ($type==3){
					$bank=$bank + ($dataTable[$e]["BANK"]);
					$player=$player + ($dataTable[$e]["PLAYER"]);
					$tie=$tie + ($dataTable[$e]["TIE"]);
				}
				for ($i =0 ; $i < $fieldNamesCount; $i++)
				{
					if ($fieldNames[$fieldNumber]=="CURRENCYID"){
						if ($dataTable[$e][$fieldNames[$fieldNumber]]==1){
							$cells .='<td>USD</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==2){
							$cells .='<td>SGD</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==3){
							$cells .='<td>MYR</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==4){
							$cells .='<td>THB</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==5){
							$cells .='<td>CNY</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==6){
							$cells .='<td>HKD</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==7){
							$cells .='<td>TWD</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==8){
							$cells .='<td>VND</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==9){
							$cells .='<td>TEST</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==10){
							$cells .='<td>KHR</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==11){
							$cells .='<td>IDR</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==12){
							$cells .='<td>EUR</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==13){
							$cells .='<td>KRW</td>';
						}else if ($dataTable[$e][$fieldNames[$fieldNumber]]==14){
							$cells .='<td>IDRII</td>';
						}
					}elseif($fieldNames[$fieldNumber]=="BANK" || $fieldNames[$fieldNumber]=="PLAYER" || $fieldNames[$fieldNumber]=="TIE" || $fieldNames[$fieldNumber]=="BANKPAIR" || $fieldNames[$fieldNumber]=="PLAYPAIR" || $fieldNames[$fieldNumber]=="BIG" || $fieldNames[$fieldNumber]=="SMALL"
							|| $fieldNames[$fieldNumber]=="REDBET" || $fieldNames[$fieldNumber]=="BLACKBET" || $fieldNames[$fieldNumber]=="ODDBET" || $fieldNames[$fieldNumber]=="EVENBET" || $fieldNames[$fieldNumber]=="FRISTROW" 
							|| $fieldNames[$fieldNumber]=="SNDROW" || $fieldNames[$fieldNumber]=="THRROW" || $fieldNames[$fieldNumber]=="FRISTCOL" || $fieldNames[$fieldNumber]=="SNDCOL" || $fieldNames[$fieldNumber]=="THRCOL" || $fieldNames[$fieldNumber]=="WINLOSS" || $fieldNames[$fieldNumber]=="CREDITQUANTITY" || $fieldNames[$fieldNumber]=="BALANCE"){
						$cells .='<td >'. number_format($dataTable[$e][$fieldNames[$fieldNumber]]) . '</td>';
//					}elseif ($fieldNames[$fieldNumber]=="IP"){
//
//						$cells .= '<td>'. $dataTable[$e][$fieldNames[$fieldNumber]] . '<label style="color:#006666;">('.Yii::app()->geoip->lookupCountryName($dataTable[$e][$fieldNames[$fieldNumber]]).')</label></td>';

					}else{
						$cells .='<td >'. $dataTable[$e][$fieldNames[$fieldNumber]] . '</td>';
								
					}
					
				if ($i != $fieldNamesCount - 1)
				
					$cells .=  '</td>';
					$fieldNumber++;
				}
				
				$cells .= '</tr>';
				$id ++;
			}
		}
	
		$cells = rtrim($cells,",");
		if ($type==1){
			$cells .='<tr><td><b>TOTAL</b></td><td><b>'.number_format($bank,2).'</b></td><td><b>'.number_format($player,2).'</b></td><td><b>'.number_format($tie,2).'</b></td><td><b>'.number_format($bPair,2).'</b></td><td><b>'.number_format($pPair,2).'</b></td><td><b>'.number_format($big,2).'</b></td><td><b>'.number_format($small,2).'</b></td><td></td><td></td><td></td><td></td><td></td><tr>';
		}elseif ($type==2){
			$cells .='<tr><td><b>TOTAL</b></td><td><b>'.number_format($redBet,2).'</b></td><td><b>'.number_format($blackBet,2).'</b></td><td><b>'.number_format($oddBet,2).'</b></td><td><b>'.number_format($evenBet,2).'</b></td><td><b>'.number_format($big,2).'</b></td><td><b>'.number_format($small,2).'</b></td><td></td><td></td><td></td><td></td><td></td><td><b>'.number_format($threeBet,2).'</b></td><td><b>'.number_format($fourBet,2).'</b></td><td><b>'.number_format($firstRow,2).'</b></td><td><b>'.number_format($sndRow,2).'</b></td><td><b>'.number_format($thrRow,2).'</b></td><td><b>'.number_format($firstCol,2).'</b></td><td><b>'.number_format($sndCol,2).'</b></td><td><b>'.number_format($thrCol,2).'</b></td><td></td><td></td><td></td><td></td><td></td><tr>';
		}elseif ($type==3){
			$cells .='<tr><td><b>TOTAL</td><td><b>'.number_format($bank,2).'</b></td><td><b>'.number_format($player,2).'</b></td><td><b>'.number_format($tie,2).'</b></td><td></td><td></td><td></td><td></td><td></td><tr>';
		}
		$footer = '</table></div>'; 
	
		$json_data =   $cells . $footer;
	
		return $json_data;
	}

	/**
	 * @todo generate jqgrid data from redis
	 * @author Allan, Sitthykun
	 * @param string with comma
	 * @param int $total
	 * @param int $page
	 * @param int $records
	 */
	public static function jsonAdvisory($dataTable, $total, $page, $records, $fieldNames, $urlEdit, $urlDelete)
	{
		$cells = "";
		$id = 1;

		foreach($dataTable as $row)
		{
			// initialize
			$fieldNamesCount = count($fieldNames);
			$fieldNumber = 0;
			$cells .= '{"id":"' . $id . '", "cell":["';

			for ($i =0 ; $i < $fieldNamesCount; $i++)
			{
				// input url
// 				if ($i == count($fieldNames) - 2)
// 				{
// 					$cells .= str_replace('rel=\"#\"', 'rel=\"' . $row[$fieldNames[$i]] . '\"', $urlEdit);
// 				}
// 				else if ($i == count($fieldNames) - 1)
// 				{
// 					$cells .= str_replace('rel=\"#\"', 'rel=\"' . $row[$fieldNames[$i]] . '\"', $urlDelete);
// 				} else {
// 					$cells .= $row[$fieldNames[$i]];					
// 				}
				if ($i == count($fieldNames) -1)
				{
					$cells .= str_replace('rel=\"#\"', 'rel=\"' . $row[$fieldNames[$i]] . '\" onclick=\"advisoryEdit(' .$row[$fieldNames[$i]] . ');\"', str_replace('#', $row[$fieldNames[$i]], $urlEdit));
					$cells .= '&nbsp;&nbsp;';
					$cells .= str_replace('rel=\"#\"', 'rel=\"' . $row[$fieldNames[$i]] . '\" onclick=\"advisoryDelete(' .$row[$fieldNames[$i]] . ');\"', $urlDelete);
				} else {
					$cells .= $row[$fieldNames[$i]];
				}


				if ($i != $fieldNamesCount - 1) $cells .=  '","';
			}
			
			$cells .= '"]},';
			$id ++;
		}

		$cells = rtrim($cells, ",");
		$header = '{' .
	
		'"total": "' . $total . '",' .
		'"page": "' . $page . '",' .
		'"records": "' . $records . '",' .
		'"rows" : [';
	
		$footer = ']' .
	
		'}';
	
		$json_data =  $header . $cells . $footer;

		return $json_data;
	}
}