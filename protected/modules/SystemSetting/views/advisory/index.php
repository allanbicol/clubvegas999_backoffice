<html>
<head>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>

	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/advisory.css" />
  	<script src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/ui/minified/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-timepicker-addon.js"></script>
	<!-- <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.10.3/ui/i18n/jquery.ui.timepicker.js"></script> -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/advisory.js"></script>
	<script>
		var advisoryListURL  ='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/GetList';
		var createAdvisoryURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/New';
		var editAdvisoryURL  = '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/GetById';
		var deleteAdvisoryURL='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SystemSetting/Advisory/Delete';
		var advisoryLanguages = <?php echo Languages::getLanguageCodeAsJSON(); ?>;
	</script>

    <style>
    .ui-jqgrid tr.jqgrow td {
            word-wrap: break-word!important; /* IE 5.5+ and CSS3 */
            white-space: pre-wrap!important; /* CSS3 */
            white-space: -moz-pre-wrap!important; /* Mozilla, since 1999 */
            white-space: -pre-wrap!important; /* Opera 4-6 */
            white-space: -o-pre-wrap!important; /* Opera 7 */
            overflow: hidden;
            height: auto;
            vertical-align: middle;
            padding-top: 3px;
            padding-bottom: 3px
        }
    </style>
</head>
<body>
<section class="advisory-container">
	<div class="advisory-box">
		<header class="advisory-header">Advisory Management <a href="index.php?r=SystemSetting/Advisory/New" id="advisoryAddbtn" class="btn red">Add</a></header>
		<table id="advisoryList"><tr><td /></tr></table>
		<div id="advisoryPaper"></div>
	</div>
</section>
</body>      
</html>
