<?php

class CashPlayerWinLossDetailSavan
{

	public function getPlayerWinLossDetailSavan($orderField, $sortType, $startIndex, $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,bet_result AS result,win_loss,win_loss as winloss1,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'Savenvegas999' as casino_name,'1' as counter,ip as IP from  tbl_savan_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,bet_result AS result,win_loss,win_loss as winloss1,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'Savenvegas999' as casino_name,'1' as counter,ip as IP from  tbl_savan_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountPlayerWinLossDetailSavan()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
	if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select COUNT(0) from tbl_savan_casino_history where   account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' "  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(0) from tbl_savan_casino_history where   account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getPlayerWinLossDetailSavanAllPages()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_POST['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_POST['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_POST['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_POST['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_POST['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_POST['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_POST['methodId']=="ALL")
		{
			$command = $connection->createCommand("SELECT COUNT(id) AS bet_count,FORMAT(SUM(available_bet),2) AS available_bet,FORMAT(SUM(bet_amount),2) AS bet_amount,FORMAT(IFNULL(SUM(win_loss),0),2) AS win_loss,FORMAT(IFNULL(SUM(commission),0),2) AS commission,FORMAT((SUM(win_loss) + SUM(commission)),2) AS total_win_loss,FORMAT((SELECT total_balance_after FROM tbl_savan_casino_history AS CH1 WHERE CH1.id=MAX(CH.id)),2) AS current_balance from  tbl_savan_casino_history AS CH where  account_id='".$_POST['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_POST['dateFrom']."' and '".$_POST['dateTo']."'"  );
		}
		else{
			$command = $connection->createCommand("SELECT COUNT(id) AS bet_count,FORMAT(SUM(available_bet),2) AS available_bet,FORMAT(SUM(bet_amount),2) AS bet_amount,FORMAT(IFNULL(SUM(win_loss),0),2) AS win_loss,FORMAT(IFNULL(SUM(commission),0),2) AS commission,FORMAT((SUM(win_loss) + SUM(commission)),2) AS total_win_loss,FORMAT((SELECT total_balance_after FROM tbl_savan_casino_history AS CH1 WHERE CH1.id=MAX(CH.id)),2) AS current_balance  from  tbl_savan_casino_history AS CH where  account_id='".$_POST['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and game_type='".$_POST['methodId']."' and bet_time between '".$_POST['dateFrom']."' and '".$_POST['dateTo']."'"  );
		}
	
		$rows = $command->queryRow();
		$betCount= $rows['bet_count'];
		$validBet= $rows['available_bet'];
		$betAmount= $rows['bet_amount'];
		$winloss= $rows['win_loss'];
		$commission= $rows['commission'];
		$total= $rows['total_win_loss'];
		$currentBalance=$rows['current_balance'];
	
		echo $betCount.'#'.$validBet.'#'.$betAmount.'#'.$winloss.'#'.$commission.'#'.$total.'#'.$currentBalance;
	
	
	}
	public function getSavanVegasResultDetails(){
		$row = TableSavanCasinoHistory::model()->find(array('select'=>'betrslink','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
		 
		return $row['betrslink'];
	}
	public function getSavanVegasWinLossResultDetails()
	{
		$row = TableSavanCasinoHistory::model()->find(array('select'=>'game_type,currency,bet_result_detail,bet_detail,bet_result','condition'=>'id=:id',
				'params'=>array(':id'=>$_GET['winlossid']),));
	
		echo $this->getWinLossBettingDetail($row['game_type'], $row['bet_result_detail'], $row['bet_detail'],$row['bet_result'], $row['currency']);
		
	}
	
	public function getWinLossBettingDetail($game_type, $bet_result_detail_array, $bet_detail_array, $bet_result_array, $currency){
		 
	
		// table header
		$table_data = '<table border="0" cellspacing="2" cellpadding="0" width="100%">'.
				'<tr><th class="c_winlossresultheader">Type</th><th class="c_winlossresultheader">Bet Amount</th><th class="c_winlossresultheader">Win/Loss</th></tr>';
	
		if($game_type == 1){ // baccarat
	
			/*
			 10.0#10.0#9.5^20.0#0.0#0.0^30.0#0.0#0.0^0.0#0.0#0.0^0.0#0.0#0.0^0.0#0.0#0.0^0.0#0.0#0.0^
	
			banker betting amount# banker return current cash amount # banker win amount
			^
			Player betting amount # player return current cash amount # player win amount
			^
			Tie betting amount # Tie return cash amount # Tie win amount
			^
			banker pair betting amount # Banker pair return current cash amount # Banker pair win amount
			^
			player pair betting amount # player pair return current cash amount # player pair win amount
			^
			Big betting amount # big return current cash amount # big win amount
			^
			small betting amount # small return current cash amount # small win amount
			*/
	
			for($i = 0; $i <= 6; $i++) {
	
				/*
				 * @definition
				* $bet_detail[0] = bet_amount
				* $bet_detail[2] = win_loss
				*/
				$bet_result_detail = explode("^", $bet_result_detail_array);
				$bet_detail = split("#",$bet_result_detail[$i]);
				$bet_result = split("#",$bet_result_array);
				if($bet_result[0] == 3){ // tie game
					if($bet_detail[1] > 0){
						$win_loss = ($bet_detail[2] == 0) ? '0' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
					}else{
						$win_loss = ($bet_detail[2] == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $bet_detail[0]) . '</font>' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
					}
				}else{
					$win_loss = ($bet_detail[2] == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $bet_detail[0]) . '</font>' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
				}
				 
	
				if($bet_detail[0] > 0){
					// return table row data
					$table_data .= '<tr><td class="c_bet_type">' . $this->getBaccaratBetTypeByBetDetailNumber($i) . '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $bet_detail[0]) . '</td><td class="c_win_loss">' . $win_loss . '</td></tr>';
				}
			}
			 
		}else if($game_type == 4){ // dragon tiger
			/*
			 * \
			10.0#0.0#0.0^30.0#30.0#29.1^20.0#0.0#0.0^
	
			Dragon betting amount # Dragon return current cash amount # Dragon win amount
			^
			Tiger betting amount # Tiger return current cash amount # Tiger win amount
			^
			Tie betting amount # Tie return current cash amount # Tie win amount
			^
			*/
			for($i = 0; $i <= 2; $i++) {
	
				/*
				 * @definition
				* $bet_detail[0] = bet_amount
				* $bet_detail[2] = win_loss
				*/
				$bet_result_detail = explode("^", $bet_result_detail_array);
				$bet_detail = split("#",$bet_result_detail[$i]);
				$win_loss = ($bet_detail[2] == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $bet_detail[0]) . '</font>' : '<font color="#000c75">' . $this->amountIncentive($currency, $bet_detail[2]) . '</font>'; // if winloss == 0, winloss = -bet_amount
	
				if($bet_detail[0] > 0){
					// return table row data
					$table_data .= '<tr><td class="c_bet_type">' . $this->getDragonTigerBetTypeByBetDetailNumber($i) . '</td><td class="c_bet_amount">' . $this->amountIncentive($currency, $bet_detail[0]) . '</td><td class="c_win_loss">' . $win_loss . '</td></tr>';
				}
			}
			 
		}else if($game_type == 2){ // roulette
			/*
			 00#60|08#30|10#40|11#40|12#30|16#30|19#30|20#70|22#30|23#30|30#70|
			^1619#20|
			^010203#30|101112#30|161718#20|192021#20|313233#50|
			^0
			^0
			^0
			^0
			^0
			^0
			^0
			^210
			^210
			^0
			^100
			^0
			^0
			^0
			^0
			^0^
				
				
			00#0.0#0.0|08#0.0#0.0|10#0.0#0.0|11#0.0#0.0|12#0.0#0.0|16#0.0#0.0|19#0.0#0.0|20#0.0#0.0|22#0.0#0.0|23#0.0#0.0|30#0.0#0.0|
			^1619#0.0#0.0|
			^010203#0.0#0.0|101112#0.0#0.0|161718#20.0#220.0|192021#0.0#0.0|313233#0.0#0.0|
			^0#0
			^0#0
			^0#0
			^0.0#0.0|0#0
			^0#0
			^0#0
			^0#0
			^0.0#0.0|
			^210.0#420.0|
			^0#0
			^100.0#100.0|
			^0#0
			^0#0
			^0#0
			^0#0
			^0#0^
			*/
	
			//straight bet,	split bet,	triple bet,	conner bet,	line bet,	three number,	four number,
			//row1,	row2,	row3,	column1,	column2,	column3,	red,	black,	odd,	even,	small,	big
	
			// bet_detail
			list($straight_bet, $split_bet, $triple_bet, $corner_bet, $line_bet, $three_number, $four_number, $row1, $row2,
					$row3, $col1, $col2, $col3, $red, $black, $odd, $even, $small, $big) = explode("^",$bet_detail_array);
	
			// bet_result_detail
			list($straight_bet_detail, $split_bet_detail, $triple_bet_detail, $corner_bet_detail, $line_bet_detail, $three_number_detail, $four_number_detail, $row1_detail, $row2_detail,
					$row3_detail, $col1_detail, $col2_detail, $col3_detail, $red_detail, $black_detail, $odd_detail, $even_detail, $small_detail, $big_detail) = explode("^",$bet_result_detail_array);
	
			// 1. straight_bet ...
			$raw_straight_bet = split("\|",$straight_bet); // type#bet_amount | type#bet_amount | ...
			$raw_straight_bet_detail = split("\|",$straight_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
			
			$type = ''; $bet_amount = ''; $winloss = '';
	
			for($i=0; $i<count($raw_straight_bet) -1; $i++){
				list($sb_type, $sb_amount) = explode("#",$raw_straight_bet[$i]); //type # bet_amount
				list($sb_type_detail, $sb_return, $sb_win) = explode("#",$raw_straight_bet_detail[$i]); // type # return # win
				$separator = (count($raw_straight_bet)-1 > 1) ? ';'  : '';
				 
				$type .= $this->getRouletteNumberTypeFromBetResultDetail($sb_type) . $separator .'<br/>';
				$bet_amount .= $this->amountIncentive($currency, $sb_amount) . $separator .'<br/>';
				$winloss .= ($sb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $sb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $sb_win) . '</font>'. $separator .'<br/>';
			}
			if($type != ''){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			 
			// 2. split bet ...
			$raw_split_bet = split("\|",$split_bet); // type#bet_amount | type#bet_amount | ...
			$raw_split_bet_detail = split("\|",$split_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
			 
			$type = ''; $bet_amount = ''; $winloss = '';
			 
			for($i=0; $i<count($raw_split_bet) -1; $i++){
				list($sp_type, $sp_amount) = explode("#",$raw_split_bet[$i]); //type # bet_amount
				list($sp_type_detail, $sp_return, $sp_win) = explode("#",$raw_split_bet_detail[$i]); // type # return # win
				$separator = (count($raw_split_bet)-1 > 1) ? ';'  : '';
	
				$type .= $this->getRouletteNumberTypeFromBetResultDetail($sp_type) . $separator .'<br/>';
				$bet_amount .= $this->amountIncentive($currency, $sp_amount) . $separator .'<br/>';
				$winloss .= ($sp_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $sp_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $sp_win) . '</font>'. $separator .'<br/>';
			}
			if($type != ''){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
	
			// ... split bet
	
			// 3. triple bet ...
			$raw_triple_bet = split("\|",$triple_bet); // type#bet_amount | type#bet_amount | ...
			$raw_triple_bet_detail = split("\|",$triple_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
	
			$type = ''; $bet_amount = ''; $winloss = '';
	
			for($i=0; $i<count($raw_triple_bet) -1; $i++){
				list($tb_type, $tb_amount) = explode("#",$raw_triple_bet[$i]); //type # bet_amount
				list($tb_type_detail, $tb_return, $tb_win) = explode("#",$raw_triple_bet_detail[$i]); // type # return # win
				$separator = (count($raw_triple_bet)-1 > 1) ? ';'  : '';
	
				$type .= $this->getRouletteNumberTypeFromBetResultDetail($tb_type) . $separator . '<br/>';
				$bet_amount .= $this->amountIncentive($currency, $tb_amount) . $separator .'<br/>';
				$winloss .= ($tb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $tb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $tb_win) . '</font>'. $separator .'<br/>';
			}
			 
			if($type != ''){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// ... triple bet
			 
			// 4. corner bet ...
			$raw_corner_bet = split("\|",$corner_bet); // type#bet_amount | type#bet_amount | ...
			$raw_corner_bet_detail = split("\|",$corner_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
			 
			$type = ''; $bet_amount = ''; $winloss = '';
			 
			for($i=0; $i<count($raw_corner_bet) -1; $i++){
				list($cb_type, $cb_amount) = explode("#",$raw_corner_bet[$i]); //type # bet_amount
				list($cb_type_detail, $cb_return, $cb_win) = explode("#",$raw_corner_bet_detail[$i]); // type # return # win
				$separator = (count($raw_corner_bet)-1 > 1) ? ';'  : '';
	
				$type .= $this->getRouletteNumberTypeFromBetResultDetail($cb_type) . $separator .'<br/>';
				$bet_amount .= $this->amountIncentive($currency, $cb_amount) . $separator . '<br/>';
				$winloss .= ($cb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $cb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $cb_win) . '</font>'. $separator .'<br/>';
			}
			 
			if($type != ''){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
	
			// ... corner bet
			 
			// 5. line bet ...
			$raw_line_bet = split("\|",$line_bet); // type#bet_amount | type#bet_amount | ...
			$raw_line_bet_detail = split("\|",$line_bet_detail); // type#return#bet_amount | type#return#bet_amount | ...
	
			$type = ''; $bet_amount = ''; $winloss = '';
	
			for($i=0; $i<count($raw_line_bet) -1; $i++){
				list($lb_type, $lb_amount) = explode("#",$raw_line_bet[$i]); //type # bet_amount
				list($lb_type_detail, $lb_return, $lb_win) = explode("#",$raw_line_bet_detail[$i]); // type # return # win
				$separator = (count($raw_line_bet)-1 > 1) ? ';'  : '';
	
				$type .= $this->getRouletteNumberTypeFromBetResultDetail($lb_type) . $separator .'<br/>';
				$bet_amount .= $this->amountIncentive($currency, $lb_amount) . $separator .'<br/>';
				$winloss .= ($lb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $lb_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $lb_win) . '</font>'. $separator .'<br/>';
			}
			 
			if($type != ''){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// ... line bet
			 
			// 6. three number ...
			$raw_three_num = split("\|",$three_number); // type#bet_amount | type#bet_amount | ...
			$raw_three_num_detail = split("\|",$three_number_detail); // type#return#bet_amount | type#return#bet_amount | ...
			 
			$type = ''; $bet_amount = ''; $winloss = '';
			 
			for($i=0; $i<count($raw_three_num) -1; $i++){
				list($three_type, $three_amount) = explode("#",$raw_three_num[$i]); //type # bet_amount
				list($three_type_detail, $three_return, $three_win) = explode("#",$raw_three_num_detail[$i]); // type # return # win
				$separator = (count($raw_three_num)-1 > 1) ? ';'  : '';
	
				$type .= $this->getRouletteNumberTypeFromBetResultDetail($three_type) . $separator .'<br/>';
				$bet_amount .= $this->amountIncentive($currency, $three_amount) . $separator .'<br/>';
				$winloss .= ($three_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $three_amount) . '</font>'. $separator .'<br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $three_win) . '</font>'. $separator .'<br/>';
			}
			if($type != ''){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
	
			// ... three number
			 
			// 7. four number
			//$raw_four_num = split("\|",$four_number); // bet_amount | bet_amount | ...
			$raw_four_num = $four_number;
			$raw_four_num_detail = split("\|",$four_number_detail);
			list($fb_bet, $fb_win) = explode("#",$raw_four_num_detail[0]);
			$type = 'Four Number';
			$bet_amount = $this->amountIncentive($currency, $four_number);
			$winloss = ($fb_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $four_number) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $fb_win) . '</font><br/>';
			 
			if($four_number > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 8. Row One
			$raw_row1_detail = split("\|",$row1_detail);
			list($r1_bet, $r1_win) = explode("#",$raw_row1_detail[0]);
			$type = 'Row One';
			$bet_amount = $this->amountIncentive($currency, $row1);
			$winloss = ($r1_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $row1) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $r1_win) . '</font><br/>';
			if($row1 > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 9. Row Two
			$raw_row2_detail = split("\|",$row2_detail);
			list($r2_bet, $r2_win) = explode("#",$raw_row2_detail[0]);
			$type = 'Row Two';
			$bet_amount = $this->amountIncentive($currency, $row2);
			$winloss = ($r2_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $row2) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $r2_win) . '</font><br/>';
			if($row2 > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 10. Row Three
			$raw_row3_detail = split("\|",$row3_detail);
			list($r3_bet, $r3_win) = explode("#",$raw_row3_detail[0]);
			$type = 'Row Three';
			$bet_amount = $this->amountIncentive($currency, $row3);
			$winloss = ($r3_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $row3) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $r3_win) . '</font><br/>';
	
			if($row3 > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 11. Column One
			$raw_col1_detail = split("\|",$col1_detail);
			list($c1_bet, $c1_win) = explode("#",$raw_col1_detail[0]);
			$type = 'Column One';
			$bet_amount = $this->amountIncentive($currency, $col1);
			$winloss = ($c1_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $col1) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $c1_win) . '</font><br/>';
			if($col1 > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 12. Column Two
			$raw_col2_detail = split("\|",$col2_detail);
			list($c2_bet, $c2_win) = explode("#",$raw_col2_detail[0]);
			$type = 'Column Two';
			$bet_amount = $this->amountIncentive($currency, $col2);
			$winloss = ($c2_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $col2) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $c2_win) . '</font><br/>';
			if($col2 > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 13. Column Three
			$raw_col3_detail = split("\|",$col3_detail);
			list($c3_bet, $c3_win) = explode("#",$raw_col3_detail[0]);
			$type = 'Column Three';
			$bet_amount = $this->amountIncentive($currency, $col3);
			$winloss = ($c3_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $col3) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $c3_win) . '</font><br/>';
			if($col3 > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 14. Red
			$raw_red_detail = split("\|",$red_detail);
			list($red_bet, $red_win) = explode("#",$raw_red_detail[0]);
			$type = 'Red';
			$bet_amount = $this->amountIncentive($currency, $red);
			$winloss = ($red_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $red) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $red_win) . '</font><br/>';
			if($red > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 15. Black
			$raw_black_detail = split("\|",$black_detail);
			list($black_bet, $black_win) = explode("#",$raw_black_detail[0]);
			$type = 'Black';
			$bet_amount = $this->amountIncentive($currency, $black);
			$winloss = ($black_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $black) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $black_win) . '</font><br/>';
			if($black > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 16. Odd
			$raw_odd_detail = split("\|",$odd_detail);
			list($odd_bet, $odd_win) = explode("#",$raw_odd_detail[0]);
			$type = 'Odd';
			$bet_amount = $this->amountIncentive($currency, $odd);
			$winloss = ($odd_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $odd) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $odd_win) . '</font><br/>';
			if($odd > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 17. Even
			$raw_even_detail = split("\|",$even_detail);
			list($even_bet, $even_win) = explode("#",$raw_even_detail[0]);
			$type = 'Even';
			$bet_amount = $this->amountIncentive($currency, $even);
			$winloss = ($even_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $even) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $even_win) . '</font><br/>';
			if($even > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 18. Small
			$raw_small_detail = split("\|",$small_detail);
			list($small_bet, $small_win) = explode("#",$raw_small_detail[0]);
			$type = 'Small';
			$bet_amount = $this->amountIncentive($currency, $small);
			$winloss = ($small_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $small) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $small_win) . '</font><br/>';
			if($small > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
			 
			// 19. Big
			$raw_big_detail = split("\|",$big_detail);
			list($big_bet, $big_win) = explode("#",$raw_big_detail[0]);
			$type = 'Big';
			$bet_amount = $this->amountIncentive($currency, $big);
			$winloss = ($big_win == 0) ? '<font color="red">-' . $this->amountIncentive($currency, $big) . '</font><br/>' : '<font color="#000c75">' . $this->amountIncentive($currency, $big_win) . '</font><br/>';
			 
			if($big > 0){
				$table_data .= '<tr><td class="c_bet_type">' . $type . '</td><td class="c_bet_amount">' . $bet_amount . '</td><td class="c_win_loss">' . $winloss . '</td></tr>';
			}
	
			//     		$bet_detail = split("\|",$bet_detail_array);
	
			//     		for($i = 0; $i <= count($bet_detail) - 1; $i++) {
			//     			if(strpos($bet_detail[$i],'#') !== false){
			//     				// initialize
			// 	    			$data_row = split("#",$bet_detail[$i]);
			// 	    			$type = $this->getRouletteNumberTypeFromBetResultDetail($data_row[0]);
			// 	    			$bet_amount = $data_row[1];
			// 	    			$winloss = ($this->getRouletteWinLossFromBetResultDetail($bet_result_detail_array, $i) == 0) ? '<font color="red">-' . $bet_amount . '</font>' : '<font color="#000c75">' . $this->getRouletteWinLossFromBetResultDetail($bet_result_detail_array, $i) . '</font>';
	
			// 	    			// return table row data
			// 	    			$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
			//     			}else{
			//     				//if($i > 0){
			// 	    				$data_row = split("\^",$bet_detail[$i]);
			// 	    				for($e = 0; $e <= count($data_row) - 1; $e++){
			// 	    					if($data_row[$e] != 0){
			// 	    						$type = $this->getRouletteTypeFromBetResultDetail($e);
			// 	    						$bet_amount = $data_row[$e];
			// 	    						$winloss = ($this->getRouletteWinLossFromBetResultDetail1($bet_result_detail_array, $i, $e) == 0) ? '<font color="red">-' . $bet_amount . '</font>' : '<font color="#000c75">' . $this->getRouletteWinLossFromBetResultDetail1($bet_result_detail_array, $i, $e) . '</font>';
				
			// 	    						// return table row data
			// 	    						$table_data .= '<tr><td class="bet_type">' . $type . '</td><td class="bet_amount">' . $bet_amount . '</td><td class="win_loss">' . $winloss . '</td></tr>';
			// 	    					}
			// 	    				}
			// //     				}else{
				
			// //     				}
			//     			}
			//     		}
	
	
		}
		 
		$table_data .= '</table>';
		return  $table_data;
	}
	public function getRouletteNumberTypeFromBetResultDetail($raw_type){
		// 0^0^112233 = 11&22&33
		$extract_type = split("\^",$raw_type);
		$type = $extract_type[count($extract_type)-1];
		$separator_number = 2;
		 
		$loop_until = (strpos($raw_type,'^') === false) ? count($extract_type)-1 : count($extract_type)-2;
		 
		for($i = 0; $i <= (strlen($type)/2)/2; $i++){
			$type = substr_replace($type,'&',$separator_number,0);
			$separator_number += 3;
		}
		$type = (substr_compare($type,"&",-1,1) == 0) ? substr($type,0,strlen($type) -1) : $type;
		$type = str_replace('&', '<font color="#c0d2fa">&</font>', $type);
		 
		return (strlen($raw_type) <= 2) ? $raw_type : $type;
	}
	
	public function amountIncentive($currency, $amount){
		$row = TableCurrency::model()->find(array('select'=>'incentive,incentive_amount','condition'=>'currency_name=:currency',
				'params'=>array(':currency'=>$currency),));
		 
		return ($row['incentive'] == 1) ? number_format($amount * $row['incentive_amount'],2,".",",") : number_format($amount,2,".",",");
	}
	
	public function getBaccaratBetTypeByBetDetailNumber($bet_detail_number){
		 
		switch ($bet_detail_number)
		{
			case 0:
				return '<font color="red">Banker</font>'; break;
			case 1:
				return '<font color="blue">Player</font>'; break;
			case 2:
				return '<font color="green">Tie</font>'; break;
			case 3:
				return '<font color="red">Banker pair</font>'; break;
			case 4:
				return '<font color="blue">Player pair</font>'; break;
			case 5:
				return '<font color="red">Big</font>'; break;
			case 6:
				return '<font color="blue">Small</font>'; break;
			default:
				return "Undefined";
		}
	}
	
	public function getDragonTigerBetTypeByBetDetailNumber($bet_detail_number){
		if($bet_detail_number == 0){
			return '<font color="red">Dragon</font>';
		}
		elseif($bet_detail_number == 1){
			return '<font color="blue">Tiger</font>';
		}
		elseif($bet_detail_number == 2){
			return '<font color="green">Tie</font>';
		}
	}
	
	public function getExportPlayerWinLossDetailSavan()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
	
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
	
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,bet_result AS result,win_loss,win_loss as winloss1,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'Savenvegas999' as casino_name,'1' as counter,ip as IP from  tbl_savan_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		else{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,bet_result AS result,win_loss,win_loss as winloss1,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'Savenvegas999' as casino_name,'1' as counter,ip as IP from  tbl_savan_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
}



