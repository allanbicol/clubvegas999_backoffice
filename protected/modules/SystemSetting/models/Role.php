<?php
/**
 * @todo	Manger user role.
 * @copyright	CE
 * @author Kimny MOUK
 * @since	2012-11-15
 */
class Role 
{

	/**
	 * @todo Get the role list
	 * @copyright CE
	 * @author Kimny MOUK
	 * @since 2012-11-15
	 *
	 */
	public function getRoleList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT `name` as user_name,`name` as user_role FROM AuthItem WHERE type=2 ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	public function getRecordCount()
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT COUNT(0) FROM AuthItem where type=2");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo get roles
	 * @author leokarl
	 * @since 2013-01-08
	 * @param string $role
	 * @param string $module
	 */
	public function getRolesForUpdate($role, $module){
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT a.name,a.type,a.description,a.data,(CASE WHEN b.parent IS NULL THEN 0 ELSE 1 END) AS s_checked 
				FROM AuthItem a LEFT JOIN AuthItemChild b ON (a.name = b.child AND b.parent='" . $role . "')
				WHERE a.type=0 AND a.name LIKE '" . $module . ".%'
				ORDER BY a.data ASC, a.description ASC");
		$rows = $command->query();
		return $rows->readAll();
	}
	
	
	public function getHomePageList(){
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM tbl_home_page_url");
		$rows = $command->query();
		$list = CHtml::listData($rows->readAll(), 'id', 'description');
		return $list;
	}
	
	/**
	 * @todo update roles
	 * @author leokarl
	 * @since 2013-01-08
	 */
	public function updateRole(){
		// check changes for logs
		$changes = ($this->isThereChanges($_POST['role_id'], $_POST['agent_roles'], $_POST['cashplayer_roles'], $_POST['banking_roles'], 
				$_POST['game_setting_roles'], $_POST['system_monitor_roles'], $_POST['system_setting_roles'])) ? 1 : 0;
		$role_id_change = ($this->isRoleIdChange($_POST['role_id'], $_POST['role_id_before'])) ? 1 : 0;
		$role_name_change = ($this->isRoleNameChange($_POST['role_id'], $_POST['role_name'])) ? 1 : 0;
		$home_page_url_id_change = ($this->getHomePageUrlId($_POST['role_id_before']) == $_POST['home_page_url_id']) ? 0 : 1;
		$role_name_before = $this->getDataAsRoleName($_POST['role_id']);
		$url_before = $this->getHomePageUrlId($_POST['role_id_before']);
		
		TableAuthItemChild::model()->deleteAll('parent="' . trim($_POST['role_id_before']) . '"');
		
		// update item name
		TableAuthItem::model()->updateAll(array('name'=>trim(strtolower($_POST['role_id'])),
				'data'=>trim($_POST['role_name']),
				'home_page_url_id'=>trim($_POST['home_page_url_id']))
				,'name="' . trim($_POST['role_id_before']) . '"');
		TableAuthAssignment::model()->updateAll(array('itemname'=>trim(strtolower($_POST['role_id']))),'itemname="' . trim($_POST['role_id_before']) . '"');
		
		// save agent_roles
		if($_POST['agent_roles'] != ''){
			for($i=0; $i<count($_POST['agent_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['agent_roles'][$i];
				$post->save();
			}
		}
		
		// save cashplayer_roles
		if($_POST['cashplayer_roles'] != ''){
			for($i=0; $i<count($_POST['cashplayer_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['cashplayer_roles'][$i];
				$post->save();
			}
		}
		
		// save banking_roles
		if($_POST['banking_roles'] != ''){
			for($i=0; $i<count($_POST['banking_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['banking_roles'][$i];
				$post->save();
			}
		}
		
		// save game_setting_roles
		if($_POST['game_setting_roles'] != ''){
			for($i=0; $i<count($_POST['game_setting_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['game_setting_roles'][$i];
				$post->save();
			}
		}
		
		// save system_monitor_roles
		if($_POST['system_monitor_roles'] != ''){
			for($i=0; $i<count($_POST['system_monitor_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['system_monitor_roles'][$i];
				$post->save();
			}
		}
		
		// save system_setting_roles		
		if($_POST['system_setting_roles'] != ''){
			for($i=0; $i<count($_POST['system_setting_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['system_setting_roles'][$i];
				$post->save();
			}
		}
		
		// save system_setting_roles
		if($_POST['security_log_roles'] != ''){
			for($i=0; $i<count($_POST['security_log_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['security_log_roles'][$i];
				$post->save();
			}
		}
		
		// save marketing_customreport_roles
		if($_POST['marketing_customreport_roles'] != ''){
			for($i=0; $i<count($_POST['marketing_customreport_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['marketing_customreport_roles'][$i];
				$post->save();
			}
		}
		
		// logs
		
		if($changes == 1 || $role_id_change == 1 || $role_name_change == 1 || $home_page_url_id_change == 1){
			$log = new TableLog();
			$log->operated_by = Yii::app()->session['account_id'];
			$log->operated_by_level = Yii::app()->session['level_name'];
			$log->operated = trim(strtolower($_POST['role_id']));
			$log->operation_time = date('Y-m-d H:i:s');
			$log->log_type_id = 21;
			$log->log_details = $this->getLogDetails($changes, $role_id_change, $role_name_change, $home_page_url_id_change, $this->getHomePage($url_before), $this->getHomePage($_POST['home_page_url_id']), $_POST['role_id'], $_POST['role_id_before'],
					$_POST['role_id'], $role_name_before);
			$log->save();
		}
		exit('completed');
	}
	
	/**
	 * @todo get log details
	 * @author leokarl
	 * @since 2013-01-09
	 * @param int $item_change
	 * @param int $role_id_change
	 * @param int $role_name_change
	 */
	public function getLogDetails($item_change, $role_id_change, $role_name_change, $home_page_url_id_change,$url_before,$url_after, $role_id, $role_id_before, $role_name, $role_name_before){
		$log_details = '';
		if($role_id_change == 1){
			$log_details = '<b>'. Yii::app()->session['level_name'] .' <label style=\"color:#7A5C00\">'. Yii::app()->session['account_id'] .
			'</label> CHANGE User Role ID from <label style=\"color:green\">'. trim(strtolower($role_id_before)) .
			'</label> to <label style=\"color:red\">'. trim(strtolower($role_id)) . '</label></b>';
		}else if($role_name_change == 1){
			$log_details = '<b>'. Yii::app()->session['level_name'] .' <label style=\"color:#7A5C00\">'. Yii::app()->session['account_id'] .
			'</label> CHANGE User Role Name from <label style=\"color:green\">'. $role_name_before .
			'</label> to <label style=\"color:red\">'. $role_name . '</label></b>';
		}else if($home_page_url_id_change == 1){
			$log_details = '<b>'. Yii::app()->session['level_name'] .' <label style=\"color:#7A5C00\">'. Yii::app()->session['account_id'] .
			'</label> CHANGE User Role (<font color="blue">' . $role_id_before . '\'s</font>) Home Page from <label style=\"color:green\">'. $url_before .
			'</label> to <label style=\"color:red\">'. $url_after . '</label></b>';
		}else if($item_change == 1){
			$log_details = '<b>'. Yii::app()->session['level_name'] .' <label style=\"color:#7A5C00\">'. Yii::app()->session['account_id'] .
			'</label> CHANGE User Role Permission(s) for <label style=\"color:green\">'. trim(strtolower($role_id_before)) . '</label></b>';
		} 
		return $log_details;
	}
	
	/**
	 * @todo create roles
	 * @author leokarl
	 * @since 2013-01-09
	 */
	public function createRole(){
		
		// add role_name to AuthItem
		$post = new TableAuthItem();
		$post->name = trim(strtolower($_POST['role_id']));
		$post->type = 2;
		$post->data = trim($_POST['role_name']);
		$post->home_page_url_id = trim($_POST['home_page_url_id']);
		$post->save();
	
		// save agent_roles
		if($_POST['agent_roles'] != ''){
			for($i=0; $i<count($_POST['agent_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['agent_roles'][$i];
				$post->save();
			}
		}
	
		// save cashplayer_roles
		if($_POST['cashplayer_roles'] != ''){
			for($i=0; $i<count($_POST['cashplayer_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['cashplayer_roles'][$i];
				$post->save();
			}
		}
	
		// save banking_roles
		if($_POST['banking_roles'] != ''){
			for($i=0; $i<count($_POST['banking_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['banking_roles'][$i];
				$post->save();
			}
		}
	
		// save game_setting_roles
		if($_POST['game_setting_roles'] != ''){
			for($i=0; $i<count($_POST['game_setting_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['game_setting_roles'][$i];
				$post->save();
			}
		}
	
		// save system_monitor_roles
		if($_POST['system_monitor_roles'] != ''){
			for($i=0; $i<count($_POST['system_monitor_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['system_monitor_roles'][$i];
				$post->save();
			}
		}
	
		// save system_setting_roles
		if($_POST['system_setting_roles'] != ''){
			for($i=0; $i<count($_POST['system_setting_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['system_setting_roles'][$i];
				$post->save();
			}
		}
	
		// save security_log_roles
		if($_POST['security_log_roles'] != ''){
			for($i=0; $i<count($_POST['security_log_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['security_log_roles'][$i];
				$post->save();
			}
		}
		// save marketing_customreport_roles
		if($_POST['marketing_customreport_roles'] != ''){
			for($i=0; $i<count($_POST['marketing_customreport_roles']); $i++){
				$post = new TableAuthItemChild();
				$post->parent = trim(strtolower($_POST['role_id']));
				$post->child = $_POST['marketing_customreport_roles'][$i];
				$post->save();
			}
		}
		// logs
		$log = new TableLog();
		$log->operated_by = Yii::app()->session['account_id'];
		$log->operated_by_level = Yii::app()->session['level_name'];
		$log->operated = trim(strtolower($_POST['role_id']));
		$log->operation_time = date('Y-m-d H:i:s');
		$log->log_type_id = 20;
		$log->log_details = '<b>'. Yii::app()->session['level_name'] .' <label style=\"color:#7A5C00\">'. Yii::app()->session['account_id'] . '</label> CREATE User Role <label style=\"color:green\">'. trim(strtolower($_POST['role_id'])) . '</label></b>';
		$log->save();
		
		exit('completed');
	}
	
	/**
	 * @todo get roles
	 * @author leokarl
	 * @since 2013-01-08
	 * @param string $role
	 * @param string $module
	 */
	public function getRolesForCreate($module){
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT * FROM AuthItem
				WHERE type=0 AND name LIKE '" . $module . ".%' ORDER BY data ASC, description ASC");
		$rows = $command->query();
		return $rows->readAll();
	}
	
	/**
	 * @todo get data as role name
	 * @author leokarl
	 * @since 2013-01-09
	 * @param string $role_name
	 */
	public function getDataAsRoleName($role_name){
		$model = TableAuthItem::model()->find(array('condition'=>'name=:name and type=2',
				'params'=>array(':name' => $role_name),)
		);
		return $model['data'];
	}
	
	/**
	 * @todo get url
	 * @author leokarl
	 * @since 2013-01-11
	 * @param int $url_id
	 */
	public function getHomePage($url_id){
		// initialize
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT description FROM tbl_home_page_url WHERE id=" . $url_id);
		$rows = $command->query();
		$url=$rows->readAll();
		return $url[0]['description'];
	}
	/**
	 * @todo get home page url
	 * @author leokarl
	 * @since 2013-01-09
	 * @param string $role_name
	 */
	public function getHomePageUrlId($role_id){
		$model = TableAuthItem::model()->find(array('condition'=>'name=:name and type=2',
				'params'=>array(':name' => $role_id),)
		);
		return $model['home_page_url_id'];
	}
	
	/**
	 * @todo count permission
	 * @author leokarl
	 * @since 2013-01-10
	 * @param string $permission
	 */
	public function isPermissionExist($permission){
		$count = TableAuthItem::model()->count(array('condition'=>'name=:name and type=0',
				'params'=>array(':name' => $permission),)
		);
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo check all permissions validity
	 * @param array $agent
	 * @param array $cashplayer
	 * @param array $banking
	 * @param array $gamesetting
	 * @param array $systemmonitor
	 * @param array $systemsetting
	 */
	public function isPermissionsValid($agent, $cashplayer, $banking, $gamesetting, $systemmonitor, $systemsetting){
		// initialize
		$invalid_permission = 0;
		$count_agent = ($agent != '') ? count($agent) : 0;
		$count_cashplayer = ($cashplayer != '') ? count($cashplayer) : 0;
		$count_banking = ($banking != '') ? count($banking) : 0;
		$count_gamesetting = ($gamesetting != '') ? count($gamesetting) : 0;
		$count_systemmonitor = ($systemmonitor != '') ? count($systemmonitor) : 0;
		$count_systemsetting = ($systemsetting != '') ? count($systemsetting) : 0;
	
		// agent permissions
		for($i=0; $i<$count_agent; $i++){
			if(!$this->isPermissionExist($agent[$i])){
				$invalid_permission += 1;
			}
		}
	
		// cashplayer permissions
		for($i=0; $i<$count_cashplayer; $i++){
			if(!$this->isPermissionExist($cashplayer[$i])){
				$invalid_permission += 1;
			}
		}
	
		// banking permissions
		for($i=0; $i<$count_banking; $i++){
			if(!$this->isPermissionExist($banking[$i])){
				$invalid_permission += 1;
			}
		}
	
		// gamesetting permissions
		for($i=0; $i<$count_gamesetting; $i++){
			if(!$this->isPermissionExist($gamesetting[$i])){
				$invalid_permission += 1;
			}
		}
	
		// systemmonitor permissions
		for($i=0; $i<$count_systemmonitor; $i++){
			if(!$this->isPermissionExist($systemmonitor[$i])){
				$invalid_permission += 1;
			}
		}
	
		// systemsetting permissions
		for($i=0; $i<$count_systemsetting; $i++){
			if(!$this->isPermissionExist($systemsetting[$i])){
				$invalid_permission += 1;
			}
		}
	
		return ($invalid_permission == 0) ? TRUE : FALSE;
	}
	/**
	 * @todo check role_id changes
	 * @author leokarl
	 * @since 2013-01-09
	 * @param string $role_id
	 * @param string $role_id_before
	 */
	public function isRoleIdChange($role_id, $role_id_before){
		return (trim($role_id) != trim($role_id_before)) ? TRUE : FALSE;
	}
	
	/**
	 * @todo check role_name changes
	 * @author leokarl
	 * @since 2013-01-09
	 * @param string $role_id
	 * @param string $role_name
	 */
	public function isRoleNameChange($role_id, $role_name){
		return (trim($this->getDataAsRoleName($role_id)) != trim($role_name)) ? TRUE : FALSE;
	}
	
	/**
	 * @todo count roles
	 * @author leokarl
	 * @since 2013-01-09
	 * @param string $role_id
	 * @param string $module
	 */
	public function countRoles($role_id, $module){
		$count = TableAuthItemChild::model()->count(array('condition'=>'parent=:parent and child like "' . $module . '.%"',
				'params'=>array(':parent' => $role_id),)
		);
		return $count;
	}
	
	/**
	 * @todo check role
	 * @author leokarl
	 * @since 2013-01-09
	 * @param string $role_id
	 * @param string $module_name
	 */
	public function isRoleForRoleIdExist($role_id,$module_name){
		$count = TableAuthItemChild::model()->count(array('condition'=>'parent=:parent and child=:child',
				'params'=>array(':parent' => $role_id,':child' => $module_name),)
		);
	
		return ($count > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo check if there are changes
	 * @author leokarl
	 * @since 2013-01-09
	 * @param string $role_id
	 * @param array $agent
	 * @param array $cashplayer
	 * @param array $banking
	 * @param array $gamesetting
	 * @param array $systemmonitor
	 * @param array $systemsetting
	 * @return boolean
	 */
	public function isThereChanges($role_id, $agent, $cashplayer, $banking, $gamesetting, $systemmonitor, $systemsetting){
	
		// initialize
		$changes = 0;
		$count_agent = ($agent != '') ? count($agent) : 0;
		$count_cashplayer = ($cashplayer != '') ? count($cashplayer) : 0;
		$count_banking = ($banking != '') ? count($banking) : 0;
		$count_gamesetting = ($gamesetting != '') ? count($gamesetting) : 0;
		$count_systemmonitor = ($systemmonitor != '') ? count($systemmonitor) : 0;
		$count_systemsetting = ($systemsetting != '') ? count($systemsetting) : 0;
	
		
		// agent roles
		if($this->countRoles(trim(strtolower($role_id)), 'agent') != $count_agent){
			$changes += 1;
		}else{
			for($i=0; $i<$count_agent; $i++){
				if(!$this->isRoleForRoleIdExist($role_id, $agent[$i])){
					$changes += 1;
				}
			}
		}
	
		// cashplayer roles
		if($this->countRoles(trim(strtolower($role_id)), 'cashPlayer') != $count_cashplayer){
			$changes += 1;
		}else{
			for($i=0; $i<$count_cashplayer; $i++){
				if(!$this->isRoleForRoleIdExist($role_id, $cashplayer[$i])){
					$changes += 1;
				}
			}
		}
	
		// banking roles
		if($this->countRoles(trim(strtolower($role_id)), 'banking') != $count_banking){
			$changes += 1;
		}else{
			for($i=0; $i<$count_banking; $i++){
				if(!$this->isRoleForRoleIdExist($role_id, $banking[$i])){
					$changes += 1;
				}
			}
		}
	
		// gamesetting roles
		if($this->countRoles(trim(strtolower($role_id)), 'gameSetting') != $count_gamesetting){
			$changes += 1;
		}else{
			for($i=0; $i<$count_gamesetting; $i++){
				if(!$this->isRoleForRoleIdExist($role_id, $gamesetting[$i])){
					$changes += 1;
				}
			}
		}
	
		// system monitor roles
		if($this->countRoles(trim(strtolower($role_id)), 'systemMonitor') != $count_systemmonitor){
			$changes += 1;
		}else{
			for($i=0; $i<$count_systemmonitor; $i++){
				if(!$this->isRoleForRoleIdExist($role_id, $systemmonitor[$i])){
					$changes += 1;
				}
			}
		}
	
		// system setting roles
		if($this->countRoles(trim(strtolower($role_id)), 'systemSetting') != $count_systemsetting){
			$changes += 1;
		}else{
			for($i=0; $i<$count_systemsetting; $i++){
				if(!$this->isRoleForRoleIdExist($role_id, $systemsetting[$i])){
					$changes += 1;
				}
			}
		}
	
		
		return ($changes > 0) ? TRUE : FALSE;
	}
	
	/**
	 * @todo disallow special characters
	 * @author leokarl
	 * @since 2012-01-09
	 * @param string $mystring
	 */
	public function isSpecialCharPresentIncludeWhiteSpace($mystring){
		if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
				&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
				&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false && strpos($mystring, ' ') === false
				&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '-') === false && strpos($mystring, '`') === false
				&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
				&& strpos($mystring, ',') === false && strpos($mystring, '.') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
				&& strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){
	
			return FALSE; // special character not present
		}else{
			return TRUE; // special character present
		}
	}
	
	/**
	 * @todo disallow special characters
	 * @author leokarl
	 * @since 2012-01-09
	 * @param string $mystring
	 */
	public function isSpecialCharPresent($mystring){
		if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '@') === false && strpos($mystring, '#') === false
				&& strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false && strpos($mystring, '&') === false
				&& strpos($mystring, '*') === false && strpos($mystring, '(') === false && strpos($mystring, ')') === false 
				&& strpos($mystring, '+') === false && strpos($mystring, '=') === false && strpos($mystring, '-') === false && strpos($mystring, '`') === false
				&& strpos($mystring, ';') === false && strpos($mystring, "'") === false && strpos($mystring, ':') === false && strpos($mystring, '"') === false
				&& strpos($mystring, ',') === false && strpos($mystring, '.') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
				&& strpos($mystring, '?') === false && strpos($mystring, '|') === false){
	
			return FALSE; // special character not present
		}else{
			return TRUE; // special character present
		}
	}
	
	/**
	 * @todo check role name if exist
	 * @author leokarl
	 * @since 2013-01-08
	 * @param string $role_name
	 */
	public function isRoleNameExist($role_name){
		$count = TableAuthItem::model()->count(array('condition'=>'name=:name and type=2',
				'params'=>array(':name' => $role_name),)
		);
		return ($count > 0) ? TRUE : FALSE;
	}
	
	public function isRoleAssigned($parent, $child){
		$count = TableAuthItemChild::model()->count(array('condition'=>'parent=:parent and child=:child',
				'params'=>array(':parent' => $parent, ':child'=>$child),)
		);
		return ($count > 0) ? TRUE : FALSE;
	}
	
	
}