<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
<link rel="stylesheet"type="text/css" href="<?php echo $this->module->assetsUrl; ?>/css/cashplayerinfo.css"/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/tableToExcel.js"></script>
<script type="text/javascript">
	//active menu color
	document.getElementById('cashplayerHeader').className="start active";
	document.getElementById('mnu_cash_player_info').className="active";
	$('.breadcrumb').html("<li><i class='icon-home'></i>Cash Player <i class='icon-angle-right'></i></li><a href='#'>Cash Player Information</a></li>");
</script>
<script type="text/javascript">
function tableCashPlayerInfo(id,accountId,accountName,testChecked,accountType,accountStatus,rowNum) { 
	var divTag = document.createElement("Table"); 
    divTag.id = 'list2'; 
    divTag.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag);

    var divTag1 = document.createElement("div"); 
    divTag1.id = 'pager2'; 
    divTag1.style.margin = "0px auto"; 
    document.getElementById("qry_result").appendChild(divTag1);

	$(document).ready(function() {
	jQuery("#list2").jqGrid({ 
		url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo/PlayerInfo&id='+accountId+'&name='+accountName+'&test='+testChecked+'&type='+accountType+'&status='+accountStatus+'&idnum='+id, 
		datatype: 'json',		
	    mtype: 'POST',
	    height: 'auto',
	    colNames: ['ID','<i class="icon-user"></i> Account ID', '<i class="icon-user"></i> Account Name','<i class="icon-info-sign"></i> Account Type','<i class="icon-info-sign"></i> Affiliate Code','<i class="icon-money"></i> Currency','<i class="icon-globe"></i> Country','<i class="icon-envelope"></i> Email','<i class="icon-hdd"></i> Phone Number','Security No.','Value Score','<i class="icon-male"></i> Sex','Date of Birth','Address','Zip Code','City','<i class="icon-dashboard"></i> Opening Acc. Date','Player Survey','<i class="icon-info-sign"></i>Status','Bank Acc. Number','Bank Acc. Name','Bank Name','Bank Branch','Skrill Email','Neteller Account'],
	    colModel: [
          {name: 'id', index: 'id', width: 50,title:false},
	      {name: 'account_id', index: 'account_id',width: 120,title:false,formatter:function (cellvalue, options, rowObject) {
			    return '<a style="text-decoration:none" href="#" onClick="getPlayerInfoById(\''+cellvalue+'\',\'\');"><label style="color:#800000;font-weight:bold;font-size:12px;cursor: pointer">' +cellvalue+ '</label></a>';       
		  }},
	      {name: 'account_name', index: 'account_name', width: 100,title:false},
	      {name: 'account_type', index: 'account_type', width: 100,title:false},
          {name: 'afftoken', index: 'afftoken', width: 100,title:false},
	      {name: 'currency_name', index: 'currency_name', width: 70,title:false},
	      {name: 'country', index: 'country', width: 120,title:false},
	      {name: 'email', index: 'email', width: 200,title:false},
	      {name: 'phone_number', index: 'phone_number', width: 100,title:false},
	      {name: 'security_num', index: 'security_num', width: 80,title:false,align:'left'},
	      {name: 'value_score', index: 'value_score', width: 70,title:false,align:'left'},	
	      {name: 'sex', index: 'sex', width: 40,title:false},
	      {name: 'dob', index: 'dob', width: 80, align:"right",title:false},
	      {name: 'address', index: 'address', width: 250,title:false},
	      {name: 'zip_code', index: 'zip_code', width: 60,title:false},
	      {name: 'city', index: 'city', width: 110,title:false},
	      {name: 'opening_acc_date', index: 'opening_acc_date', width: 130,title:false},
	      {name: 'survey_name', index: 'survey_name', width: 130,title:false},
	      {name: 'status_name', index: 'status_name', width: 50,title:false},
	      {name: 'bank_account_number', index: 'bank_account_number', width: 110,title:false},
	      {name: 'bank_account_name', index: 'bank_account_name', width: 110,title:false},
	      {name: 'bank_used', index: 'bank_used', width: 110,title:false},
	      {name: 'bank_branch', index: 'bank_branch', width: 110,title:false},
	      {name: 'skrill_email', index: 'skrill_email', width: 180,title:false},
	      {name: 'neteller_account', index: 'neteller_account', width: 110,title:false},
	      ],
	    loadComplete: function(){
	    	var grid=jQuery("#list2");
	        var i=0; 
		    for(i=0;i<=grid.getGridParam("reccount");i++)
		    {
		    	var myrow = grid.jqGrid('getRowData', i);
		    	grid.jqGrid('setCell',i,"opening_acc_date","",{background:'#f7f7c8'});
		    	grid.jqGrid('setCell',i,"survey_name","",{background:'#cfe5fa'});
			}   
	    	$("tr.jqgrow:odd").css("background", "#DDDDDC");
	    	$("tr.jqgrow:even").css("background", "#ffffff");
		},
	    rowNum: rowNum,
	    rownumbers:true,
	    rowList: [100,200,500,1000,5000,10000,99999],
	    pager: '#pager2',
	    sortname: 'opening_acc_date',
	    sortorder: 'ASC',
	    caption: 'Cash Player Information',
	    hidegrid: false,
	    loadtext: "",
	    viewrecords: true,
	});
	
	
	$('#list2').jqGrid('navGrid', '#pager2', {edit: false, add: false, del:false, search:false, refresh: false});
	jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
	       caption:"Print", 
	       onClickButton : function () { 
	          printDiv();
	       } 
	});
	
	jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
                    caption:"Export Current Page to Excel", 
                    buttonicon:"ui-icon-calculator", 
                    onClickButton: function(){ 
                    	exportToExcel();
                   }, 
                    position:"last"
                });
	jQuery("#list2").jqGrid('navButtonAdd','#pager2',{
        caption:"Export All Pages to Excel", 
        buttonicon:"ui-icon-calculator", 
        onClickButton: function(){ 
        	exportAllToExcel(accountId,accountName,testChecked,accountType,accountStatus,rowNum);
       }, 
        position:"last"
    });

	});
	
	function printDiv()
	{
	  var divToPrint=document.getElementById('gbox_list2');
	  newWin= window.open("");
	  newWin.document.write(divToPrint.innerHTML);
	  newWin.print();
	  newWin.close();
	}
		

}
</script>
<script type="text/javascript">
	function runKeyPress(e) {
	    if (e.keyCode == 13) {
	       chkBtn("Search");
	    }
	}
	
	function loadTable(id,accountId,accountName,testChecked,accountType,accountStatus,rowNum){
	    document.getElementById('qry_result').innerHTML='';
	    tableCashPlayerInfo(id,accountId,accountName,testChecked,accountType,accountStatus,rowNum);
	}

	function chkBtn(btnName)
	{
		var accountType=document.getElementById('cmbAccountType').value;
		var accountStatus=document.getElementById('cmbStatus').value;
		var num=100;
		if (btnName=="Search")
		{
			if (document.getElementById('txtViewClicked').value=='1'){
				checkBeforeLoad(num);
			}else{
				getPlayerInfoById(document.getElementById('txtId').value,document.getElementById('txtAccountId').value,document.getElementById('txtAccountName').value,accountType,accountStatus);
			}
		}
		else if (btnName=="Load")
		{
			document.getElementById('chkTestCurrency').checked=true;
			testChecked=1;

			loadTable("","All","All",testChecked,accountType,accountStatus,num);
		}
	}

	function checkBeforeLoad(rowNum)
	{
		testChecked=0;
		if (document.getElementById("chkTestCurrency").checked==true){
			testChecked=1;
		}else{
			testChecked=0;
		}
		
		if (document.getElementById('txtAccountId').value=="" && document.getElementById('txtAccountName').value=="")
		{
			var accountType=document.getElementById('cmbAccountType').value;
			var accountStatus=document.getElementById('cmbStatus').value;
			loadTable(document.getElementById('txtId').value,"All","All",testChecked,accountType,accountStatus,rowNum);
		}
		else if (document.getElementById('txtAccountId').value=="" && document.getElementById('txtAccountName').value!="")
		{
			var accountName=document.getElementById('txtAccountName').value;
			var accountType=document.getElementById('cmbAccountType').value;
			var accountStatus=document.getElementById('cmbStatus').value;
			loadTable(document.getElementById('txtId').value,"All",accountName,testChecked,accountType,accountStatus,rowNum);
		}
		else if (document.getElementById('txtAccountId').value!="" && document.getElementById('txtAccountName').value=="")
		{
			var accountId=document.getElementById('txtAccountId').value;
			var accountType=document.getElementById('cmbAccountType').value;
			var accountStatus=document.getElementById('cmbStatus').value;
			loadTable(document.getElementById('txtId').value,accountId,"All",testChecked,accountType,accountStatus,rowNum);
		}else
		{
			var accountId=document.getElementById('txtAccountId').value;
			var accountName=document.getElementById('txtAccountName').value;
			var accountType=document.getElementById('cmbAccountType').value;
			var accountStatus=document.getElementById('cmbStatus').value;
			loadTable(document.getElementById('txtId').value,accountId,accountName,testChecked,accountType,accountStatus,rowNum);
		}
	}


	function getPlayerInfoOneByOneInDetail(index,method){
		hideListView();
		
		document.getElementById('formViewFix').style.visibility="visible";
		$('#loader1').show();
		$('#loader2').show();
		$('#btnEdit').hide();
		if (method=='first'){
			document.getElementById('btnPrevious1').disabled = true;
			document.getElementById('btnFirst1').disabled = false;
			document.getElementById('btnNext1').disabled = false;
			document.getElementById('btnLast1').disabled = false;
		}else if (method=='previous'){
			if (document.getElementById('txtIndex1').value=='2'){
				document.getElementById('btnPrevious1').disabled = true;
				document.getElementById('btnFirst1').disabled = false;
				document.getElementById('btnNext1').disabled = false;
				document.getElementById('btnLast1').disabled = false;
			}
		}else if (method=='next'){
			document.getElementById('btnPrevious1').disabled = false;
			document.getElementById('btnFirst1').disabled = false;
			document.getElementById('btnNext1').disabled = false;
			document.getElementById('btnLast1').disabled = false;
		}else if (method=='last'){
			document.getElementById('btnPrevious1').disabled = false;
			document.getElementById('btnFirst1').disabled = false;
			document.getElementById('btnNext1').disabled = true;
			document.getElementById('btnLast1').disabled = false;
		}
		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo/PlayerInformation',
    		type: 'POST',
    		data: {'index_id': index,'method':method},
    		//datatype: 'json',
    		context:'',
    		success: function(data) {
        		//alert (JSON.stringify(data,""));
        		var result=data.split("##");
        		document.getElementById('txtIndex1').value=result[0];
        		document.getElementById('txtAccount_Id1').value=result[1];
        		document.getElementById('txtAccount_Name1').value=result[2];
        		document.getElementById('txtEmail1').value=result[3];
        		document.getElementById('txtPhoneNumber1').value=result[4];
        		document.getElementById('txtCurrency1').value=result[5];
        		document.getElementById('txtCountry1').value=result[6];
        		document.getElementById('txtSex1').value=result[7];
        		document.getElementById('txtDOB1').value=result[8];
        		document.getElementById('txtAddress1').value=result[9];
        		document.getElementById('txtZipCode1').value=result[10];
        		document.getElementById('txtCity1').value=result[11];
        		document.getElementById('txtODA1').value=result[12];
        		document.getElementById('txtStatus1').value=result[13];
        		document.getElementById('txtBankAccountNo1').value=result[14];
        		document.getElementById('txtBankAccountName1').value=result[15];
        		document.getElementById('txtBankName1').value=result[16];
        		document.getElementById('txtBankBranch1').value=result[17];
        		document.getElementById('txtSkrillEmail1').value=result[18];
        		document.getElementById('txtNetellerAccount1').value=result[19];
        		document.getElementById('txtSecurityNum1').value=result[20];
        		document.getElementById('txtLAD1').value=result[21];
        		document.getElementById('txtPPC1').value=result[22];
        		document.getElementById('txtVSG1').value=result[23];
        		document.getElementById('txtLifetimeTurnover1').value=result[24];
        		document.getElementById('txtTurnover20121').value=result[25];
        		document.getElementById('txtLifetimeGGR1').value=result[26];
        		document.getElementById('txtLifetimeNGR1').value=result[28];
        		document.getElementById('txtGGR20121').value=result[27];
        		document.getElementById('txtNGR20121').value=result[29];
        		document.getElementById('txtLifetimeDeposit1').value=result[30];
        		document.getElementById('txtDepositcount1').value=result[31];
        		document.getElementById('txtLifetimeWithdraw1').value=result[32];
        		document.getElementById('txtWithdrawCount1').value=result[33];
        		document.getElementById('txtLifetimeBonus1').value=result[34];
        		document.getElementById('txtBonusCount1').value=result[35];
        		$('#loader1').hide();
        		$('#loader2').hide();
        		$('#btnEdit').show();
	    	}
    	});
	}

	
	function getPlayerInfoById(account_id,account_name){
		
		var div = document.getElementById('formViewById');

			if (div!=null){
		    	div.parentNode.removeChild(div);
			}

		hideListView();
		if (document.getElementById('txtViewClicked').value=='2'){
			var div = document.getElementById('formView');

			if (div!=null){
		    	div.parentNode.removeChild(div);
			}
			document.getElementById('btnBack').style.visibility = 'hidden';
			document.getElementById('btnBack').value = '';
		}else{
			document.getElementById('btnBack').value = 'Back';
			document.getElementById('btnBack').style.visibility = 'visible';
		}
		$('input[type=text]').each(function() {
			$(this).val('');
		});
		document.getElementById('formViewFix').style.visibility="visible";
		$('#formViewFix').show();
		
		$('#btnEdit1').hide();
		$('#loader1').show();
		$('#loader2').show();
		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo/PlayerInformationById',
    		type: 'POST',
    		data: {'account_id': account_id,'account_name':account_name},
    		context:'',
    		success: function(data) {
        		var result=data.split("##");
	        		document.getElementById('txtIndex1').value=result[0];
	        		document.getElementById('txtAccount_Id1').value=result[1];
	        		document.getElementById('txtAccount_Name1').value=result[2];
	        		document.getElementById('txtEmail1').value=result[3];
	        		document.getElementById('txtPhoneNumber1').value=result[4];
	        		document.getElementById('txtCurrency1').value=result[5];
	        		document.getElementById('txtCountry1').value=result[6];
	        		document.getElementById('txtSex1').value=result[7];
	        		document.getElementById('txtDOB1').value=result[8];
	        		document.getElementById('txtAddress1').value=result[9];
	        		document.getElementById('txtZipCode1').value=result[10];
	        		document.getElementById('txtCity1').value=result[11];
	        		document.getElementById('txtODA1').value=result[12];
	        		document.getElementById('txtStatus1').value=result[13];
	        		document.getElementById('txtBankAccountNo1').value=result[14];
	        		document.getElementById('txtBankAccountName1').value=result[15];
	        		document.getElementById('txtBankName1').value=result[16];
	        		document.getElementById('txtBankBranch1').value=result[17];
	        		document.getElementById('txtSkrillEmail1').value=result[18];
	        		document.getElementById('txtNetellerAccount1').value=result[19];
	        		document.getElementById('txtSecurityNum1').value=result[20];
	        		document.getElementById('txtLAD1').value=result[21];
	        		document.getElementById('txtPPC1').value=result[22];
	        		document.getElementById('txtVSG1').value=result[23];
	        		document.getElementById('txtLifetimeTurnover1').value=result[24];
	        		document.getElementById('txtTurnover20121').value=result[25];
	        		document.getElementById('txtLifetimeGGR1').value=result[26];
	        		document.getElementById('txtLifetimeNGR1').value=result[28];
	        		document.getElementById('txtGGR20121').value=result[27];
	        		document.getElementById('txtNGR20121').value=result[29];
	        		document.getElementById('txtLifetimeDeposit1').value=result[30];
	        		document.getElementById('txtDepositcount1').value=result[31];
	        		document.getElementById('txtLifetimeWithdraw1').value=result[32];
	        		document.getElementById('txtWithdrawCount1').value=result[33];
	        		document.getElementById('txtLifetimeBonus1').value=result[34];
	        		document.getElementById('txtBonusCount1').value=result[35];
	        		$('#loader1').hide();
	        		$('#loader2').hide();
	        		$('#btnBack').show();
	        		$('#btnEdit1').show();
	    	}
    	});
	}

	function assignValueScore(account_id,valueScore){

		jQuery.ajax({
    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo/AssignValueScore',
    		type: 'POST',
    		data: {'accountId': account_id,'valueScore':valueScore},
    		context:'',
    		success: function(data) {
        		alert(data);
	    	}
    	});
	}
	function hideFormView(){
		$('input[type=text]').each(function() {
			$(this).val('');
		});
		$('#btnBack').hide();
		document.getElementById('formViewFix').style.visibility="hidden";
		$("#qry_result").show();
		$("#pager2").show();
		//$("#formView").hide();
		var div = document.getElementById('formViewById');
		if (div!=null){
	    	div.parentNode.removeChild(div);
		}
		var div = document.getElementById('formView');
		if (div!=null){
	    	div.parentNode.removeChild(div);
		}
		
	}
	function backToOriginal(){
		$('input[type=text]').each(function() {
			$(this).attr("disabled", "disabled"); 
		});
		document.getElementById('btnEdit1').value = 'Edit';
		document.getElementById('txtAccountId').disabled=false;
		document.getElementById('txtAccountName').disabled=false;
	}
	function hideListView(){
		$("#qry_result").hide();
		$("#pager2").hide();
	}
	
	function exportToExcel(){
		 var accountId=$('#txtAccountId')[0].value;
		 var accountName=$('#txtAccountName')[0].value;
		 var type=$('#cmbAccountType')[0].value;
		 var status=$('#cmbStatus')[0].value;
		 testChecked=0;
			if (document.getElementById("chkTestCurrency").checked==true){
				 testChecked=1;
			}
		 var testC	=	testChecked;

		 var table= document.getElementById('qry_result');
		 var html = table.outerHTML;
		 document.forms[1].csvBuffer.value=html;
		 document.forms[1].txtParams.value=$('.ui-paging-info')[0].innerHTML+'#'+accountId+'#'+accountName+'#'+type+'#'+status+'#'+testC;
         document.forms[1].method='POST';
         document.forms[1].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/Excel';  // send it to server which will open this contents in excel file
         document.forms[1].target='_top';
         document.forms[1].submit();
		
	}

	function exportAllToExcel(accountId,accountName,testChecked,accountType,accountStatus,rowNum){
		//document.forms[1].method='POST';
        //document.forms[1].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/ExportAllToExcel';  // send it to server which will open this contents in excel file
        //document.forms[1].target='_top';
        //document.forms[1].submit();
		//var num=99999;
		//jQuery.ajax({
    	//	url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/ExportAllToExcel',
    	//	type: 'POST',
    	//	data: '',
    	//	//async:true,
    	//	context:'',
    	//	success: function(data) {
    	//		//alert(data);
    	//		
	    //	}
    	//});

        $.ajax({
   		 url:'<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo/ExportAllPlayerInfo&id='+accountId+'&name='+accountName+'&test='+testChecked+'&type='+accountType+'&status='+accountStatus,
   		 async:true,
   		 success: function(result) {
   				var data =result.split("<BREAK>");
   				
   			 var accountId=$('#txtAccountId')[0].value;
   			 var accountName=$('#txtAccountName')[0].value;
   			 var type=$('#cmbAccountType')[0].value;
   			 var status=$('#cmbStatus')[0].value;
   			 testChecked=0;
   				if (document.getElementById("chkTestCurrency").checked==true){
   					 testChecked=1;
   				}
   			 var testC	=	testChecked;
   			 
   			 document.forms[1].csvBuffer.value=data[0];
   			 document.forms[1].txtParams.value='<b>No. of Records:</b> '+data[1]+'#'+accountId+'#'+accountName+'#'+type+'#'+status+'#'+testC;
   	         document.forms[1].method='POST';
   	         document.forms[1].action='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/Excel';  
   	         document.forms[1].target='_top';
   	         document.forms[1].submit();
   		 },
   		 error: function(XMLHttpRequest, textStatus, errorThrown) { 
   	 }});
		
	}

	function UpdateInfo(btn){
	       
		if (btn=='Edit'){
			document.getElementById('txtEmail').disabled=false;
			document.getElementById('txtSex').disabled=false;
			document.getElementById('txtAddress').disabled=false;
			document.getElementById('txtCity').disabled=false;
			document.getElementById('txtPhoneNumber').disabled=false;
			document.getElementById('txtCountry').disabled=false;
			document.getElementById('txtZipCode').disabled=false;
			document.getElementById('txtVSG').disabled=false;
			document.getElementById('btnEdit').value='Save';
			document.getElementById('btnEdit').innerHTML='Save <i class="icon-save"></i>';
		}else if (btn=='Save'){
			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo/AssignValueScore',
	    		type: 'POST',
	    		data: {'accountId':document.getElementById('txtAccount_Id').value,
		    		'valueScore':document.getElementById('txtVSG').value,
		    		'sex':document.getElementById('txtSex').value,
		    		'address':document.getElementById('txtAddress').value,
		    		'city':document.getElementById('txtCity').value,
		    		'phoneNumber':document.getElementById('txtPhoneNumber').value,
		    		'country':document.getElementById('txtCountry').value,
		    		'zipCode':document.getElementById('txtZipCode').value,
		    		'email':document.getElementById('txtEmail').value},
	    		context:'',
	    		success: function(data) {
		    		if (data=='0'){
		    			document.getElementById('txtSex').disabled=false;
		    			document.getElementById('txtAddress').disabled=false;
		    			document.getElementById('txtCity').disabled=false;
		    			document.getElementById('txtPhoneNumber').disabled=false;
		    			document.getElementById('txtCountry').disabled=false;
		    			document.getElementById('txtZipCode').disabled=false;
		    			document.getElementById('txtVSG').disabled=false;
		    			document.getElementById('txtEmail').disabled=false;
		    			document.getElementById('btnEdit').value='Save';
		    			document.getElementById('btnEdit').innerHTML='Save <i class="icon-save"></i>';
			    		alert('Value score cannot be lower than zero and higher than 100.');
		    		}else if (data=='dup'){
		    			document.getElementById('txtSex').disabled=false;
		    			document.getElementById('txtAddress').disabled=false;
		    			document.getElementById('txtCity').disabled=false;
		    			document.getElementById('txtPhoneNumber').disabled=false;
		    			document.getElementById('txtCountry').disabled=false;
		    			document.getElementById('txtZipCode').disabled=false;
		    			document.getElementById('txtVSG').disabled=false;
		    			document.getElementById('txtEmail').disabled=false;
		    			document.getElementById('btnEdit').value='Save';
		    			document.getElementById('btnEdit').innerHTML='Save <i class="icon-save"></i>';
			    		alert('This email is already used.');
			    	}else{
		        		alert(data);
		        		document.getElementById('txtSex').disabled=true;
		    			document.getElementById('txtAddress').disabled=true;
		    			document.getElementById('txtCity').disabled=true;
		    			document.getElementById('txtPhoneNumber').disabled=true;
		    			document.getElementById('txtCountry').disabled=true;
		    			document.getElementById('txtZipCode').disabled=true;
		    			document.getElementById('txtVSG').disabled=true;
		    			document.getElementById('txtEmail').disabled=true;
		    			document.getElementById('btnEdit').value='Edit';
		    			document.getElementById('btnEdit').innerHTML='Edit <i class="icon-edit"></i>';
		    			$('#list2').trigger("reloadGrid");
			    	}
		    	}
	    	});
		}
	}
    function UpdateInfo1(btn){

		if (btn=='Edit'){
			document.getElementById('txtEmail1').disabled=false;
			document.getElementById('txtSex1').disabled=false;
			document.getElementById('txtAddress1').disabled=false;
			document.getElementById('txtCity1').disabled=false;
			document.getElementById('txtPhoneNumber1').disabled=false;
			document.getElementById('txtCountry1').disabled=false;
			document.getElementById('txtZipCode1').disabled=false;
			document.getElementById('txtVSG1').disabled=false;
			document.getElementById('btnEdit1').value='Save';
			document.getElementById('btnEdit1').innerHTML='Save <i class="icon-save"></i>';
		}else if (btn=='Save'){
			jQuery.ajax({
	    		url: '<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerInfo/AssignValueScore',
	    		type: 'POST',
	    		data: {'accountId':document.getElementById('txtAccount_Id1').value,
		    		'valueScore':document.getElementById('txtVSG1').value,
		    		'sex':document.getElementById('txtSex1').value,
		    		'address':document.getElementById('txtAddress1').value,
		    		'city':document.getElementById('txtCity1').value,
		    		'phoneNumber':document.getElementById('txtPhoneNumber1').value,
		    		'country':document.getElementById('txtCountry1').value,
		    		'zipCode':document.getElementById('txtZipCode1').value,
		    		'email':document.getElementById('txtEmail1').value},
	    		context:'',
	    		success: function(data) {
		    		if (data=='0'){
		    			document.getElementById('txtSex1').disabled=false;
		    			document.getElementById('txtAddress1').disabled=false;
		    			document.getElementById('txtCity1').disabled=false;
		    			document.getElementById('txtPhoneNumber1').disabled=false;
		    			document.getElementById('txtCountry1').disabled=false;
		    			document.getElementById('txtZipCode1').disabled=false;
		    			document.getElementById('txtVSG1').disabled=false;
		    			document.getElementById('txtEmail1').disabled=false;
		    			document.getElementById('btnEdit1').value='Save';
		    			document.getElementById('btnEdit1').innerHTML='Save <i class="icon-save"></i>';
			    		alert('Value score cannot be lower than zero and higher than 100.');
		    		}else if (data=='dup'){
		    			document.getElementById('txtSex1').disabled=false;
		    			document.getElementById('txtAddress1').disabled=false;
		    			document.getElementById('txtCity1').disabled=false;
		    			document.getElementById('txtPhoneNumber1').disabled=false;
		    			document.getElementById('txtCountry1').disabled=false;
		    			document.getElementById('txtZipCode1').disabled=false;
		    			document.getElementById('txtVSG1').disabled=false;
		    			document.getElementById('txtEmail1').disabled=false;
		    			document.getElementById('btnEdit1').value='Save';
		    			document.getElementById('btnEdit1').innerHTML='Save <i class="icon-save"></i>';
			    		alert('This email is already used.');
				    }else{
		        		alert(data);
		        		document.getElementById('txtSex1').disabled=true;
		    			document.getElementById('txtAddress1').disabled=true;
		    			document.getElementById('txtCity1').disabled=true;
		    			document.getElementById('txtPhoneNumber1').disabled=true;
		    			document.getElementById('txtCountry1').disabled=true;
		    			document.getElementById('txtZipCode1').disabled=true;
		    			document.getElementById('txtVSG1').disabled=true;
		    			document.getElementById('txtEmail1').disabled=true;
		    			document.getElementById('btnEdit1').value='Edit';
		    			document.getElementById('btnEdit1').innerHTML='Edit <i class="icon-edit"></i>';
		    			$('#list2').trigger("reloadGrid");
				    }
		    	}
	    	});
		}
    }

    
</script>

<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
</style>
</head>
<body onload="$('#formViewFix').hide();chkBtn('Load');document.getElementById('txtViewClicked').value=1;$('#formViewFix').hide();">
<div  style="position: relative;left: 5px; top:5px">
<div style="width: 1095px; height: 43px;background-color: #333333;color:#FFFFFF;border-radius: 2px; position: relative; float: left;">
		<div style="position: relative; left:5px; top:5px"><label>ID: </label><input style="width: 95px" type="text" id="txtId" onkeypress="return runKeyPress(event)"></input>
			<label>Account ID: </label><input style="width: 95px" type="text" id="txtAccountId" onkeypress="return runKeyPress(event)"></input>
			<label>Account Name: </label><input style="width: 95px" type="text" id="txtAccountName" onkeypress="return runKeyPress(event)"></input>
			&nbsp;&nbsp;&nbsp;
			<label>Type: </label><select id="cmbAccountType" onkeypress="return runKeyPress(event)" onchange="chkBtn('Search');"><option value="All">All</option><option value="Cash">Cash</option><option value="Affiliate">Affiliate</option></select>&nbsp;&nbsp;&nbsp;
			<label>Status: </label><select id="cmbStatus" onkeypress="return runKeyPress(event)" onchange="chkBtn('Search');"><option value="All">All</option><option value="Active">Active</option><option value="Closed">Closed</option></select>
			&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkTestCurrency" onclick="javascript: chkBtn('Search');">Except TEST Currency&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn red" id="btnSearch" onclick="javascript: chkBtn(this.value);" value="Search">Search <i class="icon-search"></i></button>
		</div>
		
</div>

<div style="padding: 0px 0px 5px 15px;float: left;">
<div id="divListView"><button class="btn red" onclick="hideFormView();backToOriginal();document.getElementById('txtViewClicked').value=1;var div = document.getElementById('formView'); if (div!=null){div.parentNode.removeChild(div);}">List View <i class="icon-reorder"></i></button></div>
<div id="divFormView"><button class="btn red" onclick="hideFormView();backToOriginal();document.getElementById('txtViewClicked').value=2;getPlayerInfoOneByOneInDetail('0','first');chkBtn('Search');"> Form View <i class="icon-tasks"></i></button></div>
<input type="hidden" id="txtViewClicked">
</div></br>
</br>
</br>
	<div id="qry_result"></div>
	<div id="pager2"></div>
</div>
<div id="formViewMain"></div>
<div id="formViewById1"></div>

<div id="formViewFix">
<div id="setting_header">Player Details &nbsp;&nbsp;<img id="loader1" src="<?php echo Yii::app()->request->baseUrl;?>/images/loader.gif"><button class="btn red" id="btnBack" onClick="hideFormView();">Back <i class="icon-circle-arrow-left"></i></button>&nbsp;<button class="btn red" id="btnEdit1" onClick="UpdateInfo1(this.value);" value="Edit">Edit <i class="icon-edit"></i></button></div>
		<table id="setting_table" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="left">Account ID</td><td class="right"><input id="txtIndex1" type="hidden"><input id="txtAccount_Id1" type="text"  disabled="disabled" style="color:black;"></td>
			<td class="left">Account Name</td><td class="right"><input id="txtAccount_Name1" type="text"  disabled="disabled" style="color:black;"></td>
		</tr><tr>
			<td class="left">Email</td><td class="right"><input id="txtEmail1" type="text"  disabled="disabled" style="color:black;"></td>
			<td class="left">Phone Number</td><td class="right"><input id="txtPhoneNumber1" type="text"  disabled="disabled" style="color:black;"></td>
		</tr><tr>
			<td class="left">Currency</td><td class="right"><input id="txtCurrency1" type="text"  disabled="disabled" style="color:black;"></td>
			<td class="left">Country</td><td class="right"><input id="txtCountry1" type="text"  disabled="disabled" style="color:black;"></td>
		</tr><tr>
			<td class="left">Sex (M/F)</td><td class="right"><input id="txtSex1" type="text"  disabled="disabled" style="color:black;"></td>
			<td class="left">Date of Birth</td><td class="right"><input id="txtDOB1" type="text"  disabled="disabled" style="color:black;"></td>
		</tr><tr>
			<td class="left">Address</td><td class="right"><input id="txtAddress1" type="text"  disabled="disabled" style="color:black;"></td>
		 	<td class="left">Zip Code</td><td class="right"><input id="txtZipCode1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 	<td class="left">City</td><td class="right"><input id="txtCity1" type="text"  disabled="disabled" style="color:black;"></td>
		 	<td class="left">Opening Date Account</td><td class="right"><input id="txtODA1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 	<td class="left">Status</td><td class="right"><input id="txtStatus1" type="text"  disabled="disabled" style="color:black;"></td>
		 	<td class="left">Bank Account No.</td><td class="right"><input id="txtBankAccountNo1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 	<td class="left">Bank Account Name</td><td class="right"><input id="txtBankAccountName1" type="text"  disabled="disabled" style="color:black;"></td>
		 	<td class="left">Bank Name</td><td class="right"><input id="txtBankName1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 	<td class="left">Bank Branch</td><td class="right"><input id="txtBankBranch1" type="text"  disabled="disabled" style="color:black;"></td>
		 	<td class="left">Skrill Email Account</td><td class="right"><input id="txtSkrillEmail1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 	<td class="left">Neteller Account</td><td class="right"><input id="txtNetellerAccount1" type="text" disabled="disabled" style="color:black;"></td>
		 	<td class="left">Security Number</td><td class="right"><input id="txtSecurityNum1" type="text" disabled="disabled" style="color:black;"></td>
		 </tr>
		 </table>
		 <div id="setting_header">Casino Activities &nbsp;&nbsp;<img id="loader2" src="<?php echo Yii::app()->request->baseUrl;?>/images/loader.gif"></div>
		 <table id="setting_table" border="0" cellpadding="0" cellspacing="0">
		 <tr>
		 <td class="left">Last Access Date</td><td class="right"><input id="txtLAD1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Player Primary Casino</td><td class="right"><input id="txtPPC1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Value Score Gentile</td><td class="right"><input id="txtVSG1" type="text" value="0" disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Lifetime Valid Stake</td><td class="right"><input id="txtLifetimeTurnover1" type="text"  disabled="disabled" style="color:black;"></td>
		 <td class="left">Valid Stake <?php echo date("Y");?></td><td class="right"><input id="txtTurnover20121" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Lifetime win/loss (GGR)</td><td class="right"><input id="txtLifetimeGGR1" type="text"  disabled="disabled" style="color:black;"></td>
		 <td class="left">Lifetime win/loss exclude bonus (NGR)</td><td class="right"><input id="txtLifetimeNGR1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Win/loss (GGR) <?php echo date("Y");?></td><td class="right"><input id="txtGGR20121" type="text"  disabled="disabled" style="color:black;"></td>
		 <td class="left">Win/loss exclude bonus (NGR) <?php echo date("Y");?></td><td class="right"><input id="txtNGR20121" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Lifetime Deposit</td><td class="right"><input id="txtLifetimeDeposit1" type="text"  disabled="disabled" style="color:black;"></td>
		 <td class="left">Number of Deposit</td><td class="right"><input id="txtDepositcount1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Lifetime Withdraw</td><td class="right"><input id="txtLifetimeWithdraw1" type="text"  disabled="disabled" style="color:black;"></td>
		 <td class="left">Number of Withdraw</td><td class="right"><input id="txtWithdrawCount1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr><tr>
		 <td class="left">Lifetime Bonus</td><td class="right"><input id="txtLifetimeBonus1" type="text"  disabled="disabled" style="color:black;"></td>
		 <td class="left">Bonus <?php echo date("Y");?></td><td class="right"><input id="txtBonusCount1" type="text"  disabled="disabled" style="color:black;"></td>
		 </tr>
		 </table>
		 </br>
		 <div id="setting_header1"><center><div>
		 <button class="btn red" value="First" id="btnFirst1" type="button" title="First" onclick="javascript:getPlayerInfoOneByOneInDetail('0','first')"><i class="icon-fast-backward"></i></button>
		 <button class="btn red"  value="Previous" id="btnPrevious1" type="button" title="Previous" onclick="javascript:getPlayerInfoOneByOneInDetail(document.getElementById('txtIndex1').value,'previous')"><i class="icon-backward"></i></button>
		 <button class="btn red"  value="Next" id="btnNext1" type="button" title="Next" onclick="javascript:getPlayerInfoOneByOneInDetail(document.getElementById('txtIndex1').value,'next')"><i class="icon-forward"></i></button>
		 <button class="btn red"  value="Last" id="btnLast1" type="button" title="Last" onclick="javascript:getPlayerInfoOneByOneInDetail('0','last')"><i class="icon-fast-forward"></i></button>
		 </div></center></div>
</div>
</br>
<form method="post" action="<?php echo Yii::app()->request->baseUrl;?>/index.php?r=CashPlayer/CashPlayerList/Excel">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="txtParams" id="txtParams" value="" />
</form>
</body>
</html>