function validation()
{
	var count=0;
	var duration=0;
	jQuery.ajax({
		url: checkLocked,
		type: 'POST',
		data: {'accountId': document.getElementById('LoginForm_username').value,'password': document.getElementById('LoginForm_password').value},
		context:'',
		success: function(data) {
			var result=data.split("#");
			
			if (result[1]>0){
				if (parseInt(attempt_count) >=2 || result[0]==1)
				{
					$('#LoginForm_username').attr('disabled','disabled');
					$('#LoginForm_password').attr('disabled','disabled');
					$('#LoginForm_verifyCode').attr('disabled','disabled');
					$('input[type="button"]').attr('disabled','disabled');
					duration=15;
					jQuery.ajax({
			    		url: addLockedUser,
			    		type: 'POST',
			    		data: {'accountId': document.getElementById('LoginForm_username').value,'lockedDuration':duration,'extendedDuration':15,'result':result[1],'event':1},
			    		context:'',
			    		success: function(data) {
			        		count=parseInt(data);
			        		timeCountdown=parseInt(count);
			        		attempts=data;
			    			var time1=setInterval(function(){
			    				timeCountdown=parseInt(timeCountdown)-1;
			    				if (timeCountdown >=0)
			    				{
			    					document.getElementById('locked_error').innerHTML='Login failed for '+(3+((attempts-15)/15))+' attempts! Please wait '+timeCountdown+' seconds and login again.';
			    				}else{
			    					$('#LoginForm_username').removeAttr('disabled');
			    					$('#LoginForm_password').removeAttr('disabled');
			    					$('#LoginForm_verifyCode').removeAttr('disabled');
			    					$('input[type="button"]').removeAttr('disabled');
			    					document.getElementById('locked_error').innerHTML='';
			    					jQuery.ajax({
			    			    		url: removeLockedUser,
			    			    		type: 'POST',
			    			    		data: {'accountId': document.getElementById('LoginForm_username').value},
			    			    		context:'',
			    			    		success: function(data) {
			    			    			 clearTimeout(time1);
			    			    			 window.location.replace(window.location.href);
			    				    	}
			    			    	});
			    					
			    				}
			    			},1000);
				    	}
			    	});
					return false;		
				}else{
					jQuery.ajax({
			    		url: addLockedUser,
			    		type: 'POST',
			    		data: {'accountId': document.getElementById('LoginForm_username').value,'result':result[1],'event':1},
			    		context:'',
			    		success: function(data) {
			    			document.getElementById("login-form").submit() ;
				    	}
			    	});
					return false;
				}

			}else{
				if (parseInt(attempt_count) >=2 || result[0]==1)
				{
					$('#LoginForm_username').attr('disabled','disabled');
					$('#LoginForm_password').attr('disabled','disabled');
					$('#LoginForm_verifyCode').attr('disabled','disabled');
					$('input[type="button"]').attr('disabled','disabled');
					duration=15;
					jQuery.ajax({
			    		url: addLockedUser,
			    		type: 'POST',
			    		data: {'accountId': document.getElementById('LoginForm_username').value,'lockedDuration':duration,'extendedDuration':15,'result':4,'event':1},
			    		context:'',
			    		success: function(data) {
			        		count=parseInt(data);
			        		timeCountdown=parseInt(count);
			        		attempts=data;
			    			var time2=setInterval(function(){
			    				timeCountdown=parseInt(timeCountdown)-1;
			    				
			    				if (timeCountdown >=0)
			    					
			    				{		
			    					document.getElementById('locked_error').innerHTML='Login failed for '+(3+((attempts-15)/15))+' attempts! Please wait '+timeCountdown+' seconds and login again.';
			    				
			    				}else{
			    					$('#LoginForm_username').removeAttr('disabled');
			    					$('#LoginForm_password').removeAttr('disabled');
			    					$('#LoginForm_verifyCode').removeAttr('disabled');
			    					$('input[type="button"]').removeAttr('disabled');
			    					document.getElementById('locked_error').innerHTML='';
			    					jQuery.ajax({
			    			    		url: removeLockedUser,
			    			    		type: 'POST',
			    			    		data: {'accountId': document.getElementById('LoginForm_username').value},
			    			    		context:'',
			    			    		success: function(data) {
			    			    			 clearTimeout(time2);
			    			    			 window.location.replace(window.location.href);
			    			    			 
			    				    	}
			    			    	});
			    					
			    				}
			    			},1000);
				    	}
			    	});
					return false;		
				}else{
					jQuery.ajax({
			    		url: addLockedUser,
			    		type: 'POST',
			    		data: {'accountId': document.getElementById('LoginForm_username').value,'result':result[1],'event':0},
			    		context:'',
			    		success: function(data) {
			    			document.getElementById("login-form").submit();
				    	}
			    	});
					return false;
				}
			}
		}
	});
}

function loadCheck(){
	var count=0;
	var duration=0;
	if (user_that_attempt!='' || user_that_attempt!=null){
		jQuery.ajax({
			url: checkLocked,
			type: 'POST',
			data: {'accountId':user_that_attempt,'password': document.getElementById('LoginForm_password').value},
			context:'',
			success: function(data) {
				var result=data.split("#");
					if (result[0]==1 && result[1]==1)
					{
						$('#LoginForm_username').attr('disabled','disabled');
						$('#LoginForm_password').attr('disabled','disabled');
						$('#LoginForm_verifyCode').attr('disabled','disabled');
						$('input[type="button"]').attr('disabled','disabled');
						duration=0;
						jQuery.ajax({
				    		url: getDuration,
				    		type: 'POST',
				    		data: {'accountId': user_that_attempt},
				    		context:'',
				    		success: function(data) {
				        		count=parseInt(data);
				        		timeCountdown=parseInt(count);
				        		attempts=data;
				    			var time1=setInterval(function(){
				    				timeCountdown=parseInt(timeCountdown)-1;
				    				if (timeCountdown >=0)
				    				{
				    					document.getElementById('locked_error').innerHTML='Login failed for '+(3+((attempts-15)/15))+' attempts! Please wait '+timeCountdown+' seconds and login again.';
				    				}else{
				    					$('#LoginForm_username').removeAttr('disabled');
				    					$('#LoginForm_password').removeAttr('disabled');
				    					$('#LoginForm_verifyCode').removeAttr('disabled');
				    					$('input[type="button"]').removeAttr('disabled');
				    					document.getElementById('locked_error').innerHTML='';
				    					jQuery.ajax({
				    			    		url: removeLockedUser,
				    			    		type: 'POST',
				    			    		data: {'accountId': user_that_attempt},
				    			    		context:'',
				    			    		success: function(data) {
				    			    			 clearTimeout(time1);
				    			    			 window.location.replace(window.location.href);
				    				    	}
				    			    	});
				    					
				    				}
				    			},1000);
					    	}
				    	});
						return false;		
					}else{
	//					jQuery.ajax({
	//			    		url: addLockedUser,
	//			    		type: 'POST',
	//			    		data: {'accountId': user_that_attempt,'result':result[1],'event':1},
	//			    		context:'',
	//			    		success: function(data) {
	//			    			//document.getElementById("login-form").submit() ;
	//				    	}
	//			    	});
						return false;
					}
				}
		});
	}
}
