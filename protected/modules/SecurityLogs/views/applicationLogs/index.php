<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/agenttransactionhistory.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/js/jquery-ui-1.8.17/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/jqgrid/ui.jqgrid.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/i18n/grid.locale-en.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/js/jqgrid/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="<?php echo $this->module->assetsUrl; ?>/js/applicationLogsList.js"></script>
<style>
.ui-jqgrid .loading
{
    left: 45%;
    top: 45%;
    border:0;
    background: url("<?php echo Yii::app()->request->baseUrl;?>/images/320.png");
    background-position-x: 50%;
    background-position-y: 50%;
    background-repeat: no-repeat;
    height: 90px;
    width: 90px;
}
.ui-jqgrid tr.jqgrow td {
        word-wrap: break-word!important; /* IE 5.5+ and CSS3 */
        white-space: pre-wrap!important; /* CSS3 */
        white-space: -moz-pre-wrap!important; /* Mozilla, since 1999 */
        white-space: -pre-wrap!important; /* Opera 4-6 */
        white-space: -o-pre-wrap!important; /* Opera 7 */
        overflow: hidden;
        height: auto;
        vertical-align: middle;
        padding-top: 3px;
        padding-bottom: 3px
    }
</style>
</head>

<body onload="javascript: submitComplete('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/ApplicationLogs/ApplicationLogs');">
<div id="body_wraper">
	<div id="transaction_parameter_box1">
		<div class="header">APPLICATION LOGS</div>
		<div class="body">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr><td class="left">Account ID</td>
			<td class="right">
				<input id="txtAccountId" type="text" style="width: 150px;" >
			</td></tr>
			<tr><td class="left">Type</td>
			<td class="right">
				<Select name="cmbLogType" id="cmbLogType">
					<option value="0">All</option>
					<?php 
						$dataReader = TableLogType::model()->findAll();
						foreach ($dataReader as $row){
							//if ($row['id']<19){
								echo '<option value="' . $row['id'] . '">'. strtoupper($row['log_type_name']) . '</option>';
							//}
					}
					?>
				</Select>
			</td></tr>
			<tr><td class="left">Operation</td>
			<td class="right">
				<Select name="cmbOperation" id="cmbOperation">
					<option value="All">All</option>
					<option value="0">Operating</option>
					<option value="1">Operated</option>
				</Select>
			</td></tr>
			<tr><td class="left">FROM</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtFrom" name="dtFrom" class="dtText">
				<select id="cmbTime1">
				<?php 
					for($i=0;$i<=23;$i++){
						echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
					}
				?>
				</select> :00:00
			</td></tr>
			<tr><td class="left">
				TO
			</td>
			<td class="right">
				<input type="text" value="<?php echo date("Y-m-d");?>" id="dtTo" class="dtText">
				<select id="cmbTime2">
				<?php 
					for($i=0;$i<=23;$i++){
						if($i<23){
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '">' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}else{
							echo '<option value="' . str_pad($i, 2, 0, STR_PAD_LEFT) . '" selected>' . str_pad($i, 2, 0, STR_PAD_LEFT) . '</option>';
						}
					}
				?> 
				</select> :59:59
			</td>
			</tr>
			<tr>
			<td class="left"></td><td class="right"><input id="btnSubmit" class="btn red" name="" type="button" value="Submit" onclick="javascript: submitComplete('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/ApplicationLogs/ApplicationLogs');"> <input class="btn red" type="button" value="Back" onclick="javascript: history.go(-1);"></td>
			</tr>
		</table>
		
		
		</div>
		<div class="row">
		</div>
		<!--  <center><input id="btnSubmit" name="" type="button" value="Submit" onclick="javascript: submitComplete('<?php echo Yii::app()->request->baseUrl;?>/index.php?r=SecurityLogs/ApplicationLogs/ApplicationLogs');"> <input type="button" value="Back" onclick="javascript: history.go(-1);"></center>-->
		
		</div>
	<div id="qry_result">
	<div id="pager1"></div>
	</div>
</div>
</body>
</html>