<?php
class CashPlayerTransHistoryController extends MyController
{
	public function actionCashPlayerTransHistory()
	{

		$cpH = new CashPlayerTransHistory;
		$page = $_GET['page'];

		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];

		$sortType = $_GET['sord'];
		
		$result=$cpH->getCountPlayerTransHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		// calculate the starting position of the rows
		//$startIndex = $limit*$page - $limit;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		
	
		$player_records = $cpH->getPlayerTransHistory($orderField, $sortType, $startIndex, $limit);
		$filedNames = array("transNumber","player_id","full_name","transTypeName","currencyName","Amount","balance_before","balance_after","deposit_via","submitTime","finishTime","cashierID","status");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
		
			
	}
	
	public function actionCashPlayerTransHistoryforBonus()
	{
		//echo $_GET['AccountID'];
		$cpH = new CashPlayerTransHistory;
		$page = $_GET['page'];
		
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		
		$sortType = $_GET['sord'];
		
		$result=$cpH->getCountPlayerTransHistoryForBonus();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		// calculate the starting position of the rows
		
		//$startIndex = $limit*$page - $limit;
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpH->getPlayerTransHistoryForBonus($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("transNumber","player_id","full_name","cashierID","Amount","deposit_via","balance_before","balance_after","currencyName","submitTime","status");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readTransHistoryDetails'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionBonusExcel(){
		$file="CashPlayerTrans_BonusDetail".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
		
		$exportCount=str_replace("View", "<b>Exported Page</b>", $_POST['exportCount']);
		$params=explode("#", $exportCount)	;
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Transaction History Bonus Detail</label> to excel file.</b>\n'.$params[0].'\n<b>Method</b> = '.$params[1].'\n<b>Type</b> = '.$params[2].'\n<b>Status</b> = '.$params[3].'\n<b>TestCurrency</b> = '.$params[4].'\n<b>AccountId</b> ='.$params[5].'\n<b>DateFrom</b> = '.$params[6].'\n<b>DateTo</b> = '.$params[7];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionFundExcel(){
		$file="CashPlayerTrans_FundDetail".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportCount=str_replace("View", "<b>Exported Page</b>", $_POST['exportCount']);
		$params=explode("#", $exportCount)	;
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Transaction History Fund Detail</label> to excel file</b>\n'.$params[0].'\n<b>Method</b> = '.$params[1].'\n<b>Type</b> = '.$params[2].'\n<b>Status</b> = '.$params[3].'\n<b>TestCurrency</b> = '.$params[4].'\n<b>AccountId</b> ='.$params[5].'\n<b>DateFrom</b> = '.$params[6].'\n<b>DateTo</b> = '.$params[7];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionExportAllFundTransactionHistory()
	{
		$cpH = new CashPlayerTransHistory;
		$result=$cpH->getCountPlayerTransHistory();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];
		
		$player_records = $cpH->exportAllPlayerTransHistory();
		$fieldNames = array("transNumber","player_id","full_name","transTypeName","currencyName","Amount","balance_before","balance_after","deposit_via","submitTime","finishTime","cashierID","status");
		$headerNames=array("Trans Number","Player Id","Full Name","Trans Type","Currency","Amount","Balance Before","Balance After","Deposit via","Submit Time","Finished Time","Cashier","Status");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	}
	public function actionExportAllBonusTransactionHistory()
	{
		$cpH = new CashPlayerTransHistory;
		$result=$cpH->getCountPlayerTransHistoryForBonus();
		$data=$result->readAll();
		$records=$data[0]['COUNT(0)'];

		$player_records = $cpH->exportAllPlayerTransHistoryForBonus();
		$fieldNames = array("transNumber","player_id","full_name","cashierID","Amount","deposit_via","balance_before","balance_after","currencyName","submitTime","status");
		$headerNames=array("Trans Number","Player Id","Full Name","Cashier","Amount","Bonus Type","Bonus Before","Bonus After","Currency Name","Submit Time","Status");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
		
	}
}