<?php
/**
 * @todo bind tbl_Level CActiveRecord
 * @copyright CE
 * @author Allan Bicol
 * @since 2012-09-04
 */
class TableLevel extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_level';
	}
}