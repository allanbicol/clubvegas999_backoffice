<?php
// initialize
$redis= new RedisManager();
// get casino name
$casino=$redis->getCasinoNameByID($redis->getCasinoIDByPlayer($_GET['player_id']));

echo 'Player <b>' . $_GET['player_id'] . '</b> is on the ' . $casino . ' lobby. <br/>Click "Force Logout" to force logout this player first.';
echo '<br/><br/>';
echo CHtml::button('Force Logout',array('id'=>'btnForceLogOut','onclick'=>'clickForceLogOut()'));
echo CHtml::button('Cancel',array('id'=>'btnCancel','onclick'=>'$("#forcelogout-dialog").dialog("close");'));
echo '<br/><br/>';
echo '<img id="imgLogout" style="display: none; width:470px;height:20px" src="'. $this->module->assetsUrl .'/images/logout-loader.gif" >';

?>

<script>
// initialize
var urlForceLogOutPlayerOnLobby='<?php echo Yii::app()->request->baseUrl;?>/index.php?r=Banking/Payment/ForceLogOutPlayer';
var casino_id='<?php echo $redis->getCasinoIDByPlayer($_GET['player_id']);?>';
var int;

if(casino_id==1){
	var counter=5;
}else if(casino_id==2){
	var counter=10;
}else if(casino_id==3 || casino_id==4){
	var counter=60;
}

/*
 * @todo call wait message 
 * @author leokarl
 * @date 2012-12-06
 */
function clickForceLogOut(){
	
	// initialize wait message
	//$('#btnForceLogOut').val('Wait ...');

	// process force logout
	forceLogOut('<?php echo $_GET['player_id']; ?>');
	
	// start count down
	//int=self.setInterval(function(){myTimer();},1000);
	
//	// disabled all buttons
//	$("#btnForceLogOut").attr("disabled", "disabled");
//	$("#btnCancel").attr("disabled", "disabled");

//	// hide close button
//	$(".ui-dialog-titlebar-close").hide();
	$("#forcelogout-dialog").dialog("close");
}

/*
 * @todo countdown timer 
 * @author leokarl
 * @date 2012-12-06
 */
function myTimer(){
	
	// display Wait (counter)
	$('#btnForceLogOut').val('Wait (' + counter + ')');

	if(counter==0){
		// stop counter
		int=window.clearInterval(int);

		// show close button
		$(".ui-dialog-titlebar-close").show();

		alert('<?php echo $_GET['player_id'];?> has been successfully logged out from the <?php echo $casino;?> lobby.\nPlease try again!');
		// close dialog box
		$("#forcelogout-dialog").dialog("close");
	}

	// counter
	counter = counter - 1;

}

/*
 * @todo call forceLogoutOnLobbyByPlayer function
 * @author leokarl
 * @date 2012-12-06
 */
function forceLogOut(player_id){
	jQuery.ajax({ url: urlForceLogOutPlayerOnLobby, type: 'POST', data: {'player_id': player_id}, success: function(msg){}});
}
</script>