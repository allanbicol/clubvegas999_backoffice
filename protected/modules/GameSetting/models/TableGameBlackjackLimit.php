<?php
/**
 * @todo bind tbl_game_blackjack_limit to TableGameBlackjackLimit CActiveRecord
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-28
 */
class TableGameBlackjackLimit extends CActiveRecord //CV99ActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_game_blackjack_limit';
	}
}