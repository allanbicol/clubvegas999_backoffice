<script>
$(document).ready(function() {
	$("#role_name").val($("#User_role option:selected").text());
	//alert($("#User_role option:selected").text()); 
	
	
});

function selectRole()
{
	alert("hello");
}
	
</script>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
				 	'id'=>'roleDialog', 
				 	'options'=>array( 
				 			'title'=>'Update Role', 
				 			'autoOpen'=>true, 
				 			'modal'=>true,
				 			'width'=>'auto',
				 			'height'=>'auto',
				 	), 
		));

	echo CHtml::beginForm(); 
?>	
<h3>Permissions</h3>
<div class="box tabular" id="permissions">
	<?php echo CHtml::textField('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'root'));?>
    <fieldset><legend>AGENT SYSTEM</legend>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readAgentOverview'));?>
        View Agent Overview
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.addNewSubCompany'));?>
        Create new agent
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readSubCompanyList'));?>
        View Agent List
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readTransactionHistory'));?>
        View Agent Transaction
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readLog'));?>
        View Agent Log
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.writeSubCompanyList'));?>
        Update Agent Setting
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.writeSubCompanyList'));?>
        Transfer Balance
        </label>
        <br/>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readCostaWinLoss'));?>
        View Costa Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readHTVWinLoss'));?>
        View HTV Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readSavanWinLoss'));?>
        View Savan Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readVirtuaWinLoss'));?>
        View Virtua Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readHTVWinLoss'));?>
        View HTV Slot Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readSavanSlotWinLoss'));?>
        View Savan Slot Win/Loss
        </label>
        <br/>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readAgentParameters'));?>
        View Agent Parameter
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.writeAgentParameters'));?>
        Update Agent Parameter
        </label>
    </fieldset>
    
    <fieldset><legend>CASH PLAYER</legend>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCashPlayerOverview'));?>
        View Cash Player Overview
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCashPlayerList'));?>
        View Cash Player List
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCashPlayerTransHistory'));?>
        View Cash Player Transaction
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCashPlayerListLog'));?>
        View Cash Player Log
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.writeCashPlayerSetting'));?>
        View Cash Player Setting
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.writeCashPlayerSetBonus'));?>
        Set Bonus
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.writeCashPlayerDeposit'));?>
        Deposit
        </label>
        <br/>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.writeCashPlayerWithdraw'));?>
        Withdraw
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCashPlayerInfo'));?>
        View Cash Player Information
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.writeCashPlayerInfo'));?>
        Update Cash Player Information
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readVirtuaWinLoss'));?>
        View Virtua Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readHTVWinLoss'));?>
        View HTV Slot Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readSavanSlotWinLoss'));?>
        View Savan Slot Win/Loss
        </label>
        <br/>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.readAgentParameters'));?>
        View Agent Parameter
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'agent.writeAgentParameters'));?>
        Update Agent Parameter
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCostaWinLoss'));?>
        View Costa Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readHTVWinLoss'));?>
        View HTV Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readSavanWinLoss'));?>
        View Savan Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readVirtuaWinLoss'));?>
        View Virtua Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCostaSlotWinLoss'));?>
        View HTV Slot Win/Loss
        </label>
        <br/>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readSavanSlotWinLoss'));?>
        View Savan Slot Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readConsolidatedWinLoss'));?>
        View Consolidated Win/Loss
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readTransHistorySummary'));?>
        View Transaction Summary
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readTransHistoryDetails'));?>
        View Transaction Details
        </label>
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.readCashPlayerParameters'));?>
        View Cash Player Parameters
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'cashPlayer.writeCashPlayerParameters'));?>
        Update Cash Player Parameters
        </label>
    </fieldset>
    
    <fieldset><legend>AGENT SYSTEM</legend>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'banking.readPayment'));?>
        View Payment
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'banking.writePayment'));?>
        Update Payment
        </label>
    </fieldset>
    
    <fieldset><legend>GAME SETTING</legend>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'gameSetting.readTableLimit'));?>
        View Table Limit
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'gameSetting.writeTableLimit'));?>
        Add/Update Table Limit
        </label>
    </fieldset>
    
        <fieldset><legend>SYSTEM MONITOR</legend>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemMonitor.readOnlineUsers'));?>
        View Online User
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemMonitor.writeOnlineUsers'));?>
        Logout User
        </label>
    </fieldset>
    </fieldset>
    
        <fieldset><legend>SYSTEM SETTING</legend>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.readCurrencySetting'));?>
        View Currency Setting
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.writeCurrencySetting'));?>
        Update Currency Setting
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.readEmailSetting'));?>
        View Email Setting
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.writeEmailSetting'));?>
        Update Email Setting
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.readUserAccount'));?>
        View User Accounts
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.writeUserAccount'));?>
        Add/Update User Accounts
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.writeTranslation'));?>
        Update Language Translation
        </label>
        <br/>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.readWhiteList'));?>
        View White List
        </label>
        <label class="floating">
        <?php echo CHtml::checkBox('role[permissions][]',false,array('id'=>'role_permissions_','value'=>'systemSetting.writeWhiteList'));?>
        Update White List
        </label>
    </fieldset>
    
<br><a href="#" onclick="checkAll('permissions', true); return false;">Check all</a> | <a href="#" onclick="checkAll('permissions', false); return false;">Uncheck all</a>
<input id="role_permissions_" name="role[permissions][]" type="hidden" value="">
</div>
<center>
	<br/>
	<?php 
		echo CHtml::ajaxSubmitButton('Save',
					CHtml::normalizeUrl(array('updateRole','render'=>false)),
					
				//array('beforeSend' => 'js:function(XMLHttpRequest) { selectRole(); }'),
				
					array('success'=>'js: function(data) {
                        //$("div#roleDialog").dialog("close");
                    }'),array('id'=>'dialog-ajax'.uniqid(),'live'=>false)); 
	?>
	<?php echo CHtml::button('Cancel') ?>
</center>
<?php echo CHtml::endForm(); ?>


<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
