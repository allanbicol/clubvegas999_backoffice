<?php
/**
 * @todo SettlementController
 * @copyright CE
 * @author Kimny MOUK
 * @since 2013-06-10
 */
class SettlementController extends MyController
{
	public function actionPlayerStatusList()
	{
		$accountID = Yii::app()->session['account_id'];
		$model = new Settlement();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		$result=$model->getRecordCount($accountID);
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 1;
		}
		if ($page > $total_pages) $page=$total_pages;
	
		$startIndex = $limit*$page - $limit;
		$user_records=$model->getPlayerStatusList($accountID,$orderField, $sortType, $startIndex, $limit);
	
		$filedNames = array("account_id","account_name","kick_off","kick_off_time","kick_on_time");
		$htmValue='';
		echo JsonUtil::jsonEncode($user_records->readAll(),$total_pages, $page, $records,$filedNames,$htmValue,0);
	}
	
	/**
	 * @todo actionIndex
	 * @copyright CE
	 * @author Kimny MOUK
	 * @since 2013-06-10
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readSettlementReport')) {	
			$this->render("tab");
		} else {
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}

	/**
	 * 
	 */
	public function actionKickOff()
	{
		if (Yii::app()->session['level'] === 'AGT')
		{
			$deactive = 2;
			$active = 1;
			// current account
			$agentID = Yii::app()->session['account_id'];
			// model
			$agentKickOff = new AgentKickOff();
			$agentPlayer = new AgentPlayer();
			// get value for method
			$kickOffValue = $agentKickOff->getKickOffStatus($agentID);
			$kickOffStatus = $kickOffValue['@status'];
			$kickOffTime =  $kickOffValue['@kickOffTime'];
			$kickOnTime =  $kickOffValue['@kickOnTime'];
			// redis
			//$redisLobby = new RedisLobbyManager();
			$redis = new RedisManager(); // load redis libraries
			$online = new CV999OnlinePlayersYii();
			// check
			if ($kickOffStatus == $deactive) {
				// set kick of to agent
				$agentKickOff->setKickOffAgentByAgent($agentID, $active);
				$agentKickOff->setKickOffAgentPlayerByAgent($agentID, $active);
				// echo  $active;
				echo '{"value":' . $active . ', "activeTime":"' . $kickOffTime . '","deactiveTime":"' . $kickOnTime . '"}';
			} else if ($kickOffStatus == $active) {
				// get all player by agent ID
				$ap = $agentPlayer->getAgentPlayersByAgent($agentID);
				// extract
				foreach ($ap as $p) {
					// kick off from lobby
					//$redisLobby->deletePlayer('a', $p['account_id'], '*');
					$online->deleteLobbyByPlayer($p['account_id']);
				}
				// set kick of to agent
				$agentKickOff->setKickOffAgentByAgent($agentID, $deactive);
				$agentKickOff->setKickOffAgentPlayerByAgent($agentID, $deactive);
				// output back
				// echo $deactive;
				echo '{"value":' . $deactive . ', "activeTime":"' . $kickOffTime . '","deactiveTime":"' . $kickOnTime . '"}';
			}
		}
	}

	public function actionSettlementSubCompany()
	{
		$cpHfund = new Settlement();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountAgentSubCompanyList();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentSubCompanyList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("account_id","account_name","currency_name","balance_to_from","credit_assigned","credit","balance");
		$urlAS= Yii::app()->request->baseUrl;
		$htmvalue='';

		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,12);
	
	}
	
	public function actionSettlementSeniorMaster()
	{
		$cpHfund = new Settlement();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountAgentSeniorMasterList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentSeniorMasterList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_name","currency_name","balance_to_from","credit_assigned","credit","balance");
		$urlAS= Yii::app()->request->baseUrl;
		$htmvalue='';
	
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,12);
	
	}
	
	public function actionSettlementMaster()
	{
		$cpHfund = new Settlement();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountAgentMasterList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentMasterList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_name","currency_name","balance_to_from","credit_assigned","credit","balance");
		$urlAS= Yii::app()->request->baseUrl;
		$htmvalue='';
	
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,12);
	
	}
	public function actionSettlementAgent()
	{
		$cpHfund = new Settlement();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountAgentList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_name","currency_name","balance_to_from","credit_assigned","credit","balance");
		$urlAS= Yii::app()->request->baseUrl;
		$htmvalue='';
	
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,12);
	
	}
	
	public function actionSettlementAgentPlayer()
	{
		$cpHfund = new Settlement();
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountAgentPlayerList();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentPlayerList($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_name","currency_name","balance_to_from","credit","balance");
		$urlAS= Yii::app()->request->baseUrl;
		$htmvalue='';
	
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,12);
	
	}
	
	public function actionAgentTransferAllProcess()
	{
		$cpd = new Settlement();
		$cpd->agentTransferAll();
	}
	public function actionAgentPlayerTransferAllProcess()
	{
		$cpd = new Settlement();
		$cpd->agentPlayerTransferAll();
	}
}