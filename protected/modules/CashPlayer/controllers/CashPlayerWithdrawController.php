<?php


class CashPlayerWithdrawController extends MyController
{
	public function actionProcessWithdrawalRequest()
	{
// 		$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
// 		$rd=$command->queryRow();
// 		if ($rd['deposit_withdrawal'] == 1){
// 			if ($rd['casino_id']==1 || $rd['casino_id']==2 || $rd['casino_id']==4 || $rd['casino_id']==6){
// 				$checkBalanceIfLock = new RedisLobbyManager();
// 				$result=$checkBalanceIfLock->withdrawFromCasinoLobby($_POST['accountID']);
// 				//if there's a problem in HTV and SAVAN
// 				if ($result['errorCode'] == 1 && $result['casino'] === 'HTV') // error
// 				{
// 					//to display message that there is an error during lobby withdraw ih HTV
// 					echo 'h';
// 				}
// 				else if ($result['errorCode'] == 1 && $result['casino'] === 'SAVAN') // error
// 				{
// 					//to display message that there is an error during lobby withdraw ih SAVAN
// 					echo 's';
// 				}
// 				else if ($result['errorCode'] == 1 && $result['casino'] === 'VIRTUALCASINO') // error
// 				{
// 					//to display message that there is an error during lobby withdraw ih SAVAN
// 					echo 'vc';
// 				}
// 				else if ($result['errorCode'] == 1 && $result['casino'] === 'SLOTVEGAS') // error
// 				{
// 					//to display message that there is an error during lobby withdraw ih SAVAN
// 					echo 'svs';
// 				}
// 			}
// 			else // balance stuck on Costavegas & VirtuaVegas
// 			{ 
// 				echo '1';
// 			}
// 		}
		//$withdrawal = new RedisLobbyManager();
		//$withdrawal->ProcessWithdrawalRequest($_POST['accountID']);
		
	}
	
	public function actionWithdraw()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.writeCashPlayerWithdraw'))
		{
			$connection = Yii::app()->db_cv999_fd_master;
			if (isset($_POST['task'])==''){
				exit;
			}else{
				if($_POST['task']=='confirmWithdraw'){
					
					//$checkLobby = new RedisManager();
					$model = new CashPlayerBonus();
					if(!isset($_POST['accountIDWithdraw'])){
						exit('invalid_submit_method');
					}
					if(!isset($_POST['AmountWithdraw'])){
						exit('invalid_submit_method');
					}
					if($model->isSpecialCharPresent($_POST['AmountWithdraw'])){
						exit('invalid_amount');
					}
					if(!is_numeric($_POST['AmountWithdraw'])){
						exit('invalid_amount');
					}
					if(!$model->isAccountIdExist($_POST['accountIDWithdraw'])){
						exit('acount_doesnt_exist');
					}
					if(!isset($_POST['txtTransItem']) || trim($_POST['txtTransItem'])==''){
						exit('Invalid withdrawal type.');
					}
//					if ($checkLobby->isExistingOnLobbyByPlayer($_POST['accountIDWithdraw']) <> 0){
//						exit('player_on_the_lobby');
//					}
					if ($_POST['AmountWithdraw']<=0){
						exit("Invalid value entered.") ;
					}else{
					 		$dateTime = date('Y-m-d H:i:s');
	 				 		$transNum= TransactionNumber::generateTransactionNumber("MW");
					 		$AccountID = Yii::app()->session['account_id'];
					 		$level=Yii::app()->session['level_name'];
					 		
					 		$command = Yii::app()->db->createCommand("SELECT currency_id from tbl_cash_player where account_id='".$_POST['accountIDWithdraw']."'");
							$rd=$command->queryRow();
					 		
					 		$command = $connection->createCommand("CALL spUpdateCashPlayerWithdraw('". $_POST['accountIDWithdraw'] . "','".$AccountID."','".$_POST['AmountWithdraw']."','".$rd['currency_id']."','".$transNum."','".$_POST['txtTransItem']."','".$level."',@msg);");
					 		$command->execute();
					 		
					 		$val=1;
					 		$unlock=TableCashPlayer::model()->updateAll(array(
					 				'kick_off'=>$val),
					 				'account_id="'.$_POST['accountIDWithdraw'].'"');
					 		
					 		$command = $connection->createCommand("SELECT @msg as msg");
					 		$rd=$command->queryRow();
							echo $rd['msg'];
						}
					}
				}

				if($_POST['task']=='checkPlayerOnlineStat')
				{
					if (Yii::app()->session['account_id']!=null){
							
						//$redis=new RedisManager();
						$checkLobby=0;//$redis->isExistingOnLobbyByPlayer($_POST['accountID']);//($_POST['AccountID']);
							
						//$casinoId=$redis->getCasinoIDByPlayer($_POST['accountID']);
							
				
						if ($checkLobby!=0){
							echo $casinoId;
						}else{
							$command = Yii::app()->db->createCommand("SELECT casino_id,deposit_withdrawal FROM tbl_player_wallet_transfer where account_id='".$_POST["accountID"]."' and deposit_withdrawal<>0");
							$rd=$command->queryRow();
							if ($rd['deposit_withdrawal'] == 1){ // display waiting message for calling withdrawal API.
								echo'w';
							}else {
								
								TableCashPlayer::model()->updateAll(array(
								'kick_off'=>3),
								'account_id="'.$_POST["accountID"].'"');
								
								//display withdrawal dialog 
								echo '0';
							}
						}
					}else{
						echo 'die';
					}
				}
			}
		}
	
	
	/*
	 * @todo check player if online on the lobby
	 * @author leokarl
	 * @date 2012-12-07
	 */
	public function actionCheckPlayerOnTheLobby(){
		//$redis = new RedisManager();
		//echo $redis->isExistingOnLobbyByPlayer($_POST["accountIDWithdraw"]);
	}

}

