<?php

class CashPlayerWinLossDetailHTV
{

	public function getPlayerWinLossDetailHTV($orderField, $sortType, $startIndex, $limit)
	{

		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,win_loss,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'HTV999' as casino_name,'1' as counter from  tbl_htv_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id  where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'  order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		else{
			$command = $connection->createCommand("Select CH.id,CONCAT(ttm.table_name,'|',shoe_id,'|',game_id) AS table_shoe_game,bet_time,account_id,(Select CP.account_name from tbl_cash_player as CP where CH.account_id = CP.account_id ) as account_name,currency,(Select TG.game_name from tbl_game as TG where TG.id=CH.game_type) AS game_type,available_bet,bet_amount,win_loss,commission,0 as amount_tips,(win_loss + commission) as total_win_loss,balance_before,balance_after,'HTV999' as casino_name,'1' as counter  from  tbl_htv_casino_history AS CH inner join tbl_table_management ttm on ttm.id=CH.table_id where  account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' order by $orderField $sortType LIMIT $startIndex , $limit"  );
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountPlayerWinLossDetailHTV()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_GET['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_GET['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_GET['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_GET['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_GET['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_GET['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
	if ($_GET['methodId']=="ALL")
		{
			$command = $connection->createCommand("Select COUNT(0) from tbl_htv_casino_history where   account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."' "  );
		}
		else{
			$command = $connection->createCommand("Select COUNT(0) from tbl_htv_casino_history where   account_id='".$_GET['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  game_type='".$_GET['methodId']."' and bet_time between '".$_GET['dateFrom']."' and '".$_GET['dateTo']."'"  );
		}
		$rows = $command->query();
		return $rows;
	
	
	}
	
	public function getPlayerWinLossDetailHTVAllPages()
	{
	
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($_POST['tableId']<>""){
			$andTableId="and table_id="."'".htmlEntities($_POST['tableId'],ENT_QUOTES)."'";;
		}else{ $andTableId='';
		}
		if ($_POST['gameId']<>""){
			$andGameId="and game_id="."'".htmlEntities($_POST['gameId'],ENT_QUOTES)."'";;
		}else{ $andGameId='';
		}
		if ($_POST['shoesId']<>""){
			$andShoesId="and shoe_id="."'".htmlEntities($_POST['shoesId'],ENT_QUOTES)."'";;
		}else{ $andShoesId='';
		}
		
		if ($_POST['methodId']=="ALL")
		{
			$command = $connection->createCommand("SELECT COUNT(id) AS bet_count,FORMAT(SUM(available_bet),2) AS available_bet,FORMAT(SUM(bet_amount),2) AS bet_amount,FORMAT(IFNULL(SUM(win_loss),0),2) AS win_loss,FORMAT(IFNULL(SUM(commission),0),2) AS commission,FORMAT((SUM(win_loss) + SUM(commission)),2) AS total_win_loss,FORMAT((SELECT total_balance_after FROM tbl_htv_casino_history AS CH1 WHERE CH1.id=MAX(CH.id)),2) AS current_balance from  tbl_htv_casino_history AS CH where  account_id='".$_POST['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and  bet_time between '".$_POST['dateFrom']."' and '".$_POST['dateTo']."'"  );
		}
		else{
			$command = $connection->createCommand("SELECT COUNT(id) AS bet_count,FORMAT(SUM(available_bet),2) AS available_bet,FORMAT(SUM(bet_amount),2) AS bet_amount,FORMAT(IFNULL(SUM(win_loss),0),2) AS win_loss,FORMAT(IFNULL(SUM(commission),0),2) AS commission,FORMAT((SUM(win_loss) + SUM(commission)),2) AS total_win_loss,FORMAT((SELECT total_balance_after FROM tbl_htv_casino_history AS CH1 WHERE CH1.id=MAX(CH.id)),2) AS current_balance  from  tbl_htv_casino_history AS CH where  account_id='".$_POST['accountId']."' ".$andTableId." ".$andGameId." ".$andShoesId." and game_type='".$_POST['methodId']."' and bet_time between '".$_POST['dateFrom']."' and '".$_POST['dateTo']."'"  );
		}
		
		$rows = $command->queryRow();
		$betCount= $rows['bet_count'];
		$validBet= $rows['available_bet'];
		$betAmount= $rows['bet_amount'];
		$winloss= $rows['win_loss'];
		$commission= $rows['commission'];
		$total= $rows['total_win_loss'];
		$currentBalance=$rows['current_balance'];
		
		echo $betCount.'#'.$validBet.'#'.$betAmount.'#'.$winloss.'#'.$commission.'#'.$total.'#'.$currentBalance;
	
	
	}
	
	
}



