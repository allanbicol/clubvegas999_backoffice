<?php
class CashPlayerTransHistoryBonusDetailController extends MyController
{
	public function actionCashPlayerTransHistoryBonusDetail()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerTransHistoryBonusDetail;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountPlayerTransHistoryBonusDetail();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerTransHistoryBonusDetail($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		
		$filedNames = array("transNumber","transStatus","currencyName","Amount","trans_via","balance_before","balance_after","submitTime","finishTime","cashierID","status");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerTotalTransHistoryBonusDetail()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerTransHistoryBonusDetail;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountTotalPlayerTransHistoryBonusDetail();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(0)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getTotalPlayerTransHistoryBonusDetail();
		//$test = $records->readAll();
		$filedNames = array("currency_name","trans_number","deposit_amount","withdrawal_amount","Total");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), 1, 1, $records,$filedNames,$htmvalue,6);
	
			
	}
	public function actionGetBonusItem()
	{
		$htmlCode='';
		$dataReader = TableTransactionItem::model()->findAll(array('condition'=>'item_type=:item_type',
				'params'=>array(':item_type'=>6),'order'=>'transaction_item'));
		foreach ($dataReader as $row){
			$htmlCode.= $row['transaction_item'] . ':' . $row['id'] . ',';
		}
		echo $htmlCode;
	}
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readTransHistoryDetails')){
			$this->render("index");
		}else{
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
		}
	}
	
	
}