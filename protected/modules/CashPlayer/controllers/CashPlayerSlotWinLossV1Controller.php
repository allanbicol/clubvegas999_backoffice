<?php
class CashPlayerSlotWinLossV1Controller extends MyController
{
	//for Costa
	public function actionCashPlayerSlotWinLossVirtua()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossV1;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerSlotWinLossVirtua();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(*)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerSlotWinLossVirtua($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_id","account_name","last_bet_date","currency_name","amount_wager","avg_bet","win_los","commission","amount_tips","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerSlotWinLossVirtuaTotal()
	{
	
		$cpHfund = new CashPlayerSlotWinLossV1;
		$player_records = $cpHfund->getPlayerSlotWinLossVirtuaTotal();
		//$test = $records->readAll();
	
		$filedNames = array("currency_name","amount_wager","win_los","commission","amount_tips","total_win_los","vig_winloss","vig_tips","vig_total","p_l","rate","amount_wager","win_los","commission","amount_tips","total_win_los","vig_winloss","vig_tips","vig_total","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), 0, 0, 0,$filedNames,$htmvalue,6);
	
	}
	
	//for HTV 
	public function actionCashPlayerSlotWinLossHTV()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossV1;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerslotWinLossHTV();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(*)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerSlotWinLossHTV($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_id","account_name","currency","bet_count","total_stake","avg_bet","win_los","commission","jackpot","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerSlotWinLossHTVTotal()
	{

		$cpHfund = new CashPlayerSlotWinLossV1;
		$player_records = $cpHfund->getPlayerSlotWinLossHTVTotal();

		$filedNames = array("currency","bet_count","total_stake","win_los","commission","jackpot","total_win_los","p_l","rate","total_stake","win_los","commission","jackpot","total_win_los","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
	
	//For savan vegas
	public function actionCashPlayerSlotWinLossSAVAN()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossV1;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerSlotWinLossSAVAN();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(*)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerSlotWinLossSAVAN($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_id","account_name","currency","bet_count","total_stake","avg_bet","win_los","commission","jackpot","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerSlotWinLossSAVANTotal()
	{

		$cpHfund = new CashPlayerSlotWinLossV1;
		$player_records = $cpHfund->getPlayerSlotWinLossSAVANTotal();

		$filedNames = array("currency","bet_count","total_stake","win_los","commission","jackpot","total_win_los","p_l","rate","total_stake","win_los","commission","jackpot","total_win_los","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
	
	//For Slotvegas/ new savan vegas
	public function actionCashPlayerNewSlotWinLossSAVAN()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossV1;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
	
		$result=$cpHfund->getCountPlayerNewSlotWinLossSAVAN();
		$data=$result->readAll();
	
		$records=$data[0]['COUNT(*)'];
	
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
	
		if ($page > $total_pages) $page=$total_pages;
	
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getPlayerNewSlotWinLossSAVAN($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
	
		$filedNames = array("account_id","account_id","account_name","currency","bet_count","total_stake","avg_bet","win_los","commission","jackpot","total_win_los","current_balance","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	public function actionCashPlayerNewSlotWinLossSAVANTotal()
	{
	
		$cpHfund = new CashPlayerSlotWinLossV1;
		$player_records = $cpHfund->getPlayerNewSlotWinLossSAVANTotal();
	
		$filedNames = array("currency","bet_count","total_stake","win_los","commission","jackpot","total_win_los","p_l","rate","total_stake","win_los","commission","jackpot","total_win_los","p_l");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(),0, 0, 0,$filedNames,$htmvalue,6);
	
			
	}
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('cashPlayer.readSlotWinLoss'))
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	public function actionVirtuaVegasSlotWinLossExcel(){
		$file="CashPlayerVirtuaVegas_SlotWinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
		
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($params[2] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';}
		
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Slot Winloss for Costavegas</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> = '.$params[1].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[3].'\n<b>Test Currency</b> = '.$params[4].'\n<b>Date From</b> = '.$params[5].'\n<b>Date To</b> = '.$params[6];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionHTVSlotWinLossExcel(){
		$file="CashPlayerHTV_SlotWinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($params[2] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';
		}
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Slot Winloss for HTV</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> = '.$params[1].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[3].'\n<b>Test Currency</b> = '.$params[4].'\n<b>Date From</b> = '.$params[5].'\n<b>Date To</b> = '.$params[6];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	public function actionSavanSlotWinLossExcel(){
		$file="CashPlayerSavan_SlotWinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
		
		$connection = Yii::app()->db_cv999_fd_master;
		
		if ($params[2] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';
		}
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Slot Winloss for Savanvegas</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> = '.$params[1].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[3].'\n<b>Test Currency</b> = '.$params[4].'\n<b>Date From</b> = '.$params[5].'\n<b>Date To</b> = '.$params[6];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	
	public function actionSavanNewSlotWinLossExcel(){
		$file="CashPlayerSavan_SlotVegasWinLoss".date('Y-m-d').".xls";
		$test="TheTableMarkupWillGoHere";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		$buffer = $_POST['csvBuffer'];
	
		$exportParams=str_replace("View", "<b>Exported Page</b>", $_POST['txtParams']);
		$params=explode("#", $exportParams)	;
	
		$connection = Yii::app()->db_cv999_fd_master;
	
		if ($params[2] != 'All'){
			$command = $connection->createCommand("SELECT game_name from tbl_game  WHERE id='" . $params[2] . "'");
			$rd=$command->queryRow();
			$game=$rd['game_name'];
		}else{ $game ='ALL';
		}
		try{
			echo $buffer;
			$postLog = new TableLog;
			$postLog->operated_by = Yii::app()->session['account_id'];
			$postLog->operated_by_level = Yii::app()->session['level_name'];
			$postLog->operated ='Cash Player';
			$postLog->operated_level = 'Clubvegas999';
			$postLog->operation_time = date('Y-m-d H:i:s');
			$postLog->log_type_id = 23;
			$postLog->log_details = '<b>'.Yii::app()->session['level_name'].' <label style=\"color:#7A5C00\">'.Yii::app()->session['account_id'].'</label> Export <label style=\"color:red\">Cash Player Slot Winloss for Savanvegas</label> to excel file</b>\n'.$params[0].'\n<b>Account Id</b> = '.$params[1].'\n<b>Game Type</b> = '.$game.'\n<b>Currency</b> = '.$params[3].'\n<b>Test Currency</b> = '.$params[4].'\n<b>Date From</b> = '.$params[5].'\n<b>Date To</b> = '.$params[6];
			$postLog->save();
		}catch(Exception $e){
	
		}
	}
	
	public function actionExportAllCashPlayerSlotWinLossVirtua()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossV1;
		$result=$cpHfund->getCountPlayerSlotWinLossVirtua();
		$data=$result->readAll();
		$records=$data[0]['COUNT(*)'];
	
		$player_records = $cpHfund->getExportPlayerSlotWinLossVirtua();

		$fieldNames = array("account_id","account_name","last_bet_date","currency_name","amount_wager","avg_bet","win_los","commission","total_win_los","current_balance","p_l");
		$headerNames=array("Account Id","Account Name","Last Bet Date","Currency","Valid Stake","Average Bet","Winloss","Commission","Total","Balance","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerSlotWinLossVirtuaTotal();

		$fieldNames = array("currency_name","amount_wager","win_los","commission","total_win_los","vig_winloss","vig_total","p_l");
		$headerNames=array("Currency","Valid Stake","Winloss","Commission","Total","VIG Winloss","VIG Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
	
	public function actionExportAllCashPlayerSlotWinLossSAVAN()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossV1;
		$result=$cpHfund->getCountPlayerSlotWinLossSAVAN();
		$data=$result->readAll();
		$records=$data[0]['COUNT(*)'];
	
		$player_records = $cpHfund->getExportPlayerSlotWinLossSAVAN();
	
		$fieldNames = array("account_id","account_name","currency","bet_count","total_stake","avg_bet","win_los","commission","jackpot","total_win_los","current_balance","p_l");
		$headerNames=array("Account Id","Account Name","Currency","Bet Count","Total Stake","Average Bet","Winloss","Commission","Jackpot","Total","Balance","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerSlotWinLossSAVANTotal();
		
		$fieldNames = array("currency","bet_count","total_stake","win_los","commission","jackpot","total_win_los","p_l");
		$headerNames=array("Currency","Bet Count","Total Stake","Winloss","Commission","Jackpot","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
	
	public function actionExportAllCashPlayerNewSlotWinLossSAVAN()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new CashPlayerSlotWinLossV1;
		$result=$cpHfund->getCountPlayerNewSlotWinLossSAVAN();
		$data=$result->readAll();
		$records=$data[0]['COUNT(*)'];
	
		$player_records = $cpHfund->getExportPlayerNewSlotWinLossSAVAN();
	
		$fieldNames = array("account_id","account_name","currency","bet_count","total_stake","avg_bet","win_los","commission","jackpot","total_win_los","current_balance","p_l");
		$headerNames=array("Account Id","Account Name","Currency","Bet Count","Total Stake","Average Bet","Winloss","Commission","Jackpot","Total","Balance","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,$records);
	
		$player_records = $cpHfund->getPlayerNewSlotWinLossSAVANTotal();
	
		$fieldNames = array("currency","bet_count","total_stake","win_los","commission","jackpot","total_win_los","p_l");
		$headerNames=array("Currency","Bet Count","Total Stake","Winloss","Commission","Jackpot","Total","P/L");
		echo JsonUtil::jsonJqgridDataExport($player_records->readAll(),$fieldNames,$headerNames,'0');
	}
}