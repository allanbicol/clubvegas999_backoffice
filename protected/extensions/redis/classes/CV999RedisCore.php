<?php
/**
 * @TODO store all variables CV999
 * @copyright Creative Entertainment Ltd.
 * @author sitthykun
 * @since 2012-04-04
 */

class CV999RedisCore implements ICV999RedisStaff {
	private $key;
	private $name;
	private $expire;
	protected $redis;

	// Location for overloaded data
	private $dynamicProperty = array();

	// Instance object
	public static $instance = NULL;

	/**
	 * 
	 */
	function CV999RedisCore () {
		$redis = new Constants();
		$single_server = array(
				'host'     => $redis->redisServerIPAddress,
				'port'     => $redis->redisPort,
				'password' => $redis->redisPassword,
				'database' => 7,
		);

		$this->redis = new Predis\Client($single_server);
	}

	/**
	 *
	 * @param string $key
	 * @param integer $expiration
	 */
	protected function add($value, $expiration) {
		// add site code = 2
		$this->redis->set($this->key, $value);
		$this->redis->expire($this->key, $expiration);
	}

	/**
	 *
	 */
	protected function delete() {
		$this->redis->del($this->key);
	}

	/**
	 * 
	 */
	protected function get() {
		$this->redis->get($this->key);
	}
	
	/**
	 * 
	 */
	public static function getInstance() {
		//  
		if(!isset(self::$instance)) {
			self::$instance = new CV999RedisCore();
		}

		return self::$instance;
	}

	/**
	 *
	 * @param string $name
	 * @param object $arguments
	 */
	public function __call($name, $arguments) {
		// Note: value of $name is case sensitive.
		// echo "Calling object method '$name' " . implode(', ', $arguments). "\n";
	}
	
	/**
	 *
	 * @param string $name
	 * @param object $arguments
	 */
	public static function __callStatic($name, $arguments) {
		// Note: value of $name is case sensitive.
		// echo "Calling static method '$name' " . implode(', ', $arguments). "\n";
	}

	/**
	 * 
	 * @param string $name
	 */
	public function __get($name) {
		// echo "Getting '$name'\n";
		if (array_key_exists($name, $this->dynamicProperty)) {
			return $this->dynamicProperty[$name];
		}

		$trace = debug_backtrace();
		trigger_error('Undefined property via __get(): ' . $name . ' in ' . $trace[0]['file'] . ' on line ' . $trace[0]['line'], E_USER_NOTICE);

		return null;
	}

	/**
	 *
	 * @param string $name
	 */
	public function __isset($name) {
		// echo "Is '$name' set?\n";
		return isset($this->dynamicProperty[$name]);
	}

	/**
	 *
	 * @param string $name
	 * @param string $value
	 */
	public function __set($name, $value) {
		// echo "Setting '$name' to '$value'\n";
		$this->dynamicProperty[$name] = $value;
	}

	/**
	 * 
	 * @param string $name
	 */
	public function __unset($name) {
		//echo "Unsetting '$name'\n";
		unset($this->dynamicProperty[$name]);
	}

	/**
	 * @return string
	 */
	protected function getKey() {
		return $this->key;
	}

	/**
	 * @return integer
	 */
	protected function getExpiration() {
		return $this->expire;
	}

	/**
	 *
	 * @param integer $expiration
	 */
	protected function setExpiration($expiration = NULL) {
		$this->expire = $expiration;
	}

	/**
	 * 
	 * @param string $key
	 */
	protected function setKey($key) {
		$this->key = $key;
	}

	/**
	 * 
	 * @param String $name
	 */
	protected function setName($name) {
		$this->name = $name;
	}
}

?>
