<?php

class AgentFundTransferHistory
{

	public function getAgentFundTransferHistory($orderField, $sortType, $startIndex, $limit)
	{
		if (Yii::app()->session['account_type']=='agent'){
			$accountID="and tt.account_id like "."'".Yii::app()->session['account_id']."%'";
		}else{
			$accountID='';
		}
		$testCurrency = ($_GET['test']!=0) ? ' AND tc.currency_name <>"TEST"' : '';

		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['currencyId']=="ALL")
		{
			$command = $connection->createCommand("SELECT '' as counter,tt.account_id,tcp.account_name,tc.currency_name FROM tbl_casino_balance_transmitter tt 
									LEFT JOIN tbl_cash_player tcp ON tt.account_id=tcp.account_id
									LEFT JOIN tbl_currency tc ON tt.currency_id=tc.id 
									WHERE  tt.account_type='a' $accountID $testCurrency AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY tt.account_id order by $orderField $sortType LIMIT $startIndex , $limit");	
		}
		else{
			$command = $connection->createCommand("SELECT '' as counter,tt.account_id,tcp.account_name,tc.currency_name FROM tbl_casino_balance_transmitter tt
					LEFT JOIN tbl_cash_player tcp ON tt.account_id=tcp.account_id
					LEFT JOIN tbl_currency tc ON tt.currency_id=tc.id
					WHERE  tt.account_type='a' $accountID $testCurrency AND tt.currency_id='".$_GET['currencyId']."' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY tt.account_id order by $orderField $sortType LIMIT $startIndex , $limit");
		}
		$rows = $command->query();
		return $rows;

	
	}
	
	public function getCountAgentFundTransferHistory()
	{
		if (Yii::app()->session['account_type']=='agent'){
			$accountID="and tt.account_id="."'".$_GET['accountId']."%'";
		}else{
			$accountID='';
		}
		
		$testCurrency = ($_GET['test']!=0) ? ' AND tc.currency_name <>"TEST"' : '';
		
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['currencyId']=="ALL")
		{
			$command = $connection->createCommand("SELECT COUNT(0) from (SELECT COUNT(0) as counter FROM tbl_casino_balance_transmitter tt
					LEFT JOIN tbl_cash_player tcp ON tt.account_id=tcp.account_id
					LEFT JOIN tbl_currency tc ON tt.currency_id=tc.id
					WHERE  tt.account_type='a' $accountID $testCurrency AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY tt.account_id ) as fh");
		}
		else{
			$command = $connection->createCommand("SELECT COUNT(0) from (SELECT COUNT(0) as counter FROM tbl_casino_balance_transmitter tt
							LEFT JOIN tbl_cash_player tcp ON tt.account_id=tcp.account_id
							LEFT JOIN tbl_currency tc ON tt.currency_id=tc.id
							WHERE  tt.account_type='a' $accountID $testCurrency AND tt.currency_id='".$_GET['currencyId']."' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY tt.account_id) as fh");
		}
		$rows = $command->query();
				return $rows;
	
	}

	
	public function getExportAgentFundTransferHistory()
	{
		if (Yii::app()->session['account_type']=='agent'){
			$accountID="and tt.account_id="."'".$_GET['accountId']."%'";
		}else{
			$accountID='';
		}
		
		$testCurrency = ($_GET['test']!=0) ? ' AND tc.currency_name <>"TEST"' : '';

		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['currencyId']=="ALL")
		{
			$command = $connection->createCommand("SELECT '' as counter,tt.account_id,tcp.account_name,tc.currency_name FROM tbl_casino_balance_transmitter tt 
									LEFT JOIN tbl_cash_player tcp ON tt.account_id=tcp.account_id
									LEFT JOIN tbl_currency tc ON tt.currency_id=tc.id 
									WHERE  tt.account_type='a' $accountID $testCurrency AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY tt.account_id ");	
		}
		else{
			$command = $connection->createCommand("SELECT '' as counter,tt.account_id,tcp.account_name,tc.currency_name FROM tbl_casino_balance_transmitter tt
					LEFT JOIN tbl_cash_player tcp ON tt.account_id=tcp.account_id
					LEFT JOIN tbl_currency tc ON tt.currency_id=tc.id
					WHERE  tt.account_type='a' $accountID $testCurrency AND tt.currency_id='".$_GET['currencyId']."' AND transaction_date BETWEEN '".$_GET['dateFrom']."' AND '".$_GET['dateTo']."' GROUP BY tt.account_id ");
		}
		$rows = $command->query();
		return $rows;

	}
}



