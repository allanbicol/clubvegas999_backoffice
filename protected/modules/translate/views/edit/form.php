<?php $action=$model->getIsNewRecord() ? 'Create' : 'Update';?>
<h1><?php echo TranslateModule::t(($action) . ' Message')." # ".$model->id." - ".TranslateModule::translator()->acceptedLanguages[$model->language]; ?></h1>

<div class="form">
<table>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'message-form',
	'enableAjaxValidation'=>false,
)); ?>

	    
    <?php echo $form->hiddenField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
    <?php echo $form->hiddenField($model,'language',array('size'=>16,'maxlength'=>16)); ?>
    
    <div class="row">
    	<tr>
    		<td>
        	<?php echo $form->label($model->source,'category'); ?>
        	</td>
        	<td>
        		<?php echo $form->textField($model->source,'category',array('disabled'=>'disabled')); ?>
        	</td>
        </tr>
    </div>
    <div class="row">
    	<tr>
    		<td>
        		<?php echo $form->label($model->source,'message'); ?>
        	</td>
        	<td>
        		<?php echo $form->textField($model->source,'message',array('disabled'=>'disabled')); ?>
        	</td>
        </tr>
    </div>
    
	<div class="row">
		<tr>
			<td>
				<?php echo $form->labelEx($model,'translation'); ?>
			</td>
			<td>
				<?php echo $form->textArea($model,'translation',array('rows'=>3, 'cols'=>80,'style'=>'font-size: 20px;')); ?>
			</td>
			<td>
				<?php echo $form->error($model,'translation'); ?>
			</td>
		</tr>
	</div>

	<div class="row buttons">
		<tr>
			<td></td>
			<td>
				<?php echo CHtml::submitButton(TranslateModule::t($action)); ?>
			</td>
		</tr>
	</div>

<?php $this->endWidget(); ?>
</table>
</div><!-- form -->
