<?php
/**
 * @todo system setting general purpose model
 * @author leokarl
 * @since 2012-12-12
 *
 */

class SystemSetting
{
	/**
	 * @todo transaction logs
	 * @author leokarl
	 * @since 2012-12-12
	 * @param string $operated_by
	 * @param string $operated_by_level
	 * @param string $operated
	 * @param string $operated_level
	 * @param int $log_type_id
	 * @param string $log_details
	 */
	public function saveLog($operated_by, $operated_by_level, $operated, $operated_level, $log_type_id, $log_details){
		$log = new TableLog();
		$log->operated_by = $operated_by;
		$log->operated_by_level = $operated_by_level;
		$log->operated = $operated;
		$log->operated_level = $operated_level;
		$log->operation_time = date('Y-m-d H:i:s');
		$log->log_type_id = $log_type_id;
		$log->log_details = $log_details;
		$log->save();
	}
}