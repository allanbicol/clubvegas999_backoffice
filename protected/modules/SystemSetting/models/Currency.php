<?php
/**
 * @todo UserAccountList Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-06
 *
 */
class Currency
{
    /**
     * @todo getCurrencyList
     * @copyright CE
     * @author Leo karl
     * @since 2012-04-06
     *
     */
	public function getCurrencyList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT id,currency_name,FORMAT(exchange_rate,2) as exchange_rate,last_update,operator_id from tbl_currency ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo getCountCurrencyRecord
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-04-06
	 *
	 */
	public function getCountCurrencyRecord()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT COUNT(0) FROM tbl_currency");
		$rows = $command->query();
		return $rows;
	}
	
	/**
	 * @todo addNewCurrency
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-04-06
	 *
	 */
	Public function addNewCurrency()
	{
		$countCurrency=TableCurrency::model()->count(array('select'=>'currency_name','condition'=>'currency_name=:currency_name','params'=>array(':currency_name'=>$_GET['strCurrency']),));
		
		if ($countCurrency==0)
		{
			$post=new TableCurrency();
			$post->currency_name=$_GET['strCurrency'];
			$post->exchange_rate=$_GET['dblExchangeRate'];
			$post->last_update=new CDbExpression('NOW()');
			$post->operator_id=0;
			$post->save();
			
			$rd=TableCurrency::model()->find(array('select'=>'id,last_update','condition'=>'currency_name=:currency_name','params'=>array(':currency_name'=>$_GET['strCurrency']),));
			$timestamp = strtotime($rd['last_update']);
			$newdate = date("Y-m-d", $timestamp);
			$newtime= date("H:i:s", $timestamp);
		}
		echo $countCurrency . ";" . $newdate . " " . $newtime . ";" . $rd['id'];
	}
	
	/**
	 * @todo updateCurrency
	 * @copyright CE
	 * @author Leo karl
	 * @since 2012-04-06
	 *
	 */
	public function updateCurrency()
	{
		$dateTime = date('Y-m-d H:i:s');
		$AccountID = Yii::app()->session['account_id'];
		$level=Yii::app()->session['level_name'];
		$connection = Yii::app()->db_cv999_fd_master;
		//Get previous currency settings- Added by Allan |date:2012-06-29
        $command = $connection->createCommand("SELECT currency_name,exchange_rate from tbl_currency  WHERE  id='".$_GET["intUCurrencyID"]."' ");
        $rd=$command->queryRow();
        $name=$rd['currency_name'];
        $rate=$rd['exchange_rate'];
				
			
		$countCurrency=TableCurrency::model()->count(array('select'=>'currency_name','condition'=>'currency_name=:currency_name AND id!=:id','params'=>array(':currency_name'=>$_GET['strUCurrency'],':id'=>$_GET['intUCurrencyID']),));
		if ($countCurrency==0){
			TableCurrency::model()->updateAll(array('currency_name'=>$_GET['strUCurrency'],'exchange_rate'=>$_GET['dblUExchangeRate'],'last_update'=>new CDbExpression('NOW()'),'operator_id'=>Yii::app()->session['account_id']),'id='.$_GET["intUCurrencyID"]);
			
			$rd=TableCurrency::model()->find(array('select'=>'last_update','condition'=>'id=:id','params'=>array(':id'=>$_GET['intUCurrencyID']),));
			$timestamp = strtotime($rd['last_update']);
			$newdate = date("Y-m-d", $timestamp);
			$newtime= date("H:i:s", $timestamp);
			
			if ($name!=$_GET['strUCurrency'] || $rate!=$_GET['dblUExchangeRate']){
				$postLog=new TableLog;
				$postLog->operated_by=$AccountID;
				$postLog->operated_by_level=$level;
				$postLog->operated='Company';
				$postLog->operated_level='Company';
				$postLog->operation_time=$dateTime;
				$postLog->log_type_id=14;
				$postLog->log_details='<b>'.$level.' <label style=\"color:#7A5C00\">'.$AccountID.'</label> CHANGE currency setting from:<label style=\"color:red\">'.$name.'</label>='.$rate.' to:<label style=\"color:red\">'.$_GET['strUCurrency'].'='.$_GET['dblUExchangeRate'].'</b>';
				$postLog->save();
			}
		}
		echo $countCurrency . ";" . $newdate . " " . $newtime;
	}
}