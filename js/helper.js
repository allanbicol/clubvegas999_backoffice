$(document).ready(function() {
    $(".chose_en").click(function(){
        $("input[name$='lang']").val("en");
    })
    
    $(".chose_zh_cn").click(function(){
        $("input[name$='lang']").val("zh_cn");
    })
    
    $(".chose_zh_hk").click(function(){
        $("input[name$='lang']").val("zh_hk");
    })
    
    $(".chose_vi").click(function(){
        $("input[name$='lang']").val("vi");
    })
    
    $(".chose_th").click(function(){
        $("input[name$='lang']").val("th");
    })
});

