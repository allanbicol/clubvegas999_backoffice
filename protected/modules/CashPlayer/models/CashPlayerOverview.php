<?php
/**
 * @todo CashPlayerOverview Model
 * @copyright CE
 * @author Allan Bicol	
 * @since 2012-07-19
 */
class CashPlayerOverview
{
	public function getCashPlayerOverview()
	{
		if ($_GET['test_currency']==1){
			$test="WHERE tcp.currency_id<>9 AND tcp.test_real_currency<>1";
		}else{
			$test="";
		}
		$connection = Yii::app()->db_cv999_fd_master;
		$command = $connection->createCommand("SELECT (SELECT currency_name FROM tbl_currency tc WHERE tc.id=tcp.currency_id) AS currency,format(COUNT(account_id),0) AS no_players,SUM(balance) AS balance FROM tbl_cash_player tcp $test GROUP BY currency_id" );
		$rows = $command->query();
		return $rows;
	}
	
	public function getPlayersBalance()
	{
		if ($_POST['test']==1){
			$test=" where currency_id <>9 AND tcp.test_real_currency<>1";
			$testHTV="and htv.currency<>'TEST' and htv.currency<>'FUN'" ;
			$testSavan="and savan.currency<>'TEST' and savan.currency<>'FUN'";
			$testCosta="and costa.currency_name<>'TEST' and costa.currency_name<>'FUN'";
			
			$testHTVSlot="and htv.currency<>'TEST' and htv.currency<>'FUN'";
			$testSavanSlot="and savan.currency<>'TEST' and savan.currency<>'FUN'";
			$testVirtuaSlot="and virtua.currency_name<>'TEST' and virtua.currency_name<>'FUN'";
		}else{
			$test="";
			$testHTV="";
			$testSavan="";
			$testCosta="";
			$testHTVSlot="";
			$testSavanSlot="";
			$testVirtuaSlot="";
		}
		
		$yesterday=date("Y-m-d", strtotime("-1 day"));
		$connection = Yii::app()->db_cv999_fd_master;
		
		$command = $connection->createCommand("SELECT currency_name,exchange_rate from tbl_currency where id='".$_POST['currency']."'");
		$rd=$command->queryRow();
		$currency=$rd['currency_name'];
		$rate=$rd['exchange_rate'];
		
		$command1 = $connection->createCommand("SELECT FORMAT(ROUND(SUM(converted),2),2) AS total_balace FROM (SELECT balance,(SELECT exchange_rate FROM tbl_currency AS tc WHERE tc.id=tcp.currency_id) AS ex_rate,(balance/(SELECT exchange_rate FROM tbl_currency AS tc WHERE tc.id=tcp.currency_id))*'".$rate."' AS converted FROM tbl_cash_player AS tcp $test) AS con");
		$rd1=$command1->queryRow();
		$result=$rd1['total_balace'];
		
		$command2 = $connection->createCommand("SELECT ROUND((IFNULL(htv_winloss,0) + IFNULL(savan_winloss,0)+IFNULL(costa_winloss,0)),2) AS total_winloss FROM ((SELECT SUM((htv.win_loss/tc.exchange_rate)*'".$rate."') AS htv_winloss  FROM tbl_htv_casino_history  htv INNER JOIN tbl_currency tc ON tc.currency_name=htv.currency WHERE htv.bet_time BETWEEN '".$yesterday." 00:00:00' AND '".$yesterday." 23:59:59' and account_type='c'  $testHTV) AS htv,
												(SELECT SUM((savan.win_loss/tc.exchange_rate)*'".$rate."') AS savan_winloss  FROM tbl_savan_casino_history_v2  savan INNER JOIN tbl_currency tc ON tc.currency_name=savan.currency WHERE savan.end_time BETWEEN '".$yesterday." 00:00:00' AND '".$yesterday." 23:59:59'  and account_type='c' $testSavan) AS savan,
												(	SELECT SUM(((costa.amount_settlements - costa.amount_wagers)/tc.exchange_rate)*'".$rate."') AS costa_winloss  FROM tbl_costavegas_casino_history  costa INNER JOIN tbl_currency tc ON tc.currency_name=costa.currency_name WHERE costa.bet_date BETWEEN '".$yesterday." 00:00:00' AND '".$yesterday." 23:59:59' and account_type='c' $testCosta) AS costa)");
		$rd2=$command2->queryRow();
		$totalWinLoss=$rd2['total_winloss'];
		
		$command3 = $connection->createCommand("SELECT ROUND((IFNULL(htv_winloss,0) + IFNULL(savan_winloss,0) + IFNULL(virtua_winloss,0)),2) AS total_slot_winloss FROM ((SELECT SUM((htv.win_loss/tc.exchange_rate)*'".$rate."') AS htv_winloss  FROM tbl_htv_bet_slot_record_casino_history  htv INNER JOIN tbl_currency tc ON tc.currency_name=htv.currency WHERE htv.bet_time BETWEEN '".$yesterday." 00:00:00' AND '".$yesterday." 23:59:59' and account_type=1 $testHTVSlot) AS htv,
												(SELECT SUM((savan.win_loss/tc.exchange_rate)*'".$rate."') AS savan_winloss  FROM tbl_slotsvegas_casino_history  savan INNER JOIN tbl_currency tc ON tc.currency_name=savan.currency WHERE savan.create_time BETWEEN '".$yesterday." 00:00:00' AND '".$yesterday." 23:59:59' and account_type='c' $testSavanSlot) AS savan,
												(SELECT SUM((virtua.win_loss/tc.exchange_rate)*'".$rate."') AS virtua_winloss  FROM tbl_virtuavegas_casino_history  virtua INNER JOIN tbl_currency tc ON tc.currency_name=virtua.currency_name WHERE virtua.bet_date BETWEEN '".$yesterday." 00:00:00' AND '".$yesterday." 23:59:59' AND account_type='c' $testVirtuaSlot) AS virtua)");
		$rd3=$command3->queryRow();
		$totalSlotWinLoss=$rd3['total_slot_winloss'];
		
		echo $result.'#'.$currency.'#'.$totalWinLoss.'#'.$totalSlotWinLoss;
	}
	
	
	
}






