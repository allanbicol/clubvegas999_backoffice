<?php
/**
 * @todo CreateTableLimitController
 * @copyright CE
 * @author Leo karl
 * @since 2012-04-30
 */
class CreateTableLimitController extends MyController
{
	public function actionCreateBacarratLimit()
	{
		
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->addNewBacarratLimit();
		}
	}
	public function actionCreateDragonTigerLimit()
	{
		
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->addNewDragonTigerLimit();
		}
	}
	public function actionCreateRouletteLimit()
	{
		
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->addNewRouletteLimit();
		}
	}
	public function actionCreateBlackjackLimit()
	{
		
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->addNewBlackjackLimit();
		}
	}
	public function actionCreateAmericanRouletteLimit()
	{
		
		if(Yii::app()->user->checkAccess('gameSetting.writeTableLimit'))
		{
			$tl=new TableLimit();
			$tl->addNewAmericanRouletteLimit();
		}
	}
	public function actionIndex()
	{
		$this->render("index");
	}
}
