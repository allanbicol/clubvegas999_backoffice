<?php

/**
 * This is the model class for table "tbl_cash_player".
 @Author: Allan Bicol
 @Since: 11/21/2012
 */


class CashPlayerDeductionTransHistory extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_cash_player_trans_history';
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

}
