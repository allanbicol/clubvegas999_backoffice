<?php
/**
 * @todo tbl_game model
 * @copyright CE
 * @author Allan Bicol
 * @since 2013-03-21
 */
 

class TableGame extends CActiveRecord //CV99ActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDbConnection()
	{
		return Yii::app()->db_cv999_fd_master;
	}

	public function tableName()
	{
		return 'tbl_game';
	}
}