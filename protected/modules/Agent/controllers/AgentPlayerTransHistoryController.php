<?php
class AgentPlayerTransHistoryController extends MyController
{
	public function actionAgentPlayerTransHistory()
	{
		//echo $_GET['AccountID'];
		$cpHfund = new AgentPlayerTransHistory;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$orderField = $_GET['sidx'];
		$sortType = $_GET['sord'];
		
		$result=$cpHfund->getCountAgentPlayerTransHistory();
		$data=$result->readAll();
		
		$records=$data[0]['COUNT(0)'];
		
		if( $records > 0 && $limit > 0) {
			$total_pages = ceil($records/$limit);
		} else {
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page=$total_pages;
		
		if ($records == 0){
			$startIndex=0;
		}else{
			$startIndex = $limit*$page - $limit;
		}
		$player_records = $cpHfund->getAgentPlayerTransHistory($orderField, $sortType, $startIndex, $limit);
		//$test = $records->readAll();
		$filedNames = array("transaction_number","trans_type_id","currency","amount","credit_before","credit_after","balance_before","balance_after","transaction_date","operator_id");
		$htmvalue="";
		echo JsonUtil::generateJqgridData($player_records->readAll(), $total_pages, $page, $records,$filedNames,$htmvalue,6);
	
			
	}
	
	
	public function actionIndex()
	{
		if(Yii::app()->user->checkAccess('agent.readTransactionHistory') ==true ||
		    Yii::app()->session['level'] =='SC' ||
			Yii::app()->session['level'] =='SMA' ||
			Yii::app()->session['level'] =='MA' ||
			Yii::app()->session['level'] =='AGT' 
		   )
			$this->render("index");
		else
			$this->redirect(Yii::app()->request->baseUrl ."/index.php?r=AuthError");
	}
	
	
}