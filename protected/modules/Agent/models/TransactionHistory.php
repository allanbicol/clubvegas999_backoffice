<?php
/**
 * @todo TransactionHistory Model
 * @copyright CE
 * @author Leo karl
 * @since 2012-05-15
 */
class TransactionHistory
{
	//FUND TRANSACTION HISTORY===============================================================================================
	public function getTransactionHistoryList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and b.currency_name<>'TEST'";
		}else{
			$test="";
		}
		if ($_GET['accountID']==""){
			$andAccountId='';
		}else{
			$andAccountId="and a.agent_account_id ="."'".$_GET['accountID']."'";
		}
		
		$command ='';
		
		if (Yii::app()->user->checkAccess('agent.readTransactionHistory'))
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a 
				inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=2 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId   and a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date between '" . $_GET['dtfrom'] . "' AND '" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=2 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.transaction_date between '" . $_GET['dtfrom'] . "' AND '" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="SC")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=4 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=4 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="SMA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="MA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="AGT")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		
		$rows = $command->query();
		return $rows;
	}
	
public function getCountAgentTransactionHistory()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and b.currency_name<>'TEST'";
		}else{
			$test="";
		}
		if ($_GET['accountID']==""){
			$andAccountId='';
		}else{
			$andAccountId="and a.agent_account_id ="."'".$_GET['accountID']."'";
		}
		
		if (Yii::app()->user->checkAccess('agent.readTransactionHistory'))
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a 
				inner join  tbl_currency b on a.currency_id=b.id  WHERE LENGTH(a.agent_account_id)=2 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id  WHERE LENGTH(a.agent_account_id)=2 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="SC")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE  LENGTH(a.agent_account_id)=4 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE  LENGTH(a.agent_account_id)=4 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="SMA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="MA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="AGT")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10 and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10  and (a.trans_type_id=7 or a.trans_type_id=8) $andAccountId $test and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		
		$rows = $command->query();
		return $rows;
	}
	//================================================================================================================
	
	//AVAILABLE CREDIT TRANSACTION HISTORY===============================================================================
	public function getAvailableCreditTransactionHistoryList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and b.currency_name<>'TEST'";
		}else{
			$test="";
		}
		if ($_GET['accountID']==""){
			$andAccountId='';
		}else{
			$andAccountId="and a.agent_account_id ="."'".$_GET['accountID']."'";
		}
		
		
		if (Yii::app()->user->checkAccess('agent.readTransactionHistory'))
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a 
				inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=2 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4)  and a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date between '" . $_GET['dtfrom'] . "' AND '" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=2 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.transaction_date between '" . $_GET['dtfrom'] . "' AND '" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="SC")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=4 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and  a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%' AND a.agent_account_id!='" . trim(Yii::app()->session['account_id']) . "' AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=4 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%' AND a.agent_account_id!='" . trim(Yii::app()->session['account_id']) . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="SMA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="MA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="AGT")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountAvailableCreditAgentTransactionHistory()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and b.currency_name<>'TEST'";
		}else{
			$test="";
		}
		if ($_GET['accountID']==""){
			$andAccountId='';
		}else{
			$andAccountId="and a.agent_account_id ="."'".$_GET['accountID']."'";
		}
		
		if (Yii::app()->user->checkAccess('agent.readTransactionHistory'))
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a 
				inner join  tbl_currency b on a.currency_id=b.id  WHERE LENGTH(a.agent_account_id)=2 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id  WHERE LENGTH(a.agent_account_id)=2 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="SC")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE  LENGTH(a.agent_account_id)=4 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE  LENGTH(a.agent_account_id)=4 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="SMA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=6 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="MA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 $andAccountId and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=8 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="AGT")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10 $andAccountId  and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)=10 $andAccountId $test and (a.trans_type_id=2 or a.trans_type_id=4) and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		
		$rows = $command->query();
		return $rows;
	}
	//================================================================================================================
	
	//BONUS TRANSACTION HISTORY===============================================================================
	public function getBonusTransactionHistoryList($orderField, $sortType, $startIndex , $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and b.currency_name<>'TEST'";
		}else{
			$test="";
		}
		
		if (Yii::app()->user->checkAccess('agent.readTransactionHistory'))
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a 
				inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=2 and  a.trans_type_id=6  and a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date between '" . $_GET['dtfrom'] . "' AND '" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=2 $test and  a.trans_type_id=6 and a.transaction_date between '" . $_GET['dtfrom'] . "' AND '" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="SC")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=2 and  a.trans_type_id=6 and  a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=2 $test and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="SMA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=4 and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=4 $test and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="MA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=6 and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=6 $test and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		elseif (Yii::app()->session['level']=="AGT")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=8 and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}else{
				$command = $connection->createCommand("SELECT a.agent_account_id,a.currency_id, b.currency_name,COUNT(a.agent_account_id) as number, SUM(amount) as total_amnt FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=8 $test and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id ORDER BY $orderField $sortType LIMIT $startIndex , $limit");
			}
		}
		
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountBonusAgentTransactionHistory()
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if ($_GET['test']!=0){
			$test="and b.currency_name<>'TEST'";
		}else{
			$test="";
		}
		
		
		if (Yii::app()->user->checkAccess('agent.readTransactionHistory'))
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a 
				inner join  tbl_currency b on a.currency_id=b.id  WHERE LENGTH(a.agent_account_id)>=2 and  a.trans_type_id=6 and a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id  WHERE LENGTH(a.agent_account_id)>=2 $test and  a.trans_type_id=6 and a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="SC")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=2 and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=2 $test and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="SMA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=4 and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=4 $test and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="MA")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=6 and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=6 $test and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		elseif (Yii::app()->session['level']=="AGT")
		{
			if($_GET['currency_id']!=0){
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=8 and  a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.currency_id=" . $_GET['currency_id'] . " AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}else{
				$command = $connection->createCommand("SELECT COUNT(counter) FROM (SELECT COUNT(0) as counter FROM vwGetAgentAndPlayerTransactionHistory a
						inner join  tbl_currency b on a.currency_id=b.id WHERE LENGTH(a.agent_account_id)>=8 $test and a.trans_type_id=6 and a.agent_account_id like '". trim(Yii::app()->session['account_id']) ."%'  AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' GROUP BY a.currency_id,a.agent_account_id) as F");
			}
		}
		
		$rows = $command->query();
		return $rows;
	}
	//================================================================================================================
	
	public function getTransactionHistoryDetails($orderField, $sortType, $startIndex, $limit)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if($_GET['type']==1){
			$command = $connection->createCommand("SELECT a.transaction_number,a.agent_account_id, b.currency_name, a.amount,a.trans_type_id,a.credit_before,a.credit_after,a.balance_before,a.balance_after,a.transaction_date,a.operator_id,a.market_type FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id WHERE (a.trans_type_id=7 or a.trans_type_id=8) and a.currency_id=" . $_GET['currency_id'] . " AND  a.agent_account_id='" . $_GET['account_id'] . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' ORDER BY $orderField $sortType");
		}elseif ($_GET['type']==2) {
			$command = $connection->createCommand("SELECT a.transaction_number,a.agent_account_id, b.currency_name, a.amount,a.trans_type_id,a.credit_before,a.credit_after,a.balance_before,a.balance_after,a.transaction_date,a.operator_id,a.market_type FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id WHERE (a.trans_type_id=2 or a.trans_type_id=4) and a.currency_id=" . $_GET['currency_id'] . " AND  a.agent_account_id='" . $_GET['account_id'] . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' ORDER BY $orderField $sortType");
		}elseif ($_GET['type']==3) {
			$command = $connection->createCommand("SELECT a.transaction_number,a.agent_account_id, b.currency_name, a.amount,a.trans_type_id,a.credit_before,a.credit_after,a.balance_before,a.balance_after,a.transaction_date,a.operator_id,a.market_type FROM vwGetAgentAndPlayerTransactionHistory a
				inner join  tbl_currency b on a.currency_id=b.id WHERE a.trans_type_id=6 and a.currency_id=" . $_GET['currency_id'] . " AND  a.agent_account_id='" . $_GET['account_id'] . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "' ORDER BY $orderField $sortType");
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getCountTransactionHistory()
	{
		$connection = Yii::app()->db_cv999_fd_master;
// 		$command = $connection->createCommand("SELECT COUNT(a.agent_account_id) as cnt FROM vwGetAgentAndPlayerTransactionHistory a
// 					inner join  tbl_currency b on a.currency_id=b.id WHERE a.currency_id=" . $_GET['currency_id'] . " AND  a.agent_account_id='" . $_GET['account_id'] . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "'");
		if($_GET['type']==1){
			$command = $connection->createCommand("SELECT COUNT(a.transaction_number) as cnt FROM vwGetAgentAndPlayerTransactionHistory a
					inner join  tbl_currency b on a.currency_id=b.id WHERE (a.trans_type_id=7 or a.trans_type_id=8) and a.currency_id=" . $_GET['currency_id'] . " AND  a.agent_account_id='" . $_GET['account_id'] . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "'");
		}elseif ($_GET['type']==2) {
			$command = $connection->createCommand("SELECT COUNT(a.transaction_number) as cnt FROM vwGetAgentAndPlayerTransactionHistory a
					inner join  tbl_currency b on a.currency_id=b.id WHERE (a.trans_type_id=2 or a.trans_type_id=4) and a.currency_id=" . $_GET['currency_id'] . " AND  a.agent_account_id='" . $_GET['account_id'] . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "'");
		}elseif ($_GET['type']==3) {
			$command = $connection->createCommand("SELECT COUNT(a.transaction_number) as cnt FROM vwGetAgentAndPlayerTransactionHistory a
					inner join  tbl_currency b on a.currency_id=b.id WHERE a.trans_type_id=6 and a.currency_id=" . $_GET['currency_id'] . " AND  a.agent_account_id='" . $_GET['account_id'] . "' AND a.transaction_date>='" . $_GET['dtfrom'] . "' AND a.transaction_date<='" . $_GET['dtto'] . "'");
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getSumTransaction($type,$account_id,$except_test_currency)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$qry_test_currency='';
		if($except_test_currency==0){
			$qry_test_currency='';
		}else{
			$qry_test_currency=' AND b.currency_name!="TEST"';
		}
		if($account_id==''){
			$command = $connection->createCommand("SELECT sum(total_credit) as total_credit,sum(total_assigned_credit) as total_assigned_credit,sum(total_percentage_credit_val) as total_percentage_credit_val FROM (
					    SELECT (case when b.currency_name='USD' then a.credit else a.credit/b.exchange_rate end) as total_credit, 
							   (case when b.currency_name='USD' then a.credit_assigned else a.credit_assigned/b.exchange_rate end) as total_assigned_credit,
					           (case when b.currency_name='USD' then a.credit_percentage else a.credit_percentage/b.exchange_rate end) as total_percentage_credit_val
					    FROM tbl_agent a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE a.agent_type='" . $type . "' $qry_test_currency) t");
		}else{
			$command = $connection->createCommand("SELECT sum(total_credit) as total_credit,sum(total_assigned_credit) as total_assigned_credit,sum(total_percentage_credit_val) as total_percentage_credit_val FROM (
					    SELECT (case when b.currency_name='USD' then a.credit else a.credit/b.exchange_rate end) as total_credit, 
							   (case when b.currency_name='USD' then a.credit_assigned else a.credit_assigned/b.exchange_rate end) as total_assigned_credit,
					           (case when b.currency_name='USD' then a.credit_percentage else a.credit_percentage/b.exchange_rate end) as total_percentage_credit_val
					    FROM tbl_agent a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE a.agent_type='" . $type . "' AND a.agent_parent_id='" . $account_id . "' $qry_test_currency) t");
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getSumParentTransaction($type,$account_id,$except_test_currency)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		$qry_test_currency='';
		if($except_test_currency==0){
			$qry_test_currency='';
		}else{
			$qry_test_currency=' AND currency_id!=9';
		}
		if($type!='MEM'){
			//$command = $connection->createCommand("SELECT SUM(credit) as total_credit,SUM(credit_assigned) as total_assigned_credit,SUM(credit_assigned*(credit_percentage/100)) as total_percentage_credit_val FROM tbl_agent WHERE agent_type='" . $type . "' AND account_id like '" . $account_id . "%' $qry_test_currency");
			$command = $connection->createCommand("SELECT SUM(credit) as total_credit,SUM(credit_assigned) as total_assigned_credit,SUM(credit_percentage) as total_percentage_credit_val FROM tbl_agent WHERE agent_type='" . $type . "' AND account_id like '" . $account_id . "%' $qry_test_currency");
		}else{
			if($account_id!=''){
				$command = $connection->createCommand("SELECT SUM(credit) as total_credit,0 as total_assigned_credit,0 as total_percentage_credit_val FROM tbl_agent_player WHERE account_id like '" . $account_id . "%' $qry_test_currency");
			}else{
				if($qry_test_currency!=''){
					$command = $connection->createCommand("SELECT SUM(CASE WHEN b.currency_name='USD' THEN a.credit ELSE a.credit/b.exchange_rate END) as total_credit,0 as total_assigned_credit,0 as total_percentage_credit_val FROM tbl_agent_player a INNER JOIN tbl_currency b ON a.currency_id=b.id WHERE currency_id!=9");
				}else{
					$command = $connection->createCommand("SELECT SUM(CASE WHEN b.currency_name='USD' THEN a.credit ELSE a.credit/b.exchange_rate END) as total_credit,0 as total_assigned_credit,0 as total_percentage_credit_val FROM tbl_agent_player a INNER JOIN tbl_currency b ON a.currency_id=b.id");
				}
			}
		}
		$rows = $command->query();
		return $rows;
	}
	
	public function getStatusNo($type,$status_id,$account_id)
	{
		$connection = Yii::app()->db_cv999_fd_master;
		if($type!='MEM'){
			$command = $connection->createCommand("SELECT COUNT(account_id) as total FROM tbl_agent WHERE agent_type='" . $type . "' AND agent_status_id=". $status_id ." AND account_id like '". $account_id ."%'");
		}else{
			$command = $connection->createCommand("SELECT COUNT(account_id) as total FROM tbl_agent_player WHERE status_id=". $status_id ." AND account_id like '". $account_id ."%'");
		}
		$rows = $command->query();
		return $rows;
	}
	
}
